<?php

/**
 * ページ表示データクラス
 * 
 */
class PageData
{
    /**
     * ページリンク部品を表示する差分ページ数
     */
    const PAGEITEM_PUT_DIFF  = 1;


    /**
     * ページデータ
     *
     * @var array[行][カラム名 => 値]
     */
    public $pageDatas;

    /**
     * ページ数
     *
     * @var integer
     */
    public $pageCount;

    /**
     * 現在のページ番号
     *
     * @var integer
     */
    public $currentPageNo;

    /**
     * 前ページのページ番号
     *
     * @var integer
     */
    public $prevPageNo;

    /**
     * 次ページのページ番号
     *
     * @var integer
     */
    public $nextPageNo;

    /**
     * ページャー部品情報
     *
     * @var [type]
     */
    public $pagerItems;


    /**
     * コンストラクタ
     */
    public function __construct($pageDatas, $pageCount, $currentPageNo) {
        // 基本情報を設定。
        $this->pageDatas     = $pageDatas;
        $this->pageCount     = $pageCount;
        $this->currentPageNo = $currentPageNo;

        // ページャー部品の情報を作成する。
        $this->getPagerParts();
    }


    /**
     * ページャー部品の情報を作成する。
     *
     */
    private function getPagerParts() {
        // 部品情報を生成。
        $this->pagerItems = array();
        
        $isSetHellip = false;
        for ($i = 1; $i <= $this->pageCount; $i++) {
            if ($i == 1 or $i == $this->pageCount or abs($i - $this->currentPageNo) <= self::PAGEITEM_PUT_DIFF) {
                // ページリンク部品の情報を登録。
                // (先頭/最終ページ or 現在ページに隣接するページについて、リンク設定用の情報を設定。)
                $this->pagerItems[] = $i;
                $isSetHellip = false;
            } else if (!$isSetHellip) {
                // ページ存在(リンク非設定)の情報を登録。
                $this->pagerItems[] = "";
                $isSetHellip = true;
            }
        }

        // 前/次ページの情報を設定。
        $this->prevPageNo = ($this->currentPageNo > 1               ) ? $this->currentPageNo - 1 : "";
        $this->nextPageNo = ($this->currentPageNo < $this->pageCount) ? $this->currentPageNo + 1 : "";
    }

}
