﻿<?php
/*
メール送信クラス
*/
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class ses_mailSender{

	private $SESMail;
	
    /**
     * コンストラクタ
     */
    public function __construct() {
		$this->SESMail = new PHPMailer(true);
    }


    /**
     * メール送信処理
	 * 引数
	 * $sender　　　　送信元（空文字で省略可）デフォルト設定される
	 * $senderName　　送信者名
	 * $toAdd         送信先（必須）
	 * $subject　　　　タイトル（必須）
	 * $mailText
	 * 
	 * 戻り値
	 * 　　成功時　"OK"
	 * 　　異常時　上記以外。"NG"もしくはエクセプションメッセージ
     */
	public function mailSend($sender, $senderName, $toAdd, $subject, $mailText) {
		//From
		if($sender==""){
			$sender = '送信元';
		}
		if($senderName==""){
			$senderName = '送信者名';
		}

		if($toAdd=="" or $mailText==""){
			return "NG";
		}
		//To
		$recipient = $toAdd;

		// Replace smtp_username with your Amazon SES SMTP user name.
		$usernameSmtp = 'ユーザー名';

		// Replace smtp_password with your Amazon SES SMTP password.
		$passwordSmtp = 'パスワード';

		// Specify a configuration set. If you do not want to use a configuration
		// set, comment or remove the next line.
		//$configurationSet = 'ConfigSet';

		// If you're using Amazon SES in a region other than US West (Oregon),
		// replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP
		// endpoint in the appropriate region.
		$host = 'ホスト名';
		$port = ポート番号;

		// The subject line of the email
		if ($subject == '') {
			$subject = 'info ftom talent-asia.jp';
		}

		// The plain-text body of the email
		$bodyText =  $mailText;

		// The HTML-formatted body of the email
		// $bodyHtml = '<h1>Email Test</h1>
		//     <p>This email was sent through the
		//     <a href="https://aws.amazon.com/ses">Amazon SES</a> SMTP
		//     interface using the <a href="https://github.com/PHPMailer/PHPMailer">
		//     PHPMailer</a> class.</p>';
		$bodyHtml = '';

		$mail = new PHPMailer(true);

		try {
		    // Specify the SMTP settings.
		    $this->SESMail->isSMTP();
		    $this->SESMail->setFrom($sender, $senderName);
		    $this->SESMail->Username   = $usernameSmtp;
		    $this->SESMail->Password   = $passwordSmtp;
		    $this->SESMail->Host       = $host;
		    $this->SESMail->Port       = $port;
		    $this->SESMail->SMTPAuth   = true;
			$this->SESMail->SMTPSecure = 'tls';
			$this->SESMail->CharSet    = 'UTF-8';
		    // $this->SESMail->addCustomHeader('X-SES-CONFIGURATION-SET', $configurationSet);

		    // Specify the message recipients.
		    $this->SESMail->addAddress($recipient);
		    // You can also add CC, BCC, and additional To recipients here.

			// Specify the content of the message.
		    $this->SESMail->isHTML(false);
		    $this->SESMail->Subject    = $subject;
		    $this->SESMail->Body       = $bodyText;
		    //$this->SESMail->AltBody    = $bodyHtml;

			$this->SESMail->Send();

			return "OK";
		} catch (phpmailerException $e) {
		    echo "An error occurred. {$e->errorMessage()}", PHP_EOL; //Catch errors from PHPMailer.
		    return $e->errorMessage();

		} catch (Exception $e) {
		    echo "Email not sent. {$mail->ErrorInfo}", PHP_EOL; //Catch errors from Amazon SES.
		    return $e->errorMessage();
		}
	}
}
