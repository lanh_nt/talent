<?php

/**
 * DB処理クラス
 * 
 */
class JinzaiDb
{
    // DBコネクション
    private $con;

    // 更新ユーザ種別
    private $userType;
    // 更新ユーザID(RowID)
    private $userRowId;


    /**
     * コンストラクタ
     * 
     * @param string $conSettings 接続文字列
     */
    public function __construct($conSettings) {
        // コネクションを生成。
        $this->con = new PDO(
            'mysql:host='.$conSettings['host'].';dbname='.$conSettings['dbname'], 
            $conSettings['user'], 
            $conSettings['pass']
        );
        // 異常時に例外を発生する。
        $this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // 更新ユーザ情報を設定する。
        if (isset($_SESSION['loginUserType'])) {
            $this->userType  = $_SESSION['loginUserType'];
            $this->userRowId = $_SESSION['loginUserRowId'];
        } else {
            $this->userType  = "9";
            $this->userRowId = 0;
        }
    }

    /**
     * 更新ユーザ種別を設定する。
     * <p>※非ログイン状態の場合、ログイン情報のセッションが未設定となるため、本メソッドにて外部から与える必要がある。</p>
     *
     * @param string $userType 更新ユーザ種別
     */
    public function setUserType($userType) {
        if ($userType != null) {
            $this->userType = $userType;
        } else {
            $this->userType = "9";
        }
    }
    /**
     * 更新ユーザID(RowID)を設定する。
     * <p>※非ログイン状態の場合、ログイン情報のセッションが未設定となるため、本メソッドにて外部から与える必要がある。</p>
     *
     * @param string $userType 更新ユーザID(RowID)
     */
    public function setUserRowId($userRowId) {
        if ($userRowId != null) {
            $this->userRowId = $userRowId;
        } else {
            $this->userRowId = 0;
        }
    }


    /**
     * トランザクションを開始(beginTransaction)する。
     *
     * @return void
     */
    public function beginTransaction() {
        $this->con->beginTransaction();
    }

    /**
     * トランザクションを承認(commit)する。
     *
     * @return void
     */
    public function commit() {
        $this->con->commit();
    }

    /**
     * トランザクションを破棄(rollback)する。
     * 
     * @param Exception $e 発生した例外
     *
     * @return void
     */
    public function rollback($e = null) {
        // トランザクションをロールバックする。
        $this->con->rollback();

        $this->exception_log($e);
    }
    
    /**
     * 異常時のSQLログを登録する
     * 
     * @param Exception $e 発生した例外
     *
     * @return void
     */
    private function exception_log($e = null) {
        // 例外の内容をロギングする。
        if ($e != null) {
            if (LOGGING_UPDATE_ERROR) {
                $log = $this->con->prepare(
                    "INSERT INTO log_update_data( "
                    .    "user_type, user_id, create_date, log_text "
                    .") VALUES (  "
                    .   ":user_type, :user_id, NOW(), :log_text "
                    .") "
                );
    
                $log->bindValue(":user_type", $this->userType);
                $log->bindValue(":user_id",   $this->userRowId);
                $log->bindValue(":log_text",  "[ERR]\n".$e->getMessage());
    
                $log->execute();
            }
            if (DISP_UPDATE_ERROR) {
                echo $e->getMessage();
            }
        }
    }

    /**
     * 問合せを発行し、結果を返す。
     *
     * @param string                   $sql    SQL文
     * @param array[パラメータ名 => 値] $params パラメータ
     * 
     * @return array[行][カラム名 => 値] 問合せ結果
     */
    public function select($sql, $params) {
        // プリペアードステートメントを作成。
        $stmt =  $this->con->prepare($sql);

        // パラメータを作成。
        foreach($params as $key => $value) {
            $stmt->bindValue($key, $value);
        }

        //-----------------------------
        // 問合せSQLを発行し、結果を取得。
        //-----------------------------
        $stmt->execute();
        $sqlResult = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // 問合せから、返却内容を再構成して返す。
        // (こうすることで、内容変更が可能な配列となり、呼出元でキー→文言の設定等が可能となる。)
        $result = array();
        foreach ($sqlResult as $sqlRow) {
            $row = array();
            foreach ($sqlRow as $itemName => $itemValue) {
                $row[$itemName] = $itemValue;
            }
            $result[] = $row;
        }
        return $result;
    }

    /**
     * 問合せを発行し、先頭の1行を返す。
     *
     * @param string                   $sql    SQL文
     * @param array[パラメータ名 => 値] $params パラメータ
     * 
     * @return array[カラム名 => 値]|null 問合せ結果(先頭の1行)(結果がゼロ件の場合は null)
     */
    public function selectOne($sql, $params) {
        $result = $this->select($sql, $params);

        if (count($result) > 0) {
            return $result[0];
        } else {
            return null;
        }
    }

    /**
     * 問合せを発行し、検索画面用の結果を返す。
     *
     * @param string  $sql             SQL文
     * @param array[パラメータ名 => 値] $params パラメータ
     * @param integer $pageNo          ページ番号
     * @param integer $pageLimitCount  ページ表示件数
     * 
     * @return PageData 表示データ情報
     */
    public function getPageData($sql, $params, $pageNo = 1, $pageLimitCount = PAGING_ITEM_COUNT) {
        // データ件数を取得する。
        // ・データを全件取得し、その件数をカウント。
        $countSql = "SELECT COUNT(*) FROM (" . $sql . ") tmp ";
        $stmt =  $this->con->prepare($countSql);
        foreach($params as $key => $value) {
            $stmt->bindValue($key, $value);
        }
        $stmt->execute();
        $recordCount = $stmt->fetchColumn();

        if ($recordCount == 0) {
            // データが存在しない場合、空のページデータを返す。
            $pageData = new PageData(array(), 0, 1);
            return $pageData;
        }

    	// ページ数を計算。
    	$pageCount = ceil($recordCount / $pageLimitCount);
    	
        // 取得データの範囲を計算。
        $startPageNo = 0;
        $startRecordIndex = 0;

        if ($pageNo * $pageLimitCount < $recordCount) {
			$startPageNo = $pageNo;
        } else {
			$startPageNo = ceil($recordCount / $pageLimitCount);
        }
		$startRecordIndex = ($startPageNo - 1) * $pageLimitCount;

        // データ取得用SQLを生成。
        // ・元のSQL文に取得範囲(LIMIT句)を追加。
        $dataSql = $sql . " LIMIT ".$startRecordIndex.",".$pageLimitCount." ";

        // 実際のデータ取得SQLを発行。
        $result = $this->select($dataSql, $params);

        // 表示データ情報を返す。
        $pageData = new PageData($result, $pageCount, $startPageNo);
        return $pageData;
    }


    /**
     * WHERE条件の配列から、WHERE句を生成して返す。
     * 
     * @param string[] $whereSqls WHERE条件の配列
     * 
     * @return string WHERE句を表す文字列
     */
    public function getWhereSql($whereSqls) {
        $buf = "";
        foreach ($whereSqls as $whereItem) {
            if ($buf != "") {
                $buf .= "AND ";
            }
            $buf .= $whereItem . " ";
        }
        return $buf;
    }


    /**
     * ラベル情報を返す。
     *
     * @param string $functionId 機能ID
     * @param string $sceneId    画面ID
     * @param string $lang       言語
     * 
     * @return array[キー => 値] ラベル情報
     */
    public function getLabels($functionId, $sceneId, $lang) {
        // 取得する言語に対応するカラムを設定。
        if ($lang == '2') {
            $langColumn = 'lang2';
        } else if ($lang == '3') {
            $langColumn = 'lang3';
        } else {
            $langColumn = 'lang1';
        }

        // 問合せを発行。
        $sql = "SELECT label_id, ".$langColumn." AS value "
            . "FROM mst_gui_label "
            . "WHERE function_id = :function_id AND scene_id = :scene_id "
            . "ORDER BY label_id ";
        $params = array(
            ":function_id" => $functionId,
            ":scene_id"    => $sceneId
        );

        $result = $this->select($sql, $params);

        // 問合せ結果を、返却用に再構成。
        $labels = array();
        foreach ($result as $row) {
            $labels[$row["label_id"]] = $row["value"];
        }

        // ラベル情報を返す。
        return $labels;
    }


    /**
     * INSERT文を発行する。
     *
     * @param string $tableName テーブル名
     * @param array[カラム名 => 値 or array[値,型]] $columns カラム情報
     * 
     * @return integer レコード件数
     */
    public function insert($tableName, $columns) {
        try{
            // 項目一覧から、カラム定義SQL・パラメータ定義SQLを生成。
            $columnSql = "";
            $paramSql  = "";
            foreach($columns as $columnName => $value) {
                if ($columnSql != "") {
                    $columnSql .= ", ";
                    $paramSql  .= ", ";
                }
                $columnSql  .= $columnName;
                $paramSql .= ":".$columnName;
            }

            // SQL文を生成。
            $sql = "INSERT INTO ".$tableName." ( "
                .     $columnSql
                .     ", create_id, create_date"
                .     ", update_id, update_date "
                . ") VALUES ( "
                .     $paramSql
                .     ", :loginUserRowId, now() "
                .     ", :loginUserRowId, now() "
                . ") ";

            // プリペアードステートメントを作成。
            $insert = $this->con->prepare($sql);

            // パラメータをバインド。
            $sqlParams = array();
            foreach($columns as $columnName => $value) {
                $sqlParams[":".$columnName] = $value;
            }
            $sqlParams[":loginUserRowId"] = $this->userRowId;

            foreach($sqlParams as $paramName => $value) {
                if (is_array($value)) {
                    $insert->bindValue($paramName, $value[0], $value[1]);  // 値&型
                } else {
                    $insert->bindValue($paramName, $value);
                }
            }

            // 更新SQLをロギング。
            $this->loggingUpdating($sql, $sqlParams);

            //-----------------------------
            // INSERT文を発行。
            //-----------------------------
            $insert->execute();

            // 処理されたレコード件数を返す。
            $rc = $insert->rowCount();
            return $rc;

        }catch(Exception $e){
            //例外発生時はロギングしてスロー
            $this->exception_log($e);
            throw $e;
        }
    }

    /**
     * UPDATE文を発行する。
     *
     * @param string $tableName テーブル名
     * @param array[カラム名 => 値 or array[値,型]] $columns カラム情報
     * @param string $whereString WHERE句
     * @param array[カラム名 => 値 or array[値,型]] $whereParams WHERE条件パラメータ
     * 
     * @return integer レコード件数
     */
    public function update($tableName, $columns, $whereString, $whereParams) {
        try{
            // SQL文を生成。
            $sql  = "UPDATE ".$tableName." ";
            $sql .= "SET ";
            foreach($columns as $columnName => $value) {
                $sql .= $columnName." = :".$columnName.", ";
            }
            $sql .= "update_id = :loginUserRowId, ";
            $sql .= "update_date = now() ";
            if ($whereString != "") {
                $sql .= "WHERE " . $whereString;
            }

            // プリペアードステートメントを作成。
            $update = $this->con->prepare($sql);

            // パラメータをバインド。
            $sqlParams = array();
            foreach($columns as $columnName => $value) {
                $sqlParams[":".$columnName] = $value;
            }
            foreach($whereParams as $columnName => $value) {
                $columnName = str_replace(":","",$columnName);  //他のwhere系との同期用
                $sqlParams[":".$columnName] = $value;
            }
            $sqlParams[":loginUserRowId"] = $this->userRowId;

            foreach($sqlParams as $paramName => $value) {
                if (is_array($value)) {
                    $update->bindValue($paramName, $value[0], $value[1]);  // 値&型
                } else {
                    $update->bindValue($paramName, $value);
                }
            }

            // 更新前データをロギング。
            $this->loggingUpdateTarget($tableName, $whereString, $whereParams);
            // 更新SQLをロギング。
            $this->loggingUpdating($sql, $sqlParams);

            //-----------------------------
            // UPDATE文を発行。
            //-----------------------------
            $update->execute();

            // 処理されたレコード件数を返す。
            $rc = $update->rowCount();
            return $rc;
        }catch(Exception $e){
            //例外発生時はロギングしてスロー
            $this->exception_log($e);
            throw $e;
        }
    }

    /**
     * DELETE文を発行する。
     *
     * @param string $tableName テーブル名
     * @param string $whereString WHERE句
     * @param array[カラム名 => 値 or array[値,型]] $whereParams WHERE条件パラメータ
     * 
     * @return integer レコード件数
     */
    public function delete($tableName, $whereString, $whereParams) {
        try{
            // SQL文を生成。
            $sql  = "DELETE FROM ".$tableName." ";
            if ($whereString != "") {
                $sql .= "WHERE " . $whereString;
            }

            // プリペアードステートメントを作成。
            $delete = $this->con->prepare($sql);

            // パラメータをバインド。
            $sqlParams = array();
            foreach($whereParams as $columnName => $value) {
                $sqlParams[":".$columnName] = $value;
            }

            foreach($sqlParams as $paramName => $value) {
                if (is_array($value)) {
                    $delete->bindValue($paramName, $value[0], $value[1]);  // 値&型
                } else {
                    $delete->bindValue($paramName, $value);
                }
            }

            // 更新前データをロギング。
            $this->loggingUpdateTarget($tableName, $whereString, $whereParams);
            // 更新SQLをロギング。
            $this->loggingUpdating($sql, $sqlParams);

            //-----------------------------
            // DELETE文を発行。
            //-----------------------------
            $delete->execute();

            // 処理されたレコード件数を返す。
            $rc = $delete->rowCount();
            return $rc;
        }catch(Exception $e){
            //例外発生時はロギングしてスロー
            $this->exception_log($e);
            throw $e;
        }
    }

    /**
     * 更新SQLをロギングする。
     *
     * @param [type] $sql    SQL文
     * @param [type] $params パラメータ
     */
    private function loggingUpdating($sql, $params) {
        if (!LOGGING_UPDATE_SQL) return;

        $paramString = "";
        foreach($params as $name => $value) {
            $paramString .= "\n".$name."=".$value;
        }

        $log = $this->con->prepare(
             "INSERT INTO log_update_data( "
             .    "user_type, user_id, create_date, log_text "
             .") VALUES (  "
             .   ":user_type, :user_id, NOW(), :log_text "
             .") "
        );

        $log->bindValue(":user_type", $this->userType);
        $log->bindValue(":user_id",   $this->userRowId);
        $log->bindValue(":log_text",  "[SQL]\n".$sql."\n[PARAMS]".$paramString);

        $log->execute();
    }

    /**
     * 更新前データをロギングする。
     *
     * @param string $tableName テーブル名
     * @param string $whereString WHERE句
     * @param array[カラム名 => 値 or array[値,型]] $whereParams WHERE条件パラメータ
     */
    private function loggingUpdateTarget($tableName, $whereString, $whereParams) {
        if (!LOGGING_UPDATE_DATA) return;

        // 更新前データを取得する。
        $sql = "SELECT * "
             . "FROM ".$tableName;
        if ($whereString != "") {
            $sql .= " WHERE " . $whereString;
        }
        $stmt = $this->con->prepare($sql);
        
        foreach($whereParams as $paramName => $value) {
            if (is_array($value)) {
                $stmt->bindValue($paramName, $value[0], $value[1]);  // 値&型
            } else {
                $stmt->bindValue($paramName, $value);
            }
        }

        $stmt->execute();

        $dataString = "";
        $i = 0;
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
            $rowString = "";
            foreach ($row as $key => $value) {
                if ($rowString != "") $rowString .= ",";
                $rowString .= $key."=".$value;
            }
            $i++;
            $dataString .= "\n[".$i."] ".$rowString;
        }

        // 取得内容をログデータに出力する。
        $log = $this->con->prepare(
             "INSERT INTO log_update_data( "
            .    "user_type, user_id, create_date, log_text "
            .") VALUES (  "
            .   ":user_type, :user_id, NOW(), :log_text "
            .") "
        );

        $log->bindValue(":user_type", $this->userType);
        $log->bindValue(":user_id",   $this->userRowId);
        $log->bindValue(":log_text",  "[DATAS]".$dataString);

        $log->execute();
    }


    /**
     * 指定したテーブルのIDカラムにおける使用可能なID(最大値+1の値)を返す。<br>
     * ※トランザクションの中で使用することを想定。
     *
     * @param string  $tableName     テーブル名
     * @param string  $idColumnName　カラム名
     * @param integer $digits        番号部分の桁数
     * @param string  $whereSql      ID抽出の条件(SQL文・where ～の部分)
     * @param array[パラメータ名 => 値] $whereParams ID抽出の条件(パラメータ)
     * 
     * @return string 使用可能なID
     */
    public function getNewId($tableName, $idColumnName, $digits, $whereSql = null, $whereParams = null) {
        // IDの最大値を取得。
        $sql = "SELECT MAX(".$idColumnName.") AS maxId "
             . "FROM ".$tableName." ";
        if ($whereSql != null) {
            $sql .= "WHERE " . $whereSql;
        }
        if ($whereParams != null) {
            $params = $whereParams;
        } else {
            $params = array();
        }
        $sql .= " FOR UPDATE ";
        $result = $this->selectOne($sql, $params);

        // 取得内容から接頭部分と数値部分を取得。
        $prefix = "";
        $idVal = 0;
        if (isset($result) && count($result) > 0) {
            // "XXXX999999"の形式の場合、前後をそれぞれ切り出し。
            // ex)"PRE0012"の場合、「"PRE",12」に切り出す。
            preg_match("/^(.*?)([0-9]*)$/u", $result["maxId"], $matchies);
            $prefix = $matchies[1];
            if (count($matchies) > 2) {
                $idVal = intval($matchies[2]);
            }
        }

        // 数値部分をインクリメント。
        $idVal++;

        // 数値部分にゼロ埋めを施す。
        $idStr = str_pad($idVal, $digits, "0", STR_PAD_LEFT);

        // 接頭部分と数値部分を連結した内容を、「使用可能なID」として返す。
        return $prefix.$idStr;
    }


    /**
     * 管理者の行IDを取得する。
     *
     * @param string $adminId 取得対象の管理者ID
     * 
     * @return integer 行ID
     */
    public function getAdminRowId($adminId) {
        $sql = "SELECT id AS rowid "
             . "FROM mst_admin_user "
             . "WHERE user_id = :admin_id "
        ;
        $params[":admin_id"] = $adminId;
        
        $result = $this->selectOne($sql, $params);

        if ($result != null) {
            return $result["rowid"];
        } else {
            return null;
        }
    }

    
    /**
     * 監督者の行IDを取得する。
     *
     * @param string $adminId 取得対象の管理者ID
     * 
     * @return integer 行ID
     */
    public function getSupervisorRowId($adminId) {
        $sql = "SELECT id AS rowid "
             . "FROM tbl_supervisor_info "
             . "WHERE supervisor_id = :admin_id "
        ;
        $params[":admin_id"] = $adminId;
        
        $result = $this->selectOne($sql, $params);

        if ($result != null) {
            return $result["rowid"];
        } else {
            return null;
        }
    }

    /**
     * 労働者の行IDを取得する。
     *
     * @param string $workerId 取得対象の労働者ID
     * 
     * @return integer 行ID
     */
    public function getWorkerRowId($workerId) {
        $sql = "SELECT id AS rowid "
             . "FROM mst_workers "
             . "WHERE worker_id = :worker_id "
        ;
        $params[":worker_id"] = $workerId;
        
        $result = $this->selectOne($sql, $params);

        if ($result != null) {
            return $result["rowid"];
        } else {
            return null;
        }
    }

    /**
     * 企業の行IDを取得する。
     *
     * @param string $companyId 取得対象の企業ID
     * 
     * @return integer 行ID
     */
    function getCompanyRowId($companyId) {
        $sql = "SELECT id AS rowid "
             . "FROM mst_company "
             . "WHERE company_id = :company_id "
        ;
        $params[":company_id"] = $companyId;
        
        $result = $this->selectOne($sql, $params);

        if ($result != null) {
            return $result["rowid"];
        } else {
            return 0;
        }
    }

}
