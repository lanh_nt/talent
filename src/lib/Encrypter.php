<?php
/**
 * 暗号化処理クラス
 * 
 */
class Encrypter
{
    // 暗号化アルゴリズム
    const ENCRYPT_ALGORITHM = 'AES-128-CBC'; // AES 128bit CBCモード

    // 暗号化キー
    const ENCRYPT_KEY = "pj.aisa-tnelat pj.aisa-tnelat pj.aisa-tnelat";


    /**
     * 指定した文字列を暗号化する。
     *
     * @param string $src 対象文字列
     * 
     * @return string 暗号化した文字列
     */
    public function encrypt($src) {
        // 初期化ベクトルを生成。
        $iv_size = openssl_cipher_iv_length(self::ENCRYPT_ALGORITHM); // 初期化ベクトルの長さを取得
        $iv = $this->getRandomString($iv_size); // ランダムな文字列で初期化ベクトルを作成

        // 暗号化。
        $dest = openssl_encrypt(
            $src, 
            self::ENCRYPT_ALGORITHM, 
            self::ENCRYPT_KEY, 
            OPENSSL_RAW_DATA, 
            $iv
        );
        
        // 内容を文字列にエンコード。
        $destString = base64_encode($dest);

        // 暗号化した内容を返す。
        return $iv.$destString;
    }

    /**
     * 暗号化された文字列を復号する。
     *
     * @param string $src 暗号化された文字列
     * 
     * @return string 復号した文字列
     */
    public function decrypt($src) {
        // 暗号データと初期化ベクトルを分ける
        $iv_size   = openssl_cipher_iv_length(self::ENCRYPT_ALGORITHM); // 初期化ベクトルの長さを取得
        $iv        = substr($src, 0, $iv_size);
        $encString = substr($src, $iv_size);
        $encString = str_replace(' ', '+', $encString);

        // 暗号データをバイナリに戻す。
        $encData = base64_decode($encString);

        // 復号
        $data = openssl_decrypt(
            $encData, 
            self::ENCRYPT_ALGORITHM, 
            self::ENCRYPT_KEY, 
            OPENSSL_RAW_DATA, 
            $iv
        );

        // 復化した内容を返す。
        return $data;
    }


    /**
     * ランダムな文字列を生成して返す。
     *
     * @param int $length 文字数
     * 
     * @return 生成した文字列
     */
    private function getRandomString($length){
        $chars = implode('', array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9')));
        $str = '';
        for ($i = 0; $i < $length; ++$i) {
            $str .= $chars[mt_rand(0, 61)];
        }
        return $str;
    }

}
