﻿<?php
/**
 * S3ファイルアクセス
 * 
 */
use Aws\S3\S3Client;
class S3_FileAccess{
 
    /** S3コネクション */
    private $s3;


    /**
     * コンストラクタ
     */
    public function __construct() {
        if(PDF_SAVE_FILE_AWS){
            // コネクションを生成。
            $this->s3 = new S3Client([
                'version' => 'latest',
                'region'  => 'ap-northeast-1',
            ]);
        }
    }

    /**
     * S3にPDFファイルを保存する
     *
     * @param stream  $file    ファイルストリーム
     * @param string  $workerId  労働者ID
     * @param string  $fname     ファイル名
     * 
     * @return 結果
     */
    public function PDFFileUpload($file, $workerId, $fname) {
        $result = "";

        if(PDF_SAVE_FILE_AWS) {
            try {
                $filePath = PDF_SAVE_FILE_PATH_A.$workerId."/".$fname;

                $result=$this->s3->putObject([
                    'Bucket' => S3_BUCKET,
                    'Key'    => $filePath,
                    'Body'   => $file,
                ]);

                return $result['effectiveUri'];

            } catch (Exception $e) {
                //echo "failed to upload:" . PHP_EOL . $e->getMessage() . PHP_EOL;
                $result = $e->getMessage();
            }
        } else {
            $this->checkDir(PDF_SAVE_FILE_PATH_W, $workerId);
            $filePath = PDF_SAVE_FILE_PATH_W.$workerId."/".$fname;
            file_put_contents($filePath, $file);
            $result = $filePath;
        }

        return $result;
    }

    /**
     * S3にファイルを保存する
     *
     * @param stream  $file      ファイルストリーム
     * @param string  $workerId  労働者ID
     * @param string  $fname     ファイル名
     * 
     * @return 結果
     */
    public function fileUpload($file, $workerId, $fname) {
        $result = "";

        if (PDF_SAVE_FILE_AWS) {
            try {
                $filePath = FILEMNG_SAVE_FILE_PATH_A.$workerId."/".$fname;

                $result = $this->s3->putObject([
                    'Bucket' => S3_BUCKET,
                    'Key'    => $filePath,
                    'Body'   => $file,
                ]);

                return $result['effectiveUri'];

            } catch (Exception $e) {
                $result = $e->getMessage();
            }
        } else {
            $this->checkDir(FILEMNG_SAVE_FILE_PATH_W, $workerId);
            $filePath = FILEMNG_SAVE_FILE_PATH_W.$workerId."/".$fname;
            file_put_contents($filePath, $file);
            $result = $filePath;
        }

        return $result;
    }

    /**
     * S3からファイルを取得する
     *
     * @param string  $fname    ファイル名
     * @param string  $dirname  フォルダ名
     * 
     * @return $file    ファイルストリーム
     */
    public function FileDownload($fname, $dirname) {
        $result = "";

        if(PDF_SAVE_FILE_AWS){
            try{
                $result = $this->s3->getObject([
                    'Bucket' => S3_BUCKET,
                    'Key'    => $dirname . $fname,
                ]);


                //----------------------------------------------------
                //取得語のファイルデータを使った例　参考：https://gootablog.com/s3-php#S3-2
                //----------------------------------------------------
                // $len = $result['ContentLength'];  //サイズ取得

                // //ファイルを表示
                // header("Content-Type: {$result['ContentType']}");
                // echo $result['Body'];

                // //ファイルダウンロード
                // header('Content-Type: application/force-download;');
                // header('Content-Length: '.$len);
                // header('Content-Disposition: attachment; filename="sample.jpg"');
                // echo $result['Body'];
                //----------------------------------------------------
                    
                return $result['Body'];

            } catch (Exception $e) {
                //echo "failed to upload:" . PHP_EOL . $e->getMessage() . PHP_EOL;
            }
        } else {
            $result = "";
        }

        return $result;
    }

    /**
     * ファイルの内容を出力する。
     *
     * @param string $type        動作タイプ(1:ファイル管理, 2:提出済みPDF)
     * @param string $workerId    労働者ID
     * @param string $fileName    ストレージ格納
     * @param string $orgFileName 元ファイルの名前
     * 
     * @return void (http-responseとしてファイルコンテンツを出力する。)
     */
    public function outputFile($type, $workerId, $fileName, $orgFileName) {
        // ベースディレクトリを設定。
        if (PDF_SAVE_FILE_AWS) {
            if ($type == 1) {
                $path = FILEMNG_SAVE_FILE_PATH_A;
            } else {
                $path = PDF_SAVE_FILE_PATH_A;
            }
        } else {
            if ($type == 1) {
                $path = FILEMNG_SAVE_FILE_PATH_W;
            } else {
                $path = PDF_SAVE_FILE_PATH_W;
            }
        }
        // パスにユーザID(メールアドレス)を追加。
        $path .= $workerId."/";
        // パスにファイルIDを追加。
        $path .= $fileName;

        // ファイルの内容を取得する。
        if (PDF_SAVE_FILE_AWS) {
            // S3から取得。
            try {
                $result = $this->s3->getObject([
                    'Bucket' => S3_BUCKET,
                    'Key'    => $path,
                ]);
                $contents = $result['Body'];
                $len      = $result['ContentLength'];
            } catch (Exception $e) {
                //echo "failed to upload:" . PHP_EOL . $e->getMessage() . PHP_EOL;
            }
        } else {
            // ファイルサイズを取得。
            // ※ファイル読み込みは、出力と同時にあとで行う。
            $len = filesize($path);
        }

        // ヘッダを出力。
        $headerInfo = getHeaderAttributes($orgFileName);
        header("Content-Type: ".$headerInfo["mimeType"]);
        if ($headerInfo["isInline"]) {
            header("Content-Disposition: inline;");
        } else {
            header("Content-Disposition: attachment; filename*=UTF-8''".rawurlencode($orgFileName));
        }
        header('Content-Length: '.$len);

        // 取得したコンテンツを出力する。
        if (PDF_SAVE_FILE_AWS) {
            // S3から取得したコンテンツを出力。
            echo $result['Body'];
        } else {
            // ファイルを読み込んで、そのまま出力する。
            readfile($path);
        }
    }

    /**
     * S3からファイルを削除する
     *
     * @param string $type        動作タイプ(1:ファイル管理, 2:提出済みPDF)
     * @param string $workerId    労働者ID
     * @param string $fileName    ストレージ格納
     * 
     * @return 結果
     */
    public function deleteFile($type, $workerId, $fileName) {
        $result = false;

        // ベースディレクトリを設定。
        if (PDF_SAVE_FILE_AWS) {
            if ($type == 1) {
                $path = FILEMNG_SAVE_FILE_PATH_A;
            } else {
                $path = PDF_SAVE_FILE_PATH_A;
            }
        } else {
            if ($type == 1) {
                $path = FILEMNG_SAVE_FILE_PATH_W;
            } else {
                $path = PDF_SAVE_FILE_PATH_W;
            }
        }
        // パスにユーザID(メールアドレス)を追加。
        $path .= $workerId."/";
        // パスにファイルIDを追加。
        $path .= $fileName;

        if(PDF_SAVE_FILE_AWS) {
            try{
                $result = $this->s3->deleteObject([
                    'Bucket' => S3_BUCKET,
                    'Key'    => $path,
                ]);
                    
                return $result['effectiveUri'];

            } catch (Exception $e) {
                //echo "failed to upload:" . PHP_EOL . $e->getMessage() . PHP_EOL;
            }
        } else {
            unlink($path);
            $result = $path;
        }

        return $result;
    }


    /**
     * ディレクトリの存在をチェックする。<br>
     * 存在しなければ作成する。
     *
     * @param string $baseDir  ベースディレクトリ
     * @param string $workerId 労働者ID
     * 
     * @return boolean チェック結果
     */
    private function checkDir($baseDir, $workerId) {
        if (!file_exists($baseDir.$workerId)) {
            mkdir($baseDir.$workerId);
        }
        return true;
    }

}
