<?php
/**
 * 共通処理定義
 * 
 */

/**
 * ログイン済みかどうかをチェックする。
 *
 * @return boolean チェック結果
 */
function checkLogin() {
    checkMaintenance();

    // 必要なセッション情報が設定されていない場合は、エラー画面に飛ばす。
    if(!(isset($_SESSION['loginUserId']) && isset($_SESSION['loginUserType']) && isset($_SESSION['loginUserLang']))) {
        $_SESSION['err'] = getCommonMessage("EC001");
        header('Location:../TP01/ER01001');
        exit();
    }

    // すべてのチェックを通過できた場合、チェック完了を返す。
    return true;
}

/**
 * メンテナンス中かどうかをチェックする。
 *
 * @return なし
 */
function checkMaintenance() {
    //システムルートのmaintenanceフォルダにMTファイルが存在するかチェック
    $filename = '../../maintenance/MT';
    if (file_exists($filename)) {
        header('Location:../TP01/MAINTENANCE');
        exit();
    }
}

/**
 * メンテナンス中かどうかをチェックする。
 *
 * @return なし
 */
function checkMaintenanceNologin() {
    //システムルートのmaintenanceフォルダにMTファイルが存在するかチェック
    $filename = '../maintenance/MT';
    if (file_exists($filename)) {
        header('Location:./TP01/MAINTENANCE');
        exit();
    }
}

/**
 * トークンを更新する。
 *
 * @return string 新規生成したトークン
 */
function updateToken() {
    // 新しいトークンを生成。
    $tokenValue = openssl_random_pseudo_bytes(16);
    $tokenString = bin2hex($tokenValue);

    // 生成したトークンをセッションに保存する。
    $_SESSION['csrf_token'] = $tokenString;

    // 生成したトークンを返す。
    return $tokenString;
}

/**
 * トークンをチェックする。
 * 
 * @param string $token チェック対象のトークン値
 * @param string $errorCode エラー時に表示するエラーコード
 * 
 * @return boolean チェック結果
 *                 <br>(※実際は、エラーの場合はエラー画面に遷移するため、正常(true)のみが返る。)
 */
function checkToken($token = null, $errorCode = "EC003") {
    if ($token == null) {
        $token = $_POST['csrf_token'];
    }
    // トークンを照合する。
    if ($token != $_SESSION['csrf_token']) {
        // 内容が合致していない場合は、エラー画面に遷移。
        $errMessage = getCommonMessage($errorCode);
        goErrorPage($errMessage);
    }

    // チェックを通過できた場合、チェック完了を返す。
    return true;
}


/**
 * 指定ページにフォワードする。
 * 
 * <p>
 * ※注意点<br>
 * 実際には forward ではなく、 redirect である。<br>
 * 自動遷移のための空ページを表示し、ページロード処理にて指定画面への submit を行う。
 * </p>
 *
 * @param string $nextpage　次ページのパス
 * @param array[項目名 => 値] $items POSTしたい項目一覧
 * @param boolean $isRoot 現在のパスがルート直下かどうか
 */
function goForwardPage($nextpage, $items, $isRoot = false) {
    // Smartyテンプレートエンジンを生成。
    $smarty = new Smarty();
    if ($isRoot) {
        $smarty->template_dir = '../templates/';
        $smarty->compile_dir  = '../templates_c/';
        $smarty->config_dir   = '../configs/';
        $smarty->cache_dir    = '../cache/';
    } else {
        $smarty->template_dir = '../../templates/';
        $smarty->compile_dir  = '../../templates_c/';
        $smarty->config_dir   = '../../configs/';
        $smarty->cache_dir    = '../../cache/';
    }

    // Smartyに変数をバインド。
    $smarty->assign("nextpage", $nextpage);
    $smarty->assign("items", $items);
    $smarty->assign("isRoot", $isRoot);

    // フォワード用画面を表示する。
    $smarty->display('forward.tpl');

    // ページ処理を終了。
    exit();
}

/**
 * エラーページに遷移する。
 *
 * @param string $errMessage
 */
function goErrorPage($errMessage) {
    // セッションにエラー内容を設定。
    $_SESSION['err'] = $errMessage;

    // エラーページにリダイレクト。
    header('Location:../TP01/ER01001');

    // ページ処理を終了。
    exit();
}


/**
 * CSVフォーマットファイルをダウンロードする。
 *
 * @param string $formatFileName フォーマットファイルの名前
 * @param string $outputName     ダウンロード時のファイル名
 */
function formatDownload($formatFileName, $outputName) {
    header("Content-Type: application/vnd.ms-excel.sheet.macroEnabled.12");
    header("Content-Disposition: attachment; filename*=UTF-8''".rawurlencode($outputName.'.xlsm'));
    readfile("../../csvformat/".$formatFileName.".xlsm");
    exit();
}


/**
 * 入力元の配列から、新たな配列を生成して返す。<br>
 * 入力元から必要項目を抽出・存在しない項目を追加した配列を生成して返す。
 *
 * @param array[項目名]       $sourceArray 入力元の配列
 * @param array[項目名 => 値] $params      配列に設定したいキー項目
 * 
 * @return array[項目名 => 値] 生成した配列
 */
function getParamsArray($sourceArray, $params) {
    // 配列を新規生成。
    $paramsArray = array();

    // 指定したキー項目を追加。
    foreach($params as $param) {
        $paramsArray[$param] = is_array($sourceArray) && isset($sourceArray[$param])
                             ? $sourceArray[$param] 
                             : "";
    }

    // 生成した配列を返す。
    return $paramsArray;
}

/**
 * ラベル情報を返す。
 *
 * @param string $functionId 機能ID
 * @param string $sceneId    画面ID
 * @param string $lang       言語
 * @param boolean $isGetCommonItems 共通ラベル項目を追加取得するか
 * 
 * @return array[キー => 値] ラベル情報
 */
function getLabels($functionId, $sceneId, $lang = null, $isGetCommonItems = true) {
    $db = new JinzaiDb(DB_DEFINE);

    // 取得する言語に対応するカラムを設定。
    if ($lang == null) $lang = getLangValue();
    if ($lang == '2') {
        $langColumn = 'lang2';
    } else if ($lang == '3') {
        $langColumn = 'lang3';
    } else {
        $langColumn = 'lang1';
    }

    // 問合せを発行。
    $sql = "SELECT label_id, ".$langColumn." AS value "
         . "FROM mst_gui_label "
         . "WHERE (function_id = :function_id AND scene_id = :scene_id) ";
    if ($isGetCommonItems) {
        $sql .= "OR (function_id = 'comm' AND scene_id = 'common') ";
    }
    $params = array(
        ":function_id" => $functionId,
        ":scene_id"    => $sceneId
    );
    $result = $db->select($sql, $params);

    // 問合せ結果を、返却用に再構成。
    $labels = array();
    foreach ($result as $row) {
        $labels[$row["label_id"]] = $row["value"];
    }

    // ラベル情報を返す。
    return $labels;
}

/**
 * 選択項目ラベル情報を返す。
 *
 * @param string $functionId 機能ID
 * @param string $sceneId    項目ID(画面ID)
 * @param string $lang       言語
 * 
 * @return array[キー => 値] ラベル情報
 */
function getOptionItems($functionId, $sceneId, $lang = null) {
    return getLabels($functionId, $sceneId, $lang, false);
}

/**
 * 選択項目に対応する名称を取得する。
 *
 * @param array[キー => 名称] $options 選択項目一覧
 * @param string $key キー値
 * 
 * @return string 名称
 */
function getOprionItemValue($options, $key) {
    $value = "";
    if ($key != null and $key != "") {
        if ($options != null and isset($options[$key])) {
            $value = $options[$key];
        }
    }
    return $value;
}


/**
 * メッセージ文言を返す。
 *
 * @param string $functionId 機能ID
 * @param string $sceneId    画面ID
 * @param string $messageId  メッセージID
 * @param mixed  $addingStrings 追加パラメータ
 * 
 * @return string メッセージ文言
 */
function getMessage($functionId, $sceneId, $messageId, ...$addingStrings) {
    $db = new JinzaiDb(DB_DEFINE);

    // 取得する言語に対応するカラムを設定。
    $lang = getLangValue();
    if ($lang == '2') {
        $langColumn = 'lang2';
    } else if ($lang == '3') {
        $langColumn = 'lang3';
    } else {
        $langColumn = 'lang1';
    }

    // 問合せを発行。
    $sql = "SELECT ".$langColumn." AS value "
         . "FROM mst_gui_label "
         . "WHERE function_id = :function_id "
         .   "AND scene_id = :scene_id "
         .   "AND label_id = :messageId ";
    $params = array(
        ":function_id" => $functionId,
        ":scene_id"    => $sceneId,
        ":messageId"   => $messageId
    );
    $result = $db->selectOne($sql, $params);

    // 取得した文言を返す。
    if ($result != null) {
        $message = $result["value"];
    } else {
        $message = $messageId;
    }
    if (count($addingStrings) > 0) {
        $message = sprintf($message, ...$addingStrings);
    }
    return $message;
}

/**
 * 共通メッセージ文言を返す。
 *
 * @param string $messageId  メッセージID
 * 
 * @return string メッセージ文言
 */
function getCommonMessage($messageId, ...$addingStrings) {
    return getMessage("comm", "message", $messageId, ...$addingStrings);
}

/**
 * 言語の設定値を返す。
 *
 * @return string 言語の設定値
 */
function getLangValue() {
    if (isset($_SESSION['loginUserLang'])) {
        // セッションに言語が設定されている場合、その値を返す。
        return $_SESSION['loginUserLang'];
    } else {
        // 設定がない場合、HTTPヘッダの言語情報から値を返す。
        $langs = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
        $langVal = 1;
        foreach ($langs as $lang) {
            if (preg_match('/^ja.*/', $lang)) {
                $langVal = 1;
                break;
            }
            if (preg_match('/^en.*/', $lang)) {
                $langVal = 2;
                break;
            }
            if (preg_match('/^vi.*/', $lang)) {
                $langVal = 3;
                break;
            }
        }
        return $langVal;
    }
}


/**
 * 配列の内容を連結して返す。
 * 
 * @param array[string]  $arr 連結対象の配列
 * @param string $delimitor 区切り文字
 * 
 * @return string 連結した文字列
 */
function joinArray($arr, $delimitor = ", ") {
    $buf = "";
    foreach ($arr as $item) {
        if ($buf != "") {
            $buf .= $delimitor;
        }
        $buf .= $item;
    }
    return $buf;
}

/**
 * 2次元配列の行/列を入れ替える。<br>
 * DB取得結果をCSV出力用に転置する用途を想定している。
 *
 * @param array[行][項目名 => 値] $srcArray 変換元データ(SELECT文の結果セットを想定。)
 * 
 * @return array[項目名][値] 処理結果
 */
function transposeMatrix($srcArray) {
    $transArray = array();
    $rowid = 0;
    foreach ($srcArray as $row) {
        foreach ($row as $key => $val) {
            $transArray[$key][$rowid] = $val;
        }
        $rowid++;
    }
    return $transArray;
}

/**
 * 文字列を1文字ずつ配列に分割して返す。
 *
 * @param [type] $source   対象の文字列
 * @param [type] $len      配列長さ
 * @param string $charcode 文字コード
 * 
 * @return array 分割後の配列
 */
function arraySplit($source, $len, $charcode = "UTF-8") {
    $result = array();

    // 文字列を配列に分割する。
    for ($i = 0; $i < $len; $i++) {
        $result[] = mb_substr($source, $i, 1, $charcode);
    }

    // まだ文字が余っている場合は、最後に追記。
    if (mb_strlen($source, $charcode) > $len) {
        $result[$len - 1] .= mb_substr($source, $i, null, $charcode);
    }

    // 処理結果を返す。
    return $result;
}


/**
 * レコードの内容が空かどうか判定する。<br>
 * 調査対象の配列について、指定インデックス以降の内容がすべて空であるかどうかを調査して返す。
 *
 * @param array   $row            調査対象の配列
 * @param integer $targetRowIndex 調査対象のインデックス
 * 
 * @return boolean 判定結果
 */
function isEmptyRecord($row, $targetRowIndex) {
    for ($i = $targetRowIndex; $i < count($row); $i++) {
        if ($row[$i] != "") {
            // 空でない項目が存在する場合は、「空でない」を返して終了。
            return false;
        }
    }

    // 「内容がすべて空である」を返す。
    return true;
}


/**
 * 日付項目のパラメータ設定値を返す。
 * 
 * <p>追加文字列について<br>
 * 追加文字列は、データが年月の場合に日付を補う用途を想定している。<br>
 * 例)日付文字列="2020/04",追加文字列="/01" → 返却文字列="""2020/04/01"
 * </p>
 *
 * @param string $dateString   日付文字列
 * @param string $addingString 追加文字列
 * 
 * @return string パラメータ設定値
 */
function setDateParam($dateString, $addingString = "") {
    // 値が未設定なら null を返す。
    if ($dateString == null or $dateString == "") {
        return null;
    }

    // 渡された値を返す。
    return $dateString.$addingString;
}


/**
 * 入力内容が英数大文字のみで構成されているかどうかをチェックする。
 *
 * @param array $str チェック対象文字列
 * 
 * @return boolean チェック結果
 */
function validateAlphabetCharacters($str) {
    return (preg_match('/^[ A-Z]+$/', $str));
}

/**
 * 入力内容がメールアドレスのフォーマットで構成されているかどうかをチェックする。
 *
 * @param array $str チェック対象文字列
 * 
 * @return boolean チェック結果
 */
function validateMailAddress($str) {
    // input type=email のバリデーションルールに合致するかどうかを返す。
    return (preg_match("/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/", $str));
}

/**
 * 入力内容に禁止文字が存在しないかどうかをチェックする。
 *
 * @param array $items チェック対象の項目一覧
 * @param array $exceptItemNames チェックを除外する項目名の一覧
 * 
 * @return boolean チェック結果(true:NG文字が含まれていない、false:NG文字が含まれている)
 */
function validateProhibitedCharacters($items, ...$exceptItemNames) {
    // 項目一覧を走査。
    foreach ($items as $itemName => $itemVal) {
        // 除外項目ならスキップ。
        if (in_array($itemName, $exceptItemNames)) continue;

        // 禁止文字の存在をチェックする。
        if (!validateProhibitedCharacter($itemVal)) {
            // 禁止文字が含まれていたらエラー。
            return false;
        }
    }

    // チェックOKを返す。
    return true;
}

/**
 * 入力内容に禁止文字が存在しないかどうかをチェックする。
 *
 * @param array $str チェック対象文字列
 * 
 * @return boolean チェック結果(true:NG文字が含まれていない、false:NG文字が含まれている)
 */
function validateProhibitedCharacter($str) {
    // 対象が配列だった場合は、内容を再帰的に判定。
    if (is_array($str)) {
        foreach ($str as $stritem) {
            if (!validateProhibitedCharacter($stritem)) {
                return false;
            }
        }
        return true;
    }

    // 文字単位にチェックする。
    for ($i = 0; $i < mb_strlen($str, "UTF-8"); $i++) {
        $c = mb_substr($str, $i, 1, 'UTF-8');
        $n = mb_ord($c, 'UTF-8');

        // ラテン文字拡張はNG。
        if (0x0080 <= $n and $n <= 0x024F) return false;
        if (0x1E00 <= $n and $n <= 0x1FFF) return false;
    }

    // チェックOKを返す。
    return true;
}


/**
 * ファイル名に対応するHTTPヘッダ情報を返す。
 *
 * @param string $filename ファイル名
 * @param boolean $isGetDefaultValue デフォルト値を受け取るかどうか
 * 
 * @return array HTTPヘッダ情報
 */
function getHeaderAttributes($filename, $isGetDefaultValue = true) {
    $mimeTypes = array(
        ".pdf"  => ["isInline" => true,  "mimeType" => "application/pdf"],           // PDF
        ".jpg"  => ["isInline" => true,  "mimeType" => "image/jpeg"],                // JPEG
        ".jpeg" => ["isInline" => true,  "mimeType" => "image/jpeg"],                // JPEG
        ".png"  => ["isInline" => true,  "mimeType" => "image/png"],                 // PNG
        ".xls"  => ["isInline" => false, "mimeType" => "application/vnd.ms-excel"],  // Excel
        ".xlsx" => ["isInline" => false, "mimeType" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"],  // Excel
        ".doc"  => ["isInline" => false, "mimeType" => "application/msword"],        // Word
        ".docx" => ["isInline" => false, "mimeType" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document"]  // Word
    );

    $result = $isGetDefaultValue ? array("isInline" => false, "mimeType" => "application/octet-stream") : null;

    // 拡張子がない場合は「対応なし」を返す。
    if (!preg_match("/\.[A-Za-z]+$/u", $filename, $m)) {
        return $result;
    }

    // システムで対応しない拡張子の場合は「対応なし」を返す。
    $ext = strtolower($m[0]);
    if (!isset($mimeTypes[$ext])) {
        return $result;
    }

    // 対応する MIME-TYPE を返す。
    $result = $mimeTypes[$ext];
    return $result;
}


/**
 * ランダムな文字列を返す。
 * 
 * @param integer $len 生成する文字数
 *
 * @return 生成した文字列
 */
function getRandomString($len = 8) {
    $passValue = openssl_random_pseudo_bytes($len);
    $passString = base64_encode($passValue);
    $passString = preg_replace("/[+_\/]/", "", $passString);
    $passString = substr($passString, 0, $len);

    return $passString;
}
