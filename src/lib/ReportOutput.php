<?php

/**
 * PDF帳票出力クラス
 * 
 */
class ReportOutput
{
    // PDF出力オブジェクト
    private $pdf;

    // 帳票項目定義
    private $def;

    // フォント
    private $pdfFont;


    /**
     * コンストラクタ
     *
     * @param string $reportId 帳票ID
     */
    function __construct($reportId) {
        $this->reportId = $reportId;

        // 定義ファイルを読み込む。
        $deffile = file_get_contents('../../report/'.$this->reportId.'.json');
        $this->def = json_decode($deffile);
        // 項目定義情報を、ページ単位に再構成する。
        $pageItems = array();
        for($pageCount = 1; $pageCount <= $this->def->page; $pageCount++) {
            $pageItems[$pageCount] = array();
        }
        foreach($this->def->items as $item) {
            $pageItems[$item->page][] = $item;
        }
        $this->def->pageItems = $pageItems;

        // PDFライブラリのインスタンスを生成。
        $this->pdf = new \setasign\Fpdi\TcpdfFpdi(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // 帳票タイトルを設定。
        $this->pdf->setTitle($this->def->name);

        // 用紙の向きを設定。
        $this->pdf->setPageOrientation($this->def->orient);

        // ヘッダ/フッタを出力しない。
        $this->pdf->setPrintHeader(false);
        $this->pdf->setPrintFooter(false);
        
        // 自動改ページをオフ
        $this->pdf->SetAutoPageBreak(false);

        // フォント情報を生成する。
        $font = new TCPDF_FONTS(); 
        $this->pdfFont = $font->addTTFfont('../../fonts/ipaexm.ttf');

        // テンプレートを読み込む。
        $this->pdf->setSourceFile('../../report/'.$this->reportId.'.pdf');
    }


    /**
     * PDFを出力する。
     *
     * @param mixed $datas 出力データ
     * @param boolean $isDirectOutput PDFの出力方法(true:HTTP-responseとして出力、false:PDFデータを戻り値として返却)
     * 
     * @return なし or PDFデータ
     */
    public function output($datas, $isDirectOutput = true) {
        for($pageCount = 1; $pageCount <= $this->def->page; $pageCount++) {
            // 新規ページを生成。
            $this->pdf->AddPage();
            $this->pdf->useTemplate($this->pdf->importPage($pageCount));

            // ページ内の定義項目を走査。
            foreach($this->def->pageItems[$pageCount] as $itemDef) {
                // 対応するデータが画面から渡されていない場合はスキップ。
                if (!isset($datas[$itemDef->id])) {
                    continue;
                }

                // 項目を出力する。
                switch ($itemDef->type) {
                    case 'text':
                        $this->putText($itemDef, $datas[$itemDef->id]);
                        break;
                    case 'cell':
                        $this->putCell($itemDef, $datas[$itemDef->id]);
                        break;
                    case 'circle':
                        $this->putCircle($itemDef);
                        break;
                    case 'check':
                        $this->putCheck($itemDef);
                        break;
                }
            }
        }

        // PDFを出力する。
        if ($isDirectOutput) {
            date_default_timezone_set('Asia/Tokyo');
            $fileName = $this->reportId."_".date("Ymd_Hi").".pdf";
            return $this->pdf->Output($fileName);
        } else {
            return $this->pdf->Output($this->reportId, "S");  // S : PDFドキュメントの内容を文字列として出力する。
        }
    }

    /**
     * PDFを出力する。(帳票版)
     *
     * @param mixed $pageDatas 出力データ 
     * @param boolean $isDirectOutput PDFの出力方法(true:HTTP-responseとして出力、false:PDFデータを戻り値として返却)
     * 
     * @return なし or PDFデータ
     */
    public function outputMultiPage($pageDatas, $isDirectOutput = true) {
        // ページデータを走査。
        foreach($pageDatas as $datas) {
            $currentPageNo = $datas["_templatePageNo"];

            // 新規ページを生成。
            $this->pdf->AddPage();
            $this->pdf->useTemplate($this->pdf->importPage($currentPageNo));

            // ページ内の定義項目を走査。
            foreach($this->def->pageItems[$currentPageNo] as $itemDef) {
                // 対応するデータが画面から渡されていない場合はスキップ。
                if (!isset($datas[$itemDef->id])) {
                    continue;
                }

                // 項目を出力する。
                switch ($itemDef->type) {
                    case 'text':
                        $this->putText($itemDef, $datas[$itemDef->id]);
                        break;
                    case 'cell':
                        $this->putCell($itemDef, $datas[$itemDef->id]);
                        break;
                    case 'circle':
                        $this->putCircle($itemDef);
                        break;
                    case 'check':
                        $this->putCheck($itemDef);
                        break;
                }
            }
        }

        // PDFを出力する。
        if ($isDirectOutput) {
            return $this->pdf->Output($this->reportId);
        } else {
            return $this->pdf->Output($this->reportId, "S");  // S : PDFドキュメントの内容を文字列として出力する。
        }
    }


    /**
     * テキスト項目を出力する。
     *
     * @param [type] $itemDef 項目定義情報
     * @param [type] $value   値
     * 
     * @return void
     */
    private function putText($itemDef, $value) {
        // フォントを設定。
        if (array_key_exists('fontSize', $itemDef)) {
            $fontSize = $itemDef->fontSize;
        } else {
            $fontSize = 10.5;
        }
        $this->pdf->SetFont($this->pdfFont, '', $fontSize);

        $this->pdf->SetXY($itemDef->x, $itemDef->y);
        $this->pdf->Cell(0, 0, $value);
    }

    /**
     * セル項目を出力する。
     *
     * @param [type] $itemDef 項目定義情報
     * @param [type] $value   値
     * 
     * @return void
     */
    private function putCell($itemDef, $value) {
        // フォントを設定。
        if (array_key_exists('fontSize', $itemDef)) {
            $fontSize = $itemDef->fontSize;
        } else {
            $fontSize = 10.5;
        }
        $this->pdf->SetFont($this->pdfFont, '', $fontSize);

        $this->pdf->MultiCell(
            $itemDef->w,    // 幅
            $itemDef->h,    // 高さ
            $value,         // 文言
            0,              // 境界線 : なし
            'L',            // 文字配置(横) : 左揃え
            0,              // 塗りつぶし : なし
            1,              // 出力後のカーソル位置 : 次行
            $itemDef->x,    // X座標
            $itemDef->y,    // Y座標
            true,           // 前回のセルの高さ設定をリセット : する
            0,              // テキストの伸縮(ストレッチ)モード : なし
            false,          // ishtml : NO
            true,           // 行幅に自動調整 : する
            $itemDef->h,    // 高さ上限
            'M'             // 文字配置(縦) : 中央
        );
    }

    /**
     * マル印を出力する。
     *
     * @param [type] $itemDef 項目定義情報
     * 
     * @return void
     */
    private function putCircle($itemDef) {
        if (array_key_exists('circleRadius', $itemDef)) {
            $radius = $itemDef->circleRadius;
        } else {
            $radius = 2.5;
        }

        $this->pdf->SetLineWidth(0.5);

        $this->pdf->Circle($itemDef->x, $itemDef->y, $radius);
    }

    /**
     * チェック印を出力する。
     *
     * @param [type] $itemDef 項目定義情報
     * 
     * @return void
     */
    private function putCheck($itemDef) {
        if (array_key_exists('checkSize', $itemDef)) {
            $size = $itemDef->checkSize;
        } else {
            $size = 1.5;
        }

        $this->pdf->SetLineWidth(0.5);
        
        $x1 = $itemDef->x - ($size * 1.1);
        $x2 = $itemDef->x;
        $x3 = $itemDef->x + ($size * 1.1);
        $y1 = $itemDef->y - ($size * 0.5);
        $y2 = $itemDef->y + ($size);
        $y3 = $itemDef->y - ($size * 1.25);
        $this->pdf->Line($x1, $y1, $x2, $y2); 
        $this->pdf->Line($x2, $y2, $x3, $y3);
    }

}
