<?php

/**
 * 面談記録情報を取得し、結果を返す。
 *
 * @param $userId ユーザID
 * 
 * @return array[] 取得結果
 */
function search($userId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         // 1.基本情報
         .     "admins.supervisor_id AS user_id, "
         .     "admins.user_name AS user_name, "
         .     "admins.position AS user_department, "
         .     "admins.department AS supervisor_department, "
         .     "DATE_FORMAT(rep.F_1_1, '%Y/%m/%d') AS interview_ymd, "
         // 2.面談対応者
         .     "rep.F_2_1, "
         .     "rep.F_2_2, "
         .     "rep.F_2_3, "
         // 3.面談事項
         .     "F_3_1_1_1, F_3_1_1_2, "
         .     "F_3_1_2_1, F_3_1_2_2, "
         .     "F_3_1_3_1, F_3_1_3_2, "
         .     "F_3_2_1_1, F_3_2_1_2, "
         .     "F_3_2_2_1, F_3_2_2_2, "
         .     "F_3_2_3_1, F_3_2_3_2, "
         .     "F_3_2_4_1, F_3_2_4_2, "
         .     "F_3_2_5_1, F_3_2_5_2, "
         .     "F_3_2_6_1, F_3_2_6_2, "
         .     "F_3_3_1_1, F_3_3_1_2, "
         .     "F_3_3_2_1, F_3_3_2_2, "
         .     "F_3_3_3_1, F_3_3_3_2, "
         .     "F_3_3_4_1, F_3_3_4_2, "
         .     "F_3_3_5_1, F_3_3_5_2, "
         .     "F_3_4_1_1, F_3_4_1_2, "
         .     "F_3_4_2_1, F_3_4_2_2, "
         .     "F_3_5_1_1, F_3_5_1_2, "
         .     "F_3_5_2_1, F_3_5_2_2, F_3_5_2_3, "
         .     "F_3_6, "
         .     "F_3_7, "
         // 4.法令違反への対応
         .     "DATE_FORMAT(rep.F_4_1, '%Y/%m/%d') AS F_4_1_ymd, "
         .     "F_4_2, "
         .     "F_4_3_1_1, F_4_3_1_2, F_4_3_1_3, "
         .     "F_4_3_2_1, DATE_FORMAT(rep.F_4_3_2_2, '%Y/%m/%d') AS F_4_3_2_2_ymd, F_4_3_2_3, F_4_3_2_4, F_4_3_2_5, "
         .     "F_4_3_3_1, DATE_FORMAT(rep.F_4_3_3_2, '%Y/%m/%d') AS F_4_3_3_2_ymd, F_4_3_3_3, F_4_3_3_4 "
         . "FROM tbl_report_008 rep "
         . "INNER JOIN tbl_supervisor_info admins "
         .     "ON admins.id = rep.admin_id "
         . "LEFT JOIN mst_company companies "
         .     "ON admins.company_id = companies.id "
         . "WHERE rep.admin_id = :user_id "
    ;
    $params = array(
        ":user_id" => getSupervisorRowId($db,$userId)
    );

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 表示用データを追加設定。
    $yesno     = getOptionItems("comm", "yesno");
    $supporter = getOptionItems("WK02", "09_supporter");
    $response1 = getOptionItems("WK02", "09_response_1");
    $response2 = getOptionItems("WK02", "09_response_2");
    $response3 = getOptionItems("WK02", "09_response_3");
    $response4 = getOptionItems("WK02", "09_response_4");

    // コードに対応する文言を設定。
    $result["F_2_2"] = getOprionItemValue($supporter, $result["F_2_2"]);

    $result["F_3_1_1_1"] = getOprionItemValue($yesno, $result["F_3_1_1_1"]);
    $result["F_3_1_2_1"] = getOprionItemValue($yesno, $result["F_3_1_2_1"]);
    $result["F_3_1_3_1"] = getOprionItemValue($yesno, $result["F_3_1_3_1"]);
    $result["F_3_2_1_1"] = getOprionItemValue($yesno, $result["F_3_2_1_1"]);
    $result["F_3_2_2_1"] = getOprionItemValue($yesno, $result["F_3_2_2_1"]);
    $result["F_3_2_3_1"] = getOprionItemValue($yesno, $result["F_3_2_3_1"]);
    $result["F_3_2_4_1"] = getOprionItemValue($yesno, $result["F_3_2_4_1"]);
    $result["F_3_2_5_1"] = getOprionItemValue($yesno, $result["F_3_2_5_1"]);
    $result["F_3_2_6_1"] = getOprionItemValue($yesno, $result["F_3_2_6_1"]);
    $result["F_3_3_1_1"] = getOprionItemValue($yesno, $result["F_3_3_1_1"]);
    $result["F_3_3_2_1"] = getOprionItemValue($yesno, $result["F_3_3_2_1"]);
    $result["F_3_3_3_1"] = getOprionItemValue($yesno, $result["F_3_3_3_1"]);
    $result["F_3_3_4_1"] = getOprionItemValue($yesno, $result["F_3_3_4_1"]);
    $result["F_3_3_5_1"] = getOprionItemValue($yesno, $result["F_3_3_5_1"]);
    $result["F_3_4_1_1"] = getOprionItemValue($yesno, $result["F_3_4_1_1"]);
    $result["F_3_4_2_1"] = getOprionItemValue($yesno, $result["F_3_4_2_1"]);
    $result["F_3_5_1_1"] = getOprionItemValue($yesno, $result["F_3_5_1_1"]);
    $result["F_3_5_2_1"] = getOprionItemValue($yesno, $result["F_3_5_2_1"]);
    $result["F_3_6"    ] = getOprionItemValue($yesno, $result["F_3_6"]);

    $result["F_4_3_1_1"] = getOprionItemValue($response1, $result["F_4_3_1_1"]);
    $result["F_4_3_2_1"] = getOprionItemValue($response2, $result["F_4_3_2_1"]);
    $result["F_4_3_2_5"] = getOprionItemValue($response3, $result["F_4_3_2_5"]);
    $result["F_4_3_3_1"] = getOprionItemValue($response4, $result["F_4_3_3_1"]);

    // 取得結果を返す。
    return $result;
}

/**
 * 労働者の行IDを取得する。
 *
 * @param string $workerId 取得対象の労働者ID
 * 
 * @return integer 行ID
 */
function getSupervisorRowId($db,$supervisor_id) {
    $sql = "SELECT id AS rowid "
            . "FROM tbl_supervisor_info "
            . "WHERE supervisor_id = :supervisor_id "
    ;
    $params[":supervisor_id"] = $supervisor_id;
    
    $result = $db->selectOne($sql, $params);

    if ($result != null) {
        return $result["rowid"];
    } else {
        return null;
    }
}

