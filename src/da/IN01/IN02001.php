<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * @param $pageNo ページ番号
 * 
 * @return PageData 検索結果
 */
function search($items, $pageNo) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE句を生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    // SQL文を生成。
    $sql = "SELECT "
        .     "admins.supervisor_id AS user_id, "
         .     "admins.user_name AS user_name, "
         .     "companies.company_name AS company_name, "
         .     "admins.department, "
         .     "DATE_FORMAT(rep.create_date, '%Y/%m/%d') AS create_ymd, "
         .     "DATE_FORMAT(rep.F_1_1, '%Y/%m/%d') AS interview_ymd "
         . "FROM tbl_report_008 rep "
         . "INNER JOIN tbl_supervisor_info admins "
         .     "ON admins.id = rep.admin_id "
         . "LEFT JOIN mst_company companies "
         .     "ON admins.company_id = companies.id "
    ;
    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY admins.supervisor_id ";

    // SQL文を発行する。
    $pageData = $db->getPageData($sql, $params, $pageNo);

    // 取得結果を返す。
    return $pageData;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $items     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($items, &$whereSqls, &$params) {
    // ・監督者ID
    if ($items["C_01"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "admins.supervisor_id = :user_id ";
        $params[":user_id"] = $items["C_01"];
    }

    // ・監督者氏名
    if ($items["C_02"] != "") {
        // 部分一致条件を設定。
        $whereSqls[] = "admins.user_name LIKE :user_name ";
        $params[":user_name"] = "%".$items["C_02"]."%";
    }

    // ・企業名 (受入機関ユーザでログイン中のみ)
    if ($items["C_company"] != "") {
        $whereSqls[] = "admins.company_id = :company_id ";
        $params[":company_id"] = $items["C_company"];
    }

    // 所属部署
    if ($items["C_03"] != "") {
        $whereSqls[] = "admins.department LIKE :department ";
        $params[":department"] = "%".$items["C_03"]."%";
    }

    // ・登録日
    if ($items["C_04_1"] != "") {
        $whereSqls[] = "rep.create_date >= STR_TO_DATE(:create_date_from, '%Y-%m-%d') ";
        $params[":create_date_from"] = $items["C_04_1"];
    }
    if ($items["C_04_2"] != "") {
        $whereSqls[] = "rep.create_date < DATE_ADD(STR_TO_DATE(:create_date_to, '%Y-%m-%d'), INTERVAL 1 DAY) ";
        $params[":create_date_to"] = $items["C_04_2"];
    }

    // ・未実施者も含める
    if ($items["C_05"] == "") {
        // チェックがされていない場合、登録日 IS NOT NULL の条件を設定。
        $whereSqls[] = "rep.F_1_1 IS NOT NULL ";
    }
}


/**
 * 受入企業の一覧を取得する。
 *
 * @return void
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "WHERE classification = '1' "  // 受入機関
         . "ORDER BY company_name "
    ;
    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}
