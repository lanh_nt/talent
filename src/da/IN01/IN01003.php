<?php

/**
 * 面談記録情報を取得し、結果を返す。
 *
 * @param $userId ユーザID
 * 
 * @return array[] 取得結果
 */
function search($userId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         // 1.基本情報
         .     "workers.worker_id AS user_id, "
         .     "workers.user_name AS user_name, "
         .     "companies.company_name AS company_name, "
         .     "DATE_FORMAT(rep.E_1_1, '%Y-%m-%d') AS interview_ymd, "
         // 2.面談対応者
         .     "rep.E_2_1, "
         .     "rep.E_2_2, "
         .     "rep.E_2_3, "
         // 3.面談事項
         .     "E_3_1_1_1, E_3_1_1_2, "
         .     "E_3_1_2_1, E_3_1_2_2, "
         .     "E_3_1_3_1, E_3_1_3_2, "
         .     "E_3_2_1_1, E_3_2_1_2, "
         .     "E_3_2_2_1, E_3_2_2_2, "
         .     "E_3_2_3_1, E_3_2_3_2, "
         .     "E_3_2_4_1, E_3_2_4_2, "
         .     "E_3_2_5_1, E_3_2_5_2, "
         .     "E_3_2_6_1, E_3_2_6_2, "
         .     "E_3_3_1_1, E_3_3_1_2, "
         .     "E_3_3_2_1, E_3_3_2_2, "
         .     "E_3_3_3_1, E_3_3_3_2, "
         .     "E_3_3_4_1, E_3_3_4_2, "
         .     "E_3_3_5_1, E_3_3_5_2, "
         .     "E_3_4_1_1, E_3_4_1_2, "
         .     "E_3_4_2_1, E_3_4_2_2, "
         .     "E_3_5_1_1, E_3_5_1_2, "
         .     "E_3_5_2_1, E_3_5_2_2, E_3_5_2_3, "
         .     "E_3_6, "
         .     "E_3_7, "
         // 4.法令違反への対応
         .     "DATE_FORMAT(rep.E_4_1, '%Y-%m-%d') AS E_4_1_ymd, "
         .     "E_4_2, "
         .     "E_4_3_1_1, E_4_3_1_2, E_4_3_1_3, "
         .     "E_4_3_2_1, DATE_FORMAT(rep.E_4_3_2_2, '%Y-%m-%d') AS E_4_3_2_2_ymd, E_4_3_2_3, E_4_3_2_4, E_4_3_2_5, "
         .     "E_4_3_3_1, DATE_FORMAT(rep.E_4_3_3_2, '%Y-%m-%d') AS E_4_3_3_2_ymd, E_4_3_3_3, E_4_3_3_4 "
         . "FROM mst_workers workers "
         . "INNER JOIN tbl_report_007 rep "
         .     "ON workers.id = rep.worker_id "
         . "LEFT JOIN mst_company companies "
         .     "ON workers.company_id = companies.id "
         . "WHERE workers.worker_id = :user_id "
    ;
    $params = array(
        ":user_id" => $userId
    );

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 取得結果を返す。
    return $result;
}

/**
 * 登録処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function update($items) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        // 更新対象のユーザ(労働者)に対応するレコードのIDを取得する。
        $userRowId = $db->getWorkerRowId($items["user_id"]);

        // カラムと設定値を定義。
        $columns = array(
            // 1.基本情報
            "E_1_1" => setDateParam($items["interview_ymd"]),
            // 2.面談対応者
            "E_2_1" => $items["E_2_1"], "E_2_2" => $items["E_2_2"], "E_2_3" => $items["E_2_3"],
            // 3.面談事項
            "E_3_1_1_1" => $items["E_3_1_1_1"], "E_3_1_1_2" => $items["E_3_1_1_2"],
            "E_3_1_2_1" => $items["E_3_1_2_1"], "E_3_1_2_2" => $items["E_3_1_2_2"],
            "E_3_1_3_1" => $items["E_3_1_3_1"], "E_3_1_3_2" => $items["E_3_1_3_2"],
            "E_3_2_1_1" => $items["E_3_2_1_1"], "E_3_2_1_2" => $items["E_3_2_1_2"],
            "E_3_2_2_1" => $items["E_3_2_2_1"], "E_3_2_2_2" => $items["E_3_2_2_2"],
            "E_3_2_3_1" => $items["E_3_2_3_1"], "E_3_2_3_2" => $items["E_3_2_3_2"],
            "E_3_2_4_1" => $items["E_3_2_4_1"], "E_3_2_4_2" => $items["E_3_2_4_2"],
            "E_3_2_5_1" => $items["E_3_2_5_1"], "E_3_2_5_2" => $items["E_3_2_5_2"],
            "E_3_2_6_1" => $items["E_3_2_6_1"], "E_3_2_6_2" => $items["E_3_2_6_2"],
            "E_3_3_1_1" => $items["E_3_3_1_1"], "E_3_3_1_2" => $items["E_3_3_1_2"],
            "E_3_3_2_1" => $items["E_3_3_2_1"], "E_3_3_2_2" => $items["E_3_3_2_2"],
            "E_3_3_3_1" => $items["E_3_3_3_1"], "E_3_3_3_2" => $items["E_3_3_3_2"],
            "E_3_3_4_1" => $items["E_3_3_4_1"], "E_3_3_4_2" => $items["E_3_3_4_2"],
            "E_3_3_5_1" => $items["E_3_3_5_1"], "E_3_3_5_2" => $items["E_3_3_5_2"],
            "E_3_4_1_1" => $items["E_3_4_1_1"], "E_3_4_1_2" => $items["E_3_4_1_2"],
            "E_3_4_2_1" => $items["E_3_4_2_1"], "E_3_4_2_2" => $items["E_3_4_2_2"],
            "E_3_5_1_1" => $items["E_3_5_1_1"], "E_3_5_1_2" => $items["E_3_5_1_2"],
            "E_3_5_2_1" => $items["E_3_5_2_1"], "E_3_5_2_2" => $items["E_3_5_2_2"], "E_3_5_2_3" => $items["E_3_5_2_3"],
            "E_3_6"     => $items["E_3_6"],
            "E_3_7"     => $items["E_3_7"],
            // 4.法令違反への対応
            "E_4_1"     => setDateParam($items["E_4_1_ymd"]),
            "E_4_2"     => $items["E_4_2"],
            "E_4_3_1_1" => $items["E_4_3_1_1"], "E_4_3_1_2" =>              $items["E_4_3_1_2"],      "E_4_3_1_3" => $items["E_4_3_1_3"],
            "E_4_3_2_1" => $items["E_4_3_2_1"], "E_4_3_2_2" => setDateParam($items["E_4_3_2_2_ymd"]), "E_4_3_2_3" => $items["E_4_3_2_3"], "E_4_3_2_4" => $items["E_4_3_2_4"], "E_4_3_2_5" => $items["E_4_3_2_5"],
            "E_4_3_3_1" => $items["E_4_3_3_1"], "E_4_3_3_2" => setDateParam($items["E_4_3_3_2_ymd"]), "E_4_3_3_3" => $items["E_4_3_3_3"], "E_4_3_3_4" => $items["E_4_3_3_4"]
        );
        $whereString = "worker_id = :user_id ";
        $whereParams = array(
            "user_id" => $userRowId
        );
    
        // update処理を発行する。
        $db->update("tbl_report_007", $columns, $whereString, $whereParams);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}

/**
 * 受入企業の一覧を取得する。
 *
 * @return void
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "WHERE classification = '1' "  // 受入機関
         . "ORDER BY company_name ";

    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}
