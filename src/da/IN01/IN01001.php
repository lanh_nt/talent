<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * @param $pageNo ページ番号
 * 
 * @return PageData 検索結果
 */
function search($items, $pageNo) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE句を生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    // SQL文を生成。
    $sql = "SELECT "
         .     "workers.worker_id AS user_id, "
         .     "workers.user_name AS user_name, "
         .     "companies.company_name AS company_name, "
         .     "DATE_FORMAT(rep.create_date, '%Y/%m/%d') AS create_ymd, "
         .     "DATE_FORMAT(rep.E_1_1,       '%Y/%m/%d') AS interview_ymd "
         . "FROM mst_workers workers "
         . "INNER JOIN tbl_report_007 rep "
         .     "ON workers.id = rep.worker_id "
         . "LEFT JOIN mst_company companies "
         .     "ON workers.company_id = companies.id "
    ;
    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY workers.worker_id ";

    // SQL文を発行する。
    $pageData = $db->getPageData($sql, $params, $pageNo);

    // 取得結果を返す。
    return $pageData;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $items     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($items, &$whereSqls, &$params) {
    // ・労働者ID
    if ($items["C_01"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "workers.worker_id = :user_id ";
        $params[":user_id"] = $items["C_01"];
    }

    // ・労働者氏名
    if ($items["C_02"] != "") {
        // 労働者姓/名に対して部分一致条件を設定。
        $whereSqls[] = "workers.user_name LIKE :user_name ";
        $params[":user_name"] = "%".$items["C_02"]."%";
    }

    // ・企業名
    if ($items["C_03"] != "") {
        $whereSqls[] = "workers.company_id = :company_id ";
        $params[":company_id"] = $items["C_03"];
    }

    // ・登録日
    if ($items["C_04_1"] != "") {
        $whereSqls[] = "rep.create_date >= STR_TO_DATE(:create_date_from, '%Y-%m-%d') ";
        $params[":create_date_from"] = $items["C_04_1"];
    }
    if ($items["C_04_2"] != "") {
        $whereSqls[] = "rep.create_date < DATE_ADD(STR_TO_DATE(:create_date_to, '%Y-%m-%d'), INTERVAL 1 DAY) ";
        $params[":create_date_to"] = $items["C_04_2"];
    }

    // ・未実施者も含める
    if ($items["C_05"] == "") {
        // チェックがされていない場合、登録日 IS NOT NULL の条件を設定。
        $whereSqls[] = "rep.E_1_1 IS NOT NULL ";
    }
}


/**
 * 受入企業の一覧を取得する。
 *
 * @return void
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "WHERE classification = '1' "  // 受入機関
         . "ORDER BY company_name "
    ;
    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}
