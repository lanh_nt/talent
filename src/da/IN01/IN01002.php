<?php

/**
 * 面談記録情報を取得し、結果を返す。
 *
 * @param $userId ユーザID
 * 
 * @return array[] 取得結果
 */
function search($userId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         // 1.基本情報
         .     "workers.worker_id AS user_id, "
         .     "workers.user_name AS user_name, "
         .     "companies.company_name AS company_name, "
         .     "DATE_FORMAT(rep.E_1_1, '%Y/%m/%d') AS interview_ymd, "
         // 2.面談対応者
         .     "rep.E_2_1, "
         .     "rep.E_2_2, "
         .     "rep.E_2_3, "
         // 3.面談事項
         .     "E_3_1_1_1, E_3_1_1_2, "
         .     "E_3_1_2_1, E_3_1_2_2, "
         .     "E_3_1_3_1, E_3_1_3_2, "
         .     "E_3_2_1_1, E_3_2_1_2, "
         .     "E_3_2_2_1, E_3_2_2_2, "
         .     "E_3_2_3_1, E_3_2_3_2, "
         .     "E_3_2_4_1, E_3_2_4_2, "
         .     "E_3_2_5_1, E_3_2_5_2, "
         .     "E_3_2_6_1, E_3_2_6_2, "
         .     "E_3_3_1_1, E_3_3_1_2, "
         .     "E_3_3_2_1, E_3_3_2_2, "
         .     "E_3_3_3_1, E_3_3_3_2, "
         .     "E_3_3_4_1, E_3_3_4_2, "
         .     "E_3_3_5_1, E_3_3_5_2, "
         .     "E_3_4_1_1, E_3_4_1_2, "
         .     "E_3_4_2_1, E_3_4_2_2, "
         .     "E_3_5_1_1, E_3_5_1_2, "
         .     "E_3_5_2_1, E_3_5_2_2, E_3_5_2_3, "
         .     "E_3_6, "
         .     "E_3_7, "
         // 4.法令違反への対応
         .     "DATE_FORMAT(rep.E_4_1, '%Y/%m/%d') AS E_4_1_ymd, "
         .     "E_4_2, "
         .     "E_4_3_1_1, E_4_3_1_2, E_4_3_1_3, "
         .     "E_4_3_2_1, DATE_FORMAT(rep.E_4_3_2_2, '%Y/%m/%d') AS E_4_3_2_2_ymd, E_4_3_2_3, E_4_3_2_4, E_4_3_2_5, "
         .     "E_4_3_3_1, DATE_FORMAT(rep.E_4_3_3_2, '%Y/%m/%d') AS E_4_3_3_2_ymd, E_4_3_3_3, E_4_3_3_4 "
         . "FROM mst_workers workers "
         . "INNER JOIN tbl_report_007 rep "
         .     "ON workers.id = rep.worker_id "
         . "LEFT JOIN mst_company companies "
         .     "ON workers.company_id = companies.id "
         . "WHERE workers.worker_id = :user_id "
    ;
    $params = array(
        ":user_id" => $userId
    );

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 表示用データを追加設定。
    $yesno     = getOptionItems("comm", "yesno");
    $supporter = getOptionItems("WK02", "09_supporter");
    $response1 = getOptionItems("WK02", "09_response_1");
    $response2 = getOptionItems("WK02", "09_response_2");
    $response3 = getOptionItems("WK02", "09_response_3");
    $response4 = getOptionItems("WK02", "09_response_4");

    // コードに対応する文言を設定。
    $result["E_2_2"] = getOprionItemValue($supporter, $result["E_2_2"]);

    $result["E_3_1_1_1"] = getOprionItemValue($yesno, $result["E_3_1_1_1"]);
    $result["E_3_1_2_1"] = getOprionItemValue($yesno, $result["E_3_1_2_1"]);
    $result["E_3_1_3_1"] = getOprionItemValue($yesno, $result["E_3_1_3_1"]);
    $result["E_3_2_1_1"] = getOprionItemValue($yesno, $result["E_3_2_1_1"]);
    $result["E_3_2_2_1"] = getOprionItemValue($yesno, $result["E_3_2_2_1"]);
    $result["E_3_2_3_1"] = getOprionItemValue($yesno, $result["E_3_2_3_1"]);
    $result["E_3_2_4_1"] = getOprionItemValue($yesno, $result["E_3_2_4_1"]);
    $result["E_3_2_5_1"] = getOprionItemValue($yesno, $result["E_3_2_5_1"]);
    $result["E_3_2_6_1"] = getOprionItemValue($yesno, $result["E_3_2_6_1"]);
    $result["E_3_3_1_1"] = getOprionItemValue($yesno, $result["E_3_3_1_1"]);
    $result["E_3_3_2_1"] = getOprionItemValue($yesno, $result["E_3_3_2_1"]);
    $result["E_3_3_3_1"] = getOprionItemValue($yesno, $result["E_3_3_3_1"]);
    $result["E_3_3_4_1"] = getOprionItemValue($yesno, $result["E_3_3_4_1"]);
    $result["E_3_3_5_1"] = getOprionItemValue($yesno, $result["E_3_3_5_1"]);
    $result["E_3_4_1_1"] = getOprionItemValue($yesno, $result["E_3_4_1_1"]);
    $result["E_3_4_2_1"] = getOprionItemValue($yesno, $result["E_3_4_2_1"]);
    $result["E_3_5_1_1"] = getOprionItemValue($yesno, $result["E_3_5_1_1"]);
    $result["E_3_5_2_1"] = getOprionItemValue($yesno, $result["E_3_5_2_1"]);
    $result["E_3_6"    ] = getOprionItemValue($yesno, $result["E_3_6"]);

    $result["E_4_3_1_1"] = getOprionItemValue($response1, $result["E_4_3_1_1"]);
    $result["E_4_3_2_1"] = getOprionItemValue($response2, $result["E_4_3_2_1"]);
    $result["E_4_3_2_5"] = getOprionItemValue($response3, $result["E_4_3_2_5"]);
    $result["E_4_3_3_1"] = getOprionItemValue($response4, $result["E_4_3_3_1"]);

    // 取得結果を返す。
    return $result;
}
