<?php

/**
 * 面談記録情報を取得し、結果を返す。
 *
 * @param $userId ユーザID
 * 
 * @return array[] 取得結果
 */
function search($userId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         // 1.基本情報
         .     "admins.supervisor_id AS user_id, "
         .     "admins.user_name AS user_name, "
         .     "admins.position AS user_department, "
         .     "admins.department AS supervisor_department, "
         .     "DATE_FORMAT(rep.F_1_1, '%Y-%m-%d') AS interview_ymd, "
         // 2.面談対応者
         .     "rep.F_2_1, "
         .     "rep.F_2_2, "
         .     "rep.F_2_3, "
         // 3.面談事項
         .     "F_3_1_1_1, F_3_1_1_2, "
         .     "F_3_1_2_1, F_3_1_2_2, "
         .     "F_3_1_3_1, F_3_1_3_2, "
         .     "F_3_2_1_1, F_3_2_1_2, "
         .     "F_3_2_2_1, F_3_2_2_2, "
         .     "F_3_2_3_1, F_3_2_3_2, "
         .     "F_3_2_4_1, F_3_2_4_2, "
         .     "F_3_2_5_1, F_3_2_5_2, "
         .     "F_3_2_6_1, F_3_2_6_2, "
         .     "F_3_3_1_1, F_3_3_1_2, "
         .     "F_3_3_2_1, F_3_3_2_2, "
         .     "F_3_3_3_1, F_3_3_3_2, "
         .     "F_3_3_4_1, F_3_3_4_2, "
         .     "F_3_3_5_1, F_3_3_5_2, "
         .     "F_3_4_1_1, F_3_4_1_2, "
         .     "F_3_4_2_1, F_3_4_2_2, "
         .     "F_3_5_1_1, F_3_5_1_2, "
         .     "F_3_5_2_1, F_3_5_2_2, F_3_5_2_3, "
         .     "F_3_6, "
         .     "F_3_7, "
         // 4.法令違反への対応
         .     "DATE_FORMAT(rep.F_4_1, '%Y-%m-%d') AS F_4_1_ymd, "
         .     "F_4_2, "
         .     "F_4_3_1_1, F_4_3_1_2, F_4_3_1_3, "
         .     "F_4_3_2_1, DATE_FORMAT(rep.F_4_3_2_2, '%Y-%m-%d') AS F_4_3_2_2_ymd, F_4_3_2_3, F_4_3_2_4, F_4_3_2_5, "
         .     "F_4_3_3_1, DATE_FORMAT(rep.F_4_3_3_2, '%Y-%m-%d') AS F_4_3_3_2_ymd, F_4_3_3_3, F_4_3_3_4 "
         . "FROM tbl_report_008 rep "
         . "INNER JOIN tbl_supervisor_info admins "
         .     "ON admins.id = rep.admin_id "
         . "LEFT JOIN mst_company companies "
         .     "ON admins.company_id = companies.id "
         . "WHERE rep.admin_id = :user_id "
    ;
    $params = array(
        ":user_id" => getSupervisorRowId($db,$userId)
    );

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 取得結果を返す。
    return $result;
}

/**
 * 登録処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function update($items) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {

        // カラムと設定値を定義。
        $columns = array(
            // 1.基本情報
            "F_1_1" => setDateParam($items["interview_ymd"]),
            // 2.面談対応者
            "F_2_1" => $items["F_2_1"], "F_2_2" => $items["F_2_2"], "F_2_3" => $items["F_2_3"],
            // 3.面談事項
            "F_3_1_1_1" => $items["F_3_1_1_1"], "F_3_1_1_2" => $items["F_3_1_1_2"],
            "F_3_1_2_1" => $items["F_3_1_2_1"], "F_3_1_2_2" => $items["F_3_1_2_2"],
            "F_3_1_3_1" => $items["F_3_1_3_1"], "F_3_1_3_2" => $items["F_3_1_3_2"],
            "F_3_2_1_1" => $items["F_3_2_1_1"], "F_3_2_1_2" => $items["F_3_2_1_2"],
            "F_3_2_2_1" => $items["F_3_2_2_1"], "F_3_2_2_2" => $items["F_3_2_2_2"],
            "F_3_2_3_1" => $items["F_3_2_3_1"], "F_3_2_3_2" => $items["F_3_2_3_2"],
            "F_3_2_4_1" => $items["F_3_2_4_1"], "F_3_2_4_2" => $items["F_3_2_4_2"],
            "F_3_2_5_1" => $items["F_3_2_5_1"], "F_3_2_5_2" => $items["F_3_2_5_2"],
            "F_3_2_6_1" => $items["F_3_2_6_1"], "F_3_2_6_2" => $items["F_3_2_6_2"],
            "F_3_3_1_1" => $items["F_3_3_1_1"], "F_3_3_1_2" => $items["F_3_3_1_2"],
            "F_3_3_2_1" => $items["F_3_3_2_1"], "F_3_3_2_2" => $items["F_3_3_2_2"],
            "F_3_3_3_1" => $items["F_3_3_3_1"], "F_3_3_3_2" => $items["F_3_3_3_2"],
            "F_3_3_4_1" => $items["F_3_3_4_1"], "F_3_3_4_2" => $items["F_3_3_4_2"],
            "F_3_3_5_1" => $items["F_3_3_5_1"], "F_3_3_5_2" => $items["F_3_3_5_2"],
            "F_3_4_1_1" => $items["F_3_4_1_1"], "F_3_4_1_2" => $items["F_3_4_1_2"],
            "F_3_4_2_1" => $items["F_3_4_2_1"], "F_3_4_2_2" => $items["F_3_4_2_2"],
            "F_3_5_1_1" => $items["F_3_5_1_1"], "F_3_5_1_2" => $items["F_3_5_1_2"],
            "F_3_5_2_1" => $items["F_3_5_2_1"], "F_3_5_2_2" => $items["F_3_5_2_2"], "F_3_5_2_3" => $items["F_3_5_2_3"],
            "F_3_6"     => $items["F_3_6"],
            "F_3_7"     => $items["F_3_7"],
            // 4.法令違反への対応
            "F_4_1"     => setDateParam($items["F_4_1_ymd"]),
            "F_4_2"     => $items["F_4_2"],
            "F_4_3_1_1" => $items["F_4_3_1_1"], "F_4_3_1_2" =>              $items["F_4_3_1_2"],      "F_4_3_1_3" => $items["F_4_3_1_3"],
            "F_4_3_2_1" => $items["F_4_3_2_1"], "F_4_3_2_2" => setDateParam($items["F_4_3_2_2_ymd"]), "F_4_3_2_3" => $items["F_4_3_2_3"], "F_4_3_2_4" => $items["F_4_3_2_4"], "F_4_3_2_5" => $items["F_4_3_2_5"],
            "F_4_3_3_1" => $items["F_4_3_3_1"], "F_4_3_3_2" => setDateParam($items["F_4_3_3_2_ymd"]), "F_4_3_3_3" => $items["F_4_3_3_3"], "F_4_3_3_4" => $items["F_4_3_3_4"]
        );
        $whereString = "admin_id = :user_id ";
        $whereParams = array(
            "user_id" => getSupervisorRowId($db,$items["user_id"])
        );
    
        // update処理を発行する。
        $db->update("tbl_report_008", $columns, $whereString, $whereParams);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}

/**
 * 労働者の行IDを取得する。
 *
 * @param string $workerId 取得対象の労働者ID
 * 
 * @return integer 行ID
 */
function getSupervisorRowId($db,$supervisor_id) {
    $sql = "SELECT id AS rowid "
            . "FROM tbl_supervisor_info "
            . "WHERE supervisor_id = :supervisor_id "
    ;
    $params[":supervisor_id"] = $supervisor_id;
    
    $result = $db->selectOne($sql, $params);

    if ($result != null) {
        return $result["rowid"];
    } else {
        return null;
    }
}



/**
 * 受入企業の一覧を取得する。
 *
 * @return void
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "WHERE classification = '1' "  // 受入機関
         . "ORDER BY company_name ";

    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}
