<?php

/**
 * ファイル管理情報を取得し、結果を返す。
 *
 * @param $fileRowId ファイルID
 * 
 * @return array[] 取得結果
 */
function search($fileRowId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT  "
         .     "file_data.id AS file_rowid, "
         .     "users.user_name AS upload_user_name, "
         .     "DATE_FORMAT(file_data.create_date, '%Y/%m/%d') AS upload_ymd,  "
         .     "file_data.file_name_org, "
         .     "file_data.classification AS file_type,  "
         .     "file_data.comment AS file_comment "
         . "FROM tbl_upload_file file_data "
         . "INNER JOIN ( "
         .     "SELECT '1' AS user_type, id AS user_id, user_name, company_id "
         .     "FROM mst_admin_user "
         .     "UNION "
         .     "SELECT '2' AS user_type, id AS user_id, user_name, company_id "
         .     "FROM mst_workers "
         . ") users "
         .     "ON  file_data.upload_user_type = users.user_type "
         .     "AND file_data.create_id = users.user_id "
         . "WHERE file_data.id = :fileRowId "
    ;
    $params = array();
    $params[":fileRowId"] = $fileRowId;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 表示用データを追加設定。
    $fileType = getOptionItems("comm", "file_type");

    // コードに対応する文言を設定。
    $result["file_type_name"] = getOprionItemValue($fileType, $result["file_type"]);

    // ブラウザで開くファイルかどうかフラグを設定。
    $result["isDispDirect"] = preg_match('/\.pdf$/i', $result["file_name_org"]);

    // 取得結果を返す。
    return $result;
}
