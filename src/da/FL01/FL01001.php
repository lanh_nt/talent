<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * @param $pageNo ページ番号
 * 
 * @return PageData 検索結果
 */
function search($items, $pageNo) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE句を生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    if (count($whereSqls) > 0) {
        $whereSqlString = "WHERE " . $db->getWhereSql($whereSqls);
    } else {
        $whereSqlString = "";
    }

    // SQL文を生成。
    $sql = "SELECT "
         .     "file_data.id AS file_rowid, "
         .     "file_data.classification AS file_type,  "
         .     "users.user_name AS upload_user_name, "
         .     "DATE_FORMAT(file_data.create_date, '%Y/%m/%d') AS upload_ymd,  "
         .     "file_data.comment AS file_comment "
         . "FROM tbl_upload_file file_data "
         . "INNER JOIN ( "
         .     "SELECT '1' AS user_type, id AS user_id, user_name, company_id "
         .     "FROM mst_admin_user "
         .     "UNION "
         .     "SELECT '2' AS user_type, id AS user_id, user_name, company_id "
         .     "FROM mst_workers "
         . ") users "
         .     "ON  file_data.upload_user_type = users.user_type "
         .     "AND file_data.create_id = users.user_id "
         . $whereSqlString
         . "ORDER BY upload_ymd DESC, file_rowid DESC "
    ;

    // SQL文を発行する。
    $pageData = $db->getPageData($sql, $params, $pageNo);

    // 表示用データを追加設定。
    $fileType = getOptionItems("comm", "file_type");

    // コードに対応する文言を設定。
    foreach ($pageData->pageDatas as &$row) {
        $row["file_type_name"] = getOprionItemValue($fileType, $row["file_type"]);
    }

    // 取得結果を返す。
    return $pageData;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $items     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($items, &$whereSqls, &$params) {
    // ・企業ID
    if ($items["C_company"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "file_data.company_id = :company_id ";
        $params[":company_id"] = $items["C_company"];
    }

    // ・労働者ID
    if ($items["C_worker"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "file_data.worker_id = :worker_id ";
        $params[":worker_id"] = $items["C_worker"];
    }

    // ・ファイル名
    if ($items["C_01"] != "") {
        // ファイル名に対して部分一致条件を設定。
        $whereSqls[] = "file_data.file_name_org LIKE :file_name_org ";
        $params[":file_name_org"] = "%".$items["C_01"]."%";
    }

    // ・登録者名
    if ($items["C_02"] != "") {
        // 登録者名に対して部分一致条件を設定。
        $whereSqls[] = "users.user_name LIKE :user_name ";
        $params[":user_name"] = "%".$items["C_02"]."%";
    }

    // ・種別
    if ($items["C_03_1"].$items["C_03_2"].$items["C_03_3"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c3Params = array();
        if ($items["C_03_1"] != "") $c3Params[] = "'1'";
        if ($items["C_03_2"] != "") $c3Params[] = "'2'";
        if ($items["C_03_3"] != "") $c3Params[] = "'3'";
        $c3ParamsString = joinArray($c3Params);
        $whereSqls[] = "file_data.classification IN (".$c3ParamsString.") ";
    }

    // ・登録日
    if ($items["C_04_1"] != "") {
        $whereSqls[] = "file_data.create_date >= STR_TO_DATE(:upload_date_from, '%Y-%m-%d') ";
        $params[":upload_date_from"] = $items["C_04_1"];
    }
    if ($items["C_04_2"] != "") {
        $whereSqls[] = "file_data.create_date < DATE_ADD(STR_TO_DATE(:upload_date_to, '%Y-%m-%d'), INTERVAL 1 DAY) ";
        $params[":upload_date_to"] = $items["C_04_2"];
    }
}


/**
 * 削除処理を実行する。
 *
 * @param integer $fileRowId ファイルID(行ID)
 * 
 * @return boolean 実行成否
 */
function delete($fileRowId) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        // ファイル情報を取得。
        $fileInfo = getFileInfo($db, $fileRowId);

        //----------------------------------
        // ファイル情報を削除。
        //----------------------------------
        $whereString = "id = :file_rowid ";
        $whereParams = array(
            "file_rowid" => $fileRowId
        );
        // delete処理を発行する。
        $rc = $db->delete("tbl_upload_file", $whereString, $whereParams);

        //----------------------------------
        // ファイルをストレージから削除。
        //----------------------------------
        $S3 = new S3_FileAccess();
        $S3->deleteFile(
            1,   // ファイル管理
            $fileInfo["worker_id"], 
            $fileInfo["file_id"]
        );

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}

/**
 * ファイル情報を取得する。
 * 
 * @param $db  DBアクセスクラス
 * @param $fileRowId ファイルID(行ID)
 * 
 * @return array ファイル情報
 */
function getFileInfo($db, $fileRowId) {
    // SQL文を生成。
    $sql = "SELECT "
         .     "workers.worker_id, "
         .     "files.file_id "
         . "FROM tbl_upload_file files "
         . "INNER JOIN mst_workers workers "
         .     "ON files.worker_id = workers.id "
         . "WHERE files.id = :file_rowid "
    ;
    $params = array(
        "file_rowid" => $fileRowId
    );

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 取得結果を返す。
    return $result;
}


/**
 * 受入企業の一覧を取得する。
 *
 * @return void
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "WHERE classification = '1' "  // 受入機関
         . "ORDER BY company_name "
    ;
    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}
