<?php

/**
 * ファイル管理情報を取得し、結果を返す。
 *
 * @param $fileRowId ファイルID
 * 
 * @return array[] 取得結果
 */
function search($fileRowId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         .     "workers.worker_id AS file_user_id, "
         .     "file_data.file_id, "
         .     "file_data.file_name_org "
         . "FROM tbl_upload_file file_data "
         . "INNER JOIN mst_workers workers "
         .     "ON  file_data.worker_id = workers.id "
         . "WHERE file_data.id = :fileRowId "
    ;
    $params = array();
    $params[":fileRowId"] = $fileRowId;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 取得結果を返す。
    return $result;
}
