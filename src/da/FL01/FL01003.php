<?php

/**
 * 登録処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * @param file $upfile アップロードファイル情報
 * 
 * @return boolean 実行成否
 */
function insert($items, $upfile) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        // 労働者のID(メールアドレス)・所属企業を取得。
        $workerInfo = getWorkerInfo($items["D_2"]);

        // 現在日時からファイルIDを生成。
        date_default_timezone_set('Asia/Tokyo');
        $fileId = date("YmdHis");

        //----------------------------------
        // ファイル情報を追加。
        //----------------------------------
        // カラムと設定値を定義。
        $columns = array(
            "file_id"           => $fileId,
            "company_id"        => $workerInfo["company_id"],
            "worker_id"         => $items["D_2"],
            "file_name_org"     => $upfile["name"],
            "classification"    => $items["D_4"],
            "comment"           => $items["D_3"],
            "upload_user_type"  => $_SESSION['loginUserType']
        );

        // insert処理を発行する。
        $db->insert("tbl_upload_file", $columns);

        //----------------------------------
        // ファイルをストレージに登録。
        //----------------------------------
        $buf = file_get_contents($upfile["tmp_name"]);

        $S3 = new S3_FileAccess();
        $S3->fileUpload($buf, $workerInfo["worker_id"], $fileId);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}

/**
 * 対象者の一覧を取得する。
 * 
 * @param integer $companyRowId 取得対象の企業(行ID)
 *
 * @return array 対象者の一覧
 */
function getWorkers($companyRowId = null) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE句を生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqlString = "";
    $params = array();
    if ($companyRowId != null) {
        $whereSqlString = "WHERE company_id = :company_id ";
        $params["company_id"] = $companyRowId;
    }

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "worker_id, "
         .     "user_name "
         . "FROM mst_workers workers "
         . $whereSqlString
         . "ORDER BY user_name "
    ;

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}

/**
 * 対象者の情報を取得する。
 * 
 * @param integer $workerRowId 取得対象の行ID
 *
 * @return array 対象者の情報
 */
function getWorkerInfo($workerRowId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "worker_id, "
         .     "company_id "
         . "FROM mst_workers workers "
         . "WHERE id = :worker_rowid "
    ;
    $params = array(
        "worker_rowid" => $workerRowId
    );

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 取得結果を返す。
    return $result;
}
