<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $cmp_id  検索条件　企業行ID
 * @param $lang  検索条件　言語
 * 
 * @return rowsDatas 検索結果
 */
function search($cmp_id,$lang) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($cmp_id,$lang, $whereSqls, $params);

    // SQL文を生成。
    $sql = "SELECT "
         .     "qa.id as qa_id, "
         .     "qa.question_no, "
         .     "qa.sequence, "
         .     "qa.release_status, "
         .     "qa.delivery_language, "
         .     "qa.question_contents, "
         .     "qa.answer_contents, "
         .     "DATE_FORMAT(qa.create_date, '%Y/%m/%d') AS qa_create_date, "
         .     "DATE_FORMAT(qa.update_date, '%Y/%m/%d') AS qa_update_date "
         . "FROM tbl_qa_info qa "
         ;
    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY qa.sequence,qa_id desc ";

    // SQL文を発行する。
    $rowsDatas = $db->select($sql, $params);

    // 取得結果を返す。
    return $rowsDatas;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $cmp_id  検索条件　企業行ID
 * @param $lang  検索条件　言語
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($cmp_id,$lang, &$whereSqls, &$params) {
    //ID群を分解し、or条件にする

    $where = "";

    $whereSqls[0] = "qa.company_id=:cmp_id ";
    $params[":cmp_id"] = $cmp_id;

    $whereSqls[1] = "qa.release_status=:release_status ";
    $params[":release_status"] = "1";

    $whereSqls[2] = "qa.delivery_language=:delivery_language ";
    $params[":delivery_language"] = $lang;

}


/**
 * 更新処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function update($report_ids,$items) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {

        $IDary = explode(",",$report_ids);

        // カラムと設定値を定義。
        $columns = array(
            "status"              => $items["C_09"],      //ステータス
        );

        for($iLoop=0;$iLoop<count($IDary);$iLoop++){
            if(count($IDary) ==1){
                //無ければ触らない処理（安全策）
                if($items["input_deadline"]!=""){
                    $columns["input_deadline"] = $items["input_deadline"];    //入力期限
                }
                if($items["submission_deadline"]!=""){
                    $columns["submission_deadline"] = $items["submission_deadline"];    //入力期限
                }
            }
    
            // 同時に、SQLパラメータの設定も行う。
            $whereSqls = array();
            $params = array();
            getWhereParams2($IDary[$iLoop], $whereSqls, $params);
    
            $whereSqlString  ="";
    
            if (count($whereSqls) > 0) {
                $whereSqlString = $db->getWhereSql($whereSqls);
            }
    
            // update処理を発行する。
            $db->update("tbl_report_status", $columns, $whereSqlString, $params);
    
        }

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}

/**
 * 受入企業の一覧を取得する。
 *
 * @return array[行データ][キー => 値] 取得結果
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "WHERE classification = '1' "  // 受入機関
         . "ORDER BY company_name ";

    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}
