<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * @param $pageNo ページ番号
 * 
 * @return PageData 検索結果
 */
function search($items, $pageNo) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    // SQL文を生成。
    $sql = "SELECT "
         .     "qa.id as qa_id, "
         .     "qa.question_no, "
         .     "qa.sequence, "
         .     "qa.release_status, "
         .     "qa.delivery_language, "
         .     "qa.question_contents, "
         .     "qa.answer_contents, "
         .     "companies.company_name, "
         .     "DATE_FORMAT(qa.create_date, '%Y/%m/%d') AS qa_create_date "
         . "FROM tbl_qa_info qa "
         . "LEFT JOIN mst_company companies "
         .     "ON qa.company_id = companies.id "
    ;
    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY qa.sequence,qa_id desc ";

    // SQL文を発行する。
    $pageData = $db->getPageData($sql, $params, $pageNo);

    // 表示用データを追加設定。
    $release_status = getOptionItems("comm", "release_status");

    // コードに対応する文言を設定。
    foreach ($pageData->pageDatas as &$row) {
        $row["release_status_name"] = getOprionItemValue($release_status, $row["release_status"]);
    }

    // 取得結果を返す。
    return $pageData;
}


/**
 * 更新処理を実行する。
 *
 * @param array 更新ID、ソートの配列
 * 
 * @return boolean 実行成否
 */
function seq_update($data_arr_upd) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {

        for($iLoop=0;$iLoop<count($data_arr_upd);$iLoop++){

            // カラムと設定値を定義。
            $columns = array(
                "sequence"              => $data_arr_upd[$iLoop][1],      //ステータス
            );

            // 同時に、SQLパラメータの設定も行う。
            $whereSqls = array();
            $params = array();
            getWhereParamsUPD($data_arr_upd[$iLoop][0], $whereSqls, $params);
    
            $whereSqlString  ="";
    
            if (count($whereSqls) > 0) {
                $whereSqlString = $db->getWhereSql($whereSqls);
            }
    
            // update処理を発行する。
            $db->update("tbl_qa_info", $columns, $whereSqlString, $params);
    
        }

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $qa_id     QAデータのレコードID
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParamsUPD($qa_id, &$whereSqls, &$params) {
    $where = "";

    $where = "id=:qa_id ";
    $params[":qa_id"] = $qa_id;
    $whereSqls[] = $where;

}

/**
 * CSV出力用データを取得する。
 *
 * @param $items  検索条件
 * 
 * @return PageData 取得結果
 */
function getExportDatas($items) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    // SQL文を生成。
    $sql = "SELECT "
         .     "qa.question_no, "
         .     "qa.company_id, "
         .     "qa.sequence, "
         .     "qa.release_status, "
         .     "qa.delivery_language, "
         .     "qa.question_contents, "
         .     "qa.answer_contents "
         . "FROM tbl_qa_info qa "
    ;
    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY qa.sequence ";

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}


/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $items     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($items, &$whereSqls, &$params) {
    // 質問内容
    if ($items["C_01"] != "") {
        // 部分一致条件を設定。
        $whereSqls[] = "qa.question_contents like :question_contents ";
        $params[":question_contents"] = "%".$items["C_01"]."%";
    }

    // 回答内容
    if ($items["C_02"] != "") {
        // 部分一致条件を設定。
        $whereSqls[] = "qa.answer_contents LIKE :answer_contents ";
        $params[":answer_contents"] = "%".$items["C_02"]."%";
    }

    // 企業名
    if ($items["C_03"] != "") {
        $whereSqls[] = "qa.company_id = :company_id ";
        $params[":company_id"] = $items["C_03"];
    }

    // ・登録日
    if ($items["C_04_1"] != "") {
        $whereSqls[] = "qa.create_date >= STR_TO_DATE(:create_date_from, '%Y-%m-%d') ";
        $params[":create_date_from"] = $items["C_04_1"];
    }
    if ($items["C_04_2"] != "") {
        $whereSqls[] = "qa.create_date < DATE_ADD(STR_TO_DATE(:create_date_to, '%Y-%m-%d'), INTERVAL 1 DAY) ";
        $params[":create_date_to"] = $items["C_04_2"];
    }

    // 公開状況
    if ($items["C_05_1"].$items["C_05_2"].$items["C_05_3"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c5Params = array();
        if ($items["C_05_1"] != "") $c5Params[] = "'1'";
        if ($items["C_05_2"] != "") $c5Params[] = "'2'";
        if ($items["C_05_3"] != "") $c5Params[] = "'3'";
        $c5ParamsString = joinArray($c5Params);
        $whereSqls[] = "qa.release_status IN (".$c5ParamsString.") ";
    }

}


/**
 * CSVデータを登録する。
 *
 * @param array[レコード => カラム] $datas CSVデータ
 * 
 * @return boolean 実行成否
 */
function csvDataImport($datas) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        foreach ($datas as $row) {
            // 列内容に対応する登録処理を判定。
            $existRowdata = !isEmptyRecord($row, COL_QA_NO + 1);
            if ($existRowdata) {
                $sql = "SELECT COUNT(*) AS cnt "
                     . "FROM tbl_qa_info "
                     . "WHERE question_no = :question_no ";
                $params[":question_no"] = $row[COL_QA_NO];
                $result = $db->selectOne($sql, $params);
                $noMatchRecord = ($result["cnt"] == 0);
                if ($noMatchRecord) {
                    // 行データあり&既存データなし : INSERT
                    csvInsert($db, $row);
                } else {
                    // 行データあり&既存データあり : UPDATE
                    csvUpdate($db, $row);
                }
            } else {
                // 行データなし : DELETE
                csvDelete($db, $row);
            }
        }

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}


/**
 * データを新規登録する。
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 * 
 * @return integer レコード件数
 */
function csvInsert($db, $row) {
    //-----------------------------------------------
    // QAにデータを新規作成する。
    //-----------------------------------------------

    // カラムと設定値を定義。
    $columns = array();
    $columns["question_no" ] = $row[COL_QA_NO];
    setColumns($columns, $db, $row);

    // insert処理を発行する。
    $rc = $db->insert("tbl_qa_info", $columns);

    return $rc;
}

/**
 * データを更新する。
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 * 
 * @return integer レコード件数
 */
function csvUpdate($db, $row) {
    //-----------------------------------------------
    // QAデータを更新する。
    //-----------------------------------------------

    // カラムと設定値を定義。
    $columns = array();
    setColumns($columns, $db, $row);

    $whereString = "question_no = :question_no ";
    $whereParams = array(
        "question_no" => $row[COL_QA_NO]
    );

    // update処理を発行する。
    $rc = $db->update("tbl_qa_info", $columns, $whereString, $whereParams);
    return $rc;
}

/**
 * データを削除する。
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 * 
 * @return integer レコード件数
 */
function csvDelete($db, $row) {
    // 削除対象のレコードのIDを取得する。
    $QARowId = getQARowId($row[COL_QA_NO]);

    //-----------------------------------------------
    // QAデータを削除する。
    //-----------------------------------------------
    $whereString = "id = :qaid ";
    $whereParams = array(
        "qaid" => $QARowId
    );

    // delete処理を発行する。
    $rc = $db->delete("tbl_qa_info", $whereString, $whereParams);


    return $rc;
}

/**
 * QAデータの行IDを取得する。
 *
 * @param string $workerId 取得対象の労働者ID
 * 
 * @return integer 行ID
 */
function getQARowId($QA_No) {
    $db = new JinzaiDb(DB_DEFINE);

    $sql = "SELECT id AS rowid "
            . "FROM tbl_qa_info "
            . "WHERE question_no = :question_no "
    ;
    $params[":question_no"] = $QA_No;
    
    $result = $db->selectOne($sql, $params);

    if ($result != null) {
        return $result["rowid"];
    } else {
        return null;
    }
}


/**
 * CSVレコードの内容から、更新用データを生成する。
 *
 * @param array[DBカラム名 => 値] $columns 更新用カラム一覧
 * @param array[値] $row CSV行データ
 */
function setColumns(&$columns, $db, $row) {
    // 企業の行IDを取得。
    $companyRowId = $db->getCompanyRowId($row[COL_COMPANY_ID]);

    // レコードの内容から更新用データを生成。
    $columns["question_no"      ] = $row[COL_QA_NO];
    $columns["company_id"       ] = $companyRowId;
    $rowid = COL_COMPANY_ID + 1;
    $columns["sequence"         ] = $row[$rowid++];
    $columns["release_status"   ] = $row[$rowid++];
    $columns["delivery_language"] = $row[$rowid++];
    $columns["question_contents"] = $row[$rowid++];
    $columns["answer_contents"  ] = $row[$rowid++];

    // デフォルト値を設定。
    if ($columns["release_status"    ] == "") $columns["release_status"    ] = "2"; // 2=作成中
    if ($columns["delivery_language" ] == "") $columns["delivery_language" ] = "1"; // 1=日本語
}

/**
 * 受入企業の一覧を取得する。
 *
 * @return array[行データ][キー => 値] 取得結果
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "WHERE classification = '1' "  // 受入機関
         . "ORDER BY company_name ";

    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}
