<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $qa_id  検索条件
 * 
 * @return PageData 検索結果
 */
function search($qa_id) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($qa_id, $whereSqls, $params);

    // SQL文を生成。
    $sql = "SELECT "
         .     "qa.id as qa_id, "
         .     "qa.question_no, "
         .     "qa.sequence, "
         .     "qa.release_status, "
         .     "qa.delivery_language, "
         .     "qa.question_contents, "
         .     "qa.answer_contents, "
         .     "qa.company_id as cp_id, "
         .     "companies.company_name, "
         .     "adm1.user_name as cre_user, "
         .     "DATE_FORMAT(qa.create_date, '%Y/%m/%d') AS qa_create_date, "
         .     "adm2.user_name as upd_user, "
         .     "DATE_FORMAT(qa.update_date, '%Y/%m/%d') AS qa_update_date "
         . "FROM tbl_qa_info qa "
         . "LEFT JOIN mst_company companies "
         .     "ON qa.company_id = companies.id "
         ."LEFT JOIN mst_admin_user adm1 ON adm1.id = qa.create_id "
         ."LEFT JOIN mst_admin_user adm2 ON adm2.id = qa.update_id "
         ;
    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }

    // SQL文を発行する。
    $rowsData = $db->selectOne($sql, $params);


    // 表示用データを追加設定。
    $release_status = getOptionItems("comm", "release_status");

    // コードに対応する文言を設定。
    $rowsData["release_status_name"] = getOprionItemValue($release_status, $rowsData["release_status"]);


    // 取得結果を返す。
    return $rowsData;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $qa_id     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($qa_id, &$whereSqls, &$params) {
    //ID群を分解し、or条件にする

    $where = "";

    $where = "qa.id=:qa_id ";

    $params[":qa_id"] = $qa_id;

    $whereSqls[0] = $where;

}

/**
 * 更新条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $qa_id     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParamsUpd($qa_id, &$whereSqls, &$params) {
    //ID群を分解し、or条件にする

    $where = "";

    $where = "id=:qa_id ";

    $params[":qa_id"] = $qa_id;

    $whereSqls[0] = $where;

}


/**
 * 更新処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function update($qa_id,$items) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {

        // 同時に、SQLパラメータの設定も行う。
        $whereSqls = array();
        $params = array();
        getWhereParamsUpd($qa_id, $whereSqls, $params);
        
        $whereSqlString  ="";

        if (count($whereSqls) > 0) {
            $whereSqlString = $db->getWhereSql($whereSqls);
        }
        
        // カラムと設定値を定義。
        $columns = array(
            "question_no"              => $items["question_no"],      //ステータス
            "question_contents"              => $items["question_contents"],      //ステータス
            "answer_contents"              => $items["answer_contents"],      //ステータス
        );

        //無ければ触らない処理（安全策）
        if($items["c_name"]!=""){
            $columns["company_id"] = $items["c_name"];    //入力期限
        }
        if($items["disp_lang"]!=""){
            $columns["delivery_language"] = $items["disp_lang"];    //入力期限
        }
        if($items["release_status"]!=""){
            $columns["release_status"] = $items["release_status"];    //入力期限
        }

        // update処理を発行する。
        $db->update("tbl_qa_info", $columns, $whereSqlString, $params);
    

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}

/**
 * 受入企業の一覧を取得する。
 *
 * @return array[行データ][キー => 値] 取得結果
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "WHERE classification = '1' "  // 受入機関
         . "ORDER BY company_name ";

    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}
