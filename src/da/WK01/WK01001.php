<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * @param $pageNo ページ番号
 * 
 * @return PageData 検索結果
 */
function search($items, $pageNo) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    // SQL文を生成。
    $sql = "SELECT "
         .     "workers.worker_id, "
         .     "workers.user_name, "
         .     "workers.qualifications, "
         .     "companies.company_name, "
         .     "DATE_FORMAT(workers.create_date, '%Y/%m/%d') AS create_date_ymd "
         . "FROM mst_workers workers "
         . "LEFT JOIN mst_company companies "
         .     "ON workers.company_id = companies.id "
    ;
    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY workers.worker_id ";

    // SQL文を発行する。
    $pageData = $db->getPageData($sql, $params, $pageNo);

    // 表示用データを追加設定。
    $qualification = getOptionItems("comm", "qualification");

    // コードに対応する文言を設定。
    foreach ($pageData->pageDatas as &$row) {
        $row["qualifications_name"] = getOprionItemValue($qualification, $row["qualifications"]);
    }

    // 取得結果を返す。
    return $pageData;
}

/**
 * CSV出力用データを取得する。
 *
 * @param $items  検索条件
 * 
 * @return PageData 取得結果
 */
function getExportDatas($items) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    // SQL文を生成。
    $sql = "SELECT "
         .     "workers.worker_id, "
         .     "companies.company_id, "
         .     "workers.disp_lang, "
         .     "workers.qualifications, "
         .     "workers.qualifications_etc, "
         .     "workers.status, "
         .     "workers.nationality_region, "
         .     "DATE_FORMAT(workers.birthday, '%Y/%m/%d') AS birthday_ymd, "
         .     "workers.user_name, "
         .     "workers.user_name_e, "
         .     "workers.family_name, "
         .     "workers.given_name, "
         .     "workers.sex, "
         .     "workers.place_birth, "
         .     "workers.marital_flag, "
         .     "workers.occupation, "
         .     "workers.home_town, "
         .     "workers.postal_code, "
         .     "workers.address, "
         .     "workers.telephone, "
         .     "workers.cellular_phone, "
         .     "A_01_1, "
         .     "DATE_FORMAT(A_01_2, '%Y/%m/%d') AS A_01_2_ymd, "
         .     "A_02_1, "
         .     "A_02_2, "
         .     "DATE_FORMAT(A_03, '%Y/%m/%d') AS A_03_ymd, "
         .     "A_04, "
         .     "A_05, "
         .     "A_06, "
         .     "A_07, "
         .     "A_08_1, "
         .     "A_08_2, "
         .     "DATE_FORMAT(A_08_3, '%Y/%m/%d') AS A_08_3_ymd, "
         .     "DATE_FORMAT(A_08_4, '%Y/%m/%d') AS A_08_4_ymd, "
         .     "A_15_1, "
         .     "A_15_2, "
         .     "A_16_1, "
         .     "A_17_1, "
         .     "DATE_FORMAT(A_17_2, '%Y/%m/%d') AS A_17_2_ymd, "
         .     "A_18_1, "
         .     "A_19_1_1, "
         .     "A_19_1_2, "
         .     "DATE_FORMAT(A_19_1_3, '%Y/%m/%d') AS A_19_1_3_ymd, "
         .     "A_19_1_4, "
         .     "A_19_1_5, "
         .     "A_19_1_6, "
         .     "A_19_1_7, "
         .     "A_19_2_1, "
         .     "A_19_2_2, "
         .     "DATE_FORMAT(A_19_2_3, '%Y/%m/%d') AS A_19_2_3_ymd, "
         .     "A_19_2_4, "
         .     "A_19_2_5, "
         .     "A_19_2_6, "
         .     "A_19_2_7, "
         .     "A_19_3_1, "
         .     "A_19_3_2, "
         .     "DATE_FORMAT(A_19_3_3, '%Y/%m/%d') AS A_19_3_3_ymd, "
         .     "A_19_3_4, "
         .     "A_19_3_5, "
         .     "A_19_3_6, "
         .     "A_19_3_7, "
         .     "A_19_4_1, "
         .     "A_19_4_2, "
         .     "DATE_FORMAT(A_19_4_3, '%Y/%m/%d') AS A_19_4_3_ymd, "
         .     "A_19_4_4, "
         .     "A_19_4_5, "
         .     "A_19_4_6, "
         .     "A_19_4_7, "
         .     "A_19_5_1, "
         .     "A_19_5_2, "
         .     "DATE_FORMAT(A_19_5_3, '%Y/%m/%d') AS A_19_5_3_ymd, "
         .     "A_19_5_4, "
         .     "A_19_5_5, "
         .     "A_19_5_6, "
         .     "A_19_5_7, "
         .     "A_19_6_1, "
         .     "A_19_6_2, "
         .     "DATE_FORMAT(A_19_6_3, '%Y/%m/%d') AS A_19_6_3_ymd, "
         .     "A_19_6_4, "
         .     "A_19_6_5, "
         .     "A_19_6_6, "
         .     "A_19_6_7, "
         .     "A_20_1, "
         .     "A_20_2, "
         .     "A_20_3, "
         .     "A_21_1, "
         .     "A_21_2, "
         .     "A_21_3, "
         .     "A_21_4, "
         .     "A_21_5, "
         .     "A_21_6, "
         .     "A_21_7, "
         .     "A_22_1, "
         .     "A_22_2, "
         .     "A_22_3, "
         .     "A_22_4, "
         .     "A_22_5, "
         .     "A_22_6, "
         .     "A_22_7, "
         .     "A_23_1_1, "
         .     "A_23_1_2, "
         .     "A_23_1_3, "
         .     "A_23_2_1, "
         .     "A_23_2_2, "
         .     "A_23_2_3, "
         .     "A_24_1, "
         .     "A_24_2, "
         .     "A_25_1, "
         .     "A_25_2, "
         .     "A_25_3, "
         .     "A_26_1, "
         .     "A_26_2, "
         .     "A_26_3, "
         .     "A_27, "
         .     "A_28, "
         .     "A_29, "
         .     "A_30, "
         .     "DATE_FORMAT(A_31_1_1, '%Y/%m') AS A_31_1_1_ymd, "
         .     "DATE_FORMAT(A_31_1_2, '%Y/%m') AS A_31_1_2_ymd, "
         .     "A_31_1_3, "
         .     "DATE_FORMAT(A_31_2_1, '%Y/%m') AS A_31_2_1_ymd, "
         .     "DATE_FORMAT(A_31_2_2, '%Y/%m') AS A_31_2_2_ymd, "
         .     "A_31_2_3, "
         .     "DATE_FORMAT(A_31_3_1, '%Y/%m') AS A_31_3_1_ymd, "
         .     "DATE_FORMAT(A_31_3_2, '%Y/%m') AS A_31_3_2_ymd, "
         .     "A_31_3_3, "
         .     "DATE_FORMAT(A_31_4_1, '%Y/%m') AS A_31_4_1_ymd, "
         .     "DATE_FORMAT(A_31_4_2, '%Y/%m') AS A_31_4_2_ymd, "
         .     "A_31_4_3, "
         .     "DATE_FORMAT(A_31_5_1, '%Y/%m') AS A_31_5_1_ymd, "
         .     "DATE_FORMAT(A_31_5_2, '%Y/%m') AS A_31_5_2_ymd, "
         .     "A_31_5_3, "
         .     "DATE_FORMAT(A_31_6_1, '%Y/%m') AS A_31_6_1_ymd, "
         .     "DATE_FORMAT(A_31_6_2, '%Y/%m') AS A_31_6_2_ymd, "
         .     "A_31_6_3, "
         .     "A_32, "
         .     "A_33, "
         .     "A_34, "
         .     "A_35, "
         .     "A_36, "
         .     "A_37, "
         .     "A_38, "
         .     "A_39, "
         .     "A_40, "
         .     "A_41 "
         . "FROM mst_workers workers "
         . "LEFT JOIN mst_company companies "
         .     "ON workers.company_id = companies.id "
    ;
    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY workers.worker_id ";

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}


/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $items     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($items, &$whereSqls, &$params) {
    // ・労働者ID
    if ($items["C_01"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "workers.worker_id = :worker_id ";
        $params[":worker_id"] = $items["C_01"];
    }

    // ・労働者氏名
    if ($items["C_02"] != "") {
        // 労働者姓/名に対して部分一致条件を設定。
        $whereSqls[] = "workers.user_name LIKE :worker_name ";
        $params[":worker_name"] = "%".$items["C_02"]."%";
    }

    // ・在留資格
    if ($items["C_03_1"].$items["C_03_2"].$items["C_03_3"].$items["C_03_4"].$items["C_03_5"].$items["C_03_6"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c3Params = array();
        if ($items["C_03_1"] != "") $c3Params[] = "'1'";
        if ($items["C_03_2"] != "") $c3Params[] = "'2'";
        if ($items["C_03_3"] != "") $c3Params[] = "'3'";
        if ($items["C_03_4"] != "") $c3Params[] = "'4'";
        if ($items["C_03_5"] != "") $c3Params[] = "'5'";
        if ($items["C_03_6"] != "") $c3Params[] = "'6'";
        $c3ParamsString = joinArray($c3Params);
        $whereSqls[] = "workers.qualifications IN (".$c3ParamsString.") ";
    }
    // ・在留資格その他
    if ($items["C_03_6"] != "" and $items["C_03_etc"] != "") {
        $whereSqls[] = "workers.qualifications_etc = :C_03_etc ";
        $params[":C_03_etc"] = $items["C_03_etc"];
    }

    // ・ステータス
    if ($items["C_04_1"].$items["C_04_2"].$items["C_04_3"].$items["C_04_4"].$items["C_04_5"].$items["C_04_6"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c4Params = array();
        if ($items["C_04_1"] != "") $c4Params[] = "'1'";
        if ($items["C_04_2"] != "") $c4Params[] = "'2'";
        if ($items["C_04_3"] != "") $c4Params[] = "'3'";
        if ($items["C_04_4"] != "") $c4Params[] = "'4'";
        if ($items["C_04_5"] != "") $c4Params[] = "'5'";
        if ($items["C_04_6"] != "") $c4Params[] = "'6'";
        $c4ParamsString = joinArray($c4Params);
        $whereSqls[] = "workers.status IN (".$c4ParamsString.") ";
    }

    // ・性別
    if ($items["C_05_1"].$items["C_05_2"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c5Params = array();
        if ($items["C_05_1"] != "") $c5Params[] = "'1'";
        if ($items["C_05_2"] != "") $c5Params[] = "'2'";
        $c5ParamsString = joinArray($c5Params);
        $whereSqls[] = "workers.sex IN (".$c5ParamsString.") ";
    }

    // ・企業名
    if ($items["C_06"] != "") {
        $whereSqls[] = "workers.company_id = :company_id ";
        $params[":company_id"] = $items["C_06"];
    }

    // ・登録日
    if ($items["C_07_1"] != "") {
        $whereSqls[] = "workers.create_date >= STR_TO_DATE(:create_date_from, '%Y-%m-%d') ";
        $params[":create_date_from"] = $items["C_07_1"];
    }
    if ($items["C_07_2"] != "") {
        $whereSqls[] = "workers.create_date < DATE_ADD(STR_TO_DATE(:create_date_to, '%Y-%m-%d'), INTERVAL 1 DAY) ";
        $params[":create_date_to"] = $items["C_07_2"];
    }

    // ・在留期間
    if ($items["C_08_1"] != "") {
        $whereSqls[] = "workers.A_09_3 >= STR_TO_DATE(:A_09_3_from, '%Y-%m-%d') ";
        $params[":A_09_3_from"] = $items["C_08_1"];
    }
    if ($items["C_08_2"] != "") {
        $whereSqls[] = "workers.A_09_3 < DATE_ADD(STR_TO_DATE(:A_09_3_to, '%Y-%m-%d'), INTERVAL 1 DAY) ";
        $params[":A_09_3_to"] = $items["C_08_2"];
    }
}


/**
 * CSVデータを登録する。
 *
 * @param array[レコード => カラム] $datas CSVデータ
 * 
 * @return boolean 実行成否
 */
function csvDataImport($datas) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        foreach ($datas as $row) {
            // 列内容に対応する登録処理を判定。
            $existRowdata = !isEmptyRecord($row, COL_MAILADDRESS + 1);
            if ($existRowdata) {
                $sql = "SELECT COUNT(*) AS cnt "
                     . "FROM mst_workers "
                     . "WHERE worker_id = :worker_id ";
                $params[":worker_id"] = $row[COL_MAILADDRESS];
                $result = $db->selectOne($sql, $params);
                $noMatchRecord = ($result["cnt"] == 0);
                if ($noMatchRecord) {
                    // 行データあり&既存データなし : INSERT
                    csvInsert($db, $row);
                } else {
                    // 行データあり&既存データあり : UPDATE
                    csvUpdate($db, $row);
                }
            } else {
                // 行データなし : DELETE
                csvDelete($db, $row);
            }
        }

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}


/**
 * データを新規登録する。
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 * 
 * @return integer レコード件数
 */
function csvInsert($db, $row) {
    //-----------------------------------------------
    // 労働者マスタにデータを新規作成する。
    //-----------------------------------------------
    // ランダムな値でパスワード設定値を生成。
    $passString = getRandomString();
    $passValue = password_hash($passString, PASSWORD_DEFAULT);

    // カラムと設定値を定義。
    $columns = array();
    $columns["worker_id" ] = $row[COL_MAILADDRESS];
    $columns["password"  ] = $passValue;
    setColumns($columns, $db, $row);

    // insert処理を発行する。
    $rc = $db->insert("mst_workers", $columns);

    // 労働者マスタに作成したレコードのIDを取得する。
    $workerRowId = $db->getWorkerRowId($row[COL_MAILADDRESS]);

    //-----------------------------------------------
    // 各種関連データを新規作成する。
    //-----------------------------------------------
    $rcolumns = array(
        "worker_id" => $workerRowId
    );

    //・帳票ステータス
    for ($i = 1; $i <= 10; $i++) {
        $stsColumns = array(
            "user_type" => "2",     // 2:労働者
            "user_id" => $workerRowId,
            "report_id" => sprintf('%02d', $i),
            "status" => "1"
        );
        $rc += $db->insert("tbl_report_status", $stsColumns);
    }

    //・在留資格認定証明書
    $rc += $db->insert("tbl_report_001", $rcolumns);
    //・支援計画書
    $rc += $db->insert("tbl_report_002", $rcolumns);
    //・支援実施状況に係る届出
    $rc += $db->insert("tbl_report_003", $rcolumns);
    //・事前ガイダンスの確認書
    $rc += $db->insert("tbl_report_004", $rcolumns);
    //・生活オリエンテーションの確認書
    $rc += $db->insert("tbl_report_005", $rcolumns);
    //・相談記録書
    // ※集計時期に都度作成するので、ここでの用意は不要。
    //・定期面談報告書（１号特定技能外国人用）
    $rc += $db->insert("tbl_report_007", $rcolumns);
    //・履歴書
    $rc += $db->insert("tbl_report_009", $rcolumns);

    //-----------------------------------------------
    // 登録完了メールを送信する。
    //-----------------------------------------------
    sendmailStartup($row[COL_MAILADDRESS]);
    sendmailPassword($row[COL_MAILADDRESS], $passString);
   
    return $rc;
}

/**
 * データを更新する。
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 * 
 * @return integer レコード件数
 */
function csvUpdate($db, $row) {
    //-----------------------------------------------
    // 労働者マスタデータを更新する。
    //-----------------------------------------------

    // カラムと設定値を定義。
    $columns = array();
    setColumns($columns, $db, $row);

    $whereString = "worker_id = :worker_id ";
    $whereParams = array(
        "worker_id" => $row[COL_MAILADDRESS]
    );

    // update処理を発行する。
    $rc = $db->update("mst_workers", $columns, $whereString, $whereParams);
    return $rc;
}

/**
 * データを削除する。
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 * 
 * @return integer レコード件数
 */
function csvDelete($db, $row) {
    // 削除対象のレコードのIDを取得する。
    $workerRowId = $db->getWorkerRowId($row[COL_MAILADDRESS]);

    //-----------------------------------------------
    // 労働者マスタデータを削除する。
    //-----------------------------------------------
    $whereString = "worker_id = :worker_id ";
    $whereParams = array(
        "worker_id" => $row[COL_MAILADDRESS]
    );
    $stsWhereParams = array(
        "user_id" => $row[COL_MAILADDRESS]
    );
    // delete処理を発行する。
    $rc = $db->delete("mst_workers", $whereString, $whereParams);

    //-----------------------------------------------
    // 各種関連データを削除する。
    //-----------------------------------------------
    $rcolumns = array(
        "worker_id" => $workerRowId
    );
    $stsWhereString = "user_id = :worker_id ";

    //・帳票ステータス
    $rc += $db->delete("tbl_report_status", $stsWhereString, $rcolumns);
    //・在留資格認定証明書
    $rc += $db->delete("tbl_report_001", $whereString, $rcolumns);
    //・支援計画書
    $rc += $db->delete("tbl_report_002", $whereString, $rcolumns);
    //・支援実施状況に係る届出
    $rc += $db->delete("tbl_report_003", $whereString, $rcolumns);
    //・事前ガイダンスの確認書
    $rc += $db->delete("tbl_report_004", $whereString, $rcolumns);
    //・生活オリエンテーションの確認書
    $rc += $db->delete("tbl_report_005", $whereString, $rcolumns);
    //・相談記録書
    $rc += $db->delete("tbl_report_006", $whereString, $rcolumns);
    //・定期面談報告書（１号特定技能外国人用）
    $rc += $db->delete("tbl_report_007", $whereString, $rcolumns);
    //・履歴書
    $rc += $db->delete("tbl_report_009", $whereString, $rcolumns);

    return $rc;
}

/**
 * CSVレコードの内容から、更新用データを生成する。
 *
 * @param array[DBカラム名 => 値] $columns 更新用カラム一覧
 * @param array[値] $row CSV行データ
 */
function setColumns(&$columns, $db, $row) {
    $rowid = 2 - 1;

    // 企業の行IDを取得。
    $companyRowId = $db->getCompanyRowId($row[$rowid++]);

    // 基本情報
    $columns["company_id"         ] = $companyRowId;
    $columns["disp_lang"          ] = $row[$rowid++];
    $columns["qualifications"     ] = $row[$rowid++];
    $columns["qualifications_etc" ] = $row[$rowid++];
    $columns["status"             ] = $row[$rowid++];
    $columns["nationality_region" ] = $row[$rowid++];
    $columns["birthday"           ] = setDateParam($row[$rowid++]);
    $columns["user_name"          ] = $row[$rowid++];
    $columns["user_name_e"        ] = $row[$rowid++];
    $columns["family_name"        ] = $row[$rowid++];
    $columns["given_name"         ] = $row[$rowid++];
    $columns["sex"                ] = $row[$rowid++];
    $columns["place_birth"        ] = $row[$rowid++];
    $columns["marital_flag"       ] = $row[$rowid++];
    $columns["occupation"         ] = $row[$rowid++];
    $columns["home_town"          ] = $row[$rowid++];
    $columns["postal_code"        ] = $row[$rowid++];
    $columns["address"            ] = $row[$rowid++];
    $columns["telephone"          ] = $row[$rowid++];
    $columns["cellular_phone"     ] = $row[$rowid++];

    $columns["A_01_1"   ] = $row[$rowid++];
    $columns["A_01_2"   ] = setDateParam($row[$rowid++], "/01");
    $columns["A_02_1"   ] = $row[$rowid++];
    $columns["A_02_2"   ] = $row[$rowid++];
    $columns["A_03"     ] = setDateParam($row[$rowid++]);
    $columns["A_04"     ] = $row[$rowid++];
    $columns["A_05"     ] = $row[$rowid++];
    $columns["A_06"     ] = $row[$rowid++];
    $columns["A_07"     ] = $row[$rowid++];
    $columns["A_08_1"   ] = $row[$rowid++];
    $columns["A_08_2"   ] = $row[$rowid++];
    $columns["A_08_3"   ] = setDateParam($row[$rowid++]);
    $columns["A_08_4"   ] = setDateParam($row[$rowid++]);
    
    // 09-14:証明書変更/更新に関する項目は不要。

    // 15-17:退去強制等
    $columns["A_15_1"   ] = $row[$rowid++];
    $columns["A_15_2"   ] = $row[$rowid++];
    $columns["A_16_1"   ] = $row[$rowid++];
    // 16_2:退去強制又は出国命令による出国の内容は設定しない。
    $columns["A_17_1"   ] = $row[$rowid++];
    $columns["A_17_2"   ] = setDateParam($row[$rowid++]);

    // 18-19:在日親族及び同居者
    $columns["A_18_1"   ] = $row[$rowid++];
    $columns["A_19_1_1" ] = $row[$rowid++];
    $columns["A_19_1_2" ] = $row[$rowid++];
    $columns["A_19_1_3" ] = setDateParam($row[$rowid++]);
    $columns["A_19_1_4" ] = $row[$rowid++];
    $columns["A_19_1_5" ] = $row[$rowid++];
    $columns["A_19_1_6" ] = $row[$rowid++];
    $columns["A_19_1_7" ] = $row[$rowid++];
    $columns["A_19_2_1" ] = $row[$rowid++];
    $columns["A_19_2_2" ] = $row[$rowid++];
    $columns["A_19_2_3" ] = setDateParam($row[$rowid++]);
    $columns["A_19_2_4" ] = $row[$rowid++];
    $columns["A_19_2_5" ] = $row[$rowid++];
    $columns["A_19_2_6" ] = $row[$rowid++];
    $columns["A_19_2_7" ] = $row[$rowid++];
    $columns["A_19_3_1" ] = $row[$rowid++];
    $columns["A_19_3_2" ] = $row[$rowid++];
    $columns["A_19_3_3" ] = setDateParam($row[$rowid++]);
    $columns["A_19_3_4" ] = $row[$rowid++];
    $columns["A_19_3_5" ] = $row[$rowid++];
    $columns["A_19_3_6" ] = $row[$rowid++];
    $columns["A_19_3_7" ] = $row[$rowid++];
    $columns["A_19_4_1" ] = $row[$rowid++];
    $columns["A_19_4_2" ] = $row[$rowid++];
    $columns["A_19_4_3" ] = setDateParam($row[$rowid++]);
    $columns["A_19_4_4" ] = $row[$rowid++];
    $columns["A_19_4_5" ] = $row[$rowid++];
    $columns["A_19_4_6" ] = $row[$rowid++];
    $columns["A_19_4_7" ] = $row[$rowid++];
    $columns["A_19_5_1" ] = $row[$rowid++];
    $columns["A_19_5_2" ] = $row[$rowid++];
    $columns["A_19_5_3" ] = setDateParam($row[$rowid++]);
    $columns["A_19_5_4" ] = $row[$rowid++];
    $columns["A_19_5_5" ] = $row[$rowid++];
    $columns["A_19_5_6" ] = $row[$rowid++];
    $columns["A_19_5_7" ] = $row[$rowid++];
    $columns["A_19_6_1" ] = $row[$rowid++];
    $columns["A_19_6_2" ] = $row[$rowid++];
    $columns["A_19_6_3" ] = setDateParam($row[$rowid++]);
    $columns["A_19_6_4" ] = $row[$rowid++];
    $columns["A_19_6_5" ] = $row[$rowid++];
    $columns["A_19_6_6" ] = $row[$rowid++];
    $columns["A_19_6_7" ] = $row[$rowid++];

    // 20:特定技能所属機関ID
    $columns["A_20_1"   ] = $row[$rowid++];
    $columns["A_20_2"   ] = $row[$rowid++];
    $columns["A_20_3"   ] = $row[$rowid++];

    $str = $columns["A_20_1"   ] ." / " . $columns["A_20_2"   ] ." / " . $columns["A_20_3"   ] ;

    // 21:技能水準
    $columns["A_21_1"   ] = $row[$rowid++];
    $columns["A_21_2"   ] = $row[$rowid++];
    $columns["A_21_3"   ] = $row[$rowid++];
    $columns["A_21_4"   ] = $row[$rowid++];
    $columns["A_21_5"   ] = $row[$rowid++];
    $columns["A_21_6"   ] = $row[$rowid++];
    $columns["A_21_7"   ] = $row[$rowid++];

    // 22:日本語能力
    $columns["A_22_1"   ] = $row[$rowid++];
    $columns["A_22_2"   ] = $row[$rowid++];
    $columns["A_22_3"   ] = $row[$rowid++];
    $columns["A_22_4"   ] = $row[$rowid++];
    $columns["A_22_5"   ] = $row[$rowid++];
    $columns["A_22_6"   ] = $row[$rowid++];
    $columns["A_22_7"   ] = $row[$rowid++];

    // 23:良好に修了した技能
    $columns["A_23_1_1" ] = $row[$rowid++];
    $columns["A_23_1_2" ] = $row[$rowid++];
    $columns["A_23_1_3" ] = $row[$rowid++];
    $columns["A_23_2_1" ] = $row[$rowid++];
    $columns["A_23_2_2" ] = $row[$rowid++];
    $columns["A_23_2_3" ] = $row[$rowid++];

    // 24-30:その他いろいろ
    $columns["A_24_1"   ] = $row[$rowid++];
    $columns["A_24_2"   ] = $row[$rowid++];
    $columns["A_25_1"   ] = $row[$rowid++];
    $columns["A_25_2"   ] = $row[$rowid++];
    $columns["A_25_3"   ] = $row[$rowid++];
    $columns["A_26_1"   ] = $row[$rowid++];
    $columns["A_26_2"   ] = $row[$rowid++];
    $columns["A_26_3"   ] = $row[$rowid++];
    $columns["A_27"     ] = $row[$rowid++];
    $columns["A_28"     ] = $row[$rowid++];
    $columns["A_29"     ] = $row[$rowid++];
    $columns["A_30"     ] = $row[$rowid++];

    // 31:職歴
    $columns["A_31_1_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["A_31_1_2" ] = setDateParam($row[$rowid++], "/01");
    $columns["A_31_1_3" ] = $row[$rowid++];
    $columns["A_31_2_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["A_31_2_2" ] = setDateParam($row[$rowid++], "/01");
    $columns["A_31_2_3" ] = $row[$rowid++];
    $columns["A_31_3_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["A_31_3_2" ] = setDateParam($row[$rowid++], "/01");
    $columns["A_31_3_3" ] = $row[$rowid++];
    $columns["A_31_4_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["A_31_4_2" ] = setDateParam($row[$rowid++], "/01");
    $columns["A_31_4_3" ] = $row[$rowid++];
    $columns["A_31_5_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["A_31_5_2" ] = setDateParam($row[$rowid++], "/01");
    $columns["A_31_5_3" ] = $row[$rowid++];
    $columns["A_31_6_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["A_31_6_2" ] = setDateParam($row[$rowid++], "/01");
    $columns["A_31_6_3" ] = $row[$rowid++];

    // 32-36:代理人
    $columns["A_32"     ] = $row[$rowid++];
    $columns["A_33"     ] = $row[$rowid++];
    $columns["A_34"     ] = $row[$rowid++];
    $columns["A_35"     ] = $row[$rowid++];
    $columns["A_36"     ] = $row[$rowid++];

    // 取次者
    $columns["A_37"     ] = $row[$rowid++];
    $columns["A_38"     ] = $row[$rowid++];
    $columns["A_39"     ] = $row[$rowid++];
    $columns["A_40"     ] = $row[$rowid++];
    $columns["A_41"     ] = $row[$rowid++];
}


/**
 * 登録完了メール送信処理
 *
 * @param string $mailto 送信先
 * 
 */
function sendmailStartup($mailto) {
    // テンプレートエンジンを生成。
    $smarty = new Smarty();
    $smarty->template_dir = '../../templates/';
    $smarty->compile_dir  = '../../templates_c/';
    $smarty->config_dir   = '../../configs/';
    $smarty->cache_dir    = '../../cache/';

    // テンプレート変数をアサイン。
    $smarty->assign("mailto", $mailto);
    $smarty->assign("prog_id", "");

    // 言語モードに応じたメールテンプレートを読み込む。
    $lang = getLangValue();
    if ($lang == '2') {
        $tmplName = 'common/starter-en.tpl';
        $subject = "[Talent Asia autoreply]User ID Notification.";
    } else if ($lang == '3') {
        $tmplName = 'common/starter-vi.tpl';
        $subject = "[Talent Asia autoreply]User ID Notification.";
    } else {
        $tmplName = 'common/starter-jp.tpl';
        $subject = "[Talent Asia システム自動返信]利用IDのお知らせ";
    }

    // テンプレートからメール本文を生成。
    $body = $smarty->fetch($tmplName);

    // メールを送信する。
    $SesMailer = new ses_mailSender();
    
    $ret = $SesMailer->mailSend(
        "", //$Sender by default, 
        "", //$SenderName by default, 
        $mailto, 
        $subject, 
        $body
    );
}

/**
 * パスワード通知メール送信処理
 *
 * @param string $mailto 送信先
 * @param string $passString パスワード文字列
 * 
 */
function sendmailPassword($mailto, $passString) {
    // テンプレートエンジンを生成。
    $smarty = new Smarty();
    $smarty->template_dir = '../../templates/';
    $smarty->compile_dir  = '../../templates_c/';
    $smarty->config_dir   = '../../configs/';
    $smarty->cache_dir    = '../../cache/';

    // テンプレート変数をアサイン。
    $smarty->assign("passString", $passString);

    // 言語モードに応じたメールテンプレートを読み込む。
    $lang = getLangValue();
    if ($lang == '2') {
        $tmplName = 'common/password-en.tpl';
        $subject = "[Talent Asia autoreply]Password Notification.";
    } else if ($lang == '3') {
        $tmplName = 'common/password-vi.tpl';
        $subject = "[Talent Asia autoreply]Password Notification.";
    } else {
        $tmplName = 'common/password-jp.tpl';
        $subject = "[Talent Asia システム自動返信]パスワードのお知らせ";
    }

    // テンプレートからメール本文を生成。
    $body = $smarty->fetch($tmplName);

    // メールを送信する。
    $SesMailer = new ses_mailSender();
    
    $ret = $SesMailer->mailSend(
        "", //$Sender by default, 
        "", //$SenderName by default, 
        $mailto, 
        $subject, 
        $body
    );
}


/**
 * 受入企業の一覧を取得する。
 *
 * @return array[行データ][キー => 値] 取得結果
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "WHERE classification = '1' "  // 受入機関
         . "ORDER BY company_name ";

    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}
