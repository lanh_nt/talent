<?php

/**
 * お知らせ情報を取得し、結果を返す。
 *
 * @param $infoId お知らせID
 * 
 * @return array[] 取得結果
 */
function search($infoId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         .     "DATE_FORMAT(info_data.delivery_date, '%Y/%m/%d') AS dday, "
         .     "admins.user_name AS sender_name, "
         .     "info_data.delivery_language, "
         .     "info_data.destination_flag, "
         .     "GROUP_CONCAT( "  // 対象の労働者を、改行区切りで列挙。
         .         "DISTINCT workers.user_name "
         .         "ORDER BY workers.worker_id "
         .         "SEPARATOR '\n' "
         .     ") AS worker_name, "
         .     "info_data.subject_japanese AS subject_jp, "
         .     "info_data.subject_native AS subject_nv, "
         .     "info_data.text_japanese AS msg_body_jp, "
         .     "info_data.text_native AS msg_body_nv "
         . "FROM tbl_information info_data "
         . "LEFT JOIN tbl_information_destination info_user "
         .     "ON info_data.id = info_user.info_id "
         . "LEFT JOIN mst_workers workers "
         .     "ON info_user.worker_id = workers.id "
         . "INNER JOIN mst_admin_user admins "
         .     "ON info_data.sender = admins.id "
         . "WHERE info_data.info_id = :info_id "
         . "GROUP BY dday, sender_name, delivery_language, destination_flag, subject_jp, subject_nv, msg_body_jp, msg_body_nv "
    ;
    $params = array();
    $params[":info_id"] = $infoId;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 表示用データを追加設定。
    $langs   = getOptionItems("comm", "lang");
    $targets = getOptionItems("comm", "target_user");

    // コードに対応する文言を設定。
    $result["delivery_language_string"] = getOprionItemValue($langs, $result["delivery_language"]);

    // 〇宛先 : 対象が全員なら、「全員」の文言を宛先欄に設定。
    if ($result["destination_flag"] == "1") {
        $result["worker_name"] = $targets["1"];
    }

    // 取得結果を返す。
    return $result;
}
