<?php

/**
 * 登録処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function insert($items) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        // お知らせIDを生成。
        $newInfoId = $db->getNewId("tbl_information", "info_id", 6);

        //----------------------------------
        // お知らせ情報を追加。
        //----------------------------------
        // カラムと設定値を定義。
        $columns = array(
            "info_id"           => $newInfoId,
            "company_id"        => $items["D_3"],
            "delivery_date"     => $items["D_1"],
            "destination_flag"  => $items["D_4_1"],
            "sender"            => $_SESSION['loginUserRowId'],
            "delivery_language" => $items["D_2"],
            "subject_japanese"  => $items["D_5_1"],
            "subject_native"    => $items["D_5_2"],
            "text_japanese"     => $items["D_6_1"],
            "text_native"       => $items["D_6_2"]
        );

        // insert処理を発行する。
        $db->insert("tbl_information", $columns);

        //----------------------------------
        // お知らせ送信先を追加。
        //----------------------------------
        if ($items["D_4_1"] == "2") {
            // お知らせ情報に登録した行のIDを取得。
            $newInfoRowId = getInfoRowId($db, $newInfoId);

            foreach ($items["D_4_2"] as $workerId) {
                // カラムと設定値を定義。
                $columns = array(
                    "info_id"   => $newInfoRowId,
                    "worker_id" => $workerId
                );

                // insert処理を発行する。
                $db->insert("tbl_information_destination", $columns);
            }
        }

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    //----------------------------------
    // お知らせ通知メールを送信。
    //----------------------------------
    sendmail($db, $newInfoId, $items["D_4_1"]);

    return true;
}

/**
 * お知らせデータの行IDを取得する。
 *
 * @param JinzaiDb $db     DB接続
 * @param string   $infoId 取得対象のお知らせID
 * 
 * @return integer 行ID
 */
function getInfoRowId(&$db, $infoId) {
    $sql = "SELECT id AS rowid "
         . "FROM tbl_information "
         . "WHERE info_id = :info_id "
    ;
    $params[":info_id"] = $infoId;
    
    $result = $db->selectOne($sql, $params);

    if ($result != null) {
        return $result["rowid"];
    } else {
        return null;
    }
}

/**
 * 新着メールを送信する。
 *
 * @param JinzaiDb $db     DB接続
 * @param string   $infoId お知らせID
 * @param string   $destinationType  宛先フラグ
 * 
 * @return void
 */
function sendmail($db, $infoId, $destinationType) {
    // 送信対象者を取得。
    if ($destinationType == "1") {
        // 全員：お知らせ情報の企業に所属する労働者を抽出。
        $sql = "SELECT "
             .     "workers.worker_id AS mailto, "
             .     "workers.disp_lang AS lang_type "
             . "FROM tbl_information info "
             . "INNER JOIN mst_workers workers "
             .     "ON info.company_id = workers.company_id "
             . "WHERE info.info_id = :info_id "
        ;
    } else { //if ($destinationType == "2") {
        // 個別：お知らせ送信先に存在する労働者を抽出。
        $sql = "SELECT "
             .     "workers.worker_id AS mailto, "
             .     "workers.disp_lang AS lang_type "
             . "FROM tbl_information info "
             . "INNER JOIN tbl_information_destination dest "
             .     "ON info.id = dest.info_id "
             . "INNER JOIN mst_workers workers "
             .     "ON dest.worker_id = workers.id "
             . "WHERE info.info_id = :info_id "
        ;
    }
    $params = array(
        ":info_id" => $infoId
    );
    $result = $db->select($sql, $params);

    $sesMailer = new ses_mailSender();
    $mailDatas = array();
    foreach ($result as $row) {
        // メール内容を取得。
        if (!isset($mailDatas[$row["lang_type"]])) {
            $mailDatas[$row["lang_type"]] = getMailData($row["lang_type"]);
        }
        $mailData = $mailDatas[$row["lang_type"]];

        // メールを送信する。
        $ret = $sesMailer->mailSend(
            "", //$Sender by default, 
            "", //$SenderName by default, 
            $row["mailto"], 
            $mailData["subject"], 
            $mailData["body"]
        );
    }
}

/**
 * メール内容を取得。
 *
 * @param string $langType 言語フラグ
 * 
 * @return array メール情報
 */
function getMailData($langType) {
    $mailData = array();

    // テンプレートエンジンを生成。
    $smarty = new Smarty();
    $smarty->template_dir = '../../templates/';
    $smarty->compile_dir  = '../../templates_c/';
    $smarty->config_dir   = '../../configs/';
    $smarty->cache_dir    = '../../cache/';

    // テンプレート変数をアサイン。
    // $smarty->assign("mailto", $mailto);

    // 言語モードに応じたメールテンプレートを読み込む。
    if ($langType == '2') {
        $tmplName = 'IF01/newinfo-en.tpl';
        $mailData["subject"] = "[Talent Asia autoreply]There was a news.";
    } else if ($langType == '3') {
        $tmplName = 'IF01/newinfo-vi.tpl';
        $mailData["subject"] = "[Talent Asia autoreply]Có một tin tức.";
    } else {
        $tmplName = 'IF01/newinfo-jp.tpl';
        $mailData["subject"] = "[Talent Asia システム自動返信]お知らせがありました。";
    }

    // テンプレートからメール本文を生成。
    $mailData["body"] = $smarty->fetch($tmplName);

    return $mailData;
}

/**
 * 受入企業の一覧を取得する。
 *
 * @return void
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "WHERE classification = '1' "  // 受入機関
         . "ORDER BY company_name ";

    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}

/**
 * 企業に所属する労働者の一覧を返す。
 *
 * @param $companyId 企業ID
 * 
 * @return array[] 取得結果
 */
function getWorkers($companyId) {
    if ($companyId == "") return array();

    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         .     "id, "
         .     "user_name AS name "
         . "FROM mst_workers "
         . "WHERE company_id = :company_id "
         . "ORDER BY user_name "
    ;
    $params = array(
        ":company_id" => $companyId
    );

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}
