<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * @param $pageNo ページ番号
 * 
 * @return PageData 検索結果
 */
function search($items, $pageNo) {
    $db = new JinzaiDb(DB_DEFINE);

    // 「全員」の文言を取得。
    $targets = $db->getLabels("comm", "target_user", $_SESSION['loginUserLang']);
    $targetAll = $targets["1"];

    // 検索条件に従ったWHERE句を生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    if (count($whereSqls) > 0) {
        $whereSqlString = "AND ".$db->getWhereSql($whereSqls);
    }

    // SQL文を生成。
    $sqlAll = "SELECT "
         .     "info_data.info_id AS info_id, "
         .     "DATE_FORMAT(info_data.delivery_date, '%Y/%m/%d') AS dday, "
         .     "'".$targetAll."' AS worker_name, "
         .     "info_data.subject_japanese AS subject, "
         .     "info_data.text_japanese AS msg_body, "
         .     "admins.user_name AS sender_name "
         . "FROM tbl_information info_data "
         . "INNER JOIN mst_company companies "
         .     "ON info_data.company_id = companies.id "
         . "INNER JOIN mst_workers workers "
         .     "ON companies.id = workers.company_id "
         . "INNER JOIN mst_admin_user admins "
         .     "ON info_data.sender = admins.id "
         . "WHERE info_data.destination_flag = '1' "
         .  $whereSqlString
         . "GROUP BY info_id, dday, subject, msg_body, sender_name "
    ;
    $sqlSep = "SELECT "
         .     "info_data.info_id AS info_id, "
         .     "DATE_FORMAT(info_data.delivery_date, '%Y/%m/%d') AS dday, "
         .     "GROUP_CONCAT( "  // 対象の労働者を、改行区切りで列挙。
         .         "DISTINCT workers.user_name "
         .         "ORDER BY workers.worker_id "
         .         "SEPARATOR '\n' "
         .     ") AS worker_name, "
         .     "info_data.subject_japanese AS subject, "
         .     "info_data.text_japanese AS msg_body, "
         .     "admins.user_name AS sender_name "
         . "FROM tbl_information info_data "
         . "INNER JOIN tbl_information_destination info_user "
         .     "ON info_data.id = info_user.info_id "
         . "INNER JOIN mst_workers workers "
         .     "ON info_user.worker_id = workers.id "
         . "INNER JOIN mst_company companies "
         .     "ON info_data.company_id = companies.id "
         . "INNER JOIN mst_admin_user admins "
         .     "ON info_data.sender = admins.id "
         . "WHERE info_data.destination_flag = '2' "
         .  $whereSqlString
         . "GROUP BY info_id, dday, subject, msg_body, sender_name "
    ;
    $sql = "SELECT * "
         . "FROM ( "
         .     $sqlAll
         .     "UNION "
         .     $sqlSep
         . ") AS datas "
         . "ORDER BY info_id DESC "
    ;

    // SQL文を発行する。
    $pageData = $db->getPageData($sql, $params, $pageNo);

    // 取得結果を返す。
    return $pageData;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $items     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($items, &$whereSqls, &$params) {
    // ・労働者ID
    if ($items["C_01"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "workers.worker_id = :worker_id ";
        $params[":worker_id"] = $items["C_01"];
    }

    // ・労働者氏名
    if ($items["C_02"] != "") {
        // 労働者名に対して部分一致条件を設定。
        $whereSqls[] = "workers.user_name LIKE :worker_name ";
        $params[":worker_name"] = "%".$items["C_02"]."%";
    }

    // ・区分(在留資格)
    if ($items["C_03_1"].$items["C_03_2"].$items["C_03_3"].$items["C_03_4"].$items["C_03_5"].$items["C_03_6"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c3Params = array();
        if ($items["C_03_1"] != "") $c3Params[] = "'1'";
        if ($items["C_03_2"] != "") $c3Params[] = "'2'";
        if ($items["C_03_3"] != "") $c3Params[] = "'3'";
        if ($items["C_03_4"] != "") $c3Params[] = "'4'";
        if ($items["C_03_5"] != "") $c3Params[] = "'5'";
        if ($items["C_03_6"] != "") $c3Params[] = "'6'";
        $c3ParamsString = joinArray($c3Params);
        $whereSqls[] = "workers.qualifications IN (".$c3ParamsString.") ";
    }
    // ・在留資格その他
    if ($items["C_03_6"] != "" and $items["C_03_etc"] != "") {
        $whereSqls[] = "workers.qualifications_etc = :C_03_etc ";
        $params[":C_03_etc"] = $items["C_03_etc"];
    }

    // ・企業名
    if ($items["C_04"] != "") {
        $whereSqls[] = "workers.company_id = :company_id ";
        $params[":company_id"] = $items["C_04"];
    }

    // ・配信者名
    if ($items["C_05"] != "") {
        // 管理者名に対して部分一致条件を設定。
        $whereSqls[] = "admins.user_name LIKE :sender_name ";
        $params[":sender_name"] = "%".$items["C_05"]."%";
    }

    // ・件名
    if ($items["C_06"] != "") {
        // 件名に対して部分一致条件を設定。
        $whereSqls[] = "info_data.subject_japanese LIKE :subject ";
        $params[":subject"] = "%".$items["C_06"]."%";
    }

    // ・配信日
    if ($items["C_07_1"] != "") {
        $whereSqls[] = "info_data.delivery_date >= STR_TO_DATE(:delivery_date_from, '%Y-%m-%d') ";
        $params[":delivery_date_from"] = $items["C_07_1"];
    }
    if ($items["C_07_2"] != "") {
        $whereSqls[] = "info_data.delivery_date < DATE_ADD(STR_TO_DATE(:delivery_date_to, '%Y-%m-%d'), INTERVAL 1 DAY) ";
        $params[":delivery_date_to"] = $items["C_07_2"];
    }

    // ・配信言語
    if ($items["C_08_1"].$items["C_08_2"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c8Params = array();
        if ($items["C_08_1"] != "") $c8Params[] = "'2'";
        if ($items["C_08_2"] != "") $c8Params[] = "'3'";
        $c8ParamsString = joinArray($c8Params);
        $whereSqls[] = "info_data.delivery_language IN (".$c8ParamsString.") ";
    }
}


/**
 * 受入企業の一覧を取得する。
 *
 * @return void
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "WHERE classification = '1' "  // 受入機関
         . "ORDER BY company_name "
    ;
    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}
