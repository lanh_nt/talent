<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * @param $multi 複数かどうか
 * 
 * @return PageData 検索結果
 */
function search($sp_Id) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
          // 事前ガイダンスの提供
    .     "reportdata.C_1_01_1, "
    .     "reportdata.C_1_01_2, "
    .     "reportdata.C_1_01_3,"
    .     "reportdata.C_1_01_4, "
    .     "reportdata.C_1_01_5_1, "
    .     "reportdata.C_1_01_5_2, "
    .     "reportdata.C_1_01_5_3, "
    .     "reportdata.C_1_01_6, "
    .     "reportdata.C_1_02_1, "
    .     "reportdata.C_1_02_2, "
    .     "reportdata.C_1_02_3, "
    .     "reportdata.C_1_02_4, "
    .     "reportdata.C_1_02_5_1, "
    .     "reportdata.C_1_02_5_2, "
    .     "reportdata.C_1_02_5_3, "
    .     "reportdata.C_1_02_6, "
    .     "reportdata.C_1_03_1, "
    .     "reportdata.C_1_03_2, "
    .     "reportdata.C_1_03_3, "
    .     "reportdata.C_1_03_4, "
    .     "reportdata.C_1_03_5_1, "
    .     "reportdata.C_1_03_5_2, "
    .     "reportdata.C_1_03_5_3, "
    .     "reportdata.C_1_03_6, "
    .     "reportdata.C_1_04_1, "
    .     "reportdata.C_1_04_2, "
    .     "reportdata.C_1_04_3, "
    .     "reportdata.C_1_04_4, "
    .     "reportdata.C_1_04_5_1, "
    .     "reportdata.C_1_04_5_2, "
    .     "reportdata.C_1_04_5_3, "
    .     "reportdata.C_1_04_6, "
    .     "reportdata.C_1_05_1, "
    .     "reportdata.C_1_05_2, "
    .     "reportdata.C_1_05_3, "
    .     "reportdata.C_1_05_4, "
    .     "reportdata.C_1_05_5_1, "
    .     "reportdata.C_1_05_5_2, "
    .     "reportdata.C_1_05_5_3, "
    .     "reportdata.C_1_05_6, "
    .     "reportdata.C_1_06_1, "
    .     "reportdata.C_1_06_2, "
    .     "reportdata.C_1_06_3, "
    .     "reportdata.C_1_06_4, "
    .     "reportdata.C_1_06_5_1, "
    .     "reportdata.C_1_06_5_2, "
    .     "reportdata.C_1_06_5_3, "
    .     "reportdata.C_1_06_6, "
    .     "reportdata.C_1_07_1, "
    .     "reportdata.C_1_07_2, "
    .     "reportdata.C_1_07_3, "
    .     "reportdata.C_1_07_4, "
    .     "reportdata.C_1_07_5_1, "
    .     "reportdata.C_1_07_5_2, "
    .     "reportdata.C_1_07_5_3, "
    .     "reportdata.C_1_07_6, "
    .     "reportdata.C_1_08_1, "
    .     "reportdata.C_1_08_2, "
    .     "reportdata.C_1_08_3, "
    .     "reportdata.C_1_08_4, "
    .     "reportdata.C_1_08_5_1, "
    .     "reportdata.C_1_08_5_2, "
    .     "reportdata.C_1_08_5_3, "
    .     "reportdata.C_1_08_6, "
    .     "reportdata.C_1_09_1, "
    .     "reportdata.C_1_09_2, "
    .     "reportdata.C_1_09_3, "
    .     "reportdata.C_1_09_4, "
    .     "reportdata.C_1_09_5_1, "
    .     "reportdata.C_1_09_5_2, "
    .     "reportdata.C_1_09_5_3, "
    .     "reportdata.C_1_09_6, "
    .     "reportdata.C_1_10_1, "
    .     "reportdata.C_1_10_2, "
    .     "reportdata.C_1_10_3, "
    .     "reportdata.C_1_10_4, "
    .     "reportdata.C_1_10_5_1, "
    .     "reportdata.C_1_10_5_2, "
    .     "reportdata.C_1_10_5_3, "
    .     "reportdata.C_1_10_6, "
    .     "reportdata.C_1_11_1, "
    .     "reportdata.C_1_11_2, "
    .     "reportdata.C_1_11_3, "
    .     "reportdata.C_1_11_4, "
    .     "reportdata.C_1_11_5, "
    .     "reportdata.C_1_11_6_1, "
    .     "reportdata.C_1_11_6_2, "
    .     "reportdata.C_1_11_6_3, "
    .     "reportdata.C_1_11_7, "
    .     "reportdata.C_1_12, "
    .     "reportdata.C_1_13, "

          // 出入国する際の送迎
    .     "reportdata.C_2_1_1, "
    .     "reportdata.C_2_1_2, "
    .     "reportdata.C_2_1_3, "
    .     "reportdata.C_2_1_4, "
    .     "reportdata.C_2_1_5_1, "
    .     "reportdata.C_2_1_5_2, "
    .     "reportdata.C_2_1_6, "
    .     "reportdata.C_2_1_7, "
    .     "reportdata.C_2_2_1, "
    .     "reportdata.C_2_2_2, "
    .     "reportdata.C_2_2_3, "
    .     "reportdata.C_2_2_4, "
    .     "reportdata.C_2_2_5_1, "
    .     "reportdata.C_2_2_5_2, "
    .     "reportdata.C_2_2_6, "
    .     "reportdata.C_2_2_7, "
    .     "reportdata.C_2_2_8, "
    .     "reportdata.C_2_3_1, "
    .     "reportdata.C_2_3_2, "
    .     "reportdata.C_2_3_3, "
    .     "reportdata.C_2_3_4, "
    .     "reportdata.C_2_3_5, "
    .     "reportdata.C_2_3_6, "

          // 適切な住居の確保に係る支援・生活に必要な契約に係る支援
    .     "reportdata.C_3_1_1, "
    .     "reportdata.C_3_1_2, "
    .     "reportdata.C_3_1_3, "
    .     "reportdata.C_3_1_4, "
    .     "reportdata.C_3_2_1, "
    .     "reportdata.C_3_2_2, "
    .     "reportdata.C_3_2_3, "
    .     "reportdata.C_3_2_4, "
    .     "reportdata.C_3_3_1, "
    .     "reportdata.C_3_3_2, "
    .     "reportdata.C_3_3_3, "
    .     "reportdata.C_3_3_4, "
    .     "reportdata.C_3_4_1, "
    .     "reportdata.C_3_4_2, "
    .     "reportdata.C_3_4_3, "
    .     "reportdata.C_3_4_4, "
    .     "reportdata.C_3_4_5, "
    .     "reportdata.C_3_4_6, "
    .     "reportdata.C_3_5_01, "
    .     "reportdata.C_3_5_02, "
    .     "reportdata.C_3_5_03, "
    .     "reportdata.C_3_5_04, "
    .     "reportdata.C_3_5_05, "
    .     "reportdata.C_3_5_06, "
    .     "reportdata.C_3_5_07, "
    .     "reportdata.C_3_5_08, "
    .     "reportdata.C_3_5_09_1, "
    .     "reportdata.C_3_5_09_2, "
    .     "reportdata.C_3_5_09_3, "
    .     "reportdata.C_3_5_10, "
    .     "reportdata.C_3_6_1, "
    .     "reportdata.C_3_6_2, "
    .     "reportdata.C_3_6_3, "
    .     "reportdata.C_3_6_4, "
    .     "reportdata.C_3_6_5_1, "
    .     "reportdata.C_3_6_5_2, "
    .     "reportdata.C_3_6_5_3, "
    .     "reportdata.C_3_6_6, "
    .     "reportdata.C_3_7_1, "
    .     "reportdata.C_3_7_2, "
    .     "reportdata.C_3_7_3, "
    .     "reportdata.C_3_7_4, "
    .     "reportdata.C_3_7_5_1, "
    .     "reportdata.C_3_7_5_2, "
    .     "reportdata.C_3_7_5_3, "
    .     "reportdata.C_3_7_6, "
    .     "reportdata.C_3_8_1, "
    .     "reportdata.C_3_8_2, "
    .     "reportdata.C_3_8_3, "
    .     "reportdata.C_3_8_4, "
    .     "reportdata.C_3_8_5, "
    .     "reportdata.C_3_8_6, "
          
          // 生活オリエンテーションの実施
    .     "reportdata.C_4_1_1, "
    .     "reportdata.C_4_1_2, "
    .     "reportdata.C_4_1_3, "
    .     "reportdata.C_4_1_4, "
    .     "reportdata.C_4_1_5_1, "
    .     "reportdata.C_4_1_5_2, "
    .     "reportdata.C_4_1_5_3, "
    .     "reportdata.C_4_1_6, "
    .     "reportdata.C_4_2_1, "
    .     "reportdata.C_4_2_2, "
    .     "reportdata.C_4_2_3, "
    .     "reportdata.C_4_2_4, "
    .     "reportdata.C_4_2_5_1, "
    .     "reportdata.C_4_2_5_2, "
    .     "reportdata.C_4_2_5_3, "
    .     "reportdata.C_4_2_5_4, "
    .     "reportdata.C_4_2_6, "
    .     "reportdata.C_4_3_1, "
    .     "reportdata.C_4_3_2, "
    .     "reportdata.C_4_3_3, "
    .     "reportdata.C_4_3_4, "
    .     "reportdata.C_4_3_5_1, "
    .     "reportdata.C_4_3_5_2, "
    .     "reportdata.C_4_3_5_3, "
    .     "reportdata.C_4_3_6, "
    .     "reportdata.C_4_4_1, "
    .     "reportdata.C_4_4_2, "
    .     "reportdata.C_4_4_3, "
    .     "reportdata.C_4_4_4, "
    .     "reportdata.C_4_4_5_1, "
    .     "reportdata.C_4_4_5_2, "
    .     "reportdata.C_4_4_5_3, "
    .     "reportdata.C_4_4_6, "
    .     "reportdata.C_4_5_1, "
    .     "reportdata.C_4_5_2, "
    .     "reportdata.C_4_5_3, "
    .     "reportdata.C_4_5_4, "
    .     "reportdata.C_4_5_5_1, "
    .     "reportdata.C_4_5_5_2, "
    .     "reportdata.C_4_5_5_3, "
    .     "reportdata.C_4_5_6, "
    .     "reportdata.C_4_6_1, "
    .     "reportdata.C_4_6_2, "
    .     "reportdata.C_4_6_3, "
    .     "reportdata.C_4_6_4, "
    .     "reportdata.C_4_6_5_1, "
    .     "reportdata.C_4_6_5_2, "
    .     "reportdata.C_4_6_5_3, "
    .     "reportdata.C_4_6_6, "
    .     "reportdata.C_4_7_1, "
    .     "reportdata.C_4_7_2, "
    .     "reportdata.C_4_7_3, "
    .     "reportdata.C_4_7_4, "
    .     "reportdata.C_4_7_5, "
    .     "reportdata.C_4_7_6_1, "
    .     "reportdata.C_4_7_6_2, "
    .     "reportdata.C_4_7_6_3, "
    .     "reportdata.C_4_7_7, "
    .     "reportdata.C_4_8, "
    .     "reportdata.C_4_9, "
          
          // 日本語学習の機会の提供
    .     "reportdata.C_5_1_1, "
    .     "reportdata.C_5_1_2, "
    .     "reportdata.C_5_1_3, "
    .     "reportdata.C_5_1_4, "
    .     "reportdata.C_5_2_1, "
    .     "reportdata.C_5_2_2, "
    .     "reportdata.C_5_2_3, "
    .     "reportdata.C_5_2_4, "
    .     "reportdata.C_5_3_1, "
    .     "reportdata.C_5_3_2, "
    .     "reportdata.C_5_3_3, "
    .     "reportdata.C_5_3_4, "
    .     "reportdata.C_5_4_1, "
    .     "reportdata.C_5_4_2, "
    .     "reportdata.C_5_4_3, "
    .     "reportdata.C_5_4_4, "
    .     "reportdata.C_5_4_5, "
    .     "reportdata.C_5_4_6, "

          // 相談又は苦情への対応
    .     "reportdata.C_6_1_1, "
    .     "reportdata.C_6_1_2, "
    .     "reportdata.C_6_1_3, "
    .     "reportdata.C_6_1_4, "
    .     "reportdata.C_6_2_1, "
    .     "reportdata.C_6_2_2, "
    .     "reportdata.C_6_2_3, "
    .     "reportdata.C_6_2_4, "
    .     "reportdata.C_6_3_1, "
    .     "reportdata.C_6_3_2, "
    .     "reportdata.C_6_3_3, "
    .     "reportdata.C_6_3_4, "
    .     "reportdata.C_6_3_5, "
    .     "reportdata.C_6_4_01, "
    .     "reportdata.C_6_4_02, "
    .     "reportdata.C_6_4_03, "
    .     "reportdata.C_6_4_04, "
    .     "reportdata.C_6_4_05, "
    .     "reportdata.C_6_4_06, "
    .     "reportdata.C_6_4_07, "
    .     "reportdata.C_6_4_08, "
    .     "reportdata.C_6_4_09, "
    .     "reportdata.C_6_4_10, "
    .     "reportdata.C_6_5_1, "
    .     "reportdata.C_6_5_2, "
    .     "reportdata.C_6_6_1, "
    .     "reportdata.C_6_6_2, "
    .     "reportdata.C_6_7_1, "
    .     "reportdata.C_6_7_2, "
    .     "reportdata.C_6_8_1_1, "
    .     "reportdata.C_6_8_1_2, "
    .     "reportdata.C_6_8_1_3, "
    .     "reportdata.C_6_8_1_4, "
    .     "reportdata.C_6_8_2, "
    .     "reportdata.C_6_8_3, "
    .     "reportdata.C_6_8_4, "
    .     "reportdata.C_6_9_1_1, "
    .     "reportdata.C_6_9_1_2, "
    .     "reportdata.C_6_9_1_3, "
    .     "reportdata.C_6_9_1_4, "
    .     "reportdata.C_6_9_2, "
    .     "reportdata.C_6_9_3, "
    .     "reportdata.C_6_9_4, "
    .     "reportdata.C_6_9_5, "

          // 日本人との交流促進に係る支援
    .     "reportdata.C_7_1_1, "
    .     "reportdata.C_7_1_2, "
    .     "reportdata.C_7_1_3, "
    .     "reportdata.C_7_1_4, "
    .     "reportdata.C_7_2_1, "
    .     "reportdata.C_7_2_2, "
    .     "reportdata.C_7_2_3, "
    .     "reportdata.C_7_2_4, "
    .     "reportdata.C_7_3_1, "
    .     "reportdata.C_7_3_2, "
    .     "reportdata.C_7_3_3, "
    .     "reportdata.C_7_3_4, "
    .     "reportdata.C_7_3_5, "
    .     "reportdata.C_7_3_6, "

          // 非自発的離職時の転職支援
    .     "reportdata.C_8_1_1, "
    .     "reportdata.C_8_1_2, "
    .     "reportdata.C_8_1_3, "
    .     "reportdata.C_8_1_4, "
    .     "reportdata.C_8_2_1, "
    .     "reportdata.C_8_2_2, "
    .     "reportdata.C_8_2_3, "
    .     "reportdata.C_8_2_4, "
    .     "reportdata.C_8_3_1, "
    .     "reportdata.C_8_3_2, "
    .     "reportdata.C_8_3_3, "
    .     "reportdata.C_8_3_4, "
    .     "reportdata.C_8_4_1, "
    .     "reportdata.C_8_4_2, "
    .     "reportdata.C_8_4_3, "
    .     "reportdata.C_8_4_4, "
    .     "reportdata.C_8_5_1, "
    .     "reportdata.C_8_5_2, "
    .     "reportdata.C_8_6_1, "
    .     "reportdata.C_8_6_2, "
    .     "reportdata.C_8_6_3, "
    .     "reportdata.C_8_6_4, "
    .     "reportdata.C_8_6_5_1, "
    .     "reportdata.C_8_6_5_2, "
    .     "reportdata.C_8_6_5_3, "
    .     "reportdata.C_8_6_6, "
    .     "reportdata.C_8_7_1, "
    .     "reportdata.C_8_7_2, "
    .     "reportdata.C_8_7_3, "
    .     "reportdata.C_8_7_4, "
    .     "reportdata.C_8_8_1, "
    .     "reportdata.C_8_8_2, "
    .     "reportdata.C_8_8_3, "
    .     "reportdata.C_8_8_4, "
    .     "reportdata.C_8_8_5, "
    .     "reportdata.C_8_8_6, "

          // 定期的な面談の実施・行政機関への通報
    .     "reportdata.C_9_1_1, "
    .     "reportdata.C_9_1_2, "
    .     "reportdata.C_9_1_3, "
    .     "reportdata.C_9_1_4, "
    .     "reportdata.C_9_1_5_1, "
    .     "reportdata.C_9_1_5_2, "
    .     "reportdata.C_9_1_5_3, "
    .     "reportdata.C_9_1_6, "
    .     "reportdata.C_9_2_1, "
    .     "reportdata.C_9_2_2, "
    .     "reportdata.C_9_2_3, "
    .     "reportdata.C_9_2_4, "
    .     "reportdata.C_9_2_5_1, "
    .     "reportdata.C_9_2_5_2, "
    .     "reportdata.C_9_2_5_3, "
    .     "reportdata.C_9_2_6, "
    .     "reportdata.C_9_3_1, "
    .     "reportdata.C_9_3_2, "
    .     "reportdata.C_9_3_3, "
    .     "reportdata.C_9_3_4, "
    .     "reportdata.C_9_4_1, "
    .     "reportdata.C_9_4_2, "
    .     "reportdata.C_9_4_3, "
    .     "reportdata.C_9_4_4, "
    .     "reportdata.C_9_5_1, "
    .     "reportdata.C_9_5_2, "
    .     "reportdata.C_9_5_3, "
    .     "reportdata.C_9_5_4, "
    .     "reportdata.C_9_5_5, "
    .     "reportdata.C_9_5_6, "
    .     "reportdata.C_9_6, "
    .     "reportdata.C_9_7 "
    . "FROM tbl_report_002 reportdata "
    . "INNER JOIN mst_workers workers "
    . "ON reportdata.worker_id = workers.id "
    . "WHERE reportdata.id = :sp_Id "
;

    $params = array();
    $params[":sp_Id"] = $sp_Id;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);



        // 管理ユーザーマスタ情報から取得した担当者名をデータに設定。
        $result["C_1_01_4"] = getAdminLRowId($db, $result["C_1_01_4"]);
        $result["C_1_02_4"] = getAdminLRowId($db, $result["C_1_02_4"]);
        $result["C_1_03_4"] = getAdminLRowId($db, $result["C_1_03_4"]);
        $result["C_1_04_4"] = getAdminLRowId($db, $result["C_1_04_4"]);
        $result["C_1_05_4"] = getAdminLRowId($db, $result["C_1_05_4"]);
        $result["C_1_06_4"] = getAdminLRowId($db, $result["C_1_06_4"]);
        $result["C_1_07_4"] = getAdminLRowId($db, $result["C_1_07_4"]);
        $result["C_1_08_4"] = getAdminLRowId($db, $result["C_1_08_4"]);
        $result["C_1_09_4"] = getAdminLRowId($db, $result["C_1_09_4"]);
        $result["C_1_10_4"] = getAdminLRowId($db, $result["C_1_10_4"]);
        $result["C_1_11_5"] = getAdminLRowId($db, $result["C_1_11_5"]);
        $result["C_2_1_4"]  = getAdminLRowId($db, $result["C_2_1_4"]);
        $result["C_2_2_4"]  = getAdminLRowId($db, $result["C_2_2_4"]);
        $result["C_2_3_5"]  = getAdminLRowId($db, $result["C_2_3_5"]);
        $result["C_3_1_4"]  = getAdminLRowId($db, $result["C_3_1_4"]);
        $result["C_3_2_4"]  = getAdminLRowId($db, $result["C_3_2_4"]);
        $result["C_3_3_4"]  = getAdminLRowId($db, $result["C_3_3_4"]);
        $result["C_3_4_5"]  = getAdminLRowId($db, $result["C_3_4_5"]);
        $result["C_3_5_08"] = getAdminLRowId($db, $result["C_3_5_08"]);
        $result["C_3_6_4"]  = getAdminLRowId($db, $result["C_3_6_4"]);
        $result["C_3_7_4"]  = getAdminLRowId($db, $result["C_3_7_4"]);
        $result["C_3_8_5"]  = getAdminLRowId($db, $result["C_3_8_5"]);
        $result["C_4_1_4"]  = getAdminLRowId($db, $result["C_4_1_4"]);
        $result["C_4_2_4"]  = getAdminLRowId($db, $result["C_4_2_4"]);
        $result["C_4_3_4"]  = getAdminLRowId($db, $result["C_4_3_4"]);
        $result["C_4_4_4"]  = getAdminLRowId($db, $result["C_4_4_4"]);
        $result["C_4_5_4"]  = getAdminLRowId($db, $result["C_4_5_4"]);
        $result["C_4_6_4"]  = getAdminLRowId($db, $result["C_4_6_4"]);
        $result["C_4_7_5"]  = getAdminLRowId($db, $result["C_4_7_5"]);
        $result["C_5_1_4"]  = getAdminLRowId($db, $result["C_5_1_4"]);
        $result["C_5_2_4"]  = getAdminLRowId($db, $result["C_5_2_4"]);
        $result["C_5_3_4"]  = getAdminLRowId($db, $result["C_5_3_4"]);
        $result["C_5_4_5"]  = getAdminLRowId($db, $result["C_5_4_5"]);
        $result["C_6_1_4"]  = getAdminLRowId($db, $result["C_6_1_4"]);
        $result["C_6_2_4"]  = getAdminLRowId($db, $result["C_6_2_4"]);
        $result["C_6_3_5"]  = getAdminLRowId($db, $result["C_6_3_5"]);
        $result["C_7_1_4"]  = getAdminLRowId($db, $result["C_7_1_4"]);
        $result["C_7_2_4"]  = getAdminLRowId($db, $result["C_7_2_4"]);
        $result["C_7_3_5"]  = getAdminLRowId($db, $result["C_7_3_5"]);
        $result["C_8_1_4"]  = getAdminLRowId($db, $result["C_8_1_4"]);
        $result["C_8_2_4"]  = getAdminLRowId($db, $result["C_8_2_4"]);
        $result["C_8_3_4"]  = getAdminLRowId($db, $result["C_8_3_4"]);
        $result["C_8_4_4"]  = getAdminLRowId($db, $result["C_8_4_4"]);
        $result["C_8_6_4"]  = getAdminLRowId($db, $result["C_8_6_4"]);
        $result["C_8_7_4"]  = getAdminLRowId($db, $result["C_8_7_4"]);
        $result["C_8_8_5"]  = getAdminLRowId($db, $result["C_8_8_5"]);
        $result["C_9_1_4"]  = getAdminLRowId($db, $result["C_9_1_4"]);
        $result["C_9_2_4"]  = getAdminLRowId($db, $result["C_9_2_4"]);
        $result["C_9_3_4"]  = getAdminLRowId($db, $result["C_9_3_4"]);
        $result["C_9_4_4"]  = getAdminLRowId($db, $result["C_9_4_4"]);
        $result["C_9_5_5"]  = getAdminLRowId($db, $result["C_9_5_5"]);

        
    // 表示用データを追加設定。
    $sex            = getOptionItems("comm", "sex");
    $yesno          = getOptionItems("comm", "yesno");
    $wk02004_01     = getOptionItems("comm", "WK02004_01");
    $wk02004_02     = getOptionItems("comm", "WK02004_02");
    $wk02004_03     = getOptionItems("comm", "WK02004_03");
    $wk02004_04     = getOptionItems("comm", "WK02004_04");
    $wk02004_05     = getOptionItems("comm", "WK02004_05");
    $wk02004_06     = getOptionItems("comm", "WK02004_06");
    $wk02004_07     = getOptionItems("comm", "WK02004_07");
    $wk02004_08     = getOptionItems("comm", "WK02004_08");
    $wk02004_09     = getOptionItems("comm", "WK02004_09");
    $wk02004_10     = getOptionItems("comm", "WK02004_10");
    $wk02004_11     = getOptionItems("comm", "WK02004_11");
    $wk02004_12     = getOptionItems("comm", "WK02004_12");
    $wk02004_13     = getOptionItems("comm", "WK02004_13");
    $wk02004_14     = getOptionItems("comm", "WK02004_14");
    $wk02004_15     = getOptionItems("comm", "WK02004_15");
    $number         = getOptionItems("comm", "SP01001_number");
    

    // コードに対応する文言を設定。
    $result["C_3_5_01"]    = getOprionItemValue($number,        $result["C_1_01_1"]);
    $result["C_3_5_02"]    = getOprionItemValue($number,        $result["C_1_01_1"]);
    $result["C_3_5_03"]    = getOprionItemValue($number,        $result["C_1_01_1"]);

    // 実施予定
    $result["C_1_01_1"]     = getOprionItemValue($yesno,        $result["C_1_01_1"]);
    $result["C_1_02_1"]     = getOprionItemValue($yesno,        $result["C_1_02_1"]);
    $result["C_1_03_1"]     = getOprionItemValue($yesno,        $result["C_1_03_1"]);
    $result["C_1_04_1"]     = getOprionItemValue($yesno,        $result["C_1_04_1"]);
    $result["C_1_05_1"]     = getOprionItemValue($yesno,        $result["C_1_05_1"]);
    $result["C_1_06_1"]     = getOprionItemValue($yesno,        $result["C_1_06_1"]);
    $result["C_1_07_1"]     = getOprionItemValue($yesno,        $result["C_1_07_1"]);
    $result["C_1_08_1"]     = getOprionItemValue($yesno,        $result["C_1_08_1"]);
    $result["C_1_09_1"]     = getOprionItemValue($yesno,        $result["C_1_09_1"]);
    $result["C_1_10_1"]     = getOprionItemValue($yesno,        $result["C_1_10_1"]);
    $result["C_1_11_2"]     = getOprionItemValue($yesno,        $result["C_1_11_2"]);

    $result["C_2_1_1"]     = getOprionItemValue($yesno,        $result["C_2_1_1"]);
    $result["C_2_2_1"]     = getOprionItemValue($yesno,        $result["C_2_2_1"]);
    $result["C_2_3_2"]     = getOprionItemValue($yesno,        $result["C_2_3_2"]);

    $result["C_3_1_1"]     = getOprionItemValue($yesno,        $result["C_3_1_1"]);
    $result["C_3_2_1"]     = getOprionItemValue($yesno,        $result["C_3_2_1"]);
    $result["C_3_3_1"]     = getOprionItemValue($yesno,        $result["C_3_3_1"]);
    $result["C_3_4_2"]     = getOprionItemValue($yesno,        $result["C_3_4_2"]);
    $result["C_3_5_05"]    = getOprionItemValue($yesno,        $result["C_3_5_05"]);
    $result["C_3_6_1"]     = getOprionItemValue($yesno,        $result["C_3_6_1"]);
    $result["C_3_7_1"]     = getOprionItemValue($yesno,        $result["C_3_7_1"]);
    $result["C_3_8_2"]     = getOprionItemValue($yesno,        $result["C_3_8_2"]);

    $result["C_4_1_1"]     = getOprionItemValue($yesno,        $result["C_4_1_1"]);
    $result["C_4_2_1"]     = getOprionItemValue($yesno,        $result["C_4_2_1"]);
    $result["C_4_3_1"]     = getOprionItemValue($yesno,        $result["C_4_3_1"]);
    $result["C_4_4_1"]     = getOprionItemValue($yesno,        $result["C_4_4_1"]);
    $result["C_4_5_1"]     = getOprionItemValue($yesno,        $result["C_4_5_1"]);
    $result["C_4_6_1"]     = getOprionItemValue($yesno,        $result["C_4_6_1"]);
    $result["C_4_7_2"]     = getOprionItemValue($yesno,        $result["C_4_7_2"]);

    $result["C_5_1_1"]     = getOprionItemValue($yesno,        $result["C_5_1_1"]);
    $result["C_5_2_1"]     = getOprionItemValue($yesno,        $result["C_5_2_1"]);
    $result["C_5_3_1"]     = getOprionItemValue($yesno,        $result["C_5_3_1"]);
    $result["C_5_4_2"]     = getOprionItemValue($yesno,        $result["C_5_4_2"]);

    $result["C_6_1_1"]     = getOprionItemValue($yesno,        $result["C_6_1_1"]);
    $result["C_6_2_1"]     = getOprionItemValue($yesno,        $result["C_6_2_1"]);
    $result["C_6_3_2"]     = getOprionItemValue($yesno,        $result["C_6_3_2"]);

    $result["C_7_1_1"]     = getOprionItemValue($yesno,        $result["C_7_1_1"]);
    $result["C_7_2_1"]     = getOprionItemValue($yesno,        $result["C_7_2_1"]);
    $result["C_7_3_2"]     = getOprionItemValue($yesno,        $result["C_7_3_2"]);

    $result["C_8_1_1"]     = getOprionItemValue($yesno,        $result["C_8_1_1"]);
    $result["C_8_2_1"]     = getOprionItemValue($yesno,        $result["C_8_2_1"]);
    $result["C_8_3_1"]     = getOprionItemValue($yesno,        $result["C_8_3_1"]);
    $result["C_8_4_1"]     = getOprionItemValue($yesno,        $result["C_8_4_1"]);
    $result["C_8_5_1"]     = getOprionItemValue($yesno,        $result["C_8_5_1"]);
    $result["C_8_6_1"]     = getOprionItemValue($yesno,        $result["C_8_6_1"]);
    $result["C_8_7_1"]     = getOprionItemValue($yesno,        $result["C_8_7_1"]);
    $result["C_8_8_2"]     = getOprionItemValue($yesno,        $result["C_8_8_2"]);

    $result["C_9_1_1"]     = getOprionItemValue($yesno,        $result["C_9_1_1"]);
    $result["C_9_2_1"]     = getOprionItemValue($yesno,        $result["C_9_2_1"]);
    $result["C_9_3_1"]     = getOprionItemValue($yesno,        $result["C_9_3_1"]);
    $result["C_9_4_1"]     = getOprionItemValue($yesno,        $result["C_9_4_1"]);
    $result["C_9_5_2"]     = getOprionItemValue($yesno,        $result["C_9_5_2"]);


    // 委託の有無
    $result["C_1_01_3"]     = getOprionItemValue($yesno,        $result["C_1_01_3"]);
    $result["C_1_02_3"]     = getOprionItemValue($yesno,        $result["C_1_02_3"]);
    $result["C_1_03_3"]     = getOprionItemValue($yesno,        $result["C_1_03_3"]);
    $result["C_1_04_3"]     = getOprionItemValue($yesno,        $result["C_1_04_3"]);
    $result["C_1_05_3"]     = getOprionItemValue($yesno,        $result["C_1_05_3"]);
    $result["C_1_06_3"]     = getOprionItemValue($yesno,        $result["C_1_06_3"]);
    $result["C_1_07_3"]     = getOprionItemValue($yesno,        $result["C_1_07_3"]);
    $result["C_1_08_3"]     = getOprionItemValue($yesno,        $result["C_1_08_3"]);
    $result["C_1_09_3"]     = getOprionItemValue($yesno,        $result["C_1_09_3"]);
    $result["C_1_10_3"]     = getOprionItemValue($yesno,        $result["C_1_10_3"]);
    $result["C_1_11_4"]     = getOprionItemValue($yesno,        $result["C_1_11_4"]);

    $result["C_2_1_3"]     = getOprionItemValue($yesno,        $result["C_2_1_3"]);
    $result["C_2_2_3"]     = getOprionItemValue($yesno,        $result["C_2_2_3"]);
    $result["C_2_3_4"]     = getOprionItemValue($yesno,        $result["C_2_3_4"]);

    $result["C_3_1_3"]     = getOprionItemValue($yesno,        $result["C_3_1_3"]);
    $result["C_3_2_3"]     = getOprionItemValue($yesno,        $result["C_3_2_3"]);
    $result["C_3_3_3"]     = getOprionItemValue($yesno,        $result["C_3_3_3"]);
    $result["C_3_4_4"]     = getOprionItemValue($yesno,        $result["C_3_4_4"]);
    $result["C_3_5_07"]    = getOprionItemValue($yesno,        $result["C_3_5_07"]);
    $result["C_3_6_3"]     = getOprionItemValue($yesno,        $result["C_3_6_3"]);
    $result["C_3_7_3"]     = getOprionItemValue($yesno,        $result["C_3_7_3"]);
    $result["C_3_8_4"]     = getOprionItemValue($yesno,        $result["C_3_8_4"]);

    $result["C_4_1_3"]     = getOprionItemValue($yesno,        $result["C_4_1_3"]);
    $result["C_4_2_3"]     = getOprionItemValue($yesno,        $result["C_4_2_3"]);
    $result["C_4_3_3"]     = getOprionItemValue($yesno,        $result["C_4_3_3"]);
    $result["C_4_4_3"]     = getOprionItemValue($yesno,        $result["C_4_4_3"]);
    $result["C_4_5_3"]     = getOprionItemValue($yesno,        $result["C_4_5_3"]);
    $result["C_4_6_3"]     = getOprionItemValue($yesno,        $result["C_4_6_3"]);
    $result["C_4_7_4"]     = getOprionItemValue($yesno,        $result["C_4_7_4"]);

    $result["C_5_1_3"]     = getOprionItemValue($yesno,        $result["C_5_1_3"]);
    $result["C_5_2_3"]     = getOprionItemValue($yesno,        $result["C_5_2_3"]);
    $result["C_5_3_3"]     = getOprionItemValue($yesno,        $result["C_5_3_3"]);
    $result["C_5_4_4"]     = getOprionItemValue($yesno,        $result["C_5_4_4"]);

    $result["C_6_1_3"]     = getOprionItemValue($yesno,        $result["C_6_1_3"]);
    $result["C_6_2_3"]     = getOprionItemValue($yesno,        $result["C_6_2_3"]);
    $result["C_6_3_4"]     = getOprionItemValue($yesno,        $result["C_6_3_4"]);

    $result["C_7_1_3"]     = getOprionItemValue($yesno,        $result["C_7_1_3"]);
    $result["C_7_2_3"]     = getOprionItemValue($yesno,        $result["C_7_2_3"]);
    $result["C_7_3_4"]     = getOprionItemValue($yesno,        $result["C_7_3_4"]);

    $result["C_8_1_3"]     = getOprionItemValue($yesno,        $result["C_8_1_3"]);
    $result["C_8_2_3"]     = getOprionItemValue($yesno,        $result["C_8_2_3"]);
    $result["C_8_3_3"]     = getOprionItemValue($yesno,        $result["C_8_3_3"]);
    $result["C_8_4_3"]     = getOprionItemValue($yesno,        $result["C_8_4_3"]);
    $result["C_8_6_3"]     = getOprionItemValue($yesno,        $result["C_8_6_3"]);
    $result["C_8_7_3"]     = getOprionItemValue($yesno,        $result["C_8_7_3"]);
    $result["C_8_8_4"]     = getOprionItemValue($yesno,        $result["C_8_8_4"]);

    $result["C_9_1_3"]     = getOprionItemValue($yesno,        $result["C_9_1_3"]);
    $result["C_9_2_3"]     = getOprionItemValue($yesno,        $result["C_9_2_3"]);
    $result["C_9_3_3"]     = getOprionItemValue($yesno,        $result["C_9_3_3"]);
    $result["C_9_4_3"]     = getOprionItemValue($yesno,        $result["C_9_4_3"]);
    $result["C_9_5_4"]     = getOprionItemValue($yesno,        $result["C_9_5_4"]);


    // 実装方法
    $c_1_01[]       = getOprionItemValue($wk02004_01,   $result["C_1_01_5_1"]);
    $c_1_01[]       = getOprionItemValue($wk02004_02,   $result["C_1_01_5_2"]);
    $c_1_01[]       = getOprionItemValue($wk02004_15,   $result["C_1_01_5_3"]);

    $c_1_02[]       = getOprionItemValue($wk02004_01,   $result["C_1_02_5_1"]);
    $c_1_02[]       = getOprionItemValue($wk02004_02,   $result["C_1_02_5_2"]);
    $c_1_02[]       = getOprionItemValue($wk02004_15,   $result["C_1_02_5_3"]);

    $c_1_03[]       = getOprionItemValue($wk02004_01,   $result["C_1_03_5_1"]);
    $c_1_03[]       = getOprionItemValue($wk02004_02,   $result["C_1_03_5_2"]);
    $c_1_03[]       = getOprionItemValue($wk02004_15,   $result["C_1_03_5_3"]);

    $c_1_04[]       = getOprionItemValue($wk02004_01,   $result["C_1_04_5_1"]);
    $c_1_04[]       = getOprionItemValue($wk02004_02,   $result["C_1_04_5_2"]);
    $c_1_04[]       = getOprionItemValue($wk02004_15,   $result["C_1_04_5_3"]);

    $c_1_05[]       = getOprionItemValue($wk02004_01,   $result["C_1_05_5_1"]);
    $c_1_05[]       = getOprionItemValue($wk02004_02,   $result["C_1_05_5_2"]);
    $c_1_05[]       = getOprionItemValue($wk02004_15,   $result["C_1_05_5_3"]);

    $c_1_06[]       = getOprionItemValue($wk02004_01,   $result["C_1_06_5_1"]);
    $c_1_06[]       = getOprionItemValue($wk02004_02,   $result["C_1_06_5_2"]);
    $c_1_06[]       = getOprionItemValue($wk02004_15,   $result["C_1_06_5_3"]);

    $c_1_07[]       = getOprionItemValue($wk02004_01,   $result["C_1_07_5_1"]);
    $c_1_07[]       = getOprionItemValue($wk02004_02,   $result["C_1_07_5_2"]);
    $c_1_07[]       = getOprionItemValue($wk02004_15,   $result["C_1_07_5_3"]);

    $c_1_08[]       = getOprionItemValue($wk02004_01,   $result["C_1_08_5_1"]);
    $c_1_08[]       = getOprionItemValue($wk02004_02,   $result["C_1_08_5_2"]);
    $c_1_08[]       = getOprionItemValue($wk02004_15,   $result["C_1_08_5_3"]);

    $c_1_09[]       = getOprionItemValue($wk02004_01,   $result["C_1_09_5_1"]);
    $c_1_09[]       = getOprionItemValue($wk02004_02,   $result["C_1_09_5_2"]);
    $c_1_09[]       = getOprionItemValue($wk02004_15,   $result["C_1_09_5_3"]);

    $c_1_10[]       = getOprionItemValue($wk02004_01,   $result["C_1_10_5_1"]);
    $c_1_10[]       = getOprionItemValue($wk02004_02,   $result["C_1_10_5_2"]);
    $c_1_10[]       = getOprionItemValue($wk02004_15,   $result["C_1_10_5_3"]);

    $c_1_11[]       = getOprionItemValue($wk02004_01,   $result["C_1_11_6_1"]);
    $c_1_11[]       = getOprionItemValue($wk02004_02,   $result["C_1_11_6_2"]);
    $c_1_11[]       = getOprionItemValue($wk02004_15,   $result["C_1_11_6_3"]);

    $c_2_1[]        = getOprionItemValue($wk02004_03,   $result["C_2_1_5_1"]);
    $c_2_1[]        = getOprionItemValue($wk02004_04,   $result["C_2_1_5_2"]);

    $c_2_2[]        = getOprionItemValue($wk02004_03,   $result["C_2_2_5_1"]);
    $c_2_2[]        = getOprionItemValue($wk02004_04,   $result["C_2_2_5_2"]);

    $c_3_1[]        = getOprionItemValue($wk02004_05,   $result["C_3_5_09_1"]);
    $c_3_1[]        = getOprionItemValue($wk02004_06,   $result["C_3_5_09_2"]);
    $c_3_1[]        = getOprionItemValue($wk02004_15,   $result["C_3_5_09_3"]);

    $c_3_2[]        = getOprionItemValue($wk02004_05,   $result["C_3_6_5_1"]);
    $c_3_2[]        = getOprionItemValue($wk02004_06,   $result["C_3_6_5_2"]);
    $c_3_2[]        = getOprionItemValue($wk02004_15,   $result["C_3_6_5_3"]);

    $c_3_3[]        = getOprionItemValue($wk02004_05,   $result["C_3_7_5_1"]);
    $c_3_3[]        = getOprionItemValue($wk02004_06,   $result["C_3_7_5_2"]);
    $c_3_3[]        = getOprionItemValue($wk02004_15,   $result["C_3_7_5_3"]);

    $c_4_1[]        = getOprionItemValue($wk02004_07,   $result["C_4_1_5_1"]);
    $c_4_1[]        = getOprionItemValue($wk02004_08,   $result["C_4_1_5_2"]);
    $c_4_1[]        = getOprionItemValue($wk02004_15,   $result["C_4_1_5_3"]);

    $c_4_2[]        = getOprionItemValue($wk02004_07,   $result["C_4_2_5_1"]);
    $c_4_2[]        = getOprionItemValue($wk02004_08,   $result["C_4_2_5_2"]);
    $c_4_2[]        = getOprionItemValue($wk02004_06,   $result["C_4_2_5_3"]);
    $c_4_2[]        = getOprionItemValue($wk02004_15,   $result["C_4_2_5_4"]);

    $c_4_3[]        = getOprionItemValue($wk02004_07,   $result["C_4_3_5_1"]);
    $c_4_3[]        = getOprionItemValue($wk02004_08,   $result["C_4_3_5_2"]);
    $c_4_3[]        = getOprionItemValue($wk02004_15,   $result["C_4_3_5_3"]);

    $c_4_4[]        = getOprionItemValue($wk02004_07,   $result["C_4_4_5_1"]);
    $c_4_4[]        = getOprionItemValue($wk02004_08,   $result["C_4_4_5_2"]);
    $c_4_4[]        = getOprionItemValue($wk02004_15,   $result["C_4_4_5_3"]);

    $c_4_5[]        = getOprionItemValue($wk02004_07,   $result["C_4_5_5_1"]);
    $c_4_5[]        = getOprionItemValue($wk02004_08,   $result["C_4_5_5_2"]);
    $c_4_5[]        = getOprionItemValue($wk02004_15,   $result["C_4_5_5_3"]);

    $c_4_6[]        = getOprionItemValue($wk02004_07,   $result["C_4_6_5_1"]);
    $c_4_6[]        = getOprionItemValue($wk02004_08,   $result["C_4_6_5_2"]);
    $c_4_6[]        = getOprionItemValue($wk02004_15,   $result["C_4_6_5_3"]);

    $c_4_7[]        = getOprionItemValue($wk02004_07,   $result["C_4_7_6_1"]);
    $c_4_7[]        = getOprionItemValue($wk02004_08,   $result["C_4_7_6_2"]);
    $c_4_7[]        = getOprionItemValue($wk02004_15,   $result["C_4_7_6_3"]);

    $c_6_8[]        = getOprionItemValue($wk02004_09,   $result["C_6_8_1_1"]);
    $c_6_8[]        = getOprionItemValue($wk02004_10,   $result["C_6_8_1_2"]);
    $c_6_8[]        = getOprionItemValue($wk02004_11,   $result["C_6_8_1_3"]);
    $c_6_8[]        = getOprionItemValue($wk02004_15,   $result["C_6_8_1_4"]);

    $c_6_9[]        = getOprionItemValue($wk02004_09,   $result["C_6_9_1_1"]);
    $c_6_9[]        = getOprionItemValue($wk02004_10,   $result["C_6_9_1_2"]);
    $c_6_9[]        = getOprionItemValue($wk02004_11,   $result["C_6_9_1_3"]);
    $c_6_9[]        = getOprionItemValue($wk02004_15,   $result["C_6_9_1_4"]);

    $c_8_6[]        = getOprionItemValue($wk02004_07,   $result["C_8_6_5_1"]);
    $c_8_6[]        = getOprionItemValue($wk02004_11,   $result["C_8_6_5_2"]);
    $c_8_6[]        = getOprionItemValue($wk02004_15,   $result["C_8_6_5_3"]);

    $c_9_1[]        = getOprionItemValue($wk02004_01,   $result["C_9_1_5_1"]);
    $c_9_1[]        = getOprionItemValue($wk02004_12,   $result["C_9_1_5_2"]);
    $c_9_1[]        = getOprionItemValue($wk02004_15,   $result["C_9_1_5_3"]);

    $c_9_2[]        = getOprionItemValue($wk02004_01,   $result["C_9_2_5_1"]);
    $c_9_2[]        = getOprionItemValue($wk02004_12,   $result["C_9_2_5_2"]);
    $c_9_2[]        = getOprionItemValue($wk02004_15,   $result["C_9_2_5_3"]);


        // カンマを設定。
    $result["C_1_01_5"] = setCommna($c_1_01);
    $result["C_1_01_5"] = setCommna($c_1_01);
    $result["C_1_02_5"] = setCommna($c_1_02);
    $result["C_1_03_5"] = setCommna($c_1_03);
    $result["C_1_04_5"] = setCommna($c_1_04);
    $result["C_1_05_5"] = setCommna($c_1_05);
    $result["C_1_06_5"] = setCommna($c_1_06);
    $result["C_1_07_5"] = setCommna($c_1_07);
    $result["C_1_08_5"] = setCommna($c_1_08);
    $result["C_1_09_5"] = setCommna($c_1_09);
    $result["C_1_10_5"] = setCommna($c_1_10);
    $result["C_1_11_6"] = setCommna($c_1_11);
    $result["C_2_1_5"]  = setCommna($c_2_1);
    $result["C_2_2_5"]  = setCommna($c_2_2);
    $result["C_3_5_09"] = setCommna($c_3_1);
    $result["C_3_6_5"]  = setCommna($c_3_2);
    $result["C_3_7_5"]  = setCommna($c_3_3);
    $result["C_4_1_5"]  = setCommna($c_4_1);
    $result["C_4_2_5"]  = setCommna($c_4_2);
    $result["C_4_3_5"]  = setCommna($c_4_3);
    $result["C_4_4_5"]  = setCommna($c_4_4);
    $result["C_4_5_5"]  = setCommna($c_4_5);
    $result["C_4_6_5"]  = setCommna($c_4_6);
    $result["C_4_7_6"]  = setCommna($c_4_7);
    $result["C_6_8_1"]  = setCommna($c_6_8);
    $result["C_6_9_1"]  = setCommna($c_6_9);
    $result["C_8_6_5"]  = setCommna($c_8_6);
    $result["C_9_1_5"]  = setCommna($c_9_1);
    $result["C_9_2_5"]  = setCommna($c_9_2);
    
    
    // 対応時間の設定
    $result["monday_time"]    = setTimeItemValue($result["C_6_4_01"],  $result["C_6_4_02"]);
    $result["tuesday_time"]   = setTimeItemValue($result["C_6_4_03"],  $result["C_6_4_04"]);
    $result["wednesday_time"] = setTimeItemValue($result["C_6_4_05"],  $result["C_6_4_06"]);
    $result["thursday_time"]  = setTimeItemValue($result["C_6_4_07"],  $result["C_6_4_08"]);
    $result["friday_time"]    = setTimeItemValue($result["C_6_4_09"],  $result["C_6_4_10"]);
    $result["saturday_time"]  = setTimeItemValue($result["C_6_5_1"] ,  $result["C_6_5_2"]);
    $result["sunday_time"]    = setTimeItemValue($result["C_6_6_1"] ,  $result["C_6_6_2"]);
    $result["holiday_time"]   = setTimeItemValue($result["C_6_7_1"] ,  $result["C_6_7_2"]);
    
            // 到着空港等の空港・方法の値を設定----------------------
    $air_contents = "";
    // 出迎え空港等が「有」の場合
    if($result["C_2_1_5_1"] == 1) {
        // 空港（名称）をセット
        $air_contents = $result["C_2_1_6"];
    }
    // 送迎方法が「有」の場合
    if($result["C_2_1_5_2"] == 1) {
        if ($air_contents != "") {
            $air_contents .= ",";
        }
        // 送迎方法（内容）をセット
        $air_contents .= $result["C_2_1_7"];
    }
    $result["C_2_1_air_contents"] = $air_contents;


    // 出国予定空港等の空港・方法の値を設定---------------------
    $air_contents = "";
    // 出迎え空港等2が「有」の場合、
    if($result["C_2_2_5_1"] == 1) {
        if($result["C_2_2_7"] == 1) {
            // 空港（名称）をセット
            $air_contents = $result["C_2_2_6"];
        } else {
            // 未定をセット
            $air_contents = getOprionItemValue($wk02004_14,   $result["C_2_2_7"]);
        }
    }
    // 送迎方法が「有」の場合、
    if($result["C_2_2_5_2"] == 1) {
        if ($air_contents != "") {
            $air_contents .= ",";
        }
        // 送迎方法（内容）をセット
        $air_contents .= $result["C_2_2_8"];
    }
    $result["C_2_2_air_contents"] = $air_contents;

    // 取得結果を返す。
    return $result;
}
/**
 *
 * 企業情報から取得し、支援担当者名を返す。
 * 
 * @param array[string]  $str 連結対象の配列
 * 
 * @return array[string] 担当者名
 */
function getAdminLRowId($db, $adminLRowId) {

    // SQLを生成。
    $sql = "SELECT "
    .     "mst_company.support_handler_name "

    . "FROM mst_company "
    . "WHERE  mst_company.id = :adminLRowId "   
    ;

    $params = array();
    $params[":adminLRowId"] = $adminLRowId;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

        // 取得結果を返す。
    return $result["support_handler_name"];
}
/**
 * 配列の内容を連結して返す。
 * 
 * @param array[string]  $str,$str2 連結対象の配列
 * 
 * @return string 連結した文字列
 */
function setTimeItemValue($str, $str2) {
    $buf = ""; 

    // 曜日の「から」、「まで」に両方値がある場合、「~」の文字列をセット
    if($str !="" && $str2 != ""){
        $buf = $str ." ~ ". $str2;
    } 

    return $buf;
}
/**
 * 配列の内容を連結して返す。
 * 
 * @param array[string]  $arr 連結対象の配列
 * @param string $delimitor 区切り文字
 * 
 * @return string 連結した文字列
 */
function setCommna($arr) {
    $buf = "";
    foreach ($arr as $item) {
        if ($item != "" && $buf != "") {
            $buf .= ",";
        }
        $buf .= $item;
    }
    return $buf;
}

