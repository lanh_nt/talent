<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * @param $pageNo ページ番号
 * 
 * @return PageData 検索結果
 */
function search($items, $pageNo) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    // SQLを生成。
    $sql = "SELECT "
    . "reportdata.id as r_id, "
    . "workers.worker_id as u_id, "
    . "workers.user_name, "
    . "workers.sex, "
    . "workers.qualifications, "
    . "DATE_FORMAT(reportdata.update_date, '%Y/%m/%d') AS update_date, "
    . "cmpy.company_name as company_name "

    . "FROM tbl_report_002 reportdata "
    . "INNER JOIN mst_workers workers "
    . "ON reportdata.worker_id = workers.id "
    . "LEFT JOIN mst_company cmpy "
    . "ON workers.company_id = cmpy.id "
    ;

    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY update_date desc,u_id ";

    // SQL文を発行する。
    $pageData = $db->getPageData($sql, $params, $pageNo);

    // 表示用データを追加設定。
    $sex           = getOptionItems("comm", "sex");
    $qualification = getOptionItems("comm", "qualification");

    // コードに対応する文言を設定。
    foreach ($pageData->pageDatas as &$row) {
        $row["sex_name"] = getOprionItemValue($sex,    $row["sex"]);
        $row["qualifications_name"] = getOprionItemValue($qualification,    $row["qualifications"]);
    }

    // 取得結果を返す。
    return $pageData;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $items     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($items, &$whereSqls, &$params) {
    // ・労働者ID 管理者ID
    if ($items["C_01"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "workers.worker_id = :u_id ";
        $params[":u_id"] = $items["C_01"];
    }

    // ・労働者氏名　 管理者氏名
    if ($items["C_02"] != "") {
        // 労働者姓/名に対して部分一致条件を設定。
        $whereSqls[] = "user_name LIKE :user_name ";
        $params[":user_name"] = "%".$items["C_02"]."%";
    }

    // ・性別
    if ($items["C_05_1"].$items["C_05_2"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c5Params = array();
        if ($items["C_05_1"] != "") $c5Params[] = "'1'";
        if ($items["C_05_2"] != "") $c5Params[] = "'2'";
        $c5ParamsString = joinArray($c5Params);
        $whereSqls[] = "sex IN (".$c5ParamsString.") ";
    }

    // ・企業名
    if ($items["C_06"] != "") {
        $whereSqls[] = "workers.company_id = :company_id ";
        $params[":company_id"] = $items["C_06"];
    }

    // ・登録日
    if ($items["C_07_1"] != "") {
        $whereSqls[] = "reportdata.update_date >= STR_TO_DATE(:update_date_from, '%Y-%m-%d 00:00') ";
        $params[":update_date_from"] = $items["C_07_1"];
    }
    if ($items["C_07_2"] != "") {
        $whereSqls[] = "reportdata.update_date < DATE_ADD(STR_TO_DATE(:update_date_to, '%Y-%m-%d 23:59'), INTERVAL 1 DAY) ";
        $params[":update_date_to"] = $items["C_07_2"];
    }
}


/**
 * 受入企業の一覧を取得する。
 *
 * @return array[行データ][キー => 値] 取得結果
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "WHERE classification = '1' "  // 受入機関
         . "ORDER BY company_name ";

    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}
/**
 * CSV出力用データを取得する。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function getExportDatas($items) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    // SQL文を生成。
    $sql = "SELECT "
    .     "workers.worker_id as u_id, "

    .     "reportdata.C_1_01_1, "
    .     "reportdata.C_1_01_2, "
    .     "reportdata.C_1_01_3,"
    .     "reportdata.C_1_01_4, "
    .     "reportdata.C_1_01_5_1, "
    .     "reportdata.C_1_01_5_2, "
    .     "reportdata.C_1_01_5_3, "
    .     "reportdata.C_1_01_6, "
    .     "reportdata.C_1_02_1, "
    .     "reportdata.C_1_02_2, "
    .     "reportdata.C_1_02_3, "
    .     "reportdata.C_1_02_4, "
    .     "reportdata.C_1_02_5_1, "
    .     "reportdata.C_1_02_5_2, "
    .     "reportdata.C_1_02_5_3, "
    .     "reportdata.C_1_02_6, "
    .     "reportdata.C_1_03_1, "
    .     "reportdata.C_1_03_2, "
    .     "reportdata.C_1_03_3, "
    .     "reportdata.C_1_03_4, "
    .     "reportdata.C_1_03_5_1, "
    .     "reportdata.C_1_03_5_2, "
    .     "reportdata.C_1_03_5_3, "
    .     "reportdata.C_1_03_6, "
    .     "reportdata.C_1_04_1, "
    .     "reportdata.C_1_04_2, "
    .     "reportdata.C_1_04_3, "
    .     "reportdata.C_1_04_4, "
    .     "reportdata.C_1_04_5_1, "
    .     "reportdata.C_1_04_5_2, "
    .     "reportdata.C_1_04_5_3, "
    .     "reportdata.C_1_04_6, "
    .     "reportdata.C_1_05_1, "
    .     "reportdata.C_1_05_2, "
    .     "reportdata.C_1_05_3, "
    .     "reportdata.C_1_05_4, "
    .     "reportdata.C_1_05_5_1, "
    .     "reportdata.C_1_05_5_2, "
    .     "reportdata.C_1_05_5_3, "
    .     "reportdata.C_1_05_6, "
    .     "reportdata.C_1_06_1, "
    .     "reportdata.C_1_06_2, "
    .     "reportdata.C_1_06_3, "
    .     "reportdata.C_1_06_4, "
    .     "reportdata.C_1_06_5_1, "
    .     "reportdata.C_1_06_5_2, "
    .     "reportdata.C_1_06_5_3, "
    .     "reportdata.C_1_06_6, "
    .     "reportdata.C_1_07_1, "
    .     "reportdata.C_1_07_2, "
    .     "reportdata.C_1_07_3, "
    .     "reportdata.C_1_07_4, "
    .     "reportdata.C_1_07_5_1, "
    .     "reportdata.C_1_07_5_2, "
    .     "reportdata.C_1_07_5_3, "
    .     "reportdata.C_1_07_6, "
    .     "reportdata.C_1_08_1, "
    .     "reportdata.C_1_08_2, "
    .     "reportdata.C_1_08_3, "
    .     "reportdata.C_1_08_4, "
    .     "reportdata.C_1_08_5_1, "
    .     "reportdata.C_1_08_5_2, "
    .     "reportdata.C_1_08_5_3, "
    .     "reportdata.C_1_08_6, "
    .     "reportdata.C_1_09_1, "
    .     "reportdata.C_1_09_2, "
    .     "reportdata.C_1_09_3, "
    .     "reportdata.C_1_09_4, "
    .     "reportdata.C_1_09_5_1, "
    .     "reportdata.C_1_09_5_2, "
    .     "reportdata.C_1_09_5_3, "
    .     "reportdata.C_1_09_6, "
    .     "reportdata.C_1_10_1, "
    .     "reportdata.C_1_10_2, "
    .     "reportdata.C_1_10_3, "
    .     "reportdata.C_1_10_4, "
    .     "reportdata.C_1_10_5_1, "
    .     "reportdata.C_1_10_5_2, "
    .     "reportdata.C_1_10_5_3, "
    .     "reportdata.C_1_10_6, "
    .     "reportdata.C_1_11_1, "
    .     "reportdata.C_1_11_2, "
    .     "reportdata.C_1_11_3, "
    .     "reportdata.C_1_11_4, "
    .     "reportdata.C_1_11_5, "
    .     "reportdata.C_1_11_6_1, "
    .     "reportdata.C_1_11_6_2, "
    .     "reportdata.C_1_11_6_3, "
    .     "reportdata.C_1_11_7, "
    .     "reportdata.C_1_12, "
    .     "reportdata.C_1_13, "

    .     "reportdata.C_2_1_1, "
    .     "reportdata.C_2_1_2, "
    .     "reportdata.C_2_1_3, "
    .     "reportdata.C_2_1_4, "
    .     "reportdata.C_2_1_5_1, "
    .     "reportdata.C_2_1_5_2, "
    .     "reportdata.C_2_1_6, "
    .     "reportdata.C_2_1_7, "
    .     "reportdata.C_2_2_1, "
    .     "reportdata.C_2_2_2, "
    .     "reportdata.C_2_2_3, "
    .     "reportdata.C_2_2_4, "
    .     "reportdata.C_2_2_5_1, "
    .     "reportdata.C_2_2_5_2, "
    .     "reportdata.C_2_2_6, "
    .     "reportdata.C_2_2_7, "
    .     "reportdata.C_2_2_8, "
    .     "reportdata.C_2_3_1, "
    .     "reportdata.C_2_3_2, "
    .     "reportdata.C_2_3_3, "
    .     "reportdata.C_2_3_4, "
    .     "reportdata.C_2_3_5, "
    .     "reportdata.C_2_3_6, "

    .     "reportdata.C_3_1_1, "
    .     "reportdata.C_3_1_2, "
    .     "reportdata.C_3_1_3, "
    .     "reportdata.C_3_1_4, "
    .     "reportdata.C_3_2_1, "
    .     "reportdata.C_3_2_2, "
    .     "reportdata.C_3_2_3, "
    .     "reportdata.C_3_2_4, "
    .     "reportdata.C_3_3_1, "
    .     "reportdata.C_3_3_2, "
    .     "reportdata.C_3_3_3, "
    .     "reportdata.C_3_3_4, "
    .     "reportdata.C_3_4_1, "
    .     "reportdata.C_3_4_2, "
    .     "reportdata.C_3_4_3, "
    .     "reportdata.C_3_4_4, "
    .     "reportdata.C_3_4_5, "
    .     "reportdata.C_3_4_6, "
    .     "reportdata.C_3_5_01, "
    .     "reportdata.C_3_5_02, "
    .     "reportdata.C_3_5_03, "
    .     "reportdata.C_3_5_04, "
    .     "reportdata.C_3_5_05, "
    .     "reportdata.C_3_5_06, "
    .     "reportdata.C_3_5_07, "
    .     "reportdata.C_3_5_08, "
    .     "reportdata.C_3_5_09_1, "
    .     "reportdata.C_3_5_09_2, "
    .     "reportdata.C_3_5_09_3, "
    .     "reportdata.C_3_5_10, "
    .     "reportdata.C_3_6_1, "
    .     "reportdata.C_3_6_2, "
    .     "reportdata.C_3_6_3, "
    .     "reportdata.C_3_6_4, "
    .     "reportdata.C_3_6_5_1, "
    .     "reportdata.C_3_6_5_2, "
    .     "reportdata.C_3_6_5_3, "
    .     "reportdata.C_3_6_6, "
    .     "reportdata.C_3_7_1, "
    .     "reportdata.C_3_7_2, "
    .     "reportdata.C_3_7_3, "
    .     "reportdata.C_3_7_4, "
    .     "reportdata.C_3_7_5_1, "
    .     "reportdata.C_3_7_5_2, "
    .     "reportdata.C_3_7_5_3, "
    .     "reportdata.C_3_7_6, "
    .     "reportdata.C_3_8_1, "
    .     "reportdata.C_3_8_2, "
    .     "reportdata.C_3_8_3, "
    .     "reportdata.C_3_8_4, "
    .     "reportdata.C_3_8_5, "
    .     "reportdata.C_3_8_6, "

    .     "reportdata.C_4_1_1, "
    .     "reportdata.C_4_1_2, "
    .     "reportdata.C_4_1_3, "
    .     "reportdata.C_4_1_4, "
    .     "reportdata.C_4_1_5_1, "
    .     "reportdata.C_4_1_5_2, "
    .     "reportdata.C_4_1_5_3, "
    .     "reportdata.C_4_1_6, "
    .     "reportdata.C_4_2_1, "
    .     "reportdata.C_4_2_2, "
    .     "reportdata.C_4_2_3, "
    .     "reportdata.C_4_2_4, "
    .     "reportdata.C_4_2_5_1, "
    .     "reportdata.C_4_2_5_2, "
    .     "reportdata.C_4_2_5_3, "
    .     "reportdata.C_4_2_5_4, "
    .     "reportdata.C_4_2_6, "
    .     "reportdata.C_4_3_1, "
    .     "reportdata.C_4_3_2, "
    .     "reportdata.C_4_3_3, "
    .     "reportdata.C_4_3_4, "
    .     "reportdata.C_4_3_5_1, "
    .     "reportdata.C_4_3_5_2, "
    .     "reportdata.C_4_3_5_3, "
    .     "reportdata.C_4_3_6, "
    .     "reportdata.C_4_4_1, "
    .     "reportdata.C_4_4_2, "
    .     "reportdata.C_4_4_3, "
    .     "reportdata.C_4_4_4, "
    .     "reportdata.C_4_4_5_1, "
    .     "reportdata.C_4_4_5_2, "
    .     "reportdata.C_4_4_5_3, "
    .     "reportdata.C_4_4_6, "
    .     "reportdata.C_4_5_1, "
    .     "reportdata.C_4_5_2, "
    .     "reportdata.C_4_5_3, "
    .     "reportdata.C_4_5_4, "
    .     "reportdata.C_4_5_5_1, "
    .     "reportdata.C_4_5_5_2, "
    .     "reportdata.C_4_5_5_3, "
    .     "reportdata.C_4_5_6, "
    .     "reportdata.C_4_6_1, "
    .     "reportdata.C_4_6_2, "
    .     "reportdata.C_4_6_3, "
    .     "reportdata.C_4_6_4, "
    .     "reportdata.C_4_6_5_1, "
    .     "reportdata.C_4_6_5_2, "
    .     "reportdata.C_4_6_5_3, "
    .     "reportdata.C_4_6_6, "
    .     "reportdata.C_4_7_1, "
    .     "reportdata.C_4_7_2, "
    .     "reportdata.C_4_7_3, "
    .     "reportdata.C_4_7_4, "
    .     "reportdata.C_4_7_5, "
    .     "reportdata.C_4_7_6_1, "
    .     "reportdata.C_4_7_6_2, "
    .     "reportdata.C_4_7_6_3, "
    .     "reportdata.C_4_7_7, "
    .     "reportdata.C_4_8, "
    .     "reportdata.C_4_9, "

    .     "reportdata.C_5_1_1, "
    .     "reportdata.C_5_1_2, "
    .     "reportdata.C_5_1_3, "
    .     "reportdata.C_5_1_4, "
    .     "reportdata.C_5_2_1, "
    .     "reportdata.C_5_2_2, "
    .     "reportdata.C_5_2_3, "
    .     "reportdata.C_5_2_4, "
    .     "reportdata.C_5_3_1, "
    .     "reportdata.C_5_3_2, "
    .     "reportdata.C_5_3_3, "
    .     "reportdata.C_5_3_4, "
    .     "reportdata.C_5_4_1, "
    .     "reportdata.C_5_4_2, "
    .     "reportdata.C_5_4_3, "
    .     "reportdata.C_5_4_4, "
    .     "reportdata.C_5_4_5, "
    .     "reportdata.C_5_4_6, "

    .     "reportdata.C_6_1_1, "
    .     "reportdata.C_6_1_2, "
    .     "reportdata.C_6_1_3, "
    .     "reportdata.C_6_1_4, "
    .     "reportdata.C_6_2_1, "
    .     "reportdata.C_6_2_2, "
    .     "reportdata.C_6_2_3, "
    .     "reportdata.C_6_2_4, "
    .     "reportdata.C_6_3_1, "
    .     "reportdata.C_6_3_2, "
    .     "reportdata.C_6_3_3, "
    .     "reportdata.C_6_3_4, "
    .     "reportdata.C_6_3_5, "
    .     "reportdata.C_6_4_01, "
    .     "reportdata.C_6_4_02, "
    .     "reportdata.C_6_4_03, "
    .     "reportdata.C_6_4_04, "
    .     "reportdata.C_6_4_05, "
    .     "reportdata.C_6_4_06, "
    .     "reportdata.C_6_4_07, "
    .     "reportdata.C_6_4_08, "
    .     "reportdata.C_6_4_09, "
    .     "reportdata.C_6_4_10, "
    .     "reportdata.C_6_5_1, "
    .     "reportdata.C_6_5_2, "
    .     "reportdata.C_6_6_1, "
    .     "reportdata.C_6_6_2, "
    .     "reportdata.C_6_7_1, "
    .     "reportdata.C_6_7_2, "
    .     "reportdata.C_6_8_1_1, "
    .     "reportdata.C_6_8_1_2, "
    .     "reportdata.C_6_8_1_3, "
    .     "reportdata.C_6_8_1_4, "
    .     "reportdata.C_6_8_2, "
    .     "reportdata.C_6_8_3, "
    .     "reportdata.C_6_8_4, "
    .     "reportdata.C_6_9_1_1, "
    .     "reportdata.C_6_9_1_2, "
    .     "reportdata.C_6_9_1_3, "
    .     "reportdata.C_6_9_1_4, "
    .     "reportdata.C_6_9_2, "
    .     "reportdata.C_6_9_3, "
    .     "reportdata.C_6_9_4, "
    .     "reportdata.C_6_9_5, "

    .     "reportdata.C_7_1_1, "
    .     "reportdata.C_7_1_2, "
    .     "reportdata.C_7_1_3, "
    .     "reportdata.C_7_1_4, "
    .     "reportdata.C_7_2_1, "
    .     "reportdata.C_7_2_2, "
    .     "reportdata.C_7_2_3, "
    .     "reportdata.C_7_2_4, "
    .     "reportdata.C_7_3_1, "
    .     "reportdata.C_7_3_2, "
    .     "reportdata.C_7_3_3, "
    .     "reportdata.C_7_3_4, "
    .     "reportdata.C_7_3_5, "
    .     "reportdata.C_7_3_6, "

    .     "reportdata.C_8_1_1, "
    .     "reportdata.C_8_1_2, "
    .     "reportdata.C_8_1_3, "
    .     "reportdata.C_8_1_4, "
    .     "reportdata.C_8_2_1, "
    .     "reportdata.C_8_2_2, "
    .     "reportdata.C_8_2_3, "
    .     "reportdata.C_8_2_4, "
    .     "reportdata.C_8_3_1, "
    .     "reportdata.C_8_3_2, "
    .     "reportdata.C_8_3_3, "
    .     "reportdata.C_8_3_4, "
    .     "reportdata.C_8_4_1, "
    .     "reportdata.C_8_4_2, "
    .     "reportdata.C_8_4_3, "
    .     "reportdata.C_8_4_4, "
    .     "reportdata.C_8_5_1, "
    .     "reportdata.C_8_5_2, "
    .     "reportdata.C_8_6_1, "
    .     "reportdata.C_8_6_2, "
    .     "reportdata.C_8_6_3, "
    .     "reportdata.C_8_6_4, "
    .     "reportdata.C_8_6_5_1, "
    .     "reportdata.C_8_6_5_2, "
    .     "reportdata.C_8_6_5_3, "
    .     "reportdata.C_8_6_6, "
    .     "reportdata.C_8_7_1, "
    .     "reportdata.C_8_7_2, "
    .     "reportdata.C_8_7_3, "
    .     "reportdata.C_8_7_4, "
    .     "reportdata.C_8_8_1, "
    .     "reportdata.C_8_8_2, "
    .     "reportdata.C_8_8_3, "
    .     "reportdata.C_8_8_4, "
    .     "reportdata.C_8_8_5, "
    .     "reportdata.C_8_8_6, "
            
    .     "reportdata.C_9_1_1, "
    .     "reportdata.C_9_1_2, "
    .     "reportdata.C_9_1_3, "
    .     "reportdata.C_9_1_4, "
    .     "reportdata.C_9_1_5_1, "
    .     "reportdata.C_9_1_5_2, "
    .     "reportdata.C_9_1_5_3, "
    .     "reportdata.C_9_1_6, "
    .     "reportdata.C_9_2_1, "
    .     "reportdata.C_9_2_2, "
    .     "reportdata.C_9_2_3, "
    .     "reportdata.C_9_2_4, "
    .     "reportdata.C_9_2_5_1, "
    .     "reportdata.C_9_2_5_2, "
    .     "reportdata.C_9_2_5_3, "
    .     "reportdata.C_9_2_6, "
    .     "reportdata.C_9_3_1, "
    .     "reportdata.C_9_3_2, "
    .     "reportdata.C_9_3_3, "
    .     "reportdata.C_9_3_4, "
    .     "reportdata.C_9_4_1, "
    .     "reportdata.C_9_4_2, "
    .     "reportdata.C_9_4_3, "
    .     "reportdata.C_9_4_4, "
    .     "reportdata.C_9_5_1, "
    .     "reportdata.C_9_5_2, "
    .     "reportdata.C_9_5_3, "
    .     "reportdata.C_9_5_4, "
    .     "reportdata.C_9_5_5, "
    .     "reportdata.C_9_5_6, "
    .     "reportdata.C_9_6, "
    .     "reportdata.C_9_7 "

    . "FROM tbl_report_002 reportdata "
    . "INNER JOIN mst_workers workers "
    . "ON reportdata.worker_id = workers.id "
    . "LEFT JOIN mst_company cmpy "
    . "ON workers.company_id = cmpy.id "
    ;

    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY reportdata.update_date desc,u_id ";

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 企業のrowIDをIDに変換する。
    $companyDataCache = array();
    foreach ($result as &$row) {
        $row["C_1_01_4" ] = convCompanyId($db, $row["C_1_01_4" ], $companyDataCache);
        $row["C_1_02_4" ] = convCompanyId($db, $row["C_1_02_4" ], $companyDataCache);
        $row["C_1_03_4" ] = convCompanyId($db, $row["C_1_03_4" ], $companyDataCache);
        $row["C_1_04_4" ] = convCompanyId($db, $row["C_1_04_4" ], $companyDataCache);
        $row["C_1_05_4" ] = convCompanyId($db, $row["C_1_05_4" ], $companyDataCache);
        $row["C_1_06_4" ] = convCompanyId($db, $row["C_1_06_4" ], $companyDataCache);
        $row["C_1_07_4" ] = convCompanyId($db, $row["C_1_07_4" ], $companyDataCache);
        $row["C_1_08_4" ] = convCompanyId($db, $row["C_1_08_4" ], $companyDataCache);
        $row["C_1_09_4" ] = convCompanyId($db, $row["C_1_09_4" ], $companyDataCache);
        $row["C_1_10_4" ] = convCompanyId($db, $row["C_1_10_4" ], $companyDataCache);
        $row["C_1_11_5" ] = convCompanyId($db, $row["C_1_11_5" ], $companyDataCache);
        $row["C_2_1_4"  ] = convCompanyId($db, $row["C_2_1_4"  ], $companyDataCache);
        $row["C_2_2_4"  ] = convCompanyId($db, $row["C_2_2_4"  ], $companyDataCache);
        $row["C_2_3_5"  ] = convCompanyId($db, $row["C_2_3_5"  ], $companyDataCache);
        $row["C_3_1_4"  ] = convCompanyId($db, $row["C_3_1_4"  ], $companyDataCache);
        $row["C_3_2_4"  ] = convCompanyId($db, $row["C_3_2_4"  ], $companyDataCache);
        $row["C_3_3_4"  ] = convCompanyId($db, $row["C_3_3_4"  ], $companyDataCache);
        $row["C_3_4_5"  ] = convCompanyId($db, $row["C_3_4_5"  ], $companyDataCache);
        $row["C_3_5_08" ] = convCompanyId($db, $row["C_3_5_08" ], $companyDataCache);
        $row["C_3_6_4"  ] = convCompanyId($db, $row["C_3_6_4"  ], $companyDataCache);
        $row["C_3_7_4"  ] = convCompanyId($db, $row["C_3_7_4"  ], $companyDataCache);
        $row["C_3_8_5"  ] = convCompanyId($db, $row["C_3_8_5"  ], $companyDataCache);
        $row["C_4_1_4"  ] = convCompanyId($db, $row["C_4_1_4"  ], $companyDataCache);
        $row["C_4_2_4"  ] = convCompanyId($db, $row["C_4_2_4"  ], $companyDataCache);
        $row["C_4_3_4"  ] = convCompanyId($db, $row["C_4_3_4"  ], $companyDataCache);
        $row["C_4_4_4"  ] = convCompanyId($db, $row["C_4_4_4"  ], $companyDataCache);
        $row["C_4_5_4"  ] = convCompanyId($db, $row["C_4_5_4"  ], $companyDataCache);
        $row["C_4_6_4"  ] = convCompanyId($db, $row["C_4_6_4"  ], $companyDataCache);
        $row["C_4_7_5"  ] = convCompanyId($db, $row["C_4_7_5"  ], $companyDataCache);
        $row["C_5_1_4"  ] = convCompanyId($db, $row["C_5_1_4"  ], $companyDataCache);
        $row["C_5_2_4"  ] = convCompanyId($db, $row["C_5_2_4"  ], $companyDataCache);
        $row["C_5_3_4"  ] = convCompanyId($db, $row["C_5_3_4"  ], $companyDataCache);
        $row["C_5_4_5"  ] = convCompanyId($db, $row["C_5_4_5"  ], $companyDataCache);
        $row["C_6_1_4"  ] = convCompanyId($db, $row["C_6_1_4"  ], $companyDataCache);
        $row["C_6_2_4"  ] = convCompanyId($db, $row["C_6_2_4"  ], $companyDataCache);
        $row["C_6_3_5"  ] = convCompanyId($db, $row["C_6_3_5"  ], $companyDataCache);
        $row["C_7_1_4"  ] = convCompanyId($db, $row["C_7_1_4"  ], $companyDataCache);
        $row["C_7_2_4"  ] = convCompanyId($db, $row["C_7_2_4"  ], $companyDataCache);
        $row["C_7_3_5"  ] = convCompanyId($db, $row["C_7_3_5"  ], $companyDataCache);
        $row["C_8_1_4"  ] = convCompanyId($db, $row["C_8_1_4"  ], $companyDataCache);
        $row["C_8_2_4"  ] = convCompanyId($db, $row["C_8_2_4"  ], $companyDataCache);
        $row["C_8_3_4"  ] = convCompanyId($db, $row["C_8_3_4"  ], $companyDataCache);
        $row["C_8_4_4"  ] = convCompanyId($db, $row["C_8_4_4"  ], $companyDataCache);
        $row["C_8_6_4"  ] = convCompanyId($db, $row["C_8_6_4"  ], $companyDataCache);
        $row["C_8_7_4"  ] = convCompanyId($db, $row["C_8_7_4"  ], $companyDataCache);
        $row["C_8_8_5"  ] = convCompanyId($db, $row["C_8_8_5"  ], $companyDataCache);
        $row["C_9_1_4"  ] = convCompanyId($db, $row["C_9_1_4"  ], $companyDataCache);
        $row["C_9_2_4"  ] = convCompanyId($db, $row["C_9_2_4"  ], $companyDataCache);
        $row["C_9_3_4"  ] = convCompanyId($db, $row["C_9_3_4"  ], $companyDataCache);
        $row["C_9_4_4"  ] = convCompanyId($db, $row["C_9_4_4"  ], $companyDataCache);
        $row["C_9_5_5"  ] = convCompanyId($db, $row["C_9_5_5"  ], $companyDataCache);
    }

    // 取得結果を返す。
    return $result;
}

/**
 * 企業のrowIDをIDに変換する。
 *
 * @param [type] $db DB接続
 * @param [type] $companyRowId 行ID
 * @param [type] $cacheArray データキャッシュ
 * 
 * @return string 企業ID
 */
function convCompanyId($db, $companyRowId, &$cacheArray) {
    // ID未設定なら、空文字を返して終了。
    if ($companyRowId == null or $companyRowId == "") return "";

    // 検索済みなら、キャッシュから内容を取得して返す。
    if (isset($cacheArray[$companyRowId])) return $cacheArray[$companyRowId];

    // DBに問合せる。
    $sql = "SELECT company_id "
         . "FROM mst_company "
         . "WHERE id = :row_id ";
    $params = array(":row_id" => $companyRowId);
    $result = $db->selectOne($sql, $params);
    $companyId = ($result != null) ? $result["company_id"] : "";

    // キャッシュに問合せ結果を追加。
    $cacheArray[$companyRowId] = $companyId;

    // 取得した情報を返す。
    return $companyId;
}


/**
 * CSVデータを登録する。
 *
 * @param array[カラム] $datas CSVデータ
 * 
 * @return boolean 実行成否
 */
function csvDataImport($datas) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {

        foreach ($datas as $row) {

            // 労働者マスタに作成したレコードのIDを取得する。
            $workerRowId = $db->getWorkerRowId($row[COL_WORKER_ID]);

            $sql = "SELECT COUNT(*) AS cnt "
                    . "FROM tbl_report_002 "
                    . "WHERE worker_id = :worker_id ";
            $params[":worker_id"] = $workerRowId;
            $result = $db->selectOne($sql, $params);
            $noMatchRecord = ($result["cnt"] == 0);
            if ($noMatchRecord) {
                // 行データあり&既存データなし : INSERT
                csvInsert($db, $row, $workerRowId);
            } else {
                // 行データあり&既存データあり : UPDATE
                csvUpdate($db, $row, $workerRowId);
            }
        }

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);
        //echo $e->getMessage();
        return false;
    }

    return true;
}
/**
 * データを新規登録する。
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 * 
 * @return integer レコード件数
 */
function csvInsert($db, $row, $workerRowId) {
    //-----------------------------------------------
    // 支援計画にデータを新規作成する。
    //-----------------------------------------------

    // カラムと設定値を定義。
    $columns = array();
    $columns["worker_id" ] = $workerRowId;
    setColumns($db, $columns, $row);

    // insert処理を発行する。
    $rc = $db->insert("tbl_report_002", $columns);

    $rcolumns = array(
        "worker_id" => $columns["worker_id" ]
    );

    return $rc;
}
/**
 * データを更新する。
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 * 
 * @return integer レコード件数
 */
function csvUpdate($db, $row, $workerRowId) {
    //-----------------------------------------------
    // 支援計画データを更新する。
    //-----------------------------------------------

    // カラムと設定値を定義。
    $columns = array();
    setColumns($db, $columns, $row);

    $whereString = "worker_id = :worker_id ";
    $whereParams = array(
        "worker_id" => $workerRowId
    );

    // update処理を発行する。
    $rc = $db->update("tbl_report_002", $columns, $whereString, $whereParams);
    return $rc;
}

/*
*DBのinteger対策用null判定関数
*/
function setIntegerCol($dat_str, $addingString = "") {
    // 値が未設定なら null を返す。
    if ($dat_str == null or $dat_str == "") {
        return null;
    }

    // 渡された値を返す。
    return $dat_str.$addingString;
}

/**
 * CSVレコードの内容から、更新用データを生成する。
 *
 * @param array[DBカラム名 => 値] $columns 更新用カラム一覧
 * @param array[値] $row CSV行データ
 */
function setColumns($db, &$columns, $row) {
    $rowid = 2 - 1;

    // 事前ガイダンスの提供
    $columns["C_1_01_1"]    = $row[$rowid++];
    $columns["C_1_01_2"]    = $row[$rowid++];
    $columns["C_1_01_3"]    = $row[$rowid++];
    $columns["C_1_01_4"]    = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_1_01_5_1"]  = $row[$rowid++];
    $columns["C_1_01_5_2"]  = $row[$rowid++];
    $columns["C_1_01_5_3"]  = $row[$rowid++];
    $columns["C_1_01_6"]    = $row[$rowid++];
    $columns["C_1_02_1"]    = $row[$rowid++];
    $columns["C_1_02_2"]    = $row[$rowid++];
    $columns["C_1_02_3"]    = $row[$rowid++];
    $columns["C_1_02_4"]    = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_1_02_5_1"]  = $row[$rowid++];
    $columns["C_1_02_5_2"]  = $row[$rowid++];
    $columns["C_1_02_5_3"]  = $row[$rowid++];
    $columns["C_1_02_6"]    = $row[$rowid++];
    $columns["C_1_03_1"]    = $row[$rowid++];
    $columns["C_1_03_2"]    = $row[$rowid++];
    $columns["C_1_03_3"]    = $row[$rowid++];
    $columns["C_1_03_4"]    = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_1_03_5_1"]  = $row[$rowid++];
    $columns["C_1_03_5_2"]  = $row[$rowid++];
    $columns["C_1_03_5_3"]  = $row[$rowid++];
    $columns["C_1_03_6"]    = $row[$rowid++];
    $columns["C_1_04_1"]    = $row[$rowid++];
    $columns["C_1_04_2"]    = $row[$rowid++];
    $columns["C_1_04_3"]    = $row[$rowid++];
    $columns["C_1_04_4"]    = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_1_04_5_1"]  = $row[$rowid++];
    $columns["C_1_04_5_2"]  = $row[$rowid++];
    $columns["C_1_04_5_3"]  = $row[$rowid++];
    $columns["C_1_04_6"]    = $row[$rowid++];
    $columns["C_1_05_1"]    = $row[$rowid++];
    $columns["C_1_05_2"]    = $row[$rowid++];
    $columns["C_1_05_3"]    = $row[$rowid++];
    $columns["C_1_05_4"]    = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_1_05_5_1"]  = $row[$rowid++];
    $columns["C_1_05_5_2"]  = $row[$rowid++];
    $columns["C_1_05_5_3"]  = $row[$rowid++];
    $columns["C_1_05_6"]    = $row[$rowid++];
    $columns["C_1_06_1"]    = $row[$rowid++];
    $columns["C_1_06_2"]    = $row[$rowid++];
    $columns["C_1_06_3"]    = $row[$rowid++];
    $columns["C_1_06_4"]    = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_1_06_5_1"]  = $row[$rowid++];
    $columns["C_1_06_5_2"]  = $row[$rowid++];
    $columns["C_1_06_5_3"]  = $row[$rowid++];
    $columns["C_1_06_6"]    = $row[$rowid++];
    $columns["C_1_07_1"]    = $row[$rowid++];
    $columns["C_1_07_2"]    = $row[$rowid++];
    $columns["C_1_07_3"]    = $row[$rowid++];
    $columns["C_1_07_4"]    = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_1_07_5_1"]  = $row[$rowid++];
    $columns["C_1_07_5_2"]  = $row[$rowid++];
    $columns["C_1_07_5_3"]  = $row[$rowid++];
    $columns["C_1_07_6"]    = $row[$rowid++];
    $columns["C_1_08_1"]    = $row[$rowid++];
    $columns["C_1_08_2"]    = $row[$rowid++];
    $columns["C_1_08_3"]    = $row[$rowid++];
    $columns["C_1_08_4"]    = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_1_08_5_1"]  = $row[$rowid++];
    $columns["C_1_08_5_2"]  = $row[$rowid++];
    $columns["C_1_08_5_3"]  = $row[$rowid++];
    $columns["C_1_08_6"]    = $row[$rowid++];
    $columns["C_1_09_1"]    = $row[$rowid++];
    $columns["C_1_09_2"]    = $row[$rowid++];
    $columns["C_1_09_3"]    = $row[$rowid++];
    $columns["C_1_09_4"]    = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_1_09_5_1"]  = $row[$rowid++];
    $columns["C_1_09_5_2"]  = $row[$rowid++];
    $columns["C_1_09_5_3"]  = $row[$rowid++];
    $columns["C_1_09_6"]    = $row[$rowid++];
    $columns["C_1_10_1"]    = $row[$rowid++];
    $columns["C_1_10_2"]    = $row[$rowid++];
    $columns["C_1_10_3"]    = $row[$rowid++];
    $columns["C_1_10_4"]    = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_1_10_5_1"]  = $row[$rowid++];
    $columns["C_1_10_5_2"]  = $row[$rowid++];
    $columns["C_1_10_5_3"]  = $row[$rowid++];
    $columns["C_1_10_6"]    = $row[$rowid++];
    $columns["C_1_11_1"]    = $row[$rowid++];
    $columns["C_1_11_2"]    = $row[$rowid++];
    $columns["C_1_11_3"]    = $row[$rowid++];
    $columns["C_1_11_4"]    = $row[$rowid++];
    $columns["C_1_11_5"]    = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_1_11_6_1"]  = $row[$rowid++];
    $columns["C_1_11_6_2"]  = $row[$rowid++];
    $columns["C_1_11_6_3"]  = $row[$rowid++];
    $columns["C_1_11_7"]    = $row[$rowid++];
    $columns["C_1_12"]      = $row[$rowid++];
    $columns["C_1_13"]      = $row[$rowid++];
    
    // 出入国する際の送迎
    $columns["C_2_1_1"]     = $row[$rowid++];
    $columns["C_2_1_2"]     = $row[$rowid++];
    $columns["C_2_1_3"]     = $row[$rowid++];
    $columns["C_2_1_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_2_1_5_1"]   = $row[$rowid++];
    $columns["C_2_1_5_2"]   = $row[$rowid++];
    $columns["C_2_1_6"]     = $row[$rowid++];
    $columns["C_2_1_7"]     = $row[$rowid++];
    $columns["C_2_2_1"]     = $row[$rowid++];
    $columns["C_2_2_2"]     = $row[$rowid++];
    $columns["C_2_2_3"]     = $row[$rowid++];
    $columns["C_2_2_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_2_2_5_1"]   = $row[$rowid++];
    $columns["C_2_2_5_2"]   = $row[$rowid++];
    $columns["C_2_2_6"]     = $row[$rowid++];
    $columns["C_2_2_7"]     = $row[$rowid++];
    $columns["C_2_2_8"]     = $row[$rowid++];
    $columns["C_2_3_1"]     = $row[$rowid++];
    $columns["C_2_3_2"]     = $row[$rowid++];
    $columns["C_2_3_3"]     = $row[$rowid++];
    $columns["C_2_3_4"]     = $row[$rowid++];
    $columns["C_2_3_5"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_2_3_6"]     = $row[$rowid++];
    
    // 適切な住居の確保に係る支援・生活に必要な契約に係る支援
    $columns["C_3_1_1"]     = $row[$rowid++];
    $columns["C_3_1_2"]     = $row[$rowid++];
    $columns["C_3_1_3"]     = $row[$rowid++];
    $columns["C_3_1_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_3_2_1"]     = $row[$rowid++];
    $columns["C_3_2_2"]     = $row[$rowid++];
    $columns["C_3_2_3"]     = $row[$rowid++];
    $columns["C_3_2_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_3_3_1"]     = $row[$rowid++];
    $columns["C_3_3_2"]     = $row[$rowid++];
    $columns["C_3_3_3"]     = $row[$rowid++];
    $columns["C_3_3_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_3_4_1"]     = $row[$rowid++];
    $columns["C_3_4_2"]     = $row[$rowid++];
    $columns["C_3_4_3"]     = $row[$rowid++];
    $columns["C_3_4_4"]     = $row[$rowid++];
    $columns["C_3_4_5"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_3_4_6"]     = $row[$rowid++];
    $columns["C_3_5_01"]    = $row[$rowid++];
    $columns["C_3_5_02"]    = $row[$rowid++];
    $columns["C_3_5_03"]    = $row[$rowid++];
    $columns["C_3_5_04"]    = $row[$rowid++];
    $columns["C_3_5_05"]    = $row[$rowid++];
    $columns["C_3_5_06"]    = $row[$rowid++];
    $columns["C_3_5_07"]    = $row[$rowid++];
    $columns["C_3_5_08"]    = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_3_5_09_1"]  = $row[$rowid++];
    $columns["C_3_5_09_2"]  = $row[$rowid++];
    $columns["C_3_5_09_3"]  = $row[$rowid++];
    $columns["C_3_5_10"]    = $row[$rowid++];
    $columns["C_3_6_1"]     = $row[$rowid++];
    $columns["C_3_6_2"]     = $row[$rowid++];
    $columns["C_3_6_3"]     = $row[$rowid++];
    $columns["C_3_6_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_3_6_5_1"]   = $row[$rowid++];
    $columns["C_3_6_5_2"]   = $row[$rowid++];
    $columns["C_3_6_5_3"]   = $row[$rowid++];
    $columns["C_3_6_6"]     = $row[$rowid++];
    $columns["C_3_7_1"]     = $row[$rowid++];
    $columns["C_3_7_2"]     = $row[$rowid++];
    $columns["C_3_7_3"]     = $row[$rowid++];
    $columns["C_3_7_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_3_7_5_1"]   = $row[$rowid++];
    $columns["C_3_7_5_2"]   = $row[$rowid++];
    $columns["C_3_7_5_3"]   = $row[$rowid++];
    $columns["C_3_7_6"]     = $row[$rowid++];
    $columns["C_3_8_1"]     = $row[$rowid++];
    $columns["C_3_8_2"]     = $row[$rowid++];
    $columns["C_3_8_3"]     = $row[$rowid++];
    $columns["C_3_8_4"]     = $row[$rowid++];
    $columns["C_3_8_5"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_3_8_6"]     = $row[$rowid++];
    
    // 生活オリエンテーションの実施 
    $columns["C_4_1_1"]     = $row[$rowid++];
    $columns["C_4_1_2"]     = $row[$rowid++];
    $columns["C_4_1_3"]     = $row[$rowid++];
    $columns["C_4_1_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_4_1_5_1"]   = $row[$rowid++];
    $columns["C_4_1_5_2"]   = $row[$rowid++];
    $columns["C_4_1_5_3"]   = $row[$rowid++];
    $columns["C_4_1_6"]     = $row[$rowid++];
    $columns["C_4_2_1"]     = $row[$rowid++];
    $columns["C_4_2_2"]     = $row[$rowid++];
    $columns["C_4_2_3"]     = $row[$rowid++];
    $columns["C_4_2_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_4_2_5_1"]   = $row[$rowid++];
    $columns["C_4_2_5_2"]   = $row[$rowid++];
    $columns["C_4_2_5_3"]   = $row[$rowid++];
    $columns["C_4_2_5_4"]   = $row[$rowid++];
    $columns["C_4_2_6"]     = $row[$rowid++];
    $columns["C_4_3_1"]     = $row[$rowid++];
    $columns["C_4_3_2"]     = $row[$rowid++];
    $columns["C_4_3_3"]     = $row[$rowid++];
    $columns["C_4_3_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_4_3_5_1"]   = $row[$rowid++];
    $columns["C_4_3_5_2"]   = $row[$rowid++];
    $columns["C_4_3_5_3"]   = $row[$rowid++];
    $columns["C_4_3_6"]     = $row[$rowid++];
    $columns["C_4_4_1"]     = $row[$rowid++];
    $columns["C_4_4_2"]     = $row[$rowid++];
    $columns["C_4_4_3"]     = $row[$rowid++];
    $columns["C_4_4_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_4_4_5_1"]   = $row[$rowid++];
    $columns["C_4_4_5_2"]   = $row[$rowid++];
    $columns["C_4_4_5_3"]   = $row[$rowid++];
    $columns["C_4_4_6"]     = $row[$rowid++];
    $columns["C_4_5_1"]     = $row[$rowid++];
    $columns["C_4_5_2"]     = $row[$rowid++];
    $columns["C_4_5_3"]     = $row[$rowid++];
    $columns["C_4_5_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_4_5_5_1"]   = $row[$rowid++];
    $columns["C_4_5_5_2"]   = $row[$rowid++];
    $columns["C_4_5_5_3"]   = $row[$rowid++];
    $columns["C_4_5_6"]     = $row[$rowid++];
    $columns["C_4_6_1"]     = $row[$rowid++];
    $columns["C_4_6_2"]     = $row[$rowid++];
    $columns["C_4_6_3"]     = $row[$rowid++];
    $columns["C_4_6_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_4_6_5_1"]   = $row[$rowid++];
    $columns["C_4_6_5_2"]   = $row[$rowid++];
    $columns["C_4_6_5_3"]   = $row[$rowid++];
    $columns["C_4_6_6"]     = $row[$rowid++];
    $columns["C_4_7_1"]     = $row[$rowid++];
    $columns["C_4_7_2"]     = $row[$rowid++];
    $columns["C_4_7_3"]     = $row[$rowid++];
    $columns["C_4_7_4"]     = $row[$rowid++];
    $columns["C_4_7_5"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_4_7_6_1"]   = $row[$rowid++];
    $columns["C_4_7_6_2"]   = $row[$rowid++];
    $columns["C_4_7_6_3"]   = $row[$rowid++];
    $columns["C_4_7_7"]     = $row[$rowid++];
    $columns["C_4_8"]       = $row[$rowid++];
    $columns["C_4_9"]       = $row[$rowid++];
    
    // 日本語学習の機会の提供
    $columns["C_5_1_1"]     = $row[$rowid++];
    $columns["C_5_1_2"]     = $row[$rowid++];
    $columns["C_5_1_3"]     = $row[$rowid++];
    $columns["C_5_1_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_5_2_1"]     = $row[$rowid++];
    $columns["C_5_2_2"]     = $row[$rowid++];
    $columns["C_5_2_3"]     = $row[$rowid++];
    $columns["C_5_2_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_5_3_1"]     = $row[$rowid++];
    $columns["C_5_3_2"]     = $row[$rowid++];
    $columns["C_5_3_3"]     = $row[$rowid++];
    $columns["C_5_3_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_5_4_1"]     = $row[$rowid++];
    $columns["C_5_4_2"]     = $row[$rowid++];
    $columns["C_5_4_3"]     = $row[$rowid++];
    $columns["C_5_4_4"]     = $row[$rowid++];
    $columns["C_5_4_5"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_5_4_6"]     = $row[$rowid++];
    
    // 相談又は苦情への対応
    $columns["C_6_1_1"]     = $row[$rowid++];
    $columns["C_6_1_2"]     = $row[$rowid++];
    $columns["C_6_1_3"]     = $row[$rowid++];
    $columns["C_6_1_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_6_2_1"]     = $row[$rowid++];
    $columns["C_6_2_2"]     = $row[$rowid++];
    $columns["C_6_2_3"]     = $row[$rowid++];
    $columns["C_6_2_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_6_3_1"]     = $row[$rowid++];
    $columns["C_6_3_2"]     = $row[$rowid++];
    $columns["C_6_3_3"]     = $row[$rowid++];
    $columns["C_6_3_4"]     = $row[$rowid++];
    $columns["C_6_3_5"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_6_4_01"]    = $row[$rowid++];
    $columns["C_6_4_02"]    = $row[$rowid++];
    $columns["C_6_4_03"]    = $row[$rowid++];
    $columns["C_6_4_04"]    = $row[$rowid++];
    $columns["C_6_4_05"]    = $row[$rowid++];
    $columns["C_6_4_06"]    = $row[$rowid++];
    $columns["C_6_4_07"]    = $row[$rowid++];
    $columns["C_6_4_08"]    = $row[$rowid++];
    $columns["C_6_4_09"]    = $row[$rowid++];
    $columns["C_6_4_10"]    = $row[$rowid++];
    $columns["C_6_5_1"]     = $row[$rowid++];
    $columns["C_6_5_2"]     = $row[$rowid++];
    $columns["C_6_6_1"]     = $row[$rowid++];
    $columns["C_6_6_2"]     = $row[$rowid++];
    $columns["C_6_7_1"]     = $row[$rowid++];
    $columns["C_6_7_2"]     = $row[$rowid++];
    $columns["C_6_8_1_1"]   = $row[$rowid++];
    $columns["C_6_8_1_2"]   = $row[$rowid++];
    $columns["C_6_8_1_3"]   = $row[$rowid++];
    $columns["C_6_8_1_4"]   = $row[$rowid++];
    $columns["C_6_8_2"]     = $row[$rowid++];
    $columns["C_6_8_3"]     = $row[$rowid++];
    $columns["C_6_8_4"]     = $row[$rowid++];
    $columns["C_6_9_1_1"]   = $row[$rowid++];
    $columns["C_6_9_1_2"]   = $row[$rowid++];
    $columns["C_6_9_1_3"]   = $row[$rowid++];
    $columns["C_6_9_1_4"]   = $row[$rowid++];
    $columns["C_6_9_2"]     = $row[$rowid++];
    $columns["C_6_9_3"]     = $row[$rowid++];
    $columns["C_6_9_4"]     = $row[$rowid++];
    $columns["C_6_9_5"]     = $row[$rowid++];
    
    // 日本人との交流促進に係る支援
    $columns["C_7_1_1"]     = $row[$rowid++];
    $columns["C_7_1_2"]     = $row[$rowid++];
    $columns["C_7_1_3"]     = $row[$rowid++];
    $columns["C_7_1_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_7_2_1"]     = $row[$rowid++];
    $columns["C_7_2_2"]     = $row[$rowid++];
    $columns["C_7_2_3"]     = $row[$rowid++];
    $columns["C_7_2_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_7_3_1"]     = $row[$rowid++];
    $columns["C_7_3_2"]     = $row[$rowid++];
    $columns["C_7_3_3"]     = $row[$rowid++];
    $columns["C_7_3_4"]     = $row[$rowid++];
    $columns["C_7_3_5"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_7_3_6"]     = $row[$rowid++];
    
    // 非自発的離職時の転職支援
    $columns["C_8_1_1"]     = $row[$rowid++];
    $columns["C_8_1_2"]     = $row[$rowid++];
    $columns["C_8_1_3"]     = $row[$rowid++];
    $columns["C_8_1_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_8_2_1"]     = $row[$rowid++];
    $columns["C_8_2_2"]     = $row[$rowid++];
    $columns["C_8_2_3"]     = $row[$rowid++];
    $columns["C_8_2_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_8_3_1"]     = $row[$rowid++];
    $columns["C_8_3_2"]     = $row[$rowid++];
    $columns["C_8_3_3"]     = $row[$rowid++];
    $columns["C_8_3_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_8_4_1"]     = $row[$rowid++];
    $columns["C_8_4_2"]     = $row[$rowid++];
    $columns["C_8_4_3"]     = $row[$rowid++];
    $columns["C_8_4_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_8_5_1"]     = $row[$rowid++];
    $columns["C_8_5_2"]     = $row[$rowid++];
    $columns["C_8_6_1"]     = $row[$rowid++];
    $columns["C_8_6_2"]     = $row[$rowid++];
    $columns["C_8_6_3"]     = $row[$rowid++];
    $columns["C_8_6_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_8_6_5_1"]   = $row[$rowid++];
    $columns["C_8_6_5_2"]   = $row[$rowid++];
    $columns["C_8_6_5_3"]   = $row[$rowid++];
    $columns["C_8_6_6"]     = $row[$rowid++];
    $columns["C_8_7_1"]     = $row[$rowid++];
    $columns["C_8_7_2"]     = $row[$rowid++];
    $columns["C_8_7_3"]     = $row[$rowid++];
    $columns["C_8_7_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_8_8_1"]     = $row[$rowid++];
    $columns["C_8_8_2"]     = $row[$rowid++];
    $columns["C_8_8_3"]     = $row[$rowid++];
    $columns["C_8_8_4"]     = $row[$rowid++];
    $columns["C_8_8_5"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_8_8_6"]     = $row[$rowid++];
    
    // 定期的な面談の実施・行政機関への通報
    $columns["C_9_1_1"]     = $row[$rowid++];
    $columns["C_9_1_2"]     = $row[$rowid++];
    $columns["C_9_1_3"]     = $row[$rowid++];
    $columns["C_9_1_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_9_1_5_1"]   = $row[$rowid++];
    $columns["C_9_1_5_2"]   = $row[$rowid++];
    $columns["C_9_1_5_3"]   = $row[$rowid++];
    $columns["C_9_1_6"]     = $row[$rowid++];
    $columns["C_9_2_1"]     = $row[$rowid++];
    $columns["C_9_2_2"]     = $row[$rowid++];
    $columns["C_9_2_3"]     = $row[$rowid++];
    $columns["C_9_2_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_9_2_5_1"]   = $row[$rowid++];
    $columns["C_9_2_5_2"]   = $row[$rowid++];
    $columns["C_9_2_5_3"]   = $row[$rowid++];
    $columns["C_9_2_6"]     = $row[$rowid++];
    $columns["C_9_3_1"]     = $row[$rowid++];
    $columns["C_9_3_2"]     = $row[$rowid++];
    $columns["C_9_3_3"]     = $row[$rowid++];
    $columns["C_9_3_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_9_4_1"]     = $row[$rowid++];
    $columns["C_9_4_2"]     = $row[$rowid++];
    $columns["C_9_4_3"]     = $row[$rowid++];
    $columns["C_9_4_4"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_9_5_1"]     = $row[$rowid++];
    $columns["C_9_5_2"]     = $row[$rowid++];
    $columns["C_9_5_3"]     = $row[$rowid++];
    $columns["C_9_5_4"]     = $row[$rowid++];
    $columns["C_9_5_5"]     = $db->getCompanyRowId($row[$rowid++]);
    $columns["C_9_5_6"]     = $row[$rowid++];
    $columns["C_9_6"]       = $row[$rowid++];
    $columns["C_9_7"]       = $row[$rowid++];
}
/**
 * 労働者の行IDを取得する。
 *
 * @param PDO $db DB接続
 * @param string $workerId 取得対象の労働者ID
 * 
 * @return integer 行ID
 */
function getWorkerRowId(&$db, $workerId) {
    $sql = "SELECT id AS rowid "
         . "FROM mst_workers "
         . "WHERE worker_id = :worker_id ";
    $params[":worker_id"] = $workerId;
    $result = $db->selectOne($sql, $params);
    return $result["rowid"];
}
/**
 * 労働者のIDを取得する。
 *
 * @param string $workerId 取得対象の労働者ID
 * 
 * @return integer 行ID
 */
function getWorkerId($workerId) {
    $db = new JinzaiDb(DB_DEFINE);
    $sql = "SELECT id AS rowid "
         . "FROM mst_workers "
         . "WHERE worker_id = :worker_id ";
    $params[":worker_id"] = $workerId;
    $result = $db->selectOne($sql, $params);
    return $result["rowid"];
}