<?php

/**
 * 労働者の基本情報を取得し、結果を返す。
 *
 * @param $string  $workerId 労働者ID
 * @param $array[] $labels   ラベル情報
 * 
 * @return array[] 取得結果
 */
function search($workerId, $labels) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         .     "workers.user_name, "
         .     "workers.user_name_e, "
         .     "workers.nationality_region, "
         .     "DATE_FORMAT(workers.birthday, '%Y/%m/%d') AS birthday_ymd, "
         .     "workers.family_name, "
         .     "workers.given_name, "
         .     "workers.sex, "
         .     "workers.place_birth, "
         .     "workers.marital_flag, "
         .     "workers.occupation, "
         .     "workers.home_town, "
         .     "workers.postal_code, "
         .     "workers.address, "
         .     "workers.telephone, "
         .     "workers.cellular_phone, "
         .     "workers.A_01_1, "
         .     "DATE_FORMAT(workers.A_01_2, '%Y/%m/%d') AS A_01_2_ymd, "
         .     "workers.A_02_1, "
         .     "workers.A_02_2, "
         .     "DATE_FORMAT(workers.A_03, '%Y/%m/%d') AS A_03_ymd, "
         .     "workers.A_04, "
         .     "workers.A_05, "
         .     "workers.A_06, "
         .     "workers.A_07, "
         .     "workers.A_08_1, "
         .     "workers.A_08_2, "
         .     "DATE_FORMAT(workers.A_08_3, '%Y/%m/%d') AS A_08_3_ymd, "
         .     "DATE_FORMAT(workers.A_08_4, '%Y/%m/%d') AS A_08_4_ymd, "
         .     "workers.A_15_1, "
         .     "workers.A_15_2, "
         .     "workers.A_16_1, "
         .     "workers.A_17_1, "
         .     "DATE_FORMAT(workers.A_17_2, '%Y/%m/%d') AS A_17_2_ymd, "
         .     "workers.A_18_1, "
         .     "workers.A_19_1_1, "
         .     "workers.A_19_1_2, "
         .     "DATE_FORMAT(workers.A_19_1_3, '%Y/%m/%d') AS A_19_1_3_ymd, "
         .     "workers.A_19_1_4, "
         .     "workers.A_19_1_5, "
         .     "workers.A_19_1_6, "
         .     "workers.A_19_1_7, "
         .     "workers.A_19_2_1, "
         .     "workers.A_19_2_2, "
         .     "DATE_FORMAT(workers.A_19_2_3, '%Y/%m/%d') AS A_19_2_3_ymd, "
         .     "workers.A_19_2_4, "
         .     "workers.A_19_2_5, "
         .     "workers.A_19_2_6, "
         .     "workers.A_19_2_7, "
         .     "workers.A_19_3_1, "
         .     "workers.A_19_3_2, "
         .     "DATE_FORMAT(workers.A_19_3_3, '%Y/%m/%d') AS A_19_3_3_ymd, "
         .     "workers.A_19_3_4, "
         .     "workers.A_19_3_5, "
         .     "workers.A_19_3_6, "
         .     "workers.A_19_3_7, "
         .     "workers.A_19_4_1, "
         .     "workers.A_19_4_2, "
         .     "DATE_FORMAT(workers.A_19_4_3, '%Y/%m/%d') AS A_19_4_3_ymd, "
         .     "workers.A_19_4_4, "
         .     "workers.A_19_4_5, "
         .     "workers.A_19_4_6, "
         .     "workers.A_19_4_7, "
         .     "workers.A_20_1, "
         .     "workers.A_20_2, "
         .     "workers.A_20_3, "
         .     "workers.A_21_1, "
         .     "workers.A_21_3, "
         .     "workers.A_21_4, "
         .     "workers.A_21_5, "
         .     "workers.A_21_7, "
         .     "workers.A_22_1, "
         .     "workers.A_22_3, "
         .     "workers.A_22_4, "
         .     "workers.A_22_5, "
         .     "workers.A_22_7, "
         .     "workers.A_23_1_1, "
         .     "workers.A_23_1_2, "
         .     "workers.A_23_1_3, "
         .     "workers.A_23_2_1, "
         .     "workers.A_23_2_2, "
         .     "workers.A_23_2_3, "
         .     "workers.A_24_1, "
         .     "workers.A_24_2, "
         .     "workers.A_25_1, "
         .     "workers.A_25_2, "
         .     "workers.A_25_3, "
         .     "workers.A_26_1, "
         .     "workers.A_26_2, "
         .     "workers.A_26_3, "
         .     "workers.A_27, "
         .     "workers.A_28, "
         .     "workers.A_29, "
         .     "workers.A_30, "
         .     "DATE_FORMAT(workers.A_31_1_1, '%Y/%m') AS A_31_1_1_ymd, "
         .     "DATE_FORMAT(workers.A_31_1_2, '%Y/%m') AS A_31_1_2_ymd, "
         .     "workers.A_31_1_3, "
         .     "DATE_FORMAT(workers.A_31_2_1, '%Y/%m') AS A_31_2_1_ymd, "
         .     "DATE_FORMAT(workers.A_31_2_2, '%Y/%m') AS A_31_2_2_ymd, "
         .     "workers.A_31_2_3, "
         .     "DATE_FORMAT(workers.A_31_3_1, '%Y/%m') AS A_31_3_1_ymd, "
         .     "DATE_FORMAT(workers.A_31_3_2, '%Y/%m') AS A_31_3_2_ymd, "
         .     "workers.A_31_3_3, "
         .     "DATE_FORMAT(workers.A_31_4_1, '%Y/%m') AS A_31_4_1_ymd, "
         .     "DATE_FORMAT(workers.A_31_4_2, '%Y/%m') AS A_31_4_2_ymd, "
         .     "workers.A_31_4_3, "
         .     "DATE_FORMAT(workers.A_31_5_1, '%Y/%m') AS A_31_5_1_ymd, "
         .     "DATE_FORMAT(workers.A_31_5_2, '%Y/%m') AS A_31_5_2_ymd, "
         .     "workers.A_31_5_3, "
         .     "DATE_FORMAT(workers.A_31_6_1, '%Y/%m') AS A_31_6_1_ymd, "
         .     "DATE_FORMAT(workers.A_31_6_2, '%Y/%m') AS A_31_6_2_ymd, "
         .     "workers.A_31_6_3, "
         .     "workers.A_32, "
         .     "workers.A_33, "
         .     "workers.A_34, "
         .     "workers.A_35, "
         .     "workers.A_36, "
         .     "workers.A_37, "
         .     "workers.A_38, "
         .     "workers.A_39, "
         .     "workers.A_40, "
         .     "workers.A_41, "

               //変更許可申請書・在留期間更新許可申請書　固有項目
         .     "workers.qualifications     AS A_09_1_1, "
         .     "workers.qualifications_etc AS A_09_1_2, "
         .     "workers.A_09_2, "
         .     "DATE_FORMAT(workers.A_09_3, '%Y/%m/%d') AS A_09_3_ymd, "
         .     "workers.A_10, "
         .     "workers.A_12, "
         .     "workers.A_13, "
         .     "workers.A_11_1, "
         .     "workers.A_11_2, "
         .     "workers.A_14, "
         .     "workers.A_19_5_1, "
         .     "workers.A_19_5_2, "
         .     "DATE_FORMAT(workers.A_19_5_3, '%Y/%m/%d') AS A_19_5_3_ymd, "
         .     "workers.A_19_5_4, "
         .     "workers.A_19_5_5, "
         .     "workers.A_19_5_6, "
         .     "workers.A_19_5_7, "
         .     "workers.A_19_6_1, "
         .     "workers.A_19_6_2, "
         .     "DATE_FORMAT(workers.A_19_6_3, '%Y/%m/%d') AS A_19_6_3_ymd, "
         .     "workers.A_19_6_4, "
         .     "workers.A_19_6_5, "
         .     "workers.A_19_6_6, "
         .     "workers.A_19_6_7 "
         . "FROM mst_workers workers "
         . "WHERE workers.worker_id = :worker_id "
         ;

    $params = array();
    $params[":worker_id"] = $workerId;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);
    
    // 表示用データを追加設定。
    $sex           = getOptionItems("comm", "sex");
    $yesno         = getOptionItems("comm", "yesno");
    $purpose       = getOptionItems("comm", "purpose");
    $completion    = getOptionItems("comm", "completion");
    $completion2   = getOptionItems("comm", "completion2");
    $qualification = getOptionItems("comm", "qualification");

    // コードに対応する文言を設定。
    $result["sex"]          = getOprionItemValue($sex,          $result["sex"]);
    $result["marital_flag"] = getOprionItemValue($yesno,        $result["marital_flag"]);
    $result["A_02_1"]       = getOprionItemValue($purpose,      $result["A_02_1"]);
    $result["A_06"]         = getOprionItemValue($yesno,        $result["A_06"]);
    $result["A_08_1"]       = getOprionItemValue($yesno,        $result["A_08_1"]);
    $result["A_15_1"]       = getOprionItemValue($yesno,        $result["A_15_1"]);
    $result["A_16_1"]       = getOprionItemValue($yesno,        $result["A_16_1"]);
    $result["A_18_1"]       = getOprionItemValue($yesno,        $result["A_18_1"]);
    $result["A_19_1_5"]     = getOprionItemValue($yesno,        $result["A_19_1_5"]);
    $result["A_19_2_5"]     = getOprionItemValue($yesno,        $result["A_19_2_5"]);
    $result["A_19_3_5"]     = getOprionItemValue($yesno,        $result["A_19_3_5"]);
    $result["A_19_4_5"]     = getOprionItemValue($yesno,        $result["A_19_4_5"]);
    $result["A_19_5_5"]     = getOprionItemValue($yesno,        $result["A_19_5_5"]);
    $result["A_19_6_5"]     = getOprionItemValue($yesno,        $result["A_19_6_5"]);
    $result["A_21_1"]       = getOprionItemValue($completion,   $result["A_21_1"]);
    $result["A_22_1"]       = getOprionItemValue($completion,   $result["A_22_1"]);
    $result["A_23_1_3"]     = getOprionItemValue($completion2,  $result["A_23_1_3"]);
    $result["A_23_2_3"]     = getOprionItemValue($completion2,  $result["A_23_2_3"]);
    $result["A_25_1"]       = getOprionItemValue($yesno,        $result["A_25_1"]);
    $result["A_26_1"]       = getOprionItemValue($yesno,        $result["A_26_1"]);
    $result["A_27"]         = getOprionItemValue($yesno,        $result["A_27"]);
    $result["A_28"]         = getOprionItemValue($yesno,        $result["A_28"]);
    $result["A_29"]         = getOprionItemValue($yesno,        $result["A_29"]);
    $result["A_30"]         = getOprionItemValue($yesno,        $result["A_30"]);

    // 申請時における特定技能1号での通算在留期間の「年」「月」のラベルをセット
    if ($result["A_24_1"].$result["A_24_2"] != "") {
        $result["A_24_ym"] = $result["A_24_1"].$labels["year"].$result["A_24_2"].$labels["month"];  //TODO:年/月をラベル化する。
    } else {
        $result["A_24_ym"] = "";
    }
    // 現に有する在留資格のセット
    if ($result["A_09_1_1"] == "6") {
        // 在留資格で「その他」の場合、在留資格のその他情報をセット
        $result["A_09_1"]  = getOprionItemValue($purpose,        $result["A_09_1_2"]);

    } else {
        $result["A_09_1"] = getOprionItemValue($qualification,    $result["A_09_1_1"]);
    }

    // 希望する在留資格のセット
    if ($result["A_11_1"] == "6") {
        // 希望する在留資格で「その他」の場合、希望する在留資格のその他情報をセット
        $result["A_11"]  = getOprionItemValue($purpose,        $result["A_11_2"]);
    } else {
        $result["A_11"] = getOprionItemValue($qualification,    $result["A_11_1"]);
    }

    // 取得結果を返す。
    return $result;
}
