<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $workerId 労働者ID
 * 
 * @return PageData 検索結果
 */
function search($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "files.id AS file_rowid, "
         .     "files.file_name_org AS file_name, "
         .     "DATE_FORMAT(files.create_date, '%Y/%m/%d') AS upload_ymd, "
         .     "files.classification AS file_type "
         . "FROM tbl_upload_file files "
         . "INNER JOIN mst_workers workers "
         .     "ON files.worker_id = workers.id "
         . "WHERE workers.worker_id = :worker_id "
         . "ORDER BY upload_ymd DESC, file_rowid DESC "
    ;
    $params = array(
        ":worker_id" => $workerId
    );

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 表示用データを追加設定。
    $fileType = getOptionItems("comm", "file_type");

    // コードに対応する文言を設定。
    foreach ($result as &$row) {
        $row["file_type_name"] = getOprionItemValue($fileType, $row["file_type"]);
    }

    // 取得結果を返す。
    return $result;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $items     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($items, &$whereSqls, &$params) {
    // ・企業ID
    if ($items["C_company"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "file_data.company_id = :company_id ";
        $params[":company_id"] = $items["C_company"];
    }

    // ・労働者ID
    if ($items["C_worker"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "file_data.worker_id = :worker_id ";
        $params[":worker_id"] = $items["C_worker"];
    }

    // ・ファイル名
    if ($items["C_01"] != "") {
        // ファイル名に対して部分一致条件を設定。
        $whereSqls[] = "file_data.file_name_org LIKE :file_name_org ";
        $params[":file_name_org"] = "%".$items["C_01"]."%";
    }

    // ・登録者名
    if ($items["C_02"] != "") {
        // 登録者名に対して部分一致条件を設定。
        $whereSqls[] = "users.user_name LIKE :user_name ";
        $params[":user_name"] = "%".$items["C_02"]."%";
    }

    // ・種別
    if ($items["C_03_1"].$items["C_03_2"].$items["C_03_3"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c3Params = array();
        if ($items["C_03_1"] != "") $c3Params[] = "'1'";
        if ($items["C_03_2"] != "") $c3Params[] = "'2'";
        if ($items["C_03_3"] != "") $c3Params[] = "'3'";
        $c3ParamsString = joinArray($c3Params);
        $whereSqls[] = "file_data.classification IN (".$c3ParamsString.") ";
    }

    // ・登録日
    if ($items["C_04_1"] != "") {
        $whereSqls[] = "file_data.create_date >= STR_TO_DATE(:upload_date_from, '%Y-%m-%d') ";
        $params[":upload_date_from"] = $items["C_04_1"];
    }
    if ($items["C_04_2"] != "") {
        $whereSqls[] = "file_data.create_date < DATE_ADD(STR_TO_DATE(:upload_date_to, '%Y-%m-%d'), INTERVAL 1 DAY) ";
        $params[":upload_date_to"] = $items["C_04_2"];
    }
}
