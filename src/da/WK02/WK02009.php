<?php

/**
 * 労働者の定期面談報告書情報を取得し、結果を返す。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function search($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         // 1.対応者
         .     "E_2_1, "
         .     "E_2_2, "
         .     "E_2_3, "
         // 2.面談事項
         .     "E_3_1_1_1, E_3_1_1_2, "
         .     "E_3_1_2_1, E_3_1_2_2, "
         .     "E_3_1_3_1, E_3_1_3_2, "
         .     "E_3_2_1_1, E_3_2_1_2, "
         .     "E_3_2_2_1, E_3_2_2_2, "
         .     "E_3_2_3_1, E_3_2_3_2, "
         .     "E_3_2_4_1, E_3_2_4_2, "
         .     "E_3_2_5_1, E_3_2_5_2, "
         .     "E_3_2_6_1, E_3_2_6_2, "
         .     "E_3_3_1_1, E_3_3_1_2, "
         .     "E_3_3_2_1, E_3_3_2_2, "
         .     "E_3_3_3_1, E_3_3_3_2, "
         .     "E_3_3_4_1, E_3_3_4_2, "
         .     "E_3_3_5_1, E_3_3_5_2, "
         .     "E_3_4_1_1, E_3_4_1_2, "
         .     "E_3_4_2_1, E_3_4_2_2, "
         .     "E_3_5_1_1, E_3_5_1_2, "
         .     "E_3_5_2_1, E_3_5_2_2, E_3_5_2_3, "
         .     "E_3_6, "
         .     "E_3_7, "
         // 3.法令違反への対応
         .     "DATE_FORMAT(rep.E_4_1, '%Y/%m/%d') AS E_4_1_ymd, "
         .     "E_4_2, "
         .     "E_4_3_1_1, E_4_3_1_2, E_4_3_1_3, "
         .     "E_4_3_2_1, DATE_FORMAT(rep.E_4_3_2_2, '%Y/%m/%d') AS E_4_3_2_2_ymd, E_4_3_2_3, E_4_3_2_4, E_4_3_2_5, "
         .     "E_4_3_3_1, DATE_FORMAT(rep.E_4_3_3_2, '%Y/%m/%d') AS E_4_3_3_2_ymd, E_4_3_3_3, E_4_3_3_4 "
         
         . "FROM tbl_report_007 rep "
         . "INNER JOIN mst_workers workers "
         . "ON rep.worker_id = workers.id "
         . "WHERE workers.worker_id = :worker_id "
         ;

    $params = array();
    $params[":worker_id"] = $workerId;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    if (count($result) == 0) {
        // レコードが取得できなかった場合、データ不整備エラーとする。
        $errMessage = getCommonMessage("EC004");
        goErrorPage($errMessage);
    }
   
    // 表示用データを追加設定。
    $yesno     = getOptionItems("comm", "yesno");
    $supporter = getOptionItems("WK02", "09_supporter");
    $response1 = getOptionItems("WK02", "09_response_1");
    $response2 = getOptionItems("WK02", "09_response_2");
    $response3 = getOptionItems("WK02", "09_response_3");
    $response4 = getOptionItems("WK02", "09_response_4");

    // コードに対応する文言を設定。
    $result["E_2_2"] = getOprionItemValue($supporter, $result["E_2_2"]);

    $result["E_3_1_1_1"]   = getOprionItemValue($yesno, $result["E_3_1_1_1"]); 
    $result["E_3_1_2_1"]   = getOprionItemValue($yesno, $result["E_3_1_2_1"]); 
    $result["E_3_1_3_1"]   = getOprionItemValue($yesno, $result["E_3_1_3_1"]); 
    $result["E_3_2_1_1"]   = getOprionItemValue($yesno, $result["E_3_2_1_1"]); 
    $result["E_3_2_2_1"]   = getOprionItemValue($yesno, $result["E_3_2_2_1"]); 
    $result["E_3_2_3_1"]   = getOprionItemValue($yesno, $result["E_3_2_3_1"]); 
    $result["E_3_2_4_1"]   = getOprionItemValue($yesno, $result["E_3_2_4_1"]); 
    $result["E_3_2_5_1"]   = getOprionItemValue($yesno, $result["E_3_2_5_1"]); 
    $result["E_3_2_6_1"]   = getOprionItemValue($yesno, $result["E_3_2_6_1"]); 
    $result["E_3_3_1_1"]   = getOprionItemValue($yesno, $result["E_3_3_1_1"]); 
    $result["E_3_3_2_1"]   = getOprionItemValue($yesno, $result["E_3_3_2_1"]); 
    $result["E_3_3_3_1"]   = getOprionItemValue($yesno, $result["E_3_3_3_1"]); 
    $result["E_3_3_4_1"]   = getOprionItemValue($yesno, $result["E_3_3_4_1"]); 
    $result["E_3_3_5_1"]   = getOprionItemValue($yesno, $result["E_3_3_5_1"]); 
    $result["E_3_4_1_1"]   = getOprionItemValue($yesno, $result["E_3_4_1_1"]); 
    $result["E_3_4_2_1"]   = getOprionItemValue($yesno, $result["E_3_4_2_1"]); 
    $result["E_3_5_1_1"]   = getOprionItemValue($yesno, $result["E_3_5_1_1"]); 
    $result["E_3_5_2_1"]   = getOprionItemValue($yesno, $result["E_3_5_2_1"]); 
    $result["E_3_6"]       = getOprionItemValue($yesno, $result["E_3_6"]); 

    $result["E_4_3_1_1"] = getOprionItemValue($response1, $result["E_4_3_1_1"]);
    $result["E_4_3_2_1"] = getOprionItemValue($response2, $result["E_4_3_2_1"]);
    $result["E_4_3_2_5"] = getOprionItemValue($response3, $result["E_4_3_2_5"]);
    $result["E_4_3_3_1"] = getOprionItemValue($response4, $result["E_4_3_3_1"]);

    // 取得結果を返す。
    return $result;
}

/**
 * CSV出力用データを取得する。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function getExportDatas($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
        // 1.面談日
         .     "DATE_FORMAT(rep.E_1_1, '%Y/%m/%d') AS E_1_1_ymd, "
        // 2.対応者
         .     "E_2_1, "
         .     "E_2_2, "
         .     "E_2_3, "
         // 3.面談事項
         .     "E_3_1_1_1, E_3_1_1_2, "
         .     "E_3_1_2_1, E_3_1_2_2, "
         .     "E_3_1_3_1, E_3_1_3_2, "
         .     "E_3_2_1_1, E_3_2_1_2, "
         .     "E_3_2_2_1, E_3_2_2_2, "
         .     "E_3_2_3_1, E_3_2_3_2, "
         .     "E_3_2_4_1, E_3_2_4_2, "
         .     "E_3_2_5_1, E_3_2_5_2, "
         .     "E_3_2_6_1, E_3_2_6_2, "
         .     "E_3_3_1_1, E_3_3_1_2, "
         .     "E_3_3_2_1, E_3_3_2_2, "
         .     "E_3_3_3_1, E_3_3_3_2, "
         .     "E_3_3_4_1, E_3_3_4_2, "
         .     "E_3_3_5_1, E_3_3_5_2, "
         .     "E_3_4_1_1, E_3_4_1_2, "
         .     "E_3_4_2_1, E_3_4_2_2, "
         .     "E_3_5_1_1, E_3_5_1_2, "
         .     "E_3_5_2_1, E_3_5_2_2, E_3_5_2_3, "
         .     "E_3_6, "
         .     "E_3_7, "
         // 4.法令違反への対応
         .     "DATE_FORMAT(rep.E_4_1, '%Y/%m/%d') AS E_4_1_ymd, "
         .     "E_4_2, "
         .     "E_4_3_1_1, E_4_3_1_2, E_4_3_1_3, "
         .     "E_4_3_2_1, DATE_FORMAT(rep.E_4_3_2_2, '%Y/%m/%d') AS E_4_3_2_2_ymd, E_4_3_2_3, E_4_3_2_4, E_4_3_2_5, "
         .     "E_4_3_3_1, DATE_FORMAT(rep.E_4_3_3_2, '%Y/%m/%d') AS E_4_3_3_2_ymd, E_4_3_3_3, E_4_3_3_4 "

    . "FROM tbl_report_007 rep "
    . "INNER JOIN mst_workers workers "
    . "ON rep.worker_id = workers.id "
    . "WHERE workers.worker_id = :worker_id "
    ;

    $params = array();
    $params[":worker_id"] = $workerId;

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}


/**
 * CSVデータを登録する。
 *
 * @param array[カラム] $row CSVデータ
 * @param string $workerId 労働者ID
 * 
 * @return boolean 実行成否
 */
function csvDataImport($row, $workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        // 労働者の行IDを取得。
        $workerRowId = getWorkerRowId($db, $workerId);

        // カラムと設定値を定義。
        $columns = array();
        setColumns($columns, $db, $row);

        $whereString = "worker_id = :worker_id ";
        $whereParams = array(
            "worker_id" => $workerRowId
        );

        // update処理を発行する。
        $db->update("tbl_report_007", $columns, $whereString, $whereParams);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}

/**
 * CSVレコードの内容から、更新用データを生成する。
 *
 * @param array[DBカラム名 => 値] $columns 更新用カラム一覧
 * @param array[値] $row CSV行データ
 */
function setColumns(&$columns, $db, $row) {
    $rowid = 1 - 1;

    $columns["E_1_1"] = setDateParam($row[$rowid++]);
    $columns["E_2_1"] = $row[$rowid++];
    $columns["E_2_2"] = $row[$rowid++];
    $columns["E_2_3"] = $row[$rowid++];
    $columns["E_3_1_1_1"] = $row[$rowid++];
    $columns["E_3_1_1_2"] = $row[$rowid++];
    $columns["E_3_1_2_1"] = $row[$rowid++];
    $columns["E_3_1_2_2"] = $row[$rowid++];
    $columns["E_3_1_3_1"] = $row[$rowid++];
    $columns["E_3_1_3_2"] = $row[$rowid++];
    $columns["E_3_2_1_1"] = $row[$rowid++];
    $columns["E_3_2_1_2"] = $row[$rowid++];
    $columns["E_3_2_2_1"] = $row[$rowid++];
    $columns["E_3_2_2_2"] = $row[$rowid++];
    $columns["E_3_2_3_1"] = $row[$rowid++];
    $columns["E_3_2_3_2"] = $row[$rowid++];
    $columns["E_3_2_4_1"] = $row[$rowid++];
    $columns["E_3_2_4_2"] = $row[$rowid++];
    $columns["E_3_2_5_1"] = $row[$rowid++];
    $columns["E_3_2_5_2"] = $row[$rowid++];
    $columns["E_3_2_6_1"] = $row[$rowid++];
    $columns["E_3_2_6_2"] = $row[$rowid++];
    $columns["E_3_3_1_1"] = $row[$rowid++];
    $columns["E_3_3_1_2"] = $row[$rowid++];
    $columns["E_3_3_2_1"] = $row[$rowid++];
    $columns["E_3_3_2_2"] = $row[$rowid++];
    $columns["E_3_3_3_1"] = $row[$rowid++];
    $columns["E_3_3_3_2"] = $row[$rowid++];
    $columns["E_3_3_4_1"] = $row[$rowid++];
    $columns["E_3_3_4_2"] = $row[$rowid++];
    $columns["E_3_3_5_1"] = $row[$rowid++];
    $columns["E_3_3_5_2"] = $row[$rowid++];
    $columns["E_3_4_1_1"] = $row[$rowid++];
    $columns["E_3_4_1_2"] = $row[$rowid++];
    $columns["E_3_4_2_1"] = $row[$rowid++];
    $columns["E_3_4_2_2"] = $row[$rowid++];
    $columns["E_3_5_1_1"] = $row[$rowid++];
    $columns["E_3_5_1_2"] = $row[$rowid++];
    $columns["E_3_5_2_1"] = $row[$rowid++];
    $columns["E_3_5_2_2"] = $row[$rowid++];
    $columns["E_3_5_2_3"] = $row[$rowid++];
    $columns["E_3_6"]     = $row[$rowid++];
    $columns["E_3_7"]     = $row[$rowid++];
    $columns["E_4_1"]     = setDateParam($row[$rowid++]);
    $columns["E_4_2"]     = $row[$rowid++];
    $columns["E_4_3_1_1"] = $row[$rowid++];
    $columns["E_4_3_1_2"] = $row[$rowid++];
    $columns["E_4_3_1_3"] = $row[$rowid++];
    $columns["E_4_3_2_1"] = $row[$rowid++];
    $columns["E_4_3_2_2"] = setDateParam($row[$rowid++]);
    $columns["E_4_3_2_3"] = $row[$rowid++];
    $columns["E_4_3_2_4"] = $row[$rowid++];
    $columns["E_4_3_2_5"] = $row[$rowid++];
    $columns["E_4_3_3_1"] = $row[$rowid++];
    $columns["E_4_3_3_2"] = setDateParam($row[$rowid++]);
    $columns["E_4_3_3_3"] = $row[$rowid++];
    $columns["E_4_3_3_4"] = $row[$rowid++];

}

/**
 * 労働者の行IDを取得する。
 *
 * @param PDO $db DB接続
 * @param string $workerId 取得対象の労働者ID
 * 
 * @return integer 行ID
 */
function getWorkerRowId(&$db, $workerId) {
    $sql = "SELECT id AS rowid "
         . "FROM mst_workers "
         . "WHERE worker_id = :worker_id ";
    $params[":worker_id"] = $workerId;
    $result = $db->selectOne($sql, $params);
    return $result["rowid"];
}
