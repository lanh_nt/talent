<?php

/**
 * 「履歴書」の帳票情報を取得し、結果を返す。
 *
 * @param $targetWorkerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function makePdfData10($targetWorkerId){

    //-----------------------------------------
    // DBから履歴書の情報を取得する。
    //-----------------------------------------
    $row = searchPDF10($targetWorkerId);

    //-----------------------------------------
    // DBの情報から帳票情報を作成する。
    //-----------------------------------------
    $datas = Array();

    $datas['01_01'] = $row['user_name_e'];          //氏名：英字
    $datas['01_02'] = ""; // $row['user_name'];     //氏名：漢字 : 常に空欄とする。
    if( strcmp(  strval($row['sex']) , "1") == 0 ){
        $datas['02_01'] = $row['sex'];              //性別：男
    }
    if( strcmp(  strval($row['sex']) , "2") == 0 ){
        $datas['02_02'] = $row['sex'];              //性別：女
    }
    $datas['03_01'] = $row['w_bd_y'];               //年
    $datas['03_02'] = $row['w_bd_m'];               //月
    $datas['03_03'] = $row['w_bd_d'];               //日
    
    $datas['04_01'] = $row['nationality_region'];   //国籍・地域
    $datas['05_01'] = $row['G_1'];                  //十分に理解できる言語
    $datas['06_01'] = $row['address'];              //住所
    //電話番号の「ー」分割　★★★
    $telAry = preg_split("/[-]/",$row['telephone']);
    if(count($telAry)==1){
        $datas['06_04'] = $telAry[0];               //電話番号：市外局番
    }elseif(count($telAry)==2){
        $datas['06_03'] = $telAry[0];               //電話番号：市外局番
        $datas['06_04'] = $telAry[1];               //電話番号：市外局番
    }elseif(count($telAry)>=3){
        $datas['06_02'] = $telAry[0];               //電話番号：市外局番
        $datas['06_03'] = $telAry[1];               //電話番号：市外局番
        $datas['06_04'] = $telAry[2];               //電話番号：市外局番
    }
    $datas['07_01_01'] = $row['G_2_1_1_Y'];         //7-1：年
    $datas['07_01_02'] = $row['G_2_1_1_M'];         //7-1：月
    $datas['07_01_03'] = $row['G_2_1_2'];           //7-1：詳細
    $datas['07_02_01'] = $row['G_2_2_1_Y'];         //7-2：年
    $datas['07_02_02'] = $row['G_2_2_1_M'];         //7-2：月
    $datas['07_02_03'] = $row['G_2_2_2'];           //7-2：詳細
    $datas['07_03_01'] = $row['G_2_3_1_Y'];         //7-3：年
    $datas['07_03_02'] = $row['G_2_3_1_M'];         //7-3：月
    $datas['07_03_03'] = $row['G_2_3_2'];           //7-3：詳細
    $datas['07_04_01'] = $row['G_2_4_1_Y'];         //7-4：年
    $datas['07_04_02'] = $row['G_2_4_1_M'];         //7-4：月
    $datas['07_04_03'] = $row['G_2_4_2'];           //7-4：詳細
    $datas['07_05_01'] = $row['G_2_5_1_Y'];         //7-5：年
    $datas['07_05_02'] = $row['G_2_5_1_M'];         //7-5：月
    $datas['07_05_03'] = $row['G_2_5_2'];           //7-5：詳細
    $datas['07_06_01'] = $row['G_2_6_1_Y'];         //7-6：年
    $datas['07_06_02'] = $row['G_2_6_1_M'];         //7-6：月
    $datas['07_06_03'] = $row['G_2_6_2'];           //7-6：詳細
    $datas['08_01'] = $row['G_3'];                  //資格・免許
    $datas['09_01_01'] = $row['G_4_1_1_Y'];         //9-1：年
    $datas['09_01_02'] = $row['G_4_1_1_M'];         //9-1：月
    $datas['09_01_03'] = $row['G_4_1_2'];           //9-1：在留資格
    $datas['09_01_04'] = $row['G_4_1_3'];           //9-1：所属機関等
    $datas['09_01_05'] = $row['G_4_1_4'];           //9-1：監理団体
    $datas['09_02_01'] = $row['G_4_2_1_Y'];         //9-2：年
    $datas['09_02_02'] = $row['G_4_2_1_M'];         //9-2：月
    $datas['09_02_03'] = $row['G_4_2_2'];           //9-2：在留資格
    $datas['09_02_04'] = $row['G_4_2_3'];           //9-2：所属機関等
    $datas['09_02_05'] = $row['G_4_2_4'];           //9-2：監理団体
    $datas['09_03_01'] = $row['G_4_3_1_Y'];         //9-3：年
    $datas['09_03_02'] = $row['G_4_3_1_M'];         //9-3：月
    $datas['09_03_03'] = $row['G_4_3_2'];           //9-3：在留資格
    $datas['09_03_04'] = $row['G_4_3_3'];           //9-3：所属機関等
    $datas['09_03_05'] = $row['G_4_3_4'];           //9-3：監理団体
    $datas['09_04_01'] = $row['G_4_4_1_Y'];         //9-4：年
    $datas['09_04_02'] = $row['G_4_4_1_M'];         //9-4：月
    $datas['09_04_03'] = $row['G_4_4_2'];           //9-4：在留資格
    $datas['09_04_04'] = $row['G_4_4_3'];           //9-4：所属機関等
    $datas['09_04_05'] = $row['G_4_4_4'];           //9-4：監理団体
    $datas['09_05_01'] = $row['G_4_5_1_Y'];         //9-5：年
    $datas['09_05_02'] = $row['G_4_5_1_M'];         //9-5：月
    $datas['09_05_03'] = $row['G_4_5_2'];           //9-5：在留資格
    $datas['09_05_04'] = $row['G_4_5_3'];           //9-5：所属機関等
    $datas['09_05_05'] = $row['G_4_5_4'];           //9-5：監理団体
    $datas['09_06_01'] = $row['G_4_6_1_Y'];         //9-6：年
    $datas['09_06_02'] = $row['G_4_6_1_M'];         //9-6：月
    $datas['09_06_03'] = $row['G_4_6_2'];           //9-6：在留資格
    $datas['09_06_04'] = $row['G_4_6_3'];           //9-6：所属機関等
    $datas['09_06_05'] = $row['G_4_6_4'];           //9-6：監理団体
    $datas['09_07_01'] = $row['G_4_7_1_Y'];         //9-7：年
    $datas['09_07_02'] = $row['G_4_7_1_M'];         //9-7：月
    $datas['09_07_03'] = $row['G_4_7_2'];           //9-7：在留資格
    $datas['09_07_04'] = $row['G_4_7_3'];           //9-7：所属機関等
    $datas['09_07_05'] = $row['G_4_7_4'];           //9-7：監理団体

    return $datas;
}

/**
 * DBから「履歴書」の情報を取得し、結果を返す。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function searchPDF10($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    //取得に必要な労働者rowIDを取得する
    $WorkerRowId = $db->getWorkerRowId($workerId);

    $select_sql_str  = "SELECT mst_workers.*,tbl_report_009.* ";
    $select_sql_str  .= ",YEAR(mst_workers.birthday) as w_bd_y,MONTH(mst_workers.birthday) as w_bd_m,DAY(mst_workers.birthday) as w_bd_d ";
    $select_sql_str  .= ",date_format(G_2_1_1,'%Y')  as G_2_1_1_Y,date_format(G_2_1_1,'%c')  as G_2_1_1_M "; 
    $select_sql_str  .= ",date_format(G_2_2_1,'%Y')  as G_2_2_1_Y,date_format(G_2_2_1,'%c')  as G_2_2_1_M ";
    $select_sql_str  .= ",date_format(G_2_3_1,'%Y')  as G_2_3_1_Y,date_format(G_2_3_1,'%c')  as G_2_3_1_M ";
    $select_sql_str  .= ",date_format(G_2_4_1,'%Y')  as G_2_4_1_Y,date_format(G_2_4_1,'%c')  as G_2_4_1_M ";
    $select_sql_str  .= ",date_format(G_2_5_1,'%Y')  as G_2_5_1_Y,date_format(G_2_5_1,'%c')  as G_2_5_1_M ";
    $select_sql_str  .= ",date_format(G_2_6_1,'%Y')  as G_2_6_1_Y,date_format(G_2_6_1,'%c')  as G_2_6_1_M ";
    $select_sql_str  .= ",date_format(G_4_1_1,'%Y')  as G_4_1_1_Y,date_format(G_4_1_1,'%c')  as G_4_1_1_M ";
    $select_sql_str  .= ",date_format(G_4_2_1,'%Y')  as G_4_2_1_Y,date_format(G_4_2_1,'%c')  as G_4_2_1_M ";
    $select_sql_str  .= ",date_format(G_4_3_1,'%Y')  as G_4_3_1_Y,date_format(G_4_3_1,'%c')  as G_4_3_1_M ";
    $select_sql_str  .= ",date_format(G_4_4_1,'%Y')  as G_4_4_1_Y,date_format(G_4_4_1,'%c')  as G_4_4_1_M ";
    $select_sql_str  .= ",date_format(G_4_5_1,'%Y')  as G_4_5_1_Y,date_format(G_4_5_1,'%c')  as G_4_5_1_M ";
    $select_sql_str  .= ",date_format(G_4_6_1,'%Y')  as G_4_6_1_Y,date_format(G_4_6_1,'%c')  as G_4_6_1_M ";
    $select_sql_str  .= ",date_format(G_4_7_1,'%Y')  as G_4_7_1_Y,date_format(G_4_7_1,'%c')  as G_4_7_1_M ";
    $select_sql_str  .= "FROM mst_workers ";
    $select_sql_str  .= "left join tbl_report_009 on tbl_report_009.worker_id = mst_workers.id ";
    $select_sql_str  .= " WHERE mst_workers.id= :worker_rowid";

    $params = array();
    $params[":worker_rowid"] = $WorkerRowId;

    $result = $db->selectone($select_sql_str, $params);

    if (count($result) == 0) {
        // レコードが取得できなかった場合、データ不整備エラーとする。
        $errMessage = getCommonMessage("EC004");
        goErrorPage($errMessage);
    }

    // 取得結果を返す。
    return $result;
}
