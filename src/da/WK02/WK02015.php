<?php

/**
 * 労働者の基本情報を取得し、結果を返す。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function search($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         .     "workers.worker_id, "
         .     "workers.user_name, "
         .     "workers.user_name_e, "
         .     "workers.disp_lang, "
         .     "workers.qualifications, "
         .     "workers.qualifications_etc, "
         .     "workers.status, "
         .     "workers.nationality_region, "
         .     "DATE_FORMAT(workers.birthday, '%Y') AS birthday_year, "
         .     "DATE_FORMAT(workers.birthday, '%m') AS birthday_month, "
         .     "DATE_FORMAT(workers.birthday, '%d') AS birthday_day, "
         .     "workers.family_name, "
         .     "workers.given_name, "
         .     "workers.sex, "
         .     "workers.place_birth, "
         .     "workers.marital_flag, "
         .     "workers.occupation, "
         .     "workers.home_town, "
         .     "workers.postal_code, "
         .     "workers.address, "
         .     "workers.telephone, "
         .     "workers.cellular_phone, "
         .     "workers.A_01_1, "
         .     "DATE_FORMAT(workers.A_01_2, '%Y') AS A_01_2_year, "
         .     "DATE_FORMAT(workers.A_01_2, '%m') AS A_01_2_month, "
         .     "DATE_FORMAT(workers.A_01_2, '%d') AS A_01_2_day, "

         .     "workers.A_02_1, "
         .     "workers.A_02_2, "
         .     "DATE_FORMAT(workers.A_03, '%Y') AS A_03_year, "
         .     "DATE_FORMAT(workers.A_03, '%m') AS A_03_month, "
         .     "DATE_FORMAT(workers.A_03, '%d') AS A_03_day, "
         .     "workers.A_04, "
         .     "workers.A_05, "
         .     "workers.A_06, "
         .     "workers.A_07, "
         .     "workers.A_08_1, "
         .     "workers.A_08_2, "
         .     "DATE_FORMAT(workers.A_08_3, '%Y') AS A_08_3_year, "
         .     "DATE_FORMAT(workers.A_08_3, '%m') AS A_08_3_month, "
         .     "DATE_FORMAT(workers.A_08_3, '%d') AS A_08_3_day, "
         .     "DATE_FORMAT(workers.A_08_4, '%Y') AS A_08_4_year, "
         .     "DATE_FORMAT(workers.A_08_4, '%m') AS A_08_4_month, "
         .     "DATE_FORMAT(workers.A_08_4, '%d') AS A_08_4_day, "
         .     "workers.A_15_1, "
         .     "workers.A_15_2, "
         .     "workers.A_16_1, "
         .     "workers.A_17_1, "
         .     "DATE_FORMAT(workers.A_17_2, '%Y') AS A_17_2_year, "
         .     "DATE_FORMAT(workers.A_17_2, '%m') AS A_17_2_month, "
         .     "DATE_FORMAT(workers.A_17_2, '%d') AS A_17_2_day, "
         .     "workers.A_18_1, "
         .     "workers.A_19_1_1, "
         .     "workers.A_19_1_2, "
         .     "DATE_FORMAT(workers.A_19_1_3, '%Y') AS A_19_1_3_year, "
         .     "DATE_FORMAT(workers.A_19_1_3, '%m') AS A_19_1_3_month, "
         .     "DATE_FORMAT(workers.A_19_1_3, '%d') AS A_19_1_3_day, "
         .     "workers.A_19_1_4, "
         .     "workers.A_19_1_5, "
         .     "workers.A_19_1_6, "
         .     "workers.A_19_1_7, "
         .     "workers.A_19_2_1, "
         .     "workers.A_19_2_2, "
         .     "DATE_FORMAT(workers.A_19_2_3, '%Y') AS A_19_2_3_year, "
         .     "DATE_FORMAT(workers.A_19_2_3, '%m') AS A_19_2_3_month, "
         .     "DATE_FORMAT(workers.A_19_2_3, '%d') AS A_19_2_3_day, "
         .     "workers.A_19_2_4, "
         .     "workers.A_19_2_5, "
         .     "workers.A_19_2_6, "
         .     "workers.A_19_2_7, "
         .     "workers.A_19_3_1, "
         .     "workers.A_19_3_2, "
         .     "DATE_FORMAT(workers.A_19_3_3, '%Y') AS A_19_3_3_year, "
         .     "DATE_FORMAT(workers.A_19_3_3, '%m') AS A_19_3_3_month, "
         .     "DATE_FORMAT(workers.A_19_3_3, '%d') AS A_19_3_3_day, "
         .     "workers.A_19_3_4, "
         .     "workers.A_19_3_5, "
         .     "workers.A_19_3_6, "
         .     "workers.A_19_3_7, "
         .     "workers.A_19_4_1, "
         .     "workers.A_19_4_2, "
         .     "DATE_FORMAT(workers.A_19_4_3, '%Y') AS A_19_4_3_year, "
         .     "DATE_FORMAT(workers.A_19_4_3, '%m') AS A_19_4_3_month, "
         .     "DATE_FORMAT(workers.A_19_4_3, '%d') AS A_19_4_3_day, "
         .     "workers.A_19_4_4, "
         .     "workers.A_19_4_5, "
         .     "workers.A_19_4_6, "
         .     "workers.A_19_4_7, "
         .     "workers.A_20_1, "
         .     "workers.A_20_2, "
         .     "workers.A_20_3, "
         .     "workers.A_21_1, "
         .     "workers.A_21_3, "
         .     "workers.A_21_4, "
         .     "workers.A_21_5, "
         .     "workers.A_21_7, "
         .     "workers.A_22_1, "
         .     "workers.A_22_3, "
         .     "workers.A_22_4, "
         .     "workers.A_22_5, "
         .     "workers.A_22_7, "
         .     "workers.A_23_1_1, "
         .     "workers.A_23_1_2, "
         .     "workers.A_23_1_3, "
         .     "workers.A_23_2_1, "
         .     "workers.A_23_2_2, "
         .     "workers.A_23_2_3, "
         .     "workers.A_24_1, "
         .     "workers.A_24_2, "
         .     "workers.A_25_1, "
         .     "workers.A_25_2, "
         .     "workers.A_25_3, "
         .     "workers.A_26_1, "
         .     "workers.A_26_2, "
         .     "workers.A_26_3, "
         .     "workers.A_27, "
         .     "workers.A_28, "
         .     "workers.A_29, "
         .     "workers.A_30, "
         .     "DATE_FORMAT(workers.A_31_1_1, '%Y') AS A_31_1_1_year, "
         .     "DATE_FORMAT(workers.A_31_1_1, '%m') AS A_31_1_1_month, "
         .     "DATE_FORMAT(workers.A_31_1_2, '%Y') AS A_31_1_2_year, "
         .     "DATE_FORMAT(workers.A_31_1_2, '%m') AS A_31_1_2_month, "
         .     "workers.A_31_1_3, "
         .     "DATE_FORMAT(workers.A_31_2_1, '%Y') AS A_31_2_1_year, "
         .     "DATE_FORMAT(workers.A_31_2_1, '%m') AS A_31_2_1_month, "
         .     "DATE_FORMAT(workers.A_31_2_2, '%Y') AS A_31_2_2_year, "
         .     "DATE_FORMAT(workers.A_31_2_2, '%m') AS A_31_2_2_month, "
         .     "workers.A_31_2_3, "
         .     "DATE_FORMAT(workers.A_31_3_1, '%Y') AS A_31_3_1_year, "
         .     "DATE_FORMAT(workers.A_31_3_1, '%m') AS A_31_3_1_month, "
         .     "DATE_FORMAT(workers.A_31_3_2, '%Y') AS A_31_3_2_year, "
         .     "DATE_FORMAT(workers.A_31_3_2, '%m') AS A_31_3_2_month, "
         .     "workers.A_31_3_3, "
         .     "DATE_FORMAT(workers.A_31_4_1, '%Y') AS A_31_4_1_year, "
         .     "DATE_FORMAT(workers.A_31_4_1, '%m') AS A_31_4_1_month, "
         .     "DATE_FORMAT(workers.A_31_4_2, '%Y') AS A_31_4_2_year, "
         .     "DATE_FORMAT(workers.A_31_4_2, '%m') AS A_31_4_2_month, "
         .     "workers.A_31_4_3, "
         .     "DATE_FORMAT(workers.A_31_5_1, '%Y') AS A_31_5_1_year, "
         .     "DATE_FORMAT(workers.A_31_5_1, '%m') AS A_31_5_1_month, "
         .     "DATE_FORMAT(workers.A_31_5_2, '%Y') AS A_31_5_2_year, "
         .     "DATE_FORMAT(workers.A_31_5_2, '%m') AS A_31_5_2_month, "
         .     "workers.A_31_5_3, "
         .     "DATE_FORMAT(workers.A_31_6_1, '%Y') AS A_31_6_1_year, "
         .     "DATE_FORMAT(workers.A_31_6_1, '%m') AS A_31_6_1_month, "
         .     "DATE_FORMAT(workers.A_31_6_2, '%Y') AS A_31_6_2_year, "
         .     "DATE_FORMAT(workers.A_31_6_2, '%m') AS A_31_6_2_month, "
         .     "workers.A_31_6_3, "
         .     "workers.A_32, "
         .     "workers.A_33, "
         .     "workers.A_34, "
         .     "workers.A_35, "
         .     "workers.A_36, "
         .     "workers.A_37, "
         .     "workers.A_38, "
         .     "workers.A_39, "
         .     "workers.A_40, "
         .     "workers.A_41, "

         //変更許可申請書・在留期間更新許可申請書　固有項目
         .     "workers.A_09_2, "
         .     "DATE_FORMAT(workers.A_09_3, '%Y') AS A_09_3_year, "
         .     "DATE_FORMAT(workers.A_09_3, '%m') AS A_09_3_month, "
         .     "DATE_FORMAT(workers.A_09_3, '%d') AS A_09_3_day, "
         .     "workers.A_10, "
         .     "workers.A_12, "
         .     "workers.A_13, "
         .     "workers.A_11_1, " 
         .     "workers.A_11_2, "
         .     "workers.A_14, "
         .     "workers.A_19_5_1, "
         .     "workers.A_19_5_2, "
         .     "DATE_FORMAT(workers.A_19_5_3, '%Y') AS A_19_5_3_year, "
         .     "DATE_FORMAT(workers.A_19_5_3, '%m') AS A_19_5_3_month, "
         .     "DATE_FORMAT(workers.A_19_5_3, '%d') AS A_19_5_3_day, "
         .     "workers.A_19_5_4, "
         .     "workers.A_19_5_5, "
         .     "workers.A_19_5_6, "
         .     "workers.A_19_5_7, "
         .     "workers.A_19_6_1, "
         .     "workers.A_19_6_2, "
         .     "DATE_FORMAT(workers.A_19_6_3, '%Y') AS A_19_6_3_year, "
         .     "DATE_FORMAT(workers.A_19_6_3, '%m') AS A_19_6_3_month, "
         .     "DATE_FORMAT(workers.A_19_6_3, '%d') AS A_19_6_3_day, "
         .     "workers.A_19_6_4, "
         .     "workers.A_19_6_5, "
         .     "workers.A_19_6_6, "
         .     "workers.A_19_6_7 "
         . "FROM mst_workers workers "
         . "WHERE workers.worker_id = :worker_id "
         ;

    $params = array();
    $params[":worker_id"] = $workerId;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);
    
    // 表示用データを追加設定。



    // 取得結果を返す。
    return $result;
}
/**
 * 年を返す。
 *
 * @return 
 */
function getYears() {
    $year = "1950";
    $count = 80;
    $years = array();
    for ($i=0; $i < $count; $i++) {
        $years[] = $year;  
        $year++;
    }
    return $years;
}
/**
 * 月を返す。
 *
 * @return array[項目名] フォーム項目名の一覧
 */
function getMonths() {
    return array(
        "01", "02", "03", "04", "05", "06","07","08","09","10","11","12"
    );
}
/**
 * 日を返す。
 *
 * @return array[項目名] フォーム項目名の一覧
 */
function getDays() {
    return array(
        "01", "02", "03", "04", "05", "06","07","08","09","10","11","12","13","14","15",
        "16", "17", "18", "19", "20", "21","22","23","24","25","26","27","28","29","30",
        "31"

    );
}
/**
 * 期間（年)を返す。
 *
 * @return array[項目名] フォーム項目名の一覧
 */
function getPeriodYear() {
    $period_year = 0;
    $count = 101;
    $years = array();
    for ($i=0; $i < $count; $i++) {
        $years[] = $period_year;  
        $period_year++;
    }
    return $years;
}

/**
 * 期間（年)を返す。
 *
 * @return array[項目名] フォーム項目名の一覧
 */
function getPeriodMonth() {
    $period_month = 0;
    $count = 13;
    $months = array();
    for ($i=0; $i < $count; $i++) {
        $months[] = $period_month;  
        $period_month++;
    }
    return $months;
}
/**
 * 登録処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function update($items, $workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        // 労働者の行IDを取得。
        $workerRowId = getWorkerRowId($db, $workerId);

        // 年、月、日を「年/月/日」にセット
        $birthday      = setDate($items["birthday_year"], $items["birthday_month"], $items["birthday_day"]);
        $A_01_2        = setDate($items["A_01_2_year"],   $items["A_01_2_month"],   $items["A_01_2_day"]);
        $A_03          = setDate($items["A_03_year"],     $items["A_03_month"],     $items["A_03_day"]);
        $A_08_3        = setDate($items["A_08_3_year"],   $items["A_08_3_month"],   $items["A_08_3_day"]);
        $A_08_4        = setDate($items["A_08_4_year"],   $items["A_08_4_month"],   $items["A_08_4_day"]);
        $A_17_2        = setDate($items["A_17_2_year"],   $items["A_17_2_month"],   $items["A_17_2_day"]);
        $A_19_1_3      = setDate($items["A_19_1_3_year"], $items["A_19_1_3_month"], $items["A_19_1_3_day"]);
        $A_19_2_3      = setDate($items["A_19_2_3_year"], $items["A_19_2_3_month"], $items["A_19_2_3_day"]);
        $A_19_3_3      = setDate($items["A_19_3_3_year"], $items["A_19_3_3_month"], $items["A_19_3_3_day"]);
        $A_19_4_3      = setDate($items["A_19_4_3_year"], $items["A_19_4_3_month"], $items["A_19_4_3_day"]);

        $A_31_1_1      = setDateWork($items["A_31_1_1_year"], $items["A_31_1_1_month"]);
        $A_31_1_2      = setDateWork($items["A_31_1_2_year"], $items["A_31_1_2_month"]);
        $A_31_2_1      = setDateWork($items["A_31_2_1_year"], $items["A_31_2_1_month"]);
        $A_31_2_2      = setDateWork($items["A_31_2_2_year"], $items["A_31_2_2_month"]);
        $A_31_3_1      = setDateWork($items["A_31_3_1_year"], $items["A_31_3_1_month"]);
        $A_31_3_2      = setDateWork($items["A_31_3_2_year"], $items["A_31_3_2_month"]);
        $A_31_4_1      = setDateWork($items["A_31_4_1_year"], $items["A_31_4_1_month"]);
        $A_31_4_2      = setDateWork($items["A_31_4_2_year"], $items["A_31_4_2_month"]);
        $A_31_5_1      = setDateWork($items["A_31_5_1_year"], $items["A_31_5_1_month"]);
        $A_31_5_2      = setDateWork($items["A_31_5_2_year"], $items["A_31_5_2_month"]);
        $A_31_6_1      = setDateWork($items["A_31_6_1_year"], $items["A_31_6_1_month"]);
        $A_31_6_2      = setDateWork($items["A_31_6_2_year"], $items["A_31_6_2_month"]);

        // 変更許可申請書・在留期間更新許可申請書　固有項目
        $A_09_3        = setDate($items["A_09_3_year"],   $items["A_09_3_month"],   $items["A_09_3_day"]);
        $A_19_5_3      = setDate($items["A_19_5_3_year"], $items["A_19_5_3_month"], $items["A_19_5_3_day"]);
        $A_19_6_3      = setDate($items["A_19_6_3_year"], $items["A_19_6_3_month"], $items["A_19_6_3_day"]);

        // 試験による証明をセット
        $A_21_2 = setSelect_Proof($items["A_21_3"], $items["A_21_4"], $items["A_21_5"]); 
        $A_22_2 = setSelect_Proof($items["A_22_3"], $items["A_22_4"], $items["A_22_4"]); 

        // その他の評価方法による証明をセット
        $A_21_6 = setSelect_Proof_etc($items["A_21_7"]); 
        $A_22_6 = setSelect_Proof_etc($items["A_22_7"]); 
        
        // 現に有する在留資格のセット
        if ($items["qualifications"] == "6") {
            // 在留資格で「その他」の場合、在留資格のその他情報をセット
            $A_09_1  = $items["qualifications_etc"];
        } else {
            $A_09_1  = $items["qualifications"];
        }

        // カラムと設定値を定義。
        $columns = array(
            "worker_id"             => $items["worker_id"],
            "user_name"             => $items["user_name"],
            "user_name_e"           => $items["user_name_e"],
            "disp_lang"             => $items["disp_lang"],
            "qualifications"        => $items["qualifications"],
            "qualifications_etc"    => $items["qualifications_etc"],
            "status"                => $items["status"],
            "nationality_region"    => $items["nationality_region"],
            "birthday"              => setDateParam($birthday),
            "family_name"           => $items["family_name"],
            "given_name"            => $items["given_name"],
            "sex"                   => $items["sex"],
            "place_birth"           => $items["place_birth"],
            "marital_flag"          => $items["marital_flag"],
            "occupation"            => $items["occupation"],
            "home_town"             => $items["home_town"],
            "postal_code"           => $items["postal_code"],
            "address"               => $items["address"],
            "telephone"             => $items["telephone"],
            "cellular_phone"        => $items["cellular_phone"],
            "A_01_1"                => $items["A_01_1"],
            "A_01_2"                => setDateParam($A_01_2),
            "A_02_1"                => $items["A_02_1"],
            "A_02_2"                => $items["A_02_2"],
            "A_03"                  => setDateParam($A_03),
            "A_04"                  => $items["A_04"],
            "A_05"                  => $items["A_05"],
            "A_06"                  => $items["A_06"],
            "A_07"                  => $items["A_07"],
            "A_08_1"                => $items["A_08_1"],
            "A_08_2"                => $items["A_08_2"],
            "A_08_3"                => setDateParam($A_08_3),
            "A_08_4"                => setDateParam($A_08_4),
            "A_15_1"                => $items["A_15_1"],
            "A_15_2"                => $items["A_15_2"],
            "A_16_1"                => $items["A_16_1"],
            "A_17_1"                => $items["A_17_1"],
            "A_17_2"                => setDateParam($A_17_2),
            "A_18_1"                => $items["A_18_1"],
            "A_19_1_1"              => $items["A_19_1_1"],
            "A_19_1_2"              => $items["A_19_1_2"],
            "A_19_1_3"              => setDateParam($A_19_1_3),
            "A_19_1_4"              => $items["A_19_1_4"],
            "A_19_1_5"              => $items["A_19_1_5"],
            "A_19_1_6"              => $items["A_19_1_6"],
            "A_19_1_7"              => $items["A_19_1_7"],
            "A_19_2_1"              => $items["A_19_2_1"],
            "A_19_2_2"              => $items["A_19_2_2"],
            "A_19_2_3"              => setDateParam($A_19_2_3),
            "A_19_2_4"              => $items["A_19_2_4"],
            "A_19_2_5"              => $items["A_19_2_5"],
            "A_19_2_6"              => $items["A_19_2_6"],
            "A_19_2_7"              => $items["A_19_2_7"],
            "A_19_3_1"              => $items["A_19_3_1"],
            "A_19_3_2"              => $items["A_19_3_2"],
            "A_19_3_3"              => setDateParam($A_19_3_3),
            "A_19_3_4"              => $items["A_19_3_4"],
            "A_19_3_5"              => $items["A_19_3_5"],
            "A_19_3_6"              => $items["A_19_3_6"],
            "A_19_3_7"              => $items["A_19_3_7"],
            "A_19_4_1"              => $items["A_19_4_1"],
            "A_19_4_2"              => $items["A_19_4_2"],
            "A_19_4_3"              => setDateParam($A_19_4_3),
            "A_19_4_4"              => $items["A_19_4_4"],
            "A_19_4_5"              => $items["A_19_4_5"],
            "A_19_4_6"              => $items["A_19_4_6"],
            "A_19_4_7"              => $items["A_19_4_7"],
            "A_20_1"                => $items["A_20_1"],
            "A_20_2"                => $items["A_20_2"],
            "A_20_3"                => $items["A_20_3"],
            "A_21_1"                => $items["A_21_1"],
            "A_21_2"                => $A_21_2,
            "A_21_3"                => $items["A_21_3"],
            "A_21_4"                => $items["A_21_4"],
            "A_21_5"                => $items["A_21_5"],
            "A_21_6"                => $A_21_6,
            "A_21_7"                => $items["A_21_7"],
            "A_22_1"                => $items["A_22_1"],
            "A_22_2"                => $A_22_2,
            "A_22_3"                => $items["A_22_3"],
            "A_22_4"                => $items["A_22_4"],
            "A_22_5"                => $items["A_22_5"],
            "A_22_6"                => $A_22_6,
            "A_22_7"                => $items["A_22_7"],
            "A_23_1_1"              => $items["A_23_1_1"],
            "A_23_1_2"              => $items["A_23_1_2"],
            "A_23_1_3"              => $items["A_23_1_3"],
            "A_23_2_1"              => $items["A_23_2_1"],
            "A_23_2_2"              => $items["A_23_2_2"],
            "A_23_2_3"              => $items["A_23_2_3"],
            "A_24_1"                => $items["A_24_1"],
            "A_24_2"                => $items["A_24_2"],
            "A_25_1"                => $items["A_25_1"],
            "A_25_2"                => $items["A_25_2"],
            "A_25_3"                => $items["A_25_3"],
            "A_26_1"                => $items["A_26_1"],
            "A_26_2"                => $items["A_26_2"],
            "A_26_3"                => $items["A_26_3"],
            "A_27"                  => $items["A_27"],
            "A_28"                  => $items["A_28"],
            "A_29"                  => $items["A_29"],
            "A_30"                  => $items["A_30"],
            "A_31_1_1"              => setDateParam($A_31_1_1),
            "A_31_1_2"              => setDateParam($A_31_1_2),
            "A_31_1_3"              => $items["A_31_1_3"],
            "A_31_2_1"              => setDateParam($A_31_2_1),
            "A_31_2_2"              => setDateParam($A_31_2_2),
            "A_31_2_3"              => $items["A_31_2_3"],
            "A_31_3_1"              => setDateParam($A_31_3_1),
            "A_31_3_2"              => setDateParam($A_31_3_2),
            "A_31_3_3"              => $items["A_31_3_3"],
            "A_31_4_1"              => setDateParam($A_31_4_1),
            "A_31_4_2"              => setDateParam($A_31_4_2),
            "A_31_4_3"              => $items["A_31_4_3"],
            "A_31_5_1"              => setDateParam($A_31_5_1),
            "A_31_5_2"              => setDateParam($A_31_5_2),
            "A_31_5_3"              => $items["A_31_5_3"],
            "A_31_6_1"              => setDateParam($A_31_6_1),
            "A_31_6_2"              => setDateParam($A_31_6_2),
            "A_31_6_3"              => $items["A_31_6_3"],
            "A_32"                  => $items["A_32"],
            "A_33"                  => $items["A_33"],
            "A_34"                  => $items["A_34"],
            "A_35"                  => $items["A_35"],
            "A_36"                  => $items["A_36"],
            "A_37"                  => $items["A_37"],
            "A_38"                  => $items["A_38"],
            "A_39"                  => $items["A_39"],
            "A_40"                  => $items["A_40"],
            "A_41"                  => $items["A_41"],
            
            // //変更許可申請書・在留期間更新許可申請書　固有項目
            "A_09_1"                => $A_09_1,
            "A_09_2"                => $items["A_09_2"],
            "A_09_3"                => setDateParam($A_09_3),
            "A_10"                  => $items["A_10"],
            "A_12"                  => $items["A_12"],
            "A_13"                  => $items["A_13"],
            "A_11_1"                => $items["A_11_1"],
            "A_11_2"                => $items["A_11_2"],
            "A_14"                  => $items["A_14"],
            "A_19_5_1"              => $items["A_19_5_1"],
            "A_19_5_2"              => $items["A_19_5_2"],
            "A_19_5_3"              => setDateParam($A_19_5_3),
            "A_19_5_4"              => $items["A_19_5_4"],
            "A_19_5_5"              => $items["A_19_5_5"],
            "A_19_5_6"              => $items["A_19_5_6"],
            "A_19_5_7"              => $items["A_19_5_7"],
            "A_19_6_1"              => $items["A_19_6_1"],
            "A_19_6_2"              => $items["A_19_6_2"],
            "A_19_6_3"              => setDateParam($A_19_6_3),
            "A_19_6_4"              => $items["A_19_6_4"],
            "A_19_6_5"              => $items["A_19_6_5"],
            "A_19_6_6"              => $items["A_19_6_6"],
            "A_19_6_7"             => $items["A_19_6_7"]
        );
        $whereString = "id = :id ";
        $whereParams = array(
            "id" => $workerRowId
        );
    
        // update処理を発行する。
        $db->update("mst_workers", $columns, $whereString, $whereParams);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}
/**
 * 労働者の行IDを取得する。
 *
 * @param PDO $db DB接続
 * @param string $workerId 取得対象の労働者ID
 * 
 * @return integer 行ID
 */
function getWorkerRowId(&$db, $workerId) {
    $sql = "SELECT id AS rowid "
         . "FROM mst_workers "
         . "WHERE worker_id = :worker_id ";
    $params[":worker_id"] = $workerId;
    $result = $db->selectOne($sql, $params);
    return $result["rowid"];
}
/**
 * 日付をセットする。
 *
 * @param string $year   年
 * @param string $month  月
 * @param string $day    日
 * 
 * @return string 日付
 */
function setDate($year, $month,$day) {
    $date ="";
    if ($year !="" && $month !="" && $day !="") {
        // YYYY/MM/DDにセット
        $date = $year."/".$month."/".$day;
    }
    return $date;
}
/**
 * 職歴の日付をセットする。
 *
 * @param string $year   年
 * @param string $month  月
 * 
 * @return string 日付
 */
function setDateWork($year, $month) {
    $date ="";
    if ($year !="" && $month !="") {
        // YYYY/MM/01にセット
        $date = $year."/".$month."/"."01";
    }
    return $date;
}

/**
 * 技能水準・日本語能力の証明書の選択肢をセットする。
 *
 * @param string $item   
 * @param string $item2  
 * @param string $item3  
 * 
 * @return string $result
 * 
 */
function setSelect_Proof($item, $item2, $item3) {

    $result = "";

    // 合格した試験の証明が全て値が空の場合、2をセット
    if ($item == "" &&  $item2 == "" && $item3 == "") {
        $result = "2";
    } else {
        $result = "1";
    }
    return $result;
}
/**
 * 技能水準・日本語能力のその他の評価方法による証明の選択肢をセットする。
 *
 * @param string $item   
 * 
 * @return string $result
 * 
 */
function setSelect_Proof_etc($item) {

    $result = "";

    // その他の評価方法による証明が空の場合、2をセット
    if ($item == "") {
        $result = "2";
    } else {
        $result = "1";
    }
    return $result;
}
