<?php

/**
 * 「支援計画書」の帳票情報を取得し、結果を返す。
 *
 * @param $targetWorkerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function makePdfData04($targetWorkerId){
    $db = new JinzaiDb(DB_DEFINE);

    //-----------------------------------------
    // DBから支援計画書の情報を取得する。
    //-----------------------------------------
    $row = searchPDF04($db, $targetWorkerId);
    $supinfo = getSupportCompanyInfo();

    //-----------------------------------------
    // DBの情報から帳票情報を作成する。
    //-----------------------------------------
    $datas = Array();

    $companyDataCache = array();  // 企業ID→支援担当者の参照のためのワーク。

    $datas['label_01'] = $row['create_date_Y'];    //作成日：年
    $datas['label_02'] = $row['create_date_M'];    //作成日：月
    $datas['label_03'] = $row['create_date_D'];    //作成日：日
    $datas['01_01_01'] = $row['user_name_e'];      //支援対象者：氏名
    if (strcmp(strval($row['sex']), "1") == 0) {
        $datas['01_02_male'] = $row['sex'];        //支援対象者：性別：男
    }
    if (strcmp(strval($row['sex']), "2") == 0) {
        $datas['01_02_female'] = $row['sex'];      //支援対象者：性別：女
    }
    $datas['01_03_01'] = $row['birthday_Y'];    //支援対象者：生年月日：年
    $datas['01_03_02'] = $row['birthday_M'];    //支援対象者：生年月日：月
    $datas['01_03_03'] = $row['birthday_D'];    //支援対象者：生年月日：日
    $datas['01_04'] = $row['nationality_region'];    //支援対象者：国籍・地域
    $datas['02_01_01'] = $row['company_kana'];    //特定技能所属機関：氏名（ふりがな）
    $datas['02_01_02'] = $row['company_name'];    //特定技能所属機関：氏名
    //特定技能所属機関：郵便番号の「ー」分割　★★★            
    $postalAry = preg_split("/[-]/",$row['postal_code']);
    if(count($postalAry)==1){
        $datas['02_02_01_02'] = $postalAry[0];         //特定技能所属機関：住所：郵便番号例外
    }elseif(count($postalAry)==2){
        $datas['02_02_01_01'] = $postalAry[0];         //特定技能所属機関：住所：郵便番号3桁
        $datas['02_02_01_02'] = $postalAry[1];         //特定技能所属機関：住所：郵便番号4桁
    }
    $datas['02_02_02'] = $row['address'];    //特定技能所属機関：住所：住所
    //特定技能所属機関：電話番号の「ー」分割　★★★
    $telAry = preg_split("/[-]/",$row['tel']);
    if(count($telAry)==1){
        $datas['02_02_05'] = $telAry[0];            //特定技能所属機関：住所：電話番号例外１
    }elseif(count($telAry)==2){
        $datas['02_02_04'] = $telAry[0];            //特定技能所属機関：住所：電話番号例外２
        $datas['02_02_05'] = $telAry[1];            //特定技能所属機関：住所：電話番号例外２
    }elseif(count($telAry)>=3){
        $datas['02_02_03'] = $telAry[0];            //特定技能所属機関：住所：電話番号
        $datas['02_02_04'] = $telAry[1];            //特定技能所属機関：住所：電話番号
        $datas['02_02_05'] = $telAry[2];            //特定技能所属機関：住所：電話番号
    }
    $datas['02_03_01'] = $row['corporation_no'];    //特定技能所属機関：法人番号
    $datas['02_04_01_01'] = $row['support_manager_kana'];    //特定技能所属機関：支援責任者：氏名（ふりがな）
    $datas['02_04_01_02'] = $row['support_manager_name'];    //特定技能所属機関：支援責任者：氏名
    $datas['02_04_01_03'] = $row['support_manager_department'];    //特定技能所属機関：支援責任者：役職
    $datas['02_04_02'] = $row['worker_mumber'];    //特定技能所属機関：1号特定技能外国人数
    $datas['02_04_03'] = $row['support_stuff_number'];    //特定技能所属機関：支援担当者数

    // page 2
    $datas['03_01_02'] = $supinfo['registration_no'];             //登録支援機関：登録番号

    $datas['03_02_01'] = $supinfo['registration_date_Y'];         //登録支援機関：登録年月日：年
    $datas['03_02_02'] = $supinfo['registration_date_M'];         //登録支援機関：登録年月日：月
    $datas['03_02_03'] = $supinfo['registration_date_D'];         //登録支援機関：登録年月日：日

    $datas['03_03_01'] = $supinfo['support_scheduled_date_Y'];    //登録支援機関：予定年月日：年
    $datas['03_03_02'] = $supinfo['support_scheduled_date_M'];    //登録支援機関：予定年月日：月
    $datas['03_03_03'] = $supinfo['support_scheduled_date_D'];    //登録支援機関：予定年月日：日

    $datas['03_04_01'] = $supinfo['company_kana'];                //登録支援機関：氏名（ふりがな）
    $datas['03_04_02'] = $supinfo['company_name'];                //登録支援機関：氏名

    $postalAry = preg_split("/[-]/", $supinfo['postal_code']);
    if(count($postalAry)==1){
        $datas['03_05_01_02'] = $postalAry[0];      //登録支援機関：住所：郵便番号例外
    }elseif(count($postalAry)==2){
        $datas['03_05_01_01'] = $postalAry[0];      //登録支援機関：住所：郵便番号3桁
        $datas['03_05_01_02'] = $postalAry[1];      //登録支援機関：住所：郵便番号4桁
    }

    $datas['03_05_02'] = $supinfo['address'];       //登録支援機関：住所：住所

    $telAry = preg_split("/[-]/",$supinfo['tel']);
    if(count($telAry)==1){
        $datas['03_05_05'] = $telAry[0];            //特定技能所属機関：住所：電話番号例外１
    }elseif(count($telAry)==2){
        $datas['03_05_04'] = $telAry[0];            //特定技能所属機関：住所：電話番号例外２
        $datas['03_05_05'] = $telAry[1];            //特定技能所属機関：住所：電話番号例外２
    }elseif(count($telAry)>=3){
        $datas['03_05_03'] = $telAry[0];            //特定技能所属機関：住所：電話番号
        $datas['03_05_04'] = $telAry[1];            //特定技能所属機関：住所：電話番号
        $datas['03_05_05'] = $telAry[2];            //特定技能所属機関：住所：電話番号
    }

    $datas['03_06_01'] = $supinfo['representative_kana'];    //登録支援機関：代表者の氏名（ふりがな）
    $datas['03_06_02'] = $supinfo['representative_name'];    //登録支援機関：代表者の氏名

    $datas['03_07'] = $supinfo['corporation_no'];      //登録支援機関：法人番号

    $postalAry = preg_split("/[-]/",$supinfo['support_postal_code']);
    if(count($postalAry)==1){
        $datas['03_08_01_02'] = $postalAry[0];         //支援を行う事務所の所在地：住所：郵便番号例外
    }elseif(count($postalAry)==2){
        $datas['03_08_01_01'] = $postalAry[0];         //登録支援機関：住所：郵便番号3桁
        $datas['03_08_01_02'] = $postalAry[1];         //支援を行う事務所の所在地：住所：郵便番号4桁
    }

    $datas['03_08_02'] = $supinfo['support_address'];  //支援を行う事務所の所在地：事務所の所在地：住所

    $telAry = preg_split("/[-]/",$supinfo['support_tel']);
    if(count($telAry)==1){
        $datas['03_08_05'] = $telAry[0];            //支援を行う事務所の所在地：住所：電話番号例外１
    }elseif(count($telAry)==2){
        $datas['03_08_04'] = $telAry[0];            //支援を行う事務所の所在地：住所：電話番号例外２
        $datas['03_08_05'] = $telAry[1];            //支援を行う事務所の所在地：住所：電話番号例外２
    }elseif(count($telAry)>=3){
        $datas['03_08_03'] = $telAry[0];            //支援を行う事務所の所在地：住所：電話番号
        $datas['03_08_04'] = $telAry[1];            //支援を行う事務所の所在地：住所：電話番号
        $datas['03_08_05'] = $telAry[2];            //支援を行う事務所の所在地：住所：電話番号
    }

    $datas['03_09_01_01'] = $supinfo['support_manager_kana'];          //登録支援機関：概要：支援責任者：氏名（ふりがな）
    $datas['03_09_01_02'] = $supinfo['support_manager_name'];          //登録支援機関：概要：支援責任者：氏名
    $datas['03_09_01_03'] = $supinfo['support_manager_department'];    //登録支援機関：概要：支援責任者：役職

    $datas['03_09_02'] = $supinfo['worker_mumber'];                    //登録支援機関：概要：1号特定技能外国人数
    $datas['03_09_03'] = $supinfo['support_stuff_number'];             //登録支援機関：概要：支援担当者数

    // page 3
    if( strcmp(  strval($row['C_1_01_1']) , "1") == 0 ){
        $datas['04_01_a_a_01'] = $row['C_1_01_1'];    //4：1-ア-a：実施予定有
        $datas['04_01_a_a_03'] = $row['C_1_01_2'];    //4：1-ア-a：実施予定有：内容
    }
    if( strcmp(  strval($row['C_1_01_1']) , "2") == 0 ){
        $datas['04_01_a_a_02'] = $row['C_1_01_1'];    //4：1-ア-a：実施予定無
        $datas['04_01_a_a_04'] = $row['C_1_01_2'];    //4：1-ア-a：実施予定無：内容
    }
    if( strcmp(  strval($row['C_1_01_3']) , "1") == 0 ){
        $datas['04_01_a_a_05_yes'] = $row['C_1_01_3'];    //4：1-ア-a：委託有
    }
    if( strcmp(  strval($row['C_1_01_3']) , "2") == 0 ){
        $datas['04_01_a_a_05_no'] = $row['C_1_01_3'];    //4：1-ア-a：委託無
    }
    if( strcmp(  strval($row['C_1_01_1']) , "2") <> 0 ){
        $datas['04_01_a_a_06'] = convSuppoterName($db, $row["C_1_01_4"], $companyDataCache);    //4：1-ア-a：氏名（役職）
    }
    if( strcmp(  strval($row['C_1_01_3']) , "1") == 0 ){
        //4：1-ア-a：郵便番号の「ー」分割　★★★            
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_01_a_a_08'] = $postalAry[0];         //4：1-ア-a：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_01_a_a_07'] = $postalAry[0];         //4：1-ア-a：住所：郵便番号3桁
            $datas['04_01_a_a_08'] = $postalAry[1];         //4：1-ア-a：住所：郵便番号4桁
        }
        $datas['04_01_a_a_09'] = $row['support_address'];    //4：1-ア-a：住所：住所
    }
    if ($row['C_1_01_5_1'] == "1") $datas['04_01_a_a_10'] = true;    //4：1-ア-a：実施方法：対面
    if ($row['C_1_01_5_2'] == "1") $datas['04_01_a_a_11'] = true;    //4：1-ア-a：実施方法：テレビ電話装置
    if ($row['C_1_01_5_3'] == "1") $datas['04_01_a_a_12'] = true;    //4：1-ア-a：実施方法：その他
    $datas['04_01_a_a_13'] = $row['C_1_01_6'];    //4：1-ア-a：実施方法：その他：内容
    if( strcmp(  strval($row['C_1_02_1']) , "1") == 0 ){
        $datas['04_01_a_b_01'] = $row['C_1_02_1'];    //4：1-ア-b：実施予定有
        $datas['04_01_a_b_03'] = $row['C_1_02_2'];    //4：1-ア-b：実施予定有：内容
    }
    if( strcmp(  strval($row['C_1_02_1']) , "2") == 0 ){
        $datas['04_01_a_b_02'] = $row['C_1_02_1'];    //4：1-ア-b：実施予定無
        $datas['04_01_a_b_04'] = $row['C_1_02_2'];    //4：1-ア-b：実施予定無：内容
    }
    if( strcmp(  strval($row['C_1_02_3']) , "1") == 0 ){
        $datas['04_01_a_b_05_yes'] = $row['C_1_02_3'];    //4：1-ア-b：委託有
    }
    if( strcmp(  strval($row['C_1_02_3']) , "2") == 0 ){
        $datas['04_01_a_b_05_no'] = $row['C_1_02_3'];    //4：1-ア-b：委託無
    }
    if( strcmp(  strval($row['C_1_02_1']) , "2") <> 0 ){
        $datas['04_01_a_b_06'] = convSuppoterName($db, $row["C_1_02_4"], $companyDataCache);    //4：1-ア-b：氏名（役職）
    }
    if( strcmp(  strval($row['C_1_02_3']) , "1") == 0 ){
        //4：1-ア-b：郵便番号の「ー」分割　★★★            
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_01_a_b_08'] = $postalAry[0];         //4：1-ア-b：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_01_a_b_07'] = $postalAry[0];         //4：1-ア-b：住所：郵便番号3桁
            $datas['04_01_a_b_08'] = $postalAry[1];         //4：1-ア-b：住所：郵便番号4桁
        }
        $datas['04_01_a_b_09'] = $row['support_address'];    //4：1-ア-b：住所：住所
    }
    if ($row['C_1_02_5_1'] == "1") $datas['04_01_a_b_10'] = true;    //4：1-ア-b：実施方法：対面
    if ($row['C_1_02_5_2'] == "1") $datas['04_01_a_b_11'] = true;    //4：1-ア-b：実施方法：テレビ電話装置
    if ($row['C_1_02_5_3'] == "1") $datas['04_01_a_b_12'] = true;    //4：1-ア-b：実施方法：その他
    $datas['04_01_a_b_13'] = $row['C_1_02_6'];    //4：1-ア-b：実施方法：その他：内容
    if( strcmp(  strval($row['C_1_03_1']) , "1") == 0 ){
        $datas['04_01_a_c_01'] = $row['C_1_03_1'];    //4：1-ア-c：実施予定有
        $datas['04_01_a_c_03'] = $row['C_1_03_2'];    //4：1-ア-c：実施予定有：内容
    }
    if( strcmp(  strval($row['C_1_03_1']) , "2") == 0 ){
        $datas['04_01_a_c_02'] = $row['C_1_03_1'];    //4：1-ア-c：実施予定無
        $datas['04_01_a_c_04'] = $row['C_1_03_2'];    //4：1-ア-c：実施予定無：内容
    }
    if( strcmp(  strval($row['C_1_03_3']) , "1") == 0 ){
        $datas['04_01_a_c_05_yes'] = $row['C_1_03_3'];    //4：1-ア-c：委託有
    }
    if( strcmp(  strval($row['C_1_03_3']) , "2") == 0 ){
        $datas['04_01_a_c_05_no'] = $row['C_1_03_3'];    //4：1-ア-c：委託無
    }
    if( strcmp(  strval($row['C_1_03_1']) , "2") <> 0 ){
        $datas['04_01_a_c_06'] = convSuppoterName($db, $row["C_1_03_4"], $companyDataCache);    //4：1-ア-c：氏名（役職）
    }
    if( strcmp(  strval($row['C_1_03_3']) , "1") == 0 ){
    //4：1-ア-c：郵便番号の「ー」分割　★★★            
    $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_01_a_c_08'] = $postalAry[0];         //4：1-ア-c：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_01_a_c_07'] = $postalAry[0];         //4：1-ア-c：住所：郵便番号3桁
            $datas['04_01_a_c_08'] = $postalAry[1];         //4：1-ア-c：住所：郵便番号4桁
        }
        $datas['04_01_a_c_09'] = $row['support_address'];    //4：1-ア-c：住所：住所
    }
    if ($row['C_1_03_5_1'] == "1") $datas['04_01_a_c_10'] = true;    //4：1-ア-c：実施方法：対面
    if ($row['C_1_03_5_2'] == "1") $datas['04_01_a_c_11'] = true;    //4：1-ア-c：実施方法：テレビ電話装置
    if ($row['C_1_03_5_3'] == "1") $datas['04_01_a_c_12'] = true;    //4：1-ア-c：実施方法：その他
    $datas['04_01_a_c_13'] = $row['C_1_03_6'];    //4：1-ア-c：実施方法：その他：内容
    if( strcmp(  strval($row['C_1_04_1']) , "1") == 0 ){
        $datas['04_01_a_d_01'] = $row['C_1_04_1'];    //4：1-ア-d：実施予定有
        $datas['04_01_a_d_03'] = $row['C_1_04_2'];    //4：1-ア-d：実施予定有：内容
    }
    if( strcmp(  strval($row['C_1_04_1']) , "2") == 0 ){
        $datas['04_01_a_d_02'] = $row['C_1_04_1'];    //4：1-ア-d：実施予定無
        $datas['04_01_a_d_04'] = $row['C_1_04_2'];    //4：1-ア-d：実施予定無：内容
    }
    if( strcmp(  strval($row['C_1_04_3']) , "1") == 0 ){
        $datas['04_01_a_d_05_yes'] = $row['C_1_04_3'];    //4：1-ア-d：委託有
    }
    if( strcmp(  strval($row['C_1_04_3']) , "2") == 0 ){
        $datas['04_01_a_d_05_no'] = $row['C_1_04_3'];    //4：1-ア-d：委託無
    }
    if( strcmp(  strval($row['C_1_04_1']) , "2") <> 0 ){
        $datas['04_01_a_d_06'] = convSuppoterName($db, $row["C_1_04_4"], $companyDataCache);    //4：1-ア-d：氏名（役職）
    }
    if( strcmp(  strval($row['C_1_04_3']) , "1") == 0 ){
    //4：1-ア-c：郵便番号の「ー」分割　★★★            
    $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_01_a_d_08'] = $postalAry[0];         //4：1-ア-d：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_01_a_d_07'] = $postalAry[0];         //4：1-ア-d：住所：郵便番号3桁
            $datas['04_01_a_d_08'] = $postalAry[1];         //4：1-ア-d：住所：郵便番号4桁
        }
        $datas['04_01_a_d_09'] = $row['support_address'];    //4：1-ア-d：住所：住所
    }
    if ($row['C_1_04_5_1'] == "1") $datas['04_01_a_d_10'] = true;    //4：1-ア-d：実施方法：対面
    if ($row['C_1_04_5_2'] == "1") $datas['04_01_a_d_11'] = true;    //4：1-ア-d：実施方法：テレビ電話装置
    if ($row['C_1_04_5_3'] == "1") $datas['04_01_a_d_12'] = true;    //4：1-ア-d：実施方法：その他
    $datas['04_01_a_d_13'] = $row['C_1_04_6'];    //4：1-ア-d：実施方法：その他：内容
    if( strcmp(  strval($row['C_1_05_1']) , "1") == 0 ){
        $datas['04_01_a_e_01'] = $row['C_1_05_1'];    //4：1-ア-e：実施予定有
        $datas['04_01_a_e_03'] = $row['C_1_05_2'];    //4：1-ア-e：実施予定有：内容
    }
    if( strcmp(  strval($row['C_1_05_1']) , "2") == 0 ){
        $datas['04_01_a_e_02'] = $row['C_1_05_1'];    //4：1-ア-e：実施予定無
        $datas['04_01_a_e_04'] = $row['C_1_05_2'];    //4：1-ア-e：実施予定無：内容
    }
    if( strcmp(  strval($row['C_1_05_3']) , "1") == 0 ){
        $datas['04_01_a_e_05_yes'] = $row['C_1_05_3'];    //4：1-ア-e：委託有
    }
    if( strcmp(  strval($row['C_1_05_3']) , "2") == 0 ){
        $datas['04_01_a_e_05_no'] = $row['C_1_05_3'];    //4：1-ア-e：委託無
    }
    if( strcmp(  strval($row['C_1_05_1']) , "2") <> 0 ){
        $datas['04_01_a_e_06'] = convSuppoterName($db, $row["C_1_05_4"], $companyDataCache);    //4：1-ア-e：氏名（役職）
    }
    if( strcmp(  strval($row['C_1_05_3']) , "1") == 0 ){
        //4：1-ア-e：郵便番号の「ー」分割　★★★            
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_01_a_e_08'] = $postalAry[0];         //4：1-ア-e：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_01_a_e_07'] = $postalAry[0];         //4：1-ア-e：住所：郵便番号3桁
            $datas['04_01_a_e_08'] = $postalAry[1];         //4：1-ア-e：住所：郵便番号4桁
        }
        $datas['04_01_a_e_09'] = $row['support_address'];    //4：1-ア-e：住所：住所
    }
    if ($row['C_1_05_5_1'] == "1") $datas['04_01_a_e_10'] = true;    //4：1-ア-e：実施方法：対面
    if ($row['C_1_05_5_2'] == "1") $datas['04_01_a_e_11'] = true;    //4：1-ア-e：実施方法：テレビ電話装置
    if ($row['C_1_05_5_3'] == "1") $datas['04_01_a_e_12'] = true;    //4：1-ア-e：実施方法：その他
    $datas['04_01_a_e_13'] = $row['C_1_05_6'];    //4：1-ア-e：実施方法：その他：内容
    if( strcmp(  strval($row['C_1_06_1']) , "1") == 0 ){
        $datas['04_01_a_f_01'] = $row['C_1_06_1'];    //4：1-ア-f：実施予定有
        $datas['04_01_a_f_03'] = $row['C_1_06_2'];    //4：1-ア-f：実施予定有：内容
    }
    if( strcmp(  strval($row['C_1_06_1']) , "2") == 0 ){
        $datas['04_01_a_f_02'] = $row['C_1_06_1'];    //4：1-ア-f：実施予定無
        $datas['04_01_a_f_04'] = $row['C_1_06_2'];    //4：1-ア-f：実施予定無：内容
    }
    if( strcmp(  strval($row['C_1_06_3']) , "1") == 0 ){
        $datas['04_01_a_f_05_yes'] = $row['C_1_06_3'];    //4：1-ア-f：委託有
    }
    if( strcmp(  strval($row['C_1_06_3']) , "2") == 0 ){
        $datas['04_01_a_f_05_no'] = $row['C_1_06_3'];    //4：1-ア-f：委託無
    }
    if( strcmp(  strval($row['C_1_06_1']) , "2") <> 0 ){
        $datas['04_01_a_f_06'] = convSuppoterName($db, $row["C_1_06_4"], $companyDataCache);    //4：1-ア-f：氏名（役職）
    }
    if( strcmp(  strval($row['C_1_06_3']) , "1") == 0 ){
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_01_a_f_08'] = $postalAry[0];         //4：1-ア-f：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_01_a_f_07'] = $postalAry[0];         //4：1-ア-f：住所：郵便番号3桁
            $datas['04_01_a_f_08'] = $postalAry[1];         //4：1-ア-f：住所：郵便番号4桁
        }
        $datas['04_01_a_f_09'] = $row['support_address'];    //4：1-ア-f：住所：住所
    }
    if ($row['C_1_06_5_1'] == "1") $datas['04_01_a_f_10'] = true;    //4：1-ア-f：実施方法：対面
    if ($row['C_1_06_5_2'] == "1") $datas['04_01_a_f_11'] = true;    //4：1-ア-f：実施方法：テレビ電話装置
    if ($row['C_1_06_5_3'] == "1") $datas['04_01_a_f_12'] = true;    //4：1-ア-f：実施方法：その他
    $datas['04_01_a_f_13'] = $row['C_1_06_6'];    //4：1-ア-f：実施方法：その他：内容
    if( strcmp(  strval($row['C_1_07_1']) , "1") == 0 ){
        $datas['04_01_a_g_01'] = $row['C_1_07_1'];    //4：1-ア-g：実施予定有
        $datas['04_01_a_g_03'] = $row['C_1_07_2'];    //4：1-ア-g：実施予定有：内容
    }
    if( strcmp(  strval($row['C_1_07_1']) , "2") == 0 ){
        $datas['04_01_a_g_02'] = $row['C_1_07_1'];    //4：1-ア-g：実施予定無
        $datas['04_01_a_g_04'] = $row['C_1_07_2'];    //4：1-ア-g：実施予定無：内容
    }
    if( strcmp(  strval($row['C_1_07_3']) , "1") == 0 ){
        $datas['04_01_a_g_05_yes'] = $row['C_1_07_3'];    //4：1-ア-g：委託有
    }
    if( strcmp(  strval($row['C_1_07_3']) , "2") == 0 ){
        $datas['04_01_a_g_05_no'] = $row['C_1_07_3'];    //4：1-ア-g：委託無
    }
    if( strcmp(  strval($row['C_1_07_1']) , "2") <> 0 ){
        $datas['04_01_a_g_06'] = convSuppoterName($db, $row["C_1_07_4"], $companyDataCache);    //4：1-ア-g：氏名（役職）
    }
    if( strcmp(  strval($row['C_1_07_3']) , "1") == 0 ){
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_01_a_g_08'] = $postalAry[0];         //4：1-ア-g：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_01_a_g_07'] = $postalAry[0];         //4：1-ア-g：住所：郵便番号3桁
            $datas['04_01_a_g_08'] = $postalAry[1];         //4：1-ア-g：住所：郵便番号4桁
        }
        $datas['04_01_a_g_09'] = $row['support_address'];    //4：1-ア-g：住所：住所
    }
    if ($row['C_1_07_5_1'] == "1") $datas['04_01_a_g_10'] = true;    //4：1-ア-g：実施方法：対面
    if ($row['C_1_07_5_2'] == "1") $datas['04_01_a_g_11'] = true;    //4：1-ア-g：実施方法：テレビ電話装置
    if ($row['C_1_07_5_3'] == "1") $datas['04_01_a_g_12'] = true;    //4：1-ア-g：実施方法：その他
    $datas['04_01_a_g_13'] = $row['C_1_07_6'];    //4：1-ア-g：実施方法：その他：内容
    if( strcmp(  strval($row['C_1_08_1']) , "1") == 0 ){
        $datas['04_01_a_h_01'] = $row['C_1_08_1'];    //4：1-ア-h：実施予定有
        $datas['04_01_a_h_03'] = $row['C_1_08_2'];    //4：1-ア-h：実施予定有：内容
    }
    if( strcmp(  strval($row['C_1_08_1']) , "2") == 0 ){
        $datas['04_01_a_h_02'] = $row['C_1_08_1'];    //4：1-ア-h：実施予定無
        $datas['04_01_a_h_04'] = $row['C_1_08_2'];    //4：1-ア-h：実施予定無：内容
    }
    if( strcmp(  strval($row['C_1_08_3']) , "1") == 0 ){
        $datas['04_01_a_h_05_yes'] = $row['C_1_08_3'];    //4：1-ア-h：委託有
    }
    if( strcmp(  strval($row['C_1_08_3']) , "2") == 0 ){
        $datas['04_01_a_h_05_no'] = $row['C_1_08_3'];    //4：1-ア-h：委託無
    }
    if( strcmp(  strval($row['C_1_08_1']) , "2") <> 0 ){
        $datas['04_01_a_h_06'] = convSuppoterName($db, $row["C_1_08_4"], $companyDataCache);    //4：1-ア-h：氏名（役職）
    }
    if( strcmp(  strval($row['C_1_08_3']) , "1") == 0 ){
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_01_a_h_08'] = $postalAry[0];         //4：1-ア-h：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_01_a_h_07'] = $postalAry[0];         //4：1-ア-h：住所：郵便番号3桁
            $datas['04_01_a_h_08'] = $postalAry[1];         //4：1-ア-h：住所：郵便番号4桁
        }
        $datas['04_01_a_h_09'] = $row['support_address'];    //4：1-ア-h：住所：住所
    }
    if ($row['C_1_08_5_1'] == "1") $datas['04_01_a_h_10'] = true;    //4：1-ア-h：実施方法：対面
    if ($row['C_1_08_5_2'] == "1") $datas['04_01_a_h_11'] = true;    //4：1-ア-h：実施方法：テレビ電話装置
    if ($row['C_1_08_5_3'] == "1") $datas['04_01_a_h_12'] = true;    //4：1-ア-h：実施方法：その他
    $datas['04_01_a_h_13'] = $row['C_1_08_6'];    //4：1-ア-h：実施方法：その他：内容
    if( strcmp(  strval($row['C_1_09_1']) , "1") == 0 ){
        $datas['04_01_a_i_01'] = $row['C_1_09_1'];    //4：1-ア-i：実施予定有
        $datas['04_01_a_i_03'] = $row['C_1_09_2'];    //4：1-ア-i：実施予定有：内容
    }
    if( strcmp(  strval($row['C_1_09_1']) , "2") == 0 ){
        $datas['04_01_a_i_02'] = $row['C_1_09_1'];    //4：1-ア-i：実施予定無
        $datas['04_01_a_i_04'] = $row['C_1_09_2'];    //4：1-ア-i：実施予定無：内容
    }
    if( strcmp(  strval($row['C_1_09_3']) , "1") == 0 ){
        $datas['04_01_a_i_05_yes'] = $row['C_1_09_3'];    //4：1-ア-i：委託有
    }
    if( strcmp(  strval($row['C_1_09_3']) , "2") == 0 ){
        $datas['04_01_a_i_05_no'] = $row['C_1_09_3'];    //4：1-ア-i：委託無
    }
    if( strcmp(  strval($row['C_1_09_1']) , "2") <> 0 ){
        $datas['04_01_a_i_06'] = convSuppoterName($db, $row["C_1_09_4"], $companyDataCache);    //4：1-ア-i：氏名（役職）
    }
    if( strcmp(  strval($row['C_1_09_3']) , "1") == 0 ){
        //4：1-ア-i：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_01_a_i_08'] = $postalAry[0];         //4：1-ア-i：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_01_a_i_07'] = $postalAry[0];         //4：1-ア-i：住所：郵便番号3桁
            $datas['04_01_a_i_08'] = $postalAry[1];         //4：1-ア-i：住所：郵便番号4桁
        }
        $datas['04_01_a_i_09'] = $row['support_address'];    //4：1-ア-i：住所：住所
    }
    if ($row['C_1_09_5_1'] == "1") $datas['04_01_a_i_10'] = true;    //4：1-ア-i：実施方法：対面
    if ($row['C_1_09_5_2'] == "1") $datas['04_01_a_i_11'] = true;    //4：1-ア-i：実施方法：テレビ電話装置
    if ($row['C_1_09_5_3'] == "1") $datas['04_01_a_i_12'] = true;    //4：1-ア-i：実施方法：その他
    $datas['04_01_a_i_13'] = $row['C_1_09_6'];    //4：1-ア-i：実施方法：その他：内容
    if( strcmp(  strval($row['C_1_10_1']) , "1") == 0 ){
        $datas['04_01_a_j_01'] = $row['C_1_10_1'];    //4：1-ア-j：実施予定有
        $datas['04_01_a_j_03'] = $row['C_1_10_2'];    //4：1-ア-j：実施予定有：内容
    }
    if( strcmp(  strval($row['C_1_10_1']) , "2") == 0 ){
        $datas['04_01_a_j_02'] = $row['C_1_10_1'];    //4：1-ア-j：実施予定無
        $datas['04_01_a_j_04'] = $row['C_1_10_2'];    //4：1-ア-j：実施予定無：内容
    }
    if( strcmp(  strval($row['C_1_10_3']) , "1") == 0 ){
        $datas['04_01_a_j_05_yes'] = $row['C_1_10_3'];    //4：1-ア-j：委託有
    }
    if( strcmp(  strval($row['C_1_10_3']) , "2") == 0 ){
        $datas['04_01_a_j_05_no'] = $row['C_1_10_3'];    //4：1-ア-j：委託無
    }
    if( strcmp(  strval($row['C_1_10_1']) , "2") <> 0 ){
        $datas['04_01_a_j_06'] = convSuppoterName($db, $row["C_1_10_4"], $companyDataCache);    //4：1-ア-j：氏名（役職）
    }
    if( strcmp(  strval($row['C_1_10_3']) , "1") == 0 ){
        //4：1-ア-j：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_01_a_j_08'] = $postalAry[0];         //4：1-ア-j：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_01_a_j_07'] = $postalAry[0];         //4：1-ア-j：住所：郵便番号3桁
            $datas['04_01_a_j_08'] = $postalAry[1];         //4：1-ア-j：住所：郵便番号4桁
        }
        $datas['04_01_a_j_09'] = $row['support_address'];    //4：1-ア-j：住所：住所
    }
    if ($row['C_1_10_5_1'] == "1") $datas['04_01_a_j_10'] = true;    //4：1-ア-j：実施方法：対面
    if ($row['C_1_10_5_2'] == "1") $datas['04_01_a_j_11'] = true;    //4：1-ア-j：実施方法：テレビ電話装置
    if ($row['C_1_10_5_3'] == "1") $datas['04_01_a_j_12'] = true;    //4：1-ア-j：実施方法：その他
    $datas['04_01_a_j_13'] = $row['C_1_10_6'];    //4：1-ア-j：実施方法：その他：内容
    $datas['04_01_a_free_01'] = $row['C_1_11_1'];    //4：1-ア-free：支援内容：自由記入
    if( strcmp(  strval($row['C_1_11_2']) , "1") == 0 ){
        $datas['04_01_a_free_02'] = $row['C_1_11_2'];    //4：1-ア-free：実施予定有
        $datas['04_01_a_free_04'] = $row['C_1_11_3'];    //4：1-ア-free：実施予定有：内容
    }
    if( strcmp(  strval($row['C_1_11_2']) , "2") == 0 ){
        $datas['04_01_a_free_03'] = $row['C_1_11_2'];    //4：1-ア-free：実施予定無
        $datas['04_01_a_free_05'] = $row['C_1_11_3'];    //4：1-ア-free：実施予定無：内容
    }
    if( strcmp(  strval($row['C_1_11_4']) , "1") == 0 ){
        $datas['04_01_a_free_06_yes'] = $row['C_1_11_4'];    //4：1-ア-free：委託有
    }
    if( strcmp(  strval($row['C_1_11_4']) , "2") == 0 ){
        $datas['04_01_a_free_06_no'] = $row['C_1_11_4'];    //4：1-ア-free：委託無
    }
    if( strcmp(  strval($row['C_1_11_2']) , "2") <> 0 ){
        $datas['04_01_a_free_07'] = convSuppoterName($db, $row["C_1_11_5"], $companyDataCache);    //4：1-ア-free：氏名（役職）
    }
    if( strcmp(  strval($row['C_1_11_4']) , "1") == 0 ){
        //4：1-ア-free：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_01_a_free_09'] = $postalAry[0];         //4：1-ア-free：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_01_a_free_08'] = $postalAry[0];         //4：1-ア-free：住所：郵便番号3桁
            $datas['04_01_a_free_09'] = $postalAry[1];         //4：1-ア-free：住所：郵便番号4桁
        }
        $datas['04_01_a_free_10'] = $row['support_address'];    //4：1-ア-free：住所：住所
    }
    if ($row['C_1_11_6_1'] == "1") $datas['04_01_a_free_11'] = true;    //4：1-ア-free：実施方法：対面
    if ($row['C_1_11_6_2'] == "1") $datas['04_01_a_free_12'] = true;    //4：1-ア-free：実施方法：テレビ電話装置
    if ($row['C_1_11_6_3'] == "1") $datas['04_01_a_free_13'] = true;    //4：1-ア-free：実施方法：その他
    $datas['04_01_a_free_14'] = $row['C_1_11_7'];    //4：1-ア-free：実施方法：その他：内容
    $datas['04_01_i'] = $row['C_1_12'];    //4：1-イ：実施言語
    $datas['04_01_u'] = $row['C_1_13'];    //4：1-ウ：実施予定時間
    if( strcmp(  strval($row['C_2_1_1']) , "1") == 0 ){
        $datas['04_02_a_01'] = $row['C_2_1_1'];    //4：2-a：実施予定有
        $datas['04_02_a_03'] = $row['C_2_1_2'];    //4：2-a：実施予定有：内容
    }
    if( strcmp(  strval($row['C_2_1_1']) , "2") == 0 ){
        $datas['04_02_a_02'] = $row['C_2_1_1'];    //4：2-a：実施予定無
        $datas['04_02_a_04'] = $row['C_2_1_2'];    //4：2-a：実施予定無：内容
    }
    if( strcmp(  strval($row['C_2_1_3']) , "1") == 0 ){
        $datas['04_02_a_05_yes'] = $row['C_2_1_3'];    //4：2-a：委託有
    }
    if( strcmp(  strval($row['C_2_1_3']) , "2") == 0 ){
        $datas['04_02_a_05_no'] = $row['C_2_1_3'];    //4：2-a：委託無
    }
    if( strcmp(  strval($row['C_2_1_1']) , "2") <> 0 ){
        $datas['04_02_a_06'] = convSuppoterName($db, $row["C_2_1_4"], $companyDataCache);    //4：2-a：氏名（役職）
    }
    if( strcmp(  strval($row['C_2_1_3']) , "1") == 0 ){
        //4：2-a：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_02_a_08'] = $postalAry[0];         //4：2-a：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_02_a_07'] = $postalAry[0];         //4：2-a：住所：郵便番号3桁
            $datas['04_02_a_08'] = $postalAry[1];         //4：2-a：住所：郵便番号4桁
        }
        $datas['04_02_a_09'] = $row['support_address'];    //4：2-a：住所：住所
    }
    if ($row['C_2_1_5_1'] == "1") $datas['04_02_a_10'] = true;    //4：2-a：実施方法：出迎え空港等
    $datas['04_02_a_11'] = $row['C_2_1_6'];    //4：2-a：実施方法：出迎え空港等：空港名
    if ($row['C_2_1_5_2'] == "1") $datas['04_02_a_12'] = true;    //4：2-a：実施方法：送迎方法 
    $datas['04_02_a_13'] = $row['C_2_1_7'];    //4：2-a：実施方法：送迎方法：内容
    if( strcmp(  strval($row['C_2_2_1']) , "1") == 0 ){
        $datas['04_02_b_01'] = $row['C_2_2_1'];    //4：2-b：実施予定有
    }
    if( strcmp(  strval($row['C_2_2_1']) , "2") == 0 ){
        $datas['04_02_b_02'] = $row['C_2_2_1'];    //4：2-b：実施予定無
    }
    $datas['04_02_b_03'] = $row['C_2_2_2'];    //4：2-b：実施予定無：内容
    if( strcmp(  strval($row['C_2_2_3']) , "1") == 0 ){
        $datas['04_02_b_04_yes'] = $row['C_2_2_3'];    //4：2-b：委託有
    }
    if( strcmp(  strval($row['C_2_2_3']) , "2") == 0 ){
        $datas['04_02_b_04_no'] = $row['C_2_2_3'];    //4：2-b：委託無
    }
    if( strcmp(  strval($row['C_2_2_1']) , "2") <> 0 ){
        $datas['04_02_b_05'] = convSuppoterName($db, $row["C_2_2_4"], $companyDataCache);    //4：2-b：氏名（役職）
    }
    if( strcmp(  strval($row['C_2_2_3']) , "1") == 0 ){
        //4：2-b：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_02_b_07'] = $postalAry[0];         //4：2-b：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_02_b_06'] = $postalAry[0];         //4：2-b：住所：郵便番号3桁
            $datas['04_02_b_07'] = $postalAry[1];         //4：2-b：住所：郵便番号4桁
        }
        $datas['04_02_b_08'] = $row['support_address'];    //4：2-b：住所：住所
    }
    if( strcmp(  strval($row['C_2_2_5_2']) , "1") == 0 ){
        $datas['04_02_b_09'] = $row['C_2_2_5_2'];    //4：2-b：実施方法：出国予定空港等
        $datas['04_02_b_10'] = $row['C_2_2_6'];    //4：2-b：実施方法：出国予定空港等：空港名
    }
    if( strcmp(  strval($row['C_2_2_5_2']) , "2") == 0 ){
        $datas['04_02_b_09'] = $row['C_2_2_5_2'];    //4：2-b：実施方法：出国予定空港等
        $datas['04_02_b_11'] = $row['C_2_2_5_2'];    //4：2-b：実施方法：出国予定空港等：未定
    }
    if ($row['C_2_2_7'] == "1") $datas['04_02_b_12'] = true;    //4：2-b：実施方法：送迎方法
    $datas['04_02_b_13'] = $row['C_2_2_8'];    //4：2-b：実施方法：送迎方法：内容
    $datas['04_02_free_01'] = $row['C_2_3_1'];    //4：2-free：支援内容：自由記入
    if( strcmp(  strval($row['C_2_3_2']) , "1") == 0 ){
        $datas['04_02_free_02'] = $row['C_2_3_2'];    //4：2-free：実施予定有
        $datas['04_02_free_04'] = $row['C_2_3_3'];    //4：2-free：実施予定有：内容
    }
    if( strcmp(  strval($row['C_2_3_2']) , "2") == 0 ){
        $datas['04_02_free_03'] = $row['C_2_3_2'];    //4：2-free：実施予定無
        $datas['04_02_free_05'] = $row['C_2_3_3'];    //4：2-free：実施予定無：内容
    }
    if( strcmp(  strval($row['C_2_3_4']) , "1") == 0 ){
        $datas['04_02_free_06_yes'] = $row['C_2_3_4'];    //4：2-free：委託有
    }
    if( strcmp(  strval($row['C_2_3_4']) , "2") == 0 ){
        $datas['04_02_free_06_no'] = $row['C_2_3_4'];    //4：2-free：委託無
    }
    if( strcmp(  strval($row['C_2_3_2']) , "2") <> 0 ){
        $datas['04_02_free_07'] = convSuppoterName($db, $row["C_2_3_5"], $companyDataCache);    //4：2-free：氏名（役職）
    }
    if( strcmp(  strval($row['C_2_3_4']) , "1") == 0 ){
        //4：2-free：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_02_free_09'] = $postalAry[0];         //4：2-free：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_02_free_08'] = $postalAry[0];         //4：2-free：住所：郵便番号3桁
            $datas['04_02_free_09'] = $postalAry[1];         //4：2-free：住所：郵便番号4桁
        }
        $datas['04_02_free_10'] = $row['support_address'];    //4：2-free：住所：住所
    }
    $datas['04_02_free_11'] = $row['C_2_3_6'];    //4：2-free：実施方法：内容
    if( strcmp(  strval($row['C_3_1_1']) , "1") == 0 ){
        $datas['04_03_a_a_01'] = $row['C_3_1_1'];    //4：3-ア-a：実施予定有
        $datas['04_03_a_a_03'] = $row['C_3_1_2'];    //4：3-ア-a：実施予定有：内容
    }
    if( strcmp(  strval($row['C_3_1_1']) , "2") == 0 ){
        $datas['04_03_a_a_02'] = $row['C_3_1_1'];    //4：3-ア-a：実施予定無
        $datas['04_03_a_a_04'] = $row['C_3_1_2'];    //4：3-ア-a：実施予定無：内容
    }
    if( strcmp(  strval($row['C_3_1_3']) , "1") == 0 ){
        $datas['04_03_a_a_05_yes'] = $row['C_3_1_3'];    //4：3-ア-a：委託有
    }
    if( strcmp(  strval($row['C_3_1_3']) , "2") == 0 ){
        $datas['04_03_a_a_05_no'] = $row['C_3_1_3'];    //4：3-ア-a：委託無
    }
    if( strcmp(  strval($row['C_3_1_1']) , "2") <> 0 ){
        $datas['04_03_a_a_06'] = convSuppoterName($db, $row["C_3_1_4"], $companyDataCache);    //4：3-ア-a：氏名（役職）
    }
    if( strcmp(  strval($row['C_3_1_3']) , "1") == 0 ){
        //4：3-ア-a：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_03_a_a_08'] = $postalAry[0];         //4：3-ア-a：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_03_a_a_07'] = $postalAry[0];         //4：3-ア-a：住所：郵便番号3桁
            $datas['04_03_a_a_08'] = $postalAry[1];         //4：3-ア-a：住所：郵便番号4桁
        }
        $datas['04_03_a_a_09'] = $row['support_address'];    //4：3-ア-a：住所：住所
    }
    if( strcmp(  strval($row['C_3_2_1']) , "1") == 0 ){
        $datas['04_03_a_b_01'] = $row['C_3_2_1'];    //4：3-ア-b：実施予定有
        $datas['04_03_a_b_03'] = $row['C_3_2_2'];    //4：3-ア-b：実施予定有：内容
    }
    if( strcmp(  strval($row['C_3_2_1']) , "2") == 0 ){
        $datas['04_03_a_b_02'] = $row['C_3_2_1'];    //4：3-ア-b：実施予定無
        $datas['04_03_a_b_04'] = $row['C_3_2_2'];    //4：3-ア-b：実施予定無：内容
    }
    if( strcmp(  strval($row['C_3_2_3']) , "1") == 0 ){
        $datas['04_03_a_b_05_yes'] = $row['C_3_2_3'];    //4：3-ア-b：委託有
    }
    if( strcmp(  strval($row['C_3_2_3']) , "2") == 0 ){
        $datas['04_03_a_b_05_no'] = $row['C_3_2_3'];    //4：3-ア-b：委託無
    }
    if( strcmp(  strval($row['C_3_2_1']) , "2") <> 0 ){
        $datas['04_03_a_b_06'] = convSuppoterName($db, $row["C_3_2_4"], $companyDataCache);    //4：3-ア-b：氏名（役職）
    }
    if( strcmp(  strval($row['C_3_2_3']) , "1") == 0 ){
        //4：3-ア-b：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_03_a_b_08'] = $postalAry[0];         //4：3-ア-b：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_03_a_b_07'] = $postalAry[0];         //4：3-ア-b：住所：郵便番号3桁
            $datas['04_03_a_b_08'] = $postalAry[1];         //4：3-ア-b：住所：郵便番号4桁
        }
        $datas['04_03_a_b_09'] = $row['support_address'];    //4：3-ア-b：住所：住所
    }
    if( strcmp(  strval($row['C_3_3_1']) , "1") == 0 ){
        $datas['04_03_a_c_01'] = $row['C_3_3_1'];    //4：3-ア-c：実施予定有
        $datas['04_03_a_c_03'] = $row['C_3_3_2'];    //4：3-ア-c：実施予定有：内容
    }
    if( strcmp(  strval($row['C_3_3_1']) , "2") == 0 ){
        $datas['04_03_a_c_02'] = $row['C_3_3_1'];    //4：3-ア-c：実施予定無
        $datas['04_03_a_c_04'] = $row['C_3_3_2'];    //4：3-ア-c：実施予定無：内容
    }
    if( strcmp(  strval($row['C_3_3_3']) , "1") == 0 ){
        $datas['04_03_a_c_05_yes'] = $row['C_3_3_3'];    //4：3-ア-c：委託有
    }
    if( strcmp(  strval($row['C_3_3_3']) , "2") == 0 ){
        $datas['04_03_a_c_05_no'] = $row['C_3_3_3'];    //4：3-ア-c：委託無
    }
    if( strcmp(  strval($row['C_3_3_1']) , "2") <> 0 ){
        $datas['04_03_a_c_06'] = convSuppoterName($db, $row["C_3_3_4"], $companyDataCache);    //4：3-ア-c：氏名（役職）
    }
    if( strcmp(  strval($row['C_3_3_3']) , "1") == 0 ){
        //4：3-ア-c：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_03_a_c_08'] = $postalAry[0];         //4：3-ア-c：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_03_a_c_07'] = $postalAry[0];         //4：3-ア-c：住所：郵便番号3桁
            $datas['04_03_a_c_08'] = $postalAry[1];         //4：3-ア-c：住所：郵便番号4桁
        }
        $datas['04_03_a_c_09'] = $row['support_address'];    //4：3-ア-c：住所：住所
    }
    $datas['04_03_a_free_01'] = $row['C_3_4_1'];    //4：3-ア-free：支援内容：自由記入
    if( strcmp(  strval($row['C_3_4_2']) , "1") == 0 ){
        $datas['04_03_a_free_02'] = $row['C_3_4_2'];    //4：3-ア-free：実施予定有
        $datas['04_03_a_free_04'] = $row['C_3_4_3'];    //4：3-ア-free：実施予定有：内容
    }
    if( strcmp(  strval($row['C_3_4_2']) , "2") == 0 ){
        $datas['04_03_a_free_03'] = $row['C_3_4_2'];    //4：3-ア-free：実施予定無
        $datas['04_03_a_free_05'] = $row['C_3_4_3'];    //4：3-ア-free：実施予定無：内容
    }
    if( strcmp(  strval($row['C_3_4_4']) , "1") == 0 ){
        $datas['04_03_a_free_06_yes'] = $row['C_3_4_4'];    //4：3-ア-free：委託有
    }
    if( strcmp(  strval($row['C_3_4_4']) , "2") == 0 ){
        $datas['04_03_a_free_06_no'] = $row['C_3_4_4'];    //4：3-ア-free：委託無
    }
    if( strcmp(  strval($row['C_3_4_2']) , "2") <> 0 ){
        $datas['04_03_a_free_07'] = convSuppoterName($db, $row["C_3_4_5"], $companyDataCache);    //4：3-ア-free：氏名（役職）
    }
    if( strcmp(  strval($row['C_3_4_4']) , "1") == 0 ){
        //4：3-ア-free：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_03_a_free_09'] = $postalAry[0];         //4：3-ア-free：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_03_a_free_08'] = $postalAry[0];         //4：3-ア-free：住所：郵便番号3桁
            $datas['04_03_a_free_09'] = $postalAry[1];         //4：3-ア-free：住所：郵便番号4桁
        }
        $datas['04_03_a_free_10'] = $row['support_address'];    //4：3-ア-free：住所：住所
    }
    $datas['04_03_a_free_11'] = $row['C_3_4_6'];    //4：3-ア-free：実施方法：内容
    if( strcmp(  strval($row['C_3_5_01']) , "1") == 0 ){
        $datas['04_03_a_d_01'] = $row['C_3_5_01'];    //4：3-ア-d：実施予定：申請の時点で確保
    }
    if( strcmp(  strval($row['C_3_5_01']) , "2") == 0 ){
        $datas['04_03_a_d_02'] = $row['C_3_5_01'];    //4：3-ア-d：実施予定：申請の後で確保
    }
    $datas['04_03_a_d_03'] = $row['C_3_5_04'];    //4：3-ア-d：実施予定：同居人数計
    if( strcmp(  strval($row['C_3_5_02']) , "1") == 0 ){
        $datas['04_03_a_d_04'] = $row['C_3_5_02'];    //4：3-ア-d：実施予定：7.5㎡以上
    }
    if( strcmp(  strval($row['C_3_5_02']) , "2") == 0 ){
        $datas['04_03_a_d_05'] = $row['C_3_5_02'];    //4：3-ア-d：実施予定：7.5㎡未満
    }
    if( strcmp(  strval($row['C_3_5_03']) , "1") == 0 ){
        $datas['04_03_a_d_06'] = $row['C_3_5_03'];    //4：3-ア-d：実施予定：4.5㎡以上
    }
    if( strcmp(  strval($row['C_3_5_03']) , "2") == 0 ){
        $datas['04_03_a_d_07'] = $row['C_3_5_03'];    //4：3-ア-d：実施予定：4.5㎡未満
    }
    if( strcmp(  strval($row['C_3_5_05']) , "1") == 0 ){
        $datas['04_03_i_a_01'] = $row['C_3_5_05'];    //4：3-イ-a：実施予定有
        $datas['04_03_i_a_03'] = $row['C_3_5_06'];    //4：3-イ-a：実施予定有：内容
    }
    if( strcmp(  strval($row['C_3_5_05']) , "2") == 0 ){
        $datas['04_03_i_a_02'] = $row['C_3_5_05'];    //4：3-イ-a：実施予定無
        $datas['04_03_i_a_04'] = $row['C_3_5_06'];    //4：3-イ-a：実施予定無：内容
    }
    if( strcmp(  strval($row['C_3_5_07']) , "1") == 0 ){
        $datas['04_03_i_a_05_yes'] = $row['C_3_5_07'];    //4：3-イ-a：委託有
    }
    if( strcmp(  strval($row['C_3_5_07']) , "2") == 0 ){
        $datas['04_03_i_a_05_no'] = $row['C_3_5_07'];    //4：3-イ-a：委託無
    }
    if( strcmp(  strval($row['C_3_5_05']) , "2") <> 0 ){
        $datas['04_03_i_a_06'] = convSuppoterName($db, $row["C_3_5_08"], $companyDataCache);    //4：3-イ-a：氏名（役職）
    }
    if( strcmp(  strval($row['C_3_5_07']) , "1") == 0 ){
        //4：3-イ-a：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_03_i_a_08'] = $postalAry[0];         //4：3-イ-a：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_03_i_a_07'] = $postalAry[0];         //4：3-イ-a：住所：郵便番号3桁
            $datas['04_03_i_a_08'] = $postalAry[1];         //4：3-イ-a：住所：郵便番号4桁
        }
        $datas['04_03_i_a_09'] = $row['support_address'];    //4：3-イ-a：住所：住所
    }
    if ($row['C_3_5_09_1'] == "1") $datas['04_03_i_a_10'] = true;    //4：3-イ-a：実施方法：手続きに係る情報提供
    if ($row['C_3_5_09_2'] == "1") $datas['04_03_i_a_11'] = true;    //4：3-イ-a：実施方法：必要に応じて手続きに同行
    if ($row['C_3_5_09_3'] == "1") $datas['04_03_i_a_12'] = true;    //4：3-イ-a：実施方法：その他
    $datas['04_03_i_a_13'] = $row['C_3_5_10'];    //4：3-イ-a：実施方法：その他：内容
    if( strcmp(  strval($row['C_3_6_1']) , "1") == 0 ){
        $datas['04_03_i_b_01'] = $row['C_3_6_1'];    //4：3-イ-b：実施予定有
        $datas['04_03_i_b_03'] = $row['C_3_6_2'];    //4：3-イ-b：実施予定有：内容
    }
    if( strcmp(  strval($row['C_3_6_1']) , "2") == 0 ){
        $datas['04_03_i_b_02'] = $row['C_3_6_1'];    //4：3-イ-b：実施予定無
        $datas['04_03_i_b_04'] = $row['C_3_6_2'];    //4：3-イ-b：実施予定無：内容
    }
    if( strcmp(  strval($row['C_3_6_3']) , "1") == 0 ){
        $datas['04_03_i_b_05_yes'] = $row['C_3_6_3'];    //4：3-イ-b：委託有
    }
    if( strcmp(  strval($row['C_3_6_3']) , "2") == 0 ){
        $datas['04_03_i_b_05_no'] = $row['C_3_6_3'];    //4：3-イ-b：委託無
    }
    if( strcmp(  strval($row['C_3_6_1']) , "2") <> 0 ){
        $datas['04_03_i_b_06'] = convSuppoterName($db, $row["C_3_6_4"], $companyDataCache);    //4：3-イ-b：氏名（役職）
    }
    if( strcmp(  strval($row['C_3_6_3']) , "1") == 0 ){
        //4：3-イ-b：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_03_i_b_08'] = $postalAry[0];         //4：3-イ-b：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_03_i_b_07'] = $postalAry[0];         //4：3-イ-b：住所：郵便番号3桁
            $datas['04_03_i_b_08'] = $postalAry[1];         //4：3-イ-b：住所：郵便番号4桁
        }
        $datas['04_03_i_b_09'] = $row['support_address'];    //4：3-イ-b：住所：住所
    }
    if ($row['C_3_6_5_1'] == "1") $datas['04_03_i_b_10'] = true;    //4：3-イ-b：実施方法：手続きに係る情報提供
    if ($row['C_3_6_5_2'] == "1") $datas['04_03_i_b_11'] = true;    //4：3-イ-b：実施方法：必要に応じて手続きに同行
    if ($row['C_3_6_5_3'] == "1") $datas['04_03_i_b_12'] = true;    //4：3-イ-b：実施方法：その他
    $datas['04_03_i_b_13'] = $row['C_3_6_6'];    //4：3-イ-b：実施方法：その他：内容
    if( strcmp(  strval($row['C_3_7_1']) , "1") == 0 ){
        $datas['04_03_i_c_01'] = $row['C_3_7_1'];    //4：3-イ-c：実施予定有
        $datas['04_03_i_c_03'] = $row['C_3_7_2'];    //4：3-イ-c：実施予定有：内容
    }
    if( strcmp(  strval($row['C_3_7_1']) , "2") == 0 ){
        $datas['04_03_i_c_02'] = $row['C_3_7_1'];    //4：3-イ-c：実施予定無
        $datas['04_03_i_c_04'] = $row['C_3_7_2'];    //4：3-イ-c：実施予定無：内容
    }
    if( strcmp(  strval($row['C_3_7_3']) , "1") == 0 ){
        $datas['04_03_i_c_05_yes'] = $row['C_3_7_3'];    //4：3-イ-c：委託有
    }
    if( strcmp(  strval($row['C_3_7_3']) , "2") == 0 ){
        $datas['04_03_i_c_05_no'] = $row['C_3_7_3'];    //4：3-イ-c：委託無
    }
    if( strcmp(  strval($row['C_3_7_1']) , "2") <> 0 ){
        $datas['04_03_i_c_06'] = convSuppoterName($db, $row["C_3_7_4"], $companyDataCache);    //4：3-イ-c：氏名（役職）
    }
    if( strcmp(  strval($row['C_3_7_3']) , "1") == 0 ){
        //4：3-イ-c：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_03_i_c_08'] = $postalAry[0];         //4：3-イ-c：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_03_i_c_07'] = $postalAry[0];         //4：3-イ-c：住所：郵便番号3桁
            $datas['04_03_i_c_08'] = $postalAry[1];         //4：3-イ-c：住所：郵便番号4桁
        }
        $datas['04_03_i_c_09'] = $row['support_address'];    //4：3-イ-c：住所：住所
    }
    if ($row['C_3_7_5_1'] == "1") $datas['04_03_i_c_10'] = true;    //4：3-イ-c：実施方法：手続きに係る情報提供
    if ($row['C_3_7_5_2'] == "1") $datas['04_03_i_c_11'] = true;    //4：3-イ-c：実施方法：必要に応じて手続きに同行
    if ($row['C_3_7_5_3'] == "1") $datas['04_03_i_c_12'] = true;    //4：3-イ-c：実施方法：その他
    $datas['04_03_i_c_13'] = $row['C_3_7_6'];    //4：3-イ-c：実施方法：その他：内容
    $datas['04_03_i_free_01'] = $row['C_3_8_1'];    //4：3-イ-free：支援内容：自由記入
    if( strcmp(  strval($row['C_3_8_2']) , "1") == 0 ){
        $datas['04_03_i_free_02'] = $row['C_3_8_2'];    //4：3-イ-free：実施予定有
        $datas['04_03_i_free_04'] = $row['C_3_8_3'];    //4：3-イ-free：実施予定有：内容
    }
    if( strcmp(  strval($row['C_3_8_2']) , "2") == 0 ){
        $datas['04_03_i_free_03'] = $row['C_3_8_2'];    //4：3-イ-free：実施予定無
        $datas['04_03_i_free_05'] = $row['C_3_8_3'];    //4：3-イ-free：実施予定無：内容
    }
    if( strcmp(  strval($row['C_3_8_4']) , "1") == 0 ){
        $datas['04_03_i_free_06_yes'] = $row['C_3_8_4'];    //4：3-イ-free：委託有
    }
    if( strcmp(  strval($row['C_3_8_4']) , "2") == 0 ){
        $datas['04_03_i_free_06_no'] = $row['C_3_8_4'];    //4：3-イ-free：委託無
    }
    if( strcmp(  strval($row['C_3_8_2']) , "2") <> 0 ){
        $datas['04_03_i_free_07'] = convSuppoterName($db, $row["C_3_8_5"], $companyDataCache);    //4：3-イ-free：氏名（役職）
    }
    if( strcmp(  strval($row['C_3_8_4']) , "1") == 0 ){
        //4：3-イ-free：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_03_i_free_09'] = $postalAry[0];         //4：3-イ-free：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_03_i_free_08'] = $postalAry[0];         //4：3-イ-free：住所：郵便番号3桁
            $datas['04_03_i_free_09'] = $postalAry[1];         //4：3-イ-free：住所：郵便番号4桁
        }
        $datas['04_03_i_free_10'] = $row['support_address'];    //4：3-イ-free：住所：住所
    }
    $datas['04_03_i_free_11'] = $row['C_3_8_6'];    //4：3-イ-free：実施方法：内容
    if( strcmp(  strval($row['C_4_1_1']) , "1") == 0 ){
        $datas['04_04_a_a_01'] = $row['C_4_1_1'];    //4：4-ア-a：実施予定有
        $datas['04_04_a_a_03'] = $row['C_4_1_2'];    //4：4-ア-a：実施予定有：内容
    }
    if( strcmp(  strval($row['C_4_1_1']) , "2") == 0 ){
        $datas['04_04_a_a_02'] = $row['C_4_1_1'];    //4：4-ア-a：実施予定無
        $datas['04_04_a_a_04'] = $row['C_4_1_2'];    //4：4-ア-a：実施予定無：内容
    }
    if( strcmp(  strval($row['C_4_1_3']) , "1") == 0 ){
        $datas['04_04_a_a_05_yes'] = $row['C_4_1_3'];    //4：4-ア-a：委託有
    }
    if( strcmp(  strval($row['C_4_1_3']) , "2") == 0 ){
        $datas['04_04_a_a_05_no'] = $row['C_4_1_3'];    //4：4-ア-a：委託無
    }
    if( strcmp(  strval($row['C_4_1_1']) , "2") <> 0 ){
        $datas['04_04_a_a_06'] = convSuppoterName($db, $row["C_4_1_4"], $companyDataCache);    //4：4-ア-a：氏名（役職）
    }
    if( strcmp(  strval($row['C_4_1_3']) , "1") == 0 ){
        //4：4-ア-a：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_04_a_a_08'] = $postalAry[0];         //4：4-ア-a：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_04_a_a_07'] = $postalAry[0];         //4：4-ア-a：住所：郵便番号3桁
            $datas['04_04_a_a_08'] = $postalAry[1];         //4：4-ア-a：住所：郵便番号4桁
        }
        $datas['04_04_a_a_09'] = $row['support_address'];    //4：4-ア-a：住所：住所
    }
    if ($row['C_4_1_5_1'] == "1") $datas['04_04_a_a_10'] = true;    //4：4-ア-a：実施方法：口頭
    if ($row['C_4_1_5_2'] == "1") $datas['04_04_a_a_11'] = true;    //4：4-ア-a：実施方法：書面（翻訳含む。）
    if ($row['C_4_1_5_3'] == "1") $datas['04_04_a_a_12'] = true;    //4：4-ア-a：実施方法：その他
    $datas['04_04_a_a_13'] = $row['C_4_1_6'];    //4：4-ア-a：実施方法：その他：内容
    if( strcmp(  strval($row['C_4_2_1']) , "1") == 0 ){
        $datas['04_04_a_b_01'] = $row['C_4_2_1'];    //4：4-ア-b：実施予定有
        $datas['04_04_a_b_03'] = $row['C_4_2_2'];    //4：4-ア-b：実施予定有：内容
    }
    if( strcmp(  strval($row['C_4_2_1']) , "2") == 0 ){
        $datas['04_04_a_b_02'] = $row['C_4_2_1'];    //4：4-ア-b：実施予定無
        $datas['04_04_a_b_04'] = $row['C_4_2_2'];    //4：4-ア-b：実施予定無：内容
    }
    if( strcmp(  strval($row['C_4_2_3']) , "1") == 0 ){
        $datas['04_04_a_b_05_yes'] = $row['C_4_2_3'];    //4：4-ア-b：委託有
    }
    if( strcmp(  strval($row['C_4_2_3']) , "2") == 0 ){
        $datas['04_04_a_b_05_no'] = $row['C_4_2_3'];    //4：4-ア-b：委託無
    }
    if( strcmp(  strval($row['C_4_2_1']) , "2") <> 0 ){
        $datas['04_04_a_b_06'] = convSuppoterName($db, $row["C_4_2_4"], $companyDataCache);    //4：4-ア-b：氏名（役職）
    }
    if( strcmp(  strval($row['C_4_2_3']) , "1") == 0 ){
        //4：4-ア-b：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_04_a_b_08'] = $postalAry[0];         //4：4-ア-b：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_04_a_b_07'] = $postalAry[0];         //4：4-ア-b：住所：郵便番号3桁
            $datas['04_04_a_b_08'] = $postalAry[1];         //4：4-ア-b：住所：郵便番号4桁
        }
        $datas['04_04_a_b_09'] = $row['support_address'];    //4：4-ア-b：住所：住所
    }
    if ($row['C_4_2_5_1'] == "1") $datas['04_04_a_b_10'] = true;    //4：4-ア-b：実施方法：口頭
    if ($row['C_4_2_5_2'] == "1") $datas['04_04_a_b_11'] = true;    //4：4-ア-b：実施方法：書面（翻訳含む。）
    if ($row['C_4_2_5_3'] == "1") $datas['04_04_a_b_12'] = true;    //4：4-ア-b：実施方法：必要に応じて手続きに同行
    if ($row['C_4_2_5_4'] == "1") $datas['04_04_a_b_13'] = true;    //4：4-ア-b：実施方法：その他
    $datas['04_04_a_b_14'] = $row['C_4_2_6'];    //4：4-ア-b：実施方法：その他：内容
    if( strcmp(  strval($row['C_4_3_1']) , "1") == 0 ){
        $datas['04_04_a_c_01'] = $row['C_4_3_1'];    //4：4-ア-c：実施予定有
        $datas['04_04_a_c_03'] = $row['C_4_3_2'];    //4：4-ア-c：実施予定有：内容
    }
    if( strcmp(  strval($row['C_4_3_1']) , "2") == 0 ){
        $datas['04_04_a_c_02'] = $row['C_4_3_1'];    //4：4-ア-c：実施予定無
        $datas['04_04_a_c_04'] = $row['C_4_3_2'];    //4：4-ア-c：実施予定無：内容
    }
    if( strcmp(  strval($row['C_4_3_3']) , "1") == 0 ){
        $datas['04_04_a_c_05_yes'] = $row['C_4_3_3'];    //4：4-ア-c：委託有
    }
    if( strcmp(  strval($row['C_4_3_3']) , "2") == 0 ){
        $datas['04_04_a_c_05_no'] = $row['C_4_3_3'];    //4：4-ア-c：委託無
    }
    if( strcmp(  strval($row['C_4_3_1']) , "2") <> 0 ){
        $datas['04_04_a_c_06'] = convSuppoterName($db, $row["C_4_3_4"], $companyDataCache);    //4：4-ア-c：氏名（役職）
    }
    if( strcmp(  strval($row['C_4_3_3']) , "1") == 0 ){
        //4：4-ア-c：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_04_a_c_08'] = $postalAry[0];         //4：4-ア-c：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_04_a_c_07'] = $postalAry[0];         //4：4-ア-c：住所：郵便番号3桁
            $datas['04_04_a_c_08'] = $postalAry[1];         //4：4-ア-c：住所：郵便番号4桁
        }
        $datas['04_04_a_c_09'] = $row['support_address'];    //4：4-ア-c：住所：住所
    }
    if ($row['C_4_3_5_1'] == "1") $datas['04_04_a_c_10'] = true;    //4：4-ア-c：実施方法：口頭
    if ($row['C_4_3_5_2'] == "1") $datas['04_04_a_c_11'] = true;    //4：4-ア-c：実施方法：書面（翻訳含む。）
    if ($row['C_4_3_5_3'] == "1") $datas['04_04_a_c_12'] = true;    //4：4-ア-c：実施方法：その他
    $datas['04_04_a_c_13'] = $row['C_4_3_6'];    //4：4-ア-c：実施方法：その他：内容
    if( strcmp(  strval($row['C_4_4_1']) , "1") == 0 ){
        $datas['04_04_a_d_01'] = $row['C_4_4_1'];    //4：4-ア-d：実施予定有
        $datas['04_04_a_d_03'] = $row['C_4_4_2'];    //4：4-ア-d：実施予定有：内容
    }
    if( strcmp(  strval($row['C_4_4_1']) , "2") == 0 ){
        $datas['04_04_a_d_02'] = $row['C_4_4_1'];    //4：4-ア-d：実施予定無
        $datas['04_04_a_d_04'] = $row['C_4_4_2'];    //4：4-ア-d：実施予定無：内容
    }
    if( strcmp(  strval($row['C_4_4_3']) , "1") == 0 ){
        $datas['04_04_a_d_05_yes'] = $row['C_4_4_3'];    //4：4-ア-d：委託有
    }
    if( strcmp(  strval($row['C_4_4_3']) , "2") == 0 ){
        $datas['04_04_a_d_05_no'] = $row['C_4_4_3'];    //4：4-ア-d：委託無
    }
    if( strcmp(  strval($row['C_4_4_1']) , "2") <> 0 ){
        $datas['04_04_a_d_06'] = convSuppoterName($db, $row["C_4_4_4"], $companyDataCache);    //4：4-ア-d：氏名（役職）
    }
    if( strcmp(  strval($row['C_4_4_3']) , "1") == 0 ){
        //4：4-ア-d：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_04_a_d_08'] = $postalAry[0];         //4：4-ア-d：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_04_a_d_07'] = $postalAry[0];         //4：4-ア-d：住所：郵便番号3桁
            $datas['04_04_a_d_08'] = $postalAry[1];         //4：4-ア-d：住所：郵便番号4桁
        }
        $datas['04_04_a_d_09'] = $row['support_address'];    //4：4-ア-d：住所：住所
    }
    if ($row['C_4_4_5_1'] == "1") $datas['04_04_a_d_10'] = true;    //4：4-ア-d：実施方法：口頭
    if ($row['C_4_4_5_2'] == "1") $datas['04_04_a_d_11'] = true;    //4：4-ア-d：実施方法：書面（翻訳含む。）
    if ($row['C_4_4_5_3'] == "1") $datas['04_04_a_d_12'] = true;    //4：4-ア-d：実施方法：その他
    $datas['04_04_a_d_13'] = $row['C_4_4_6'];    //4：4-ア-d：実施方法：その他：内容
    if( strcmp(  strval($row['C_4_5_1']) , "1") == 0 ){
        $datas['04_04_a_e_01'] = $row['C_4_5_1'];    //4：4-ア-e：実施予定有
        $datas['04_04_a_e_03'] = $row['C_4_5_2'];    //4：4-ア-e：実施予定有：内容
    }
    if( strcmp(  strval($row['C_4_5_1']) , "2") == 0 ){
        $datas['04_04_a_e_02'] = $row['C_4_5_1'];    //4：4-ア-e：実施予定無
        $datas['04_04_a_e_04'] = $row['C_4_5_2'];    //4：4-ア-e：実施予定無：内容
    }
    if( strcmp(  strval($row['C_4_5_3']) , "1") == 0 ){
        $datas['04_04_a_e_05_yes'] = $row['C_4_5_3'];    //4：4-ア-e：委託有
    }
    if( strcmp(  strval($row['C_4_5_3']) , "2") == 0 ){
        $datas['04_04_a_e_05_no'] = $row['C_4_5_3'];    //4：4-ア-e：委託無
    }
    if( strcmp(  strval($row['C_4_5_1']) , "2") <> 0 ){
        $datas['04_04_a_e_06'] = convSuppoterName($db, $row["C_4_5_4"], $companyDataCache);    //4：4-ア-e：氏名（役職）   
    }
    if( strcmp(  strval($row['C_4_5_3']) , "1") == 0 ){
        //4：4-ア-e：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_04_a_e_08'] = $postalAry[0];         //4：4-ア-e：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_04_a_e_07'] = $postalAry[0];         //4：4-ア-e：住所：郵便番号3桁
            $datas['04_04_a_e_08'] = $postalAry[1];         //4：4-ア-e：住所：郵便番号4桁
        }
        $datas['04_04_a_e_09'] = $row['support_address'];    //4：4-ア-e：住所：住所
    }
    if ($row['C_4_5_5_1'] == "1") $datas['04_04_a_e_10'] = true;    //4：4-ア-e：実施方法：口頭
    if ($row['C_4_5_5_2'] == "1") $datas['04_04_a_e_11'] = true;    //4：4-ア-e：実施方法：書面（翻訳含む。）
    if ($row['C_4_5_5_3'] == "1") $datas['04_04_a_e_12'] = true;    //4：4-ア-e：実施方法：その他
    $datas['04_04_a_e_13'] = $row['C_4_5_6'];    //4：4-ア-e：実施方法：その他：内容
    if( strcmp(  strval($row['C_4_6_1']) , "1") == 0 ){
        $datas['04_04_a_f_01'] = $row['C_4_6_1'];    //4：4-ア-f：実施予定有
        $datas['04_04_a_f_03'] = $row['C_4_6_2'];    //4：4-ア-f：実施予定有：内容
    }
    if( strcmp(  strval($row['C_4_6_1']) , "2") == 0 ){
        $datas['04_04_a_f_02'] = $row['C_4_6_1'];    //4：4-ア-f：実施予定無
        $datas['04_04_a_f_04'] = $row['C_4_6_2'];    //4：4-ア-f：実施予定無：内容
    }
    if( strcmp(  strval($row['C_4_6_3']) , "1") == 0 ){
        $datas['04_04_a_f_05_yes'] = $row['C_4_6_3'];    //4：4-ア-f：委託有
    }
    if( strcmp(  strval($row['C_4_6_3']) , "2") == 0 ){
        $datas['04_04_a_f_05_no'] = $row['C_4_6_3'];    //4：4-ア-f：委託無
    }
    if( strcmp(  strval($row['C_4_6_1']) , "2") <> 0 ){
        $datas['04_04_a_f_06'] = convSuppoterName($db, $row["C_4_6_4"], $companyDataCache);    //4：4-ア-f：氏名（役職）   
    }
    if( strcmp(  strval($row['C_4_6_3']) , "1") == 0 ){
        //4：4-ア-f：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_04_a_f_08'] = $postalAry[0];         //4：4-ア-f：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_04_a_f_07'] = $postalAry[0];         //4：4-ア-f：住所：郵便番号3桁
            $datas['04_04_a_f_08'] = $postalAry[1];         //4：4-ア-f：住所：郵便番号4桁
        }
        $datas['04_04_a_f_09'] = $row['support_address'];    //4：4-ア-f：住所：住所
    }
    if ($row['C_4_6_5_1'] == "1") $datas['04_04_a_f_10'] = true;    //4：4-ア-f：実施方法：口頭
    if ($row['C_4_6_5_2'] == "1") $datas['04_04_a_f_11'] = true;    //4：4-ア-f：実施方法：書面（翻訳含む。）
    if ($row['C_4_6_5_3'] == "1") $datas['04_04_a_f_12'] = true;    //4：4-ア-f：実施方法：その他
    $datas['04_04_a_f_13'] = $row['C_4_6_6'];    //4：4-ア-f：実施方法：その他：内容
    $datas['04_04_a_free_01'] = $row['C_4_7_1'];    //4：4-ア-free：自由記入
    if( strcmp(  strval($row['C_4_7_2']) , "1") == 0 ){
        $datas['04_04_a_free_02'] = $row['C_4_7_2'];    //4：4-ア-free：実施予定有
        $datas['04_04_a_free_04'] = $row['C_4_7_3'];    //4：4-ア-free：実施予定有：内容
    }
    if( strcmp(  strval($row['C_4_7_2']) , "2") == 0 ){
        $datas['04_04_a_free_03'] = $row['C_4_7_2'];    //4：4-ア-free：実施予定無
        $datas['04_04_a_free_05'] = $row['C_4_7_3'];    //4：4-ア-free：実施予定無：内容

    }    
    if( strcmp(  strval($row['C_4_7_4']) , "1") == 0 ){
        $datas['04_04_a_free_06_yes'] = $row['C_4_7_4'];    //4：4-ア-free：委託有
    }
    if( strcmp(  strval($row['C_4_7_4']) , "2") == 0 ){
        $datas['04_04_a_free_06_no'] = $row['C_4_7_4'];    //4：4-ア-free：委託無
    }
    if( strcmp(  strval($row['C_4_7_2']) , "2") <> 0 ){
        $datas['04_04_a_free_07'] = convSuppoterName($db, $row["C_4_7_5"], $companyDataCache);    //4：4-ア-free：氏名（役職）   
    }
    if( strcmp(  strval($row['C_4_7_4']) , "1") == 0 ){
        //4：4-ア-free：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_04_a_free_09'] = $postalAry[0];         //4：4-ア-free：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_04_a_free_08'] = $postalAry[0];         //4：4-ア-free：住所：郵便番号3桁
            $datas['04_04_a_free_09'] = $postalAry[1];         //4：4-ア-free：住所：郵便番号4桁
        }
        $datas['04_04_a_free_10'] = $row['support_address'];    //4：4-ア-free：住所：住所
    }
    if ($row['C_4_7_6_1'] == "1") $datas['04_04_a_free_11'] = true;    //4：4-ア-free：実施方法：口頭
    if ($row['C_4_7_6_2'] == "1") $datas['04_04_a_free_12'] = true;    //4：4-ア-free：実施方法：書面（翻訳含む。）
    if ($row['C_4_7_6_3'] == "1") $datas['04_04_a_free_13'] = true;    //4：4-ア-free：実施方法：その他
    $datas['04_04_a_free_14'] = $row['C_4_7_7'];    //4：4-ア-free：実施方法：その他：内容
    $datas['04_04_i'] = $row['C_4_8'];    //4：4-イ：実施言語
    $datas['04_04_u'] = $row['C_4_9'];    //4：4-ウ：実施予定時間合計
    if( strcmp(  strval($row['C_5_1_1']) , "1") == 0 ){
        $datas['04_05_a_01'] = $row['C_5_1_1'];    //4：5-a：実施予定有
        $datas['04_05_a_03'] = $row['C_5_1_2'];    //4：5-a：実施予定有：内容
    }
    if( strcmp(  strval($row['C_5_1_1']) , "2") == 0 ){
        $datas['04_05_a_02'] = $row['C_5_1_1'];    //4：5-a：実施予定無
        $datas['04_05_a_04'] = $row['C_5_1_2'];    //4：5-a：実施予定無：内容
    }
    if( strcmp(  strval($row['C_5_1_3']) , "1") == 0 ){
        $datas['04_05_a_05_yes'] = $row['C_5_1_3'];    //4：5-a：委託有
    }
    if( strcmp(  strval($row['C_5_1_3']) , "2") == 0 ){
        $datas['04_05_a_05_no'] = $row['C_5_1_3'];    //4：5-a：委託無
    }
    if( strcmp(  strval($row['C_5_1_1']) , "2") <> 0 ){
        $datas['04_05_a_06'] = convSuppoterName($db, $row["C_5_1_4"], $companyDataCache);    //4：5-a：氏名（役職）   
    }
    if( strcmp(  strval($row['C_5_1_3']) , "1") == 0 ){
        //4：5-a：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_05_a_08'] = $postalAry[0];         //4：5-a：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_05_a_07'] = $postalAry[0];         //4：5-a：住所：郵便番号3桁
            $datas['04_05_a_08'] = $postalAry[1];         //4：5-a：住所：郵便番号4桁
        }
        $datas['04_05_a_09'] = $row['support_address'];    //4：5-a：住所：住所
    }
    if( strcmp(  strval($row['C_5_2_1']) , "1") == 0 ){
        $datas['04_05_b_01'] = $row['C_5_2_1'];    //4：5-b：実施予定有
        $datas['04_05_b_03'] = $row['C_5_2_2'];    //4：5-b：実施予定有：内容
    }
    if( strcmp(  strval($row['C_5_2_1']) , "2") == 0 ){
        $datas['04_05_b_02'] = $row['C_5_2_1'];    //4：5-b：実施予定無
        $datas['04_05_b_04'] = $row['C_5_2_2'];    //4：5-b：実施予定無：内容
    }
    if( strcmp(  strval($row['C_5_2_3']) , "1") == 0 ){
        $datas['04_05_b_05_yes'] = $row['C_5_2_3'];    //4：5-b：委託有
    }
    if( strcmp(  strval($row['C_5_2_3']) , "2") == 0 ){
        $datas['04_05_b_05_no'] = $row['C_5_2_3'];    //4：5-b：委託無
    }
    if( strcmp(  strval($row['C_5_2_1']) , "2") <> 0 ){
        $datas['04_05_b_06'] = convSuppoterName($db, $row["C_5_2_4"], $companyDataCache);    //4：5-b：氏名（役職）   
    }
    if( strcmp(  strval($row['C_5_2_3']) , "1") == 0 ){
        //4：5-b：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_05_b_08'] = $postalAry[0];         //4：5-b：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_05_b_07'] = $postalAry[0];         //4：5-b：住所：郵便番号3桁
            $datas['04_05_b_08'] = $postalAry[1];         //4：5-b：住所：郵便番号4桁
        }
        $datas['04_05_b_09'] = $row['support_address'];    //4：5-b：住所：住所
    }
    if( strcmp(  strval($row['C_5_3_1']) , "1") == 0 ){
        $datas['04_05_c_01'] = $row['C_5_3_1'];    //4：5-c：実施予定有
        $datas['04_05_c_03'] = $row['C_5_3_2'];    //4：5-c：実施予定有：内容
    }
    if( strcmp(  strval($row['C_5_3_1']) , "2") == 0 ){
        $datas['04_05_c_02'] = $row['C_5_3_1'];    //4：5-c：実施予定無
        $datas['04_05_c_04'] = $row['C_5_3_2'];    //4：5-c：実施予定無：内容
    }
    if( strcmp(  strval($row['C_5_3_3']) , "1") == 0 ){
        $datas['04_05_c_05_yes'] = $row['C_5_3_3'];    //4：5-c：委託有
    }
    if( strcmp(  strval($row['C_5_3_3']) , "2") == 0 ){
        $datas['04_05_c_05_no'] = $row['C_5_3_3'];    //4：5-c：委託無
    }
    if( strcmp(  strval($row['C_5_3_1']) , "2") <> 0 ){
        $datas['04_05_c_06'] = convSuppoterName($db, $row["C_5_3_4"], $companyDataCache);    //4：5-c：氏名（役職）   
    }
    if( strcmp(  strval($row['C_5_3_3']) , "1") == 0 ){
        //4：5-c：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_05_c_08'] = $postalAry[0];         //4：5-c：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_05_c_07'] = $postalAry[0];         //4：5-c：住所：郵便番号3桁
            $datas['04_05_c_08'] = $postalAry[1];         //4：5-c：住所：郵便番号4桁
        }
        $datas['04_05_c_09'] = $row['support_address'];    //4：5-c：住所：住所
    }
    $datas['04_05_free_01'] = $row['C_5_4_1'];    //4：5-free：自由記入
    if( strcmp(  strval($row['C_5_4_2']) , "1") == 0 ){
        $datas['04_05_free_02'] = $row['C_5_4_2'];    //4：5-free：実施予定有
        $datas['04_05_free_04'] = $row['C_5_4_3'];    //4：5-free：実施予定有：内容
    }
    if( strcmp(  strval($row['C_5_4_2']) , "2") == 0 ){
        $datas['04_05_free_03'] = $row['C_5_4_2'];    //4：5-free：実施予定無
        $datas['04_05_free_05'] = $row['C_5_4_3'];    //4：5-free：実施予定無：内容
    }
    if( strcmp(  strval($row['C_5_4_4']) , "1") == 0 ){
        $datas['04_05_free_06_yes'] = $row['C_5_4_4'];    //4：5-free：委託有
    }
    if( strcmp(  strval($row['C_5_4_4']) , "2") == 0 ){
        $datas['04_05_free_06_no'] = $row['C_5_4_4'];    //4：5-free：委託無
    }
    if( strcmp(  strval($row['C_5_4_2']) , "2") <> 0 ){
        $datas['04_05_free_07'] = convSuppoterName($db, $row["C_5_4_5"], $companyDataCache);    //4：5-free：氏名（役職）   
    }
    if( strcmp(  strval($row['C_5_4_4']) , "1") == 0 ){
        //4：5-free：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_05_free_09'] = $postalAry[0];         //4：5-free：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_05_free_08'] = $postalAry[0];         //4：5-free：住所：郵便番号3桁
            $datas['04_05_free_09'] = $postalAry[1];         //4：5-free：住所：郵便番号4桁
        }
        $datas['04_05_free_10'] = $row['support_address'];    //4：5-free：住所：住所
    }
    $datas['04_05_free_11'] = $row['C_5_4_6'];    //4：5-free：実施方法：内容
    if( strcmp(  strval($row['C_6_1_1']) , "1") == 0 ){
        $datas['04_06_a_a_01'] = $row['C_6_1_1'];    //4：6-ア-a：実施予定有
    }
    if( strcmp(  strval($row['C_6_1_1']) , "2") == 0 ){
        $datas['04_06_a_a_02'] = $row['C_6_1_1'];    //4：6-ア-a：実施予定無
    }
    $datas['04_06_a_a_03'] = $row['C_6_1_2'];    //4：6-ア-a：実施予定無：内容
    if( strcmp(  strval($row['C_6_1_3']) , "1") == 0 ){
        $datas['04_06_a_a_04_yes'] = $row['C_6_1_3'];    //4：6-ア-a：委託有
    }
    if( strcmp(  strval($row['C_6_1_3']) , "2") == 0 ){
        $datas['04_06_a_a_04_no'] = $row['C_6_1_3'];    //4：6-ア-a：委託無
    }
    if( strcmp(  strval($row['C_6_1_1']) , "2") <> 0 ){
        $datas['04_06_a_a_05'] = convSuppoterName($db, $row["C_6_1_4"], $companyDataCache);    //4：6-ア-a：氏名（役職）   
    }
    if( strcmp(  strval($row['C_6_1_3']) , "1") == 0 ){
        //4：6-ア-a：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_06_a_a_07'] = $postalAry[0];         //4：6-ア-a：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_06_a_a_06'] = $postalAry[0];         //4：6-ア-a：住所：郵便番号3桁
            $datas['04_06_a_a_07'] = $postalAry[1];         //4：6-ア-a：住所：郵便番号4桁
            $datas['04_06_a_a_08'] = $row['support_address'];    //4：6-ア-a：住所：住所
        }
    }
    if( strcmp(  strval($row['C_6_2_1']) , "1") == 0 ){
        $datas['04_06_a_b_01'] = $row['C_6_2_1'];    //4：6-ア-b：実施予定有
    }
    if( strcmp(  strval($row['C_6_2_1']) , "2") == 0 ){
        $datas['04_06_a_b_02'] = $row['C_6_2_1'];    //4：6-ア-b：実施予定無
    }
    $datas['04_06_a_b_03'] = $row['C_6_2_2'];    //4：6-ア-b：実施予定無：内容
    if( strcmp(  strval($row['C_6_2_3']) , "1") == 0 ){
        $datas['04_06_a_b_04_yes'] = $row['C_6_2_3'];    //4：6-ア-b：委託有
    }
    if( strcmp(  strval($row['C_6_2_3']) , "2") == 0 ){
        $datas['04_06_a_b_04_no'] = $row['C_6_2_3'];    //4：6-ア-b：委託無
    }
    if( strcmp(  strval($row['C_6_2_1']) , "2") <> 0 ){
        $datas['04_06_a_b_05'] = convSuppoterName($db, $row["C_6_2_4"], $companyDataCache);    //4：6-ア-b：氏名（役職）   
    }
    if( strcmp(  strval($row['C_6_2_3']) , "1") == 0 ){
        //4：6-ア-b：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_06_a_b_07'] = $postalAry[0];         //4：6-ア-b：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_06_a_b_06'] = $postalAry[0];         //4：6-ア-b：住所：郵便番号3桁
            $datas['04_06_a_b_07'] = $postalAry[1];         //4：6-ア-b：住所：郵便番号4桁
        }
        $datas['04_06_a_b_08'] = $row['support_address'];    //4：6-ア-b：住所：住所
    }
    $datas['04_06_a_c_01'] = $row['C_6_3_1'];    //4：6-ア-c：自由記入
    if( strcmp(  strval($row['C_6_3_2']) , "1") == 0 ){
        $datas['04_06_a_c_02'] = $row['C_6_3_2'];    //4：6-ア-c：実施予定有
        $datas['04_06_a_c_04'] = $row['C_6_3_3'];    //4：6-ア-c：実施予定有：内容
    }
    if( strcmp(  strval($row['C_6_3_2']) , "2") == 0 ){
        $datas['04_06_a_c_03'] = $row['C_6_3_2'];    //4：6-ア-c：実施予定無
        $datas['04_06_a_c_05'] = $row['C_6_3_3'];    //4：6-ア-c：実施予定無：内容
    }
    if( strcmp(  strval($row['C_6_3_4']) , "1") == 0 ){
        $datas['04_06_a_c_06_yes'] = $row['C_6_3_4'];    //4：6-ア-c：委託有
    }
    if( strcmp(  strval($row['C_6_3_4']) , "2") == 0 ){
        $datas['04_06_a_c_06_no'] = $row['C_6_3_4'];    //4：6-ア-c：委託無
    }
    if( strcmp(  strval($row['C_6_3_2']) , "2") <> 0 ){
        $datas['04_06_a_c_07'] = convSuppoterName($db, $row["C_6_3_5"], $companyDataCache);    //4：6-ア-c：氏名（役職）   
    }
    if( strcmp(  strval($row['C_6_3_4']) , "1") == 0 ){
        //4：6-ア-c：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_06_a_b_07'] = $postalAry[0];         //4：6-ア-c：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_06_a_c_08'] = $postalAry[0];         //4：6-ア-c：住所：郵便番号3桁
            $datas['04_06_a_c_09'] = $postalAry[1];         //4：6-ア-c：住所：郵便番号4桁
        }
        $datas['04_06_a_c_10'] = $row['support_address'];    //4：6-ア-c：住所：住所
    }
    $test = $row['C_6_4_01'];
    $datas['04_06_i_01_01_from'] = setSpace( $row['C_6_4_01'] );    //4：6-イ-1：対応時間：月：時（から）

    $datas['04_06_i_01_01_to'] = setSpace( $row['C_6_4_02'] );    //4：6-イ-1：対応時間：月：時（まで）
    $datas['04_06_i_01_02_from'] = setSpace( $row['C_6_4_03'] );    //4：6-イ-1：対応時間：火：時（から）
    $datas['04_06_i_01_02_to'] = setSpace( $row['C_6_4_04'] );    //4：6-イ-1：対応時間：火：時（まで）
    $datas['04_06_i_01_03_from'] = setSpace( $row['C_6_4_05'] );    //4：6-イ-1：対応時間：水：時（から）
    $datas['04_06_i_01_03_to'] = setSpace( $row['C_6_4_06'] );    //4：6-イ-1：対応時間：水：時（まで）
    $datas['04_06_i_01_04_from'] = setSpace( $row['C_6_4_07'] );    //4：6-イ-1：対応時間：木：時（から）
    $datas['04_06_i_01_04_to'] = setSpace( $row['C_6_4_08'] );    //4：6-イ-1：対応時間：木：時（まで）
    $datas['04_06_i_01_05_from'] = setSpace( $row['C_6_4_09'] );    //4：6-イ-1：対応時間：金：時（から）
    $datas['04_06_i_01_05_to'] = setSpace( $row['C_6_4_10'] );    //4：6-イ-1：対応時間：金：時（まで）
    $datas['04_06_i_01_06_from'] = setSpace( $row['C_6_5_1'] );    //4：6-イ-1：対応時間：土：時（から）
    $datas['04_06_i_01_06_to'] = setSpace( $row['C_6_5_2'] );    //4：6-イ-1：対応時間：土：時（まで）
    $datas['04_06_i_01_07_from'] = setSpace( $row['C_6_6_1'] );    //4：6-イ-1：対応時間：日：時（から）
    $datas['04_06_i_01_07_to'] = setSpace( $row['C_6_6_2'] );    //4：6-イ-1：対応時間：日：時（まで）
    $datas['04_06_i_01_08_from'] = setSpace( $row['C_6_7_1'] );    //4：6-イ-1：対応時間：祝日：時（から）
    $datas['04_06_i_01_08_to'] = setSpace( $row['C_6_7_2'] );    //4：6-イ-1：対応時間：祝日：時（まで）
    if ($row['C_6_8_1_1'] == "1") $datas['04_06_i_02_01'] = true;    //4：6-イ-2：相談方法：直接面談
    if ($row['C_6_8_1_2'] == "1") $datas['04_06_i_02_02'] = true;    //4：6-イ-2：相談方法：電話
    if ($row['C_6_8_1_3'] == "1") $datas['04_06_i_02_03'] = true;    //4：6-イ-2：相談方法：メール
    if ($row['C_6_8_1_4'] == "1") $datas['04_06_i_02_04'] = true;    //4：6-イ-2：相談方法：その他
    //4：6-イ-2：電話番号の「ー」分割　★★★
    $telAry = preg_split("/[-]/",$row['C_6_8_2']);
    if(count($telAry)==1){
        $datas['04_06_i_02_07'] = $telAry[0];            //4：6-イ-2：住所：電話番号例外１
    }elseif(count($telAry)==2){
        $datas['04_06_i_02_06'] = $telAry[0];            //4：6-イ-2：住所：電話番号例外２
        $datas['04_06_i_02_07'] = $telAry[1];            //4：6-イ-2：住所：電話番号例外２
    }elseif(count($telAry)>=3){
        $datas['04_06_i_02_05'] = $telAry[0];            //4：6-イ-2：住所：電話番号
        $datas['04_06_i_02_06'] = $telAry[1];            //4：6-イ-2：住所：電話番号
        $datas['04_06_i_02_07'] = $telAry[2];            //4：6-イ-2：住所：電話番号
    }
    $datas['04_06_i_02_08'] = $row['C_6_8_3'];    //4：6-イ-2：相談方法：メール：メールアドレス
    $datas['04_06_i_02_09'] = $row['C_6_8_4'];    //4：6-イ-2：相談方法：その他：内容
    if ($row['C_6_9_1_1'] == "1") $datas['04_06_i_03_01'] = true;    //4：6-イ-3：緊急時対応：直接面談
    if ($row['C_6_9_1_2'] == "1") $datas['04_06_i_03_02'] = true;    //4：6-イ-3：緊急時対応：電話
    if ($row['C_6_9_1_3'] == "1") $datas['04_06_i_03_03'] = true;    //4：6-イ-3：緊急時対応：メール
    if ($row['C_6_9_1_4'] == "1") $datas['04_06_i_03_04'] = true;    //4：6-イ-3：緊急時対応：その他
    //4：6-イ-3：電話番号の「ー」分割　★★★
    $telAry = preg_split("/[-]/",$row['C_6_9_2']);
    if(count($telAry)==1){
        $datas['04_06_i_03_07'] = $telAry[0];            //4：6-イ-3：住所：電話番号例外１
    }elseif(count($telAry)==2){
        $datas['04_06_i_03_06'] = $telAry[0];            //4：6-イ-3：住所：電話番号例外２
        $datas['04_06_i_03_07'] = $telAry[1];            //4：6-イ-3：住所：電話番号例外２
    }elseif(count($telAry)>=3){
        $datas['04_06_i_03_05'] = $telAry[0];            //4：6-イ-3：住所：電話番号
        $datas['04_06_i_03_06'] = $telAry[1];            //4：6-イ-3：住所：電話番号
        $datas['04_06_i_03_07'] = $telAry[2];            //4：6-イ-3：住所：電話番号
    }
    $datas['04_06_i_03_08'] = $row['C_6_9_3'];    //4：6-イ-3：緊急時対応：メール：メールアドレス
    $datas['04_06_i_03_09'] = $row['C_6_9_4'];    //4：6-イ-3：緊急時対応：その他：内容
    $datas['04_06_u'] = $row['C_6_9_5'];    //4：6-ウ：実施言語
    if( strcmp(  strval($row['C_7_1_1']) , "1") == 0 ){
        $datas['04_07_a_01'] = $row['C_7_1_1'];    //4：7-a：実施予定有
        $datas['04_07_a_03'] = $row['C_7_1_2'];    //4：7-a：実施予定有：内容
    }
    if( strcmp(  strval($row['C_7_1_1']) , "2") == 0 ){
        $datas['04_07_a_02'] = $row['C_7_1_1'];    //4：7-a：実施予定無
        $datas['04_07_a_04'] = $row['C_7_1_2'];    //4：7-a：実施予定無：内容
    }
    if( strcmp(  strval($row['C_7_1_3']) , "1") == 0 ){
        $datas['04_07_a_05_yes'] = $row['C_7_1_3'];    //4：7-a：委託有
    }
    if( strcmp(  strval($row['C_7_1_3']) , "2") == 0 ){
        $datas['04_07_a_05_no'] = $row['C_7_1_3'];    //4：7-a：委託無
    }
    if( strcmp(  strval($row['C_7_1_1']) , "2") <> 0 ){
        $datas['04_07_a_06'] = convSuppoterName($db, $row["C_7_1_4"], $companyDataCache);    //4：7-a：氏名（役職）   
    }
    if( strcmp(  strval($row['C_7_1_3']) , "1") == 0 ){
        //4：7-a：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_07_a_08'] = $postalAry[0];         //4：7-a：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_07_a_07'] = $postalAry[0];         //4：7-a：住所：郵便番号3桁
            $datas['04_07_a_08'] = $postalAry[1];         //4：7-a：住所：郵便番号4桁
        }
        $datas['04_07_a_09'] = $row['support_address'];    //4：7-a：住所：住所
    }
    if( strcmp(  strval($row['C_7_2_1']) , "1") == 0 ){
        $datas['04_07_b_01'] = $row['C_7_2_1'];    //4：7-b：実施予定有
        $datas['04_07_b_03'] = $row['C_7_2_2'];    //4：7-b：実施予定有：内容
    }
    if( strcmp(  strval($row['C_7_2_1']) , "2") == 0 ){
        $datas['04_07_b_02'] = $row['C_7_2_1'];    //4：7-b：実施予定無
        $datas['04_07_b_04'] = $row['C_7_2_2'];    //4：7-b：実施予定無：内容
    }
    if( strcmp(  strval($row['C_7_2_3']) , "1") == 0 ){
        $datas['04_07_b_05_yes'] = $row['C_7_2_3'];    //4：7-b：委託有
    }
    if( strcmp(  strval($row['C_7_2_3']) , "2") == 0 ){
        $datas['04_07_b_05_no'] = $row['C_7_2_3'];    //4：7-b：委託無
    }
    if( strcmp(  strval($row['C_7_2_1']) , "2") <> 0 ){
        $datas['04_07_b_06'] = convSuppoterName($db, $row["C_7_2_4"], $companyDataCache);    //4：7-b：氏名（役職）   
    }
    if( strcmp(  strval($row['C_7_2_3']) , "1") == 0 ){
        //4：7-b：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_07_b_08'] = $postalAry[0];         //4：7-b：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_07_b_07'] = $postalAry[0];         //4：7-b：住所：郵便番号3桁
            $datas['04_07_b_08'] = $postalAry[1];         //4：7-b：住所：郵便番号4桁
        }
        $datas['04_07_b_09'] = $row['support_address'];    //4：7-b：住所：住所
    }
    $datas['04_07_free_01'] = $row['C_7_3_1'];    //4：7-free：自由記入
    if( strcmp(  strval($row['C_7_3_2']) , "1") == 0 ){
        $datas['04_07_free_02'] = $row['C_7_3_2'];    //4：7-free：実施予定有
        $datas['04_07_free_04'] = $row['C_7_3_3'];    //4：7-free：実施予定有：内容
    }
    if( strcmp(  strval($row['C_7_3_2']) , "2") == 0 ){
        $datas['04_07_free_03'] = $row['C_7_3_2'];    //4：7-free：実施予定無
        $datas['04_07_free_05'] = $row['C_7_3_3'];    //4：7-free：実施予定無：内容
    }
    if( strcmp(  strval($row['C_7_3_4']) , "1") == 0 ){
        $datas['04_07_free_06_yes'] = $row['C_7_3_4'];    //4：7-free：委託有
    }
    if( strcmp(  strval($row['C_7_3_4']) , "2") == 0 ){
        $datas['04_07_free_06_no'] = $row['C_7_3_4'];    //4：7-free：委託無
    }
    if( strcmp(  strval($row['C_7_3_2']) , "2") <> 0 ){
        $datas['04_07_free_07'] = convSuppoterName($db, $row["C_7_3_5"], $companyDataCache);    //4：7-free：氏名（役職）   
    }
    if( strcmp(  strval($row['C_7_3_4']) , "1") == 0 ){
        //4：7-free：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_07_b_08'] = $postalAry[0];         //4：7-free：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_07_free_08'] = $postalAry[0];         //4：7-free：住所：郵便番号3桁
            $datas['04_07_free_09'] = $postalAry[1];         //4：7-free：住所：郵便番号4桁
        }
        $datas['04_07_free_10'] = $row['support_address'];    //4：7-free：住所：住所
    }
    $datas['04_07_free_11'] = $row['C_7_3_6'];    //4：7-free：実施方法：内容
    if( strcmp(  strval($row['C_8_1_1']) , "1") == 0 ){
        $datas['04_08_a_01'] = $row['C_8_1_1'];    //4：8-a：実施予定有
        $datas['04_08_a_03'] = $row['C_8_1_2'];    //4：8-a：実施予定有：内容
    }
    if( strcmp(  strval($row['C_8_1_1']) , "2") == 0 ){
        $datas['04_08_a_02'] = $row['C_8_1_1'];    //4：8-a：実施予定無
        $datas['04_08_a_04'] = $row['C_8_1_2'];    //4：8-a：実施予定無：内容
    }
    if( strcmp(  strval($row['C_8_1_3']) , "1") == 0 ){
        $datas['04_08_a_05_yes'] = $row['C_8_1_3'];    //4：8-a：委託有
    }
    if( strcmp(  strval($row['C_8_1_3']) , "2") == 0 ){
        $datas['04_08_a_05_no'] = $row['C_8_1_3'];    //4：8-a：委託無
    }
    if( strcmp(  strval($row['C_8_1_1']) , "2") <> 0 ){
        $datas['04_08_a_06'] = convSuppoterName($db, $row["C_8_1_4"], $companyDataCache);    //4：8-a：氏名（役職）   
    }
    if( strcmp(  strval($row['C_8_1_3']) , "1") == 0 ){
        //4：8-a：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_08_a_08'] = $postalAry[0];         //4：8-a：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_08_a_07'] = $postalAry[0];         //4：8-a：住所：郵便番号3桁
            $datas['04_08_a_08'] = $postalAry[1];         //4：8-a：住所：郵便番号4桁
        }
        $datas['04_08_a_09'] = $row['support_address'];    //4：8-a：住所：住所
    }
    if( strcmp(  strval($row['C_8_2_1']) , "1") == 0 ){
        $datas['04_08_b_01'] = $row['C_8_2_1'];    //4：8-b：実施予定有
        $datas['04_08_b_03'] = $row['C_8_2_2'];    //4：8-b：実施予定有：内容
    }
    if( strcmp(  strval($row['C_8_2_1']) , "2") == 0 ){
        $datas['04_08_b_02'] = $row['C_8_2_1'];    //4：8-b：実施予定無
        $datas['04_08_b_04'] = $row['C_8_2_2'];    //4：8-b：実施予定無：内容
    }
    if( strcmp(  strval($row['C_8_2_3']) , "1") == 0 ){
        $datas['04_08_b_05_yes'] = $row['C_8_2_3'];    //4：8-b：委託有
    }
    if( strcmp(  strval($row['C_8_2_3']) , "2") == 0 ){
        $datas['04_08_b_05_no'] = $row['C_8_2_3'];    //4：8-b：委託無
    }
    if( strcmp(  strval($row['C_8_2_1']) , "2") <> 0 ){
        $datas['04_08_b_06'] = convSuppoterName($db, $row["C_8_2_4"], $companyDataCache);    //4：8-b：氏名（役職）   
    }
    if( strcmp(  strval($row['C_8_2_3']) , "1") == 0 ){
        //4：8-b：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_08_b_08'] = $postalAry[0];         //4：8-b：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_08_b_07'] = $postalAry[0];         //4：8-b：住所：郵便番号3桁
            $datas['04_08_b_08'] = $postalAry[1];         //4：8-b：住所：郵便番号4桁
        }
        $datas['04_08_b_09'] = $row['support_address'];    //4：8-b：住所：住所
    }
    if( strcmp(  strval($row['C_8_3_1']) , "1") == 0 ){
        $datas['04_08_c_01'] = $row['C_8_3_1'];    //4：8-c：実施予定有
        $datas['04_08_c_03'] = $row['C_8_3_2'];    //4：8-c：実施予定有：内容
    }
    if( strcmp(  strval($row['C_8_3_1']) , "2") == 0 ){
        $datas['04_08_c_02'] = $row['C_8_3_1'];    //4：8-c：実施予定無
        $datas['04_08_c_04'] = $row['C_8_3_2'];    //4：8-c：実施予定無：内容
    }
    if( strcmp(  strval($row['C_8_3_3']) , "1") == 0 ){
        $datas['04_08_c_05_yes'] = $row['C_8_3_3'];    //4：8-c：委託有
    }
    if( strcmp(  strval($row['C_8_3_3']) , "2") == 0 ){
        $datas['04_08_c_05_no'] = $row['C_8_3_3'];    //4：8-c：委託無
    }
    if( strcmp(  strval($row['C_8_3_1']) , "2") <> 0 ){
        $datas['04_08_c_06'] = convSuppoterName($db, $row["C_8_3_4"], $companyDataCache);    //4：8-c：氏名（役職）   
    }
    if( strcmp(  strval($row['C_8_3_3']) , "1") == 0 ){
        //4：8-c：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_08_c_08'] = $postalAry[0];         //4：8-c：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_08_c_07'] = $postalAry[0];         //4：8-c：住所：郵便番号3桁
            $datas['04_08_c_08'] = $postalAry[1];         //4：8-c：住所：郵便番号4桁
        }
        $datas['04_08_c_09'] = $row['support_address'];    //4：8-c：住所：住所
    }
    if( strcmp(  strval($row['C_8_4_1']) , "1") == 0 ){
        $datas['04_08_d_01'] = $row['C_8_4_1'];    //4：8-d：実施予定有
        $datas['04_08_d_03'] = $row['C_8_4_2'];    //4：8-d：実施予定有：内容
    }
    if( strcmp(  strval($row['C_8_4_1']) , "2") == 0 ){
        $datas['04_08_d_02'] = $row['C_8_4_1'];    //4：8-d：実施予定無
        $datas['04_08_d_04'] = $row['C_8_4_2'];    //4：8-d：実施予定無：内容
    }
    if( strcmp(  strval($row['C_8_4_3']) , "1") == 0 ){
        $datas['04_08_d_05_yes'] = $row['C_8_4_3'];    //4：8-d：委託有
    }
    if( strcmp(  strval($row['C_8_4_3']) , "2") == 0 ){
        $datas['04_08_d_05_no'] = $row['C_8_4_3'];    //4：8-d：委託無
    }
    if( strcmp(  strval($row['C_8_4_1']) , "2") <> 0 ){
        $datas['04_08_d_06'] = convSuppoterName($db, $row["C_8_4_4"], $companyDataCache);    //4：8-d：氏名（役職）   
    }
    if( strcmp(  strval($row['C_8_4_3']) , "1") == 0 ){
        //4：8-d：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_08_d_08'] = $postalAry[0];         //4：8-d：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_08_d_07'] = $postalAry[0];         //4：8-d：住所：郵便番号3桁
            $datas['04_08_d_08'] = $postalAry[1];         //4：8-d：住所：郵便番号4桁
        }
        $datas['04_08_d_09'] = $row['support_address'];    //4：8-d：住所：住所
    }
    if( strcmp(  strval($row['C_8_5_1']) , "1") == 0 ){
        $datas['04_08_e_01'] = $row['C_8_5_1'];    //4：8-e：実施予定有
        $datas['04_08_e_03'] = $row['C_8_5_2'];    //4：8-e：実施予定有：内容
    }
    if( strcmp(  strval($row['C_8_5_1']) , "2") == 0 ){
        $datas['04_08_e_02'] = $row['C_8_5_1'];    //4：8-e：実施予定無
        $datas['04_08_e_04'] = $row['C_8_5_2'];    //4：8-e：実施予定無：内容
    }
    if( strcmp(  strval($row['C_8_6_1']) , "1") == 0 ){
        $datas['04_08_f_01'] = $row['C_8_6_1'];    //4：8-f：実施予定有
        $datas['04_08_f_03'] = $row['C_8_6_2'];    //4：8-f：実施予定有：内容
    }
    if( strcmp(  strval($row['C_8_6_1']) , "2") == 0 ){
        $datas['04_08_f_02'] = $row['C_8_6_1'];    //4：8-f：実施予定無
        $datas['04_08_f_04'] = $row['C_8_6_2'];    //4：8-f：実施予定無：内容
    }
    if( strcmp(  strval($row['C_8_6_3']) , "1") == 0 ){
        $datas['04_08_f_05_yes'] = $row['C_8_6_3'];    //4：8-f：委託有
    }
    if( strcmp(  strval($row['C_8_6_3']) , "2") == 0 ){
        $datas['04_08_f_05_no'] = $row['C_8_6_3'];    //4：8-f：委託無
    }
    if( strcmp(  strval($row['C_8_6_1']) , "2") <> 0 ){
        $datas['04_08_f_06'] = convSuppoterName($db, $row["C_8_6_4"], $companyDataCache);    //4：8-f：氏名（役職）   
    }
    if( strcmp(  strval($row['C_8_6_3']) , "1") == 0 ){
        //4：8-f：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_08_f_08'] = $postalAry[0];         //4：8-f：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_08_f_07'] = $postalAry[0];         //4：8-f：住所：郵便番号3桁
            $datas['04_08_f_08'] = $postalAry[1];         //4：8-f：住所：郵便番号4桁
        }
        $datas['04_08_f_09'] = $row['support_address'];    //4：8-f：住所：住所
    }
    if ($row['C_8_6_5_1'] == "1") $datas['04_08_f_10'] = true;    //4：8-f：実施方法：口頭
    if ($row['C_8_6_5_2'] == "1") $datas['04_08_f_11'] = true;    //4：8-f：実施方法：書面
    if ($row['C_8_6_5_3'] == "1") $datas['04_08_f_12'] = true;    //4：8-f：実施方法：その他
    $datas['04_08_f_13'] = $row['C_8_6_6'];    //4：8-f：実施方法：その他：内容
    if( strcmp(  strval($row['C_8_7_1']) , "1") == 0 ){
        $datas['04_08_g_01'] = $row['C_8_7_1'];    //4：8-g：実施予定有
        $datas['04_08_g_03'] = $row['C_8_7_2'];    //4：8-g：実施予定有：内容
    }
    if( strcmp(  strval($row['C_8_7_1']) , "2") == 0 ){
        $datas['04_08_g_02'] = $row['C_8_7_1'];    //4：8-g：実施予定無
        $datas['04_08_g_04'] = $row['C_8_7_2'];    //4：8-g：実施予定無：内容
    }
    if( strcmp(  strval($row['C_8_7_3']) , "1") == 0 ){
        $datas['04_08_g_05_yes'] = $row['C_8_7_3'];    //4：8-g：委託有
    }
    if( strcmp(  strval($row['C_8_7_3']) , "2") == 0 ){
        $datas['04_08_g_05_no'] = $row['C_8_7_3'];    //4：8-g：委託無
    }
    if( strcmp(  strval($row['C_8_7_1']) , "2") <> 0 ){
        $datas['04_08_g_06'] = convSuppoterName($db, $row["C_8_7_4"], $companyDataCache);    //4：8-g：氏名（役職）   
    }
    if( strcmp(  strval($row['C_8_7_3']) , "1") == 0 ){
        //4：8-g：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_08_g_08'] = $postalAry[0];         //4：8-g：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_08_g_07'] = $postalAry[0];         //4：8-g：住所：郵便番号3桁
            $datas['04_08_g_08'] = $postalAry[1];         //4：8-g：住所：郵便番号4桁
        }
        $datas['04_08_g_09'] = $row['support_address'];    //4：8-g：住所：住所
    }
    $datas['04_08_free_01'] = $row['C_8_8_1'];    //4：8-free：自由記入
    if( strcmp(  strval($row['C_8_8_2']) , "1") == 0 ){
        $datas['04_08_free_02'] = $row['C_8_8_2'];    //4：8-free：実施予定有
        $datas['04_08_free_04'] = $row['C_8_8_3'];    //4：8-free：実施予定有：内容
    }
    if( strcmp(  strval($row['C_8_8_2']) , "2") == 0 ){
        $datas['04_08_free_03'] = $row['C_8_8_2'];    //4：8-free：実施予定無
        $datas['04_08_free_05'] = $row['C_8_8_3'];    //4：8-free：実施予定無：内容
    }
    if( strcmp(  strval($row['C_8_8_4']) , "1") == 0 ){
        $datas['04_08_free_06_yes'] = $row['C_8_8_4'];    //4：8-free：委託有
    }
    if( strcmp(  strval($row['C_8_8_4']) , "2") == 0 ){
        $datas['04_08_free_06_no'] = $row['C_8_8_4'];    //4：8-free：委託無
    }
    if( strcmp(  strval($row['C_8_8_2']) , "2") <> 0 ){
        $datas['04_08_free_07'] = convSuppoterName($db, $row["C_8_8_5"], $companyDataCache);    //4：8-free：氏名（役職）   
    }
    if( strcmp(  strval($row['C_8_8_4']) , "1") == 0 ){
        //4：8-free：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_08_g_08'] = $postalAry[0];         //4：8-free：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_08_free_08'] = $postalAry[0];         //4：8-free：住所：郵便番号3桁
            $datas['04_08_free_09'] = $postalAry[1];         //4：8-free：住所：郵便番号4桁
        }
        $datas['04_08_free_10'] = $row['support_address'];    //4：8-free：住所：住所
    }
    $datas['04_08_free_11'] = $row['C_8_8_6'];    //4：8-free：実施方法：内容
    if( strcmp(  strval($row['C_9_1_1']) , "1") == 0 ){
        $datas['04_09_a_a_01'] = $row['C_9_1_1'];    //4：9-ア-a：実施予定有
        $datas['04_09_a_a_03'] = $row['C_9_1_2'];    //4：9-ア-a：実施予定有：内容
    }
    if( strcmp(  strval($row['C_9_1_1']) , "2") == 0 ){
        $datas['04_09_a_a_02'] = $row['C_9_1_1'];    //4：9-ア-a：実施予定無
        $datas['04_09_a_a_04'] = $row['C_9_1_2'];    //4：9-ア-a：実施予定無：内容
    }
    if( strcmp(  strval($row['C_9_1_3']) , "1") == 0 ){
        $datas['04_09_a_a_05_yes'] = $row['C_9_1_3'];    //4：9-ア-a：委託有
    }
    if( strcmp(  strval($row['C_9_1_3']) , "2") == 0 ){
        $datas['04_09_a_a_05_no'] = $row['C_9_1_3'];    //4：9-ア-a：委託無
    }
    if( strcmp(  strval($row['C_9_1_1']) , "2") <> 0 ){
        $datas['04_09_a_a_06'] = convSuppoterName($db, $row["C_9_1_4"], $companyDataCache);    //4：9-ア-a：氏名（役職）   
    }
    if( strcmp(  strval($row['C_9_1_3']) , "1") == 0 ){
        //4：9-ア-a：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_09_a_a_08'] = $postalAry[0];         //4：9-ア-a：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_09_a_a_07'] = $postalAry[0];         //4：9-ア-a：住所：郵便番号3桁
            $datas['04_09_a_a_08'] = $postalAry[1];         //4：9-ア-a：住所：郵便番号4桁
        }
        $datas['04_09_a_a_09'] = $row['support_address'];    //4：9-ア-a：住所：住所
    }
    if ($row['C_9_1_5_1'] == "1") $datas['04_09_a_a_10'] = true;    //4：9-ア-a：実施方法：口頭
    if ($row['C_9_1_5_2'] == "1") $datas['04_09_a_a_11'] = true;    //4：9-ア-a：実施方法：書面（アンケート配布）
    if ($row['C_9_1_5_3'] == "1") $datas['04_09_a_a_12'] = true;    //4：9-ア-a：実施方法：その他
    $datas['04_09_a_a_13'] = $row['C_9_1_6'];    //4：9-ア-a：実施方法：その他：内容
    if( strcmp(  strval($row['C_9_2_1']) , "1") == 0 ){
        $datas['04_09_a_b_01'] = $row['C_9_2_1'];    //4：9-ア-b：実施予定有
        $datas['04_09_a_b_03'] = $row['C_9_2_2'];    //4：9-ア-b：実施予定有：内容
    }
    if( strcmp(  strval($row['C_9_2_1']) , "2") == 0 ){
        $datas['04_09_a_b_02'] = $row['C_9_2_1'];    //4：9-ア-b：実施予定無
        $datas['04_09_a_b_04'] = $row['C_9_2_2'];    //4：9-ア-b：実施予定無：内容
    }
    if( strcmp(  strval($row['C_9_2_3']) , "1") == 0 ){
        $datas['04_09_a_b_05_yes'] = $row['C_9_2_3'];    //4：9-ア-b：委託有
    }
    if( strcmp(  strval($row['C_9_2_3']) , "2") == 0 ){
        $datas['04_09_a_b_05_no'] = $row['C_9_2_3'];    //4：9-ア-b：委託無
    }
    if( strcmp(  strval($row['C_9_2_1']) , "2") <> 0 ){
        $datas['04_09_a_b_06'] = convSuppoterName($db, $row["C_9_2_4"], $companyDataCache);    //4：9-ア-b：氏名（役職）   
    }
    if( strcmp(  strval($row['C_9_2_3']) , "1") == 0 ){
        //4：9-ア-b：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_09_a_b_08'] = $postalAry[0];         //4：9-ア-b：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_09_a_b_07'] = $postalAry[0];         //4：9-ア-b：住所：郵便番号3桁
            $datas['04_09_a_b_08'] = $postalAry[1];         //4：9-ア-b：住所：郵便番号4桁
        }
        $datas['04_09_a_b_09'] = $row['support_address'];    //4：9-ア-b：住所：住所
    }
    if ($row['C_9_2_5_1'] == "1") $datas['04_09_a_b_10'] = true;    //4：9-ア-b：実施方法：口頭
    if ($row['C_9_2_5_2'] == "1") $datas['04_09_a_b_11'] = true;    //4：9-ア-b：実施方法：書面（翻訳含む）
    if ($row['C_9_2_5_3'] == "1") $datas['04_09_a_b_12'] = true;    //4：9-ア-b：実施方法：その他
    $datas['04_09_a_b_13'] = $row['C_9_2_6'];    //4：9-ア-b：実施方法：その他：内容
    if( strcmp(  strval($row['C_9_3_1']) , "1") == 0 ){
        $datas['04_09_a_c_01'] = $row['C_9_3_1'];    //4：9-ア-c：実施予定有
    }
    if( strcmp(  strval($row['C_9_3_1']) , "2") == 0 ){
        $datas['04_09_a_c_02'] = $row['C_9_3_1'];    //4：9-ア-c：実施予定無
    }
    $datas['04_09_a_c_03'] = $row['C_9_3_2'];    //4：9-ア-c：実施予定無：内容
    if( strcmp(  strval($row['C_9_3_3']) , "1") == 0 ){
        $datas['04_09_a_c_04_yes'] = $row['C_9_3_3'];    //4：9-ア-c：委託有
    }
    if( strcmp(  strval($row['C_9_3_3']) , "2") == 0 ){
        $datas['04_09_a_c_04_no'] = $row['C_9_3_3'];    //4：9-ア-c：委託無
    }
    if( strcmp(  strval($row['C_9_3_1']) , "2") <> 0 ){
        $datas['04_09_a_c_05'] = convSuppoterName($db, $row["C_9_3_4"], $companyDataCache);    //4：9-ア-c：氏名（役職）   
    }
    if( strcmp(  strval($row['C_9_3_3']) , "1") == 0 ){
        //4：9-ア-c：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_09_a_c_07'] = $postalAry[0];         //4：9-ア-c：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_09_a_c_06'] = $postalAry[0];         //4：9-ア-c：住所：郵便番号3桁
            $datas['04_09_a_c_07'] = $postalAry[1];         //4：9-ア-c：住所：郵便番号4桁
        }
        $datas['04_09_a_c_08'] = $row['support_address'];    //4：9-ア-c：住所：住所
    }
    if( strcmp(  strval($row['C_9_4_1']) , "1") == 0 ){
        $datas['04_09_a_d_01'] = $row['C_9_4_1'];    //4：9-ア-d：実施予定有
    }
    if( strcmp(  strval($row['C_9_4_1']) , "2") == 0 ){
        $datas['04_09_a_d_02'] = $row['C_9_4_1'];    //4：9-ア-d：実施予定無
    }
    $datas['04_09_a_d_03'] = $row['C_9_4_2'];    //4：9-ア-d：実施予定無：内容
    if( strcmp(  strval($row['C_9_4_3']) , "1") == 0 ){
        $datas['04_09_a_d_04_yes'] = $row['C_9_4_3'];    //4：9-ア-d：委託有
    }
    if( strcmp(  strval($row['C_9_4_3']) , "2") == 0 ){
        $datas['04_09_a_d_04_no'] = $row['C_9_4_3'];    //4：9-ア-d：委託無
    }
    if( strcmp(  strval($row['C_9_4_1']) , "2") <> 0 ){
        $datas['04_09_a_d_05'] = convSuppoterName($db, $row["C_9_4_4"], $companyDataCache);    //4：9-ア-d：氏名（役職）   
    }
    if( strcmp(  strval($row['C_9_4_3']) , "1") == 0 ){
        //4：9-ア-d：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_09_a_d_07'] = $postalAry[0];         //4：9-ア-d：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_09_a_d_06'] = $postalAry[0];         //4：9-ア-d：住所：郵便番号3桁
            $datas['04_09_a_d_07'] = $postalAry[1];         //4：9-ア-d：住所：郵便番号4桁
        }
        $datas['04_09_a_d_08'] = $row['support_address'];    //4：9-ア-d：住所：住所
    }
    $datas['04_09_a_free_01'] = $row['C_9_5_1'];    //4：9-ア-free：自由記入
    if( strcmp(  strval($row['C_9_5_2']) , "1") == 0 ){
        $datas['04_09_a_free_02'] = $row['C_9_5_2'];    //4：9-ア-free：実施予定有
        $datas['04_09_a_free_04'] = $row['C_9_5_3'];    //4：9-ア-free：実施予定有：内容
    }
    if( strcmp(  strval($row['C_9_5_2']) , "2") == 0 ){
        $datas['04_09_a_free_03'] = $row['C_9_5_2'];    //4：9-ア-free：実施予定無
        $datas['04_09_a_free_05'] = $row['C_9_5_3'];    //4：9-ア-free：実施予定無：内容
    }
    if( strcmp(  strval($row['C_9_5_4']) , "1") == 0 ){
        $datas['04_09_a_free_06_yes'] = $row['C_9_5_4'];    //4：9-ア-free：委託有
    }
    if( strcmp(  strval($row['C_9_5_4']) , "2") == 0 ){
        $datas['04_09_a_free_06_no'] = $row['C_9_5_4'];    //4：9-ア-free：委託無
    }
    if( strcmp(  strval($row['C_9_5_2']) , "2") <> 0 ){
        $datas['04_09_a_free_07'] = convSuppoterName($db, $row["C_9_5_5"], $companyDataCache);    //4：9-ア-free：氏名（役職）   
    }
    if( strcmp(  strval($row['C_9_5_4']) , "1") == 0 ){
        //4：9-ア-free：郵便番号の「ー」分割　★★★
        $postalAry = preg_split("/[-]/",$row['support_postal_code']);
        if(count($postalAry)==1){
            $datas['04_09_a_d_07'] = $postalAry[0];         //4：9-ア-free：住所：郵便番号例外
        }elseif(count($postalAry)==2){
            $datas['04_09_a_free_08'] = $postalAry[0];         //4：9-ア-free：住所：郵便番号3桁
            $datas['04_09_a_free_09'] = $postalAry[1];         //4：9-ア-free：住所：郵便番号4桁
        }
        $datas['04_09_a_free_10'] = $row['support_address'];    //4：9-ア-free：住所：住所
    }
    $datas['04_09_a_free_11'] = $row['C_9_5_6'];    //4：9-ア-free：住所：実施方法：内容
    $datas['04_09_i'] = $row['C_9_6'];    //4：9-イ：実施言語
    $datas['04_09_u'] = $row['C_9_7'];    //4：9-ウ：実施予定時間

    return $datas;

}

/**
 * DBから「支援計画書」の情報を取得し、結果を返す。
 *
 * @param object $db DB接続
 * @param string $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function searchPDF04($db, $workerId) {
    //取得に必要な労働者rowIDを取得する
    $WorkerRowId = $db->getWorkerRowId($workerId);

    $select_sql_str  = "SELECT mst_workers.*,mst_company.*,tbl_report_002.* ";
    //作成日
    $select_sql_str  .= ",date_format(tbl_report_002.update_date,'%Y')  as create_date_Y ";
    $select_sql_str  .= ",date_format(tbl_report_002.update_date,'%c')  as create_date_M "; 
    $select_sql_str  .= ",date_format(tbl_report_002.update_date,'%e')  as create_date_D "; 
    //誕生日
    $select_sql_str  .= ",date_format(birthday,'%Y')  as birthday_Y ";
    $select_sql_str  .= ",date_format(birthday,'%c')  as birthday_M "; 
    $select_sql_str  .= ",date_format(birthday,'%e')  as birthday_D "; 
    //登録年月日
    $select_sql_str  .= ",date_format(registration_date,'%Y')  as registration_date_Y ";
    $select_sql_str  .= ",date_format(registration_date,'%c')  as registration_date_M "; 
    $select_sql_str  .= ",date_format(registration_date,'%e')  as registration_date_D ";
    //支援業務の予定年月日
    $select_sql_str  .= ",date_format(support_scheduled_date,'%Y')  as support_scheduled_date_Y ";
    $select_sql_str  .= ",date_format(support_scheduled_date,'%c')  as support_scheduled_date_M "; 
    $select_sql_str  .= ",date_format(support_scheduled_date,'%e')  as support_scheduled_date_D ";

    $select_sql_str  .= "FROM mst_workers ";
    $select_sql_str  .= "left join tbl_report_002 on mst_workers.id = tbl_report_002.worker_id ";
    $select_sql_str  .= "left join mst_company on mst_company.id = mst_workers.company_id ";
    $select_sql_str  .= "WHERE mst_workers.id=:worker_rowid ";

    $params = array();
    $params[":worker_rowid"] = $WorkerRowId;

    $result = $db->selectone($select_sql_str, $params);

    if (count($result) == 0) {
        // レコードが取得できなかった場合、データ不整備エラーとする。
        $errMessage = getCommonMessage("EC004");
        goErrorPage($errMessage);
    }

    // 取得結果を返す。
    return $result;
}

/**
 * DBから支援機関の情報を取得し、結果を返す。
 *
 * @return array() 支援機関の情報
 */
function getSupportCompanyInfo() {
    $db = new JinzaiDb(DB_DEFINE);

    $sql = "SELECT  "
         .     "registration_no, "
         .     "DATE_FORMAT(registration_date, '%Y') AS registration_date_Y, "
         .     "DATE_FORMAT(registration_date, '%c') AS registration_date_M, "
         .     "DATE_FORMAT(registration_date, '%e') AS registration_date_D, "
         .     "DATE_FORMAT(support_scheduled_date, '%Y') AS support_scheduled_date_Y, "
         .     "DATE_FORMAT(support_scheduled_date, '%c') AS support_scheduled_date_M, "
         .     "DATE_FORMAT(support_scheduled_date, '%e') AS support_scheduled_date_D, "
         .     "company_kana, "
         .     "company_name, "
         .     "postal_code, "
         .     "address, "
         .     "tel, "
         .     "representative_kana, "
         .     "representative_name, "
         .     "corporation_no, "
         .     "support_postal_code, "
         .     "support_address, "
         .     "support_tel, "
         .     "support_manager_kana, "
         .     "support_manager_name, "
         .     "support_manager_department, "
         .     "worker_mumber, "
         .     "support_stuff_number "
         . "FROM mst_company "
         . "WHERE mst_company.classification = 2 "  // 2:支援機関
    ;
    $params = array();

    $result = $db->selectOne($sql, $params);

    if ($result == null) {
        // レコードが取得できなかった場合、データ不整備エラーとする。
        $errMessage = getCommonMessage("EC004");
        goErrorPage($errMessage);
    }

    // 取得結果を返す。
    return $result;
}

function setSpace($str){
    $ret = "";
    $ret = str_replace(":","   ",$str);
    return $ret;
}
/**
 * 企業のrowIDを支援担当者IDに変換する。
 *
 * @param object $db DB接続
 * @param string $companyRowId 行ID
 * @param array  $cacheArray データキャッシュ
 * 
 * @return string 企業ID
 */
function convSuppoterName($db, $companyRowId, &$cacheArray) {
    // ID未設定なら、空文字を返して終了。
    if ($companyRowId == null or $companyRowId == "") return "";

    // 検索済みなら、キャッシュから内容を取得して返す。
    if (isset($cacheArray[$companyRowId])) return $cacheArray[$companyRowId];

    // DBに問合せる。
    $sql = "SELECT support_handler_name "
         . "FROM mst_company "
         . "WHERE id = :row_id ";
    $params = array(":row_id" => $companyRowId);
    $result = $db->selectOne($sql, $params);
    $suppoterName = ($result != null) ? $result["support_handler_name"] : "";

    // キャッシュに問合せ結果を追加。
    $cacheArray[$companyRowId] = $suppoterName;

    // 取得した情報を返す。
    return $suppoterName;
}
