<?php

/**
 * 労働者の支援実施状況に係る届出情報を取得し、結果を返す。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function search($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         .     "reportdata.target_period AS D_0_1, "
         .     "reportdata.quarter AS D_0_2, "
         .     "reportdata.D_1_01_1, "
         .     "reportdata.D_1_01_2, "
         .     "DATE_FORMAT(reportdata.D_1_01_3, '%Y/%m/%d') AS D_1_01_3ymd,"
         .     "reportdata.D_1_01_4, "
         .     "reportdata.D_1_02_1, "
         .     "reportdata.D_1_02_2, "
         .     "DATE_FORMAT(reportdata.D_1_02_3, '%Y/%m/%d') AS D_1_02_3ymd,"
         .     "reportdata.D_1_02_4, "
         .     "reportdata.D_1_03_1, "
         .     "reportdata.D_1_03_2, "
         .     "DATE_FORMAT(reportdata.D_1_03_3, '%Y/%m/%d') AS D_1_03_3ymd,"
         .     "reportdata.D_1_03_4, "
         .     "reportdata.D_1_04_1, "
         .     "reportdata.D_1_04_2, "
         .     "DATE_FORMAT(reportdata.D_1_04_3, '%Y/%m/%d') AS D_1_04_3ymd,"
         .     "reportdata.D_1_04_4, "
         .     "reportdata.D_1_05_1, "
         .     "reportdata.D_1_05_2, "
         .     "DATE_FORMAT(reportdata.D_1_05_3, '%Y/%m/%d') AS D_1_05_3ymd,"
         .     "reportdata.D_1_05_4, "
         .     "reportdata.D_1_06_1, "
         .     "reportdata.D_1_06_2, "
         .     "DATE_FORMAT(reportdata.D_1_06_3, '%Y/%m/%d') AS D_1_06_3ymd,"
         .     "reportdata.D_1_06_4, "
         .     "reportdata.D_1_07_1, "
         .     "reportdata.D_1_07_2, "
         .     "DATE_FORMAT(reportdata.D_1_07_3, '%Y/%m/%d') AS D_1_07_3ymd,"
         .     "reportdata.D_1_07_4, "
         .     "reportdata.D_1_08_01, "
         .     "reportdata.D_1_08_02, "
         .     "DATE_FORMAT(reportdata.D_1_08_03, '%Y/%m/%d') AS D_1_08_03ymd,"
         .     "reportdata.D_1_08_04, "
         .     "DATE_FORMAT(reportdata.D_1_08_05, '%Y/%m/%d') AS D_1_08_05ymd,"
         .     "reportdata.D_1_08_06, "
         .     "DATE_FORMAT(reportdata.D_1_08_07, '%Y/%m/%d') AS D_1_08_07ymd,"
         .     "reportdata.D_1_08_08, "
         .     "reportdata.D_1_08_09, "
         .     "reportdata.D_1_08_10, "
         .     "reportdata.D_1_09_1, "
         .     "reportdata.D_1_09_2, "
         .     "DATE_FORMAT(reportdata.D_1_09_3, '%Y/%m/%d') AS D_1_09_3ymd,"
         .     "reportdata.D_1_09_4, "
         .     "reportdata.D_1_10_1, "
         .     "reportdata.D_1_10_2, "
         .     "DATE_FORMAT(reportdata.D_1_10_3, '%Y/%m/%d') AS D_1_10_3ymd,"
         .     "reportdata.D_1_10_4, "
         .     "DATE_FORMAT(reportdata.D_1_10_5, '%Y/%m/%d') AS D_1_10_5ymd,"
         .     "reportdata.D_1_10_6, "
         .     "reportdata.D_1_10_7, "
         .     "DATE_FORMAT(reportdata.D_1_10_8, '%Y/%m/%d') AS D_1_10_8ymd,"
         .     "reportdata.D_1_10_9, "
         .     "reportdata.D_1_10_10, "
         .     "reportdata.D_1_10_11, "
         .     "reportdata.D_1_11_1, "
         .     "reportdata.D_1_11_2, "
         .     "DATE_FORMAT(reportdata.D_1_11_3, '%Y/%m/%d') AS D_1_11_3ymd,"
         .     "reportdata.D_1_11_4, "
         .     "reportdata.D_2_1_1, "
         .     "reportdata.D_2_1_2, "
         .     "reportdata.D_2_1_3, "
         .     "reportdata.D_3_1_1, "
         .     "reportdata.D_3_1_2, "
         .     "reportdata.D_3_1_3, "
         .     "reportdata.D_3_1_4, "
         .     "reportdata.target_period, "
         .     "reportdata.quarter "
              
         . "FROM tbl_report_003 reportdata "
         . "INNER JOIN mst_workers workers "
         . "ON reportdata.worker_id = workers.id "
         . "WHERE workers.worker_id = :worker_id "
         ;

    $params = array();
    $params[":worker_id"] = $workerId;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    if (count($result) == 0) {
        // レコードが取得できなかった場合、データ不整備エラーとする。
        $errMessage = getCommonMessage("EC004");
        goErrorPage($errMessage);
    }
   
    // 表示用データを追加設定。
    $yesno           = getOptionItems("comm", "yesno");
    $wk02005_1       = getOptionItems("comm", "WK02005_1");
    $wk02005_2       = getOptionItems("comm", "WK02005_2");
    
    // コードに対応する文言を設定。
    $result["D_1_01_1_select"]   = getOprionItemValue($wk02005_1, $result["D_1_01_1"]); 
    $result["D_1_02_1_select"]   = getOprionItemValue($wk02005_1, $result["D_1_02_1"]);
    $result["D_1_03_1_select"]   = getOprionItemValue($wk02005_1, $result["D_1_03_1"]);
    $result["D_1_04_1_select"]   = getOprionItemValue($wk02005_1, $result["D_1_04_1"]);
    $result["D_1_05_1_select"]   = getOprionItemValue($wk02005_1, $result["D_1_05_1"]);
    $result["D_1_06_1_select"]   = getOprionItemValue($wk02005_1, $result["D_1_06_1"]);
    $result["D_1_07_1_select"]   = getOprionItemValue($wk02005_1, $result["D_1_07_1"]);
    $result["D_1_08_01_select"]  = getOprionItemValue($wk02005_1, $result["D_1_08_01"]);
    $result["D_1_09_1_select"]   = getOprionItemValue($wk02005_1, $result["D_1_09_1"]);
    $result["D_1_10_1_select"]   = getOprionItemValue($wk02005_1, $result["D_1_10_1"]);
    $result["D_1_11_1_select"]   = getOprionItemValue($wk02005_1, $result["D_1_11_1"]);

    $result["D_1_01_2_select"]   = getOprionItemValue($wk02005_2, $result["D_1_01_2"]);
    $result["D_1_02_2_select"]   = getOprionItemValue($wk02005_2, $result["D_1_02_2"]);
    $result["D_1_03_2_select"]   = getOprionItemValue($wk02005_2, $result["D_1_03_2"]);
    $result["D_1_04_2_select"]   = getOprionItemValue($wk02005_2, $result["D_1_04_2"]);
    $result["D_1_05_2_select"]   = getOprionItemValue($wk02005_2, $result["D_1_05_2"]);
    $result["D_1_06_2_select"]   = getOprionItemValue($wk02005_2, $result["D_1_06_2"]);
    $result["D_1_07_2_select"]   = getOprionItemValue($wk02005_2, $result["D_1_07_2"]);
    $result["D_1_08_02_select"]  = getOprionItemValue($wk02005_2, $result["D_1_08_02"]);
    $result["D_1_09_2_select"]   = getOprionItemValue($wk02005_2, $result["D_1_09_2"]);
    $result["D_1_10_2_select"]   = getOprionItemValue($wk02005_2, $result["D_1_10_2"]);
    $result["D_1_11_2_select"]   = getOprionItemValue($wk02005_2, $result["D_1_11_2"]);

    $result["D_1_10_7"]  = getOprionItemValue($yesno,     $result["D_1_10_7"]);

    // 取得結果を返す。
    return $result;
}

/**
 * 企業情報から支援機関を登録している企業名を取得し、結果を返す。
 *
 * 
 * @return array[string] 会社名
 */
function getCompanyName($db) {

    // SQLを生成。
    $sql = "SELECT "
         .     "mst_company.company_name "
         . "FROM mst_company "
         . "WHERE mst_company.classification = 2 "
         ;

    $params = array();

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 取得結果を返す。
    return $result["company_name"];
}

/**
 * CSV出力用データを取得する。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function getExportDatas($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
    .     "reportdata.D_1_01_1, "
    .     "reportdata.D_1_01_2, "
    .     "DATE_FORMAT(reportdata.D_1_01_3, '%Y/%m/%d') AS D_1_01_3ymd,"
    .     "reportdata.D_1_01_4, "
    .     "reportdata.D_1_02_1, "
    .     "reportdata.D_1_02_2, "
    .     "DATE_FORMAT(reportdata.D_1_02_3, '%Y/%m/%d') AS D_1_02_3ymd,"
    .     "reportdata.D_1_02_4, "
    .     "reportdata.D_1_03_1, "
    .     "reportdata.D_1_03_2, "
    .     "DATE_FORMAT(reportdata.D_1_03_3, '%Y/%m/%d') AS D_1_03_3ymd,"
    .     "reportdata.D_1_03_4, "
    .     "reportdata.D_1_04_1, "
    .     "reportdata.D_1_04_2, "
    .     "DATE_FORMAT(reportdata.D_1_04_3, '%Y/%m/%d') AS D_1_04_3ymd,"
    .     "reportdata.D_1_04_4, "
    .     "reportdata.D_1_05_1, "
    .     "reportdata.D_1_05_2, "
    .     "DATE_FORMAT(reportdata.D_1_05_3, '%Y/%m/%d') AS D_1_05_3ymd,"
    .     "reportdata.D_1_05_4, "
    .     "reportdata.D_1_06_1, "
    .     "reportdata.D_1_06_2, "
    .     "DATE_FORMAT(reportdata.D_1_06_3, '%Y/%m/%d') AS D_1_06_3ymd,"
    .     "reportdata.D_1_06_4, "
    .     "reportdata.D_1_07_1, "
    .     "reportdata.D_1_07_2, "
    .     "DATE_FORMAT(reportdata.D_1_07_3, '%Y/%m/%d') AS D_1_07_3ymd,"
    .     "reportdata.D_1_07_4, "
    .     "reportdata.D_1_08_01, "
    .     "reportdata.D_1_08_02, "
    .     "DATE_FORMAT(reportdata.D_1_08_03, '%Y/%m/%d') AS D_1_08_03ymd,"
    .     "reportdata.D_1_08_04, "
    .     "DATE_FORMAT(reportdata.D_1_08_05, '%Y/%m/%d') AS D_1_08_05ymd,"
    .     "reportdata.D_1_08_06, "
    .     "DATE_FORMAT(reportdata.D_1_08_07, '%Y/%m/%d') AS D_1_08_07ymd,"
    .     "reportdata.D_1_08_08, "
    .     "reportdata.D_1_08_09, "
    .     "reportdata.D_1_08_10, "
    .     "reportdata.D_1_09_1, "
    .     "reportdata.D_1_09_2, "
    .     "DATE_FORMAT(reportdata.D_1_09_3, '%Y/%m/%d') AS D_1_09_3ymd,"
    .     "reportdata.D_1_09_4, "
    .     "reportdata.D_1_10_1, "
    .     "reportdata.D_1_10_2, "
    .     "DATE_FORMAT(reportdata.D_1_10_3, '%Y/%m/%d') AS D_1_10_3ymd,"
    .     "reportdata.D_1_10_4, "
    .     "DATE_FORMAT(reportdata.D_1_10_5, '%Y/%m/%d') AS D_1_10_5ymd,"
    .     "reportdata.D_1_10_6, "
    .     "reportdata.D_1_10_7, "
    .     "DATE_FORMAT(reportdata.D_1_10_8, '%Y/%m/%d') AS D_1_10_8ymd,"
    .     "reportdata.D_1_10_9, "
    .     "reportdata.D_1_10_10, "
    .     "reportdata.D_1_10_11, "
    .     "reportdata.D_1_11_1, "
    .     "reportdata.D_1_11_2, "
    .     "DATE_FORMAT(reportdata.D_1_11_3, '%Y/%m/%d') AS D_1_11_3ymd,"
    .     "reportdata.D_1_11_4, "
    .     "reportdata.D_2_1_1, "
    .     "reportdata.D_2_1_2, "
    .     "reportdata.D_2_1_3, "
    .     "reportdata.D_3_1_1, "
    .     "reportdata.D_3_1_2, "
    .     "reportdata.D_3_1_3, "
    .     "reportdata.D_3_1_4, "
    .     "reportdata.target_period, "
    .     "reportdata.quarter "

    . "FROM tbl_report_003 reportdata "
    . "INNER JOIN mst_workers workers "
    . "ON reportdata.worker_id = workers.id "
    . "WHERE workers.worker_id = :worker_id "
    ;

    $params = array();
    $params[":worker_id"] = $workerId;

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}


/**
 * CSVデータを登録する。
 *
 * @param array[カラム] $row CSVデータ
 * @param string $workerId 労働者ID
 * 
 * @return boolean 実行成否
 */
function csvDataImport($row, $workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        // 労働者の行IDを取得。
        $workerRowId = getWorkerRowId($db, $workerId);

        // カラムと設定値を定義。
        $columns = array();
        setColumns($columns, $db, $row);

        $whereString = "worker_id = :worker_id ";
        $whereParams = array(
            "worker_id" => $workerRowId
        );

        // update処理を発行する。
        $db->update("tbl_report_003", $columns, $whereString, $whereParams);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}

/**
 * CSVレコードの内容から、更新用データを生成する。
 *
 * @param array[DBカラム名 => 値] $columns 更新用カラム一覧
 * @param array[値] $row CSV行データ
 */
function setColumns(&$columns, $db, $row) {
    $rowid = 1 - 1;

    $columns["D_1_01_1"      ] = $row[$rowid++];
    $columns["D_1_01_2"      ] = $row[$rowid++];
    $columns["D_1_01_3"      ] = setDateParam($row[$rowid++]);
    $columns["D_1_01_4"      ] = $row[$rowid++];
    $columns["D_1_02_1"      ] = $row[$rowid++];
    $columns["D_1_02_2"      ] = $row[$rowid++];
    $columns["D_1_02_3"      ] = setDateParam($row[$rowid++]);
    $columns["D_1_02_4"      ] = $row[$rowid++];
    $columns["D_1_03_1"      ] = $row[$rowid++];
    $columns["D_1_03_2"      ] = $row[$rowid++];
    $columns["D_1_03_3"      ] = setDateParam($row[$rowid++]);
    $columns["D_1_03_4"      ] = $row[$rowid++];
    $columns["D_1_04_1"      ] = $row[$rowid++];
    $columns["D_1_04_2"      ] = $row[$rowid++];
    $columns["D_1_04_3"      ] = setDateParam($row[$rowid++]);
    $columns["D_1_04_4"      ] = $row[$rowid++];
    $columns["D_1_05_1"      ] = $row[$rowid++];
    $columns["D_1_05_2"      ] = $row[$rowid++];
    $columns["D_1_05_3"      ] = setDateParam($row[$rowid++]);
    $columns["D_1_05_4"      ] = $row[$rowid++];
    $columns["D_1_06_1"      ] = $row[$rowid++];
    $columns["D_1_06_2"      ] = $row[$rowid++];
    $columns["D_1_06_3"      ] = setDateParam($row[$rowid++]);
    $columns["D_1_06_4"      ] = $row[$rowid++];
    $columns["D_1_07_1"      ] = $row[$rowid++];
    $columns["D_1_07_2"      ] = $row[$rowid++];
    $columns["D_1_07_3"      ] = setDateParam($row[$rowid++]);
    $columns["D_1_07_4"      ] = $row[$rowid++];
    $columns["D_1_08_01"     ] = $row[$rowid++];
    $columns["D_1_08_02"     ] = $row[$rowid++];
    $columns["D_1_08_03"     ] = setDateParam($row[$rowid++]);
    $columns["D_1_08_04"     ] = $row[$rowid++];
    $columns["D_1_08_05"     ] = setDateParam($row[$rowid++]);
    $columns["D_1_08_06"     ] = $row[$rowid++];
    $columns["D_1_08_07"     ] = setDateParam($row[$rowid++]);
    $columns["D_1_08_08"     ] = $row[$rowid++];
    $columns["D_1_08_09"     ] = $row[$rowid++];
    $columns["D_1_08_10"     ] = $row[$rowid++];
    $columns["D_1_09_1"      ] = $row[$rowid++];
    $columns["D_1_09_2"      ] = $row[$rowid++];
    $columns["D_1_09_3"      ] = setDateParam($row[$rowid++]);
    $columns["D_1_09_4"      ] = $row[$rowid++];
    $columns["D_1_10_1"      ] = $row[$rowid++];
    $columns["D_1_10_2"      ] = $row[$rowid++];
    $columns["D_1_10_3"      ] = setDateParam($row[$rowid++]);
    $columns["D_1_10_4"      ] = $row[$rowid++];
    $columns["D_1_10_5"      ] = setDateParam($row[$rowid++]);
    $columns["D_1_10_6"      ] = $row[$rowid++];
    $columns["D_1_10_7"      ] = $row[$rowid++];
    $columns["D_1_10_8"      ] = setDateParam($row[$rowid++]);
    $columns["D_1_10_9"      ] = $row[$rowid++];
    $columns["D_1_10_10"     ] = $row[$rowid++];
    $columns["D_1_10_11"     ] = $row[$rowid++];
    $columns["D_1_11_1"      ] = $row[$rowid++];
    $columns["D_1_11_2"      ] = $row[$rowid++];
    $columns["D_1_11_3"      ] = setDateParam($row[$rowid++]);
    $columns["D_1_11_4"      ] = $row[$rowid++];
    $columns["D_2_1_1"       ] = $row[$rowid++];
    $columns["D_2_1_2"       ] = $row[$rowid++];
    $columns["D_2_1_3"       ] = $row[$rowid++];
    $columns["D_3_1_1"       ] = $row[$rowid++];
    $columns["D_3_1_2"       ] = $row[$rowid++];
    $columns["D_3_1_3"       ] = $row[$rowid++];
    $columns["D_3_1_4"       ] = $row[$rowid++];
    $columns["target_period" ] = $row[$rowid++];
    $columns["quarter"       ] = $row[$rowid++];

}

/**
 * 労働者の行IDを取得する。
 *
 * @param PDO $db DB接続
 * @param string $workerId 取得対象の労働者ID
 * 
 * @return integer 行ID
 */
function getWorkerRowId(&$db, $workerId) {
    $sql = "SELECT id AS rowid "
         . "FROM mst_workers "
         . "WHERE worker_id = :worker_id ";
    $params[":worker_id"] = $workerId;
    $result = $db->selectOne($sql, $params);
    return $result["rowid"];
}
