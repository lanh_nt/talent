<?php

/**
 * 労働者のシステム情報を取得し、結果を返す。
 *
 * @param $workerId 労働者ID
 * @param $labels 　ラベル情報
 * 
 * @return array[] 取得結果
 */
function search($workerId, $labels) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         .     "workers.id, "
         .     "workers.worker_id, "
         .     "workers.status, "
         .     "workers.qualifications "
                  . "FROM mst_workers workers "
         . "WHERE workers.worker_id = :worker_id "
         ;

    $params = array();
    $params[":worker_id"] = $workerId;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 表示用データを追加設定。
    $qualification         = getOptionItems("comm", "qualification");
    $working_status       = getOptionItems("comm", "working_status");
    
        // コードに対応する文言を設定。
        $result["qualifications"]  = getOprionItemValue($qualification,  $result["qualifications"]);
        $result["status"]          = getOprionItemValue($working_status, $result["status"]);
    
    
    // 取得結果を返す。
    return $result;
}

/**
 * 登録処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function update($workerId, $items) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {

        // パスワード設定値を生成。
        $passValue = password_hash($items["password"], PASSWORD_DEFAULT);

        // カラムと設定値を定義。
        $columns["password"] = $passValue;

        $whereString = "worker_id = :worker_id ";
        $whereParams = array(
            "worker_id" => $workerId
        );
        
        // update処理を発行する。
        $db->update("mst_workers", $columns, $whereString, $whereParams);
        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}
