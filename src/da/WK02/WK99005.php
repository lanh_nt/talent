<?php

/**
 * 「支援実施状況に係る届出」の帳票情報を取得し、結果を返す。
 *
 * @param $targetWorkerId 労働者ID
 * 
 * @return array[] 取得結果
 */

function makePdfData05($targetWorkerId){
    //-----------------------------------------
    // DBから支援実施状況に係る届出の情報を取得する。
    //-----------------------------------------
    $row = searchPDF05($targetWorkerId);

    //-----------------------------------------
    // DBの情報から帳票情報を作成する。
    //-----------------------------------------
    $datas = Array();

    $cp_num = arraySplit($row['corporation_no'], 13);
    $datas['label_01'] = $row['target_period'];     //届出の対象期間
    $datas['label_02'] = $row['quarter'];           //四半期            
    $datas['01_01_01'] = $cp_num[ 0];               //特定技能所属機関：法人番号：1
    $datas['01_01_02'] = $cp_num[ 1];               //特定技能所属機関：法人番号：2
    $datas['01_01_03'] = $cp_num[ 2];               //特定技能所属機関：法人番号：3
    $datas['01_01_04'] = $cp_num[ 3];               //特定技能所属機関：法人番号：4
    $datas['01_01_05'] = $cp_num[ 4];               //特定技能所属機関：法人番号：5
    $datas['01_01_06'] = $cp_num[ 5];               //特定技能所属機関：法人番号：6
    $datas['01_01_07'] = $cp_num[ 6];               //特定技能所属機関：法人番号：7
    $datas['01_01_08'] = $cp_num[ 7];               //特定技能所属機関：法人番号：8
    $datas['01_01_09'] = $cp_num[ 8];               //特定技能所属機関：法人番号：9
    $datas['01_01_10'] = $cp_num[ 9];               //特定技能所属機関：法人番号：10
    $datas['01_01_11'] = $cp_num[10];               //特定技能所属機関：法人番号：11
    $datas['01_01_12'] = $cp_num[11];               //特定技能所属機関：法人番号：12
    $datas['01_01_13'] = $cp_num[12];               //特定技能所属機関：法人番号：13
    $datas['01_02_01'] = $row['company_kana'];      //特定技能所属機関：氏名（ふりがな）
    $datas['01_02_02'] = $row['company_name'];      //特定技能所属機関：氏名
    
    //特定技能所属機関：郵便番号の「ー」分割　★★★            
    $postalAry = preg_split("/[-]/",$row['postal_code_cp']);
    if(count($postalAry)==1){
        $datas['01_03_02'] = $postalAry[0];         //郵便番号
    }elseif(count($postalAry)==2){
        $datas['01_03_01'] = $postalAry[0];         //郵便番号
        $datas['01_03_02'] = $postalAry[1];         //郵便番号
    }
    $datas['01_03_03'] = $row['address_cp'];        //特定技能所属機関：住所：住所
    //特定技能所属機関：電話番号の「ー」分割　★★★
    $telAry = preg_split("/[-]/",$row['tel']);
    if(count($telAry)==1){
        $datas['01_03_06'] = $telAry[0];            //電話番号：市外局番
    }elseif(count($telAry)==2){
        $datas['01_03_05'] = $telAry[0];            //電話番号：市外局番
        $datas['01_03_06'] = $telAry[1];            //電話番号：市外局番
    }elseif(count($telAry)>=3){
        $datas['01_03_04'] = $telAry[0];            //電話番号：市外局番
        $datas['01_03_05'] = $telAry[1];            //電話番号：市外局番
        $datas['01_03_06'] = $telAry[2];            //電話番号：市外局番
    }
    //特定技能外国人：郵便番号の「ー」分割　★★★
    $postalAry = preg_split("/[-]/",$row['postal_code_wk']);
    if(count($postalAry)==1){
        $datas['02_05_02'] = $postalAry[0];         //郵便番号
    }elseif(count($postalAry)==2){
        $datas['02_05_01'] = $postalAry[0];         //郵便番号
        $datas['02_05_02'] = $postalAry[1];         //郵便番号
    }
    $datas['02_05_03'] = $row['address_wk'];        //1号特定技能外国人：居住地：住所
    //特定技能外国人：電話番号の「ー」分割　★★★
    $telAry = preg_split("/[-]/",$row['telephone']);
    if(count($telAry)==1){
        $datas['02_05_06'] = $telAry[0];            //電話番号：市外局番
    }elseif(count($telAry)==2){
        $datas['02_05_05'] = $telAry[0];            //電話番号：市外局番
        $datas['02_05_06'] = $telAry[1];            //電話番号：市外局番
    }elseif(count($telAry)>=3){
        $datas['02_05_04'] = $telAry[0];            //電話番号：市外局番
        $datas['02_05_05'] = $telAry[1];            //電話番号：市外局番
        $datas['02_05_06'] = $telAry[2];            //電話番号：市外局番
    }
    $datas['02_01'] = $row['user_name_e'];          //1号特定技能外国人：氏名（ローマ字）
    if( strcmp(  strval($row['sex']) , "1") == 0 ){
        $datas['02_02_male'] = $row['sex'];         //1号特定技能外国人：性別：男
    }
    if( strcmp(  strval($row['sex']) , "2") == 0 ){
    $datas['02_02_femal'] = $row['sex'];            //1号特定技能外国人：性別：女
    }
    if(isset($row['birthday'])){
        $birthday  = $row['birthday_Y']."年";
        $birthday .= $row['birthday_M']."月";
        $birthday .= $row['birthday_D']."日";
        $datas['02_03'] = $birthday;                //1号特定技能外国人：生年月日
    }
    $datas['02_04'] = $row['nationality_region'];   //1号特定技能外国人：国籍・地域
    $datas['02_06'] = $row['A_10'];                 //1号特定技能外国人：在留カード番号
    if( strcmp(  strval($row['D_1_01_1']) , "1") == 0 ){
        $datas['03_01_01'] = $row['D_1_01_1'];      //3-1：実施
    }
    if( strcmp(  strval($row['D_1_01_2']) , "1") == 0 ){
        $datas['03_01_02'] = $row['D_1_01_2'];      //3-1：実施　当初
    }
    if( strcmp(  strval($row['D_1_01_2']) , "2") == 0 ){
        $datas['03_01_03'] = $row['D_1_01_2'];      //3-1：実施　変更後
    }
        $datas['03_01_04'] = $row['D_1_01_3_Y'];    //3-1：実施　届出日：年
        $datas['03_01_05'] = $row['D_1_01_3_M'];    //3-1：実施　届出日：月
        $datas['03_01_06'] = $row['D_1_01_3_D'];    //3-1：実施　届出日：日
    
    if( strcmp(  strval($row['D_1_01_1']) , "2") == 0 ){
        $datas['03_01_07'] = $row['D_1_01_1'];      //3-1：未実施
    }
    $datas['03_01_08'] = $row['D_1_01_4'];          //3-1：未実施　理由
    if( strcmp(  strval($row['D_1_01_1']) , "3") == 0 ){
        $datas['03_01_09'] = $row['D_1_01_1'];      //3-1：支援対象者なし
    }
    if( strcmp(  strval($row['D_1_02_1']) , "1") == 0 ){
        $datas['03_02_01'] = $row['D_1_02_1'];      //3-2：実施
    }
    if( strcmp(  strval($row['D_1_02_2']) , "1") == 0 ){
        $datas['03_02_02'] = $row['D_1_02_2'];      //3-2：実施　当初
    }
    if( strcmp(  strval($row['D_1_02_2']) , "2") == 0 ){
        $datas['03_02_03'] = $row['D_1_02_2'];      //3-2：実施　変更後
    }
    $datas['03_02_04'] = $row['D_1_02_3_Y'];        //3-2：届出日：年
    $datas['03_02_05'] = $row['D_1_02_3_M'];        //3-2：届出日：月
    $datas['03_02_06'] = $row['D_1_02_3_D'];        //3-2：届出日：日
    if( strcmp(  strval($row['D_1_02_1']) , "2") == 0 ){
        $datas['03_02_07'] = $row['D_1_02_1'];      //3-2：未実施
    }
    $datas['03_02_08'] = $row['D_1_02_4'];          //3-2：理由
    if( strcmp(  strval($row['D_1_02_1']) , "3") == 0 ){
        $datas['03_02_09'] = $row['D_1_02_1'];      //3-2：支援対象者なし
    }
    if( strcmp(  strval($row['D_1_03_1']) , "1") == 0 ){
        $datas['03_03_01'] = $row['D_1_03_1'];      //3-3：実施
    }
    if( strcmp(  strval($row['D_1_03_2']) , "1") == 0 ){
        $datas['03_03_02'] = $row['D_1_03_2'];      //3-3：実施　当初
    }
    if( strcmp(  strval($row['D_1_03_2']) , "2") == 0 ){
        $datas['03_03_03'] = $row['D_1_03_2'];      //3-3：実施　変更後
    }
    $datas['03_03_04'] = $row['D_1_03_3_Y'];        //3-3：実施　届出日：年
    $datas['03_03_05'] = $row['D_1_03_3_M'];        //3-3：実施　届出日：月
    $datas['03_03_06'] = $row['D_1_03_3_D'];        //3-3：実施　届出日：日
    if( strcmp(  strval($row['D_1_03_1']) , "2") == 0 ){
        $datas['03_03_07'] = $row['D_1_03_1'];      //3-3：未実施
    }
    $datas['03_03_08'] = $row['D_1_03_4'];          //3-3：未実施　理由
    if( strcmp(  strval($row['D_1_03_1']) , "3") == 0 ){
        $datas['03_03_09'] = $row['D_1_03_1'];      //3-3：支援対象者なし
    }
    if( strcmp(  strval($row['D_1_04_1']) , "1") == 0 ){
        $datas['03_04_01'] = $row['D_1_04_1'];      //3-4：実施
    }
    if( strcmp(  strval($row['D_1_04_2']) , "1") == 0 ){
        $datas['03_04_02'] = $row['D_1_04_2'];      //3-4：実施　当初
    }
    if( strcmp(  strval($row['D_1_04_2']) , "2") == 0 ){
        $datas['03_04_03'] = $row['D_1_04_2'];      //3-4：実施　変更後
    }
    $datas['03_04_04'] = $row['D_1_04_3_Y'];        //3-4：実施　届出日：年
    $datas['03_04_05'] = $row['D_1_04_3_M'];        //3-4：実施　届出日：月
    $datas['03_04_06'] = $row['D_1_04_3_D'];        //3-4：実施　届出日：日
    if( strcmp(  strval($row['D_1_04_1']) , "2") == 0 ){
        $datas['03_04_07'] = $row['D_1_04_1'];      //3-4：未実施
    }
    $datas['03_04_08'] = $row['D_1_04_4'];          //3-4：未実施　理由
    if( strcmp(  strval($row['D_1_04_1']) , "3") == 0 ){
        $datas['03_04_09'] = $row['D_1_04_1'];      //3-4：支援対象者なし
    }
    if( strcmp(  strval($row['D_1_05_1']) , "1") == 0 ){
        $datas['03_05_01'] = $row['D_1_05_1'];      //3-5：実施
    }
    if( strcmp(  strval($row['D_1_05_2']) , "1") == 0 ){
        $datas['03_05_02'] = $row['D_1_05_2'];      //3-5：実施　当初
    }
    if( strcmp(  strval($row['D_1_05_2']) , "2") == 0 ){
        $datas['03_05_03'] = $row['D_1_05_2'];      //3-5：実施　変更後
    }
    $datas['03_05_04'] = $row['D_1_05_3_Y'];        //3-5：実施　届出日：年
    $datas['03_05_05'] = $row['D_1_05_3_M'];        //3-5：実施　届出日：月
    $datas['03_05_06'] = $row['D_1_05_3_D'];        //3-5：実施　届出日：日
    if( strcmp(  strval($row['D_1_05_1']) , "2") == 0 ){
        $datas['03_05_07'] = $row['D_1_05_1'];      //3-5：未実施
    }
    $datas['03_05_08'] = $row['D_1_05_4'];          //3-5：未実施　理由
    if( strcmp(  strval($row['D_1_05_1']) , "3") == 0 ){
        $datas['03_05_09'] = $row['D_1_05_1'];      //3-5：支援対象者なし
    }
    if( strcmp(  strval($row['D_1_06_1']) , "1") == 0 ){
        $datas['03_06_01'] = $row['D_1_06_1'];      //3-6：実施
    }
    if( strcmp(  strval($row['D_1_06_2']) , "1") == 0 ){
        $datas['03_06_02'] = $row['D_1_06_2'];      //3-6：実施　当初
    }
    if( strcmp(  strval($row['D_1_06_2']) , "2") == 0 ){
        $datas['03_06_03'] = $row['D_1_06_2'];      //3-6：実施　変更後
    }
    $datas['03_06_04'] = $row['D_1_06_3_Y'];        //3-6：実施　届出日：年
    $datas['03_06_05'] = $row['D_1_06_3_M'];        //3-6：実施　届出日：月
    $datas['03_06_06'] = $row['D_1_06_3_D'];        //3-6：実施　届出日：日
    if( strcmp(  strval($row['D_1_06_1']) , "2") == 0 ){
        $datas['03_06_07'] = $row['D_1_06_1'];      //3-6：未実施
    }
    $datas['03_06_08'] = $row['D_1_06_4'];          //3-6：未実施　理由
    if( strcmp(  strval($row['D_1_06_1']) , "3") == 0 ){
        $datas['03_06_09'] = $row['D_1_06_1'];      //3-6：支援対象者なし
    }
    if( strcmp(  strval($row['D_1_07_1']) , "1") == 0 ){
        $datas['03_07_01'] = $row['D_1_07_1'];      //3-7：実施
    }
    if( strcmp(  strval($row['D_1_07_2']) , "1") == 0 ){
        $datas['03_07_02'] = $row['D_1_07_2'];      //3-7：実施　当初
    }
    if( strcmp(  strval($row['D_1_07_2']) , "2") == 0 ){
        $datas['03_07_03'] = $row['D_1_07_2'];      //3-7：実施　変更後
    }
    $datas['03_07_04'] = $row['D_1_07_3_Y'];        //3-7：実施　届出日：年
    $datas['03_07_05'] = $row['D_1_07_3_M'];        //3-7：実施　届出日：月
    $datas['03_07_06'] = $row['D_1_07_3_D'];        //3-7：実施　届出日：日
    if( strcmp(  strval($row['D_1_07_1']) , "2") == 0 ){
        $datas['03_07_07'] = $row['D_1_07_1'];      //3-7：未実施
    }
    $datas['03_07_08'] = $row['D_1_07_4'];          //3-7：未実施　理由
    if( strcmp(  strval($row['D_1_07_1']) , "3") == 0 ){
        $datas['03_07_09'] = $row['D_1_07_1'];      //3-7：支援対象者なし
    
    }
    if( strcmp(  strval($row['D_1_08_01']) , "1") == 0 ){
        $datas['03_08_01_01'] = $row['D_1_08_01'];  //3-8-1：実施
    }
    if( strcmp(  strval($row['D_1_08_02']) , "1") == 0 ){
        $datas['03_08_01_02'] = $row['D_1_08_02'];  //3-8-1：実施　当初
    }
    if( strcmp(  strval($row['D_1_08_02']) , "2") == 0 ){
        $datas['03_08_01_03'] = $row['D_1_08_02'];  //3-8-1：実施　変更後
    }
    $datas['03_08_01_04'] = $row['D_1_08_03_Y'];    //3-8-1：実施　届出日：年
    $datas['03_08_01_05'] = $row['D_1_08_03_M'];    //3-8-1：実施　届出日：月
    $datas['03_08_01_06'] = $row['D_1_08_03_D'];    //3-8-1：実施　届出日：日
    if( strcmp(  strval($row['D_1_08_01']) , "2") == 0 ){
        $datas['03_08_01_07'] = $row['D_1_08_01'];  //3-8-1：未実施
    }
    $datas['03_08_01_08'] = $row['D_1_08_04'];      //3-8-1：未実施　理由
    if( strcmp(  strval($row['D_1_08_01']) , "3") == 0 ){
        $datas['03_08_01_09'] = $row['D_1_08_01'];  //3-8-1：支援対象者なし
    }            
    $datas['03_08_02_01'] = $row['D_1_08_05_Y'];    //3-8-2：受理日：年
    $datas['03_08_02_02'] = $row['D_1_08_05_M'];    //3-8-2：受理日：月
    $datas['03_08_02_03'] = $row['D_1_08_05_D'];    //3-8-2：受理日：日
    $datas['03_08_02_04'] = $row['D_1_08_06'];      //3-8-2：相談内容
    $datas['03_08_02_05'] = $row['D_1_08_07_Y'];    //3-8-2：相談又は通報日：年
    $datas['03_08_02_06'] = $row['D_1_08_07_M'];    //3-8-2：相談又は通報日：月
    $datas['03_08_02_07'] = $row['D_1_08_07_D'];    //3-8-2：相談又は通報日：日
    $datas['03_08_02_08'] = $row['D_1_08_08'];      //3-8-2：相談又は通報先の名称
    $datas['03_08_02_09'] = $row['D_1_08_09'];      //3-8-2：対応結果
    $datas['03_08_02_10'] = $row['D_1_08_10'];      //3-8-2：対応者
    if( strcmp(  strval($row['D_1_09_1']) , "1") == 0 ){
        $datas['03_09_01'] = $row['D_1_09_1'];      //3-9：実施
    }
    if( strcmp(  strval($row['D_1_09_2']) , "1") == 0 ){
        $datas['03_09_02'] = $row['D_1_09_2'];      //3-9：未実施　当初
    }
    if( strcmp(  strval($row['D_1_09_2']) , "2") == 0 ){
        $datas['03_09_03'] = $row['D_1_09_2'];      //3-9：未実施　変更後
    }
    $datas['03_09_04'] = $row['D_1_09_3_Y'];        //3-9：届出日：年
    $datas['03_09_05'] = $row['D_1_09_3_M'];        //3-9：届出日：月
    $datas['03_09_06'] = $row['D_1_09_3_D'];        //3-9：届出日：日
    if( strcmp(  strval($row['D_1_09_1']) , "2") == 0 ){
        $datas['03_09_07'] = $row['D_1_09_1'];      //3-9：未実施
    }
    $datas['03_09_08'] = $row['D_1_09_4'];          //3-9：未実施　理由
    if( strcmp(  strval($row['D_1_09_1']) , "3") == 0 ){
        $datas['03_09_09'] = $row['D_1_09_1'];      //3-9：支援対象者なし
    }
    if( strcmp(  strval($row['D_1_10_1']) , "1") == 0 ){
        $datas['03_10_01_01'] = $row['D_1_10_1'];   //3-10-1：実施
    }
    if( strcmp(  strval($row['D_1_10_2']) , "1") == 0 ){
        $datas['03_10_01_02'] = $row['D_1_10_2'];   //3-10-1：未実施　当初
    }
    if( strcmp(  strval($row['D_1_10_2']) , "2") == 0 ){
        $datas['03_10_01_03'] = $row['D_1_10_2'];   //3-10-1：未実施　変更後
    }
    $datas['03_10_01_04'] = $row['D_1_10_3_Y'];     //3-10-1：実施　届出日：年
    $datas['03_10_01_05'] = $row['D_1_10_3_M'];     //3-10-1：実施　届出日：月
    $datas['03_10_01_06'] = $row['D_1_10_3_D'];     //3-10-1：実施　届出日：日
    if( strcmp(  strval($row['D_1_10_1']) , "2") == 0 ){
        $datas['03_10_01_07'] = $row['D_1_10_1'];   //3-10-1：未実施
    }
    $datas['03_10_01_08'] = $row['D_1_10_4'];       //3-10-1：理由
    if( strcmp(  strval($row['D_1_10_1']) , "3") == 0 ){
        $datas['03_10_01_09'] = $row['D_1_10_1'];   //3-10-1：支援対象者なし
    }
    $datas['03_10_02_01'] = $row['D_1_10_5_Y'];     //3-10-2：支援日：年
    $datas['03_10_02_02'] = $row['D_1_10_5_M'];     //3-10-2：支援日：月
    $datas['03_10_02_03'] = $row['D_1_10_5_D'];     //3-10-2：支援日：日
    $datas['03_10_02_04'] = $row['D_1_10_6'];       //3-10-2：支援の内容
    if( strcmp(  strval($row['D_1_10_7']) , "1") == 0 ){
        $datas['03_10_02_05_yes'] = $row['D_1_10_7'];//3-10-1：利用有無：有
    }
    if( strcmp(  strval($row['D_1_10_7']) , "2") == 0 ){
        $datas['03_10_02_05_no'] = $row['D_1_10_7']; //3-10-1：利用有無：無
    }
    $datas['03_10_02_06'] = $row['D_1_10_8_Y'];     //3-10-2：相談日：年
    $datas['03_10_02_07'] = $row['D_1_10_8_M'];     //3-10-2：相談日：月
    $datas['03_10_02_08'] = $row['D_1_10_8_D'];     //3-10-2：相談日：日
    $datas['03_10_02_09'] = $row['D_1_10_9'];       //3-10-2：公共職業安定所の名称
    $datas['03_10_02_10'] = $row['D_1_10_10'];      //3-10-2：対応結果
    $datas['03_10_02_11'] = $row['D_1_10_11'];      //3-10-2：対応者
    if( strcmp(  strval($row['D_1_11_1']) , "1") == 0 ){
        $datas['03_11_01'] = $row['D_1_11_1'];      //3-11：実施
    }
    if( strcmp(  strval($row['D_1_11_2']) , "1") == 0 ){
        $datas['03_11_02'] = $row['D_1_11_2'];      //3-11：実施　当初
    }
    if( strcmp(  strval($row['D_1_11_1']) , "1") == 0 ){
        $datas['03_11_01'] = $row['D_1_11_1'];      //3-11：実施
    }
    if( strcmp(  strval($row['D_1_11_2']) , "1") == 0 ){
        $datas['03_11_02'] = $row['D_1_11_2'];      //3-11：実施　当初
    }
    if( strcmp(  strval($row['D_1_11_2']) , "2") == 0 ){
        $datas['03_11_03'] = $row['D_1_11_2'];      //3-11：実施　変更後
    }
    $datas['03_11_04'] = $row['D_1_11_3_Y'];        //3-11：実施　届出日：年
    $datas['03_11_05'] = $row['D_1_11_3_M'];        //3-11：実施　届出日：月
    $datas['03_11_06'] = $row['D_1_11_3_D'];        //3-11：実施　届出日：日
    if( strcmp(  strval($row['D_1_11_1']) , "2") == 0 ){
        $datas['03_11_07'] = $row['D_1_11_1'];      //3-11：未実施
    }
    $datas['03_11_08'] = $row['D_1_11_4'];          //3-11：未実施　理由
    if( strcmp(  strval($row['D_1_11_1']) , "3") == 0 ){
        $datas['03_11_09'] = $row['D_1_11_1'];      //3-11：支援対象者なし
    }
    
    return $datas;
}

/**
 * DBから「支援実施状況に係る届出書」の情報を取得し、結果を返す。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function searchPDF05($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    //取得に必要な労働者rowIDを取得する
    $WorkerRowId = $db->getWorkerRowId($workerId);

    $select_sql_str  = "SELECT mst_workers.*,mst_company.*,tbl_report_003.* ";        
     
    //特定技能所属機関：郵便番号
    $select_sql_str  .= ",mst_company.postal_code  as postal_code_cp ";
    //特定技能所属機関：住所
    $select_sql_str  .= ",mst_company.address  as address_cp ";
    //特定技能外国人：郵便番号
    $select_sql_str  .= ",mst_workers.postal_code  as postal_code_wk ";
    //特定技能外国人：住所
    $select_sql_str  .= ",mst_workers.address  as address_wk ";
    //特定技能外国人：生年月日
    $select_sql_str  .= ",date_format(birthday,'%Y')  as birthday_Y ";
    $select_sql_str  .= ",date_format(birthday,'%c')  as birthday_M ";
    $select_sql_str  .= ",date_format(birthday,'%e')  as birthday_D ";
    //事前ガイダンス　　届出日
    $select_sql_str  .= ",date_format(D_1_01_3,'%Y')  as D_1_01_3_Y ";
    $select_sql_str  .= ",date_format(D_1_01_3,'%c')  as D_1_01_3_M ";
    $select_sql_str  .= ",date_format(D_1_01_3,'%e')  as D_1_01_3_D ";
    //空港等への出迎え　届出日
    $select_sql_str  .= ",date_format(D_1_02_3,'%Y')  as D_1_02_3_Y ";
    $select_sql_str  .= ",date_format(D_1_02_3,'%c')  as D_1_02_3_M ";
    $select_sql_str  .= ",date_format(D_1_02_3,'%e')  as D_1_02_3_D ";
    //空港等への見送り　届出日
    $select_sql_str  .= ",date_format(D_1_03_3,'%Y')  as D_1_03_3_Y ";
    $select_sql_str  .= ",date_format(D_1_03_3,'%c')  as D_1_03_3_M ";
    $select_sql_str  .= ",date_format(D_1_03_3,'%e')  as D_1_03_3_D ";
    //住居の確保・生活に必要な契約に関する支援　　届出日
    $select_sql_str  .= ",date_format(D_1_04_3,'%Y')  as D_1_04_3_Y ";
    $select_sql_str  .= ",date_format(D_1_04_3,'%c')  as D_1_04_3_M ";
    $select_sql_str  .= ",date_format(D_1_04_3,'%e')  as D_1_04_3_D ";
    //生活オリエンテーション　届出日
    $select_sql_str  .= ",date_format(D_1_05_3,'%Y')  as D_1_05_3_Y ";
    $select_sql_str  .= ",date_format(D_1_05_3,'%c')  as D_1_05_3_M ";
    $select_sql_str  .= ",date_format(D_1_05_3,'%e')  as D_1_05_3_D ";
    //関係機関への同行その他必要な支援　　届出日
    $select_sql_str  .= ",date_format(D_1_06_3,'%Y')  as D_1_06_3_Y ";
    $select_sql_str  .= ",date_format(D_1_06_3,'%c')  as D_1_06_3_M ";
    $select_sql_str  .= ",date_format(D_1_06_3,'%e')  as D_1_06_3_D ";
    //日本語学習の機会の提供　　届出日
    $select_sql_str  .= ",date_format(D_1_07_3,'%Y')  as D_1_07_3_Y ";
    $select_sql_str  .= ",date_format(D_1_07_3,'%c')  as D_1_07_3_M ";
    $select_sql_str  .= ",date_format(D_1_07_3,'%e')  as D_1_07_3_D ";
    //相談・苦情対応　　実施状況　届出日
    $select_sql_str  .= ",date_format(D_1_08_03,'%Y')  as D_1_08_03_Y ";
    $select_sql_str  .= ",date_format(D_1_08_03,'%c')  as D_1_08_03_M ";
    $select_sql_str  .= ",date_format(D_1_08_03,'%e')  as D_1_08_03_D ";
    //相談・苦情対応　　相談受理日　届出日
    $select_sql_str  .= ",date_format(D_1_08_05,'%Y')  as D_1_08_05_Y ";
    $select_sql_str  .= ",date_format(D_1_08_05,'%c')  as D_1_08_05_M ";
    $select_sql_str  .= ",date_format(D_1_08_05,'%e')  as D_1_08_05_D ";
    //相談・苦情対応　　関係行政機関への相談又は通報日　届出日
    $select_sql_str  .= ",date_format(D_1_08_07,'%Y')  as D_1_08_07_Y ";
    $select_sql_str  .= ",date_format(D_1_08_07,'%c')  as D_1_08_07_M ";
    $select_sql_str  .= ",date_format(D_1_08_07,'%e')  as D_1_08_07_D ";
    //日本人との交流促進　　届出日
    $select_sql_str  .= ",date_format(D_1_09_3,'%Y')  as D_1_09_3_Y ";
    $select_sql_str  .= ",date_format(D_1_09_3,'%c')  as D_1_09_3_M ";
    $select_sql_str  .= ",date_format(D_1_09_3,'%e')  as D_1_09_3_D ";
    //非自発的離職時の転職支援　　届出日
    $select_sql_str  .= ",date_format(D_1_10_3,'%Y')  as D_1_10_3_Y ";
    $select_sql_str  .= ",date_format(D_1_10_3,'%c')  as D_1_10_3_M ";
    $select_sql_str  .= ",date_format(D_1_10_3,'%e')  as D_1_10_3_D ";
    //非自発的離職時の転職支援　転職支援日
    $select_sql_str  .= ",date_format(D_1_10_5,'%Y')  as D_1_10_5_Y ";
    $select_sql_str  .= ",date_format(D_1_10_5,'%c')  as D_1_10_5_M ";
    $select_sql_str  .= ",date_format(D_1_10_5,'%e')  as D_1_10_5_D ";
    //非自発的離職時の転職支援　公共職業安定所への相談日　届出日
    $select_sql_str  .= ",date_format(D_1_10_8,'%Y')  as D_1_10_8_Y ";
    $select_sql_str  .= ",date_format(D_1_10_8,'%c')  as D_1_10_8_M ";
    $select_sql_str  .= ",date_format(D_1_10_8,'%e')  as D_1_10_8_D ";
    //定期的な面談の実施　　届出日
    $select_sql_str  .= ",date_format(D_1_11_3,'%Y')  as D_1_11_3_Y ";
    $select_sql_str  .= ",date_format(D_1_11_3,'%c')  as D_1_11_3_M ";
    $select_sql_str  .= ",date_format(D_1_11_3,'%e')  as D_1_11_3_D ";

    $select_sql_str  .= "FROM mst_workers ";
    $select_sql_str  .= "left join tbl_report_003 on tbl_report_003.worker_id = mst_workers.id ";
    $select_sql_str  .= "left join mst_company on mst_company.id = mst_workers.company_id ";
    $select_sql_str  .= "WHERE mst_workers.id=:worker_rowid ";

    $params = array();
    $params[":worker_rowid"] = $WorkerRowId;

    $result = $db->selectone($select_sql_str, $params);

    if (count($result) == 0) {
        // レコードが取得できなかった場合、データ不整備エラーとする。
        $errMessage = getCommonMessage("EC004");
        goErrorPage($errMessage);
    }

    // 取得結果を返す。
    return $result;
}
