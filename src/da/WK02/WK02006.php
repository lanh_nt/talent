<?php

/**
 * 労働者の事前ガイダンスの確認書情報を取得し、結果を返す。
 *
 * @param $workerId 労働者ID
 * @param $label 　ラベル情報
 * 
 * @return array[] 取得結果
 */
function search($workerId, $label) {
    $db = new JinzaiDb(DB_DEFINE);


    // SQLを生成。
    $sql = "SELECT "
    .     "DATE_FORMAT(reportdata.impl1_from, '%Y/%m/%d') AS impl1_from_date, "
    .     "DATE_FORMAT(reportdata.impl1_from, '%k')       AS impl1_from_time, "
    .     "DATE_FORMAT(reportdata.impl1_from, '%i')       AS impl1_from_minute, "

    .     "DATE_FORMAT(reportdata.impl1_to, '%k')         AS impl1_to_time, "
    .     "DATE_FORMAT(reportdata.impl1_to, '%i')         AS impl1_to_minute, "
    
    .     "DATE_FORMAT(reportdata.impl2_from, '%Y/%m/%d') AS impl2_from_date, "
    .     "DATE_FORMAT(reportdata.impl2_from, '%k')       AS impl2_from_time, "
    .     "DATE_FORMAT(reportdata.impl2_from, '%i')       AS impl2_from_minute, "

    .     "DATE_FORMAT(reportdata.impl2_to, '%k')         AS impl2_to_time, "
    .     "DATE_FORMAT(reportdata.impl2_to, '%i')         AS impl2_to_minute, "

    .     "DATE_FORMAT(reportdata.impl3_from, '%Y/%m/%d') AS impl3_from_date, "
    .     "DATE_FORMAT(reportdata.impl3_from, '%k')       AS impl3_from_time, "
    .     "DATE_FORMAT(reportdata.impl3_from, '%i')       AS impl3_from_minute, "

    .     "DATE_FORMAT(reportdata.impl3_to, '%k')         AS impl3_to_time, "
    .     "DATE_FORMAT(reportdata.impl3_to, '%i')         AS impl3_to_minute "
    . "FROM tbl_report_004 reportdata "
    . "LEFT JOIN mst_workers workers "
    . "ON  reportdata.worker_id = workers.id "
    . "WHERE workers.worker_id = :worker_id "
    ;

    $params = array();
    $params[":worker_id"] = $workerId;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    if (count($result) == 0) {
        // レコードが取得できなかった場合、データ不整備エラーとする。
        $errMessage = getCommonMessage("EC004");
        goErrorPage($errMessage);
    }

    // 実施日時をセット
    $result["impl1"] = "";
    $result["impl2"] = "";
    $result["impl3"] = "";

    // 実施日時1
    if ($result["impl1_from_date"] != ""){
        $date     = setDateValue($result["impl1_from_date"], $label);
        $fromTime = setTimeValue($result["impl1_from_time"], $result["impl1_from_minute"], $label);
        $toTime   = setTimeValue($result["impl1_to_time"], $result["impl1_to_minute"], $label);

        $result["impl1"] = $date.$fromTime."  ".$label["from"]."  ".$toTime."  ".$label["to"];
    }
    // 実施日時2
    if ($result["impl2_from_date"] != ""){
        $date2     = setDateValue($result["impl2_from_date"], $label);
        $fromTime2 = setTimeValue($result["impl2_from_time"], $result["impl2_from_minute"], $label);
        $toTime2   = setTimeValue($result["impl2_to_time"], $result["impl2_to_minute"], $label);

        $result["impl2"] = $date2.$fromTime2."  ".$label["from"]."  ".$toTime2."  ".$label["to"];
    }
    // 実施日時3
    if ($result["impl3_from_date"] != ""){
        $date3     = setDateValue($result["impl3_from_date"], $label);
        $fromTime3 = setTimeValue($result["impl3_from_time"], $result["impl3_from_minute"], $label);
        $toTime3   = setTimeValue($result["impl3_to_time"], $result["impl3_to_minute"], $label);

        $result["impl3"] = $date3.$fromTime3."  ".$label["from"]."  ".$toTime3."  ".$label["to"];
    }
     
    // 取得結果を返す。
    return $result;
}

/**
 * 登録処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function update($workerId, $items) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        // 労働者の行IDを取得。
        $workerRowId = getWorkerRowId($db, $workerId);

        // カラムと設定値を定義。
        $impl1_from = setDate($items["impl1_from_date"], $items["impl1_from_time"], $items["impl1_from_minute"]);
        $impl2_from = setDate($items["impl2_from_date"], $items["impl2_from_time"], $items["impl2_from_minute"]);
        $impl3_from = setDate($items["impl3_from_date"], $items["impl3_from_time"], $items["impl3_from_minute"]);

        $impl1_to = setDate($items["impl1_from_date"], $items["impl1_to_time"], $items["impl1_to_minute"]);
        $impl2_to = setDate($items["impl2_from_date"], $items["impl2_to_time"], $items["impl2_to_minute"]);
        $impl3_to = setDate($items["impl3_from_date"], $items["impl3_to_time"], $items["impl3_to_minute"]);


        $columns["impl1_from"] = setDateParam($impl1_from);
        $columns["impl1_to"]   = setDateParam($impl1_to);
        $columns["impl2_from"] = setDateParam($impl2_from);
        $columns["impl2_to"]   = setDateParam($impl2_to);
        $columns["impl3_from"] = setDateParam($impl3_from);
        $columns["impl3_to"]   = setDateParam($impl3_to);

        $whereString = "worker_id = :worker_id ";
        $whereParams = array(
            "worker_id" => $workerRowId
        );
        
        // update処理を発行する。
        $db->update("tbl_report_004", $columns, $whereString, $whereParams);
        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}
/**
 * 労働者の行IDを取得する。
 *
 * @param PDO $db DB接続
 * @param string $workerId 取得対象の労働者ID
 * 
 * @return integer 行ID
 */
function getWorkerRowId(&$db, $workerId) {
    $sql = "SELECT id AS rowid "
         . "FROM mst_workers "
         . "WHERE worker_id = :worker_id ";
    $params[":worker_id"] = $workerId;
    $result = $db->selectOne($sql, $params);
    return $result["rowid"];
}

/**
 * 時間を返す。
 *
 * @return  $times
 */
function getTime() {
    $time = "0";
    $count = 24;
    $times = array();
    for ($i=0; $i < $count; $i++) {
        $str = sprintf('%02d', $time);
        $times[] = $str;  
        $time++;
    }
    return $times;
}
/**
 * 分を返す。
 *
 * @return $minutes
 */
function getMinute() {
    $minute = "0";
    $count = 6;
    $minutes = array();
    for ($i=0; $i < $count; $i++) {
        $str = sprintf('%02d', $minute);
        $minutes[] = $str;  
        $minute = $minute +10;
    }
    return $minutes;
}
/**
 * 日付をセットする。
 *
 * @param string $date   日付
 * @param string $time   時
 * @param string $minute 分
 * 
 * @return string $result 日付
 */
function setDate($date, $time,$minute) {
    $result ="";
    if ($date !="" && $time !="" && $minute !="") {
        // YYYY/MM/DDにセット
        $result = $date." ".$time.":".$minute.":00";
    }
    return $result;
}
/*
 * @param string $item　日付
 * @param array[] $label  ラベル
 * 
 * @return string $result
 */
function setDateValue($item,  $label) {
    $date = array();

    // 「/」を分割し、「まで」「から」のラベルを含む日付をセット
    $date = explode('/', $item);
    $result  = $date[0].$label["year"].$date[1].$label["month"].$date[2].$label["day"]." ";

    return $result;
}
/*
 * @param string $item　時
 * @param string $item2　分
 * @param string $labe  ラベル
 * 
 * @return string $result
 */
function setTimeValue($item,  $item2, $label) {

    $result  = $item.$label["time"].$item2.$label["minute"];

    return $result;
}