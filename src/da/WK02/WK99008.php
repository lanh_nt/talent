<?php

/**
 * 帳票：相談記録書の帳票出力用データを返す。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function getPageDatas08($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    //-----------------------------------------------
    // DBから対象データを取得。
    //-----------------------------------------------
    $sql = "SELECT "
         .     "DATE_FORMAT(report.response_date_from,'%c') AS response_date_from_Y, "
         .     "DATE_FORMAT(report.response_date_from,'%e') AS response_date_from_M, "
         .     "DATE_FORMAT(report.response_date_to,'%c') AS response_date_to_Y, "
         .     "DATE_FORMAT(report.response_date_to,'%e') AS response_date_to_M, "
         .     "companies.company_name, "
         .     "DATE_FORMAT(report.consultation_date,'%Y/%c/%e') AS consultation_YMD, "
         .     "workers.user_name_e, "
         .     "workers.sex, "
         .     "workers.nationality_region, "
         .     "DATE_FORMAT(workers.birthday,'%Y') AS birthday_Y, "
         .     "DATE_FORMAT(workers.birthday,'%c') AS birthday_M, "
         .     "DATE_FORMAT(workers.birthday,'%e') AS birthday_D, "
         .     "workers.A_10 AS residence_card_no, "
         .     "report.consultation_content, "
         .     "report.response_results, "
         .     "report.corresponding_person "
         . "FROM tbl_report_006 report "
         . "INNER JOIN mst_workers workers "
         .     "ON report.worker_id = workers.id "
         . "LEFT JOIN mst_company companies "
         .     "ON workers.company_id = companies.id "
         . "WHERE workers.worker_id = :worker_id "
    ;
    $params = array(
        ":worker_id" => $workerId
    );
    $result = $db->select($sql, $params);

    //-----------------------------------------------
    // 帳票出力用のデータを生成。
    //-----------------------------------------------
    $datas = convertPageDatas08($result);

    // 帳票出力用のデータを返す。
    return $datas;
}

/**
 * DBの検索結果を帳票出力用のデータに変換。
 * 
 * @param array $result DBの検索結果
 * 
 * @return array[ページデータ][項目名 => 値] 帳票出力用データ
 */
function convertPageDatas08($result) {
    $datas = Array();

    if (count($result) == 0) {
        // 取得データが存在しない場合は、空ページ出力の設定を返す。
        $datas[] = Array(
            '_templatePageNo' => 1,
            'label_06' => 1,
            'label_07' => '/',
            'label_08' => 1
        );
        return $datas;
    }

    $recordIndex = 0;
    $pageCount = 0;
    foreach ($result as $row) {
        $pageRowIndex = $recordIndex % 4;
        if ($pageRowIndex == 0) {
            // 新規ページデータを生成。
            $pageCount++;
            $datas[$pageCount] = array();

            // 使用するテンプレートのページを設定。
            $datas[$pageCount]['_templatePageNo'] = 1;

            // ヘッダ/フッタ部を作成。
            // ・対応日from
            $datas[$pageCount]['label_01'] = $row['response_date_from_Y'];
            $datas[$pageCount]['label_02'] = $row['response_date_from_M'];
            // ・対応日to
            $datas[$pageCount]['label_03'] = $row['response_date_to_Y'];
            $datas[$pageCount]['label_04'] = $row['response_date_to_M'];
            // ・機関の氏名又は名称
            $datas[$pageCount]['label_05'] = $row['company_name'];
            // ・ページ番号
            $datas[$pageCount]['label_06'] = $pageCount;
            $datas[$pageCount]['label_07'] = '/';
            $datas[$pageCount]['label_08'] = ceil(count($result) / 4);
        }

        // 各行のデータを作成。
        $rowPrefix = str_pad($pageRowIndex + 1, 2, '0', STR_PAD_LEFT).'_';

        // ・受理日
        $datas[$pageCount][$rowPrefix.'01'] = $row['consultation_YMD'];

        // ・氏名
        $datas[$pageCount][$rowPrefix.'02'] = $row['user_name_e'];
        // ・性別
        if($row['sex'] == "1") {
            $datas[$pageCount][$rowPrefix.'03_male'] = true;
        }
        if($row['sex'] == "2") {
            $datas[$pageCount][$rowPrefix.'03_female'] = true;
        }
        // ・国籍・地域
        $datas[$pageCount][$rowPrefix.'04'] = $row['nationality_region'];
        // ・生年月日
        $datas[$pageCount][$rowPrefix.'05'] = $row['birthday_Y'];
        $datas[$pageCount][$rowPrefix.'06'] = $row['birthday_M'];
        $datas[$pageCount][$rowPrefix.'07'] = $row['birthday_D'];
        // ・在留カード番号
        $datas[$pageCount][$rowPrefix.'08'] = $row['residence_card_no'];

        // ・相談内容
        $datas[$pageCount][$rowPrefix.'09'] = $row['consultation_content'];
        // ・対応結果
        $datas[$pageCount][$rowPrefix.'10'] = $row['response_results'];

        // ・対応者の氏名
        $datas[$pageCount][$rowPrefix.'11'] = $row['corresponding_person'];

        $recordIndex++;
    }

    return $datas;
}
