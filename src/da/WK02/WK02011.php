<?php

/**
 * 労働者の帳票ステータス情報を取得し、結果を返す。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function search($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         .     "sts.report_id, "
         .     "sts.status, "
         .     "DATE_FORMAT(sts.report_update_date, '%Y/%m/%d') AS report_update_ymd "
         . "FROM tbl_report_status sts "
         . "INNER JOIN mst_workers workers "
         .     "ON sts.user_id = workers.id "
         . "WHERE workers.worker_id = :worker_id "
         ;

    $params = array();
    $params[":worker_id"] = $workerId;

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    if (count($result) == 0) {
        // レコードが取得できなかった場合、データ不整備エラーとする。
        $errMessage = getCommonMessage("EC004");
        goErrorPage($errMessage);
    }

    // 取得データ(帳票単位に複数レコード)を、表示用データ(フラット構造)に変換。
    $items = array();
    $reportStatus = getOptionItems("comm", "report_status");
    foreach ($result as $row) {
        $reportId = $row["report_id"];
        $items["report_update_ymd_".$reportId] = $row["report_update_ymd"];
        $items["report_status_"    .$reportId] = getOprionItemValue($reportStatus, $row["status"]);
    }

    // 取得結果を返す。
    return $items;
}
