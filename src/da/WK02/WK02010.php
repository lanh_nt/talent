<?php

/**
 * 労働者の基本情報を取得し、結果を返す。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function search($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         .     "workers.user_name_e, "
         .     "workers.user_name, "
         .     "workers.sex, "
         .     "DATE_FORMAT(workers.birthday, '%Y/%m/%d') AS birthday_ymd, "
         .     "workers.nationality_region, "
         .     "details.G_1 AS speakable_lang, "
         .     "workers.address, "
         .     "workers.telephone, "
         .     "DATE_FORMAT(details.G_2_1_1, '%Y/%m') AS job_1_ym, details.G_2_1_2 AS job_1_detail, "
         .     "DATE_FORMAT(details.G_2_2_1, '%Y/%m') AS job_2_ym, details.G_2_2_2 AS job_2_detail, "
         .     "DATE_FORMAT(details.G_2_3_1, '%Y/%m') AS job_3_ym, details.G_2_3_2 AS job_3_detail, "
         .     "DATE_FORMAT(details.G_2_4_1, '%Y/%m') AS job_4_ym, details.G_2_4_2 AS job_4_detail, "
         .     "DATE_FORMAT(details.G_2_5_1, '%Y/%m') AS job_5_ym, details.G_2_5_2 AS job_5_detail, "
         .     "DATE_FORMAT(details.G_2_6_1, '%Y/%m') AS job_6_ym, details.G_2_6_2 AS job_6_detail, "
         .     "details.G_3 AS license, "
         .     "DATE_FORMAT(details.G_4_1_1, '%Y/%m') AS visit_1_ym, details.G_4_1_2 AS visit_1_license, details.G_4_1_3 AS visit_1_company, details.G_4_1_4 AS visit_1_superviser, "
         .     "DATE_FORMAT(details.G_4_2_1, '%Y/%m') AS visit_2_ym, details.G_4_2_2 AS visit_2_license, details.G_4_2_3 AS visit_2_company, details.G_4_2_4 AS visit_2_superviser, "
         .     "DATE_FORMAT(details.G_4_3_1, '%Y/%m') AS visit_3_ym, details.G_4_3_2 AS visit_3_license, details.G_4_3_3 AS visit_3_company, details.G_4_3_4 AS visit_3_superviser, "
         .     "DATE_FORMAT(details.G_4_4_1, '%Y/%m') AS visit_4_ym, details.G_4_4_2 AS visit_4_license, details.G_4_4_3 AS visit_4_company, details.G_4_4_4 AS visit_4_superviser, "
         .     "DATE_FORMAT(details.G_4_5_1, '%Y/%m') AS visit_5_ym, details.G_4_5_2 AS visit_5_license, details.G_4_5_3 AS visit_5_company, details.G_4_5_4 AS visit_5_superviser, "
         .     "DATE_FORMAT(details.G_4_6_1, '%Y/%m') AS visit_6_ym, details.G_4_6_2 AS visit_6_license, details.G_4_6_3 AS visit_6_company, details.G_4_6_4 AS visit_6_superviser, "
         .     "DATE_FORMAT(details.G_4_7_1, '%Y/%m') AS visit_7_ym, details.G_4_7_2 AS visit_7_license, details.G_4_7_3 AS visit_7_company, details.G_4_7_4 AS visit_7_superviser, "
         .     "DATE_FORMAT(details.G_4_8_1, '%Y/%m') AS visit_8_ym, details.G_4_8_2 AS visit_8_license, details.G_4_8_3 AS visit_8_company, details.G_4_8_4 AS visit_8_superviser "
         . "FROM mst_workers workers "
         . "INNER JOIN tbl_report_009 details "
         .     "ON workers.id = details.worker_id "
         . "WHERE workers.worker_id = :worker_id "
    ;
    $params = array();
    $params[":worker_id"] = $workerId;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);
    
    // 表示用データを追加設定。
    $sex = getOptionItems("comm", "sex");

    // コードに対応する文言を設定。
    $result["sex"] = getOprionItemValue($sex,          $result["sex"]);

    // 取得結果を返す。
    return $result;
}

/**
 * CSV出力用データを取得する。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function getExportDatas($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "details.G_1 AS speakable_lang, "
         .     "DATE_FORMAT(details.G_2_1_1, '%Y/%m') AS job_1_ym, details.G_2_1_2 AS job_1_detail, "
         .     "DATE_FORMAT(details.G_2_2_1, '%Y/%m') AS job_2_ym, details.G_2_2_2 AS job_2_detail, "
         .     "DATE_FORMAT(details.G_2_3_1, '%Y/%m') AS job_3_ym, details.G_2_3_2 AS job_3_detail, "
         .     "DATE_FORMAT(details.G_2_4_1, '%Y/%m') AS job_4_ym, details.G_2_4_2 AS job_4_detail, "
         .     "DATE_FORMAT(details.G_2_5_1, '%Y/%m') AS job_5_ym, details.G_2_5_2 AS job_5_detail, "
         .     "DATE_FORMAT(details.G_2_6_1, '%Y/%m') AS job_6_ym, details.G_2_6_2 AS job_6_detail, "
         .     "details.G_3 AS license, "
         .     "DATE_FORMAT(details.G_4_1_1, '%Y/%m') AS visit_1_ym, details.G_4_1_2 AS visit_1_license, details.G_4_1_3 AS visit_1_company, details.G_4_1_4 AS visit_1_superviser, "
         .     "DATE_FORMAT(details.G_4_2_1, '%Y/%m') AS visit_2_ym, details.G_4_2_2 AS visit_2_license, details.G_4_2_3 AS visit_2_company, details.G_4_2_4 AS visit_2_superviser, "
         .     "DATE_FORMAT(details.G_4_3_1, '%Y/%m') AS visit_3_ym, details.G_4_3_2 AS visit_3_license, details.G_4_3_3 AS visit_3_company, details.G_4_3_4 AS visit_3_superviser, "
         .     "DATE_FORMAT(details.G_4_4_1, '%Y/%m') AS visit_4_ym, details.G_4_4_2 AS visit_4_license, details.G_4_4_3 AS visit_4_company, details.G_4_4_4 AS visit_4_superviser, "
         .     "DATE_FORMAT(details.G_4_5_1, '%Y/%m') AS visit_5_ym, details.G_4_5_2 AS visit_5_license, details.G_4_5_3 AS visit_5_company, details.G_4_5_4 AS visit_5_superviser, "
         .     "DATE_FORMAT(details.G_4_6_1, '%Y/%m') AS visit_6_ym, details.G_4_6_2 AS visit_6_license, details.G_4_6_3 AS visit_6_company, details.G_4_6_4 AS visit_6_superviser, "
         .     "DATE_FORMAT(details.G_4_7_1, '%Y/%m') AS visit_7_ym, details.G_4_7_2 AS visit_7_license, details.G_4_7_3 AS visit_7_company, details.G_4_7_4 AS visit_7_superviser, "
         .     "DATE_FORMAT(details.G_4_8_1, '%Y/%m') AS visit_8_ym, details.G_4_8_2 AS visit_8_license, details.G_4_8_3 AS visit_8_company, details.G_4_8_4 AS visit_8_superviser "
         . "FROM mst_workers workers "
         . "INNER JOIN tbl_report_009 details "
         .     "ON workers.id = details.worker_id "
         . "WHERE workers.worker_id = :worker_id "
    ;
    $params = array();
    $params[":worker_id"] = $workerId;

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}


/**
 * CSVデータを登録する。
 *
 * @param array[カラム] $row CSVデータ
 * @param string $workerId 労働者ID
 * 
 * @return boolean 実行成否
 */
function csvDataImport($row, $workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        // 労働者の行IDを取得。
        $workerRowId = $db->getWorkerRowId($workerId);

        // カラムと設定値を定義。
        $columns = array();
        setColumns($columns, $db, $row);

        $whereString = "worker_id = :worker_id ";
        $whereParams = array(
            "worker_id" => $workerRowId
        );

        // update処理を発行する。
        $db->update("tbl_report_009", $columns, $whereString, $whereParams);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}

/**
 * CSVレコードの内容から、更新用データを生成する。
 *
 * @param array[DBカラム名 => 値] $columns 更新用カラム一覧
 * @param array[値] $row CSV行データ
 */
function setColumns(&$columns, $db, $row) {
    $rowid = 1 - 1;

    $columns["G_1"     ] = $row[$rowid++];

    $columns["G_2_1_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["G_2_1_2" ] = $row[$rowid++];
    $columns["G_2_2_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["G_2_2_2" ] = $row[$rowid++];
    $columns["G_2_3_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["G_2_3_2" ] = $row[$rowid++];
    $columns["G_2_4_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["G_2_4_2" ] = $row[$rowid++];
    $columns["G_2_5_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["G_2_5_2" ] = $row[$rowid++];
    $columns["G_2_6_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["G_2_6_2" ] = $row[$rowid++];

    $columns["G_3"     ] = $row[$rowid++];

    $columns["G_4_1_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["G_4_1_2" ] = $row[$rowid++];
    $columns["G_4_1_3" ] = $row[$rowid++];
    $columns["G_4_1_4" ] = $row[$rowid++];
    $columns["G_4_2_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["G_4_2_2" ] = $row[$rowid++];
    $columns["G_4_2_3" ] = $row[$rowid++];
    $columns["G_4_2_4" ] = $row[$rowid++];
    $columns["G_4_3_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["G_4_3_2" ] = $row[$rowid++];
    $columns["G_4_3_3" ] = $row[$rowid++];
    $columns["G_4_3_4" ] = $row[$rowid++];
    $columns["G_4_4_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["G_4_4_2" ] = $row[$rowid++];
    $columns["G_4_4_3" ] = $row[$rowid++];
    $columns["G_4_4_4" ] = $row[$rowid++];
    $columns["G_4_5_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["G_4_5_2" ] = $row[$rowid++];
    $columns["G_4_5_3" ] = $row[$rowid++];
    $columns["G_4_5_4" ] = $row[$rowid++];
    $columns["G_4_6_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["G_4_6_2" ] = $row[$rowid++];
    $columns["G_4_6_3" ] = $row[$rowid++];
    $columns["G_4_6_4" ] = $row[$rowid++];
    $columns["G_4_7_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["G_4_7_2" ] = $row[$rowid++];
    $columns["G_4_7_3" ] = $row[$rowid++];
    $columns["G_4_7_4" ] = $row[$rowid++];
    $columns["G_4_8_1" ] = setDateParam($row[$rowid++], "/01");
    $columns["G_4_8_2" ] = $row[$rowid++];
    $columns["G_4_8_3" ] = $row[$rowid++];
    $columns["G_4_8_4" ] = $row[$rowid++];
}
