<?php

/**
 * 労働者の在留資格認定証明書交付申請書情報を取得し、結果を返す。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function search($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         .     "workers.nationality_region, "
         .     "DATE_FORMAT(workers.birthday, '%Y/%m/%d') AS birthday_ymd, "
         .     "workers.user_name, "
         .     "workers.user_name_e, "
         .     "workers.family_name, "
         .     "workers.given_name, "
         .     "workers.sex, "
         .     "workers.place_birth, "
         .     "workers.marital_flag, "
         .     "workers.occupation, "
         .     "workers.home_town, "
         .     "workers.address, "
         .     "workers.telephone, "
         .     "workers.cellular_phone, "
         .     "workers.A_01_1, "
         .     "DATE_FORMAT(workers.A_01_2, '%Y/%m/%d') AS A_01_2_ymd, "
         .     "workers.A_02_1, "
         .     "workers.A_02_2, "
         .     "DATE_FORMAT(workers.A_03, '%Y/%m/%d') AS A_03_ymd, "
         .     "workers.A_04, "
         .     "workers.A_05, "
         .     "workers.A_06, "
         .     "workers.A_07, "
         .     "workers.A_08_1, "
         .     "workers.A_08_2, "
         .     "DATE_FORMAT(workers.A_08_3, '%Y/%m/%d') AS A_08_3_ymd, "
         .     "DATE_FORMAT(workers.A_08_4, '%Y/%m/%d') AS A_08_4_ymd, "
         .     "workers.A_15_1, "
         .     "workers.A_15_2, "
         .     "workers.A_16_1, "
         .     "workers.A_16_2, "
         .     "workers.A_17_1, "
         .     "DATE_FORMAT(workers.A_17_2, '%Y/%m/%d') AS A_17_2_ymd, "
         .     "workers.A_18_1, "
         .     "workers.A_19_1_1, "
         .     "workers.A_19_1_2, "
         .     "DATE_FORMAT(workers.A_19_1_3, '%Y/%m/%d') AS A_19_1_3_ymd, "
         .     "workers.A_19_1_4, "
         .     "workers.A_19_1_5, "
         .     "workers.A_19_1_6, "
         .     "workers.A_19_1_7, "
         .     "workers.A_19_2_1, "
         .     "workers.A_19_2_2, "
         .     "DATE_FORMAT(workers.A_19_2_3, '%Y/%m/%d') AS A_19_2_3_ymd, "
         .     "workers.A_19_2_4, "
         .     "workers.A_19_2_5, "
         .     "workers.A_19_2_6, "
         .     "workers.A_19_2_7, "
         .     "workers.A_19_3_1, "
         .     "workers.A_19_3_2, "
         .     "DATE_FORMAT(workers.A_19_3_3, '%Y/%m/%d') AS A_19_3_3_ymd, "
         .     "workers.A_19_3_4, "
         .     "workers.A_19_3_5, "
         .     "workers.A_19_3_6, "
         .     "workers.A_19_3_7, "
         .     "workers.A_19_4_1, "
         .     "workers.A_19_4_2, "
         .     "DATE_FORMAT(workers.A_19_4_3, '%Y/%m/%d') AS A_19_4_3_ymd, "
         .     "workers.A_19_4_4, "
         .     "workers.A_19_4_5, "
         .     "workers.A_19_4_6, "
         .     "workers.A_19_4_7, "
         .     "workers.A_20_1, "
         .     "workers.A_20_2, "
         .     "workers.A_20_3, "
         .     "workers.A_21_1, "
         .     "workers.A_21_2, "
         .     "workers.A_21_3, "
         .     "workers.A_21_4, "
         .     "workers.A_21_5, "
         .     "workers.A_21_6, "
         .     "workers.A_21_7, "
         .     "workers.A_22_1, "
         .     "workers.A_22_2, "
         .     "workers.A_22_3, "
         .     "workers.A_22_4, "
         .     "workers.A_22_5, "
         .     "workers.A_22_6, "
         .     "workers.A_22_7, "
         .     "workers.A_23_1_1, "
         .     "workers.A_23_1_2, "
         .     "workers.A_23_1_3, "
         .     "workers.A_23_2_1, "
         .     "workers.A_23_2_2, "
         .     "workers.A_23_2_3, "
         .     "workers.A_24_1, "
         .     "workers.A_24_2, "
         .     "workers.A_25_1, "
         .     "workers.A_25_2, "
         .     "workers.A_25_3, "
         .     "workers.A_26_1, "
         .     "workers.A_26_2, "
         .     "workers.A_26_3, "
         .     "workers.A_27, "
         .     "workers.A_28, "
         .     "workers.A_29, "
         .     "workers.A_30, "
         .     "DATE_FORMAT(workers.A_31_1_1, '%Y/%m/%d') AS A_31_1_1_ymd, "
         .     "DATE_FORMAT(workers.A_31_1_2, '%Y/%m/%d') AS A_31_1_2_ymd, "
         .     "workers.A_31_1_3, "
         .     "DATE_FORMAT(workers.A_31_2_1, '%Y/%m/%d') AS A_31_2_1_ymd, "
         .     "DATE_FORMAT(workers.A_31_2_2, '%Y/%m/%d') AS A_31_2_2_ymd, "
         .     "workers.A_31_2_3, "
         .     "DATE_FORMAT(workers.A_31_3_1, '%Y/%m/%d') AS A_31_3_1_ymd, "
         .     "DATE_FORMAT(workers.A_31_3_2, '%Y/%m/%d') AS A_31_3_2_ymd, "
         .     "workers.A_31_3_3, "
         .     "DATE_FORMAT(workers.A_31_4_1, '') AS A_31_4_1_ymd, "
         .     "DATE_FORMAT(workers.A_31_4_2, '%Y/%m/%d') AS A_31_4_2_ymd, "
         .     "workers.A_31_4_3, "
         .     "DATE_FORMAT(workers.A_31_5_1, '%Y/%m/%d') AS A_31_5_1_ymd, "
         .     "DATE_FORMAT(workers.A_31_5_2, '%Y/%m/%d') AS A_31_5_2_ymd, "
         .     "workers.A_31_5_3, "
         .     "DATE_FORMAT(workers.A_31_6_1, '%Y/%m/%d') AS A_31_6_1_ymd, "
         .     "DATE_FORMAT(workers.A_31_6_2, '%Y/%m/%d') AS A_31_6_2_ymd, "
         .     "workers.A_31_6_3, "
         .     "workers.A_32, "
         .     "workers.A_33, "
         .     "workers.A_34, "
         .     "workers.A_35, "
         .     "workers.A_36, "
         .     "workers.A_37, "
         .     "workers.A_38, "
         .     "workers.A_39, "
         .     "workers.A_40, "
         .     "workers.A_41, "

         .     "DATE_FORMAT(reportdata.B_1_01_1, '%Y/%m/%d') AS B_1_01_1_ymd, "
         .     "DATE_FORMAT(reportdata.B_1_01_2, '%Y/%m/%d') AS B_1_01_2_ymd, "
         .     "reportdata.B_1_02_1, "
         .     "reportdata.B_1_02_2, "
         .     "reportdata.B_1_02_3, "
         .     "reportdata.B_1_02_4, "
         .     "reportdata.B_1_02_5, "
         .     "reportdata.B_1_02_6, "
         .     "reportdata.B_1_02_7, "
         .     "reportdata.B_1_02_8, "
         .     "reportdata.B_1_03_1, "
         .     "reportdata.B_1_03_2, "
         .     "reportdata.B_1_04_1, "
         .     "reportdata.B_1_04_2, "
         .     "reportdata.B_1_04_3, "
         .     "reportdata.B_1_05, "
         .     "reportdata.B_1_06_1, "
         .     "reportdata.B_1_06_2, "
         .     "reportdata.B_1_07, "
         .     "reportdata.B_1_08, "
         .     "reportdata.B_1_09, "
         .     "reportdata.B_1_10, "
         .     "reportdata.B_1_11, "
         .     "reportdata.B_1_12_1, "
         .     "reportdata.B_1_12_2, "
         .     "reportdata.B_1_12_3, "
         .     "reportdata.B_1_12_4, "
         .     "reportdata.B_1_12_5, "
         .     "DATE_FORMAT(reportdata.B_1_12_6, '%Y/%m/%d') AS B_1_12_6_ymd, "
         .     "DATE_FORMAT(reportdata.B_1_12_7, '%Y/%m/%d') AS B_1_12_7_ymd, "
         .     "reportdata.B_1_13_1, "
         .     "reportdata.B_1_13_2, "
         .     "reportdata.B_1_13_3, "
         .     "reportdata.B_1_13_4, "
         .     "reportdata.B_1_13_5, "
         .     "DATE_FORMAT(reportdata.B_1_13_6, '%Y/%m/%d') AS B_1_13_6_ymd, "
         .     "reportdata.B_1_14_1, "
         .     "reportdata.B_1_14_2, "
         .     "reportdata.B_1_14_3, "
         .     "reportdata.B_2_09_3, "
         .     "reportdata.B_2_09_4, "
         .     "reportdata.B_2_09_5, "
         .     "reportdata.B_2_10_1, "
         .     "reportdata.B_2_10_2, "
         .     "reportdata.B_2_11_1, "
         .     "reportdata.B_2_11_2, "
         .     "reportdata.B_2_12_1, "
         .     "reportdata.B_2_12_2, "
         .     "reportdata.B_2_13_1, "
         .     "reportdata.B_2_13_2, "
         .     "reportdata.B_2_14_1, "
         .     "reportdata.B_2_14_2, "
         .     "reportdata.B_2_15_1, "
         .     "reportdata.B_2_15_2, "
         .     "reportdata.B_2_16_1, "
         .     "reportdata.B_2_16_2, "
         .     "reportdata.B_2_17_1, "
         .     "reportdata.B_2_17_2, "
         .     "reportdata.B_2_18_1, "
         .     "reportdata.B_2_18_2, "
         .     "reportdata.B_2_19_1, "
         .     "reportdata.B_2_19_2, "
         .     "reportdata.B_2_20_1, "
         .     "reportdata.B_2_20_2, "
         .     "reportdata.B_2_21_1, "
         .     "reportdata.B_2_21_2, "
         .     "reportdata.B_2_22, "
         .     "reportdata.B_2_23_1, "
         .     "reportdata.B_2_23_2, "
         .     "reportdata.B_2_24_1, "
         .     "reportdata.B_2_24_2, "
         .     "reportdata.B_2_25, "
         .     "reportdata.B_2_26_1, "
         .     "reportdata.B_2_26_2_1, "
         .     "reportdata.B_2_26_2_2, "
         .     "reportdata.B_2_26_2_3, "
         .     "reportdata.B_2_26_2_4, "
         .     "reportdata.B_2_26_3_1, "
         .     "reportdata.B_2_26_3_2, "
         .     "reportdata.B_2_26_3_3, "
         .     "reportdata.B_2_27_1, "
         .     "reportdata.B_2_27_2, "
         .     "reportdata.B_2_28_1, "
         .     "reportdata.B_2_28_2, "
         .     "reportdata.B_2_29, "
         .     "reportdata.B_2_30, "
         .     "reportdata.B_2_31, "
         .     "reportdata.B_2_32_1 AS support_manager_name, "
         .     "reportdata.B_2_32_2 AS support_manager_department, "
         .     "reportdata.B_2_32, "
         .     "reportdata.B_2_33_1 AS support_handler_name, "
         .     "reportdata.B_2_33_2 AS support_handler_department, "
         .     "reportdata.B_2_33, "
         .     "reportdata.B_2_34_1, "
         .     "reportdata.B_2_34_2_1, "
         .     "reportdata.B_2_34_2_2, "
         .     "reportdata.B_2_34_2_3, "
         .     "reportdata.B_2_34_3, "
         .     "reportdata.B_2_35, "
         .     "reportdata.B_2_36, "
         .     "reportdata.B_2_37, "
         .     "reportdata.B_2_38_1, "
         .     "reportdata.B_2_38_2, "
         .     "reportdata.B_2_39, "
         .     "reportdata.B_2_40, "
         .     "reportdata.B_3_01, "
         .     "reportdata.B_3_02, "
         .     "reportdata.B_3_03, "
         .     "reportdata.B_3_04, "
         .     "reportdata.B_3_05, "
         .     "reportdata.B_3_06, "
         .     "reportdata.B_3_07, "
         .     "reportdata.B_3_08, "
         .     "reportdata.B_3_09, "
         .     "reportdata.B_3_10, "
         .     "reportdata.B_3_11, "
         .     "reportdata.B_3_12, "
         .     "reportdata.B_3_13, "
         .     "reportdata.B_3_14, "
         .     "reportdata.B_3_15, "
         .     "reportdata.B_3_16, "

         .     "companydata.company_name, "
         .     "companydata.corporation_no, "
         .     "companydata.industry_type, "
         .     "companydata.address AS cmp_address, "
         .     "companydata.capital, "
         .     "companydata.annual_sales_amount, "
         .     "companydata.company_stuff_count, "
         .     "companydata.representative_name, "
         .     "companydata.support_company_name, "
         .     "companydata.support_address "

         . "FROM tbl_report_001 reportdata "
         . "INNER JOIN mst_workers workers "
         . "ON reportdata.worker_id = workers.id "
         . "LEFT JOIN mst_company companydata "
         . "ON workers.company_id = companydata.id "
         . "WHERE workers.worker_id = :worker_id "
         ;

    $params = array();
    $params[":worker_id"] = $workerId;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    if (count($result) == 0) {
        // レコードが取得できなかった場合、データ不整備エラーとする。
        $errMessage = getCommonMessage("EC004");
        goErrorPage($errMessage);
    }


    // 登録支援機関の企業名を取得
    $result["support_campany"] = getCompanyName($db);

    // 雇用する外国人の氏名の設定
    $result["userName"]   = $result["user_name"]." ".$result["user_name_e"];
    
    
    // 表示用データを追加設定。
    $sex         = getOptionItems("comm", "sex");
    $yesno       = getOptionItems("comm", "yesno");
    $purpose     = getOptionItems("comm", "purpose");
    $completion  = getOptionItems("comm", "completion");
    $completion2 = getOptionItems("comm", "completion2");
    $payment     = getOptionItems("comm", "payment");
    $occupation   = getOptionItems("comm", "occupation");
    $industry     = getOptionItems("comm", "industry");
    $member_1     = getOptionItems("comm", "member_1");
    $member_2     = getOptionItems("comm", "member_2");
    $member_3     = getOptionItems("comm", "member_3");
    $member_4     = getOptionItems("comm", "member_4");

   
    // コードに対応する文言を設定。
    $result["sex"]          = getOprionItemValue($sex,          $result["sex"]);
    $result["marital_flag"] = getOprionItemValue($yesno,        $result["marital_flag"]);
    $result["A_02_1"]       = getOprionItemValue($purpose,      $result["A_02_1"]);
    $result["A_06"]         = getOprionItemValue($yesno,        $result["A_06"]);
    $result["A_08_1"]       = getOprionItemValue($yesno,        $result["A_08_1"]);
    $result["A_15_1"]       = getOprionItemValue($yesno,        $result["A_15_1"]);
    $result["A_16_1"]       = getOprionItemValue($yesno,        $result["A_16_1"]);
    $result["A_18_1"]       = getOprionItemValue($yesno,        $result["A_18_1"]);
    $result["A_19_1_5"]     = getOprionItemValue($yesno,        $result["A_19_1_5"]);
    $result["A_19_2_5"]     = getOprionItemValue($yesno,        $result["A_19_2_5"]);
    $result["A_19_3_5"]     = getOprionItemValue($yesno,        $result["A_19_3_5"]);
    $result["A_19_4_5"]     = getOprionItemValue($yesno,        $result["A_19_4_5"]);
    $result["A_21_1"]       = getOprionItemValue($completion,   $result["A_21_1"]);
    $result["A_22_1"]       = getOprionItemValue($completion,   $result["A_22_1"]);
    $result["A_23_1_3"]     = getOprionItemValue($completion2,  $result["A_23_1_3"]);
    $result["A_23_2_3"]     = getOprionItemValue($completion2,  $result["A_23_2_3"]);
    $result["A_25_1"]       = getOprionItemValue($yesno,        $result["A_25_1"]);
    $result["A_26_1"]       = getOprionItemValue($yesno,        $result["A_26_1"]);
    $result["A_27"]         = getOprionItemValue($yesno,        $result["A_27"]);
    $result["A_28"]         = getOprionItemValue($yesno,        $result["A_28"]);
    $result["A_29"]         = getOprionItemValue($yesno,        $result["A_29"]);
    $result["A_30"]         = getOprionItemValue($yesno,        $result["A_30"]);

    $result["B_1_03_2"]     = getOprionItemValue($yesno,        $result["B_1_03_2"]);
    $result["B_1_04_3"]     = getOprionItemValue($yesno,        $result["B_1_04_3"]);
    $result["B_1_05"]       = getOprionItemValue($payment ,     $result["B_1_05"]);
    $result["B_1_06_1"]     = getOprionItemValue($yesno,        $result["B_1_06_1"]);
    $result["B_1_07"]       = getOprionItemValue($yesno,        $result["B_1_07"]);
    $result["B_1_08"]       = getOprionItemValue($yesno,        $result["B_1_08"]);
    $result["B_1_09"]       = getOprionItemValue($yesno,        $result["B_1_09"]);
    $result["B_1_10"]       = getOprionItemValue($yesno,        $result["B_1_10"]);
    $result["B_1_11"]       = getOprionItemValue($yesno,        $result["B_1_11"]);
    $result["B_2_09_3"]     = getOprionItemValue($yesno,        $result["B_2_09_3"]);
    $result["B_2_09_4"]     = getOprionItemValue($yesno,        $result["B_2_09_4"]);
    $result["B_2_10_1"]     = getOprionItemValue($yesno,        $result["B_2_10_1"]);
    $result["B_2_11_1"]     = getOprionItemValue($yesno,        $result["B_2_11_1"]);
    $result["B_2_12_1"]     = getOprionItemValue($yesno,        $result["B_2_12_1"]);
    $result["B_2_13_1"]     = getOprionItemValue($yesno,        $result["B_2_13_1"]);
    $result["B_2_14_1"]     = getOprionItemValue($yesno,        $result["B_2_14_1"]);
    $result["B_2_15_1"]     = getOprionItemValue($yesno,        $result["B_2_15_1"]);
    $result["B_2_16_1"]     = getOprionItemValue($yesno,        $result["B_2_16_1"]);
    $result["B_2_17_1"]     = getOprionItemValue($yesno,        $result["B_2_17_1"]);
    $result["B_2_18_1"]     = getOprionItemValue($yesno,        $result["B_2_18_1"]);
    $result["B_2_19_1"]     = getOprionItemValue($yesno,        $result["B_2_19_1"]);
    $result["B_2_20_1"]     = getOprionItemValue($yesno,        $result["B_2_20_1"]);
    $result["B_2_21_1"]     = getOprionItemValue($yesno,        $result["B_2_21_1"]);
    $result["B_2_22"]       = getOprionItemValue($yesno,        $result["B_2_22"]);
    $result["B_2_23_1"]     = getOprionItemValue($yesno,        $result["B_2_23_1"]);
    $result["B_2_24_1"]     = getOprionItemValue($yesno,        $result["B_2_24_1"]);
    $result["B_2_25"]       = getOprionItemValue($yesno,        $result["B_2_25"]);
    $result["B_2_26_1"]     = getOprionItemValue($yesno,        $result["B_2_26_1"]);
    $result["B_2_26_2"]     = getOprionItemValue($member_1,     $result["B_2_26_2_1"]);
    $result["B_2_26_2"]    .= getOprionItemValue($member_2,     $result["B_2_26_2_2"]);
    $result["B_2_26_2"]    .= getOprionItemValue($member_3,     $result["B_2_26_2_3"]);
    $result["B_2_26_2"]    .= getOprionItemValue($member_4,     $result["B_2_26_2_4"]);
    $result["B_2_27_1"]     = getOprionItemValue($yesno,        $result["B_2_27_1"]);
    $result["B_2_28_1"]     = getOprionItemValue($yesno,        $result["B_2_28_1"]);
    $result["B_2_29"]       = getOprionItemValue($yesno,        $result["B_2_29"]);
    $result["B_2_30"]       = getOprionItemValue($yesno,        $result["B_2_30"]);
    $result["B_2_31"]       = getOprionItemValue($yesno,        $result["B_2_31"]);
    $result["B_2_32"]       = getOprionItemValue($yesno,        $result["B_2_32"]);
    $result["B_2_33"]       = getOprionItemValue($yesno,        $result["B_2_33"]);
    $result["B_2_34_1"]     = getOprionItemValue($yesno,        $result["B_2_34_1"]);
    $result["B_2_34_2"]     = getOprionItemValue($member_1,     $result["B_2_34_2_1"]);
    $result["B_2_34_2"]    .= getOprionItemValue($member_2,     $result["B_2_34_2_2"]);
    $result["B_2_34_2"]    .= getOprionItemValue($member_3,     $result["B_2_34_2_3"]);
    $result["B_2_35"]       = getOprionItemValue($yesno,        $result["B_2_35"]);
    $result["B_2_36"]       = getOprionItemValue($yesno,        $result["B_2_36"]);
    $result["B_2_37"]       = getOprionItemValue($yesno,        $result["B_2_37"]);
    $result["B_2_38_1"]     = getOprionItemValue($yesno,        $result["B_2_38_1"]);
    $result["B_2_39"]       = getOprionItemValue($yesno,        $result["B_2_39"]);
    $result["B_2_40"]       = getOprionItemValue($yesno,        $result["B_2_40"]);
    $result["B_3_01"]       = getOprionItemValue($yesno,        $result["B_3_01"]);
    $result["B_3_02"]       = getOprionItemValue($yesno,        $result["B_3_02"]);
    $result["B_3_03"]       = getOprionItemValue($yesno,        $result["B_3_03"]);
    $result["B_3_04"]       = getOprionItemValue($yesno,        $result["B_3_04"]);
    $result["B_3_05"]       = getOprionItemValue($yesno,        $result["B_3_05"]);
    $result["B_3_06"]       = getOprionItemValue($yesno,        $result["B_3_06"]);
    $result["B_3_07"]       = getOprionItemValue($yesno,        $result["B_3_07"]);
    $result["B_3_08"]       = getOprionItemValue($yesno,        $result["B_3_08"]);
    $result["B_3_09"]       = getOprionItemValue($yesno,        $result["B_3_09"]);
    $result["B_3_10"]       = getOprionItemValue($yesno,        $result["B_3_10"]);
    $result["B_3_11"]       = getOprionItemValue($yesno,        $result["B_3_11"]);
    $result["B_3_12"]       = getOprionItemValue($yesno,        $result["B_3_12"]);
    $result["B_3_13"]       = getOprionItemValue($yesno,        $result["B_3_13"]);
    $result["B_3_14"]       = getOprionItemValue($yesno,        $result["B_3_14"]);
    $result["B_3_15"]       = getOprionItemValue($yesno,        $result["B_3_15"]);
    $result["B_3_16"]       = getOprionItemValue($yesno,        $result["B_3_16"]);
    $result["industry_type"]  = getOprionItemValue($industry,   $result["industry_type"]);
    
    // 申請時における特定技能1号での通算在留期間の「年」「月」のラベルをセット
    if ($result["A_24_1"].$result["A_24_2"] != "") {
        $result["A_24_ym"] = $result["A_24_1"]."年".$result["A_24_2"]."月";  //TODO:年/月をラベル化する。
    } else {
        $result["A_24_ym"] = "";
    }

    // 取得結果を返す。
    return $result;
}

/**
 * 企業情報から支援機関を登録している企業名を取得し、結果を返す。
 *
 * 
 * @return array[string] 会社名
 */
function getCompanyName($db) {

    // SQLを生成。
    $sql = "SELECT "
         .     "mst_company.company_name "
         . "FROM mst_company "
         . "WHERE mst_company.classification = 2 "
         ;

    $params = array();

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 取得結果を返す。
    return $result["company_name"];
}

/**
 * CSV出力用データを取得する。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function getExportDatas($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
    .     "DATE_FORMAT(reportdata.B_1_01_1, '%Y/%m/%d') AS B_1_01_1_ymd, "
    .     "DATE_FORMAT(reportdata.B_1_01_2, '%Y/%m/%d') AS B_1_01_2_ymd, "
    .     "reportdata.B_1_02_1, "
    .     "reportdata.B_1_02_2, "
    .     "reportdata.B_1_02_3, "
    .     "reportdata.B_1_02_4, "
    .     "reportdata.B_1_02_5, "
    .     "reportdata.B_1_02_6, "
    .     "reportdata.B_1_02_7, "
    .     "reportdata.B_1_02_8, "
    .     "reportdata.B_1_03_1, "
    .     "reportdata.B_1_03_2, "
    .     "reportdata.B_1_04_1, "
    .     "reportdata.B_1_04_2, "
    .     "reportdata.B_1_04_3, "
    .     "reportdata.B_1_05, "
    .     "reportdata.B_1_06_1, "
    .     "reportdata.B_1_06_2, "
    .     "reportdata.B_1_07, "
    .     "reportdata.B_1_08, "
    .     "reportdata.B_1_09, "
    .     "reportdata.B_1_10, "
    .     "reportdata.B_1_11, "
    .     "reportdata.B_1_12_1, "
    .     "reportdata.B_1_12_2, "
    .     "reportdata.B_1_12_3, "
    .     "reportdata.B_1_12_4, "
    .     "reportdata.B_1_12_5, "
    .     "DATE_FORMAT(reportdata.B_1_12_6, '%Y/%m/%d') AS B_1_12_6_ymd, "
    .     "DATE_FORMAT(reportdata.B_1_12_7, '%Y/%m/%d') AS B_1_12_7_ymd, "
    .     "reportdata.B_1_13_1, "
    .     "reportdata.B_1_13_2, "
    .     "reportdata.B_1_13_3, "
    .     "reportdata.B_1_13_4, "
    .     "reportdata.B_1_13_5, "
    .     "DATE_FORMAT(reportdata.B_1_13_6, '%Y/%m/%d') AS B_1_13_6_ymd, "
    .     "reportdata.B_1_14_1, "
    .     "reportdata.B_1_14_2, "
    .     "reportdata.B_1_14_3, "
    .     "reportdata.B_2_09_3, "
    .     "reportdata.B_2_09_4, "
    .     "reportdata.B_2_09_5, "
    .     "reportdata.B_2_10_1, "
    .     "reportdata.B_2_10_2, "
    .     "reportdata.B_2_11_1, "
    .     "reportdata.B_2_11_2, "
    .     "reportdata.B_2_12_1, "
    .     "reportdata.B_2_12_2, "
    .     "reportdata.B_2_13_1, "
    .     "reportdata.B_2_13_2, "
    .     "reportdata.B_2_14_1, "
    .     "reportdata.B_2_14_2, "
    .     "reportdata.B_2_15_1, "
    .     "reportdata.B_2_15_2, "
    .     "reportdata.B_2_16_1, "
    .     "reportdata.B_2_16_2, "
    .     "reportdata.B_2_17_1, "
    .     "reportdata.B_2_17_2, "
    .     "reportdata.B_2_18_1, "
    .     "reportdata.B_2_18_2, "
    .     "reportdata.B_2_19_1, "
    .     "reportdata.B_2_19_2, "
    .     "reportdata.B_2_20_1, "
    .     "reportdata.B_2_20_2, "
    .     "reportdata.B_2_21_1, "
    .     "reportdata.B_2_21_2, "
    .     "reportdata.B_2_22, "
    .     "reportdata.B_2_23_1, "
    .     "reportdata.B_2_23_2, "
    .     "reportdata.B_2_24_1, "
    .     "reportdata.B_2_24_2, "
    .     "reportdata.B_2_25, "
    .     "reportdata.B_2_26_1, "
    .     "reportdata.B_2_26_2_1, "
    .     "reportdata.B_2_26_2_2, "
    .     "reportdata.B_2_26_2_3, "
    .     "reportdata.B_2_26_2_4, "
    .     "reportdata.B_2_26_3_1, "
    .     "reportdata.B_2_26_3_2, "
    .     "reportdata.B_2_26_3_3, "
    .     "reportdata.B_2_27_1, "
    .     "reportdata.B_2_27_2, "
    .     "reportdata.B_2_28_1, "
    .     "reportdata.B_2_28_2, "
    .     "reportdata.B_2_29, "
    .     "reportdata.B_2_30, "
    .     "reportdata.B_2_31, "
    .     "reportdata.B_2_32_1, "
    .     "reportdata.B_2_32_2, "
    .     "reportdata.B_2_32, "
    .     "reportdata.B_2_33_1, "
    .     "reportdata.B_2_33_2, "
    .     "reportdata.B_2_33, "
    .     "reportdata.B_2_34_1, "
    .     "reportdata.B_2_34_2_1, "
    .     "reportdata.B_2_34_2_2, "
    .     "reportdata.B_2_34_2_3, "
    .     "reportdata.B_2_34_3, "
    .     "reportdata.B_2_35, "
    .     "reportdata.B_2_36, "
    .     "reportdata.B_2_37, "
    .     "reportdata.B_2_38_1, "
    .     "reportdata.B_2_38_2, "
    .     "reportdata.B_2_39, "
    .     "reportdata.B_2_40, "
    .     "reportdata.B_3_01, "
    .     "reportdata.B_3_02, "
    .     "reportdata.B_3_03, "
    .     "reportdata.B_3_04, "
    .     "reportdata.B_3_05, "
    .     "reportdata.B_3_06, "
    .     "reportdata.B_3_07, "
    .     "reportdata.B_3_08, "
    .     "reportdata.B_3_09, "
    .     "reportdata.B_3_10, "
    .     "reportdata.B_3_11, "
    .     "reportdata.B_3_12, "
    .     "reportdata.B_3_13, "
    .     "reportdata.B_3_14, "
    .     "reportdata.B_3_15, "
    .     "reportdata.B_3_16 "

    . "FROM tbl_report_001 reportdata "
    . "INNER JOIN mst_workers workers "
    . "ON reportdata.worker_id = workers.id "
    . "WHERE workers.worker_id = :worker_id "
    ;

    $params = array();
    $params[":worker_id"] = $workerId;

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}


/**
 * CSVデータを登録する。
 *
 * @param array[カラム] $row CSVデータ
 * @param string $workerId 労働者ID
 * 
 * @return boolean 実行成否
 */
function csvDataImport($row, $workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        // 労働者の行IDを取得。
        $workerRowId = getWorkerRowId($db, $workerId);

        // カラムと設定値を定義。
        $columns = array();
        setColumns($columns, $db, $row);

        $whereString = "worker_id = :worker_id ";
        $whereParams = array(
            "worker_id" => $workerRowId
        );

        // update処理を発行する。
        $db->update("tbl_report_001", $columns, $whereString, $whereParams);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}

/**
 * CSVレコードの内容から、更新用データを生成する。
 *
 * @param array[DBカラム名 => 値] $columns 更新用カラム一覧
 * @param array[値] $row CSV行データ
 */
function setColumns(&$columns, $db, $row) {
    $rowid = 1 - 1;

    $columns["B_1_01_1"     ] = setDateParam($row[$rowid++]);
    $columns["B_1_01_2"     ] = setDateParam($row[$rowid++]);
    $columns["B_1_02_1"     ] = $row[$rowid++];
    $columns["B_1_02_2"     ] = $row[$rowid++];
    $columns["B_1_02_3"     ] = $row[$rowid++];
    $columns["B_1_02_4"     ] = $row[$rowid++];
    $columns["B_1_02_5"     ] = $row[$rowid++];
    $columns["B_1_02_6"     ] = $row[$rowid++];
    $columns["B_1_02_7"     ] = $row[$rowid++];
    $columns["B_1_02_8"     ] = $row[$rowid++];
    $columns["B_1_03_1"     ] = $row[$rowid++];
    $columns["B_1_03_2"     ] = $row[$rowid++];
    $columns["B_1_04_1"     ] = $row[$rowid++];
    $columns["B_1_04_2"     ] = $row[$rowid++];
    $columns["B_1_04_3"     ] = $row[$rowid++];
    $columns["B_1_05"       ] = $row[$rowid++];
    $columns["B_1_06_1"     ] = $row[$rowid++];
    $columns["B_1_06_2"     ] = $row[$rowid++];
    $columns["B_1_07"       ] = $row[$rowid++];
    $columns["B_1_08"       ] = $row[$rowid++];
    $columns["B_1_09"       ] = $row[$rowid++];
    $columns["B_1_10"       ] = $row[$rowid++];
    $columns["B_1_11"       ] = $row[$rowid++];
    $columns["B_1_12_1"     ] = $row[$rowid++];
    $columns["B_1_12_2"     ] = $row[$rowid++];
    $columns["B_1_12_3"     ] = $row[$rowid++];
    $columns["B_1_12_4"     ] = $row[$rowid++];
    $columns["B_1_12_5"     ] = $row[$rowid++];
    $columns["B_1_12_6"     ] = setDateParam($row[$rowid++]);
    $columns["B_1_12_7"     ] = setDateParam($row[$rowid++]);
    $columns["B_1_13_1"     ] = $row[$rowid++];
    $columns["B_1_13_2"     ] = $row[$rowid++];
    $columns["B_1_13_3"     ] = $row[$rowid++];
    $columns["B_1_13_4"     ] = $row[$rowid++];
    $columns["B_1_13_5"     ] = $row[$rowid++];
    $columns["B_1_13_6"     ] = setDateParam($row[$rowid++]);
    $columns["B_1_14_1"     ] = $row[$rowid++];
    $columns["B_1_14_2"     ] = $row[$rowid++];
    $columns["B_1_14_3"     ] = $row[$rowid++];
    $columns["B_2_09_3"     ] = $row[$rowid++];
    $columns["B_2_09_4"     ] = $row[$rowid++];
    $columns["B_2_09_5"     ] = $row[$rowid++];
    $columns["B_2_10_1"     ] = $row[$rowid++];
    $columns["B_2_10_2"     ] = $row[$rowid++];
    $columns["B_2_11_1"     ] = $row[$rowid++];
    $columns["B_2_11_2"     ] = $row[$rowid++];
    $columns["B_2_12_1"     ] = $row[$rowid++];
    $columns["B_2_12_2"     ] = $row[$rowid++];
    $columns["B_2_13_1"     ] = $row[$rowid++];
    $columns["B_2_13_2"     ] = $row[$rowid++];
    $columns["B_2_14_1"     ] = $row[$rowid++];
    $columns["B_2_14_2"     ] = $row[$rowid++];
    $columns["B_2_15_1"     ] = $row[$rowid++];
    $columns["B_2_15_2"     ] = $row[$rowid++];
    $columns["B_2_16_1"     ] = $row[$rowid++];
    $columns["B_2_16_2"     ] = $row[$rowid++];
    $columns["B_2_17_1"     ] = $row[$rowid++];
    $columns["B_2_17_2"     ] = $row[$rowid++];
    $columns["B_2_18_1"     ] = $row[$rowid++];
    $columns["B_2_18_2"     ] = $row[$rowid++];
    $columns["B_2_19_1"     ] = $row[$rowid++];
    $columns["B_2_19_2"     ] = $row[$rowid++];
    $columns["B_2_20_1"     ] = $row[$rowid++];
    $columns["B_2_20_2"     ] = $row[$rowid++];
    $columns["B_2_21_1"     ] = $row[$rowid++];
    $columns["B_2_21_2"     ] = $row[$rowid++];
    $columns["B_2_22"       ] = $row[$rowid++];
    $columns["B_2_23_1"     ] = $row[$rowid++];
    $columns["B_2_23_2"     ] = $row[$rowid++];
    $columns["B_2_24_1"     ] = $row[$rowid++];
    $columns["B_2_24_2"     ] = $row[$rowid++];
    $columns["B_2_25"       ] = $row[$rowid++];
    $columns["B_2_26_1"     ] = $row[$rowid++];
    $columns["B_2_26_2_1"   ] = $row[$rowid++];
    $columns["B_2_26_2_2"   ] = $row[$rowid++];
    $columns["B_2_26_2_3"   ] = $row[$rowid++];
    $columns["B_2_26_2_4"   ] = $row[$rowid++];
    $columns["B_2_26_3_1"   ] = $row[$rowid++];
    $columns["B_2_26_3_2"   ] = $row[$rowid++];
    $columns["B_2_26_3_3"   ] = $row[$rowid++];
    $columns["B_2_27_1"     ] = $row[$rowid++];
    $columns["B_2_27_2"     ] = $row[$rowid++];
    $columns["B_2_28_1"     ] = $row[$rowid++];
    $columns["B_2_28_2"     ] = $row[$rowid++];
    $columns["B_2_29"       ] = $row[$rowid++];
    $columns["B_2_30"       ] = $row[$rowid++];
    $columns["B_2_31"       ] = $row[$rowid++];
    $columns["B_2_32_1"     ] = $row[$rowid++];
    $columns["B_2_32_2"     ] = $row[$rowid++];
    $columns["B_2_32"       ] = $row[$rowid++];
    $columns["B_2_33_1"     ] = $row[$rowid++];
    $columns["B_2_33_2"     ] = $row[$rowid++];
    $columns["B_2_33"       ] = $row[$rowid++];
    $columns["B_2_34_1"     ] = $row[$rowid++];
    $columns["B_2_34_2_1"   ] = $row[$rowid++];
    $columns["B_2_34_2_2"   ] = $row[$rowid++];
    $columns["B_2_34_2_3"   ] = $row[$rowid++];
    $columns["B_2_34_3"     ] = $row[$rowid++];
    $columns["B_2_35"       ] = $row[$rowid++];
    $columns["B_2_36"       ] = $row[$rowid++];
    $columns["B_2_37"       ] = $row[$rowid++];
    $columns["B_2_38_1"     ] = $row[$rowid++];
    $columns["B_2_38_2"     ] = $row[$rowid++];
    $columns["B_2_39"       ] = $row[$rowid++];
    $columns["B_2_40"       ] = $row[$rowid++];
    $columns["B_3_01"       ] = $row[$rowid++];
    $columns["B_3_02"       ] = $row[$rowid++];
    $columns["B_3_03"       ] = $row[$rowid++];
    $columns["B_3_04"       ] = $row[$rowid++];
    $columns["B_3_05"       ] = $row[$rowid++];
    $columns["B_3_06"       ] = $row[$rowid++];
    $columns["B_3_07"       ] = $row[$rowid++];
    $columns["B_3_08"       ] = $row[$rowid++];
    $columns["B_3_09"       ] = $row[$rowid++];
    $columns["B_3_10"       ] = $row[$rowid++];
    $columns["B_3_11"       ] = $row[$rowid++];
    $columns["B_3_12"       ] = $row[$rowid++];
    $columns["B_3_13"       ] = $row[$rowid++];
    $columns["B_3_14"       ] = $row[$rowid++];
    $columns["B_3_15"       ] = $row[$rowid++];
    $columns["B_3_16"       ] = $row[$rowid++];

}

/**
 * 労働者の行IDを取得する。
 *
 * @param PDO $db DB接続
 * @param string $workerId 取得対象の労働者ID
 * 
 * @return integer 行ID
 */
function getWorkerRowId(&$db, $workerId) {
    $sql = "SELECT id AS rowid "
         . "FROM mst_workers "
         . "WHERE worker_id = :worker_id ";
    $params[":worker_id"] = $workerId;
    $result = $db->selectOne($sql, $params);
    return $result["rowid"];
}
