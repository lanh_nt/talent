<?php

/**
 * 相談記録データを取得し、結果を返す。
 *
 * @param string $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function search($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         .     "DATE_FORMAT(reports.response_date_from, '%Y-%m-%d') AS response_from_ymd, "
         .     "DATE_FORMAT(reports.response_date_to,   '%Y-%m-%d') AS response_to_ymd, "
         .     "DATE_FORMAT(reports.response_date_from, '%Y/%m/%d') AS response_from_disp, "
         .     "DATE_FORMAT(reports.response_date_to,   '%Y/%m/%d') AS response_to_disp, "
         .     "DATE_FORMAT(reports.consultation_date,  '%Y/%m/%d') AS consultation_ymd, "
         .     "reports.consultation_content AS consultation_content, "
         .     "reports.response_results     AS response_results, "
         .     "reports.corresponding_person AS corresponding_person "
         . "FROM tbl_report_006 reports "
         . "LEFT JOIN mst_workers workers "
         .     "ON reports.worker_id = workers.id "
         . "WHERE workers.worker_id = :worker_id "
         . "ORDER BY reports.consultation_date "
    ;
    $params = array();
    $params[":worker_id"] = $workerId;

    // SQL文を発行する。
    $result = $db->select($sql, $params);
    
    // 取得結果を返す。
    return $result;
}

/**
 * 登録処理を実行する。
 *
 * @param string $workerId 労働者ID
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function update($workerId, $items) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        // 労働者の行IDを取得。
        $workerRowId = $db->getWorkerRowId($workerId);

        //----------------------------------
        // 現在の相談記録データを削除。
        //----------------------------------
        // delete条件を設定。
        $whereString = "worker_id = :worker_id ";
        $whereParams = array(
            "worker_id" => $workerRowId
        );
        // delete処理を発行する。
        $rc = $db->delete("tbl_report_006", $whereString, $whereParams);

        //----------------------------------
        // 問い合わせデータから、対象データを取得。
        //----------------------------------
        $sql = "SELECT "
             .     "DATE_FORMAT(inquiries.received_date, '%Y/%m/%d') AS received_ymd, "
             .     "inquiries.content_japanese AS consult_content, "
             .     "inquiries.reply_japanese AS reply_content, "
             .     "companies.support_handler_name AS replyer_name "
             . "FROM tbl_inquiry inquiries "
             . "LEFT JOIN mst_company companies "
             .     "ON inquiries.company_id = companies.id "
             . "WHERE inquiries.worker_id = :worker_id "
             .   "AND inquiries.reply_flag = '3' "  // 3=返信済み
        ;
        $params = array();
        $params[":worker_id"] = $workerRowId;

        if ($items["span_from"] != "") {
            $sql .= "AND inquiries.received_date >= STR_TO_DATE(:span_from, '%Y-%m-%d') ";
            $params[":span_from"] = $items["span_from"];
        }
        if ($items["span_to"] != "") {
            $sql .= "AND inquiries.received_date < DATE_ADD(STR_TO_DATE(:span_to, '%Y-%m-%d'), INTERVAL 1 DAY) ";
            $params[":span_to"] = $items["span_to"];
        }

        $sql .= "ORDER BY inquiries.received_date, inquiries.id ";

        $inquiries = $db->select($sql, $params);

        //----------------------------------
        // 問い合わせデータから、相談記録データを作成。
        //----------------------------------
        foreach ($inquiries as $inqRow) {
            // カラムと設定値を定義。
            $columns["worker_id"]            = $workerRowId;
            $columns["consultation_date"]    = setDateParam($inqRow["received_ymd"]);
            $columns["consultation_content"] = $inqRow["consult_content"];
            $columns["response_results"]     = $inqRow["reply_content"];
            $columns["corresponding_person"] = $inqRow["replyer_name"];
            $columns["response_date_from"]   = setDateParam($items["span_from"]);
            $columns["response_date_to"]     = setDateParam($items["span_to"]);

            // insert処理を発行する。
            $db->insert("tbl_report_006", $columns);
        }

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}
