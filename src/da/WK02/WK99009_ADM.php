<?php

/**
 * 「面談記録報告書（監督者）」の帳票情報を取得し、結果を返す。
 *
 * @param $targetId 監督者ID
 * 
 * @return array[] 取得結果
 */
function makePdfData09a($targetId) {

    //-----------------------------------------
    // DBから面談記録報告書（監督者）の情報を取得する。
    //-----------------------------------------
    $row = searchPDF09a($targetId);

    //-----------------------------------------
    // DBの情報から帳票情報を作成する。
    //-----------------------------------------
    $datas = Array();

    $datas['01_01'] = $row['department'] . " " . $row['user_name'];          //氏名及び役職 
    $datas['01_02'] = $row['company_name'];    //所属部署 
    if( isset($row['F_1_1'])){
        $YMD = "";
        $YMD  = $row['F_1_1_Y']. "年";
        $YMD .= $row['F_1_1_M']. "月";
        $YMD .= $row['F_1_1_D']. "日";
        $datas['01_03'] = $YMD;                       //面談日
    }     
    $datas['02_01'] = $row['F_2_1'];                    //面談対応者氏名
    if( strcmp(  strval($row['F_2_2']) , "1") == 0 ){
        $datas['02_02_s'] = $row['F_2_2'];              //役職区分：支援責任者
    }
    if( strcmp(  strval($row['F_2_2']) , "2") == 0 ){
        $datas['02_02_t'] = $row['F_2_2'];              //役職区分：支援担当者
    }
    $datas['02_02_k'] = $row['F_2_3'];                  //役職名

    if( strcmp(  strval($row['F_3_1_1_1']) , "1") == 0 ){
        $datas['03_1_1_01'] = $row['F_3_1_1_1'];        //3：1-1：問題有
    }
    if( strcmp(  strval($row['F_3_1_1_1']) , "2") == 0 ){
        $datas['03_1_1_02'] = $row['F_3_1_1_1'];        //3：1-1：問題無
    }
    $datas['03_1_1_03'] = $row['F_3_1_1_2'];            //3：1-1：問題内容

    if( strcmp(  strval($row['F_3_1_2_1']) , "1") == 0 ){
        $datas['03_1_2_yes'] = $row['F_3_1_2_1'];       //3：1-2：問題有
    }
    if( strcmp(  strval($row['F_3_1_2_1']) , "2") == 0 ){
        $datas['03_1_2_no'] = $row['F_3_1_2_1'];        //3：1-2：問題無
    }
    $datas['03_1_2_detail'] = $row['F_3_1_2_2'];        //3：1-2：問題内容

    if( strcmp(  strval($row['F_3_1_3_1']) , "1") == 0 ){
        $datas['03_1_3_yes'] = $row['F_3_1_3_1'];       //3：1-3：問題有
    }
    if( strcmp(  strval($row['F_3_1_3_1']) , "2") == 0 ){
        $datas['03_1_3_no'] = $row['F_3_1_3_1'];        //3：1-3：問題無
    }
    $datas['03_1_3_detail'] = $row['F_3_1_3_2'];        //3：1-3：問題内容

    if( strcmp(  strval($row['F_3_2_1_1']) , "1") == 0 ){
        $datas['03_2_1_yes'] = $row['F_3_2_1_1'];       //3：2-1：問題有
    }
    if( strcmp(  strval($row['F_3_2_1_1']) , "2") == 0 ){
        $datas['03_2_1_no'] = $row['F_3_2_1_1'];        //3：2-1：問題無
    }
    $datas['03_2_1_detail'] = $row['F_3_2_1_2'];        //3：2-1：問題内容

    if( strcmp(  strval($row['F_3_2_2_1']) , "1") == 0 ){
        $datas['03_2_2_yes'] = $row['F_3_2_2_1'];       //3：2-2：問題有
    }
    if( strcmp(  strval($row['F_3_2_2_1']) , "2") == 0 ){
        $datas['03_2_2_no'] = $row['F_3_2_2_1'];        //3：2-2：問題無
    }
    $datas['03_2_2_detail'] = $row['F_3_2_2_2'];        //3：2-2：問題内容

    if( strcmp(  strval($row['F_3_2_3_1']) , "1") == 0 ){
        $datas['03_2_3_yes'] = $row['F_3_2_3_1'];       //3：2-3：問題有
    }
    if( strcmp(  strval($row['F_3_2_3_1']) , "2") == 0 ){
        $datas['03_2_3_no'] = $row['F_3_2_3_1'];        //3：2-3：問題無
    }
    $datas['03_2_3_detail'] = $row['F_3_2_3_2'];        //3：2-3：問題内容

    if( strcmp(  strval($row['F_3_2_4_1']) , "1") == 0 ){
        $datas['03_2_4_yes'] = $row['F_3_2_4_1'];       //3：2-4：問題有
    }
    if( strcmp(  strval($row['F_3_2_4_1']) , "2") == 0 ){
        $datas['03_2_4_no'] = $row['F_3_2_4_1'];        //3：2-4：問題無
    }
    $datas['03_2_4_detail'] = $row['F_3_2_4_2'];        //3：2-4：問題内容

    if( strcmp(  strval($row['F_3_2_5_1']) , "1") == 0 ){
        $datas['03_2_5_yes'] = $row['F_3_2_5_1'];       //3：2-5：問題有
    }
    if( strcmp(  strval($row['F_3_2_5_1']) , "2") == 0 ){
        $datas['03_2_5_no'] = $row['F_3_2_5_1'];        //3：2-5：問題無
    }
    $datas['03_2_5_detail'] = $row['F_3_2_5_2'];        //3：2-5：問題内容

    if( strcmp(  strval($row['F_3_2_6_1']) , "1") == 0 ){
        $datas['03_2_6_yes'] = $row['F_3_2_6_1'];        //3：2-6：問題有
    }
    if( strcmp(  strval($row['F_3_2_6_1']) , "2") == 0 ){
        $datas['03_2_6_no'] = $row['F_3_2_6_1'];        //3：2-6：問題無
    }
    $datas['03_2_6_detail'] = $row['F_3_2_6_2'];         //3：2-6：問題内容

    if( strcmp(  strval($row['F_3_3_1_1']) , "1") == 0 ){
        $datas['03_3_1_yes'] = $row['F_3_3_1_1'];        //3：3-1：問題有
    }
    if( strcmp(  strval($row['F_3_3_1_1']) , "2") == 0 ){
        $datas['03_3_1_no'] = $row['F_3_3_1_1'];         //3：3-1：問題無
    }
    $datas['03_3_1_detail'] = $row['F_3_3_1_2'];         //3：3-1：問題内容

    if( strcmp(  strval($row['F_3_3_2_1']) , "1") == 0 ){
        $datas['03_3_2_yes'] = $row['F_3_3_2_1'];        //3：3-2：問題有
    }
    if( strcmp(  strval($row['F_3_3_2_1']) , "2") == 0 ){
        $datas['03_3_2_no'] = $row['F_3_3_2_1'];        //3：3-2：問題無
    }
    $datas['03_3_2_detail'] = $row['F_3_3_2_2'];        //3：3-2：問題内容

    if( strcmp(  strval($row['F_3_3_3_1']) , "1") == 0 ){
        $datas['03_3_3_yes'] = $row['F_3_3_3_1'];       //3：3-3：問題有
    }
    if( strcmp(  strval($row['F_3_3_3_1']) , "2") == 0 ){
        $datas['03_3_3_no'] = $row['F_3_3_3_1'];        //3：3-3：問題無
    }
    $datas['03_3_3_detail'] = $row['F_3_3_3_2'];        //3：3-3：問題内容

    if( strcmp(  strval($row['F_3_3_4_1']) , "1") == 0 ){
        $datas['03_3_4_yes'] = $row['F_3_3_4_1'];       //3：3-4：問題有
    }
    if( strcmp(  strval($row['F_3_3_4_1']) , "2") == 0 ){
        $datas['03_3_4_no'] = $row['F_3_3_4_1'];        //3：3-4：問題無
    }   
    $datas['03_3_4_detail'] = $row['F_3_3_4_2'];        //3：3-4：問題内容

    if( strcmp(  strval($row['F_3_3_5_1']) , "1") == 0 ){
        $datas['03_3_5_yes'] = $row['F_3_3_5_1'];       //3：3-5：問題有
    }
    if( strcmp(  strval($row['F_3_3_5_1']) , "2") == 0 ){
        $datas['03_3_5_no'] = $row['F_3_3_5_1'];        //3：3-5：問題無
    }
    $datas['03_3_5_detail'] = $row['F_3_3_5_2'];        //3：3-5：問題内容

    if( strcmp(  strval($row['F_3_4_1_1']) , "1") == 0 ){
        $datas['03_4_1_yes'] = $row['F_3_4_1_1'];       //3：4-1：問題有
    }
    if( strcmp(  strval($row['F_3_4_1_1']) , "2") == 0 ){
        $datas['03_4_1_no'] = $row['F_3_4_1_1'];        //3：4-1：問題無
    }
    $datas['03_4_1_detail'] = $row['F_3_4_1_2'];        //3：4-1：問題内容

    if( strcmp(  strval($row['F_3_4_2_1']) , "1") == 0 ){
        $datas['03_4_2_yes'] = $row['F_3_4_2_1'];       //3：4-2：問題有
    }
    if( strcmp(  strval($row['F_3_4_2_1']) , "2") == 0 ){
        $datas['03_4_2_no'] = $row['F_3_4_2_1'];        //3：4-2：問題無
    }
    $datas['03_4_2_detail'] = $row['F_3_4_2_2'];        //3：4-2：問題内容

    if( strcmp(  strval($row['F_3_5_1_1']) , "1") == 0 ){
        $datas['03_5_1_yes'] = $row['F_3_5_1_1'];       //3：5-1：問題有
    }
    if( strcmp(  strval($row['F_3_5_1_1']) , "2") == 0 ){
        $datas['03_5_1_no'] = $row['F_3_5_1_1'];        //3：5-1：問題無
    }
    $datas['03_5_1_detail'] = $row['F_3_5_1_2'];        //3：5-1：問題内容

    if( strcmp(  strval($row['F_3_5_2_1']) , "1") == 0 ){
        $datas['03_5_2_yes'] = $row['F_3_5_2_1'];       //3：5-2：問題有
    }
    if( strcmp(  strval($row['F_3_5_2_1']) , "2") == 0 ){
        $datas['03_5_2_no'] = $row['F_3_5_2_1'];        //3：5-2：問題無
    }
    $datas['03_5_2_etc'] = $row['F_3_5_2_3'];           //3：5-2：その他
    $datas['03_5_2_detail'] = $row['F_3_5_2_2'];        //3：5-2：問題内容

    if( strcmp(  strval($row['F_3_6']) , "1") == 0 ){
        $datas['03_6_yes'] = $row['F_3_6'];           //3：6-1：違反有
    }
    if( strcmp(  strval($row['F_3_6']) , "2") == 0 ){
        $datas['03_6_no'] = $row['F_3_6'];            //3：6-1：違反無
    }
    $datas['03_7'] = $row['F_3_7'];                   //3：7：その他特筆すべき事項

    $datas['04_01_01'] = $row['F_4_1_Y'];               //法令違反発生日：年
    $datas['04_01_02'] = $row['F_4_1_M'];               //法令違反発生日：月
    $datas['04_01_03'] = $row['F_4_1_D'];               //法令違反発生日：日
    $datas['04_02'] = $row['F_4_2'];                 //法令違反事実の内容

    if( strcmp(  strval($row['F_4_3_1_1']) , "1") == 0 ){
        $datas['04_03_01_01'] = $row['F_4_3_1_1'];       //4-3ア 対応内容：関係行政機関を案内
    }
    $datas['04_03_01_02'] = $row['F_4_3_1_2'];           //4-3ア 対応内容：案内した機関
    if( strcmp(  strval($row['F_4_3_1_1']) , "2") == 0 ){
        $datas['04_03_01_03'] = $row['F_4_3_1_1'];       //4-3ア 対応内容：特段対応なし
    }
    $datas['04_03_01_04'] = $row['F_4_3_1_3'];           //4-3ア 対応なし理由

    if( strcmp(  strval($row['F_4_3_2_1']) , "1") == 0 ){
        $datas['04_03_02_01'] = $row['F_4_3_2_1'];       //4-3イ 責任者通知：通知済み
    }
    if(isset($row['F_4_3_2_2'])){
        $YMD = "";
        $YMD  = $row['F_4_3_2_2_Y']. "年" ;
        $YMD .= $row['F_4_3_2_2_M']. "月" ;
        $YMD .= $row['F_4_3_2_2_D']. "日" ;
        $datas['04_03_02_02'] = $YMD;              //4-3イ 責任者通知済み：通知日
    }
    $datas['04_03_02_03'] = $row['F_4_3_2_3'];           //4-3イ 責任者通知済み：通知の相手方
    if( strcmp(  strval($row['F_4_3_2_1']) , "2") == 0 ){
        $datas['04_03_02_04'] = $row['F_4_3_2_1'];       //4-3イ 責任者通知：未通知
    }
    $datas['04_03_02_05'] = $row['F_4_3_2_4'];           //4-3イ 責任者通知未通知：理由

    if( strcmp(  strval($row['F_4_3_2_5']) , "1") == 0 ){
        $datas['04_03_02_06'] = $row['F_4_3_2_5'];       //4-3イ 管理庁届出：案内済み
    }
    if( strcmp(  strval($row['F_4_3_2_5']) , "2") == 0 ){
        $datas['04_03_02_07'] = $row['F_4_3_2_5'];       //4-3イ 管理庁届出：未了
    }

    if( strcmp(  strval($row['F_4_3_3_1']) , "1") == 0 ){
        $datas['04_03_03_01'] = $row['F_4_3_3_1'];       //4-3ウ 関係行機関：通報済み
    }

    if(isset($row['F_4_3_3_2'])){
        $YMD = "";
        $YMD  = $row['F_4_3_3_2_Y']. "年";
        $YMD .= $row['F_4_3_3_2_M']. "月";
        $YMD .= $row['F_4_3_3_2_D']. "日";
        $datas['04_03_03_02'] = $YMD;              //4-3ウ 関係行機関通報済み：通報日
    }

    $datas['04_03_03_03'] = $row['F_4_3_3_3'];           //4-3ウ 関係行機関通報済み：通報先機関
 
    if( strcmp(  strval($row['F_4_3_3_1']) , "2") == 0 ){
        $datas['04_03_03_04'] = $row['F_4_3_3_1'];       //4-3ウ 関係行機関：通報未了
    }

    $datas['04_03_03_05'] = $row['F_4_3_3_4'];           //4-3ウ 関係行機関：通報未了理由

    $datas['05_01_04'] = $row['F_2_1'];                 //面談実施者氏名

    return $datas;
}

/**
 * DBから「面談記録報告書（監督者）」の情報を取得し、結果を返す。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function searchPDF09a($adm_id) {
    $db = new JinzaiDb(DB_DEFINE);

    //取得に必要な管理者rowIDを取得する
    $admRowId = $db->getSupervisorRowId($adm_id);

    $select_sql_str  = "SELECT tbl_supervisor_info.*,mst_company.*,tbl_report_008.* ";
    //面 談 日
    $select_sql_str  .= ",date_format(F_1_1,'%Y')  as F_1_1_Y ";
    $select_sql_str  .= ",date_format(F_1_1,'%c')  as F_1_1_M ";        //0なし
    $select_sql_str  .= ",date_format(F_1_1,'%e')  as F_1_1_D ";        //0なし
    //法令違反事実の発生年月日
    $select_sql_str  .= ",date_format(F_4_1,'%Y')  as F_4_1_Y ";
    $select_sql_str  .= ",date_format(F_4_1,'%c')  as F_4_1_M ";        //0なし
    $select_sql_str  .= ",date_format(F_4_1,'%e')  as F_4_1_D ";        //0なし
    //通知日
    $select_sql_str  .= ",date_format(F_4_3_2_2,'%Y')  as F_4_3_2_2_Y ";
    $select_sql_str  .= ",date_format(F_4_3_2_2,'%c')  as F_4_3_2_2_M ";  //0なし
    $select_sql_str  .= ",date_format(F_4_3_2_2,'%e')  as F_4_3_2_2_D ";  //0なし
    //通報日
    $select_sql_str  .= ",date_format(F_4_3_3_2,'%Y')  as F_4_3_3_2_Y ";
    $select_sql_str  .= ",date_format(F_4_3_3_2,'%c')  as F_4_3_3_2_M ";  //0なし
    $select_sql_str  .= ",date_format(F_4_3_3_2,'%e')  as F_4_3_3_2_D ";  //0なし
    //作成年月日
    $select_sql_str  .= ",date_format(tbl_report_008.create_date,'%Y')  as create_date_Y ";
    $select_sql_str  .= ",date_format(tbl_report_008.create_date,'%c')  as create_date_M ";//0なし
    $select_sql_str  .= ",date_format(tbl_report_008.create_date,'%e')  as create_date_D ";//0なし

    $select_sql_str  .= "FROM tbl_supervisor_info ";
    $select_sql_str  .= "left join tbl_report_008 on tbl_report_008.admin_id = tbl_supervisor_info.id ";
    $select_sql_str  .= "left join mst_company on mst_company.id = tbl_supervisor_info.company_id ";
    $select_sql_str  .= "WHERE tbl_supervisor_info.id=:admin_rowid ";

    $params = array();
    $params[":admin_rowid"] = $admRowId;

    $result = $db->selectone($select_sql_str, $params);

    if (count($result) == 0) {
        // レコードが取得できなかった場合、データ不整備エラーとする。
        $errMessage = getCommonMessage("EC004");
        goErrorPage($errMessage);
    }

    // 取得結果を返す。
    return $result;
}
