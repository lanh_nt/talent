<?php

/**
 * 「生活オリエンテーション」の帳票情報を取得し、結果を返す。
 *
 * @param $targetWorkerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function makePdfData07($targetWorkerId) {
    //-----------------------------------------
    // DBから生活オリエンテーションの情報を取得する。
    //-----------------------------------------
    $row = searchPDF07($targetWorkerId);

    //-----------------------------------------
    // DBの情報から帳票情報を作成する。
    //-----------------------------------------
    $datas = Array();

    $datas['01_01_01'] = $row['impl1_from_Y'];   //1-1：年
    $datas['01_01_02'] = $row['impl1_from_M'];   //1-1：月
    $datas['01_01_03'] = $row['impl1_from_D'];   //1-1：日
    $datas['01_01_04'] = $row['impl1_from_K'];   //1-1：時(から）
    $datas['01_01_05'] = $row['impl1_from_I'];   //1-1：分(から)
    $datas['01_01_06'] = $row['impl1_to_K'];     //1-1：時(まで)
    $datas['01_01_07'] = $row['impl1_to_I'];     //1-1：分(まで)
    $datas['01_02_01'] = $row['impl2_from_Y'];   //1-2：年
    $datas['01_02_02'] = $row['impl2_from_M'];   //1-2：月
    $datas['01_02_03'] = $row['impl2_from_D'];   //1-2：日
    $datas['01_02_04'] = $row['impl2_from_K'];   //1-2：時(から）
    $datas['01_02_05'] = $row['impl2_from_I'];   //1-2：分(から)
    $datas['01_02_06'] = $row['impl2_to_K'];     //1-2：時(まで)
    $datas['01_02_07'] = $row['impl2_to_I'];     //1-2：分(まで)
    $datas['01_03_01'] = $row['impl3_from_Y'];   //1-3：年
    $datas['01_03_02'] = $row['impl3_from_M'];   //1-3：月
    $datas['01_03_03'] = $row['impl3_from_D'];   //1-3：日
    $datas['01_03_04'] = $row['impl3_from_K'];   //1-3：時(から）
    $datas['01_03_05'] = $row['impl3_from_I'];   //1-3：分(から)
    $datas['01_03_05'] = $row['impl3_from_I'];   //1-3：分(から)
    $datas['01_03_06'] = $row['impl3_to_K'];     //1-3：時(まで)
    $datas['01_03_07'] = $row['impl3_to_I'];     //1-3：分(まで)

    return $datas;
}

/**
 * DBから「生活オリエンテーション」の情報を取得し、結果を返す。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function searchPDF07($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    //取得に必要な労働者rowIDを取得する
    $WorkerRowId = $db->getWorkerRowId($workerId);

    $select_sql_str  = "SELECT mst_workers.*,tbl_report_005.* ";
    //日付1
    $select_sql_str  .= ", YEAR(impl1_from) AS impl1_from_Y, MONTH(impl1_from) AS impl1_from_M, DAY(impl1_from) AS impl1_from_D ";
    $select_sql_str  .= ", HOUR(impl1_from) AS impl1_from_K, MINUTE(impl1_from) as impl1_from_I ";
    $select_sql_str  .= ", HOUR(impl1_to)   AS impl1_to_K,   MINUTE(impl1_to)   as impl1_to_I ";
    //日付2
    $select_sql_str  .= ", YEAR(impl2_from) AS impl2_from_Y, MONTH(impl2_from) AS impl2_from_M, DAY(impl2_from) AS impl2_from_D ";
    $select_sql_str  .= ", HOUR(impl2_from) as impl2_from_K, MINUTE(impl2_from) as impl2_from_I ";
    $select_sql_str  .= ", HOUR(impl2_to)   as impl2_to_K,   MINUTE(impl2_to)   as impl2_to_I ";
    //日付3
    $select_sql_str  .= ", YEAR(impl3_from) AS impl3_from_Y, MONTH(impl3_from) AS impl3_from_M, DAY(impl3_from) AS impl3_from_D ";
    $select_sql_str  .= ", HOUR(impl3_from) as impl3_from_K, MINUTE(impl3_from) as impl3_from_I ";
    $select_sql_str  .= ", HOUR(impl3_to)   as impl3_to_K,   MINUTE(impl3_to)   as impl3_to_I ";

    $select_sql_str  .= "FROM mst_workers ";
    $select_sql_str  .= "left join tbl_report_005 on tbl_report_005.worker_id = mst_workers.id ";
    $select_sql_str  .= " WHERE mst_workers.id=:worker_rowid ";

    $params = array();
    $params[":worker_rowid"] = $WorkerRowId;

    $result = $db->selectone($select_sql_str, $params);

    if (count($result) == 0) {
        // レコードが取得できなかった場合、データ不整備エラーとする。
        $errMessage = getCommonMessage("EC004");
        goErrorPage($errMessage);
    }

    // 取得結果を返す。
    return $result;
}
