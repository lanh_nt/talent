<?php

/**
 * 「面談記録報告書（外国人労働者）」の帳票情報を取得し、結果を返す。
 *
 * @param $targetWorkerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function makePdfData09b($targetWorkerId) {
    //-----------------------------------------
    // DBから面談記録報告書（外国人労働者）の情報を取得する。
    //-----------------------------------------
    $row = searchPDF09b($targetWorkerId);

    //-----------------------------------------
    // DBの情報から帳票情報を作成する。
    //-----------------------------------------
    $datas = Array();

    $datas['01_01'] = $row['user_name_e'];     //面談対象者：氏名
    $datas['01_02'] = $row['company_name'];    //面談対象者：所属機関
    if(isset($row['E_1_1'])){
        $YMD = "";
        $YMD  = $row['E_1_1_Y']."年";
        $YMD .= $row['E_1_1_M']."月";
        $YMD .= $row['E_1_1_D']."日";
        $datas['01_03'] = $YMD;    //面談日
    }
    $datas['02_01'] = $row['E_2_1'];    //対応者の氏名
    if( strcmp(  strval($row['E_2_2']) , "1") == 0 ){
        $datas['02_02_s'] = $row['E_2_2'];    //役職区分：支援責任者
    }
    if( strcmp(  strval($row['E_2_2']) , "2") == 0 ){
        $datas['02_02_t'] = $row['E_2_2'];    //役職区分：支援担当者
    }
    $datas['02_02_k'] = $row['E_2_3'];    //役職名
    if( strcmp(  strval($row['E_3_1_1_1']) , "1") == 0 ){
        $datas['03_1_1_01'] = $row['E_3_1_1_1'];    //3：1-1：問題有
    }
    if( strcmp(  strval($row['E_3_1_1_1']) , "2") == 0 ){
        $datas['03_1_1_02'] = $row['E_3_1_1_1'];    //3：1-1：問題無
    }
    $datas['03_1_1_03'] = $row['E_3_1_1_2'];    //3：1-1：問題内容
    if( strcmp(  strval($row['E_3_1_2_1']) , "1") == 0 ){
        $datas['03_1_2_yes'] = $row['E_3_1_2_1'];    //3：1-2：問題有
    }
    if( strcmp(  strval($row['E_3_1_2_1']) , "2") == 0 ){
        $datas['03_1_2_no'] = $row['E_3_1_2_1'];    //3：1-2：問題無
    }
    $datas['03_1_2_detail'] = $row['E_3_1_2_2'];    //3：1-2：問題内容
    if( strcmp(  strval($row['E_3_1_3_1']) , "1") == 0 ){
        $datas['03_1_3_yes'] = $row['E_3_1_3_1'];    //3：1-3：問題有
    }
    if( strcmp(  strval($row['E_3_1_3_1']) , "2") == 0 ){
        $datas['03_1_3_no'] = $row['E_3_1_3_1'];    //3：1-3：問題無
    }
    $datas['03_1_3_detail'] = $row['E_3_1_3_2'];    //3：1-3：問題内容
    if( strcmp(  strval($row['E_3_2_1_1']) , "1") == 0 ){
        $datas['03_2_1_yes'] = $row['E_3_2_1_1'];    //3：2-1：問題有
    }
    if( strcmp(  strval($row['E_3_2_1_1']) , "2") == 0 ){
        $datas['03_2_1_no'] = $row['E_3_2_1_1'];    //3：2-1：問題無
    }
    $datas['03_2_1_detail'] = $row['E_3_2_1_2'];    //3：2-1：問題内容
    if( strcmp(  strval($row['E_3_2_2_1']) , "1") == 0 ){
        $datas['03_2_2_yes'] = $row['E_3_2_2_1'];    //3：2-2：問題有
    }
    if( strcmp(  strval($row['E_3_2_2_1']) , "2") == 0 ){
        $datas['03_2_2_no'] = $row['E_3_2_2_1'];    //3：2-2：問題無
    }
    $datas['03_2_2_detail'] = $row['E_3_2_2_2'];    //3：2-2：問題内容
    if( strcmp(  strval($row['E_3_2_3_1']) , "1") == 0 ){
        $datas['03_2_3_yes'] = $row['E_3_2_3_1'];    //3：2-3：問題有
    }
    if( strcmp(  strval($row['E_3_2_3_1']) , "2") == 0 ){
        $datas['03_2_3_no'] = $row['E_3_2_3_1'];    //3：2-3：問題無
    }
    $datas['03_2_3_detail'] = $row['E_3_2_3_2'];    //3：2-3：問題内容
    if( strcmp(  strval($row['E_3_2_4_1']) , "1") == 0 ){
        $datas['03_2_4_yes'] = $row['E_3_2_4_1'];    //3：2-4：問題有
    }
    if( strcmp(  strval($row['E_3_2_4_1']) , "2") == 0 ){
        $datas['03_2_4_no'] = $row['E_3_2_4_1'];    //3：2-4：問題無
    }
    $datas['03_2_4_detail'] = $row['E_3_2_4_2'];    //3：2-4：問題内容
    if( strcmp(  strval($row['E_3_2_5_1']) , "1") == 0 ){
        $datas['03_2_5_yes'] = $row['E_3_2_5_1'];    //3：2-5：問題有
    }
    if( strcmp(  strval($row['E_3_2_5_1']) , "2") == 0 ){
        $datas['03_2_5_no'] = $row['E_3_2_5_1'];    //3：2-5：問題無
    }
    $datas['03_2_5_detail'] = $row['E_3_2_5_2'];    //3：2-5：問題内容
    if( strcmp(  strval($row['E_3_2_6_1']) , "1") == 0 ){
        $datas['03_2_6_yes'] = $row['E_3_2_6_1'];    //3：2-6：問題有
    }
    if( strcmp(  strval($row['E_3_2_6_1']) , "2") == 0 ){
        $datas['03_2_6_no'] = $row['E_3_2_6_1'];    //3：2-6：問題無
    }
    $datas['03_2_6_detail'] = $row['E_3_2_6_2'];    //3：2-6：問題内容
    if( strcmp(  strval($row['E_3_3_1_1']) , "1") == 0 ){
        $datas['03_3_1_yes'] = $row['E_3_3_1_1'];    //3：3-1：問題有
    }
    if( strcmp(  strval($row['E_3_3_1_1']) , "2") == 0 ){
        $datas['03_3_1_no'] = $row['E_3_3_1_1'];    //3：3-1：問題無
    }
    $datas['03_3_1_detail'] = $row['E_3_3_1_2'];    //3：3-1：問題内容
    if( strcmp(  strval($row['E_3_3_2_1']) , "1") == 0 ){
        $datas['03_3_2_yes'] = $row['E_3_3_2_1'];    //3：3-2：問題有
    }
    if( strcmp(  strval($row['E_3_3_2_1']) , "2") == 0 ){
        $datas['03_3_2_no'] = $row['E_3_3_2_1'];    //3：3-2：問題無
    }
    $datas['03_3_2_detail'] = $row['E_3_3_2_2'];    //3：3-2：問題内容
    if( strcmp(  strval($row['E_3_3_3_1']) , "1") == 0 ){
        $datas['03_3_3_yes'] = $row['E_3_3_3_1'];    //3：3-3：問題有
    }
    if( strcmp(  strval($row['E_3_3_3_1']) , "2") == 0 ){
        $datas['03_3_3_no'] = $row['E_3_3_3_1'];    //3：3-3：問題無
    }
    $datas['03_3_3_detail'] = $row['E_3_3_3_2'];    //3：3-3：問題内容
    if( strcmp(  strval($row['E_3_3_4_1']) , "1") == 0 ){
        $datas['03_3_4_yes'] = $row['E_3_3_4_1'];    //3：3-4：問題有
    }
    if( strcmp(  strval($row['E_3_3_4_1']) , "2") == 0 ){
        $datas['03_3_4_no'] = $row['E_3_3_4_1'];    //3：3-4：問題無
    }
    $datas['03_3_4_detail'] = $row['E_3_3_4_2'];    //3：3-4：問題内容
    if( strcmp(  strval($row['E_3_3_5_1']) , "1") == 0 ){
        $datas['03_3_5_yes'] = $row['E_3_3_5_1'];    //3：3-5：問題有
    }
    if( strcmp(  strval($row['E_3_3_5_1']) , "2") == 0 ){
        $datas['03_3_5_no'] = $row['E_3_3_5_1'];    //3：3-5：問題無
    }
    $datas['03_3_5_detail'] = $row['E_3_3_5_2'];    //3：3-5：問題内容
    if( strcmp(  strval($row['E_3_4_1_1']) , "1") == 0 ){
        $datas['03_4_1_yes'] = $row['E_3_4_1_1'];    //3：4-1：問題有
    }
    if( strcmp(  strval($row['E_3_4_1_1']) , "2") == 0 ){
        $datas['03_4_1_no'] = $row['E_3_4_1_1'];    //3：4-1：問題無
    }
    $datas['03_4_1_detail'] = $row['E_3_4_1_2'];    //3：4-1：問題内容
    if( strcmp(  strval($row['E_3_4_2_1']) , "1") == 0 ){
        $datas['03_4_2_yes'] = $row['E_3_4_2_1'];    //3：4-2：問題有
    }
    if( strcmp(  strval($row['E_3_4_2_1']) , "2") == 0 ){
        $datas['03_4_2_no'] = $row['E_3_4_2_1'];    //3：4-2：問題無
    }
    $datas['03_4_2_detail'] = $row['E_3_4_2_2'];    //3：4-2：問題内容
    if( strcmp(  strval($row['E_3_5_1_1']) , "1") == 0 ){
        $datas['03_5_1_yes'] = $row['E_3_5_1_1'];    //3：5-1：問題有
    }
    if( strcmp(  strval($row['E_3_5_1_1']) , "2") == 0 ){
        $datas['03_5_1_no'] = $row['E_3_5_1_1'];    //3：5-1：問題無
    }
    $datas['03_5_1_detail'] = $row['E_3_5_1_2'];    //3：5-1：問題内容
    if( strcmp(  strval($row['E_3_5_2_1']) , "1") == 0 ){
        $datas['03_5_2_yes'] = $row['E_3_5_2_1'];    //3：5-2：問題有
    }
    if( strcmp(  strval($row['E_3_5_2_1']) , "2") == 0 ){
        $datas['03_5_2_no'] = $row['E_3_5_2_1'];    //3：5-2：問題無
    }
    $datas['03_5_2_etc'] = $row['E_3_5_2_2'];    //3：5-2：その他
    $datas['03_5_2_detail'] = $row['E_3_5_2_3'];    //3：5-2：問題内容
    if( strcmp(  strval($row['E_3_6']) , "1") == 0 ){
        $datas['03_6_yes'] = $row['E_3_6'];    //3：6-1：違反有
    }
    if( strcmp(  strval($row['E_3_6']) , "2") == 0 ){
        $datas['03_6_no'] = $row['E_3_6'];    //3：6-1：違反無
    }
    $datas['03_7'] = $row['E_3_7'];    //3：7：その他特筆すべき事項
    $datas['04_01_01'] = $row['E_4_1_Y'];    //発生日：年
    $datas['04_01_02'] = $row['E_4_1_M'];    //発生日：月
    $datas['04_01_03'] = $row['E_4_1_D'];    //発生日：日
    $datas['04_02'] = $row['E_4_2'];    //法令違反事実の内容
    if( strcmp(  strval($row['E_4_3_1_1']) , "1") == 0 ){
        $datas['04_03_01_01'] = $row['E_4_3_1_1'];    //4-3ア 対応内容：関係行政機関を案内
    }
    $datas['04_03_01_02'] = $row['E_4_3_1_2'];    //4-3ア 対応内容：案内した機関
    if( strcmp(  strval($row['E_4_3_1_1']) , "2") == 0 ){
        $datas['04_03_01_03'] = $row['E_4_3_1_1'];    //4-3ア 対応内容：特段対応なし
    }
    $datas['04_03_01_04'] = $row['E_4_3_1_3'];    //4-3ア 対応なし理由
    if( strcmp(  strval($row['E_4_3_2_1']) , "1") == 0 ){
        $datas['04_03_02_01'] = $row['E_4_3_2_1'];    //4-3イ 責任者通知：通知済み
    }
    if(isset($row['E_4_3_2_2'])){
        $YMD  = "";
        $YMD  = $row['E_4_3_2_2_Y']. "年";
        $YMD .= $row['E_4_3_2_2_M']. "月";
        $YMD .= $row['E_4_3_2_2_D']. "日";
        $datas['04_03_02_02'] = $YMD;                 //4-3イ 責任者通知済み：通知日
    }  
    $datas['04_03_02_03'] = $row['E_4_3_2_3'];    //4-3イ 責任者通知済み：通知の相手方
    if( strcmp(  strval($row['E_4_3_2_1']) , "2") == 0 ){
        $datas['04_03_02_04'] = $row['E_4_3_2_1'];    //4-3イ 責任者通知：未通知
    }
    $datas['04_03_02_05'] = $row['E_4_3_2_4'];    //4-3イ 責任者通知未通知：理由
    if( strcmp(  strval($row['E_4_3_2_5']) , "1") == 0 ){
        $datas['04_03_02_06'] = $row['E_4_3_2_5'];    //4-3イ 管理庁届出：案内済み
    }
    if( strcmp(  strval($row['E_4_3_2_5']) , "2") == 0 ){
        $datas['04_03_02_07'] = $row['E_4_3_2_5'];    //4-3イ 管理庁届出：未了
    }
    if( strcmp(  strval($row['E_4_3_3_1']) , "1") == 0 ){
        $datas['04_03_03_01'] = $row['E_4_3_3_1'];    //4-3ウ 関係行機関：通報済み
    }
    if(isset($row['E_4_3_3_2'])){
        $YMD  = "";
        $YMD  = $row['E_4_3_3_2_Y']. "年";
        $YMD .= $row['E_4_3_3_2_M']. "月";
        $YMD .= $row['E_4_3_3_2_D']. "日";
        $datas['04_03_03_02'] = $YMD;                 //4-3ウ 関係行機関通報済み：通報日
    }
    $datas['04_03_03_03'] = $row['E_4_3_3_3'];    //4-3ウ 関係行機関通報済み：通報先機関
    if( strcmp(  strval($row['E_4_3_3_1']) , "2") == 0 ){
        $datas['04_03_03_04'] = $row['E_4_3_3_1'];    //4-3ウ 関係行機関：通報未了
    }
    $datas['04_03_03_05'] = $row['E_4_3_3_4'];    //4-3ウ 関係行機関：通報未了理由

    $datas['05_01_04'] = $row['E_2_1'];    //5:作成日　面談実施者氏名

    return $datas;

}

/**
 * DBから「面談記録報告書（外国人労働者）」の情報を取得し、結果を返す。
 *
 * @param $workerId 労働者ID
 * 
 * @return array[] 取得結果
 */
function searchPDF09b($workerId) {
    $db = new JinzaiDb(DB_DEFINE);

    //取得に必要な労働者rowIDを取得する
    $WorkerRowId = $db->getWorkerRowId($workerId);

    $select_sql_str  = "SELECT mst_workers.*,mst_company.*,tbl_report_007.* ";
    //面 談 日
    $select_sql_str  .= ",date_format(E_1_1,'%Y')  as E_1_1_Y ";
    $select_sql_str  .= ",date_format(E_1_1,'%c')  as E_1_1_M ";        //0なし
    $select_sql_str  .= ",date_format(E_1_1,'%e')  as E_1_1_D ";        //0なし
    //法令違反事実の発生年月日
    $select_sql_str  .= ",date_format(E_4_1,'%Y')  as E_4_1_Y ";
    $select_sql_str  .= ",date_format(E_4_1,'%c')  as E_4_1_M ";        //0なし
    $select_sql_str  .= ",date_format(E_4_1,'%e')  as E_4_1_D ";        //0なし
    //通知日
    $select_sql_str  .= ",date_format(E_4_3_2_2,'%Y')  as E_4_3_2_2_Y ";
    $select_sql_str  .= ",date_format(E_4_3_2_2,'%c')  as E_4_3_2_2_M ";  //0なし
    $select_sql_str  .= ",date_format(E_4_3_2_2,'%e')  as E_4_3_2_2_D ";  //0なし
    //通報日
    $select_sql_str  .= ",date_format(E_4_3_3_2,'%Y')  as E_4_3_3_2_Y ";
    $select_sql_str  .= ",date_format(E_4_3_3_2,'%c')  as E_4_3_3_2_M ";  //0なし
    $select_sql_str  .= ",date_format(E_4_3_3_2,'%e')  as E_4_3_3_2_D ";  //0なし

    $select_sql_str  .= "FROM mst_workers ";
    $select_sql_str  .= "LEFT JOIN tbl_report_007 ON tbl_report_007.worker_id = mst_workers.id ";
    $select_sql_str  .= "LEFT JOIN mst_company ON mst_company.id = mst_workers.company_id ";
    $select_sql_str  .= "WHERE mst_workers.id=:worker_rowid ";

    $params = array();
    $params[":worker_rowid"] = $WorkerRowId;

    $result = $db->selectone($select_sql_str, $params);

    if (count($result) == 0) {
        // レコードが取得できなかった場合、データ不整備エラーとする。
        $errMessage = getCommonMessage("EC004");
        goErrorPage($errMessage);
    }

    // 取得結果を返す。
    return $result;
}
