<?php

/**
 * 検索を実行し、結果を返す。abc
 *
 * @param $items  検索条件
 * @param $pageNo ページ番号
 *
 * @return PageData 検索結果
 */
function search($items, $pageNo) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    // SQLを生成。
    $sql = "SELECT "
    . "adm.id as r_id, "      // ID
    . "adm.user_id as u_id, " // 管理者ID
    . "adm.user_name, "       // 氏名
    . "adm.sex, "             // 性別
    . "adm.department, "      // 役職
    . "adm.classification, "  // 種別
    . "DATE_FORMAT(adm.update_date, '%Y/%m/%d') AS update_date "

    . "FROM mst_admin_user adm "
    . "LEFT JOIN mst_company companies "
    .     "ON adm.company_id = companies.id "
;

    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY adm.update_date desc, u_id ";

    // SQL文を発行する。
    $pageData = $db->getPageData($sql, $params, $pageNo);

    // 表示用データを追加設定。
    $adm_classification = getOptionItems("comm", "adm_class");
    $sex                = getOptionItems("comm", "sex");

    // コードに対応する文言を設定。
    foreach ($pageData->pageDatas as &$row) {
        $row["classification"] = getOprionItemValue($adm_classification,    $row["classification"]);
        $row["sex"]            = getOprionItemValue($sex,                   $row["sex"]);
    }

    // 取得結果を返す。
    return $pageData;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $items     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($items, &$whereSqls, &$params) {
    //受入企業所属の場合、自分の会社のみを対象にする▼▼▼
    if ($_SESSION['loginCompanyType'] == "1") {
        // 完全一致条件を設定。
        $whereSqls[] = "adm.company_id = :c_id ";
        $params[":c_id"] = $_SESSION['loginCompanyRowId'];
    }

    // ・会社ID
    if ($items["C_01"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "adm.user_id = :u_id ";
        $params[":u_id"] = $items["C_01"];
    }

    // ・氏名
    if ($items["C_02"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "adm.user_name LIKE :user_name ";
        $params[":user_name"] = "%".$items["C_02"]."%";
    }
    // ・企業名
    if ($items["C_03"] != "") {
        // 会社に対して部分一致条件を設定。
        $whereSqls[] = "companies.company_name LIKE :company_name ";
        $params[":company_name"] = "%".$items["C_03"]."%";
    }
    // ・役職
    if ($items["C_04"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "adm.department LIKE :department ";
        $params[":department"] = $items["C_04"]."%";
    }

    // 性別
    if ($items["C_05_1"].$items["C_05_2"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c5Params = array();
        if ($items["C_05_1"] != "") $c5Params[] = "'1'";
        if ($items["C_05_2"] != "") $c5Params[] = "'2'";
        $c5ParamsString = joinArray($c5Params);
        $whereSqls[] = "adm.sex IN (".$c5ParamsString.") ";
    }
    // 種別
    if ($items["C_06_1"].$items["C_06_2"].$items["C_06_3"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c5Params = array();
        if ($items["C_06_1"] != "") $c5Params[] = "'1'";
        if ($items["C_06_2"] != "") $c5Params[] = "'2'";
        if ($items["C_06_3"] != "") $c5Params[] = "'3'";
        $c5ParamsString = joinArray($c5Params);
        $whereSqls[] = "adm.classification IN (".$c5ParamsString.") ";
    }

    // ・登録日
    if ($items["C_07_1"] != "") {
        $whereSqls[] = "adm.update_date >= STR_TO_DATE(:update_date_from, '%Y-%m-%d 00:00') ";
        $params[":update_date_from"] = $items["C_07_1"];
    }
    if ($items["C_07_2"] != "") {
        $whereSqls[] = "adm.update_date < DATE_ADD(STR_TO_DATE(:update_date_to, '%Y-%m-%d 23:59'), INTERVAL 1 DAY) ";
        $params[":update_date_to"] = $items["C_07_2"];
    }
}

/**
 * CSV出力用データを取得する。
 *
 * @param $items データ
 *
 * @return array[] 取得結果
 */
function getExportDatas($items) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    // SQL文を生成。
    $sql = "SELECT "
         .     "adm.user_id as u_id, "
         .     "companies.company_id, "
         .     "adm.user_name, "
         .     "adm.classification, "
         .     "adm.disp_lang, "
         .     "adm.sex, "
         .     "adm.department "
         . "FROM mst_admin_user adm "
         . "LEFT JOIN mst_company companies "
         .     "ON adm.company_id = companies.id "
    ;
    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY adm.update_date desc, u_id ";

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}

/**
 * CSVデータを登録する。
 *
 * @param array[レコード => カラム] $datas CSVデータ
 *
 * @return boolean 実行成否
 */
function csvDataImport($datas) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        foreach ($datas as $row) {
            // 列内容に対応する登録処理を判定。
            $existRowdata = !isEmptyRecord($row, COL_MAILADDRESS + 1);
            if ($existRowdata) {
                $sql = "SELECT COUNT(*) AS cnt "
                     . "FROM mst_admin_user "
                     . "WHERE user_id = :user_id ";
                $params[":user_id"] = $row[COL_MAILADDRESS];
                $result = $db->selectOne($sql, $params);
                $noMatchRecord = ($result["cnt"] == 0);
                if ($noMatchRecord) {
                    // 行データあり&既存データなし : INSERT
                    csvInsert($db, $row);
                } else {
                    // 行データあり&既存データあり : UPDATE
                    csvUpdate($db, $row);
                }
            } else {
                // 行データなし : DELETE
                csvDelete($db, $row);
            }
        }

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}
/**
 * データを新規登録する。
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 *
 * @return integer レコード件数
 */
function csvInsert($db, $row) {
    //-----------------------------------------------
    // 管理者マスタにデータを新規作成する。
    //-----------------------------------------------
    // ランダムな値でパスワード設定値を生成。
    $passString = getRandomString();
    $passValue = password_hash($passString, PASSWORD_DEFAULT);

    // カラムと設定値を定義。
    $columns = array();
    $columns["user_id" ] = $row[COL_MAILADDRESS];
    $columns["password"  ] = $passValue;
    setColumns($columns, $db, $row);

    // insert処理を発行する。
    $rc = $db->insert("mst_admin_user", $columns);

    //-----------------------------------------------
    // 登録完了メールを送信する。
    //-----------------------------------------------
    sendmailStartup($row[COL_MAILADDRESS]);
    sendmailPassword($row[COL_MAILADDRESS], $passString);

    return $rc;
}

/**
 * データを更新する。
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 *
 * @return integer レコード件数
 */
function csvUpdate($db, $row) {
    //-----------------------------------------------
    // 管理者マスタデータを更新する。
    //-----------------------------------------------

    // カラムと設定値を定義。
    $columns = array();
    setColumns($columns, $db, $row);

    $whereString = "user_id = :user_id ";
    $whereParams = array(
        "user_id" => $row[COL_MAILADDRESS]
    );

    // update処理を発行する。
    $rc = $db->update("mst_admin_user", $columns, $whereString, $whereParams);

    return $rc;
}

/**
 * データを削除する。
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 *
 * @return integer レコード件数
 */
function csvDelete($db, $row) {

    //-----------------------------------------------
    // 管理者マスタデータを削除する。
    //-----------------------------------------------
    $whereString = "user_id = :user_id ";
    $whereParams = array(
        "user_id" => $row[COL_MAILADDRESS]
    );
    $stsWhereParams = array(
        "user_id" => $row[COL_MAILADDRESS]
    );
    // delete処理を発行する。
    $rc = $db->delete("mst_admin_user", $whereString, $whereParams);

    return $rc;
}

/**
 * CSVレコードの内容から、更新用データを生成する。
 *
 * @param array[DBカラム名 => 値] $columns 更新用カラム一覧
 * @param array[値] $row CSV行データ
 */
function setColumns(&$columns, $db, $row) {
    $rowid = 2 - 1;

    // 企業の行IDを取得。
    $companyRowId = $db->getCompanyRowId($row[$rowid++]);

    $columns["company_id"     ] = $companyRowId;
    $columns["user_name"      ] = $row[$rowid++];
    $columns["classification" ] = $row[$rowid++];
    $columns["disp_lang"      ] = $row[$rowid++];
    $columns["sex"            ] = $row[$rowid++];
    $columns["department"     ] = $row[$rowid++];
}


/**
 * 登録完了メール送信処理
 *
 * @param string $mailto 送信先
 *
 */
function sendmailStartup($mailto) {
    // テンプレートエンジンを生成。
    $smarty = new Smarty();
    $smarty->template_dir = '../../templates/';
    $smarty->compile_dir  = '../../templates_c/';
    $smarty->config_dir   = '../../configs/';
    $smarty->cache_dir    = '../../cache/';

    // テンプレート変数をアサイン。
    $smarty->assign("mailto", $mailto);
    $smarty->assign("prog_id", "alogin");

    // 言語モードに応じたメールテンプレートを読み込む。
    $lang = getLangValue();
    if ($lang == '2') {
        $tmplName = 'common/starter-en.tpl';
        $subject = "[Talent Asia autoreply]User ID Notification.";
    } else if ($lang == '3') {
        $tmplName = 'common/starter-vi.tpl';
        $subject = "[Talent Asia autoreply]User ID Notification.";
    } else {
        $tmplName = 'common/starter-jp.tpl';
        $subject = "[Talent Asia システム自動返信]利用IDのお知らせ";
    }

    // テンプレートからメール本文を生成。
    $body = $smarty->fetch($tmplName);

    // メールを送信する。
    $SesMailer = new ses_mailSender();

    $ret = $SesMailer->mailSend(
        "", //$Sender by default,
        "", //$SenderName by default,
        $mailto,
        $subject,
        $body
    );
}

/**
 * パスワード通知メール送信処理
 *
 * @param string $mailto 送信先
 * @param string $passString パスワード文字列
 *
 */
function sendmailPassword($mailto, $passString) {
    // テンプレートエンジンを生成。
    $smarty = new Smarty();
    $smarty->template_dir = '../../templates/';
    $smarty->compile_dir  = '../../templates_c/';
    $smarty->config_dir   = '../../configs/';
    $smarty->cache_dir    = '../../cache/';

    // テンプレート変数をアサイン。
    $smarty->assign("passString", $passString);

    // 言語モードに応じたメールテンプレートを読み込む。
    $lang = getLangValue();
    if ($lang == '2') {
        $tmplName = 'common/password-en.tpl';
        $subject = "[Talent Asia autoreply]Password Notification.";
    } else if ($lang == '3') {
        $tmplName = 'common/password-vi.tpl';
        $subject = "[Talent Asia autoreply]Password Notification.";
    } else {
        $tmplName = 'common/password-jp.tpl';
        $subject = "[Talent Asia システム自動返信]パスワードのお知らせ";
    }

    // テンプレートからメール本文を生成。
    $body = $smarty->fetch($tmplName);

    // メールを送信する。
    $SesMailer = new ses_mailSender();

    $ret = $SesMailer->mailSend(
        "", //$Sender by default,
        "", //$SenderName by default,
        $mailto,
        $subject,
        $body
    );
}
