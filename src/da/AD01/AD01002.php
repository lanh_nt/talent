<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * 
 * @return result 検索結果
 */
function search($ad_Id) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
    . "adm.user_id, "         // 管理者ID
    . "adm.user_name, "       // 氏名
    . "adm.sex, "             // 性別
    . "adm.department, "      // 役職
    . "adm.classification, "   // 種別
    . "companies.company_name "
    
    . "FROM mst_admin_user adm "
    . "LEFT JOIN mst_company companies "
    .     "ON adm.company_id = companies.id "
    . "WHERE adm.id = :ad_Id "
;

    $params = array();
    $params[":ad_Id"] = $ad_Id;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 表示用データを追加設定。
    $adm_classification = getOptionItems("comm", "adm_class");
    $sex                = getOptionItems("comm", "sex");

   // 業種の設定
    $result["class"] = getOprionItemValue($adm_classification, $result["classification"]);
    $result["sex"]            = getOprionItemValue($sex,                $result["sex"]);

    // 取得結果を返す。
    return $result;
}


