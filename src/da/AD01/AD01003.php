<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * 
 * @return result 検索結果
 */
function search($ad_Id) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
    . "user_id, "         // 管理者ID
    . "user_name, "       // 氏名
    . "sex, "             // 性別
    . "department, "      // 役職
    . "classification, "   // 種別
    . "company_id "        // 企業
    
    . "FROM mst_admin_user "
    . "WHERE id = :ad_Id "
;

    $params = array();
    $params[":ad_Id"] = $ad_Id;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);
    $newpassword = "";

    $result["new_password"] = $newpassword;
    $result["new_password2"] = $newpassword;

    // 取得結果を返す。
    return $result;
}

/**
 * 企業の一覧を取得する。
 *
 * @return array[行データ][キー => 値] 取得結果
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "ORDER BY company_name ";

    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}

/**
 * 登録処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function update($items) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {

        // カラムと設定値を定義。
        // ※パスワードは、値設定時のみ更新する。
        $columns = array(
            "user_id"         => $items["user_id"],
            "user_name"       => $items["user_name"],
            "sex"             => $items["sex"],
            "department"      => $items["department"],
            "classification"  => $items["classification"],
            "company_id"      => $items["company_id"]
        );
        if ($items["new_password"] != "") {
            // パスワード設定値を生成。
            $passValue = password_hash($items["new_password"], PASSWORD_DEFAULT);
            $columns["password"] = $passValue;
        }
        $whereString = "id = :id ";
        $whereParams = array(
            "id" => $items["ad_id"]
        );
    
        // update処理を発行する。
        $db->update("mst_admin_user", $columns, $whereString, $whereParams);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}
