<?php

/**
 * 登録処理を実行する。
 *
 * @param string $userId ユーザID
 * @param string $pwd    パスワード
 * 
 * @return boolean 実行成否
 */
function update($userId, $pwd) {
    $db = new JinzaiDb(DB_DEFINE);
    $db->setUserType("2");
    $db->setUserRowId($db->getWorkerRowId($userId));

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        // パスワード用の値(digest)を生成。
        $passValue = password_hash($pwd, PASSWORD_DEFAULT);

        // カラムと設定値を定義。
        $columns = array(
            "password" => $passValue
        );
        $whereString = "worker_id = :user_id ";
        $whereParams = array(
            "user_id" => $userId
        );
    
        // update処理を発行する。
        $db->update("mst_workers", $columns, $whereString, $whereParams);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}
