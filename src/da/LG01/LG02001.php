<?php

/**
 * HELP情報を取得する。
 *
 * @return array HELP情報
 */
function getHelpInfo($lang) {
    $db = new JinzaiDb(DB_DEFINE);

    // HELP情報を取得する。
    $sql = "SELECT "
         .     "event, "
         .     "site_name, "
         .     "site_url "
         . "FROM mst_login_contents "
         . "WHERE disp_lang = :disp_lang "
         . "ORDER BY sequence "
    ;
    $params = array(
        ":disp_lang" => $lang
    );
    $result = $db->select($sql, $params);

    // 取得した内容を返す。
    return $result;
}
