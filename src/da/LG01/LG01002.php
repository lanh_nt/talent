<?php

/**
 * ログイン情報を取得する。
 *
 * @param string $userId ユーザID
 * @param string $pwd    パスワード
 * 
 * @return array[カラム名 => 値] ユーザ情報
 */
function getUserInfo($userId, $pwd) {
    $db = new JinzaiDb(DB_DEFINE);

    // ユーザ情報を取得する。
    $sql = "SELECT "
         .     "mst_admin_user.id AS user_rowid, "
         .     "mst_admin_user.user_id, "
         .     "mst_admin_user.password, "
         .     "mst_admin_user.user_name, "
         .     "mst_admin_user.company_id AS company_rowid, "
         .     "mst_company.company_id AS company_id, "
         .     "mst_company.classification AS company_type, "
         .     "'1' AS user_type, "  // '1' : 管理者
         .     "mst_admin_user.classification AS user_auth, "
         .     "mst_admin_user.disp_lang "
         . "FROM mst_admin_user "
         . "LEFT JOIN mst_company "
         .     "ON mst_admin_user.company_id = mst_company.id "
         . "WHERE mst_admin_user.user_id = :user_id ";
    $params = array(
        ":user_id" => $userId
    );
    $result = $db->selectOne($sql, $params);

    if ($result == null) {
        return null;
    }
    // パスワードが合致しているかどうかチェック。
    if (!password_verify($pwd ,$result["password"])) {
        return null;
    }

    // 取得したユーザ情報を返す。
    return $result;
}
