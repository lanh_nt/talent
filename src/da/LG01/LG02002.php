<?php

/**
 * ログイン情報を取得する。
 *
 * @param string $userId ユーザID
 * @param string $pwd    パスワード
 * 
 * @return array[カラム名 => 値] ユーザ情報
 */
function getUserInfo($userId, $pwd) {
    $db = new JinzaiDb(DB_DEFINE);

    // ユーザ情報を取得する。
    $sql = "SELECT "
         .     "mst_workers.id AS user_rowid, "
         .     "mst_workers.worker_id AS user_id, "
         .     "mst_workers.password, "
         .     "mst_workers.user_name, "
         .     "mst_workers.company_id AS company_rowid, "
         .     "mst_company.company_id AS company_id, "
         .     "mst_company.classification AS company_type, "
         .     "'2' AS user_type, "  // '2' : 労働者
         .     "'9' AS user_auth, "  // '9' : 管理者でない
         .     "mst_workers.disp_lang "
         . "FROM mst_workers "
         . "LEFT JOIN mst_company "
         .     "ON mst_workers.company_id = mst_company.id "
         . "WHERE mst_workers.worker_id = :user_id "
    ;
    $params = array(
        ":user_id" => $userId
    );
    $result = $db->selectOne($sql, $params);

    if ($result == null) {
        return null;
    }
    // パスワードが合致しているかどうかチェック。
    if (!password_verify($pwd ,$result["password"])) {
        return null;
    }

    // 取得したユーザ情報を返す。
    return ($result);
}
