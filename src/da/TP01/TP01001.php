<?php

$reportNames = array();

/**
 * 検索を実行し、結果を返す。
 *
 * @param $companyRowId  検索条件:企業ID
 * @param $workerRowId   検索条件:労働者ID
 * 
 * @return array 検索結果
 */
function search($companyRowId = null, $workerRowId = null) {
    $items = array();

    $db = new JinzaiDb(DB_DEFINE);

    // 帳票名データを取得。
    global $reportNames;
    $reportNames = getOptionItems("comm", "report_name");

    // 対応期限が近い問い合わせ(件数)
    $deadlineInquiryCount = getDeadlineInquiryCount($db, $companyRowId, $workerRowId);
    $items["deadlineInquiryCount"] = $deadlineInquiryCount;

    // 未対応の問い合わせ(件数)
    $noReplyInquiryCount = getNoReplyInquiryCount($db, $companyRowId, $workerRowId);
    $items["noReplyInquiryCount"] = $noReplyInquiryCount;

    // 入力完了期限帳票
    $nearlyInputLimitReports = getNearlyInputLimitReports($db, $companyRowId, $workerRowId);
    $items["nearlyInputLimitReports"] = $nearlyInputLimitReports;

    // 提出期限帳票
    $nearlySubmitLimitReports = getNearlySubmitLimitReports($db, $companyRowId, $workerRowId);
    $items["nearlySubmitLimitReports"] = $nearlySubmitLimitReports;

    // 差し戻し帳票
    $rejectReports = getRejectReports($db, $companyRowId, $workerRowId);
    $items["rejectReports"] = $rejectReports;

    // 取得結果を返す。
    return $items;
}

/**
 * 対応期限が近い問い合わせ件数を取得する。
 *
 * @param $db  DB接続
 * @param $companyRowId  検索条件:企業ID
 * @param $workerRowId   検索条件:労働者ID

 * @return 件数
 */
function getDeadlineInquiryCount($db, $companyRowId, $workerRowId) {
    // 検索条件に従ったWHERE句を生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    // 〇企業ID
    if ($companyRowId != null) {
        $whereSqls[] = "company_id = :company_rowid ";
        $params[":company_rowid"] = $companyRowId;
    }
    // 〇労働者ID
    if ($workerRowId != null) {
        $whereSqls[] = "worker_id = :worker_rowid ";
        $params[":worker_rowid"] = $workerRowId;
    }
    // 〇期限
    $whereSqls[] = "reply_flag != '3' ";
    $whereSqls[] = "reply_deadline <= DATE_ADD(CURDATE(), INTERVAL 7 DAY) ";

    // SQL文を生成。
    $sql = "SELECT COUNT(*) AS cnt "
         . "FROM tbl_inquiry "
    ;
    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 取得結果を返す。
    return $result["cnt"];
}

/**
 * 未対応の問い合わせ件数を取得する。
 *
 * @param $db  DB接続
 * @param $companyRowId  検索条件:企業ID
 * @param $workerRowId   検索条件:労働者ID

 * @return 件数
 */
function getNoReplyInquiryCount($db, $companyRowId, $workerRowId) {
    // 検索条件に従ったWHERE句を生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    // 〇企業ID
    if ($companyRowId != null) {
        $whereSqls[] = "company_id = :company_rowid ";
        $params[":company_rowid"] = $companyRowId;
    }
    // 〇労働者ID
    if ($workerRowId != null) {
        $whereSqls[] = "worker_id = :worker_rowid ";
        $params[":worker_rowid"] = $workerRowId;
    }
    // 〇未対応の問い合わせ
    $whereSqls[] = "reply_flag != '3' ";

    // SQL文を生成。
    $sql = "SELECT COUNT(*) AS cnt "
         . "FROM tbl_inquiry "
    ;
    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 取得結果を返す。
    return $result["cnt"];
}

/**
 * 入力完了期限帳票を取得する。
 *
 * @param $db  DB接続
 * @param $companyRowId  検索条件:企業ID
 * @param $workerRowId   検索条件:労働者ID
 * 
 * @return array 帳票一覧
 */
function getNearlyInputLimitReports($db, $companyRowId, $workerRowId) {
    // 検索条件に従ったWHERE句を生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    // 〇企業ID
    if ($companyRowId != null) {
        $whereSqls[] = "users.company_id = :company_rowid ";
        $params[":company_rowid"] = $companyRowId;
    }
    // 〇労働者ID
    if ($workerRowId != null) {
        $whereSqls[] = "sts.user_id = :worker_rowid ";
        $params[":worker_rowid"] = $workerRowId;
    }
    // 〇期限
    $whereSqls[] = "sts.status != '5' ";
    $whereSqls[] = "sts.input_deadline <= DATE_ADD(CURDATE(), INTERVAL 14 DAY) ";

    if (count($whereSqls) > 0) {
        $whereSqlString = "AND ".$db->getWhereSql($whereSqls);
    } else {
        $whereSqlString = "";
    }

    // SQL文を生成。
    $sql = "SELECT * FROM ("
         . "SELECT "
         .     "users.worker_id AS user_id, "
         .     "users.user_name, "
         .     "sts.report_id, "
         .     "DATE_FORMAT(sts.input_deadline, '%Y/%m/%d') AS input_deadline_ymd, "
         .     "CASE WHEN sts.input_deadline <  CURDATE() THEN ' has-text-danger' "
         .          "WHEN sts.input_deadline <= DATE_ADD(CURDATE(), INTERVAL 7 DAY) THEN ' has-text-alert' "
         .          "ELSE '' "
         .     "END AS row_class "
         . "FROM tbl_report_status sts "
         . "INNER JOIN mst_workers users "
         .     "ON sts.user_id = users.id "
         . "WHERE sts.user_type = '2' "
         .     $whereSqlString
         . "UNION "
         . "SELECT "
         .     "users.user_id, "
         .     "users.user_name, "
         .     "'99' AS report_id, "
         .     "DATE_FORMAT(sts.input_deadline, '%Y/%m/%d') AS input_deadline_ymd, "
         .     "CASE WHEN sts.input_deadline <  CURDATE() THEN ' has-text-danger' "
         .          "WHEN sts.input_deadline <= DATE_ADD(CURDATE(), INTERVAL 7 DAY) THEN ' has-text-alert' "
         .          "ELSE '' "
         .     "END AS row_class "
         . "FROM tbl_report_status sts "
         . "INNER JOIN mst_admin_user users "
         .     "ON sts.user_id = users.id "
         . "WHERE sts.user_type = '1' "
         .     $whereSqlString
         . ") tmp "
         . "ORDER BY input_deadline_ymd, report_id "
    ;

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // コードに対応する文言を設定。
    global $reportNames;
    foreach ($result as &$row) {
        $row["report_name"] = getOprionItemValue($reportNames, $row["report_id"]);
    }

    // 取得結果を返す。
    return $result;
}

/**
 * 提出期限帳票を取得する。
 *
 * @param $db  DB接続
 * @param $companyRowId  検索条件:企業ID
 * @param $workerRowId   検索条件:労働者ID
 * 
 * @return array 帳票一覧
 */
function getNearlySubmitLimitReports($db, $companyRowId, $workerRowId) {
    // 検索条件に従ったWHERE句を生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    // 〇企業ID
    if ($companyRowId != null) {
        $whereSqls[] = "users.company_id = :company_rowid ";
        $params[":company_rowid"] = $companyRowId;
    }
    // 〇労働者ID
    if ($workerRowId != null) {
        $whereSqls[] = "sts.user_id = :worker_rowid ";
        $params[":worker_rowid"] = $workerRowId;
    }
    // 〇期限
    $whereSqls[] = "sts.status != '5' ";
    $whereSqls[] = "sts.submission_deadline <= DATE_ADD(CURDATE(), INTERVAL 14 DAY) ";

    if (count($whereSqls) > 0) {
        $whereSqlString = "AND ".$db->getWhereSql($whereSqls);
    } else {
        $whereSqlString = "";
    }

    // SQL文を生成。
    $sql = "SELECT * FROM ("
         . "SELECT "
         .     "users.worker_id AS user_id, "
         .     "users.user_name, "
         .     "sts.report_id, "
         .     "DATE_FORMAT(sts.submission_deadline, '%Y/%m/%d') AS submission_deadline_ymd, "
         .     "CASE WHEN sts.submission_deadline <  CURDATE() THEN ' has-text-danger' "
         .          "WHEN sts.submission_deadline <= DATE_ADD(CURDATE(), INTERVAL 7 DAY) THEN ' has-text-alert' "
         .          "ELSE '' "
         .     "END AS row_class "
         . "FROM tbl_report_status sts "
         . "INNER JOIN mst_workers users "
         .     "ON sts.user_id = users.id "
         . "WHERE sts.user_type = '2' "
         .     $whereSqlString
         . "UNION "
         . "SELECT "
         .     "users.user_id, "
         .     "users.user_name, "
         .     "sts.report_id, "
         .     "DATE_FORMAT(sts.submission_deadline, '%Y/%m/%d') AS submission_deadline_ymd, "
         .     "CASE WHEN sts.submission_deadline <  CURDATE() THEN ' has-text-danger' "
         .          "WHEN sts.submission_deadline <= DATE_ADD(CURDATE(), INTERVAL 7 DAY) THEN ' has-text-alert' "
         .          "ELSE '' "
         .     "END AS row_class "
         . "FROM tbl_report_status sts "
         . "INNER JOIN mst_admin_user users "
         .     "ON sts.user_id = users.id "
         . "WHERE sts.user_type = '1' "
         .     $whereSqlString
         . ") tmp "
         . "ORDER BY submission_deadline_ymd, report_id "
    ;

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // コードに対応する文言を設定。
    global $reportNames;
    foreach ($result as &$row) {
        $row["report_name"] = getOprionItemValue($reportNames, $row["report_id"]);
    }

    // 取得結果を返す。
    return $result;
}

/**
 * 差し戻し帳票を取得する。
 *
 * @param $db  DB接続
 * @param $companyRowId  検索条件:企業ID
 * @param $workerRowId   検索条件:労働者ID
 * 
 * @return array 帳票一覧
 */
function getRejectReports($db, $companyRowId, $workerRowId) {
    // 検索条件に従ったWHERE句を生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    // 〇企業ID
    if ($companyRowId != null) {
        $whereSqls[] = "users.company_id = :company_rowid ";
        $params[":company_rowid"] = $companyRowId;
    }
    // 〇労働者ID
    if ($workerRowId != null) {
        $whereSqls[] = "sts.user_id = :worker_rowid ";
        $params[":worker_rowid"] = $workerRowId;
    }
    // 〇状態=差戻し
    $whereSqls[] = "sts.status = '4' ";

    if (count($whereSqls) > 0) {
        $whereSqlString = "AND ".$db->getWhereSql($whereSqls);
    } else {
        $whereSqlString = "";
    }

    // SQL文を生成。
    $sql = "SELECT * FROM ("
         . "SELECT "
         .     "users.worker_id AS user_id, "
         .     "users.user_name, "
         .     "sts.report_id, "
         .     "DATE_FORMAT(sts.input_deadline, '%Y/%m/%d') AS input_deadline_ymd, "
         .     "CASE WHEN sts.input_deadline <  CURDATE() THEN ' has-text-danger' "
         .          "WHEN sts.input_deadline <= DATE_ADD(CURDATE(), INTERVAL 7 DAY) THEN ' has-text-alert' "
         .          "ELSE '' "
         .     "END AS row_class "
         . "FROM tbl_report_status sts "
         . "INNER JOIN mst_workers users "
         .     "ON sts.user_id = users.id "
         . "WHERE sts.user_type = '2' "
         .     $whereSqlString
         . "UNION "
         . "SELECT "
         .     "users.user_id, "
         .     "users.user_name, "
         .     "sts.report_id, "
         .     "DATE_FORMAT(sts.input_deadline, '%Y/%m/%d') AS input_deadline_ymd, "
         .     "CASE WHEN sts.input_deadline <  CURDATE() THEN ' has-text-danger' "
         .          "WHEN sts.input_deadline <= DATE_ADD(CURDATE(), INTERVAL 7 DAY) THEN ' has-text-alert' "
         .          "ELSE '' "
         .     "END AS row_class "
         . "FROM tbl_report_status sts "
         . "INNER JOIN mst_admin_user users "
         .     "ON sts.user_id = users.id "
         . "WHERE sts.user_type = '1' "
         .     $whereSqlString
         . ") tmp "
         . "ORDER BY input_deadline_ymd, report_id "
    ;

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // コードに対応する文言を設定。
    global $reportNames;
    foreach ($result as &$row) {
        $row["report_name"] = getOprionItemValue($reportNames, $row["report_id"]);
    }

    // 取得結果を返す。
    return $result;
}
