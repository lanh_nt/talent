<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * @param $pageNo ページ番号
 * 
 * @return PageData 検索結果
 */
function search($items, $pageNo) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE句を生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
    }

    // SQL文を生成。
    $sql = "SELECT "
         .     "inquiries.id AS inq_id, "
         .     "workers.worker_id AS user_id, "
         .     "workers.user_name AS user_name, "
         .     "companies.company_name AS company_name, "
         .     "DATE_FORMAT(inquiries.received_date, '%Y/%m/%d') AS received_ymd, "
         .     "DATE_FORMAT(inquiries.reply_deadline, '%Y/%m/%d') AS deadline_ymd, "
         .     "inquiries.reply_flag AS reply_flag "
         . "FROM tbl_inquiry inquiries "
         . "INNER JOIN mst_workers workers "
         .     "ON inquiries.worker_id = workers.id "
         . "INNER JOIN mst_company companies "
         .     "ON inquiries.company_id = companies.id "
    ;
    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY inquiries.reply_flag, inquiries.reply_deadline IS NULL ASC, inquiries.reply_deadline ASC, inquiries.received_date IS NULL DESC ";

    // SQL文を発行する。
    $pageData = $db->getPageData($sql, $params, $pageNo);

    // 表示用データを追加設定。
    $replyStatus = getOptionItems("comm", "reply_status");

    // コードに対応する文言を設定。
    foreach ($pageData->pageDatas as &$row) {
        $row["reply_status"] = getOprionItemValue($replyStatus, $row["reply_flag"]);
    }

    // 取得結果を返す。
    return $pageData;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $items     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($items, &$whereSqls, &$params) {
    // ・労働者ID
    if ($items["C_01"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "workers.worker_id = :worker_id ";
        $params[":worker_id"] = $items["C_01"];
    }

    // ・労働者氏名
    if ($items["C_02"] != "") {
        // 労働者名に対して部分一致条件を設定。
        $whereSqls[] = "workers.user_name LIKE :worker_name ";
        $params[":worker_name"] = "%".$items["C_02"]."%";
    }

    // ・企業名
    if ($items["C_03"] != "") {
        $whereSqls[] = "inquiries.company_id = :company_id ";
        $params[":company_id"] = $items["C_03"];
    }

    // ・受信日
    if ($items["C_04_1"] != "") {
        $whereSqls[] = "inquiries.received_date >= STR_TO_DATE(:received_date_from, '%Y-%m-%d') ";
        $params[":received_date_from"] = $items["C_04_1"];
    }
    if ($items["C_04_2"] != "") {
        $whereSqls[] = "inquiries.received_date < DATE_ADD(STR_TO_DATE(:received_date_to, '%Y-%m-%d'), INTERVAL 1 DAY) ";
        $params[":received_date_to"] = $items["C_04_2"];
    }

    // ・回答期限
    if ($items["C_05_1"] != "") {
        $whereSqls[] = "inquiries.reply_deadline >= STR_TO_DATE(:reply_deadline_from, '%Y-%m-%d') ";
        $params[":reply_deadline_from"] = $items["C_05_1"];
    }
    if ($items["C_05_2"] != "") {
        $whereSqls[] = "inquiries.reply_deadline < DATE_ADD(STR_TO_DATE(:reply_deadline_to, '%Y-%m-%d'), INTERVAL 1 DAY) ";
        $params[":reply_deadline_to"] = $items["C_05_2"];
    }

    // ・返信
    if ($items["C_06_1"].$items["C_06_2"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c6Params = array();
        if ($items["C_06_2"] != "") $c6Params[] = "'1'";  // 未返信(未翻訳)
        if ($items["C_06_2"] != "") $c6Params[] = "'2'";  // 未返信(翻訳済)
        if ($items["C_06_1"] != "") $c6Params[] = "'3'";  // 返信済
        $c6ParamsString = joinArray($c6Params);
        $whereSqls[] = "inquiries.reply_flag IN (".$c6ParamsString.") ";
    }
}


/**
 * 受入企業の一覧を取得する。
 *
 * @return void
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "WHERE classification = '1' "  // 受入機関
         . "ORDER BY company_name "
    ;
    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}
