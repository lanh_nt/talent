<?php

/**
 * 問い合わせ情報を取得し、結果を返す。
 *
 * @param $inqId 問い合わせID
 * 
 * @return array[] 取得結果
 */
function search($inqId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         .     "inquiries.id AS inq_id, "
         .     "inquiries.inquiry_no AS inq_no, "
         .     "inquiries.reply_flag AS status, "
         .     "DATE_FORMAT(inquiries.received_date,  '%Y/%m/%d') AS received_ymd, "
         .     "DATE_FORMAT(inquiries.reply_deadline, '%Y/%m/%d') AS deadline_ymd, "
         .     "workers.user_name AS user_name, "
         .     "inquiries.delivery_language AS lang_flag, "
         .     "inquiries.subject_native   AS subject_nv, "
         .     "inquiries.subject_japanese AS subject_jp, "
         .     "inquiries.content_native   AS content_nv, "
         .     "inquiries.content_japanese AS content_jp, "
         .     "inquiries.reply_native     AS reply_nv, "
         .     "inquiries.reply_japanese   AS reply_jp "
         . "FROM tbl_inquiry inquiries "
         . "LEFT JOIN mst_workers workers "
         .     "ON inquiries.worker_id = workers.id "
         . "WHERE inquiries.id = :inq_id "
    ;
    $params = array(
        ":inq_id" => $inqId
    );

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 表示用データを追加設定。
    $langs = getOptionItems("comm", "lang");

    // コードに対応する文言を設定。
    $result["lang_string"] = getOprionItemValue($langs, $result["lang_flag"]);

    // 取得結果を返す。
    return $result;
}

/**
 * 翻訳登録の登録処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function updateTranslate($items) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        //----------------------------------
        // 問い合わせ情報を更新。
        //----------------------------------
        // カラムと設定値を定義。
        $columns = array(
            "subject_japanese" => $items["subject_jp"],
            "content_japanese" => $items["content_jp"],
            "reply_flag"       => "2"  // 未返信(翻訳済)
        );
        $whereString = "id = :inq_id ";
        $whereParams = array(
            "inq_id" => $items["inq_id"]
        );

        // update処理を発行する。
        $db->update("tbl_inquiry", $columns, $whereString, $whereParams);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}

/**
 * 返信の登録処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function updateReply($items) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        //----------------------------------
        // 問い合わせ情報を更新。
        //----------------------------------
        // カラムと設定値を定義。
        $columns = array(
            "reply_native"   => $items["reply_nv"],
            "reply_japanese" => $items["reply_jp"],
            "reply_flag"     => "3"  // 返信済
        );
        $whereString = "id = :inq_id ";
        $whereParams = array(
            "inq_id" => $items["inq_id"]
        );

        // update処理を発行する。
        $db->update("tbl_inquiry", $columns, $whereString, $whereParams);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}
