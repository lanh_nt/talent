<?php

/**
 * 問い合わせ情報を取得し、結果を返す。
 *
 * @param $inqId 問い合わせID
 * 
 * @return array[] 取得結果
 */
function search($inqId) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQLを生成。
    $sql = "SELECT "
         .     "inquiries.id AS inq_id, "
         .     "inquiries.inquiry_no AS inq_no, "
         .     "inquiries.reply_flag AS status, "
         .     "DATE_FORMAT(inquiries.received_date,  '%Y/%m/%d') AS received_ymd, "
         .     "DATE_FORMAT(inquiries.reply_deadline, '%Y/%m/%d') AS deadline_ymd, "
         .     "workers.user_name AS user_name, "
         .     "inquiries.delivery_language AS lang_flag, "
         .     "inquiries.subject_native   AS subject_nv, "
         .     "inquiries.subject_japanese AS subject_jp, "
         .     "inquiries.content_native   AS content_nv, "
         .     "inquiries.content_japanese AS content_jp, "
         .     "inquiries.reply_native     AS reply_nv, "
         .     "inquiries.reply_japanese   AS reply_jp "
         . "FROM tbl_inquiry inquiries "
         . "LEFT JOIN mst_workers workers "
         .     "ON inquiries.worker_id = workers.id "
         . "WHERE inquiries.id = :inq_id "
    ;
    $params = array(
        ":inq_id" => $inqId
    );

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // 表示用データを追加設定。
    $langs = getOptionItems("comm", "lang");

    // コードに対応する文言を設定。
    $result["lang_string"] = getOprionItemValue($langs, $result["lang_flag"]);

    // 取得結果を返す。
    return $result;
}
