<?php

/**
 * 登録処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function insert($items) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        // お知らせIDを生成。
        $newInfoId = $db->getNewId("tbl_inquiry", "inquiry_no", 6);

        //----------------------------------
        // 問い合わせ情報を追加。
        //----------------------------------
        // カラムと設定値を定義。
        $columns = array(
            "inquiry_no"        => $newInfoId,
            "worker_id"         => $_SESSION['loginUserRowId'],
            "company_id"        => $_SESSION['loginCompanyRowId'],
            "received_date"     => date("Y/m/d"),
            "delivery_language" => $items["CN_01"],
            "subject_native"    => $items["CN_03"],
            "content_native"    => $items["CN_04"],
            "reply_flag"        => "1",  // 未返信(未翻訳)
            "reply_deadline"    => $items["CN_02"]
        );

        // insert処理を発行する。
        $db->insert("tbl_inquiry", $columns);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}
