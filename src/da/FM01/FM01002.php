<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * @param $multi 複数かどうか
 * 
 * @return PageData 検索結果
 */
function search($report_ids,$multi) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($report_ids, $whereSqls, $params);

    // SQL文を生成。
    //面談記録については管理者の帳票もある
    //帳票ステータステーブルを中心にして、
    //労働者テーブルと管理者テーブルを別で取得してUNIONする
    // ゴミデータ排除のため、外部結合したユーザーが存在しないレコードを除外
    $sql = "SELECT "
    ."rs.id as r_id, "
    ."rs.user_type, "
    ."rs.user_id, "
    ."(CASE WHEN rs.user_type='1' THEN adm.supervisor_id "
    ."      WHEN rs.user_type='2' THEN wks.worker_id "
    ."    ELSE  null END) as r_user_id, "
    ."(CASE WHEN rs.user_type='1' THEN adm.user_name "
    ."      WHEN rs.user_type='2' THEN wks.user_name "
    ."    ELSE  null END) as r_user_name, "
    ."(CASE WHEN rs.user_type='2' THEN wks.qualifications "
    ."    ELSE  null END) as r_qualifications, "
    ."(CASE WHEN rs.user_type='1' THEN DATE_FORMAT(adm.create_date,'%Y-%m-%d %H:%i') "
    ."      WHEN rs.user_type='2' THEN DATE_FORMAT(wks.create_date,'%Y-%m-%d %H:%i') "
    ."    ELSE  null END) as r_create_date, "
    ."(CASE WHEN rs.user_type='1' THEN cps1.company_name "
    ."      WHEN rs.user_type='2' THEN cps2.company_name "
    ."    ELSE  null END) as r_company_name, "
    ."(CASE WHEN rs.user_type='1' THEN cps1.id "
    ."      WHEN rs.user_type='2' THEN cps2.id "
    ."    ELSE  null END) as r_cmp_id, "
    ."rs.report_id, "
    ."rs.user_type as r_user_type, "
    ."rs.status, "
    ."DATE_FORMAT(rs.input_deadline,'%Y-%m-%d') as input_deadline, "
    ."DATE_FORMAT(rs.submission_deadline,'%Y-%m-%d') as submission_deadline, "
    ."rs.report_update_date "
    ."FROM tbl_report_status rs "
    ."LEFT JOIN mst_workers wks ON wks.id = rs.user_id "
    ."LEFT JOIN tbl_supervisor_info adm ON adm.id = rs.user_id "
    ."LEFT JOIN mst_company cps1 ON adm.company_id = cps1.id "
    ."LEFT JOIN mst_company cps2 ON wks.company_id = cps2.id "
    ;

    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;

        //$sql .= "WHERE rs.id in (" . $report_ids . ") ";
    }
    //$sql .= "ORDER BY update_date desc,user_id ";

    // SQL文を発行する。
    $rowsData = $db->select($sql, $params);

    // 表示用データを追加設定。
    $report_status = getOptionItems("comm", "report_status");
    $report_name = getOptionItems("comm", "report_name");

    // コードに対応する文言を設定。
    foreach ($rowsData as &$row) {
        $row["report_status_name"] = getOprionItemValue($report_status, $row["status"]);
        $row["report_name"] = getOprionItemValue($report_name, $row["report_id"]);        
    }

    // 取得結果を返す。
    return $rowsData;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $report_ids     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($report_ids, &$whereSqls, &$params) {
    //ID群を分解し、or条件にする

    $IDary = explode(",",$report_ids);
    $where = "";

    for($i=0 ; $i < count($IDary);$i++){
        if($i==0){
            $where = "rs.id=:report_id" . $i ." ";
        }else{
            $where .= " or rs.id=:report_id" . $i ." ";
        }
        $params[":report_id" . $i] = $IDary[$i];
    }
    $whereSqls[] = $where;

}
function getWhereParams2($report_id, &$whereSqls, &$params) {
    //ID群を分解し、or条件にする

    $where = "";

    $where = "id=:report_id ";

    $params["report_id"] = $report_id;

    $whereSqls[0] = $where;

}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $report_ids     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams3($report_ids, &$whereSql, &$params) {
    //ID群を分解し、or条件にする

    $IDary = explode(",",$report_ids);
    $whereID = "";
    $loopcnt = 0;
 
    for($i=0 ; $i < count($IDary);$i++){
        if (is_numeric($IDary[$i])){
            if($loopcnt>0){
                $whereID .= ",";
            }
            $whereID .= $IDary[$i];
            $loopcnt++;
        }
    }

    $where = "id in (" . $whereID . " ) ";
    $params["report_id"] = $whereID;

    $whereSql = $where;

}


/**
 * 更新処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function update($report_ids,$items) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {

        $IDary = explode(",",$report_ids);

        // カラムと設定値を定義。
        $columns = array(
            "status"              => $items["C_09"],      //ステータス
        );

        for($iLoop=0;$iLoop<count($IDary);$iLoop++){
            if(count($IDary) ==1){
                //無ければ触らない処理（安全策）
                if($items["input_deadline"]!=""){
                    $columns["input_deadline"] = $items["input_deadline"];    //入力期限
                }
                if($items["submission_deadline"]!=""){
                    $columns["submission_deadline"] = $items["submission_deadline"];    //入力期限
                }
            }
    
            // 同時に、SQLパラメータの設定も行う。
            $whereSqls = array();
            $params = array();
            getWhereParams2($IDary[$iLoop], $whereSqls, $params);
    
            $whereSqlString  ="";
    
            if (count($whereSqls) > 0) {
                $whereSqlString = $db->getWhereSql($whereSqls);
            }
    
            // update処理を発行する。
            $db->update("tbl_report_status", $columns, $whereSqlString, $params);
    
        }

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}

/**
 * データを新規登録する。
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 * 
 * @return integer レコード件数
 */
function pdfInsert($arg_arr) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        //-----------------------------------------------
        // PDF保存ファイル管理にデータを新規作成する。
        //-----------------------------------------------
        // カラムと設定値を定義。
        $columns = array();
        $columns["file_id" ] = $arg_arr["file_id"];
        $columns["company_id"  ] = $arg_arr["company_id"];
        $columns["user_type"  ] = $arg_arr["user_type"];
        $columns["user_id"  ] = $arg_arr["user_id"];
        $columns["report_id"  ] = $arg_arr["report_id"];

        // insert処理を発行する。
        $rc = $db->insert("tbl_save_pdf_file", $columns);

        // トランザクションをコミットする。
        $db->commit();
        return $rc;

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}
