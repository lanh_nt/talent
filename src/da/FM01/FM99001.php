<?php
/**
 * 検索を実行し、結果を返す。
 *
 * @param $r_id  検索条件 
 * 
 * @return PageData 検索結果
 */
function search_rireki($r_id) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParamsPdf($r_id, $whereSqls, $params);

    // SQL文を生成。
    //面談記録については管理者の帳票もある
    //帳票ステータステーブルを中心にして、
    //労働者テーブルと管理者テーブルを別で取得してUNIONする
    // ゴミデータ排除のため、外部結合したユーザーが存在しないレコードを除外
    $sql = "SELECT "
    ."id as pdf_id, "
    ."file_id, "
    ."company_id, "
    ."user_type, "
    ."user_id, "
    ."report_id, "
    ."create_id, "
    ."DATE_FORMAT(create_date,'%Y-%m-%d %H:%i') as create_date, "
    ."update_id, "
    ."DATE_FORMAT(update_date,'%Y-%m-%d %H:%i') as update_date "
    ."FROM tbl_save_pdf_file pdf "
    ;

    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY create_date desc ";

    // SQL文を発行する。
    $row = $db->selectone($sql, $params);


    // 取得結果を返す。
    return $row;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $r_id     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParamsPdf($r_id, &$whereSqls, &$params) {
    //ユーザー種別フラグ
    //$arg_arr["user_type"]
    //労働者または管理者ID
    //$arg_arr["user_id"]
    // 帳票ID
    //$arg_arr["report_id"]

    if ($r_id != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "id = :r_id ";
        $params[":r_id"] = $r_id;
    }

}

