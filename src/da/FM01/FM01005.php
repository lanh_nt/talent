<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * 
 * @return PageData 検索結果
 */
function search($report_ids) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($report_ids, $whereSqls, $params);

    // SQL文を生成。
    //面談記録については管理者の帳票もある
    //帳票ステータステーブルを中心にして、
    //労働者テーブルと管理者テーブルを別で取得してUNIONする
    // ゴミデータ排除のため、外部結合したユーザーが存在しないレコードを除外
    $sql = "SELECT "
    ."rs.id as r_id, "
    ."rs.user_type, "
    ."rs.user_id, "
    ."(CASE WHEN rs.user_type='1' THEN adm.user_id "
    ."      WHEN rs.user_type='2' THEN wks.worker_id "
    ."    ELSE  null END) as r_user_id, "
    ."(CASE WHEN rs.user_type='1' THEN adm.user_name "
    ."      WHEN rs.user_type='2' THEN wks.user_name "
    ."    ELSE  null END) as r_user_name, "
    ."(CASE WHEN rs.user_type='2' THEN wks.qualifications "
    ."    ELSE  null END) as r_qualifications, "
    ."(CASE WHEN rs.user_type='1' THEN DATE_FORMAT(adm.create_date,'%Y-%m-%d %H:%i') "
    ."      WHEN rs.user_type='2' THEN DATE_FORMAT(wks.create_date,'%Y-%m-%d %H:%i') "
    ."    ELSE  null END) as r_create_date, "
    ."(CASE WHEN rs.user_type='1' THEN cps1.company_name "
    ."      WHEN rs.user_type='2' THEN cps2.company_name "
    ."    ELSE  null END) as r_company_name, "
    ."rs.report_id, "
    ."rs.status, "
    ."DATE_FORMAT(rs.input_deadline,'%Y-%m-%d') as input_deadline, "
    ."DATE_FORMAT(rs.submission_deadline,'%Y-%m-%d') as submission_deadline, "
    ."rs.report_update_date "
    ."FROM tbl_report_status rs "
    ."LEFT JOIN mst_workers wks ON wks.id = rs.user_id "
    ."LEFT JOIN mst_admin_user adm ON adm.id = rs.user_id "
    ."LEFT JOIN mst_company cps1 ON adm.company_id = cps1.id "
    ."LEFT JOIN mst_company cps2 ON wks.company_id = cps2.id "
    ;

    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    //$sql .= "ORDER BY update_date desc,user_id ";

    // SQL文を発行する。
    $row = $db->selectOne($sql, $params);

    // 表示用データを追加設定。
    $report_status = getOptionItems("comm", "report_status");
    $report_name = getOptionItems("comm", "report_name");
    $qualification = getOptionItems("comm", "qualification");

    // コードに対応する文言を設定。
    $row["report_status_name"] = getOprionItemValue($report_status, $row["status"]);
    $row["report_name"] = getOprionItemValue($report_name, $row["report_id"]);
    $row["qualifications_name"] = getOprionItemValue($qualification, $row["r_qualifications"]);
    

    // 取得結果を返す。
    return $row;
}

/**
 * 検索を実行し、結果を返す。
 *
 * @param $arg_arr  検索条件 
 * 
 * @return PageData 検索結果
 */
function search_rireki($arg_arr) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParamsPdf($arg_arr, $whereSqls, $params);

    // SQL文を生成。
    //面談記録については管理者の帳票もある
    //帳票ステータステーブルを中心にして、
    //労働者テーブルと管理者テーブルを別で取得してUNIONする
    // ゴミデータ排除のため、外部結合したユーザーが存在しないレコードを除外
    $sql = "SELECT "
    ."id as pdf_id, "
    ."file_id, "
    ."company_id, "
    ."user_type, "
    ."user_id, "
    ."report_id, "
    ."create_id, "
    ."DATE_FORMAT(create_date,'%Y-%m-%d %H:%i') as create_date, "
    ."update_id, "
    ."DATE_FORMAT(update_date,'%Y-%m-%d %H:%i') as update_date "
    ."FROM tbl_save_pdf_file pdf "
    ;

    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY create_date desc ";

    // SQL文を発行する。
    $rows = $db->select($sql, $params);

    // 帳票ごとにランダムなトークンキーを追加。
    foreach ($rows as &$row) {
        $tokenValue = openssl_random_pseudo_bytes(16);
        $tokenString = bin2hex($tokenValue);
        $row["pdf_token"] = $tokenString;
    }

    // 取得結果を返す。
    return $rows;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $report_ids     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($report_ids, &$whereSqls, &$params) {
    // ・労働者ID 管理者ID
    if ($report_ids != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "rs.id in( :report_ids ) ";
        $params[":report_ids"] = $report_ids;
    }

}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $report_ids     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParamsPdf($arg_arr, &$whereSqls, &$params) {
    //ユーザー種別フラグ
    //$arg_arr["user_type"]
    //労働者または管理者ID
    //$arg_arr["user_id"]
    // 帳票ID
    //$arg_arr["report_id"]

    if ($arg_arr["user_type"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "user_type = :user_type ";
        $params[":user_type"] = $arg_arr["user_type"];
    }
    if ($arg_arr["user_id"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "user_id = :user_id ";
        $params[":user_id"] = $arg_arr["user_id"];
    }
    if ($arg_arr["report_id"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "report_id = :report_id ";
        $params[":report_id"] = $arg_arr["report_id"];
    }

}

