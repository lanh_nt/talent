<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * @param $pageNo ページ番号
 * 
 * @return PageData 検索結果
 */
function search($items, $pageNo) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    // SQL文を生成。
    //面談記録については管理者の帳票もある
    //帳票ステータステーブルを中心にして、
    //労働者テーブルと管理者テーブルを別で取得してUNIONする
    // ゴミデータ排除のため、外部結合したユーザーが存在しないレコードを除外
    $sql = "SELECT * FROM ( "
         .     "SELECT  "
         .         "rs.id, "
         .         "rs.user_type, "
         .         "wks.worker_id as u_id, "
         .         "wks.sex as sex, "
         .         "rs.report_id, "
         .         "rs.status, "
         .         "DATE_FORMAT(rs.update_date,'%Y-%m-%d %H:%i') as update_date, "
         .         "wks.company_id, "
         .         "wks.user_name as user_name, "
         .         "cps.company_name "
         .     "FROM tbl_report_status rs "
         .     "LEFT JOIN mst_workers wks ON wks.id = rs.user_id "
         .     "LEFT JOIN mst_company cps ON wks.company_id = cps.id  "
         .     "WHERE rs.user_type = 2 "  // 2:労働者
         .       "AND wks.worker_id IS NOT NULL "
         .   "UNION "
         .     "SELECT  "
         .         "rs.id, "
         .         "rs.user_type, "
         .         "adm.supervisor_id as u_id, "
         .         "NULL as sex, "   // 監督者に性別設定はないので、とりあえずnullとする。
         .         "rs.report_id, "
         .         "rs.status, "
         .         "DATE_FORMAT(rs.update_date,'%Y-%m-%d %H:%i') as update_date, "
         .         "adm.company_id, "
         .         "adm.user_name as user_name, "
         .         "cps.company_name "
         .     "FROM tbl_report_status rs "
         .     "LEFT JOIN tbl_supervisor_info adm ON adm.id = rs.user_id "
         .     "LEFT JOIN mst_company cps ON adm.company_id = cps.id  "
         .     "WHERE rs.user_type = 1 "  // 1:監督者
         .       "AND adm.supervisor_id IS NOT NULL "
         . ") TB "
    ;
    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY update_date desc,u_id ";

    // SQL文を発行する。
    $pageData = $db->getPageData($sql, $params, $pageNo);

    // 表示用データを追加設定。
    $report_status = getOptionItems("comm", "report_status");
    $report_name = getOptionItems("comm", "report_name");

    // コードに対応する文言を設定。
    foreach ($pageData->pageDatas as &$row) {
        $row["report_status_name"] = getOprionItemValue($report_status, $row["status"]);
        $row["report_name"] = getOprionItemValue($report_name, $row["report_id"]);
    }

    // 取得結果を返す。
    return $pageData;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $items     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($items, &$whereSqls, &$params) {
    // ・労働者ID 管理者ID
    if ($items["C_01"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "u_id = :u_id ";
        $params[":u_id"] = $items["C_01"];
    }

    // ・労働者氏名　 管理者氏名
    if ($items["C_02"] != "") {
        // 労働者姓/名に対して部分一致条件を設定。
        $whereSqls[] = "user_name LIKE :user_name ";
        $params[":user_name"] = "%".$items["C_02"]."%";
    }

    // ・性別
    if ($items["C_05_1"].$items["C_05_2"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c5Params = array();
        if ($items["C_05_1"] != "") $c5Params[] = "'1'";
        if ($items["C_05_2"] != "") $c5Params[] = "'2'";
        $c5ParamsString = joinArray($c5Params);
        $whereSqls[] = "(sex IN (".$c5ParamsString.") OR sex IS NULL)";
    }

    // ・企業名
    if ($items["C_06"] != "") {
        $whereSqls[] = "company_id = :company_id ";
        $params[":company_id"] = $items["C_06"];
    }

    // ・帳票
    if ($items["C_08_1"].$items["C_08_2"].$items["C_08_3"].$items["C_08_4"].$items["C_08_5"].$items["C_08_6"].$items["C_08_7"].$items["C_08_8"].$items["C_08_9"].$items["C_08_10"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c8Params = array();
        if ($items["C_08_1"] != "") $c8Params[] = "'01'";
        if ($items["C_08_2"] != "") $c8Params[] = "'02'";
        if ($items["C_08_3"] != "") $c8Params[] = "'03'";
        if ($items["C_08_4"] != "") $c8Params[] = "'04'";
        if ($items["C_08_5"] != "") $c8Params[] = "'05'";
        if ($items["C_08_6"] != "") $c8Params[] = "'06'";
        if ($items["C_08_7"] != "") $c8Params[] = "'07'";
        if ($items["C_08_8"] != "") $c8Params[] = "'08'";
        if ($items["C_08_9"] != "") $c8Params[] = "'09'";
        if ($items["C_08_10"] != "") $c8Params[] = "'10'";
        $c8ParamsString = joinArray($c8Params);
        $whereSqls[] = "report_id IN (".$c8ParamsString.") ";
    }

    // ・ステータス
    if ($items["C_09_1"].$items["C_09_2"].$items["C_09_3"].$items["C_09_4"].$items["C_09_5"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c9Params = array();
        if ($items["C_09_1"] != "") $c9Params[] = "'1'";
        if ($items["C_09_2"] != "") $c9Params[] = "'2'";
        if ($items["C_09_3"] != "") $c9Params[] = "'3'";
        if ($items["C_09_4"] != "") $c9Params[] = "'4'";
        if ($items["C_09_5"] != "") $c9Params[] = "'5'";
        $c9ParamsString = joinArray($c9Params);
        $whereSqls[] = "status IN (".$c9ParamsString.") ";
    }
    
    // ・登録日
    if ($items["C_07_1"] != "") {
        $whereSqls[] = "update_date >= STR_TO_DATE(:update_date_from, '%Y-%m-%d 00:00') ";
        $params[":update_date_from"] = $items["C_07_1"];
    }
    if ($items["C_07_2"] != "") {
        $whereSqls[] = "update_date < DATE_ADD(STR_TO_DATE(:update_date_to, '%Y-%m-%d 23:59'), INTERVAL 1 DAY) ";
        $params[":update_date_to"] = $items["C_07_2"];
    }
}


/**
 * 受入企業の一覧を取得する。
 *
 * @return array[行データ][キー => 値] 取得結果
 */
function getCompanies() {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
         .     "id, "
         .     "company_name AS name "
         . "FROM mst_company "
         . "WHERE classification = '1' "  // 受入機関
         . "ORDER BY company_name ";

    $params = array();

    // SQL文を発行する。
    $result = $db->select($sql, $params);

    // 取得結果を返す。
    return $result;
}
