<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * 
 * @return result 検索結果
 */
function search($cm_Id) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
     . "classification, "             //種別
     . "company_id, "                 //企業ID
     . "company_kana, "               //企業名(ふりがな）
     . "company_name, "               //企業名
     . "postal_code, "                //郵便番号
     . "corporation_no, "             //法人番号
     . "address, "                    //住所（所在地）
     . "tel, "                        //電話番号
     . "representative_name, "        //代表者の氏名
     . "representative_kana, "        //代表者の氏名(ふりがな)
     . "registration_no, "            //登録番号
     . "DATE_FORMAT(companydata.registration_date, '%Y/%m/%d') AS registration_date, "  //登録年月日
     . "supervisor_name1, "            //監督者氏名1
     . "supervisor_department1, "      //監督者所属部署1
     . "supervisor_position1, "        //監督者_役職1
     . "supervisor_name2, "            //監督者氏名2
     . "supervisor_department2, "      //監督者所属部署2
     . "supervisor_position2, "        //監督者_役職2
     . "supervisor_name3, "            //監督者氏名3
     . "supervisor_department3, "      //監督者所属部署3
     . "supervisor_position3, "        //監督者_役職3
     . "supervisor_name4, "            //監督者氏名4
     . "supervisor_department4, "      //監督者所属部署4
     . "supervisor_position4, "        //監督者_役職4
     . "supervisor_name5, "            //監督者氏名5
     . "supervisor_department5, "      //監督者所属部署5
     . "supervisor_position5, "        //監督者_役職5
     . "DATE_FORMAT(companydata.support_scheduled_date, '%Y/%m/%d') AS support_scheduled_date, "  //支援業務を開始する予定年月日 
     . "support_company_name, "        //支援を行う事業所の名称
     . "support_postal_code, "	       //支援を行う事務所の所在地　郵便番号
     . "support_address, "	           //支援を行う事務所の所在地　住所（所属地）
     . "support_tel, "	               //支援を行う事務所の所在地　電話番号
     . "support_manager_kana, "        //支援責任者 ふりがな
     . "support_manager_name, "        //支援責任者 氏名
     . "support_manager_department, "  //支援責任者 役職
     . "support_handler_kana, "        //支援担当者 ふりがな
     . "support_handler_name, "        //支援担当者 氏名
     . "support_handler_department, "  //支援担当者 役職
     . "available_language, "          //対応可能言語
     . "worker_mumber, "               //支援を行っている1号特定技能外国人数
     . "support_stuff_number, "        //支援担当者数
     . "industry_type, "               //業種
     . "industry_type_etc, "           //他の業種（複数）
     . "manufacturing_other, "         //製造業　その他内容
     . "wholesale_other, "             //卸売業　その他内容
     . "retail_other, "                //小売業　その他内容
     . "service_other, "               //その他サービス内容
     . "industry_other, "              //分類不能の産業内容
     . "capital, "                    //資本金
     . "annual_sales_amount, "        //年間売上金額（直近年度）
     . "company_stuff_count, "        //常勤職員数
     . "council_id, "                 //所属協議会
     . "council_apply_status "        //協議会申請書ステータス

     . "FROM mst_company  companydata "
     . "WHERE companydata.id = :cm_Id "
;

    $params = array();
    $params[":cm_Id"] = $cm_Id;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // コードに対応する文言を設定。
    $classification = getOptionItems("comm", "classification");
    $apply_status   = getOptionItems("comm", "apply_status");
    $industry   = getOptionItems("comm", "industry");

    $result["classification"]       = getOprionItemValue($classification, $result["classification"]);
    $result["council_apply_status"] = getOprionItemValue($apply_status, $result["council_apply_status"]);
    $result["industry_type"]        = getOprionItemValue($industry, $result["industry_type"]);
    
    // その他職業を選択肢毎に分割してセット
    $result["industry_type_etc"] = setIndustryType($result["industry_type_etc"], $industry);


    // 取得結果を返す。
    return $result;
}
/**
 * その他業種で選択した値を文字列セット
 *
 * @param array[値] $items 入力情報
 *  * @param array[値] $industry 業種情報
 *
 * @return string $c5ParamsString; データ
 */
function setIndustryType($items, $industry) {
    $industryType = "";

    $str ="";
    if ($items == null || $items =="") {
        return $industryType;
    }

    $industry_etc = array();
    $industry_etc = explode("," , $items);

    foreach($industry_etc as $bef) {
        if ($bef != "" && $industryType != "") {
            $industryType .= " , ";
        }
        if ($bef != null || $bef != "") {
            $bef = trim($bef);
            $bef = sprintf('%02d', $bef);
            $industryType .= getOprionItemValue($industry, $bef);
        }
    }
    
    return $industryType;
}

