<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * @param $pageNo ページ番号
 * 
 * @return PageData 検索結果
 */
function search($items, $pageNo) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    // SQLを生成。
    $sql = "SELECT "
    . "id as r_id, "
    . "company_id as u_id, "
    . "company_name, "
    . "corporation_no, "
    . "classification, "
    . "DATE_FORMAT(mst_company.update_date, '%Y/%m/%d') AS update_date "

    . "FROM mst_company "
    ;

    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY company_id ";

    // SQL文を発行する。
    $pageData = $db->getPageData($sql, $params, $pageNo);

    // 表示用データを追加設定。
    $classification           = getOptionItems("comm", "classification");

    // コードに対応する文言を設定。
    foreach ($pageData->pageDatas as &$row) {
        $row["classification"] = getOprionItemValue($classification,    $row["classification"]);
    }

    // 取得結果を返す。
    return $pageData;
}

/**
 * 検索条件に従ったWHERE条件データを生成。<br>
 * 同時に、SQLパラメータの設定も行う。
 *
 * @param $items     検索条件
 * @param $whereSqls WHERE句の各項目
 * @param $params    WHEREパラメータ
 */
function getWhereParams($items, &$whereSqls, &$params) {

    //受入企業所属の場合、自分の会社のみを対象にする▼▼▼
    if ($_SESSION['loginCompanyType'] == "1") {
        // 完全一致条件を設定。
        $whereSqls[] = "id = :c_id ";
        $params[":c_id"] = $_SESSION['loginCompanyRowId'];
    }

    // ・会社ID
    if ($items["C_01"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "company_id = :u_id ";
        $params[":u_id"] = $items["C_01"];
    }

    // ・法人番号
    if ($items["C_02"] != "") {
        // 完全一致条件を設定。
        $whereSqls[] = "corporation_no = :corporation_no ";
        $params[":corporation_no"] = $items["C_02"];
    }
    // ・企業名
    if ($items["C_03"] != "") {
        // 会社に対して部分一致条件を設定。
        $whereSqls[] = "company_name LIKE :company_name ";
        $params[":company_name"] = "%".$items["C_03"]."%";        
    }

    // ・種別
    if ($items["C_04_1"].$items["C_04_2"].$items["C_04_3"] != "") {
        // 被チェック項目に対応するIN条件を設定。
        $c5Params = array();
        if ($items["C_04_1"] != "") $c5Params[] = "'1'";
        if ($items["C_04_2"] != "") $c5Params[] = "'2'";
        if ($items["C_04_3"] != "") $c5Params[] = "'3'";
        $c5ParamsString = joinArray($c5Params);
        $whereSqls[] = "classification IN (".$c5ParamsString.") ";
    }
    // ・登録日
    if ($items["C_05_1"] != "") {
        $whereSqls[] = "update_date >= STR_TO_DATE(:update_date_from, '%Y-%m-%d 00:00') ";
        $params[":update_date_from"] = $items["C_05_1"];
    }
    if ($items["C_05_2"] != "") {
        $whereSqls[] = "update_date < DATE_ADD(STR_TO_DATE(:update_date_to, '%Y-%m-%d 23:59'), INTERVAL 1 DAY) ";
        $params[":update_date_to"] = $items["C_05_2"];
    }
}

/**
 * CSV出力用データを取得する。
 *
 * @param $items データ
 * 
 * @return array[] 取得結果
 */
function getExportDatas($items) {
    $db = new JinzaiDb(DB_DEFINE);

    // 検索条件に従ったWHERE条件データを生成。
    // 同時に、SQLパラメータの設定も行う。
    $whereSqls = array();
    $params = array();
    getWhereParams($items, $whereSqls, $params);

    // SQL文を生成。
    $sql = "SELECT "
    . "company_id as u_id, "          //    企業者ID
    . "company_name, "                //	企業名
    . "company_kana, "                //	企業名(ふりがな）
    . "classification, "              //	種別
    . "corporation_no, "              //	法人番号
    . "postal_code, "                 //	郵便番号
    . "address, "                     //	住所（所在地）
    . "tel, "                         //	電話番号
    . "representative_name, "         //	代表者の氏名
    . "representative_kana, "         //	代表者の氏名(ふりがな）
    . "registration_no, "             //	登録番号
    . "DATE_FORMAT(mst_company.registration_date, '%Y/%m/%d') AS registration_date, "  //支援業務を開始する予定年月日 
    . "supervisor_name1, "             //	監督者氏名1
    . "supervisor_department1, "       //	監督者所属部署1
    . "supervisor_position1, "        //    監督者_役職1
    . "supervisor_name2, "             //	監督者氏名2
    . "supervisor_department2, "       //	監督者所属部署2
    . "supervisor_position2, "         //   監督者_役職2
    . "supervisor_name3, "             //	監督者氏名3
    . "supervisor_department3, "       //	監督者所属部署3
    . "supervisor_position3, "         //   監督者_役職3
    . "supervisor_name4, "             //	監督者氏名4
    . "supervisor_department4, "       //	監督者所属部署4
    . "supervisor_position4, "         //   監督者_役職4
    . "supervisor_name5, "             //	監督者氏名5
    . "supervisor_department5, "       //	監督者所属部署5
    . "supervisor_position5, "         //   監督者_役職5
    . "DATE_FORMAT(mst_company.support_scheduled_date, '%Y/%m/%d') AS support_scheduled_date, "  //支援業務を開始する予定年月日 
    . "support_company_name, "        //	支援を行う事業所の名称
    . "support_postal_code, "         //	支援を行う事務所の所在地　郵便番号
    . "support_address, "             //	支援を行う事務所の所在地　住所（所属地）
    . "support_tel, "                 //	支援を行う事務所の所在地　電話番号
    . "support_manager_name, "        //	支援責任者 氏名
    . "support_manager_kana, "        //	支援責任者 ふりがな
    . "support_manager_department, "  //	支援責任者 役職
    . "support_handler_name, "        //	支援担当者 氏名
    . "support_handler_kana, "        //	支援担当者 ふりがな
    . "support_handler_department, "  //	支援担当者 役職
    . "available_language, "          //	対応可能言語
    . "worker_mumber, "               //	支援を行っている1号特定技能外国人数
    . "support_stuff_number, "        //	支援担当者数
    . "industry_type, "               //	業種
    . "industry_type_etc, "           //	他の業種（複数）
    . "manufacturing_other, "         //	製造業　その他内容
    . "wholesale_other, "             //	卸売業　その他内容
    . "retail_other, "                //	小売業　その他内容
    . "service_other, "               //	その他サービス内容
    . "industry_other, "              //	分類不能の産業内容
    . "capital, "                     //	資本金
    . "annual_sales_amount, "         //	年間売上金額（直近年度）
    . "company_stuff_count, "         //	常勤職員数
    . "council_id, "                  //	所属協議会
    . "council_apply_status "         //	協議会申請書ステータス

    . "FROM mst_company "
    ;

    if (count($whereSqls) > 0) {
        $whereSqlString = $db->getWhereSql($whereSqls);
        $sql .= "WHERE " . $whereSqlString;
    }
    $sql .= "ORDER BY company_id ";


    // SQL文を発行する。
    $result = $db->select($sql, $params);
    // 取得結果を返す。
    return $result;
}

/**
 * CSVデータを登録する。
 *
 * @param array[カラム] $row CSVデータ
 * 
 * @return boolean 実行成否
 */
function csvDataImport($row) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {

        $sql = "SELECT COUNT(*) AS cnt "
                . "FROM mst_company "
                . "WHERE company_id = :company_id ";
        $params[":company_id"] = $row[COL_COMPANY_ID];
        $result = $db->selectOne($sql, $params);
        $noMatchRecord = ($result["cnt"] == 0);
        if ($noMatchRecord) {
            // 行データあり&既存データなし : INSERT
            csvInsert($db, $row);
        } else {
            // 行データあり&既存データあり : UPDATE
            csvUpdate($db, $row);
        }
        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}
/**
 * データを新規登録する。
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 * 
 * @return integer レコード
 */
function csvInsert($db, $row) {
    //-----------------------------------------------
    // 企業にデータを新規作成する。
    //-----------------------------------------------

    // カラムと設定値を定義。
    $columns = array();
    $columns["company_id" ] = $row[COL_COMPANY_ID];
    setColumns($columns, $row);

    // insert処理を発行する。
    $rc = $db->insert("mst_company", $columns);

    //監督者を登録または更新する★★★
    setSupervisor($db, $row);

    return $rc;
}

/**
 * データを更新する。
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 * 
 * @return integer レコード
 */
function csvUpdate($db, $row) {
    //-----------------------------------------------
    // 企業データを更新する。
    //-----------------------------------------------

    // カラムと設定値を定義。
    $columns = array();
    setColumns($columns, $row);

    $whereString = "company_id = :company_id ";
    $whereParams = array(
        "company_id" => $row[COL_COMPANY_ID]
    );

    // update処理を発行する。
    $rc = $db->update("mst_company", $columns, $whereString, $whereParams);

    //監督者を登録または更新する★★★
    setSupervisor($db, $row);

    return $rc;
}

/**
 * 監督者を登録または更新する★★★
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 * 
 * @return integer レコード
 */
function setSupervisor($db, $row) {
    //-----------------------------------------------
    // 監督者データを作成する。
    //-----------------------------------------------
    $targetId = $db->getCompanyRowId($row[COL_COMPANY_ID]);

    // 監督者１～５人をセット
    $supervisor[] = $row[12];
    $supervisor[] = $row[15];
    $supervisor[] = $row[18];
    $supervisor[] = $row[21];
    $supervisor[] = $row[24];

    $department[] = $row[13];
    $department[] = $row[16];
    $department[] = $row[19];
    $department[] = $row[22];
    $department[] = $row[25];

    $position[] = $row[14];
    $position[] = $row[17];
    $position[] = $row[20];
    $position[] = $row[23];
    $position[] = $row[26];

    if ($supervisor == null || $supervisor == "") {
        return $rc;
    }
    $rc="";
    $super="";
    $depart ="";
    $pos ="";
    $user_id = 1;    
    for ($i=0; $i < 5; $i++) {
        $super = $supervisor[$i];
        $depart = $department[$i];
        $pos = $position[$i];
        
        // 監督者がいない場合、飛ばす
        if ($super == "" || $super == null) {
            $user_id = $user_id + 1;
            continue;
        }
        $str = sprintf('%02d', $user_id);
        $admin_id = $row[COL_COMPANY_ID]."_".$str;
        
        $rcolumns = array(
                "admin_id" => $admin_id
        );

        //監督者テーブルに存在するかチェックする（あれば更新、無ければ新規）
        $supervisor_sql = "SELECT COUNT(*) AS cnt "
        . "FROM tbl_supervisor_info "
        . "WHERE supervisor_id = :admin_id ";
        $result = $db->selectOne($supervisor_sql, $rcolumns);
        $noMatchRecord = ($result["cnt"] == 0);
        if ($noMatchRecord) {
            //-----------------------------------------------
            //監督者情報データを新規作成する。
            //-----------------------------------------------
            // カラムと設定値を定義。
            $rcolumns2 = array(
                "supervisor_id" => $admin_id,
                "company_id" => $targetId,
                "user_name"  => $super,
                "department" => $depart,
                "position"   => $pos
            );
            $rc = $db->insert("tbl_supervisor_info", $rcolumns2);

            $rcolumns3 = array(
                "admin_id" => getSupervisorRowId($db, $admin_id ),
            );

            // insert処理を発行する。
            $rc .= $db->insert("tbl_report_008", $rcolumns3);

            $stsColumns = array(
                "user_type" => "1",     // 1:監督者
                "user_id" => getSupervisorRowId($db, $admin_id ),
                "report_id" => "09",
                "status" => "1"
            );
            $rc = $db->insert("tbl_report_status", $stsColumns);            

        } else {
            //-----------------------------------------------
            //監督者情報データを更新する。
            //-----------------------------------------------
            // カラムと設定値を定義。
            $rcolumns3 = array(
                "user_name"  => $super,
                "department" => $depart,
                "position"   => $pos
            );
            $whereString3 = "supervisor_id = :admin_id ";
            $whereParams3 = array(
                "admin_id" => $admin_id
            );        
            // UPDATE処理を発行する。
            $rc = $db->update("tbl_supervisor_info", $rcolumns3, $whereString3, $whereParams3);
        }

        $user_id = $user_id + 1;

    }
    return $rc;
}

/**
 * 労働者の行IDを取得する。
 *
 * @param string $workerId 取得対象の労働者ID
 * 
 * @return integer 行ID
 */
function getSupervisorRowId($db,$supervisor_id) {
    $sql = "SELECT id AS rowid "
            . "FROM tbl_supervisor_info "
            . "WHERE supervisor_id = :supervisor_id "
    ;
    $params[":supervisor_id"] = $supervisor_id;
    
    $result = $db->selectOne($sql, $params);

    if ($result != null) {
        return $result["rowid"];
    } else {
        return null;
    }
}


/**
 * CSVレコードの内容から、更新用データを生成する。
 *
 * @param array[DBカラム名 => 値] $columns 更新用カラム一覧
 * @param array[値] $row CSV行データ
 */
function setColumn2s(&$columns, $companyId, $row) {

    
    
    $columns["company_id"] = $companyId;
                        
}
/**
 * CSVレコードの内容から、更新用データを生成する。
 *
 * @param array[DBカラム名 => 値] $columns 更新用カラム一覧
 * @param array[値] $row CSV行データ
 */
function setColumns(&$columns, $row) {
    $rowid = 1 - 1;

    // 事前ガイダンスの提供
    $columns["company_id"]                 = $row[$rowid++];
    $columns["company_name"]               = $row[$rowid++];
    $columns["company_kana"]               = $row[$rowid++];
    $columns["classification"]             = $row[$rowid++];
    $columns["corporation_no"]             = $row[$rowid++];
    $columns["postal_code"]                = $row[$rowid++];
    $columns["address"]                    = $row[$rowid++];
    $columns["tel"]                        = $row[$rowid++];
    $columns["representative_name"]        = $row[$rowid++];
    $columns["representative_kana"]        = $row[$rowid++];
    $columns["registration_no"]            = $row[$rowid++];
    $columns["registration_date"]          = setDateParam($row[$rowid++]);
    $columns["supervisor_name1"]           = $row[$rowid++];
    $columns["supervisor_department1"]     = $row[$rowid++];
    $columns["supervisor_position1"]       = $row[$rowid++];
    $columns["supervisor_name2"]           = $row[$rowid++];
    $columns["supervisor_department2"]     = $row[$rowid++];
    $columns["supervisor_position2"]       = $row[$rowid++];
    $columns["supervisor_name3"]           = $row[$rowid++];
    $columns["supervisor_department3"]     = $row[$rowid++];
    $columns["supervisor_position3"]       = $row[$rowid++];
    $columns["supervisor_name4"]           = $row[$rowid++];
    $columns["supervisor_department4"]     = $row[$rowid++];
    $columns["supervisor_position4"]       = $row[$rowid++];
    $columns["supervisor_name5"]           = $row[$rowid++];
    $columns["supervisor_department5"]     = $row[$rowid++];
    $columns["supervisor_position5"]       = $row[$rowid++];
    $columns["support_scheduled_date"]     = setDateParam($row[$rowid++]);
    $columns["support_company_name"]       = $row[$rowid++];
    $columns["support_postal_code"]        = $row[$rowid++];
    $columns["support_address"]            = $row[$rowid++];
    $columns["support_tel"]                = $row[$rowid++];
    $columns["support_manager_name"]       = $row[$rowid++];
    $columns["support_manager_kana"]       = $row[$rowid++];
    $columns["support_manager_department"] = $row[$rowid++];
    $columns["support_handler_name"]       = $row[$rowid++];
    $columns["support_handler_kana"]       = $row[$rowid++];
    $columns["support_handler_department"] = $row[$rowid++];
    $columns["available_language"]         = $row[$rowid++];
    $columns["worker_mumber"]              = $row[$rowid++];
    $columns["support_stuff_number"]       = $row[$rowid++];
    $columns["industry_type"]              = sprintf('%02d', $row[$rowid++]);
    $columns["industry_type_etc"]          = $row[$rowid++];
    $columns["manufacturing_other"]        = $row[$rowid++];
    $columns["wholesale_other"]            = $row[$rowid++];
    $columns["retail_other"]               = $row[$rowid++];
    $columns["service_other"]              = $row[$rowid++];
    $columns["industry_other"]             = $row[$rowid++];
    $columns["capital"]                    = $row[$rowid++];
    $columns["annual_sales_amount"]        = $row[$rowid++];
    $columns["company_stuff_count"]        = $row[$rowid++];
    $columns["council_id"]                 = $row[$rowid++];
    $columns["council_apply_status"]       = $row[$rowid++];
                        
}
/**
 * データを削除する。
 *
 * @param $row 行データ
 * 
 * @return boolean 実行成否
 */
function csvDelete($row) {

    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {
        //-----------------------------------------------
        // 企業情報マスタデータを削除する。
        //-----------------------------------------------
        $whereString = "company_id = :company_id ";
        $whereParams = array(
            "company_id" => $row[COL_COMPANY_ID]
        );
        // delete処理を発行する。
        $db->delete("mst_company", $whereString, $whereParams);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}

/**
 * 労働者マスタに企業IDかあるかチェック。
 *
 * @param string $companyId 取得対象の企業ID
 * 
 * @return boolean 判定結果
 */
function isEmptyCompanyRecord($companyId) {
    $db = new JinzaiDb(DB_DEFINE);

    $targetId = $db->getCompanyRowId($companyId);

    $sql = "SELECT company_name "
         . "FROM mst_workers "
         . "WHERE company_id = :company_id ";
    $params[":company_id"] = $targetId;
    $result = $db->selectOne($sql, $params);

        if ($result["company_id"] != "") {
            // 紐づく企業がある場合は、「空でない」を返して終了。
            return false;
        }


    // 「紐づく企業が空である」を返す。
    return true;
}

