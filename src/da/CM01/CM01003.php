<?php

/**
 * 検索を実行し、結果を返す。
 *
 * @param $items  検索条件
 * 
 * @return result 検索結果
 */
function search($cm_Id) {
    $db = new JinzaiDb(DB_DEFINE);

    // SQL文を生成。
    $sql = "SELECT "
     . "classification, "             //種別
     . "company_id, "                 //企業ID
     . "company_kana, "               //企業名(ふりがな）
     . "company_name, "               //企業名
     . "corporation_no, "             //法人番号
     . "postal_code, "                //郵便番号
     . "address, "                    //住所（所在地）
     . "tel, "                        //電話番号
     . "representative_name, "        //代表者の氏名
     . "representative_kana, "        //代表者の氏名(ふりがな)
     . "registration_no, "            //登録番号
     . "DATE_FORMAT(companydata.registration_date, '%Y') AS registration_date_year, "  //登録年月日
     . "DATE_FORMAT(companydata.registration_date, '%m') AS registration_date_month, "  //登録年月日
     . "DATE_FORMAT(companydata.registration_date, '%d') AS registration_date_day, "  //登録年月日
     . "supervisor_name1, "            //監督者氏名1
     . "supervisor_department1, "      //監督者所属部署1
     . "supervisor_position1, "        //監督者_役職1
     . "supervisor_name2, "            //監督者氏名2
     . "supervisor_department2, "      //監督者所属部署2
     . "supervisor_position2, "        //監督者_役職2
     . "supervisor_name3, "            //監督者氏名3
     . "supervisor_department3, "      //監督者所属部署3
     . "supervisor_position3, "        //監督者_役職3
     . "supervisor_name4, "            //監督者氏名4
     . "supervisor_department4, "      //監督者所属部署4
     . "supervisor_position4, "        //監督者_役職4
     . "supervisor_name5, "            //監督者氏名5
     . "supervisor_department5, "      //監督者所属部署5
     . "supervisor_position5, "        //監督者_役職5
     . "DATE_FORMAT(companydata.support_scheduled_date, '%Y') AS support_scheduled_date_year, "  //登録年月日
     . "DATE_FORMAT(companydata.support_scheduled_date, '%m') AS support_scheduled_date_month, " //登録年月日
     . "DATE_FORMAT(companydata.support_scheduled_date, '%d') AS support_scheduled_date_day, "  //登録年月日
     . "support_company_name, "        //支援を行う事業所の名称
     . "support_postal_code, "	       //支援を行う事務所の所在地　郵便番号
     . "support_address, "	           //支援を行う事務所の所在地　住所（所属地）
     . "support_tel, "	               //支援を行う事務所の所在地　電話番号
     . "support_manager_kana, "        //支援責任者 ふりがな
     . "support_manager_name, "        //支援責任者 氏名
     . "support_manager_department, "  //支援責任者 役職
     . "support_handler_kana, "        //支援担当者 ふりがな
     . "support_handler_name, "        //支援担当者 氏名
     . "support_handler_department, "  //支援担当者 役職
     . "available_language, "          //対応可能言語
     . "worker_mumber, "               //支援を行っている1号特定技能外国人数
     . "support_stuff_number, "        //支援担当者数
     . "industry_type, "               //業種
     . "industry_type_etc, "           //他の業種（複数）
     . "manufacturing_other, "         //製造業　その他内容
     . "wholesale_other, "             //卸売業　その他内容
     . "retail_other, "                //小売業　その他内容
     . "service_other, "               //その他サービス内容
     . "industry_other, "              //分類不能の産業内容
     . "capital, "                     //資本金
     . "annual_sales_amount, "         //年間売上金額（直近年度）
     . "company_stuff_count, "         //常勤職員数
     . "council_id, "                  //所属協議会
     . "council_apply_status "         //協議会申請書ステータス

     . "FROM mst_company  companydata "
     . "WHERE companydata.id = :cm_Id "
;

    $params = array();
    $params[":cm_Id"] = $cm_Id;

    // SQL文を発行する。
    $result = $db->selectOne($sql, $params);

    // その他職業を選択肢毎に分割してセット
         $str = array();
        $str = explode("," , $result["industry_type_etc"]);
        $result["industry_type_etc01"] = setIndustry($str, "01", $result["industry_type_etc"]);
        $result["industry_type_etc02"] = setIndustry($str, "02",  $result["industry_type_etc"]);
        $result["industry_type_etc03"] = setIndustry($str, "03" , $result["industry_type_etc"]);
        $result["industry_type_etc04"] = setIndustry($str, "04" , $result["industry_type_etc"]);
        $result["industry_type_etc05"] = setIndustry($str, "05" , $result["industry_type_etc"]);
        $result["industry_type_etc06"] = setIndustry($str, "06" , $result["industry_type_etc"]);
        $result["industry_type_etc07"] = setIndustry($str, "07" , $result["industry_type_etc"]);
        $result["industry_type_etc08"] = setIndustry($str, "08" , $result["industry_type_etc"]);
        $result["industry_type_etc09"] = setIndustry($str, "09" , $result["industry_type_etc"]);
        $result["industry_type_etc10"] = setIndustry($str, "10" , $result["industry_type_etc"]);
        $result["industry_type_etc11"] = setIndustry($str, "11" , $result["industry_type_etc"]);
        $result["industry_type_etc12"] = setIndustry($str, "12" , $result["industry_type_etc"]);
        $result["industry_type_etc13"] = setIndustry($str, "13" , $result["industry_type_etc"]);
        $result["industry_type_etc14"] = setIndustry($str, "14" , $result["industry_type_etc"]);
        $result["industry_type_etc15"] = setIndustry($str, "15" , $result["industry_type_etc"]);
        $result["industry_type_etc16"] = setIndustry($str, "16" , $result["industry_type_etc"]);
        $result["industry_type_etc17"] = setIndustry($str, "17" , $result["industry_type_etc"]);
        $result["industry_type_etc18"] = setIndustry($str, "18" , $result["industry_type_etc"]);
        $result["industry_type_etc19"] = setIndustry($str, "19" , $result["industry_type_etc"]);
        $result["industry_type_etc20"] = setIndustry($str, "20" , $result["industry_type_etc"]);
        $result["industry_type_etc21"] = setIndustry($str, "21" , $result["industry_type_etc"]);
        $result["industry_type_etc22"] = setIndustry($str, "22" , $result["industry_type_etc"]);
        $result["industry_type_etc23"] = setIndustry($str, "23" , $result["industry_type_etc"]);
        $result["industry_type_etc24"] = setIndustry($str, "24" , $result["industry_type_etc"]);
        $result["industry_type_etc25"] = setIndustry($str, "25" , $result["industry_type_etc"]);
        $result["industry_type_etc26"] = setIndustry($str, "26" , $result["industry_type_etc"]);
        $result["industry_type_etc27"] = setIndustry($str, "27" , $result["industry_type_etc"]);
        $result["industry_type_etc28"] = setIndustry($str, "28" , $result["industry_type_etc"]);
        $result["industry_type_etc29"] = setIndustry($str, "29" , $result["industry_type_etc"]);
        $result["industry_type_etc30"] = setIndustry($str, "30" , $result["industry_type_etc"]);
        $result["industry_type_etc31"] = setIndustry($str, "31" , $result["industry_type_etc"]);
        $result["industry_type_etc32"] = setIndustry($str, "32" , $result["industry_type_etc"]);
        $result["industry_type_etc33"] = setIndustry($str, "33" , $result["industry_type_etc"]);
        $result["industry_type_etc34"] = setIndustry($str, "34" , $result["industry_type_etc"]);
        $result["industry_type_etc35"] = setIndustry($str, "35" , $result["industry_type_etc"]);
        $result["industry_type_etc36"] = setIndustry($str, "36" , $result["industry_type_etc"]);
        $result["industry_type_etc37"] = setIndustry($str, "37" , $result["industry_type_etc"]);
        $result["industry_type_etc38"] = setIndustry($str, "38" , $result["industry_type_etc"]);
        $result["industry_type_etc39"] = setIndustry($str, "39" , $result["industry_type_etc"]);
        $result["industry_type_etc40"] = setIndustry($str, "40" , $result["industry_type_etc"]);
        $result["industry_type_etc41"] = setIndustry($str, "41" , $result["industry_type_etc"]);
        $result["industry_type_etc42"] = setIndustry($str, "42" , $result["industry_type_etc"]);
        $result["industry_type_etc43"] = setIndustry($str, "43" , $result["industry_type_etc"]);
        $result["industry_type_etc44"] = setIndustry($str, "44" , $result["industry_type_etc"]);
        $result["industry_type_etc45"] = setIndustry($str, "45" , $result["industry_type_etc"]);
        $result["industry_type_etc46"] = setIndustry($str, "46" , $result["industry_type_etc"]);
        $result["industry_type_etc47"] = setIndustry($str, "47" , $result["industry_type_etc"]);
    


    // 取得結果を返す。
    return $result;
}
/**
 * 登録処理を実行する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function update($items) {
    $db = new JinzaiDb(DB_DEFINE);

    // トランザクションを開始する。
    $db->beginTransaction();

    try {

        // 年、月、日を「年/月/日」にセット
        $registration_date      = setDate($items["registration_date_year"], $items["registration_date_month"], $items["registration_date_day"]);
        $support_scheduled_date = setDate($items["support_scheduled_date_year"], $items["support_scheduled_date_month"], $items["support_scheduled_date_day"]);
        $items["industry_type_etc"] = setIndustryType($items);

        // カラムと設定値を定義。
        $columns = array(
            "company_id"                    => $items["company_id"],
            "company_name"                  => $items["company_name"],
            "company_kana"                  => $items["company_kana"],
            "classification"                => $items["classification"],
            "corporation_no"                => $items["corporation_no"],
            "postal_code"                   => $items["postal_code"],
            "address"                       => $items["address"],
            "tel"                           => $items["tel"],
            "representative_name"           => $items["representative_name"],
            "representative_kana"           => $items["representative_kana"],
            "registration_no"               => $items["registration_no"],
            "registration_date"             => setDateParam($registration_date),
            "supervisor_name1"              => $items["supervisor_name1"],
            "supervisor_department1"        => $items["supervisor_department1"],
            "supervisor_position1"          => $items["supervisor_position1"],
            "supervisor_name2"              => $items["supervisor_name2"],            
            "supervisor_department2"        => $items["supervisor_department2"],
            "supervisor_position2"          => $items["supervisor_position2"],
            "supervisor_name3"              => $items["supervisor_name3"],
            "supervisor_department3"        => $items["supervisor_department3"],
            "supervisor_position3"          => $items["supervisor_position3"],
            "supervisor_name4"              => $items["supervisor_name4"],
            "supervisor_department4"        => $items["supervisor_department4"],
            "supervisor_position4"          => $items["supervisor_position4"],
            "supervisor_name5"              => $items["supervisor_name5"],
            "supervisor_department5"        => $items["supervisor_department5"],
            "supervisor_position5"          => $items["supervisor_position5"],
            "support_scheduled_date"        => setDateParam($support_scheduled_date),
            "support_company_name"          => $items["support_company_name"],
            "support_manager_name"          => $items["support_manager_name"],
            "support_manager_kana"          => $items["support_manager_kana"],
            "support_manager_department"    => $items["support_manager_department"],
            "support_handler_name"          => $items["support_handler_name"],
            "support_handler_kana"          => $items["support_handler_kana"],
            "support_handler_department"    => $items["support_handler_department"],
            "support_postal_code"           => $items["support_postal_code"],
            "support_address"               => $items["support_address"],
            "support_tel"                   => $items["support_tel"],
            "available_language"            => $items["available_language"],
            "worker_mumber"                 => $items["worker_mumber"],
            "support_stuff_number"          => $items["support_stuff_number"],
            "industry_type"                 => $items["industry_type"],
            "industry_type_etc"             => $items["industry_type_etc"],
            "manufacturing_other"           => $items["manufacturing_other"],
            "wholesale_other"               => $items["wholesale_other"],
            "retail_other"                  => $items["retail_other"],
            "service_other"                 => $items["service_other"],
            "industry_other"                => $items["industry_other"],
            "capital"                       => $items["capital"],
            "annual_sales_amount"           => $items["annual_sales_amount"],
            "company_stuff_count"           => $items["company_stuff_count"],
            "council_id"                    => $items["council_id"],
            "council_apply_status"          => $items["council_apply_status"]    
        );
        $whereString = "id = :id ";
        $whereParams = array(
            "id" => $items["cm_id"]
        );
    
        // update処理を発行する。
        $db->update("mst_company", $columns, $whereString, $whereParams);


        //監督者を登録または更新する★★★
        setSupervisor($db,$items);

        // トランザクションをコミットする。
        $db->commit();

    } catch(Exception $e) {
        // トランザクションをロールバックする。
        $db->rollback($e);

        return false;
    }

    return true;
}
/**
 * 年を返す。
 *
 * @return 
 */
function getYears() {
    $year = "1950";
    $count = 80;
    $years = array();
    for ($i=0; $i < $count; $i++) {
        $years[] = $year;  
        $year++;
    }
    return $years;
}
/**
 * 月を返す。
 *
 * @return array[項目名] フォーム項目名の一覧
 */
function getMonths() {
    return array(
        "01", "02", "03", "04", "05", "06","07","08","09","10","11","12"
    );
}
/**
 * 日を返す。
 *
 * @return array[項目名] フォーム項目名の一覧
 */
function getDays() {
    return array(
        "01", "02", "03", "04", "05", "06","07","08","09","10","11","12","13","14","15",
        "16", "17", "18", "19", "20", "21","22","23","24","25","26","27","28","29","30",
        "31"

    );
}
/**
 * その他業種で選択した値を文字列セット
 *
 * @param array[値] $items 入力情報
 *
 * @return string $c5ParamsString; データ
 */
function setIndustryType($items) {
        // 被チェック項目に対応するIN条件を設定。
        $c5Params = array();

        if ($items["industry_type_etc01"] != "") $c5Params[] = "01";
        if ($items["industry_type_etc02"] != "") $c5Params[] = "02";
        if ($items["industry_type_etc03"] != "") $c5Params[] = "03";
        if ($items["industry_type_etc04"] != "") $c5Params[] = "04";
        if ($items["industry_type_etc05"] != "") $c5Params[] = "05";
        if ($items["industry_type_etc06"] != "") $c5Params[] = "06";
        if ($items["industry_type_etc07"] != "") $c5Params[] = "07";
        if ($items["industry_type_etc08"] != "") $c5Params[] = "08";
        if ($items["industry_type_etc09"] != "") $c5Params[] = "09";
        if ($items["industry_type_etc10"] != "") $c5Params[] = "10";
        if ($items["industry_type_etc11"] != "") $c5Params[] = "11";
        if ($items["industry_type_etc12"] != "") $c5Params[] = "12";
        if ($items["industry_type_etc13"] != "") $c5Params[] = "13";
        if ($items["industry_type_etc14"] != "") $c5Params[] = "14";
        if ($items["industry_type_etc15"] != "") $c5Params[] = "15";
        if ($items["industry_type_etc16"] != "") $c5Params[] = "16";
        if ($items["industry_type_etc17"] != "") $c5Params[] = "17";
        if ($items["industry_type_etc18"] != "") $c5Params[] = "18";
        if ($items["industry_type_etc19"] != "") $c5Params[] = "19";
        if ($items["industry_type_etc20"] != "") $c5Params[] = "20";
        if ($items["industry_type_etc21"] != "") $c5Params[] = "21";
        if ($items["industry_type_etc22"] != "") $c5Params[] = "22";
        if ($items["industry_type_etc23"] != "") $c5Params[] = "23";
        if ($items["industry_type_etc24"] != "") $c5Params[] = "24";
        if ($items["industry_type_etc25"] != "") $c5Params[] = "25";
        if ($items["industry_type_etc26"] != "") $c5Params[] = "26";
        if ($items["industry_type_etc27"] != "") $c5Params[] = "27";
        if ($items["industry_type_etc28"] != "") $c5Params[] = "28";
        if ($items["industry_type_etc29"] != "") $c5Params[] = "29";
        if ($items["industry_type_etc30"] != "") $c5Params[] = "30";
        if ($items["industry_type_etc31"] != "") $c5Params[] = "31";
        if ($items["industry_type_etc32"] != "") $c5Params[] = "32";
        if ($items["industry_type_etc33"] != "") $c5Params[] = "33";
        if ($items["industry_type_etc34"] != "") $c5Params[] = "34";
        if ($items["industry_type_etc35"] != "") $c5Params[] = "35";
        if ($items["industry_type_etc36"] != "") $c5Params[] = "36";
        if ($items["industry_type_etc37"] != "") $c5Params[] = "37";
        if ($items["industry_type_etc38"] != "") $c5Params[] = "38";
        if ($items["industry_type_etc39"] != "") $c5Params[] = "39";
        if ($items["industry_type_etc40"] != "") $c5Params[] = "40";
        if ($items["industry_type_etc41"] != "") $c5Params[] = "41";
        if ($items["industry_type_etc42"] != "") $c5Params[] = "42";
        if ($items["industry_type_etc43"] != "") $c5Params[] = "43";
        if ($items["industry_type_etc44"] != "") $c5Params[] = "44";
        if ($items["industry_type_etc45"] != "") $c5Params[] = "45";
        if ($items["industry_type_etc46"] != "") $c5Params[] = "46";
        if ($items["industry_type_etc47"] != "") $c5Params[] = "47";

        $c5ParamsString = joinArray($c5Params);
        
        return $c5ParamsString;
}
/**
 * 日付をセットする。
 *
 * @param string $year   年
 * @param string $month  月
 * @param string $day    日
 * 
 * @return string 日付
 */
function setDate($year, $month,$day) {
    $date ="";
    if ($year !="" && $month !="" && $day !="") {
        // YYYY/MM/DDにセット
        $date = $year."/".$month."/".$day;
    }
    return $date;
}
/**
 * 検索し、一致した場合、職業にセットする。
 *
 * @param array $items   値
 * @param string $number  番号
 * 
 * @return string industryType
 */
function setIndustry($items, $number, $industry_type_etc) {

    $industryType = "";

    if ($industry_type_etc == null || $industry_type_etc =="") {
        return $industryType;
    }
    foreach($items as $item => $value) {
        if ($value == $number) {
            $industryType = $number;
            break;
        }
    }
    return $industryType;
}
/**
 * 監督者を登録または更新する★★★
 *
 * @param $db  DBアクセスクラス
 * @param $row 行データ
 * 
 * @return integer レコード
 */
function setSupervisor($db,$items) {
    //-----------------------------------------------
    // 監督者データを作成する。
    //-----------------------------------------------

    // 監督者１～５人をセット
    $supervisor[] = $items["supervisor_name1"];
    $supervisor[] = $items["supervisor_name2"];
    $supervisor[] = $items["supervisor_name3"];
    $supervisor[] = $items["supervisor_name4"];
    $supervisor[] = $items["supervisor_name5"];

    $department[] = $items["supervisor_department1"];
    $department[] = $items["supervisor_department2"];
    $department[] = $items["supervisor_department3"];
    $department[] = $items["supervisor_department4"];
    $department[] = $items["supervisor_department5"];

    $position[] = $items["supervisor_position1"];
    $position[] = $items["supervisor_position2"];
    $position[] = $items["supervisor_position3"];
    $position[] = $items["supervisor_position4"];
    $position[] = $items["supervisor_position5"];

    $targetId = $db->getCompanyRowId($items["company_id"]);

    if ($supervisor == null || $supervisor == "") {
        return $rc;
    }
    $rc="";
    $super="";
    $depart ="";
    $pos ="";
    $user_id = "1";    
    for ($i=0; $i < 5; $i++) {
        $super = $supervisor[$i];
        $depart = $department[$i];
        $pos = $position[$i];
        
        // 監督者がいない場合、飛ばす
        if ($super == "" || $super == null) {
            $user_id = $user_id + 1;
            continue;
        }
        $str = sprintf('%02d', $user_id);
        $admin_id = $items["company_id"]."_".$str;
        
        $rcolumns = array(
                "admin_id" => $admin_id
        );

        //監督者テーブルに存在するかチェックする（あれば更新、無ければ新規）
        $supervisor_sql = "SELECT COUNT(*) AS cnt "
        . "FROM tbl_supervisor_info "
        . "WHERE supervisor_id = :admin_id ";
        $result = $db->selectOne($supervisor_sql, $rcolumns);
        $noMatchRecord = ($result["cnt"] == 0);

        if ($noMatchRecord) {
            //-----------------------------------------------
            //監督者情報データを新規作成する。
            //-----------------------------------------------
            // カラムと設定値を定義。
            $rcolumns2 = array(
                "supervisor_id" => $admin_id,
                "company_id" => $targetId,
                "user_name"  => $super,
                "department" => $depart,
                "position"   => $pos
            );
            $rc .= $db->insert("tbl_supervisor_info", $rcolumns2);

            $rcolumns3 = array(
                "admin_id" => getSupervisorRowId($db, $admin_id ),
            );

            // insert処理を発行する。
            $rc .= $db->insert("tbl_report_008", $rcolumns3);


            $stsColumns = array(
                "user_type" => "1",     // 1:監督者
                "user_id" => getSupervisorRowId($db, $admin_id ),
                "report_id" => "09",
                "status" => "1"
            );
            $rc = $db->insert("tbl_report_status", $stsColumns);                 

        } else {

            //-----------------------------------------------
            //監督者情報データを更新する。
            //-----------------------------------------------
            // カラムと設定値を定義。
            $rcolumns3 = array(
                "user_name"  => $super,
                "department" => $depart,
                "position"   => $pos
            );
            $whereString3 = "supervisor_id = :admin_id ";
            $whereParams3 = array(
                "admin_id" => $admin_id
            );        
            // UPDATE処理を発行する。
            $rc .= $db->update("tbl_supervisor_info", $rcolumns3, $whereString3, $whereParams3);
        }
        $user_id = $user_id + 1;
    }
    return $rc;
}

/**
 * 労働者の行IDを取得する。
 *
 * @param string $workerId 取得対象の労働者ID
 * 
 * @return integer 行ID
 */
function getSupervisorRowId($db,$supervisor_id) {
    $sql = "SELECT id AS rowid "
            . "FROM tbl_supervisor_info "
            . "WHERE supervisor_id = :supervisor_id "
    ;
    $params[":supervisor_id"] = $supervisor_id;
    
    $result = $db->selectOne($sql, $params);

    if ($result != null) {
        return $result["rowid"];
    } else {
        return null;
    }
}

