<?php
/**
 * 管理者管理 詳細画面
 * 
 */
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/AD01/AD01002.php';

const FUNC_ID  = "AD01";
const SCENE_ID = "AD01002";

const MODE_LOAD   = "load";
const MODE_SEARCH = "search";
const MODE_DETAIL = "detail";
const MODE_UPDATE = "update";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
//　※労働者メニューではないので省略？
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。 ★★★
$condItemNames = array(
    "C_01",     //ID
    "C_02",     //氏名
    "C_03",    //企業名
    "C_04",    //役職
    "C_05_1",  "C_05_2",  //性別
    "C_06_1",  "C_06_2", "C_06_3",  //種別
    "C_07_1",  "C_07_2",  //登録日
    "page_no", "is_search", "mode", "ad_id"
);

// POST内容から構成。
$items = getParamsArray($_POST, $condItemNames);

$items["ad_id"] = $_SESSION['ad_id'];


// モードに対応する処理を実行。
if ($mode == MODE_SEARCH) {
    // 検索一覧へ移動。
    goForwardPage("../AD01/AD01001", $items);
    exit();

}
if ($mode == MODE_UPDATE) {
    // ------------------------------------------
    // 編集ボタン処理
    // ------------------------------------------
    // 編集画面に遷移する。
    goForwardPage("../AD01/AD01003", $items);
    exit();

}
// 検索処理を実行。
$rowdatas = search($items["ad_id"]);


// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("rowdata", $rowdatas);

// Smartyに変数をバインド。
$smarty->assign("labels", getLabels("AD01", "AD01002", $_SESSION['loginUserLang']));

// 画面を表示する。
$smarty->display('AD01/AD01002.tpl');

