<?php
/**
 * 企業管理 検索画面
 * 
 */
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../lib/PageData.php';
require_once '../../lib/ses_mailSender.php';
require_once '../../da/AD01/AD01001.php';

use League\Csv\Writer;
use League\Csv\Reader;

const FUNC_ID  = "AD01";
const SCENE_ID = "AD01001";

const MODE_LOAD   = "load";
const MODE_SEARCH = "search";
const MODE_DETAIL = "detail";
const MODE_IMPORT = "csv-import";
const MODE_EXPORT = "csv-export";
const MODE_FORMAT = "csv-format";

const CSV_FORMAT_NAME = "AD01001";
const CSV_OUTPUT_NAME = "管理情報";

// CSVデータのカラム数
const COLUMN_COUNT = 7;
const COL_MAILADDRESS = 1 - 1;
const COL_COMPANY_ID  = 2 - 1;
const COL_USER_NAME   = 3 - 1;
const COL_CLASSIFY    = 4 - 1;
const COL_SEX         = 6 - 1;

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
//　※労働者メニューではないので省略？
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。 ★★★
$condItemNames = array(
    "C_01",     //ID
    "C_02",     //氏名
    "C_03",    //企業名
    "C_04",    //役職
    "C_05_1",  "C_05_2",  //性別
    "C_06_1",  "C_06_2", "C_06_3",  //種別
    "C_07_1",  "C_07_2",  //登録日
    "page_no", "is_search", "mode", "ad_id"
);

// POST内容から構成。
$items = getParamsArray($_POST, $condItemNames);
if ($mode == MODE_LOAD) {
    // 初期表示の場合、チェックボックスのデフォルトをONに設定。
    setDefaultValues($items);
}

// モードに対応する処理を実行。
if ($mode == MODE_DETAIL) {
    // ------------------------------------------
    // 詳細ボタン処理
    // ------------------------------------------
    // 指定された帳票ステータスレコードIDをセッションに設定。
    $_SESSION['ad_id'] = $items["ad_id"];

    // 帳票管理　詳細画面に遷移する。
    goForwardPage("../AD01/AD01002", $items);
    exit();
}

// ------------------------------------------
// 検索処理を実行する。
// ・検索ボタン押下時 or CSV登録ボタン押下時
// ------------------------------------------
$pageData = null;
if ($mode == MODE_SEARCH ) {
    if (checkInput($items)) {
        // 表示ページを設定。
        $pageNo = ($items["page_no"] != "") ? $items["page_no"] : 1;

        // 検索処理を実行。
        $pageData = search($items, $pageNo);

        // 検索実施済みを立てる。
        $items["is_search"] = "yes";
    }
} else {
    $items["is_search"] = "";
}

if ($mode == MODE_FORMAT) {
    // ------------------------------------------
    // フォーマット出力ボタン処理
    // ------------------------------------------
    // フォーマットファイルを出力する。
    formatDownload(CSV_FORMAT_NAME, CSV_OUTPUT_NAME);
    exit();

} elseif ($mode == MODE_EXPORT) {
    // ------------------------------------------
    // CSV出力ボタン処理
    // ------------------------------------------
    // 表示中の企業情報のCSVファイルを出力する。
    csvExport($items);
    exit();

} elseif ($mode == MODE_IMPORT) {
    // ------------------------------------------
    // CSV登録ボタン処理
    // ------------------------------------------
    $upfile = $_FILES["upfile"];
    csvImport($upfile);

}

//起業種別判定をtplで使う準備 ▼▼▼
$items["loginCompanyType"] = $_SESSION['loginCompanyType'];

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';


// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("purposes", getOptionItems("comm", "purpose"));
$smarty->assign("pageData", $pageData);



// 画面を表示する。
$smarty->display('AD01/AD01001.tpl');


/**
 * デフォルト値を設定する。
 *
 * @param array[項目名 => 値] $items 画面項目一覧
 */
function setDefaultValues(&$items) {
    // チェックボックスのデフォルトをONに設定。
    $items["C_05_1"] = true;
    $items["C_05_2"] = true;
    $items["C_06_1"] = true;
    $items["C_06_2"] = true;
    $items["C_06_3"] = true;
}


/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;
    global $labels;

    $isCheckOk = true;

    // 禁止文字のチェックを行う。
    if (!validateProhibitedCharacters($items)) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // ・登録日
    if ($items["C_07_1"] != "" and $items["C_07_2"] != "") {
        if ($items["C_07_1"] > $items["C_07_2"]) {
            // from>toはエラー。
            $messages[] = getCommonMessage("WC017", "登録日");
            $isCheckOk = false;
        }
    }

    // チェック結果を返す。
    return $isCheckOk;
}


/**
 *画面の検索条件にて、CSVファイルを出力する。
 *
 * @param array[項目名 => 値] $items 画面項目一覧
 */
function csvExport($items) {
    // DBから対象データを取得する。
    $result = getExportDatas($items);

    // CSVライターを生成。
    $writer = Writer::createFromString("");
    $writer->setOutputBOM(Writer::BOM_UTF8);  // Excelで開けるようにBOMを付与。
    $writer->setNewline("\r\n");

    // CSVデータを積み上げる。
    $writer->insertAll($result);

    // CSVデータをresponseとして出力する。
    date_default_timezone_set('Asia/Tokyo');
    $fileName = CSV_OUTPUT_NAME."_".date("Ymd_Hi").".csv";
    $writer->output($fileName);

    exit();
}

/**
 * アップロードされたCSVファイルから、企業データを更新する。
 *
 * @param ファイル $upfile アップロードされたファイル
 * 
 * @return void
 */
function csvImport($upfile) {
    global $messages;

    // ファイル内容をチェックする。
    // ・拡張子
    if (!preg_match("/\.csv$/u", $upfile["name"])) {
        $messages[] = getCommonMessage("WC003");
        return false;
    }
    // ・サイズ
    if ($upfile["size"] == 0) {
        $messages[] = getCommonMessage("WC004");
        return false;
    }

    // CSVデータを読み込む。
    if (0 === strpos(PHP_OS, 'WIN')) setlocale(LC_CTYPE, 'C');  // windows環境でCSVパースを正常動作させるためのおまじない。
    $reader = Reader::createFromPath($upfile["tmp_name"], 'r');
    $reader->includeEmptyRecords();
    $csvdatas = $reader->getRecords();
    $datas = array();
    foreach ($csvdatas as $csvrow) {
        $datas[] = $csvrow;
    }
 
    $rowNo = 0;
    $keyInfo = array();
    $isError = false;

    // データの内容を検証する。
    foreach ($datas as $row) {
        $rowNo++;
        $rowInfo = " : ".$rowNo." 列目";
        $rowCount = count($row);

        // ・カラム数
        if ($rowCount < COLUMN_COUNT) {
            $messages[] = getCommonMessage("WC005").$rowInfo;
            $isError = true;
        }
        // ・必須
        if ($row[COL_MAILADDRESS] == "") {
            $messages[] = getCommonMessage("WC001", "メールアドレス").$rowInfo;
            $isError = true;
        }
        // 空レコードなら次行にスキップ。
        if (isEmptyRecord($row, COL_MAILADDRESS + 1)) continue;

        // ・必須
        if ($rowCount >= COL_COMPANY_ID and $row[COL_COMPANY_ID] == "") {
            $messages[] = getCommonMessage("WC001", "企業ID").$rowInfo;
            $isError = true;
        }
        if ($rowCount >= COL_USER_NAME and $row[COL_USER_NAME] == "") {
            $messages[] = getCommonMessage("WC001", "氏名").$rowInfo;
            $isError = true;
        }
        if ($rowCount >= COL_CLASSIFY and $row[COL_CLASSIFY] == "") {
            $messages[] = getCommonMessage("WC001", "種別").$rowInfo;
            $isError = true;
        }
        if ($rowCount >= COL_SEX and $row[COL_SEX] == "") {
            $messages[] = getCommonMessage("WC001", "性別").$rowInfo;
            $isError = true;
        }
        // ・形式
        if ($rowCount >= COL_MAILADDRESS and !validateMailAddress($row[COL_MAILADDRESS])) {
            $messages[] = getCommonMessage("WC015", "メールアドレス");
            $isError = true;
        }
        // ・禁止文字
        if (!validateProhibitedCharacters($row)) {
            $messages[] = getCommonMessage("WC013");
            $isError = true;
        }
        // ・重複(ユーザーID)
        if ($rowCount >= COL_MAILADDRESS) {
            $array_search_flg = array_search($row[COL_MAILADDRESS], $keyInfo);
            if ($array_search_flg !==false) {
                $messages[] = getCommonMessage("WC002", "メールアドレス").$rowInfo;
                $isError = true;
            }
            $keyInfo[] = $row[COL_MAILADDRESS];
        }
        // 企業が自企業かどうか(受入機関の場合)
        if ($_SESSION["loginCompanyType"] == "1") {
            if ($rowCount >= COL_COMPANY_ID and $row[COL_COMPANY_ID] != "") {
                if ($row[COL_COMPANY_ID] != $_SESSION["loginCompanyId"]) {
                    $messages[] = getCommonMessage("WC020");
                    $isError = true;
                }
            }
        }
    }
    if ($isError) {
        return false;
    }

    // 登録処理を行う。
    $isImportOk = csvDataImport($datas);

    if (!$isImportOk) {
        $messages[] = getCommonMessage("EC005", $row[COL_MAILADDRESS]);
        return false;
    }
 
    $messages[] = getCommonMessage("IC001");

    return true;
}