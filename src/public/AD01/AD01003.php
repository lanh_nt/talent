<?php
/**
 * 管理者管理 編集画面
 * 
 */
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/AD01/AD01003.php';

const FUNC_ID  = "AD01";
const SCENE_ID = "AD01003";

const MODE_LOAD   = "load";
const MODE_REGIST = "regist";
const MODE_SEARCH = "search";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
//　※労働者メニューではないので省略？
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。 ★★★
$condItemNames = array(
    "C_01",     //ID
    "C_02",     //氏名
    "C_03",    //企業名
    "C_04",    //役職
    "C_05_1",  "C_05_2",  //性別
    "C_06_1",  "C_06_2", "C_06_3",  //種別
    "C_07_1",  "C_07_2",  //登録日
    "page_no", "is_search", "mode", "ad_id"

);

$condItemNames2 = array(
    "user_id", 
    "user_name",       
    "sex",             
    "department",
    "classification",
    "company_id",
    "new_password",
    "new_password2",
    "mode", "ad_id"

);

// POST内容から構成。
$items = getParamsArray($_POST, $condItemNames);
$rowdatas = getParamsArray($_POST, $condItemNames2);
$items["ad_id"] = $_SESSION['ad_id'];

// モードに対応する処理を実行。
if ($mode == MODE_SEARCH) {
    // 検索一覧へ移動。
    goForwardPage("../AD01/AD01001", $items);
    exit();

}
if ($mode == MODE_REGIST) {
    //-----------------------------
    // 登録
    //-----------------------------
    $isOk = registAction($rowdatas);
    if ($isOk and $rowdatas["ad_id"] == $_SESSION['loginUserRowId']) {
        $_SESSION['loginUserName'] = $rowdatas["user_name"];
        $_SESSION['loginUserId'] = $rowdatas["user_id"];
    }

} else {
    //-----------------------------
    // 初期表示
    //-----------------------------
    // トークンを更新する。
    updateToken();

    // 検索処理を実行。
    $rowdatas = search($items["ad_id"]);
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels",    $labels);
$smarty->assign("messages",  $messages);
$smarty->assign("items",     $items);
$smarty->assign("rowdata",   $rowdatas);
$smarty->assign("companies", getCompanies());

// Smartyに変数をバインド。
$smarty->assign("labels", getLabels("AD01", "AD01003", $_SESSION['loginUserLang']));

// 画面を表示する。
$smarty->display('AD01/AD01003.tpl');


/**
 * regist処理
 *
 * @param array[項目名] $items     画面の内容
 * 
 * @return boolean 実行成否
 */
function registAction($items) {
    global $messages;

    // トークンをチェックする。
    checkToken();

    // 入力内容をチェックする。
    if (!checkInput($items)) {
        return false;
    }
    // DB登録を実行。
    $isUpdateOk = update($items);

    // トークンを更新する。
    updateToken();

    if ($isUpdateOk) {
        $messages[] = getCommonMessage("IC001");
    } else {
        $messages[] = getCommonMessage("EC005");
    }
    return $isUpdateOk;
}
/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items   画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;

    $isCheckOk = true;

    // 禁止文字のチェックを行う。
    if (!validateProhibitedCharacters($items)) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // 氏名
    // ・未入力
    if ($items["user_name"] == "") {
        $messages[] = getCommonMessage("WC001", "氏名");  
        $isCheckOk = false;
    }

    // 性別
    // ・未入力
    if ($items["sex"] == "") {
        $messages[] = getCommonMessage("WC001", "性別");  
        $isCheckOk = false;
    }

    // 企業名
    // ・未入力
    if ($items["company_id"] == "") {
        $messages[] = getCommonMessage("WC001", "企業名");  
        $isCheckOk = false;
    }

    // メールアドレス
    // ・未入力
    if ($items["user_id"] == "") {
        $messages[] = getCommonMessage("WC001", "メールアドレス");  
        $isCheckOk = false;
    } else {
        // ・形式
        if (!validateMailAddress($items["user_id"])) {
            $messages[] = getCommonMessage("WC015", "メールアドレス");
            $isCheckOk = false;
        }
    }

    // 種別
    // ・未入力
    if ($items["classification"] == "") {
        $messages[] = getCommonMessage("WC001", "種別");  
        $isCheckOk = false;
    }

    // パスワード
    if ($items["new_password"].$items["new_password"] != "") {
        // ・未入力
        if ($items["new_password"] == "") {
            $messages[] = getCommonMessage("WC001", "新規ログインパスワード");  
            $isCheckOk = false;
        }
        if ($items["new_password2"] == "") {
            $messages[] = getCommonMessage("WC001", "新規ログインパスワード(確認)");  
            $isCheckOk = false;
        }
        // ・不一致
        if ($items["new_password"] != $items["new_password2"] ) {
            $messages[] = getCommonMessage("WC012"); 
            $isCheckOk = false;
        }
    }

    // チェック結果を返す。
    return $isCheckOk;
}
