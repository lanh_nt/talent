<?php
/**
 * ログアウト機能
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 現在のモードを記憶。
$loginUserType = $_SESSION['loginUserType'];

// セッション情報をリセットする。
session_destroy();

// ログアウト前のモードに対応するログイン画面に遷移。
if ($loginUserType == "1") {
    header('Location:../alogin');
} else {   
    header('Location:../index');
}
