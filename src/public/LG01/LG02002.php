<?php
/**
 * 労働者ログイン：パスワード入力画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/LG01/LG02002.php';

const FUNC_ID  = "LG02";
const SCENE_ID = "LG02002";

session_start();

// 画面の動作モードを設定。
if (!isset($_REQUEST["mode"])) {
    // モード未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$mode = $_REQUEST["mode"];

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// POST内容から画面表示内容を構成。
$itemNames = array(
    "loginId", "pwd"
);
$items = getParamsArray($_POST, $itemNames);

// モードに対応する処理を実行。
if ($mode == "enter") {
    enterAction($items);
} elseif ($mode == "back") {
    // ID入力画面に遷移する。
    $items = array(
        "mode" => "back",
        "loginId" => $items["loginId"]
    );
    goForwardPage("../index", $items);
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('LG01/LG02002.tpl');


/**
 * enter処理
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean 実行成否
 */
function enterAction($items) {
    // 入力内容をチェックする。
    if (!checkInput($items)) {
        return false;
    }

    // ログイン情報を取得する。
    $result = getUserInfo($_POST["loginId"], $_POST["pwd"]);

    // ログイン情報が取得できなかった場合はエラー。
    if ($result == null) {
        global $messages;
        $messages[] = getCommonMessage("WC007");
        return false;
    }

    // ログイン情報をセッションにセット。
    $_SESSION['loginUserRowId']    = $result["user_rowid"];
    $_SESSION['loginUserId']       = $result["user_id"];
    $_SESSION['loginUserName']     = $result["user_name"];
    $_SESSION['loginCompanyType']  = "0";
    $_SESSION['loginCompanyRowId'] = $result["company_rowid"];
    $_SESSION['loginCompanyId']    = $result["company_id"];
    $_SESSION['loginUserType']     = $result["user_type"];
    $_SESSION['loginUserAuth']     = $result["user_auth"];
    $_SESSION['loginUserLang']     = $result["disp_lang"];

    // トップ画面に遷移する。
    header('Location:../TP01/TP01001');
    exit();
}

/**
 * 入力内容をチェックする。
 *
 * @param array[項目名 => 値] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;

    // パスワード
    if ($items["pwd"] == "") {
        $messages[] = getCommonMessage("WC001", "パスワード");
        return false;
    }

    // チェック正常を返す。
    return true;
}
