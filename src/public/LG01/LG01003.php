<?php
/**
 * 管理者ログイン：パスワードリセット画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../lib/ses_mailSender.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/Encrypter.php';
require_once '../../lib/JinzaiDb.php';

const FUNC_ID  = "LG01";
const SCENE_ID = "LG01003";

const MODE_LOAD     = "load";
const MODE_SENDMAIL = "sendmail";

session_start();

// 画面の動作モードを設定。
if (!isset($_REQUEST["mode"])) {
    // モード未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$mode = $_REQUEST["mode"];

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// POST内容から画面表示内容を構成。
$itemNames = array(
    "loginId"
);
$items = getParamsArray($_POST, $itemNames);

// モードに対応する処理を実行。
if ($mode == MODE_SENDMAIL) {
    // リマインダメールを送信する。
    sendmail($items["loginId"]);

    $items["isSendmail"] = true;
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('LG01/LG01003.tpl');


/**
 * リマインダメール送信処理
 *
 * @param string $mailto 送信先
 * 
 */
function sendmail($mailto) {
    // テンプレートエンジンを生成。
    $smarty = new Smarty();
    $smarty->template_dir = '../../templates/';
    $smarty->compile_dir  = '../../templates_c/';
    $smarty->config_dir   = '../../configs/';
    $smarty->cache_dir    = '../../cache/';
    
    // テンプレート変数をアサイン。
    $urlKey = getUrlKey($mailto);
    $smarty->assign("urlkey",  $urlKey);
    $smarty->assign("funcId",  "LG01");
    $smarty->assign("sceneId", "LG01004");

    // 言語モードに応じたメールテンプレートを読み込む。
    $lang = getLangValue();
    if ($lang == '2') {
        $tmplName = 'LG01/reminder-en.tpl';
    } else if ($lang == '3') {
        $tmplName = 'LG01/reminder-vi.tpl';
    } else {
        $tmplName = 'LG01/reminder-jp.tpl';
    }

    // テンプレートからメール本文を生成。
    $body = $smarty->fetch($tmplName);

    // メールを送信する。
    $SesMailer = new ses_mailSender();
    $subject = "Password remind mail.";
    
    $ret = $SesMailer->mailSend(
        "", //$Sender by default, 
        "", //$SenderName by default, 
        $mailto, 
        $subject, 
        $body
    );
}

/**
 * パスワード再発行画面リンクに設定するパラメータを生成する。
 *
 * @param string $mailto 送信先
 * 
 * @return string URLパラメータ
 */
function getUrlKey($mailto) {
    // 有効期限を生成。
    date_default_timezone_set('Asia/Tokyo');
    $nowstring = date("YmdHis", strtotime("+1 day"));

    // 有効期限とメール送信先を連結。
    $urlKeySrc = $nowstring."::".$mailto;

    // 暗号化する。
    $encrypter = new Encrypter();
    $urlKeyDest = $encrypter->encrypt($urlKeySrc);

    // 暗号化した内容を返す。
    return $urlKeyDest;
}
