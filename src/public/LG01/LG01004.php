<?php
/**
 * 労働者ログイン：パスワード再設定画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/Encrypter.php';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/LG01/LG01004.php';

const FUNC_ID  = "LG01";
const SCENE_ID = "LG01004";

const MODE_LOAD  = "load";
const MODE_ENTRY = "entry";

session_start();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// POST内容から画面表示内容を構成。
$itemNames = array(
    "loginId", "pwd1", "pwd2"
);
$items = getParamsArray($_POST, $itemNames);

// モードに対応する処理を実行。
if ($mode == MODE_LOAD) {
    // パラメータを解析し、対象者のメールアドレスを取得する。
    $mailAddress = parseUrlkey();

    $items["loginId"] = $mailAddress;

} elseif ($mode == MODE_ENTRY) {
    // パスワード変更処理を実行する。
    $isOk = entryAction($items);
    if ($isOk) {
        // ログイン画面に遷移する。
        $items = array(
            "mode" => "back",
            "loginId" => $items["loginId"],
            "message" => getCommonMessage("IC001")
        );
        goForwardPage("../alogin", $items);
    }
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('LG01/LG01004.tpl');


/**
 * パラメータを解析し、対象者のメールアドレスを取得する。
 *
 * @return string メールアドレス
 */
function parseUrlkey() {
    $urlkey = "";
    if (isset($_REQUEST["p"])) {
        $urlkey = $_REQUEST["p"];
    }
    if ($urlkey == "") {
        // パラメータ未設定はエラー画面に飛ばす。
        $errMessage = getCommonMessage("EC002");
        goErrorPage($errMessage);
    }

    // 暗号化された文字列を復号する。
    $encrypter = new Encrypter();
    $data = $encrypter->decrypt($urlkey);

    if ($data == false) {
        // 復号できなかった場合はエラー画面に飛ばす。
        $errMessage = getCommonMessage("EC002");
        goErrorPage($errMessage);
    }

    // 有効期限とメールアドレスに分解。
    $isMatch = preg_match('/^([0-9]{14})::(.*)$/u', $data, $m);
    if ($isMatch == false) {
        // 分解できなかった場合はエラー画面に飛ばす。
        $errMessage = getCommonMessage("EC002");
        goErrorPage($errMessage);
    }
    date_default_timezone_set('Asia/Tokyo');
    $expiration  = $m[1];
    $mailAddress = $m[2];

    // 有効期限内かどうか判定。
    $nowstring = date("YmdHis");
    if ($expiration < $nowstring) {
        // 有効期限切れの場合はエラー画面に飛ばす。
        $errMessage = getCommonMessage("EC006");
        goErrorPage($errMessage);
    }

    return $mailAddress;
}


/**
 * entry処理
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean 実行成否
 */
function entryAction($items) {
    // 入力内容をチェックする。
    if (!checkInput($items)) {
        return false;
    }

    // 登録処理を実行する。
    $isUpdateOk = update($items["loginId"], $items["pwd1"]);

    if (!$isUpdateOk) {
        $messages[] = getCommonMessage("EC005");
    }
    return $isUpdateOk;
}

/**
 * 入力内容をチェックする。
 *
 * @param array[項目名 => 値] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;
    global $labels;

    $isCheckOk = true;

    if ($items["pwd1"] == "") {
        $messages[] = getCommonMessage("WC001", "新パスワード");
        $isCheckOk = false;
    }
    if ($items["pwd2"] == "") {
        $messages[] = getCommonMessage("WC001", "新パスワード（確認）");
        $isCheckOk = false;
    }
    if (!$isCheckOk) return false;

    if ($items["pwd1"] != $items["pwd2"]) {
        $messages[] = getCommonMessage("WC006");
        $isCheckOk = false;
    }

    // チェック結果を返す。
    return $isCheckOk;
}
