<?php
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/WK02/WK02002.php';

const FUNC_ID ="WK02";
const SCENE_ID = "WK02002";

const MODE_LOAD = "load";

const CSV_FORMAT_NAME = "WK02002";
const CSV_OUTPUT_NAME = "システム情報";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];

    if ($_REQUEST["mode"]==MODE_LOAD) {
        // トークンを更新する。
        updateToken();

    }
} else {
    // トークンを更新する。
    updateToken();

    $mode = MODE_LOAD;
}

// 対象の労働者を取得。
if ($_SESSION['loginUserType'] == "2") {
    // 労働者ログインの場合は、自ID。
    $targetWorkerId = $_SESSION['loginUserId'];
} else {
    // 管理者ログインの場合は、検索で指定されたID。
    $targetWorkerId = $_SESSION['targetWorkerId'];
}
// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。
$itemNames = array(
    "password", "password2");

// モードに対応する処理を実行。
if ($mode == "regist") {
    // POST内容から構成。
    $items = getParamsArray($_POST, $itemNames);

    $result = registAction($targetWorkerId, $items);
}

// システム情報を取得する。
$items = search($targetWorkerId, $labels);

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('WK02/WK02002.tpl');

/**
 * regist処理
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean 実行成否
 */
function registAction($targetWorkerId, $items) {

    // トークンをチェックする。
    checkToken();

    // 入力内容をチェックする。
    if (!checkInput($items)) {
        return false;
    }

    // DB登録を実行。
    update($targetWorkerId, $items);

    // トークンを更新する。
    updateToken();

    global $messages;
    $messages[] = getCommonMessage("IC001");
    return true;
}

/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;
    global $labels;

    $isCheckOk = true;

    // パスワードの入力が一緒ではない場合、エラー。
    if ($items["password"] != $items["password2"] ) {
        $messages[] = getCommonMessage("WC012");
        $isCheckOk = false;
    }

    // 禁止文字のチェックを行う。
    if (!validateProhibitedCharacters($items)) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // チェック結果を返す。
    return $isCheckOk;
}
