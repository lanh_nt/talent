<?php
/**
 * 労働者管理/詳細：ファイル管理
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/WK02/WK02012.php';

const FUNC_ID  = "WK02";
const SCENE_ID = "WK02012";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 対象の労働者を取得。
if ($_SESSION['loginUserType'] == "2") {
    // 労働者ログインの場合は、自IDをセッションに設定。
    $_SESSION['targetWorkerId'] = $_SESSION['loginUserId'];
}
$targetWorkerId = $_SESSION['targetWorkerId'];

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// ファイル一覧を取得する。
$files = search($targetWorkerId);

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("files", $files);

// 画面を表示する。
$smarty->display('WK02/WK02012.tpl');
