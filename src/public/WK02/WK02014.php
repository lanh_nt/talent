<?php
/**
 * 労働者管理/帳票/在留資格変更許可申請書画面
 *
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/WK02/WK02014.php';

use League\Csv\Writer;
use League\Csv\Reader;

const FUNC_ID ="WK02";
const SCENE_ID = "WK02014";

const MODE_LOAD = "load";
const MODE_IMPORT = "csv-import";
const MODE_EXPORT = "csv-export";
const MODE_FORMAT = "csv-format";
const MODE_UPDATE = "update";

const CSV_FORMAT_NAME = "WK02014";
const CSV_OUTPUT_NAME = "在留資格変更許可申請書";

// CSVデータのカラム数
const COLUMN_COUNT = 121;
const COL_CONTRACT_FROM =  1 - 1;
const COL_CONTRACT_TO   =  2 - 1;
const COL_CONSIGN_FROM  = 29 - 1;
const COL_CONSIGN_TO    = 30 - 1;
const COL_NOTIFY_YMD    = 36 - 1;

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// 対象の労働者を取得。
if ($_SESSION['loginUserType'] == "2") {
    // 労働者ログインの場合は、自ID。
    $targetWorkerId = $_SESSION['loginUserId'];
} else {
    // 管理者ログインの場合は、検索で指定されたID。
    $targetWorkerId = $_SESSION['targetWorkerId'];
}
// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// モードに対応する処理を実行。
if ($mode == MODE_UPDATE) {
    // ------------------------------------------
    // 編集ボタン処理
    // ------------------------------------------
    // 編集画面に遷移する。
    goForwardPage("../WK02/WK02015", $targetWorkerId);
    exit();

} elseif ($mode == MODE_FORMAT) {
    // ------------------------------------------
    // フォーマット出力ボタン処理
    // ------------------------------------------
    // フォーマットファイルを出力する。
    formatDownload(CSV_FORMAT_NAME, CSV_OUTPUT_NAME);
    exit();

} elseif ($mode == MODE_EXPORT) {
    // ------------------------------------------
    // CSV出力ボタン処理
    // ------------------------------------------
    // 表示中の労働者のCSVファイルを出力する。
    csvExport($targetWorkerId);
    exit();

} elseif ($mode == MODE_IMPORT) {
    // ------------------------------------------
    // CSV登録ボタン処理
    // ------------------------------------------
    $upfile = $_FILES["upfile"];
    csvImport($upfile, $targetWorkerId);

}

// 在留資格変更許可申請書情報を取得する。
$items = search($targetWorkerId, $labels);

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('WK02/WK02014.tpl');

/**
 *在留資格変更許可申請書情報のCSVファイルを出力する。
 *
 * @param string $workerId 労働者ID
 */
function csvExport($workerId) {
    // DBから対象データを取得する。
    $result = getExportDatas($workerId);

    // CSVライターを生成。
    $writer = Writer::createFromString("");
    $writer->setOutputBOM(Writer::BOM_UTF8);  // Excelで開けるようにBOMを付与。
    $writer->setNewline("\r\n");

    // CSVデータを積み上げる。
    $writer->insertAll($result);

    // CSVデータをresponseとして出力する。
    date_default_timezone_set('Asia/Tokyo');
    $fileName = CSV_OUTPUT_NAME."_".date("Ymd_Hi").".csv";
    $writer->output($fileName);

    exit();
}


/**
 * アップロードされたCSVファイルから、在留資格変更許可申請書データを更新する。
 *
 * @param ファイル $upfile アップロードされたファイル
 * @param string $workerId 労働者ID
 * 
 * @return void
 */
function csvImport($upfile, $workerId) {
    global $messages;

    // ファイル内容をチェックする。
    // ・拡張子
    if (!preg_match("/\.csv$/u", $upfile["name"])) {
        $messages[] = getCommonMessage("WC003");
        return false;
    }
    // ・サイズ
    if ($upfile["size"] == 0) {
        $messages[] = getCommonMessage("WC004");
        return false;
    }

    // CSVデータを読み込む。
    if (0 === strpos(PHP_OS, 'WIN')) setlocale(LC_CTYPE, 'C');  // windows環境でCSVパースを正常動作させるためのおまじない。
    $reader = Reader::createFromPath($upfile["tmp_name"], 'r');
    $reader->includeEmptyRecords();
    $csvdatas = $reader->getRecords();
    $datas = array();
    foreach ($csvdatas as $csvrow) {
        $datas[] = $csvrow;
    }
 
    $rowNo = 0;
    $isError = false;

    // データの内容を検証する。
    foreach ($datas as $row) {
        $rowNo++;
        $rowInfo = " : ".$rowNo." 列目";

        // ・カラム数
        if (count($row) < COLUMN_COUNT) {
            $messages[] = getCommonMessage("WC005").$rowInfo;
            $isError = true;
        }
        // ・禁止文字
        if (!validateProhibitedCharacters($row)) {
            $messages[] = getCommonMessage("WC013");
            $isError = true;
        }
        // ・日付
        $isError |= checkDateSpanValue($row, COL_CONTRACT_FROM, COL_CONTRACT_TO, "特定技能雇用契約　雇用契約期間");
        $isError |= checkDateSpanValue($row, COL_CONSIGN_FROM,  COL_CONSIGN_TO,  "派遣期間");
        $isError |= checkDateValue($row, COL_NOTIFY_YMD, "許可・届出受理年月日");

        // データが複数件存在する場合でも、先頭の1件のみで検証終了。
        break;
    }
    if ($isError) {
        return false;
    }

    // データを登録する。
    $isImportOk = csvDataImport($row, $workerId);
    if (!$isImportOk) {
        $messages[] = getCommonMessage("EC005");
        return false;
    }
    
    $messages[] = getCommonMessage("IC001");
    return true;
}

/**
 * 日付(from-to)のチェックを行う。
 *
 * @param [type] $row         行データ
 * @param [type] $colnumFrom  カラム位置(から)
 * @param [type] $colnumTo    カラム位置(まで)
 * @param [type] $labelString ラベル文言
 * 
 * @return boolean エラー有無 (true:エラーあり、false:エラーなし)
 */
function checkDateSpanValue($row, $colnumFrom, $colnumTo, $labelString) {
    global $messages;

    $isError = false;

    $valFrom = (count($row) >= $colnumFrom ) ? $row[$colnumFrom] : "";
    $valTo   = (count($row) >= $colnumTo   ) ? $row[$colnumTo]   : "";

    // from
    if ($valFrom != "") {
        $isError |= checkDateValue($row, $colnumFrom, $labelString."(From)");
    }
    // to
    if ($valTo != "") {
        $isError |= checkDateValue($row, $colnumTo, $labelString."(To)");
    }
    // from-to
    if (!$isError and $valFrom != "" and $valTo != "") {
        if ($valFrom > $valTo) {
            $messages[] = getCommonMessage("WC017", $labelString);
            $isError = true;
        }
    }

    return $isError;
}

/**
 * 日付のチェックを行う。
 *
 * @param [type] $row         行データ
 * @param [type] $colnum      カラム位置
 * @param [type] $labelString ラベル文言
 * 
 * @return boolean エラー有無 (true:エラーあり、false:エラーなし)
 */
function checkDateValue($row, $colnum, $labelString) {
    global $messages;

    $isError = false;

    $val = (count($row) >= $colnum ) ? $row[$colnum] : "";

    if ($val != "") {
        $isMatch = preg_match("/^([0-9]{4})\/([0-9]{1,2})\/([0-9]{1,2})$/", $val, $m);
        if (!$isMatch) {
            $messages[] = getCommonMessage("WC008", $labelString);
            $isError = true;
        } else {
            $yy = intval($m[1]);
            $mm = intval($m[2]);
            $dd = intval($m[3]);
            if (!checkdate($mm, $dd, $yy)) {
                $messages[] = getCommonMessage("WC008", $labelString);
                $isError = true;
            }
        }
    }

    return $isError;
}
