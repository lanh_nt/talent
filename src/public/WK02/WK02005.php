<?php
/**
 * 労働者管理/帳票/支援実施状況に係る届出画面
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/WK02/WK02005.php';

use League\Csv\Writer;
use League\Csv\Reader;

const FUNC_ID ="WK02";
const SCENE_ID = "WK02005";

const MODE_LOAD = "load";
const MODE_IMPORT = "csv-import";
const MODE_EXPORT = "csv-export";
const MODE_FORMAT = "csv-format";

const CSV_FORMAT_NAME = "WK02005";
const CSV_OUTPUT_NAME = "支援実施状況に係る届出";

// CSVデータのカラム数
const COLUMN_COUNT = 66;

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// 対象の労働者を取得。
if ($_SESSION['loginUserType'] == "2") {
    // 労働者ログインの場合は、自ID。
    $targetWorkerId = $_SESSION['loginUserId'];
} else {
    // 管理者ログインの場合は、検索で指定されたID。
    $targetWorkerId = $_SESSION['targetWorkerId'];
}
// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

if ($mode == MODE_FORMAT) {
    // ------------------------------------------
    // フォーマット出力ボタン処理
    // ------------------------------------------
    // フォーマットファイルを出力する。
    formatDownload(CSV_FORMAT_NAME, CSV_OUTPUT_NAME);
    exit();

} elseif ($mode == MODE_EXPORT) {
    // ------------------------------------------
    // CSV出力ボタン処理
    // ------------------------------------------
    // 表示中の労働者のCSVファイルを出力する。
    csvExport($targetWorkerId);
    exit();

} elseif ($mode == MODE_IMPORT) {
    // ------------------------------------------
    // CSV登録ボタン処理
    // ------------------------------------------
    $upfile = $_FILES["upfile"];
    csvImport($upfile, $targetWorkerId);

}
// 支援実施状況に係る届出の情報を取得する。
$items = search($targetWorkerId);

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('WK02/WK02005.tpl');

/**
 *支援実施状況に係る届出情報のCSVファイルを出力する。
 *
 * @param string $workerId 労働者ID
 */
function csvExport($workerId) {
    // DBから対象データを取得する。
    $result = getExportDatas($workerId);

    // CSVライターを生成。
    $writer = Writer::createFromString("");
    $writer->setOutputBOM(Writer::BOM_UTF8);  // Excelで開けるようにBOMを付与。
    $writer->setNewline("\r\n");

    // CSVデータを積み上げる。
    $writer->insertAll($result);

    // CSVデータをresponseとして出力する。
    date_default_timezone_set('Asia/Tokyo');
    $fileName = CSV_OUTPUT_NAME."_".date("Ymd_Hi").".csv";
    $writer->output($fileName);

    exit();
}


/**
 * アップロードされたCSVファイルから、支援実施状況に係る届出データを更新する。
 *
 * @param ファイル $upfile アップロードされたファイル
 * @param string $workerId 労働者ID
 * 
 * @return void
 */
function csvImport($upfile, $workerId) {
    global $messages;

    // ファイル内容をチェックする。
    // ・拡張子
    if (!preg_match("/\.csv$/u", $upfile["name"])) {
        $messages[] = getCommonMessage("WC003");
        return false;
    }
    // ・サイズ
    if ($upfile["size"] == 0) {
        $messages[] = getCommonMessage("WC004");
        return false;
    }

    // CSVデータを読み込む。
    if (0 === strpos(PHP_OS, 'WIN')) setlocale(LC_CTYPE, 'C');  // windows環境でCSVパースを正常動作させるためのおまじない。
    $reader = Reader::createFromPath($upfile["tmp_name"], 'r');
    $reader->includeEmptyRecords();
    $csvdatas = $reader->getRecords();
    $datas = array();
    foreach ($csvdatas as $csvrow) {
        $datas[] = $csvrow;
    }
 
    $rowNo = 0;
    $isError = false;

    // データの内容を検証する。
    foreach ($datas as $row) {
        $rowNo++;
        $rowInfo = " : ".$rowNo." 列目";

        // ・カラム数
        if (count($row) < COLUMN_COUNT) {
            $messages[] = getCommonMessage("WC005").$rowInfo;
            $isError = true;
        }
        // ・禁止文字
        if (!validateProhibitedCharacters($row)) {
            $messages[] = getCommonMessage("WC013");
            $isError = true;
        }

        // データが複数件存在する場合でも、先頭の1件のみで検証終了。
        break;
    }
    if ($isError) {
        return false;
    }

    // データを登録する。
    $isImportOk = csvDataImport($row, $workerId);
    if (!$isImportOk) {
        $messages[] = getCommonMessage("EC005");
        return false;
    }
    
    $messages[] = getCommonMessage("IC001");
    return true;
}
