<?php
/**
 * 労働者管理/申請書出力画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/WK02/WK02011.php';

const FUNC_ID  = "WK02";
const SCENE_ID = "WK02011";

const MODE_LOAD = "load";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 対象の労働者を取得。
if (!isset($_SESSION['targetWorkerId'])) {
    // 労働者未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$targetWorkerId = $_SESSION['targetWorkerId'];

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// 帳票ステータス情報を取得する。
$items = search($targetWorkerId);

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('WK02/WK02011.tpl');
