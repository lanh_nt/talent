<?php
/**
 * 労働者管理/詳細/基本画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/WK02/WK02001.php';

const FUNC_ID  = "WK02";
const SCENE_ID = "WK02001";

const MODE_LOAD = "load";
const MODE_UPDATE = "update";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// 対象の労働者を取得。
if ($_SESSION['loginUserType'] == "2") {
    // 労働者ログインの場合は、自IDをセッションに設定。
    $_SESSION['targetWorkerId'] = $_SESSION['loginUserId'];
}
$targetWorkerId = $_SESSION['targetWorkerId'];

// モードに対応する処理を実行。
if ($mode == MODE_UPDATE) {
    // ------------------------------------------
    // 編集ボタン処理
    // ------------------------------------------
    // 編集画面に遷移する。
    goForwardPage("../WK02/WK02015", $targetWorkerId);
    exit();

}
// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 労働者の情報を取得する。
$items = search($targetWorkerId, $labels);

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('WK02/WK02001.tpl');
