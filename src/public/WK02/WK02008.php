<?php
/**
 * 労働者管理詳細/相談記録書画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/WK02/WK02008.php';

const FUNC_ID  = "WK02";
const SCENE_ID = "WK02008";

const MODE_LOAD   = "load";
const MODE_REGIST = "regist";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}
if ($mode == MODE_LOAD) {
    // トークンを更新する。
    updateToken();
}

// 対象の労働者を取得。
$targetWorkerId = $_SESSION['targetWorkerId'];

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。
$condItemNames = array(
    "span_from", "span_to", "span_disp"
);
if ($mode == MODE_LOAD) {
    // 初期表示時は、いったん空欄で構成。
    $items = getParamsArray(array(), $condItemNames);
} else {
    // それ以外の場合は、POST内容から構成。
    $items = getParamsArray($_POST, $condItemNames);
}

// 登録ボタン押下時なら、登録処理を実行。
if ($mode == MODE_REGIST) {
    $result = registAction($targetWorkerId, $items);
}

// 相談記録データを取得。
$reportDatas = search($targetWorkerId);
if (count($reportDatas) > 0) {
    // 初期表示時は、相談記録データから内容を設定。
    $items["span_from"] = $reportDatas[0]["response_from_ymd"];
    $items["span_to"]   = $reportDatas[0]["response_to_ymd"];
    if ($items["span_from"].$items["span_to"] != "") {
        $items["span_disp"] 
            = $reportDatas[0]["response_from_disp"]
            . " ～ "
            . $reportDatas[0]["response_to_disp"];
    }
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("reportDatas", $reportDatas);

// 画面を表示する。
$smarty->display('WK02/WK02008.tpl');


/**
 * regist処理
 *
 * @param string $targetWorkerId 労働者ID
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean 実行成否
 */
function registAction($targetWorkerId, $items) {
    // トークンをチェックする。
    checkToken();

    // (※入力チェック項目なし。)

    // DB登録を実行。
    $isUpdateOk = update($targetWorkerId, $items);

    // トークンを更新する。
    updateToken();

    global $messages;
    if ($isUpdateOk) {
        $messages[] = getCommonMessage("IC001");
    } else {
        $messages[] = getCommonMessage("EC005");
    }
    return $isUpdateOk;
}
