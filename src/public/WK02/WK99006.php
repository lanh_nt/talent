<?php
/**
 * 帳票出力処理：事前ガイダンス確認書
 * 
 */
require_once '../../vendor/autoload.php';
require_once '../../lib/ReportOutput.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/WK02/WK99006.php';

// 出力帳票のID
const REPORT_ID = "R01-07-GUIDANCE";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 対象の労働者を取得。
if (!isset($_SESSION['targetWorkerId'])) {
    // 労働者未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$targetWorkerId = $_SESSION['targetWorkerId'];

// 帳票出力クラスを生成。
$pdf = new ReportOutput(REPORT_ID);

//帳票出力項目配列を作成する
$retAry = makePdfData06($targetWorkerId);

// 帳票を出力する。
$pdf->Output($retAry);
