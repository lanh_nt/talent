<?php
/**
 * 面談記録：労働者：一覧画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../lib/PageData.php';
require_once '../../da/IN01/IN01001.php';

const FUNC_ID  = "IN01";
const SCENE_ID = "IN01001";

const MODE_LOAD   = "load";
const MODE_SEARCH = "search";
const MODE_DETAIL = "detail";
const MODE_REGIST = "regist";
const MODE_BACK   = "back";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。
$condItemNames = array(
    "C_01", "C_02", "C_03", "C_04_1", "C_04_2", "C_05",
    "page_no", "is_search"
);
if ($mode == MODE_BACK) {
    // 別画面から戻ってきた場合は、sessionから条件を復帰。
    $items = getParamsArray($_SESSION["IN01_conds"], $condItemNames);
} else {
    // それ以外の場合は、POST内容から構成。
    $items = getParamsArray($_POST, $condItemNames);

    if ($mode == MODE_LOAD) {
        // 初期表示の場合、チェックボックスのデフォルトをONに設定。
        setDefaultValues($items);
    }
}

// ------------------------------------------
// 検索処理を実行する。
// ・検索ボタン押下時 or 別画面から戻ってきた時
// ------------------------------------------
$pageData = null;
if ($mode == MODE_SEARCH || ($mode == MODE_BACK && $items["is_search"] == "yes")) {
    if (checkInput($items)) {
        // 表示ページを設定。
        $pageNo = ($items["page_no"] != "") ? $items["page_no"] : 1;

        // ------------------------------------------
        // 検索処理
        // ------------------------------------------
        $pageData = search($items, $pageNo);

        // 検索実施済みを立てる。
        $items["is_search"] = "yes";
    }
} else {
    $items["is_search"] = "";
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("pageData", $pageData);
if ($_SESSION["loginCompanyType"] == "2") {
    $smarty->assign("companies", getCompanies());
}

// 画面を表示する。
$smarty->display('IN01/IN01001.tpl');


/**
 * デフォルト値を設定する。
 *
 * @param array[項目名 => 値] $items 画面項目一覧
 */
function setDefaultValues(&$items) {
    // チェックボックスのデフォルトをONに設定。
    $items["C_05"] = true;
}


/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;
    global $labels;

    $isCheckOk = true;

    // 禁止文字のチェックを行う。
    if (!validateProhibitedCharacters($items)) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // ・登録日
    if ($items["C_04_1"] != "" and $items["C_04_2"] != "") {
        if ($items["C_04_1"] > $items["C_04_2"]) {
            // from>toはエラー。
            $messages[] = getCommonMessage("WC017", "登録日");
            $isCheckOk = false;
        }
    }

    // チェック結果を返す。
    return $isCheckOk;
}
