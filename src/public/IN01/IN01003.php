<?php
/**
 * 面談記録：労働者：編集画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/IN01/IN01003.php';

const FUNC_ID  = "IN01";
const SCENE_ID = "IN01003";

const MODE_LOAD   = "load";
const MODE_REGIST = "regist";
const MODE_BACK   = "back";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (!isset($_REQUEST["mode"])) {
    // モード未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$mode = $_REQUEST["mode"];

if (!isset($_POST["user_id"]) or $_POST["user_id"] == "") {
    // ユーザID未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$userId = $_POST["user_id"];

// 画面表示内容を構成。
if ($mode == MODE_LOAD) {
    // 初期表示時は、対象データを検索して、その内容から構成。
    $result = search($userId);
    $items = getParamsArray($result, getItemNames());
} else {
    // それ以外の場合は、POST内容から構成。
    $items = getParamsArray($_POST, getItemNames());
}

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// モードに対応する処理を実行。
if ($mode == MODE_LOAD) {
    //-----------------------------
    // 初期表示
    //-----------------------------
    // 検索条件を保存する。
    $_SESSION["IN01_conds"] = $_POST;

    // トークンを更新する。
    updateToken();

} else if ($mode == MODE_REGIST) {
    //-----------------------------
    // 登録
    //-----------------------------
    // 登録処理を実行する。
    registAction($items);
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('IN01/IN01003.tpl');


/**
 * フォーム項目名の一覧を返す。
 *
 * @return array[項目名] フォーム項目名の一覧
 */
function getItemNames() {
    return array(
        // 1.基本情報
        "user_id", "user_name", "company_name", "interview_ymd",
        // 2.面談対応者
        "E_2_1", "E_2_2", "E_2_3",
        // 3.面談事項
        "E_3_1_1_1", "E_3_1_1_2",
        "E_3_1_2_1", "E_3_1_2_2",
        "E_3_1_3_1", "E_3_1_3_2",
        "E_3_2_1_1", "E_3_2_1_2",
        "E_3_2_2_1", "E_3_2_2_2",
        "E_3_2_3_1", "E_3_2_3_2",
        "E_3_2_4_1", "E_3_2_4_2",
        "E_3_2_5_1", "E_3_2_5_2",
        "E_3_2_6_1", "E_3_2_6_2",
        "E_3_3_1_1", "E_3_3_1_2",
        "E_3_3_2_1", "E_3_3_2_2",
        "E_3_3_3_1", "E_3_3_3_2",
        "E_3_3_4_1", "E_3_3_4_2",
        "E_3_3_5_1", "E_3_3_5_2",
        "E_3_4_1_1", "E_3_4_1_2",
        "E_3_4_2_1", "E_3_4_2_2",
        "E_3_5_1_1", "E_3_5_1_2",
        "E_3_5_2_1", "E_3_5_2_2", "E_3_5_2_3",
        "E_3_6", "E_3_7",
        // 4.法令違反への対応
        "E_4_1_ymd",
        "E_4_2",
        "E_4_3_1_1", "E_4_3_1_2", "E_4_3_1_3",
        "E_4_3_2_1", "E_4_3_2_2_ymd", "E_4_3_2_3", "E_4_3_2_4", "E_4_3_2_5",
        "E_4_3_3_1", "E_4_3_3_2_ymd", "E_4_3_3_3", "E_4_3_3_4",
        // その他
        "user_id"
    );
}


/**
 * regist処理
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean 実行成否
 */
function registAction($items) {
    // トークンをチェックする。
    checkToken();

    // 入力内容をチェックする。
    if (!checkInput($items)) {
        return false;
    }

    // DB登録を実行。
    $isUpdateOk = update($items);

    // トークンを更新する。
    updateToken();

    global $messages;
    if ($isUpdateOk) {
        $messages[] = getCommonMessage("IC001");
    } else {
        $messages[] = getCommonMessage("EC005");
    }
    return $isUpdateOk;
}

/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items   画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;

    $isCheckOk = true;

    if (!validateProhibitedCharacters($items)) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // チェック結果を返す。
    return $isCheckOk;
}
