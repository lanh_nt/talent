<?php
/**
 * 面談記録：労働者：詳細画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/IN01/IN01002.php';

const FUNC_ID  = "IN01";
const SCENE_ID = "IN01002";

const MODE_LOAD = "load";
const MODE_BACK = "back";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 必要パラメータが設定されていない場合、エラー画面に飛ばす。
if (!isset($_REQUEST["mode"]) or !isset($_POST["user_id"])) {
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
    exit();
}

// 画面の動作モードを設定。
$mode = $_REQUEST["mode"];
if ($mode == MODE_LOAD) {
    // 検索条件を保存する。
    $_SESSION["IN01_conds"] = $_POST;
} else if ($mode != MODE_BACK) {
    // モード不正はエラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}

if (!isset($_POST["user_id"]) or $_POST["user_id"] == "") {
    // ユーザ未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$userId = $_POST["user_id"];

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

//---------------------------------
// 表示内容を取得する。
//---------------------------------
$items = search($userId);

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('IN01/IN01002.tpl');
