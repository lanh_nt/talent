<?php
/**
 * 問い合わせ詳細表示画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/CN01/CN01002.php';

const FUNC_ID  = "CN01";
const SCENE_ID = "CN01002";

const MODE_LOAD = "detail-load";
const MODE_BACK = "back";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (!isset($_REQUEST["mode"])) {
    // モード未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$mode = $_REQUEST["mode"];

if ($mode == MODE_LOAD) {
    // 検索条件を保存する。
    $_SESSION["CN01_conds"] = $_POST;
} else if ($mode != MODE_BACK) {
    // モード不正はエラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
if (!isset($_POST["inq_id"]) or $_POST["inq_id"] == "") {
    // 問い合わせID未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$inqId = $_POST["inq_id"];

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

//---------------------------------
// 表示内容を取得する。
//---------------------------------
$items = search($inqId);

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('CN01/CN01002.tpl');
