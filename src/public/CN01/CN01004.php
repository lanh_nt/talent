<?php
/**
 * 問い合わせ編集画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/CN01/CN01004.php';

const FUNC_ID  = "CN01";
const SCENE_ID = "CN01004";

const MODE_LOAD   = "edit-load";
const MODE_REGIST = "regist";
const MODE_BACK   = "back";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (!isset($_REQUEST["mode"])) {
    // モード未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$mode = $_REQUEST["mode"];

if (!isset($_POST["inq_id"]) or $_POST["inq_id"] == "") {
    // 問い合わせID未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$inqId = $_POST["inq_id"];

// 画面表示内容を構成。
if ($mode == MODE_LOAD) {
    // 初期表示時は、対象データを検索して、その内容から構成。
    $result = search($inqId);
    $items = getParamsArray($result, getItemNames());
} else {
    // それ以外の場合は、POST内容から構成。
    $items = getParamsArray($_POST, getItemNames());
}

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// モードに対応する処理を実行。
if ($mode == MODE_LOAD) {
    //-----------------------------
    // 初期表示
    //-----------------------------
    // トークンを更新する。
    updateToken();

} else if ($mode == MODE_REGIST) {
    //-----------------------------
    // 登録
    //-----------------------------
    // 登録処理を実行する。
    $result = registAction($items);
    if ($result == true) {
        // 検索画面に遷移する。
        $items = array(
            "mode" => MODE_BACK,
            "message" => getCommonMessage("IC001")
        );
        goForwardPage("CN01001", $items);
    }
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('CN01/CN01004.tpl');


/**
 * フォーム項目名の一覧を返す。
 *
 * @return array[項目名] フォーム項目名の一覧
 */
function getItemNames() {
    return array(
        "inq_id",
        "inq_no", "status",
        "received_ymd", "deadline_ymd",
        "user_name", "lang_string",
        "subject_nv", "subject_jp",
        "content_nv", "content_jp",
        "reply_nv",   "reply_jp"
    );
}


/**
 * regist処理
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean 実行成否
 */
function registAction($items) {
    // トークンをチェックする。
    checkToken();

    // 入力内容をチェックする。
    if (!checkInput($items)) {
        return false;
    }

    // DB登録を実行。
    if ($items["status"] == 1) {
        $isUpdateOk = updateTranslate($items);
    } else {
        $isUpdateOk = updateReply($items);
    }

    // トークンを更新する。
    updateToken();

    global $messages;
    if ($isUpdateOk) {
        $messages[] = getCommonMessage("IC001");
    } else {
        $messages[] = getCommonMessage("EC005");
    }
    return $isUpdateOk;
}

/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items   画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;

    $isCheckOk = true;

    // 禁止文字のチェックを行う。
    // (除外：件名、問い合わせ内容、返信の各母国語項目)
    if (!validateProhibitedCharacters($items, "subject_nv", "content_nv", "reply_nv")) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // チェック結果を返す。
    return $isCheckOk;
}
