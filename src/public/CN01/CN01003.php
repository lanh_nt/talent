<?php
/**
 * 問い合わせ新規登録画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/CN01/CN01003.php';

const FUNC_ID  = "CN01";
const SCENE_ID = "CN01003";

const MODE_LOAD   = "insert-load";
const MODE_REGIST = "regist";
const MODE_BACK   = "back";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (!isset($_REQUEST["mode"])) {
    // モード未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$mode = $_REQUEST["mode"];

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);
$langs = getOptionItems("comm", "lang");
$labels["lang-1"] = $langs["1"];
$labels["lang-2"] = $langs["2"];
$labels["lang-3"] = $langs["3"];

// メッセージ情報を生成。
$messages = array();

// モードに対応する処理を実行。
if ($mode == MODE_LOAD) {
    //-----------------------------
    // 初期表示
    //-----------------------------
    // 検索条件を保存する。
    $_SESSION["CN01_conds"] = $_POST;

    // トークンを更新する。
    updateToken();

} else if ($mode == MODE_REGIST) {
    //-----------------------------
    // 登録
    //-----------------------------
    // POST内容から画面入力内容一覧を構成。
    $formItems = getParamsArray($_POST, getItemNames());

    $result = registAction($formItems);
    if ($result == true) {
        // 検索画面に遷移する。
        $items = array(
            "mode" => MODE_BACK,
            "message" => getCommonMessage("IC001")
        );
        goForwardPage("CN01001", $items);
    }
}

// 表示内容を取得する。
$items = getParamsArray(array(), getItemNames());

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('CN01/CN01003.tpl');


/**
 * フォーム項目名の一覧を返す。
 *
 * @return array[項目名] フォーム項目名の一覧
 */
function getItemNames() {
    return array(
        "CN_01", "CN_02", "CN_03", "CN_04"
    );
}


/**
 * regist処理
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean 実行成否
 */
function registAction($items) {
    // トークンをチェックする。
    checkToken();

    // 入力内容をチェックする。
    if (!checkInput($items)) {
        return false;
    }

    // DB登録を実行。
    $isInsertOk = insert($items);

    // トークンを更新する。
    updateToken();

    global $messages;
    if ($isInsertOk) {
        $messages[] = getCommonMessage("IC001");
    } else {
        $messages[] = getCommonMessage("EC005");
    }
    return $isInsertOk;
}

/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items   画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;
    global $labels;

    $isCheckOk = true;

    // ・配信言語
    if ($items["CN_01"] == "") {
        // 未入力はエラー。
        $messages[] = getCommonMessage("WC001", $labels["L-01-01"]);
        $isCheckOk = false;
    }

    // 禁止文字のチェックを行う。
    // (除外：件名、問い合わせ内容、返信の各母国語項目)
    if (!validateProhibitedCharacters($items, "subject_nv", "content_nv", "reply_nv")) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // チェック結果を返す。
    return $isCheckOk;
}
