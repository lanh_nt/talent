<?php
/**
 * 管理者ログイン画面
 * 
 */

require_once '../vendor/autoload.php';
require_once '../const.inc';
require_once '../lib/common.inc';
require_once '../lib/JinzaiDb.php';

const FUNC_ID  = "LG01";
const SCENE_ID = "LG01001";

const MODE_LOAD  = "load";
const MODE_ENTER = "enter";
const MODE_BACK  = "back";

session_start();

checkMaintenanceNologin();

// モード指定がない場合(=初期表示時)、セッション情報をリセットする。
if (!isset($_REQUEST["mode"])) {
    session_destroy();
    session_start();
}

// 画面の動作モードを設定。
$mode = (isset($_REQUEST["mode"])) ? $_REQUEST["mode"] : "";

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// POST内容から画面表示内容を構成。
$itemNames = array(
    "loginId"
);
$items = getParamsArray($_POST, $itemNames);

// モードに対応する処理を実行。
if ($mode == MODE_ENTER) {
    enterAction($items);
} elseif ($mode == MODE_BACK) {
    if (isset($_POST["message"])) {
        $messages[] = $_POST["message"];
    }
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../templates/';
$smarty->compile_dir  = '../templates_c/';
$smarty->config_dir   = '../configs/';
$smarty->cache_dir    = '../cache/';

// Smartyに変数をバインド。
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("mode", $mode);

// 画面を表示する。
$smarty->display('alogin.tpl');


/**
 * enter処理
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean 実行成否
 */
function enterAction($items) {
    // 入力内容をチェックする。
    if (!checkInput($items)) {
        return false;
    }

    // パスワード入力画面に遷移する。
    $items = array(
        "mode" => "next",
        "loginId" => $items["loginId"]
    );
    goForwardPage("LG01/LG01002", $items, true);
    exit();
}

/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;

    // ログインID
    if ($items["loginId"] == "") {
        $messages[] = getCommonMessage("WC001", "ログインID");
        return false;
    }
    if (!validateMailAddress($items["loginId"])) {
        $messages[] = getCommonMessage("WC014");
        return false;
    }

    // チェック正常を返す。
    return true;
}
