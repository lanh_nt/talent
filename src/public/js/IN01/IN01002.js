$(function() {
	// 戻るボタン押下時
	$("#back_button").on("click", function() {
		// 遷移先を「一覧画面」に設定。
		$("#mainForm").attr("action", "IN01001");

		// 動作モードを「戻る」に設定。
		$("#mode").val("back");

		// submit
		$("#mainForm").submit();
	});
});
