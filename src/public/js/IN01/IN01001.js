$(function() {
	// 検索ボタン押下時 or ページリンク押下時
	$("#search_button").on("click", function () {
		// 動作モードを「検索」に設定。
		$("#mode").val("search");

		// ページ番号をリセット。
		$("#page_no").val("");

		// submit
		$("#mainForm").submit();
	});
	$("#pagerSelectedNo").on("change", function() {
		// 動作モードを「検索」に設定。
		$("#mode").val("search");

		// ページリンクボタンに対応するページ番号を設定。
		$("#page_no").val($("#pagerSelectedNo").val());

		// submit
		$("#mainForm").submit();
	});

	// 詳細ボタン押下時
	$(".detail-button").on("click", function() {
		// 押下ボタンに対応するユーザIDをフォームにセット。
		var idRegex = $(this).attr("id").match(/detail_button_(.*)/);
		$("#user_id").val(idRegex[1]);

		// 遷移先を「詳細表示画面」に設定。
		$("#mainForm").attr("action", "IN01002");

		// 動作モードを「初期表示」に設定。
		$("#mode").val("load");

		// submit
		$("#mainForm").submit();
	});
	// 登録ボタン押下時
	$(".regist-button").on("click", function() {
		// 押下ボタンに対応するユーザIDをフォームにセット。
		var idRegex = $(this).attr("id").match(/regist_button_(.*)/);
		$("#user_id").val(idRegex[1]);

		// 遷移先を「編集画面」に設定。
		$("#mainForm").attr("action", "IN01003");

		// 動作モードを「初期表示」に設定。
		$("#mode").val("load");

		// submit
		$("#mainForm").submit();
	});
});
