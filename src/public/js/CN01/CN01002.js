$(function() {
	// 翻訳登録/返信ボタン押下時
	$("#edit_button").on("click", function() {
		// 遷移先を「編集画面」に設定。
		$("#mainForm").attr("action", "CN01004");

		// 動作モードを「初期表示(更新)」に設定。
		$("#mode").val("edit-load");

		// submit
		$("#mainForm").submit();
	});

	// 戻るボタン押下時
	$("#back_button").on("click", function() {
		// 遷移先を「一覧画面」に設定。
		$("#mainForm").attr("action", "CN01001");

		// 動作モードを「戻る」に設定。
		$("#mode").val("back");

		// submit
		$("#mainForm").submit();
	});
});
