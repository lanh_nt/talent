$(function() {
	// 戻るボタン押下時
	$("#back_button").on("click", function() {
		// 遷移先を「詳細画面」に設定。
		$("#mainForm").attr("action", "CN01002");

		// 動作モードを「検索」に設定。
		$("#mode").val("back");

		// submit
		$("#mainForm").submit();
	});

	// 登録ボタン押下時
	$("#regist_button").on("click", function() {
		// 動作モードを「登録」に設定。
		$("#mode").val("regist");

		// submit
		$("#mainForm").submit();
	});
});
