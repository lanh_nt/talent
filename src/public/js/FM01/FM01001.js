$(function() {
	// 検索ボタン押下時 or ページリンク押下時
	$("#search_button").on("click", function () {
		// 動作モードを「検索」に設定。
		$("#mode").val("search");

		// ページ番号をリセット。
		$("#page_no").val("");

		// submit
		$("#mainForm").submit();
	});
	$("#pagerSelectedNo").on("change", function() {
		// 動作モードを「検索」に設定。
		$("#mode").val("search");

		// ページリンクボタンに対応するページ番号を設定。
		$("#page_no").val($("#pagerSelectedNo").val());

		// submit
		$("#mainForm").submit();
	});

	// 詳細ボタン押下時
	$(".detail-button").on("click", function() {
		// 押下ボタンに対応するお知らせIDをフォームにセット。
		var idRegex = $(this).attr("id").match(/detail_button_(.*)/);
		$("#report_ids").val(idRegex[1]);

		// 動作モードを「詳細表示」に設定。
		$("#mode").val("detail");

		// submit
		$("#mainForm").submit();
	});

	// 更新ボタン押下時
	$(".update-button").on("click", function() {
		// 押下ボタンに対応するお知らせIDをフォームにセット。
		var idRegex = $(this).attr("id").match(/update_button_(.*)/);
		$("#report_ids").val(idRegex[1]);

		// 動作モードを「更新」に設定。
		$("#mode").val("update");

		// submit
		$("#mainForm").submit();
	});

	// 一括更新ボタン押下時
	$("#multi-update-button").on("click", function() {
		// 押下ボタンに対応する帳票ステータスIDをフォームにセット。
		var ids = "";
		$('input[name="C_10"]:checked').each(function() {
			if (ids != "") ids += ",";
			ids += $(this).val();
		});
		$("#report_ids").val(ids);

		// 動作モードを「複数更新」に設定。
		$("#mode").val("multi-update");

		// submit
		$("#mainForm").submit();
	});

	// 一括チェック押下時
	$("#checkAll").on("click", function() {
		// 各選択チェックを一括変更する。
		$('.c-10').each(function() {
			$(this).prop('checked', $("#checkAll").prop('checked'));
		});
	});
	// 各選択チェック押下時
	$(".c-10").on("click", function() {
		// 一括チェックの値を更新。
		// ・すべて選択状態なら一括チェック=選択、ひとつでも非選択状態なら一括チェック=非選択。
		var isCheckAll = true;
		$('.c-10').each(function() {
			if (!$(this).prop('checked')) {
				isCheckAll = false;
				return false;
			}
		});
		$("#checkAll").prop('checked', isCheckAll);
	});

});
