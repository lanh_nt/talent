$(function() {
	// 戻るボタン押下時
	$("#return-button").on("click", function () {
		// 動作モードを「検索」に設定。
		$("#mode").val("search");

		// ページ番号をリセット。
		//$("#page_no").val("");

		// submit
		$("#mainForm").submit();
	});

	// 出力ボタン押下時
	$(".report-button").on("click", function() {
		// 押下ボタンに対応する帳票IDを取得。
		//alert("aaa");
		var idRegex = $(this).attr("id").match(/rirekiReport_(.*)/);
		var rirekireportId = idRegex[1];

		// 帳票出力処理&画面を、新しいタブで開く。
		window.open("FM99001?r="+rirekireportId, "_blank");
	});

});
