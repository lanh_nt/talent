$(function() {
	// 戻るボタン押下時
	$("#return-button").on("click", function () {
		// 動作モードを「検索」に設定。
		$("#mode").val("search");

		// submit
		$("#mainForm").submit();
	});

	// 登録ボタン押下時
	$("#regist-button").on("click", function () {
		// 動作モードを「登録」に設定。
		$("#mode").val("regist");

		// submit
		$("#mainForm").submit();
	});

	// 帳票編集ボタン押下時
	$("#report-button").on("click", function () {
		// 動作モードを「帳票編集」に設定。
		$("#mode").val("report");

		// submit
		$("#mainForm").submit();
	});

});
