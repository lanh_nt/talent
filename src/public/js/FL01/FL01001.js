$(function() {
	// 検索ボタン押下時 or ページリンク押下時
	$("#search_button").on("click", function () {
		// 動作モードを「検索」に設定。
		$("#mode").val("search");

		// ページ番号をリセット。
		$("#page_no").val("");

		// submit
		$("#mainForm").submit();
	});
	$("#pagerSelectedNo").on("change", function() {
		// 動作モードを「検索」に設定。
		$("#mode").val("search");

		// ページリンクボタンに対応するページ番号を設定。
		$("#page_no").val($("#pagerSelectedNo").val());

		// submit
		$("#mainForm").submit();
	});

	// アップロードボタン押下時
	$("#upload_button").on("click", function() {
		// 遷移先を「アップロード画面」に設定。
		$("#mainForm").attr("action", "FL01003");

		// 動作モードを「新規作成」に設定。
		$("#mode").val("insert-load");

		// submit
		$("#mainForm").submit();
	});

	// 詳細ボタン押下時
	$(".detail-button").on("click", function() {
		// 押下ボタンに対応するファイルIDをフォームにセット。
		var idRegex = $(this).attr("id").match(/detail_button_(.*)/);
		$("#file_rowid").val(idRegex[1]);

		// 遷移先を「詳細表示画面」に設定。
		$("#mainForm").attr("action", "FL01002");

		// 動作モードを「詳細表示」に設定。
		$("#mode").val("detail-load");

		// submit
		$("#mainForm").submit();
	});

	// 削除ボタン押下時
	$('.delete-button').on('click', function() {
		// 押下ボタンに対応するファイルIDをフォームにセット。
		var idRegex = $(this).attr("id").match(/delete_button_(.*)/);
		$("#file_rowid").val(idRegex[1]);

		// モーダルダイアログを開く。
		$("#openDelDialog").click();
	});
	// 削除ダイアログ：OKボタン押下時
	$('#execDeleteButton').on('click', function() {
		// 動作モードを「削除」に設定。
		$("#mode").val("delete");

		// submit
		$("#mainForm").submit();
	});
});
