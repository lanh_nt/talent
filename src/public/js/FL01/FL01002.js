$(function() {
	// 戻るボタン押下時
	$("#back_button").on("click", function() {
		// 遷移先を「一覧画面 or 労働者詳細」に設定。
		$("#mainForm").attr("action", $("#backToWorkerInfo").length == 0 ? "FL01001" : "../WK02/WK02012");

		// 動作モードを「ダウンロード」に設定。
		$("#mode").val("back");

		// submit
		$("#mainForm").submit();
	});

	// ダウンロードリンク押下時
	$("#filedownload").on("click", function() {
		// 遷移先を「ファイルダウンロード機能」に設定。
		$("#mainForm").attr("action", "FL01002P");

		if ($("#isDispDirect").val()) {
			// 新しいタブで、PDF出力を開く。
			window.open("FL01002P?t="+$("#csrf_token").val(), "_blank");
		} else {
			// 動作モードを「ダウンロード」に設定。
			$("#mode").val("download");

			// submit
			$("#mainForm").submit();
		}
	});
});
