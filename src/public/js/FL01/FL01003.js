$(function() {
	// 戻るボタン押下時
	$("#back_button").on("click", function() {
		// 遷移先を「一覧画面」に設定。
		$("#mainForm").attr("action", "FL01001");

		// 動作モードを「検索」に設定。
		$("#mode").val("back");

		// submit
		$("#mainForm").submit();
	});

	// 登録ボタン押下時
	$("#regist_button").on("click", function() {
		// 動作モードを「登録」に設定。
		$("#mode").val("regist");

		// submit
		$("#mainForm").submit();
	});

	// ファイル選択ボタン押下時
	$("#D_1_button").on("click", function() {
		// ファイルコントロールのクリックイベントを疑似発生。
		$("#D_1").click();
	});
	// ファイル登録時処理
	$("#D_1").on("change", function() {
		// 指定されたファイルをラベルに表示。
		files = $("#D_1")[0].files;
		if (files.length > 0) {
			$("#D_1_label").text(files[0].name);
		} else {
			$("#D_1_label").text("");
		}
	});
});

//=====================================
// 対象者ダイアログに関する処理
//=====================================
$(function() {
	// 画面表示時
	setd2sts();

	// 対象者決定ボタン押下時
	$("#d2okbutton").on("click", function() {
		// 対象者の状態ラベルを設定する。
		setd2sts();

		// 保存フラグを立てる。
		d2IsOk = true;

		// 対象者ダイアログを閉じる。
		$('.modaal').modaal('close');
	});

	// 対象者の状態ラベルを設定する。
	function setd2sts() {
		selectedId = $('input[name="D_2"]:checked').val();
		if (selectedId != null) {
			$("#d2sts").text($("#D_2_"+selectedId+"_N").val());
		} else {
			$("#d2sts").text("");
		}
	}
});

// ダイアログ項目保存
d2Stack = {};
// 保存終了したかどうか
d2IsOk = false;

// ダイアログオープン時処理：
function d2OpenEvent() {
	// 保存フラグをリセット。
	d2IsOk = false;

	// コントロールの状態を記憶。
	d2Stack = {};
	$('input[name="D_2"]').each(function() {
		d2Stack[$(this).prop('id')] = $(this).prop('checked');
	});
}

// ダイアログ：保存せずクローズ時処理
function d2QuitEvent() {
	if (!d2IsOk) {
		// コントロールの内容をオープン前に戻す。
		for (key in d2Stack) {
			$('#'+key).prop('checked', d2Stack[key]);
		}
	}
}
