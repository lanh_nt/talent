$(function() {
	// 戻るボタン押下時
	$("#return-button").on("click", function () {
		// 動作モードを「検索」に設定。
		$("#mode").val("search");

		// submit
		$("#mainForm").submit();
	});
});

$(function() {
	// 編集ボタン押下時
	$("#udate_button").on("click", function() {
		// 遷移先を「編集画面」に設定。
		$("#mainForm").attr("action", "CM01003");

		// 動作モードを「編集」に設定。
		$("#mode").val("update");

		// submit
		$("#mainForm").submit();
	});
});