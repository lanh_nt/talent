/**
 * 労働者詳細 共通機能
 */
$(function() {
	// 戻るボタン押下時
	$("#back_button").on("click", function() {
		// 遷移先を「労働者一覧画面」に設定。
		$("#mainForm").attr("action", "../WK01/WK01001");

		// 動作モードを「戻る」に設定。
		$("#mode").val("back");

		// submit
		$("#mainForm").submit();
	});
});
