$(function() {
	// フォーマット出力ボタン押下時
	$("#csv_format_button").on("click", function() {
		var result = window.confirm('履歴書フォーマットを出力します。よろしいですか？\n(出力後は、ダウンロードフォルダを確認してください。)');
		if (!result) return false;

		// 動作モードを「フォーマット出力」に設定。
		$("#mode").val("csv-format");

		// submit
		$("#mainForm").submit();
	});

	// CSV出力ボタン押下時
	$("#csv_export_button").on("click", function() {
		var result = window.confirm('履歴書データを出力します。よろしいですか？\n(出力後は、ダウンロードフォルダを確認してください。)');
		if (!result) return false;

		// 動作モードを「CSV出力」に設定。
		$("#mode").val("csv-export");

		// submit
		$("#mainForm").submit();
	});

	// CSV登録ボタン押下時
	$("#csv_import_button").on("click", function() {
		// ファイルコントロールのクリックイベントを疑似発生。
		$("#upfile").click();
	});
	// ファイル登録時処理
	$("#upfile").on("change", function() {
		files = $("#upfile")[0].files;
		if (files.length == 0) return false;

		// 動作モードを「CSV登録」に設定。
		$("#mode").val("csv-import");

		// submit
		$("#mainForm").submit();
	});
});
