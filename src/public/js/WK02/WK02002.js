$(function() {
	// 登録ボタン押下時
	$("#regist_button").on("click", function() {
		// 動作モードを「登録」に設定。
		$("#mode").val("regist");

		// submit
		$("#mainForm").submit();
	});
});