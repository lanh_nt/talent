$(function() {
	// 詳細ボタン押下時
	$(".detail-button").on("click", function() {
		// 押下ボタンに対応するファイルIDをフォームにセット。
		var idRegex = $(this).attr("id").match(/detail_button_(.*)/);
		$("#file_rowid").val(idRegex[1]);

		// 遷移先を「ファイル管理：詳細表示画面」に設定。
		$("#mainForm").attr("action", "../FL01/FL01002");

		// 動作モードを「詳細表示(労働者詳細から)」に設定。
		$("#mode").val("detail-load-worker");

		// submit
		$("#mainForm").submit();
	});
});
