$(function() {
	// 出力ボタン押下時
	$(".report-button").on("click", function() {
		// 押下ボタンに対応する帳票IDを取得。
		var idRegex = $(this).attr("id").match(/putReport(.*)/);
		var reportId = idRegex[1];

		// 帳票出力処理&画面を、新しいタブで開く。
		window.open("WK990"+reportId, "_blank");
	});
});
