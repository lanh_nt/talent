$(function() {
	// 次へボタン押下時
	$("#next_button").on("click", function () {
		// 動作モードを「検索」に設定。
		$("#mode").val("enter");

		// submit
		$("#mainForm").submit();
	});
});
