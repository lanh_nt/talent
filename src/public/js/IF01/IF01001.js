$(function() {
	// 検索ボタン押下時 or ページリンク押下時
	$("#search_button").on("click", function () {
		// 動作モードを「検索」に設定。
		$("#mode").val("search");

		// ページ番号をリセット。
		$("#page_no").val("");

		// submit
		$("#mainForm").submit();
	});
	$("#pagerSelectedNo").on("change", function() {
		// 動作モードを「検索」に設定。
		$("#mode").val("search");

		// ページリンクボタンに対応するページ番号を設定。
		$("#page_no").val($("#pagerSelectedNo").val());

		// submit
		$("#mainForm").submit();
	});

	// 新規作成ボタン押下時
	$("#insert_button").on("click", function() {
		// 遷移先を「新規作成画面」に設定。
		$("#mainForm").attr("action", "IF01002");

		// 動作モードを「新規作成」に設定。
		$("#mode").val("insert-load");

		// submit
		$("#mainForm").submit();
	});

	// 詳細ボタン押下時
	$(".detail-button").on("click", function() {
		// 押下ボタンに対応するお知らせIDをフォームにセット。
		var idRegex = $(this).attr("id").match(/detail_button_(.*)/);
		$("#info_id").val(idRegex[1]);

		// 遷移先を「詳細表示画面」に設定。
		$("#mainForm").attr("action", "IF01005");

		// 動作モードを「詳細表示」に設定。
		$("#mode").val("detail-load");

		// submit
		$("#mainForm").submit();
	});
});
