$(function() {
	// 戻るボタン押下時
	$("#back_button").on("click", function() {
		// 遷移先を「一覧画面」に設定。
		$("#mainForm").attr("action", "IF01001");

		// 動作モードを「検索」に設定。
		$("#mode").val("back");

		// submit
		$("#mainForm").submit();
	});

	// 登録ボタン押下時
	$("#regist_button").on("click", function() {
		// 動作モードを「登録」に設定。
		$("#mode").val("regist");

		// submit
		$("#mainForm").submit();
	});
});

//=====================================
// 宛先ダイアログに関する処理
//=====================================
$(function() {
	// 画面表示時
	setd4button();
	setd4sts();

	// 企業名変更時
	$("#D_3").on("change", function() {
		// 動作モードを「企業名変更」に設定。
		$("#mode").val("change-company");

		// submit
		$("#mainForm").submit();
	});

	// 宛先決定ボタン押下時
	$("#d4okbutton").on("click", function() {
		// 宛先の状態ラベルを設定する。
		setd4sts();

		// 保存フラグを立てる。
		d4IsOk = true;

		// 宛先ダイアログを閉じる。
		$('.modaal').modaal('close');
	});

	// 宛先：全員選択時
	$("#D_4_1_all").on("click", function () {
		// 労働者のチェックを外す。
		$(".worker-radio").prop("checked", false);
	});
	// 宛先：労働者選択時
	$(".worker-radio").on("click", function () {
		// 宛先：個別を選択する。
		$("#D_4_1_sep").prop("checked", true);
	});
	
	// 宛先選択ボタンの使用可否を切り替える。
	// ※実際は、企業名の選択有無により、enable/disableボタンのどちらを表示するかを決定する。
	function setd4button() {
		d3val = $("#D_3").val();
		if (d3val != "") {
			$("#d4openbutton").removeClass("is-hidden");
			$("#d4dummybutton").addClass("is-hidden");
		} else {
			$("#d4openbutton").addClass("is-hidden");
			$("#d4dummybutton").removeClass("is-hidden");
		}
	}
	// 宛先の状態ラベルを設定する。
	function setd4sts() {
		d4stsText = "";
		d4val = $('input[name="D_4_1"]:checked').val();
		if (d4val == "1") {
			d4stsText = "全員";
		} else if (d4val == "2") {
			d4stsText = "個別";
			d4memText = "";
			$('input[name="D_4_2[]"]:checked').each(function() {
				labelId = '#' + $(this).prop('id') + '_L';
				if (d4memText != "") { d4memText += ", "; }
				d4memText += $(labelId).text();
			});
			if (d4memText != "") {
				d4stsText += " (" + d4memText + ")";
			}
		}
		$("#d4sts").text(d4stsText);
	}
});

// ダイアログ項目保存
d4Stack = {};
// 保存終了したかどうか
d4IsOk = false;

// ダイアログオープン時処理：
function d4OpenEvent() {
	// 保存フラグをリセット。
	d4IsOk = false;

	// コントロールの状態を記憶。
	d4Stack = {};
	$('input[name="D_4_1"]').each(function() {
		d4Stack[$(this).prop('id')] = $(this).prop('checked');
	});
	$('input[name="D_4_2[]"]').each(function() {
		d4Stack[$(this).prop('id')] = $(this).prop('checked');
	});
}

// ダイアログ：保存せずクローズ時処理
function d4QuitEvent() {
	if (!d4IsOk) {
		// コントロールの内容をオープン前に戻す。
		for (key in d4Stack) {
			$('#'+key).prop('checked', d4Stack[key]);
		}
	}
}
