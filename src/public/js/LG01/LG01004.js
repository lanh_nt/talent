$(function() {
	// 変更ボタン押下時
	$("#entry_button").on("click", function () {
		// 動作モードを「変更」に設定。
		$("#mode").val("entry");

		// submit
		$("#mainForm").submit();
	});
});
