$(function() {
	// 送信ボタン押下時
	$("#sendmail_button").on("click", function () {
		// 動作モードを「メール送信」に設定。
		$("#mode").val("sendmail");

		// submit
		$("#mainForm").submit();
	});

	// 戻るボタン押下時
	$("#back_button").on("click", function () {
		// 遷移先を「パスワード入力画面」に設定。
		$("#mainForm").attr("action", "LG01002");

		// 動作モードを「初期表示」に設定。
		$("#mode").val("load");

		// submit
		$("#mainForm").submit();
	});
});
