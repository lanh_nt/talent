$(function() {
	// 次へボタン押下時
	$("#next_button").on("click", function () {
		// 動作モードを「検索」に設定。
		$("#mode").val("enter");

		// submit
		$("#mainForm").submit();
	});

	// 戻るボタン押下時
	$("#back_button").on("click", function () {
		// 動作モードを「戻る」に設定。
		$("#mode").val("back");

		// submit
		$("#mainForm").submit();
	});

	// パスワードリセットのリンク押下時
	$("#pwdreset").on("click", function () {
		// 遷移先を「パスワードリセット画面」に設定。
		$("#mainForm").attr("action", "LG01003");

		// 動作モードを「初期表示」に設定。
		$("#mode").val("load");

		// submit
		$("#mainForm").submit();
	});
});
