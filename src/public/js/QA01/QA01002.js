$(function() {
	// 戻るボタン押下時
	$("#return-button").on("click", function () {
		// 動作モードを「検索」に設定。
		$("#mode").val("search");

		// submit
		$("#mainForm").submit();
	});

	// 編集ボタン押下時
	$("#update-button").on("click", function () {
		// 動作モードを「編集」に設定。
		$("#mode").val("update");

		// submit
		$("#mainForm").submit();
	});

});
