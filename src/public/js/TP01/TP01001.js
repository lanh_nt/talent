$(function() {
	// 詳細ボタン押下時
	$(".detail-button").on("click", function() {
		// 押下ボタンに対応する帳票ID/ユーザIDをフォームにセット。
		var idRegex = $(this).attr("id").match(/detail_button_[123]_([0-9]{2})_(.*)/);
		$("#report_id").val(idRegex[1]);
		$("#worker_id").val(idRegex[2]);

		// 動作モードを「詳細表示」に設定。
		$("#mode").val("detail");

		// submit
		$("#mainForm").submit();
	});
});
