$(function() {
	// 検索ボタン押下時 or ページリンク押下時
	$("#search_button").on("click", function () {
		// 動作モードを「検索」に設定。
		$("#mode").val("search");

		// ページ番号をリセット。
		$("#page_no").val("");

		// submit
		$("#mainForm").submit();
	});
	$("#pagerSelectedNo").on("change", function() {
		// 動作モードを「検索」に設定。
		$("#mode").val("search");

		// ページリンクボタンに対応するページ番号を設定。
		$("#page_no").val($("#pagerSelectedNo").val());

		// submit
		$("#mainForm").submit();
	});

	// 詳細ボタン押下時
	$(".detail-button").on("click", function() {
		// 押下ボタンに対応するお知らせIDをフォームにセット。
		var idRegex = $(this).attr("id").match(/detail_button_(.*)/);
		$("#ad_id").val(idRegex[1]);

		// 動作モードを「詳細表示」に設定。
		$("#mode").val("detail");

		// submit
		$("#mainForm").submit();
	});

	// フォーマット出力ボタン押下時
	$("#csv_format_button").on("click", function() {
		var result = window.confirm('管理者フォーマットを出力します。よろしいですか？\n(出力後は、ダウンロードフォルダを確認してください。)');
		if (!result) return false;

		// 動作モードを「フォーマット出力」に設定。
		$("#mode").val("csv-format");

		// submit
		$("#mainForm").submit();
	});

	// CSV出力ボタン押下時
	$("#csv_export_button").on("click", function() {
		var result = window.confirm('管理者データを出力します。よろしいですか？\n(出力後は、ダウンロードフォルダを確認してください。)');
		if (!result) return false;

		// 動作モードを「CSV出力」に設定。
		$("#mode").val("csv-export");

		// submit
		$("#mainForm").submit();
	});

	// CSV登録ボタン押下時
	$("#csv_import_button").on("click", function() {
		// ファイルコントロールのクリックイベントを疑似発生。
		$("#upfile").click();
	});
	// ファイル登録時処理
	$("#upfile").on("change", function() {
		files = $("#upfile")[0].files;
		if (files.length == 0) return false;

		// 動作モードを「CSV登録」に設定。
		$("#mode").val("csv-import");

		// submit
		$("#mainForm").submit();
	});
});
