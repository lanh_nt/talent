$(function() {
	/**
	 * フォーム上での Enter キー無効化処理
	 */
	$("input").on("keypress", function(e) {
		// 押下キーがEnterキーの場合、入力を無効にする。
		if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
			return false;
		}

		// それ以外の場合、入力を有効とする。
		return true;
	});


	/**
	 * ページング部品のクリック時処理
	 */
	$(".pager-button").on("click", function() {
		// IDからページ番号を切り出す。
		var idRegex = $(this).attr("id").match(/.*_([0-9]*)$/);
		pageNo = idRegex[1];
		// ※ページ番号が切り出せなかった場合は処理しない。
		if (pageNo == "") return false;

		// ページ番号を変更。
		$("#pagerSelectedNo").val(pageNo);

		// 画面側に処理を捕捉させるため、changeイベントを発火する。
		$("#pagerSelectedNo").trigger("change");
	});
});
