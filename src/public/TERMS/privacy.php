<?php
/**
 * 労働者：個人情報の取り扱いについて
 * 
 */
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';

const FUNC_ID  = "TERMS";
const SCENE_ID = "privacy";

session_start();

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);

// 画面を表示する。
if ($_SESSION["loginUserLang"] == "2") {
    $templateName = "TERMS/privacy-e.tpl";
} elseif ($_SESSION["loginUserLang"] == "3") {
    $templateName = "TERMS/privacy-v.tpl";
} else {
    $templateName = "TERMS/privacy-j.tpl";
}
$smarty->display($templateName);
