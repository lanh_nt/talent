<?php
/**
 * 労働者管理：検索画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../lib/PageData.php';
require_once '../../lib/ses_mailSender.php';
require_once '../../da/WK01/WK01001.php';

use League\Csv\Writer;
use League\Csv\Reader;

const FUNC_ID  = "WK01";
const SCENE_ID = "WK01001";

const MODE_LOAD   = "load";
const MODE_SEARCH = "search";
const MODE_DETAIL = "detail";
const MODE_IMPORT = "csv-import";
const MODE_EXPORT = "csv-export";
const MODE_FORMAT = "csv-format";
const MODE_BACK   = "back";

const CSV_FORMAT_NAME = "WK01001";
const CSV_OUTPUT_NAME = "労働者基本情報";

// CSVデータのカラム数
const COLUMN_COUNT = 145;

const COL_MAILADDRESS = 1 - 1;
const COL_COMPANY_ID  = 2 - 1;
const COL_DISP_LANG   = 3 - 1;
const COL_NAME_E      = 10 - 1;
const COL_FAMILY_NAME = 11 - 1;
const COL_GIVEN_NAME  = 12 - 1;
const COL_HOME_TOWN   = 17 - 1;

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。
$condItemNames = array(
    "C_01", "C_02", 
    "C_03_1", "C_03_2", "C_03_3", "C_03_4", "C_03_5", "C_03_6", "C_03_etc", 
    "C_04_1", "C_04_2", "C_04_3", "C_04_4", "C_04_5", "C_04_6", 
    "C_05_1", "C_05_2", 
    "C_06", 
    "C_07_1", "C_07_2", 
    "C_08_1", "C_08_2", 
    "page_no", "is_search", "worker_id"
);
if ($mode == MODE_BACK) {
    // 別画面から戻ってきた場合は、sessionから条件を復帰。
    $conds = (isset($_SESSION["WK01_conds"])) ? $_SESSION["WK01_conds"] : array();
    $items = getParamsArray($conds, $condItemNames);
} else {
    // POST内容から構成。
    $items = getParamsArray($_POST, $condItemNames);

    if ($mode == MODE_LOAD) {
        // 初期表示の場合、チェックボックスのデフォルトをONに設定。
        setDefaultValues($items);
    }
}

// モードに対応する処理を実行。
if ($mode == MODE_DETAIL) {
    // ------------------------------------------
    // 詳細ボタン処理
    // ------------------------------------------
    // 指定された労働者IDをセッションに設定。
    $_SESSION['targetWorkerId'] = $items["worker_id"];
    
    // 検索条件を保存する。
    $_SESSION["WK01_conds"] = $items;

    // 労働者詳細：基本画面に遷移する。
    goForwardPage("../WK02/WK02001", array());
    exit();

} elseif ($mode == MODE_FORMAT) {
    // ------------------------------------------
    // フォーマット出力ボタン処理
    // ------------------------------------------
    // フォーマットファイルを出力する。
    formatDownload(CSV_FORMAT_NAME, CSV_OUTPUT_NAME);
    exit();

} elseif ($mode == MODE_EXPORT) {
    // ------------------------------------------
    // CSV出力ボタン処理
    // ------------------------------------------
    // 画面の検索条件にて、労働者情報のCSVファイルを出力する。
    csvExport($items);
    exit();

} elseif ($mode == MODE_IMPORT) {
    // ------------------------------------------
    // CSV登録ボタン処理
    // ------------------------------------------
    $upfile = $_FILES["upfile"];
    csvImport($upfile);

}

// ------------------------------------------
// 検索処理を実行する。
// ・検索ボタン押下時 or CSV登録ボタン押下時 or 別画面から戻ってきた時
// ------------------------------------------
$pageData = null;
if ($mode == MODE_SEARCH or ($mode == MODE_IMPORT and $items["is_search"] == "yes") or ($mode == MODE_BACK and $items["is_search"] == "yes")) {
    if (checkInput($items)) {
        // 表示ページを設定。
        $pageNo = ($items["page_no"] != "") ? $items["page_no"] : 1;

        // 検索処理を実行。
        $pageData = search($items, $pageNo);

        // 検索実施済みを立てる。
        $items["is_search"] = "yes";
    }
} else {
    $items["is_search"] = "";
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("purposes", getOptionItems("comm", "purpose"));
if ($_SESSION["loginCompanyType"] == "2") {
    $smarty->assign("companies", getCompanies());
}
$smarty->assign("pageData", $pageData);

// 画面を表示する。
$smarty->display('WK01/WK01001.tpl');


/**
 * デフォルト値を設定する。
 *
 * @param array[項目名 => 値] $items 画面項目一覧
 */
function setDefaultValues(&$items) {
    // チェックボックスのデフォルトをONに設定。
    $items["C_03_1"] = true;
    $items["C_03_2"] = true;
    $items["C_03_3"] = true;
    $items["C_03_4"] = true;
    $items["C_03_5"] = true;
    $items["C_03_6"] = true;

    $items["C_04_1"] = true;
    $items["C_04_2"] = true;
    $items["C_04_3"] = true;
    $items["C_04_4"] = true;
    $items["C_04_5"] = true;
    $items["C_04_6"] = true;

    $items["C_05_1"] = true;
    $items["C_05_2"] = true;
}


/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;
    global $labels;

    $isCheckOk = true;

    // 禁止文字のチェックを行う。
    if (!validateProhibitedCharacters($items)) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // ・登録日
    if ($items["C_07_1"] != "" and $items["C_07_2"] != "") {
        if ($items["C_07_1"] > $items["C_07_2"]) {
            // from>toはエラー。
            $messages[] = getCommonMessage("WC017", "登録日");
            $isCheckOk = false;
        }
    }

    // ・在留期間の満了日
    if ($items["C_08_1"] != "" and $items["C_08_2"] != "") {
        if ($items["C_08_1"] > $items["C_08_2"]) {
            // from>toはエラー。
            $messages[] = getCommonMessage("WC017", "在留期間の満了日");
            $isCheckOk = false;
        }
    }

    // チェック結果を返す。
    return $isCheckOk;
}


/**
 * 画面の検索条件にて、労働者情報のCSVファイルを出力する。
 *
 * @param array[項目名 => 値] $items 画面項目一覧
 */
function csvExport($items) {
    // DBから対象データを取得する。
    $result = getExportDatas($items);

    // CSVライターを生成。
    $writer = Writer::createFromString("");
    $writer->setOutputBOM(Writer::BOM_UTF8);  // Excelで開けるようにBOMを付与。
    $writer->setNewline("\r\n");

    // CSVデータを積み上げる。
    $writer->insertAll($result);

    // CSVデータをresponseとして出力する。
    date_default_timezone_set('Asia/Tokyo');
    $fileName = CSV_OUTPUT_NAME."_".date("Ymd_Hi").".csv";
    $writer->output($fileName);

    exit();
}


/**
 * アップロードされたCSVファイルから、労働者データを更新する。
 *
 * @param ファイル $upfile アップロードされたファイル
 * 
 * @return void
 */
function csvImport($upfile) {
    global $messages;

    // ファイル内容をチェックする。
    // ・拡張子
    if (!preg_match("/\.csv$/u", $upfile["name"])) {
        $messages[] = getCommonMessage("WC003");
        return false;
    }
    // ・サイズ
    if ($upfile["size"] == 0) {
        $messages[] = getCommonMessage("WC004");
        return false;
    }

    // CSVデータを読み込む。
    if (0 === strpos(PHP_OS, 'WIN')) setlocale(LC_CTYPE, 'C');  // windows環境でCSVパースを正常動作させるためのおまじない。
    $reader = Reader::createFromPath($upfile["tmp_name"], 'r');
    $reader->includeEmptyRecords();
    $csvdatas = $reader->getRecords();
    $datas = array();
    foreach ($csvdatas as $csvrow) {
        $datas[] = $csvrow;
    }

    $rowNo = 0;
    $keyInfo = array();
    $isError = false;

    // データの内容を検証する。
    foreach ($datas as $row) {
        $rowNo++;
        $rowInfo = " : ".$rowNo." 列目";
        $rowCount = count($row);

        // ・カラム数
        if ($rowCount < COLUMN_COUNT) {
            $messages[] = getCommonMessage("WC005").$rowInfo;
            $isError = true;
        }
        // ・必須
        if ($row[COL_MAILADDRESS] == "") {
            $messages[] = getCommonMessage("WC001", "メールアドレス").$rowInfo;
            $isError = true;
        }
        // 空レコードなら次行にスキップ。
        if (isEmptyRecord($row, COL_MAILADDRESS + 1)) continue;

        // ・必須
        if ($rowCount >= COL_COMPANY_ID and $row[COL_COMPANY_ID] == "") {
            $messages[] = getCommonMessage("WC001", "企業ID").$rowInfo;
            $isError = true;
        }
        if ($rowCount >= COL_DISP_LANG and $row[COL_DISP_LANG] == "") {
            $messages[] = getCommonMessage("WC001", "表示言語").$rowInfo;
            $isError = true;
        }
        // ・文字種
        if ($rowCount >= COL_NAME_E and !validateAlphabetCharacters($row[COL_NAME_E])) {
            $messages[] = getCommonMessage("WC016", "氏名（英字）").$rowInfo;
            $isError = true;
        }
        if ($rowCount >= COL_FAMILY_NAME and !validateAlphabetCharacters($row[COL_FAMILY_NAME])) {
            $messages[] = getCommonMessage("WC016", "ファミリーネーム").$rowInfo;
            $isError = true;
        }
        if ($rowCount >= COL_GIVEN_NAME and !validateAlphabetCharacters($row[COL_GIVEN_NAME])) {
            $messages[] = getCommonMessage("WC016", "ギブンネーム").$rowInfo;
            $isError = true;
        }
        // ・形式
        if ($rowCount >= COL_MAILADDRESS and !validateMailAddress($row[COL_MAILADDRESS])) {
            $messages[] = getCommonMessage("WC015", "ログインID");
            $isError = true;
        }
        // ・禁止文字
        //   (除外：本国における居住地)
        if (!validateProhibitedCharacters($row, COL_HOME_TOWN)) {
            $messages[] = getCommonMessage("WC013");
            $isError = true;
        }
        // ・重複(CSV内)
        if ($rowCount >= COL_MAILADDRESS) {
            if (in_array($row[COL_MAILADDRESS], $keyInfo)) {
                $messages[] = getCommonMessage("WC002", "メールアドレス").$rowInfo;
                $isError = true;
            }
            $keyInfo[] = $row[COL_MAILADDRESS];
        }
        // 企業が自企業かどうか(受入機関の場合)
        if ($_SESSION["loginCompanyType"] == "1") {
            if ($rowCount >= COL_COMPANY_ID and $row[COL_COMPANY_ID] != "") {
                if ($row[COL_COMPANY_ID] != $_SESSION["loginCompanyId"]) {
                    $messages[] = getCommonMessage("WC020");
                    $isError = true;
                }
            }
        }
    }
    if ($isError) {
        return false;
    }

    // データを登録する。
    $isImportOk = csvDataImport($datas);
    if (!$isImportOk) {
        $messages[] = getCommonMessage("EC005");
        return false;
    }
    
    $messages[] = getCommonMessage("IC001");
    return true;
}
