<?php
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/QA01/QA02001.php';

const FUNC_ID  = "QA01";
const SCENE_ID = "QA02001";

const MODE_LOAD   = "load";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。
$condItemNames = array(
);
// POST内容から構成。
$items = getParamsArray($_POST, $condItemNames);

// 検索処理を実行。(ログインユーザーの所属企業コードを保持)
$rowsDatas = search($_SESSION['loginCompanyRowId'],$_SESSION['loginUserLang']);


// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("rowsDatas", $rowsDatas);
$smarty->assign("purposes", getOptionItems("comm", "purpose"));
if ($_SESSION["loginCompanyType"] == "2") {
    $smarty->assign("companies", getCompanies());
}

// 画面を表示する。
$smarty->display('QA01/QA02001.tpl');
