<?php
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/QA01/QA01005.php';

const FUNC_ID  = "QA01";
const SCENE_ID = "QA01005";

const MODE_LOAD   = "load";
const MODE_SEARCH = "search";
const MODE_UPDATE = "update";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。
$condItemNames = array(
    "C_01", "C_02", "C_03", 
    "C_04_1", "C_04_2",
    "C_05_1", "C_05_2", "C_05_3", 
    "page_no", "is_search", "qa_id", "mode"
);
// POST内容から構成。
$items = getParamsArray($_POST, $condItemNames);


// モードに対応する処理を実行。
if ($mode == MODE_UPDATE) {
    // ------------------------------------------
    // 詳細ボタン処理
    // ------------------------------------------
    // 指定されたQAIDをセッションに設定。
    $_SESSION['targetQaId'] = $items["qa_id"];
    $items["mode"]="";
    // QA詳細画面に遷移する。
    goForwardPage("../QA01/QA01002", $items);
    exit();

}

// モードに対応する処理を実行。
if ($mode == MODE_SEARCH) {
    // 検索一覧へ移動。
    goForwardPage("../QA01/QA01001", $items);
    exit();

}


// ------------------------------------------
// 検索処理を実行する。
// ------------------------------------------
// 検索処理を実行。
$rowsData = search($_SESSION['targetQaId']);


// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("displangs", getOptionItems("comm", "lang"));
if ($_SESSION["loginCompanyType"] == "2") {
    $smarty->assign("companies", getCompanies());
}
$smarty->assign("rowsData", $rowsData);

// 画面を表示する。
$smarty->display('QA01/QA01005.tpl');
