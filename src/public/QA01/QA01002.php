<?php
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/QA01/QA01002.php';

const FUNC_ID  = "QA01";
const SCENE_ID = "QA01002";

const MODE_LOAD   = "load";
const MODE_SEARCH = "search";
const MODE_UPDATE = "update";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。
$condItemNames = array(
    "C_01", "C_02", "C_03", 
    "C_04_1", "C_04_2",
    "C_05_1", "C_05_2", "C_05_3", 
    "page_no", "is_search", "qa_id", "mode",
    "question_no","c_name","disp_lang","release_status","question_contents","answer_contents"
);
// POST内容から構成。
$items = getParamsArray($_POST, $condItemNames);


// モードに対応する処理を実行。
if ($mode == MODE_UPDATE) {

    // 指定されたQAIDをセッションに設定。
    $_SESSION['targetQaId'] = $items["qa_id"];

    $result = registAction($items["qa_id"],$items);

}

// モードに対応する処理を実行。
if ($mode == MODE_SEARCH) {
    // 詳細画面へ移動。
    $items["mode"]="";
    goForwardPage("../QA01/QA01005", $items);
    exit();

}

// トークンを更新する。
updateToken();

// ------------------------------------------
// 検索処理を実行する。
// ------------------------------------------
// 検索処理を実行。
$rowsData = search($_SESSION['targetQaId']);

if ($mode == "") {
    //初回遷移時はPOST内容で入力用フォームを代入する
    $items["question_no"]=$rowsData["question_no"];
    $items["c_name"]=$rowsData["cp_id"];
    $items["disp_lang"]=$rowsData["delivery_language"];
    $items["release_status"]=$rowsData["release_status"];
    $items["question_contents"]=$rowsData["question_contents"];
    $items["answer_contents"]=$rowsData["answer_contents"];
}


// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("displangs", getOptionItems("comm", "lang"));
$smarty->assign("release_statuses", getOptionItems("comm", "release_status"));
if ($_SESSION["loginCompanyType"] == "2") {
    $smarty->assign("companies", getCompanies());
}
$smarty->assign("rowsData", $rowsData);

// 画面を表示する。
$smarty->display('QA01/QA01002.tpl');


/**
 * regist処理
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean 実行成否
 */
function registAction($id,$items) {
    global $messages;

    // トークンをチェックする。
    checkToken();

    // 入力内容をチェックする。
    if (!checkInput($items)) {
        return false;
    }

    // 内容更新を実行。
      
    $isRegistOk = update($id,$items);  
    if (!$isRegistOk) {
        $messages[] = getCommonMessage("EC005");
        return false;
    }
    // トークンを更新する。
    updateToken();

    $messages[] = getCommonMessage("IC002");
    return true;
}

/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;
    global $labels;

    $isCheckOk = true;

    // 禁止文字のチェックを行う。
    // (除外：質問内容、回答内容)
    if (!validateProhibitedCharacters($items, "question_contents", "answer_contents")) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // 質問NOが入力されていない場合はエラー。
    if ($items["question_no"] == "") {
        $messages[] = getCommonMessage("WC001", "質問NO");  // $labels["D_3"]
        $isCheckOk = false;
    }
    // 企業が指定されていない場合はエラー。
    if ($items["c_name"] == "") {
        $messages[] = getCommonMessage("WC001", "企業名");  // $labels["D_3"]
        $isCheckOk = false;
    }
    // 表示言語が指定されていない場合はエラー。
    if ($items["disp_lang"] == "") {
        $messages[] = getCommonMessage("WC001", "表示言語");  // $labels["D_3"]
        $isCheckOk = false;
    }
    // 公開状況が指定されていない場合はエラー。
    if ($items["release_status"] == "") {
        $messages[] = getCommonMessage("WC001", "公開状況");  // $labels["D_3"]
        $isCheckOk = false;
    }

    // チェック結果を返す。
    return $isCheckOk;
}
