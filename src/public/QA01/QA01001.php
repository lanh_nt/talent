<?php
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../lib/PageData.php';
require_once '../../da/QA01/QA01001.php';

use League\Csv\Writer;
use League\Csv\Reader;

const FUNC_ID  = "QA01";
const SCENE_ID = "QA01001";

const MODE_LOAD   = "load";
const MODE_SEARCH = "search";
const MODE_DETAIL = "detail";
const MODE_SORT = "sort";
const MODE_IMPORT = "csv-import";
const MODE_EXPORT = "csv-export";
const MODE_FORMAT = "csv-format";

const CSV_FORMAT_NAME = "QA01001";
const CSV_OUTPUT_NAME = "QA管理";

// CSVデータのカラム数
const COLUMN_COUNT = 7;

const COL_QA_NO      = 1 - 1;
const COL_COMPANY_ID = 2 - 1;
const COL_DISP_SEQ   = 3 - 1;
const COL_QUESTION   = 6 - 1;
const COL_ANSWER     = 7 - 1;


session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。
$condItemNames = array(
    "C_01", "C_02", "C_03", 
    "C_04_1", "C_04_2",
    "C_05_1", "C_05_2", "C_05_3", 
    "page_no", "is_search", "qa_id", "qa_arr"
);
// POST内容から構成。
$items = getParamsArray($_POST, $condItemNames);

if ($mode == MODE_LOAD) {
    // 初期表示の場合、チェックボックスのデフォルトをONに設定。
    setDefaultValues($items);
}

// モードに対応する処理を実行。
if ($mode == MODE_DETAIL) {
    // ------------------------------------------
    // 詳細ボタン処理
    // ------------------------------------------
    // 指定されたQAIDをセッションに設定。
    $_SESSION['targetQaId'] = $items["qa_id"];

    // QA詳細画面に遷移する。
    goForwardPage("../QA01/QA01005", $items);
    exit();

} elseif ($mode == MODE_FORMAT) {
    // ------------------------------------------
    // フォーマット出力ボタン処理
    // ------------------------------------------
    // フォーマットファイルを出力する。
    formatDownload(CSV_FORMAT_NAME, CSV_OUTPUT_NAME);
    exit();

} elseif ($mode == MODE_EXPORT) {
    // ------------------------------------------
    // CSV出力ボタン処理
    // ------------------------------------------
    // 画面の検索条件にて、QA情報のCSVファイルを出力する。
    csvExport($items);
    exit();

} elseif ($mode == MODE_IMPORT) {
    // ------------------------------------------
    // CSV登録ボタン処理
    // ------------------------------------------
    $upfile = $_FILES["upfile"];
    csvImport($upfile);

} elseif ($mode == MODE_SORT) {
    // ------------------------------------------
    // 順序変更ボタン処理
    // ------------------------------------------
    // 表示ページを設定。
    $pageNo = ($items["page_no"] != "") ? $items["page_no"] : 1;

    // 検索実施済みを立てる。
    $items["is_search"] = "yes";
    $mode = MODE_SEARCH;

    //POST前に、送信する表示順序でならべたレコードID（A)をスクリプトで保持し、受け取る
    //echo "qa_arr:::" . $items["qa_arr"];

    //検索と同じ条件でデータを取得
    // 検索処理を実行。
    $pageData = search($items, $pageNo);

    //検索順に従ったリストのID（B)とソート番号のリストを作成する。
    $data_arr_org=[];
    $data_arr_upd=[];
    $count = 0;
    foreach($pageData->pageDatas as $row){
        $data_arr_org[$count][0]=$row["qa_id"];
        $data_arr_org[$count][1]=$row["sequence"];

        $data_arr_upd[$count][0]=$row["qa_id"];
        $data_arr_upd[$count][1]=$row["sequence"];

        $count++;
    }
    //リスト（A)のIDが（B)のリストに照らして何番目にあるか？を検査
    $sort_arr = explode(",",$items["qa_arr"]);

    for($i = 0;$i < count($sort_arr);$i++ ){
        for($j = 0;$j < count($data_arr_org);$j++ ){
            if ($sort_arr[$i]==$data_arr_org[$j][0]){
                //並び変更のリストのIDと一致したオリジナルのIDが見つかったら
                //更新用のID,ソート順を管理する配列の情報を修正する
                //　リスト（A)のIDと（B)のIDが一致した場合、（A)に（B)のシーケンス番号を登録する
                $data_arr_upd[$j][1]=$data_arr_org[$i][1];
            }
        }
    
    }

    // DB更新を実行。
    $isRegistOk = seq_update($data_arr_upd);
    if (!$isRegistOk) {
        $messages[] = getCommonMessage("EC005");
        return false;
    }else{
        $messages[] = getCommonMessage("IC002");

    }

}

// ------------------------------------------
// 検索処理を実行する。
// ・検索ボタン押下時 or CSV登録ボタン押下時
// ------------------------------------------
$pageData = null;
if ($mode == MODE_SEARCH or ($mode == MODE_IMPORT and $items["is_search"] == "yes")) {
    if (checkInput($items)) {
        // 表示ページを設定。
        $pageNo = ($items["page_no"] != "") ? $items["page_no"] : 1;

        // 検索処理を実行。
        $pageData = search($items, $pageNo);

        // 検索実施済みを立てる。
        $items["is_search"] = "yes";
    }
} else {
    $items["is_search"] = "";
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("purposes", getOptionItems("comm", "purpose"));
if ($_SESSION["loginCompanyType"] == "2") {
    $smarty->assign("companies", getCompanies());
}
$smarty->assign("pageData", $pageData);


// 画面を表示する。
$smarty->display('QA01/QA01001.tpl');


/**
 * デフォルト値を設定する。
 *
 * @param array[項目名 => 値] $items 画面項目一覧
 */
function setDefaultValues(&$items) {
    // チェックボックスのデフォルトをONに設定。
    $items["C_05_1"] = true;
    $items["C_05_2"] = true;
    $items["C_05_3"] = true;
}


/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;
    global $labels;

    $isCheckOk = true;

    // 禁止文字のチェックを行う。
    if (!validateProhibitedCharacters($items)) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // ・登録日
    if ($items["C_04_1"] != "" and $items["C_04_2"] != "") {
        if ($items["C_04_1"] > $items["C_04_2"]) {
            // from>toはエラー。
            $messages[] = getCommonMessage("WC017", "登録日");
            $isCheckOk = false;
        }
    }

    // チェック結果を返す。
    return $isCheckOk;
}


/**
 * 画面の検索条件にて、QA情報のCSVファイルを出力する。
 *
 * @param array[項目名 => 値] $items 画面項目一覧
 */
function csvExport($items) {
    // DBから対象データを取得する。
    $result = getExportDatas($items);

    // CSVライターを生成。
    $writer = Writer::createFromString("");
    $writer->setOutputBOM(Writer::BOM_UTF8);  // Excelで開けるようにBOMを付与。
    $writer->setNewline("\r\n");

    // CSVデータを積み上げる。
    $writer->insertAll($result);

    // CSVデータをresponseとして出力する。
    date_default_timezone_set('Asia/Tokyo');
    $fileName = CSV_OUTPUT_NAME."_".date("Ymd_Hi").".csv";
    $writer->output($fileName);

    //exit();
}


/**
 * アップロードされたCSVファイルから、QAデータを更新する。
 *
 * @param ファイル $upfile アップロードされたファイル
 * 
 * @return void
 */
function csvImport($upfile) {
    global $messages;

    // ファイル内容をチェックする。
    // ・拡張子
    if (!preg_match("/\.csv$/u", $upfile["name"])) {
        $messages[] = getCommonMessage("WC003");
        return false;
    }
    // ・サイズ
    if ($upfile["size"] == 0) {
        $messages[] = getCommonMessage("WC004");
        return false;
    }

    // CSVデータを読み込む。
    if (0 === strpos(PHP_OS, 'WIN')) setlocale(LC_CTYPE, 'C');  // windows環境でCSVパースを正常動作させるためのおまじない。
    $reader = Reader::createFromPath($upfile["tmp_name"], 'r');
    $reader->includeEmptyRecords();
    $csvdatas = $reader->getRecords();
    $datas = array();
    foreach ($csvdatas as $csvrow) {
        $datas[] = $csvrow;
    }

    $rowNo = 0;
    $keyInfo = array();
    $isError = false;

    // データの内容を検証する。
    foreach ($datas as $row) {
        $rowNo++;
        $rowInfo = " : ".$rowNo." 列目";
        $rowCount = count($row);

        // ・カラム数
        if (count($row) < COLUMN_COUNT) {
            $messages[] = getCommonMessage("WC005").$rowInfo;
            $isError = true;
        }
        // ・必須
        if ($row[COL_QA_NO] == "") {
            $messages[] = getCommonMessage("WC001", "QA No.").$rowInfo;
            $isError = true;
        }
        // 空レコードなら次行にスキップ。
        if (isEmptyRecord($row, COL_QA_NO + 1)) continue;

        // ・禁止文字
        //   (除外：質問内容、回答内容)
        if (!validateProhibitedCharacters($row, COL_QUESTION, COL_ANSWER)) {
            $messages[] = getCommonMessage("WC013");
            $isError = true;
        }
        // ・重複(CSV内)
        if ($rowCount >= COL_QA_NO) {
            if (in_array($row[COL_QA_NO], $keyInfo)) {
                $messages[] = getCommonMessage("WC002", "QA No.").$rowInfo;
                $isError = true;
            }
            $keyInfo[] = $row[COL_QA_NO];
        }
        // 企業が自企業かどうか(受入機関の場合)
        if ($_SESSION["loginCompanyType"] == "1") {
            if ($rowCount >= COL_COMPANY_ID and $row[COL_COMPANY_ID] != "") {
                if ($row[COL_COMPANY_ID] != $_SESSION["loginCompanyId"]) {
                    $messages[] = getCommonMessage("WC020");
                    $isError = true;
                }
            }
        }
    }
    if ($isError) {
        return false;
    }

    // データを登録する。
    $isImportOk = csvDataImport($datas);
    if (!$isImportOk) {
        $messages[] = getCommonMessage("EC005");
        return false;
    }
    
    $messages[] = getCommonMessage("IC001");
    return true;
}
