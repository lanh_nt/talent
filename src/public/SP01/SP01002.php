<?php
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/SP01/SP01002.php';

const FUNC_ID  = "SP01";
const SCENE_ID = "SP01002";

const MODE_LOAD   = "load";
const MODE_SEARCH = "search";
const MODE_DETAIL = "detail";


session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
//　※労働者メニューではないので省略？
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。 ★★★
$condItemNames = array(
    "C_01",  //ID
    "C_02",  //氏名
    "C_05_1", "C_05_2",   //性別
    "C_06",   //企業名
    "C_07_1", "C_07_2",   //登録日
    "page_no", "is_search", "mode", "sp_id"
);
// POST内容から構成。
$items = getParamsArray($_POST, $condItemNames);

$items["sp_id"] = $_SESSION['sp_id'];


// モードに対応する処理を実行。
if ($mode == MODE_SEARCH) {
    // 検索一覧へ移動。
    goForwardPage("../SP01/SP01001", $items);
    exit();

}

// 検索処理を実行。
$rowdatas = search($items["sp_id"]);


// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("rowdata", $rowdatas);
$smarty->assign("purposes", getOptionItems("comm", "purpose"));
//$smarty->assign("companies", getCompanies());


// Smartyに変数をバインド。
$smarty->assign("labels", getLabels("SP01", "SP01002", $_SESSION['loginUserLang']));

// 画面を表示する。
$smarty->display('SP01/SP01002.tpl');

