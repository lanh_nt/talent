<?php
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../lib/PageData.php';
require_once '../../da/SP01/SP01001.php';

use League\Csv\Writer;
use League\Csv\Reader;

const FUNC_ID  = "SP01";
const SCENE_ID = "SP01001";

const MODE_LOAD   = "load";
const MODE_SEARCH = "search";
const MODE_DETAIL = "detail";
const MODE_IMPORT = "csv-import";
const MODE_EXPORT = "csv-export";
const MODE_FORMAT = "csv-format";

const CSV_FORMAT_NAME = "SP01001";
const CSV_OUTPUT_NAME = "支援計画";

// CSVデータのカラム数
const COLUMN_COUNT = 371;
const COL_WORKER_ID = 1 - 1;
const COL_RESPONSE_TIME_BASE = 259 - 1;
const COL_C_1_01_4 =   5 - 1;
const COL_C_1_02_4 =  13 - 1;
const COL_C_1_03_4 =  21 - 1;
const COL_C_1_04_4 =  29 - 1;
const COL_C_1_05_4 =  37 - 1;
const COL_C_1_06_4 =  45 - 1;
const COL_C_1_07_4 =  53 - 1;
const COL_C_1_08_4 =  61 - 1;
const COL_C_1_09_4 =  69 - 1;
const COL_C_1_10_4 =  77 - 1;
const COL_C_1_11_5 =  86 - 1;
const COL_C_2_1_4  =  96 - 1;
const COL_C_2_2_4  = 104 - 1;
const COL_C_2_3_5  = 114 - 1;
const COL_C_3_1_4  = 119 - 1;
const COL_C_3_2_4  = 123 - 1;
const COL_C_3_3_4  = 127 - 1;
const COL_C_3_4_5  = 132 - 1;
const COL_C_3_5_08 = 141 - 1;
const COL_C_3_6_4  = 149 - 1;
const COL_C_3_7_4  = 157 - 1;
const COL_C_3_8_5  = 166 - 1;
const COL_C_4_1_4  = 171 - 1;
const COL_C_4_2_4  = 179 - 1;
const COL_C_4_3_4  = 188 - 1;
const COL_C_4_4_4  = 196 - 1;
const COL_C_4_5_4  = 204 - 1;
const COL_C_4_6_4  = 212 - 1;
const COL_C_4_7_5  = 221 - 1;
const COL_C_5_1_4  = 231 - 1;
const COL_C_5_2_4  = 235 - 1;
const COL_C_5_3_4  = 239 - 1;
const COL_C_5_4_5  = 244 - 1;
const COL_C_6_1_4  = 249 - 1;
const COL_C_6_2_4  = 253 - 1;
const COL_C_6_3_5  = 258 - 1;
const COL_C_7_1_4  = 293 - 1;
const COL_C_7_2_4  = 297 - 1;
const COL_C_7_3_5  = 302 - 1;
const COL_C_8_1_4  = 307 - 1;
const COL_C_8_2_4  = 311 - 1;
const COL_C_8_3_4  = 315 - 1;
const COL_C_8_4_4  = 319 - 1;
const COL_C_8_6_4  = 325 - 1;
const COL_C_8_7_4  = 333 - 1;
const COL_C_8_8_5  = 338 - 1;
const COL_C_9_1_4  = 343 - 1;
const COL_C_9_2_4  = 351 - 1;
const COL_C_9_3_4  = 359 - 1;
const COL_C_9_4_4  = 363 - 1;
const COL_C_9_5_5  = 368 - 1;

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
//　※労働者メニューではないので省略？
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。 ★★★
$condItemNames = array(
    "C_01",  //ID
    "C_02",  //氏名
    "C_05_1", "C_05_2",   //性別
    "C_06",   //企業名
    "C_07_1", "C_07_2",   //登録日
    "page_no", "is_search", "mode", "sp_id"
);

// POST内容から構成。
$items = getParamsArray($_POST, $condItemNames);
if ($mode == MODE_LOAD) {
    // 初期表示の場合、チェックボックスのデフォルトをONに設定。
    setDefaultValues($items);
}

// モードに対応する処理を実行。
if ($mode == MODE_DETAIL) {
    // ------------------------------------------
    // 詳細ボタン処理
    // ------------------------------------------
    // 指定された帳票ステータスレコードIDをセッションに設定。
    $_SESSION['sp_id'] = $items["sp_id"];

    // 帳票管理　詳細画面に遷移する。
    goForwardPage("../SP01/SP01002", $items);
    exit();

}

// ------------------------------------------
// 検索処理を実行する。
// ・検索ボタン押下時 or CSV登録ボタン押下時
// ------------------------------------------
$pageData = null;
if ($mode == MODE_SEARCH ) {
    if (checkInput($items)) {
        // 表示ページを設定。
        $pageNo = ($items["page_no"] != "") ? $items["page_no"] : 1;

        // 検索処理を実行。
        $pageData = search($items, $pageNo);

        // 検索実施済みを立てる。
        $items["is_search"] = "yes";
    }
} else {
    $items["is_search"] = "";
}

if ($mode == MODE_FORMAT) {
    // ------------------------------------------
    // フォーマット出力ボタン処理
    // ------------------------------------------
    // フォーマットファイルを出力する。
    formatDownload(CSV_FORMAT_NAME, CSV_OUTPUT_NAME);
    exit();

} elseif ($mode == MODE_EXPORT) {
    // ------------------------------------------
    // CSV出力ボタン処理
    // ------------------------------------------
    // 表示中の支援計画のCSVファイルを出力する。
    csvExport($items);
    exit();

} elseif ($mode == MODE_IMPORT) {
    // ------------------------------------------
    // CSV登録ボタン処理
    // ------------------------------------------
    $upfile = $_FILES["upfile"];
    csvImport($upfile);

}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';


// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("purposes", getOptionItems("comm", "purpose"));
$smarty->assign("companies", getCompanies());
$smarty->assign("pageData", $pageData);


// 画面を表示する。
$smarty->display('SP01/SP01001.tpl');


/**
 * デフォルト値を設定する。
 *
 * @param array[項目名 => 値] $items 画面項目一覧
 */
function setDefaultValues(&$items) {
    // チェックボックスのデフォルトをONに設定。
    $items["C_05_1"] = true;
    $items["C_05_2"] = true;
}


/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;
    global $labels;

    $isCheckOk = true;

    // 禁止文字のチェックを行う。
    if (!validateProhibitedCharacters($items)) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // ・登録日
    if ($items["C_07_1"] != "" and $items["C_07_2"] != "") {
        if ($items["C_07_1"] > $items["C_07_2"]) {
            // from>toはエラー。
            $messages[] = getCommonMessage("WC017", "登録日");
            $isCheckOk = false;
        }
    }

    // チェック結果を返す。
    return $isCheckOk;
}


/**
 *画面の検索条件にて、CSVファイルを出力する。
 *
 * @param array[項目名 => 値] $items 画面項目一覧
 */
function csvExport($items) {
    // DBから対象データを取得する。
    $result = getExportDatas($items);

    // CSVライターを生成。
    $writer = Writer::createFromString("");
    $writer->setOutputBOM(Writer::BOM_UTF8);  // Excelで開けるようにBOMを付与。
    $writer->setNewline("\r\n");

    // CSVデータを積み上げる。
    $writer->insertAll($result);

    // CSVデータをresponseとして出力する。
    date_default_timezone_set('Asia/Tokyo');
    $fileName = CSV_OUTPUT_NAME."_".date("Ymd_Hi").".csv";
    $writer->output($fileName);

    exit();
}


/**
 * アップロードされたCSVファイルから、支援計画データを更新する。
 *
 * @param ファイル $upfile アップロードされたファイル
 * 
 * @return void
 */
function csvImport($upfile) {
    global $messages;

    // ファイル内容をチェックする。
    // ・拡張子
    if (!preg_match("/\.csv$/u", $upfile["name"])) {
        $messages[] = getCommonMessage("WC003");
        return false;
    }
    // ・サイズ
    if ($upfile["size"] == 0) {
        $messages[] = getCommonMessage("WC004");
        return false;
    }

    // CSVデータを読み込む。
    if (0 === strpos(PHP_OS, 'WIN')) setlocale(LC_CTYPE, 'C');  // windows環境でCSVパースを正常動作させるためのおまじない。
    $reader = Reader::createFromPath($upfile["tmp_name"], 'r');
    $reader->includeEmptyRecords();
    $csvdatas = $reader->getRecords();
    $datas = array();
    foreach ($csvdatas as $csvrow) {
        $datas[] = $csvrow;
    }
 
    $rowNo = 0;
    $keyInfo = array();
    $isError = false;

    // データの内容を検証する。
    foreach ($datas as $row) {
        $rowNo++;
        $rowInfo = " : ".$rowNo." 列目";

        // ・カラム数
        if (count($row) < COLUMN_COUNT) {
            $messages[] = getCommonMessage("WC005").$rowInfo;
            $isError = true;
        }
        // ・必須
        if ($row[COL_WORKER_ID] == "") {
            $messages[] = getCommonMessage("WC001", "労働者ID").$rowInfo;
            $isError = true;
        }
        // 労働者IDの存在チェック
        $checkWorker = getWorkerId($row[COL_WORKER_ID]);
        if ($checkWorker == null) {
            $messages[] = getCommonMessage("WC011").$rowInfo;
            $isError = true;
        }
        // ・時刻
        $isError |= checkTimeValue($row, COL_RESPONSE_TIME_BASE +  0, COL_RESPONSE_TIME_BASE +  1, "月曜日", $rowInfo);
        $isError |= checkTimeValue($row, COL_RESPONSE_TIME_BASE +  2, COL_RESPONSE_TIME_BASE +  3, "火曜日", $rowInfo);
        $isError |= checkTimeValue($row, COL_RESPONSE_TIME_BASE +  4, COL_RESPONSE_TIME_BASE +  5, "水曜日", $rowInfo);
        $isError |= checkTimeValue($row, COL_RESPONSE_TIME_BASE +  6, COL_RESPONSE_TIME_BASE +  7, "木曜日", $rowInfo);
        $isError |= checkTimeValue($row, COL_RESPONSE_TIME_BASE +  8, COL_RESPONSE_TIME_BASE +  9, "金曜日", $rowInfo);
        $isError |= checkTimeValue($row, COL_RESPONSE_TIME_BASE + 10, COL_RESPONSE_TIME_BASE + 11, "土曜日", $rowInfo);
        $isError |= checkTimeValue($row, COL_RESPONSE_TIME_BASE + 12, COL_RESPONSE_TIME_BASE + 13, "日曜日", $rowInfo);
        $isError |= checkTimeValue($row, COL_RESPONSE_TIME_BASE + 14, COL_RESPONSE_TIME_BASE + 15, "祝日",   $rowInfo);
        // ・重複(CSV内)
        $array_search_flg = array_search($row[COL_WORKER_ID], $keyInfo);
        if ($array_search_flg !==false) {
            $messages[] = getCommonMessage("WC002", "労働者ID").$rowInfo;
            $isError = true;
        }
        $keyInfo[] = $row[COL_WORKER_ID];
        // ・企業が自企業かどうか(受入機関の場合)
        if ($_SESSION["loginCompanyType"] == "1") {
            $isError |= checkCompanyId($row, $rowInfo, COL_C_1_01_4 , "従事する業務の内容，報酬の額その他の労働条件に関する事項");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_1_02_4 , "本邦において行うことができる活動の内容");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_1_03_4 , "入国に当たっての手続に関する事項");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_1_04_4 , "保証金の徴収，契約の不履行についての違約金契約等の締結の禁止");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_1_05_4 , "入国の準備に関し外国の機関に支払った費用について，当該費用の額及び内訳を十分に理解して支払わなければならないこと");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_1_06_4 , "支援に要する費用を負担させないこととしていること");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_1_07_4 , "入国する際の送迎に関する支援の内容");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_1_08_4 , "住居の確保に関する支援の内容");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_1_09_4 , "相談・苦情の対応に関する内容");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_1_10_4 , "特定技能所属機関等の支援担当者氏名及び連絡先");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_1_11_5 , "事前ガイダンスの提供 自由項目");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_2_1_4  , "到着空港等での出迎え及び特定技能所属機関又は住居までの送迎");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_2_2_4  , "出国予定空港等までの送迎及び保安検査場入場までの出国手続の補助");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_2_3_5  , "出入国する際の送迎 自由項目");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_3_1_4  , "不動産仲介事業者や賃貸物件の情報を提供し，必要に応じて住宅確保に係る手続に同行し，住居探しの補助を行う。また，賃貸借契約の締結時に連帯保証人が必要な場合に，適当な連帯保証人がいないときは，支援対象者の連帯保証人となる又は利用可能な家賃債務保証業者を確保し自らが緊急連絡先となる");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_3_2_4  , "自ら賃借人となって賃貸借契約を締結した上で，１号特定技能外国人の合意の下，住居として提供する");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_3_3_4  , "所有する社宅等を，１号特定技能外国人の合意の下，当該外国人に対して住居として提供する");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_3_4_5  , "適切な住居の確保に係る支援 自由項目");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_3_5_08 , "銀行その他の金融機関における預金口座又は貯金口座の開設の手続の補助");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_3_6_4  , "携帯電話の利用に関する契約の手続の補助");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_3_7_4  , "電気・水道・ガス等のライフラインに関する手続の補助");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_3_8_5  , "生活に必要な契約に係る支援 自由項目");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_4_1_4  , "本邦での生活一般に関する事項");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_4_2_4  , "法令の規定により外国人が履行しなければならない国又は地方公共団体の機関に対する届出その他の手続に関する事項及び必要に応じて同行し手続を補助すること");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_4_3_4  , "相談・苦情の連絡先，申出をすべき国又は地方公共団体の機関の連絡先");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_4_4_4  , "十分に理解することができる言語により医療を受けることができる医療機関に関する事項");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_4_5_4  , "防災・防犯に関する事項，急病その他の緊急時における対応に必要な事項");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_4_6_4  , "出入国又は労働に関する法令規定の違反を知ったときの対応方法その他当該外国人の法的保護に必要な事項");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_4_7_5  , "生活オリエンテーションの実施 自由項目");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_5_1_4  , "日本語教室や日本語教育機関に関する入学案内の情報を提供し，必要に応じて同行して入学の手続の補助を行う");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_5_2_4  , "自主学習のための日本語学習教材やオンラインの日本語講座に関する情報の提供し，必要に応じて日本語学習教材の入手やオンラインの日本語講座の利用契約手続の補助を行う");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_5_3_4  , "１号特定技能外国人との合意の下，日本語教師と契約して１号特定技能外国人に日本語の講習の機会を提供する");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_5_4_5  , "日本語学習の機会の提供 自由項目");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_6_1_4  , "相談又は苦情に対し，遅滞なく十分に理解できる言語により適切に対応し，必要な助言及び指導を行う");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_6_2_4  , "必要に応じ，相談内容に対応する関係行政機関を案内し，同行する等必要な手続の補助を行う");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_6_3_5  , "相談又は苦情への対応 自由項目");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_7_1_4  , "必要に応じ，地方公共団体やボランティア団体等が主催する地域住民との交流の場に関する情報の提供や地域の自治会等の案内を行い，各行事等への参加の手続の補助を行うほか，必要に応じて同行して各行事の注意事項や実施方法を説明するなどの補助を行う");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_7_2_4  , "日本の文化を理解するために必要な情報として，就労又は生活する地域の行事に関する案内を行うほか，必要に応じて同行し現地で説明するなどの補助を行う");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_7_3_5  , "日本人との交流促進に係る支援 自由項目");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_8_1_4  , "所属する業界団体や関連企業等を通じて次の受入れ先に関する情報を入手し提供する");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_8_2_4  , "公共職業安定所，その他の職業安定機関等を案内し，必要に応じて支援対象者に同行して次の受入れ先を探す補助を行う");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_8_3_4  , "１号特定技能外国人の希望条件，技能水準，日本語能力等を踏まえ，適切に職業相談・職業紹介が受けられるよう又は円滑に就職活動が行えるよう推薦状を作成する");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_8_4_4  , "職業紹介事業の許可又は届出を受けて職業紹介を行うことができる場合は，就職先の紹介あっせんを行う");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_8_6_4  , "離職時に必要な行政手続について情報を提供する");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_8_7_4  , "倒産等により，転職のための支援が適切に実施できなくなることが見込まれるときは，それに備え，当該機関に代わって支援を行う者を確保する");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_8_8_5  , "非自発的離職時の転職支援 自由項目");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_9_1_4  , "１号特定技能外国人の労働状況や生活状況を確認するため，当該外国人及びその監督をする立場にある者それぞれと定期的な面談を実施する");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_9_2_4  , "再確認のため，生活オリエンテーションにおいて提供した情報について，改めて提供する");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_9_3_4  , "労働基準法その他の労働に関する法令の規定に違反していることを知ったときは，労働基準監督署その他の関係行政機関へ通報する");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_9_4_4  , "資格外活動等の入管法違反又は旅券及び在留カードの取上げ等その他の問題の発生を知ったときは，その旨を地方出入国在留管理局に通報する");
            $isError |= checkCompanyId($row, $rowInfo, COL_C_9_5_5  , "定期的な面談の実施・行政機関への通報 自由項目");
        }
    }
    if ($isError) {
        return false;
    }

    // データを登録する。
    $isImportOk = csvDataImport($datas);
    if (!$isImportOk) {
        $messages[] = getCommonMessage("EC005");
        return false;
    }
    
    $messages[] = getCommonMessage("IC001");
    return true;
}

/**
 * 時刻のチェックを行う。
 *
 * @param array   $row         行データ
 * @param integer $colnumFrom  カラム位置(から)
 * @param integer $colnumTo    カラム位置(まで)
 * @param string  $labelString 曜日ラベル文言
 * @param string  $rowInfo     行ラベル情報
 * 
 * @return boolean エラー有無 (true:エラーあり、false:エラーなし)
 */
function checkTimeValue($row, $colnumFrom, $colnumTo, $labelString, $rowInfo) {
    global $messages;

    $isError = false;

    $valFrom = (count($row) >= $colnumFrom ) ? $row[$colnumFrom] : "";
    $valTo   = (count($row) >= $colnumTo   ) ? $row[$colnumTo]   : "";

    // from
    if ($valFrom != "") {
        $isMatch = preg_match("/^([0-9][0-9]):([0-9][0-9])$/", $valFrom, $m);
        if (!$isMatch) {
            $messages[] = getCommonMessage("WC018", $labelString."(から)").$rowInfo;
            $isError = true;
        } else {
            $hh = intval($m[1]);
            $mm = intval($m[2]);
            if ($hh < 0 or 23 < $hh or $mm < 0 or 59 < $mm) {
                $messages[] = getCommonMessage("WC018", $labelString."(から)").$rowInfo;
                $isError = true;
            }
        }
    }
    // to
    if ($valTo != "") {
        $isMatch = preg_match("/^([0-9][0-9]):([0-9][0-9])$/", $valTo, $m);
        if (!$isMatch) {
            $messages[] = getCommonMessage("WC018", $labelString."(まで)").$rowInfo;
            $isError = true;
        } else {
            $hh = intval($m[1]);
            $mm = intval($m[2]);
            if ($hh < 0 or 23 < $hh or $mm < 0 or 59 < $mm) {
                $messages[] = getCommonMessage("WC018", $labelString."(まで)").$rowInfo;
                $isError = true;
            }
        }
    }
    // from-to
    if (!$isError and $valFrom != "" and $valTo != "") {
        if ($valFrom > $valTo) {
            $messages[] = getCommonMessage("WC019", $labelString).$rowInfo;
            $isError = true;
        }
    }

    return $isError;
}

/**
 * 企業IDがログイン中企業と同一であるかチェックする。
 *
 * @param array   $row      行データ
 * @param string  $rowInfo  行ラベル情報
 * @param integer $colNum   カラム位置
 * @param string  $colLabel 項目ラベル文言
 * 
 * @return boolean エラー有無 (true:エラーあり、false:エラーなし)
 */
function checkCompanyId($row, $rowInfo, $colNum, $colLabel) {
    global $messages;

    $isError = false;

    if (count($row) >= $colNum and $row[$colNum] != "") {
        if ($row[$colNum] != $_SESSION["loginCompanyId"]) {
            $messages[] = getCommonMessage("WC020")." [".$colLabel."]".$rowInfo;
            $isError = true;
        }
    }

    return $isError;
}
