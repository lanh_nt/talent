<?php
/**
 * お知らせ新規登録画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../lib/ses_mailSender.php';
require_once '../../da/IF01/IF01002.php';

const FUNC_ID  = "IF01";
const SCENE_ID = "IF01002";

const MODE_LOAD           = "insert-load";
const MODE_CHANGE_COMPANY = "change-company";
const MODE_REGIST         = "regist";
const MODE_BACK           = "back";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (!isset($_REQUEST["mode"])) {
    // モード未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$mode = $_REQUEST["mode"];

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// モードに対応する処理を実行。
if ($mode == MODE_LOAD) {
    //-----------------------------
    // 検索画面からの遷移時(初期表示時)処理。
    //-----------------------------
    // セッションに検索条件を保存する。
    $_SESSION["IF01_conds"] = $_POST;

    // トークンを更新する。
    updateToken();
}

// 画面表示内容を構成。
if ($mode == "insert-load") {
    // 初期表示時は、配信日のみを初期設定。
    $items = getParamsArray(array(
        "D_1" => date("Y/m/d")
    ), getItemNames());

    // 受入企業ログインの場合、自企業をデフォルト設定。
    if ($_SESSION["loginCompanyType"] == "1") {
        $items["D_3"] = $_SESSION["loginCompanyRowId"];
    }
} else {
    // それ以外の場合は、POST内容から構成。
    $items = getParamsArray($_POST, getItemNames());
}

// モードに対応する処理を実行。
if ($mode == MODE_REGIST) {
    $result = registAction($items);
    if ($result == true) {
        // 検索画面に遷移する。
        $items = array(
            "mode" => MODE_BACK,
            "message" => getCommonMessage("IC001")
        );
        goForwardPage("IF01001", $items);
    }
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
if ($_SESSION["loginCompanyType"] == "2") {
    $smarty->assign("companies", getCompanies());
}
$smarty->assign("workers", getWorkers($items["D_3"]));

// 画面を表示する。
$smarty->display('IF01/IF01002.tpl');


/**
 * フォーム項目名の一覧を返す。
 *
 * @return array[項目名] フォーム項目名の一覧
 */
function getItemNames() {
    return array(
        "D_1", "D_2", "D_3", 
        "D_4_1", "D_4_2", 
        "D_5_1", "D_5_2", 
        "D_6_1", "D_6_2"
    );
}


/**
 * regist処理
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean 実行成否
 */
function registAction($items) {
    global $messages;

    // トークンをチェックする。
    checkToken();

    // 入力内容をチェックする。
    if (!checkInput($items)) {
        return false;
    }

    // DB登録を実行。
    $isRegistOk = insert($items);
    if (!$isRegistOk) {
        $messages[] = getCommonMessage("EC005");
        return false;
    }

    // トークンを更新する。
    updateToken();

    return true;
}

/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;
    global $labels;

    $isCheckOk = true;

    // 禁止文字のチェックを行う。
    // (除外：件名、本文の各母国語項目)
    if (!validateProhibitedCharacters($items, "D_5_2", "D_6_2")) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // ・配信言語
    if ($items["D_2"] == "") {
        // 未入力はエラー。
        $messages[] = getCommonMessage("WC001", "配信言語");
        $isCheckOk = false;
    }

    // ・企業
    if ($items["D_3"] == "") {
        // 未入力はエラー。
        $messages[] = getCommonMessage("WC001", "企業名");
        $isCheckOk = false;
    }

    // ・宛先
    if ($items["D_3"] != "") { // ※企業未選択時は、宛先は絶対に設定できないのでスキップ。
        if ($items["D_4_1"] == "") {
            // 未入力はエラー。
            $messages[] = getCommonMessage("WC001", "宛先");
            $isCheckOk = false;
        }
        if ($items["D_4_1"] == "2" and !is_array($items["D_4_2"])) {
            // 個別選択時、労働者を未入力はエラー。
            $messages[] = getCommonMessage("WC001", "労働者");
            $isCheckOk = false;
        }
    }

    // チェック結果を返す。
    return $isCheckOk;
}
