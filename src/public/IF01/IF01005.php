<?php
/**
 * お知らせ詳細表示画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/IF01/IF01005.php';

const FUNC_ID  = "IF01";
const SCENE_ID = "IF01005";

const MODE_LOAD = "detail-load";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (!isset($_REQUEST["mode"])) {
    // モード未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$mode = $_REQUEST["mode"];

if ($mode == MODE_LOAD) {
    // 検索条件を保存する。
    $_SESSION["IF01_conds"] = $_POST;
} else {
    // モード不正はエラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
if (!isset($_POST["info_id"]) or $_POST["info_id"] == "") {
    // お知らせID未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$infoId = $_POST["info_id"];

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

//---------------------------------
// 表示内容を取得する。
//---------------------------------
$items = search($infoId);

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('IF01/IF01005.tpl');
