<?php
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/FM01/FM01005.php';

const FUNC_ID  = "FM01";
const SCENE_ID = "FM01005";

const MODE_LOAD   = "load";
const MODE_SEARCH = "search";
const MODE_DETAIL = "detail";
const MODE_UPDATE = "update";
const MODE_MULTI_UPD = "multi-update";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。 ★★★
$condItemNames = array(
    "C_01", "C_02", 
    "C_05_1", "C_05_2", 
    "C_06", 
    "C_07_1", "C_07_2", 
    "C_08_1", "C_08_2", "C_08_3", "C_08_4", "C_08_5", "C_08_6", "C_08_7", "C_08_8", "C_08_9",  "C_08_10", 
    "C_09_1", "C_09_2", "C_09_3", "C_09_4", "C_09_5", 
    "page_no", "is_search", "mode", "report_ids"
);
// POST内容から構成。
$items = getParamsArray($_POST, $condItemNames);

$items["report_ids"] = $_SESSION['targetReportIds'];


// モードに対応する処理を実行。
if ($mode == MODE_DETAIL) {
    // 検索処理を実行。
    $rowdata = search($_REQUEST["report_ids"]);

    $arg_arr=[];
    //ユーザー種別フラグ	user_type
    $arg_arr["user_type"] = $rowdata["user_type"];
    //労働者または管理者ID	user_id
    $arg_arr["user_id"] = $rowdata["user_id"];
    // 帳票ID	report_id
    $arg_arr["report_id"] = $rowdata["report_id"];

    //履歴取得
    $rirekidatas = search_rireki($arg_arr);

    // セッションにトークンキー情報を保存。
    // (ファイルを開くときに使用する。)
    $tokenArray = array();
    foreach ($rirekidatas as $row) {
        $tokenArray[$row["pdf_token"]] = $row["pdf_id"];
    }
    $_SESSION["FM_token"] = $tokenArray;
}

// モードに対応する処理を実行。
if ($mode == MODE_SEARCH) {
    // 検索一覧へ移動。
    goForwardPage("../FM01/FM01001", $items);
    exit();
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("rowdata", $rowdata);
$smarty->assign("rirekidatas", $rirekidatas);
$smarty->assign("purposes", getOptionItems("comm", "purpose"));


// 画面を表示する。
$smarty->display('FM01/FM01005.tpl');
