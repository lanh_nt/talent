<?php
require_once '../../const.inc';
require_once '../../vendor/autoload.php';
require_once '../../lib/ReportOutput.php';
require_once '../../lib/S3_FileAccess.php';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/FM01/FM01002.php';

const FUNC_ID  = "FM01";
const SCENE_ID = "FM01002";

const MODE_LOAD   = "load";
const MODE_SEARCH = "search";
const MODE_DETAIL = "detail";
const MODE_UPDATE = "update";
const MODE_MULTI_UPD = "multi-update";
const MODE_REGIST = "regist";
const MODE_REPORT = "report";


session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
//　※労働者メニューではないので省略？
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。 ★★★
$condItemNames = array(
    "C_01", "C_02", 
    "C_05_1", "C_05_2", 
    "C_06", 
    "C_07_1", "C_07_2", 
    "C_08_1", "C_08_2", "C_08_3", "C_08_4", "C_08_5", "C_08_6", "C_08_7", "C_08_8", "C_08_9",  "C_08_10", 
    "C_09_1", "C_09_2", "C_09_3", "C_09_4", "C_09_5", 
    "C_09", 
    "page_no", "is_search", "mode", "premode", "prestatus","report_ids",
    "input_deadline","submission_deadline","report_id","r_user_id","r_user_type"
);
// POST内容から構成。
$items = getParamsArray($_POST, $condItemNames);

//ステータスの判定（デフォルト 未選択）
$items["C_09"] = "";

// モードに対応する処理を実行。
if ($mode == MODE_REPORT) {
    //-----------------------------
    // 帳票編集ボタン処理
    //-----------------------------
    //単帳票の更新時のみ動作
    $items["premode"] = $mode;
    
    $ForwardPg = "../FM01/FM01001";

    //各画面帳票編集へ遷移する
    switch($items["report_id"]){
        case "01":
            //在留資格認定証明書交付申請書
            $ForwardPg = "../WK02/WK02003";
            break;
        case "02":
            //在留期間更新許可申請書
            $ForwardPg = "../WK02/WK02013";
            break;
        case "03":
            //在留資格変更許可申請書
            $ForwardPg = "../WK02/WK02014";
            break;
        case "04":
            //支援計画書
            $ForwardPg = "../WK02/WK02004";
            break;
        case "05":
            //支援実施状況に係る届出
            $ForwardPg = "../WK02/WK02005";
            break;
        case "06":
            //事前ガイダンスの確認書
            $ForwardPg = "../WK02/WK02006";
            break;
        case "07":
            //生活オリエンテーションの確認書
            $ForwardPg = "../WK02/WK02007";
            break;
        case "08":
            //相談記録書
            $ForwardPg = "../WK02/WK02008";
            break;
        case "09":
            //定期面談報告書
            if($items["r_user_type"]=="1"){
                //定期面談報告書 管理者
                $items["mode"] = "load";
                $items["user_id"] = $items["r_user_id"];
            
                $ForwardPg = "../IN01/IN02003";
            }else{
                //定期面談報告書　労働者
                $ForwardPg = "../WK02/WK02009";
            }
            break;
        case "10":
            //履歴書
            $ForwardPg = "../WK02/WK02010";
            break;
    }

    //ターゲットユーザー（労働者）を設定
    $_SESSION['targetWorkerId'] = $items["r_user_id"];

    // 指定の帳票編集画面に遷移する。
    goForwardPage($ForwardPg , $items);
    exit();
}

// モードに対応する処理を実行。
if ($mode == MODE_UPDATE) {

    $items["premode"] = $mode;

    // トークンを更新する。
    updateToken();
    
    // 検索処理を実行。
    $rowdatas = search($_REQUEST["report_ids"],1);

    //ステータスの判定
    foreach ($rowdatas as &$row) {
        $items["C_09"] = $row["status"];
        $items["r_user_type"] = $row["r_user_type"];
    }

}

// モードに対応する処理を実行。
if ($mode == MODE_MULTI_UPD) {

    $items["premode"] = $mode;

    // トークンを更新する。
    updateToken();

    // 検索処理を実行。
    $rowdatas = search($_REQUEST["report_ids"],99);

}

// モードに対応する処理を実行。
if ($mode == MODE_REGIST) {
    // 更新処理を実行。
    $items["C_09"] = $_REQUEST["C_09"];

    $result = registAction($_REQUEST["report_ids"],$items);

    // 検索画面に遷移する。
    $items["mode"] = $items["premode"];
    $items["prestatus"] = $items["C_09"];
    $items["report_ids"] = $items["report_ids"];

    // 検索処理を実行。
    $rowdatas = search($_REQUEST["report_ids"],99);
}

// モードに対応する処理を実行。
if ($mode == MODE_SEARCH) {
    // 検索一覧へ移動。
    goForwardPage("../FM01/FM01001", $items);
    exit();

}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("rowdatas", $rowdatas);
$smarty->assign("purposes", getOptionItems("comm", "purpose"));
$smarty->assign("report_status", getOptionItems("comm", "report_status"));



// 画面を表示する。
$smarty->display('FM01/FM01002.tpl');



/**
 * regist処理
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean 実行成否
 */
function registAction($ids,$items) {
    // トークンをチェックする。
    checkToken();

    // 入力内容をチェックする。
    if (!checkInput($items)) {
        return false;
    }

    // ステータス更新を実行。
    update($ids,$items);

    //完了状態にした場合はPDFファイルを保存する
    //　DAで実施できそうだが機能分離という事で別関数化
    savePdf($ids,$items);
    

    // トークンを更新する。
    updateToken();

    global $messages;
    $messages[] = getCommonMessage("IC002");
    return true;
}

/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;
    global $labels;

    // ステータスが指定されていない場合はエラー。
    if ($items["C_09"] == "") {
        $messages[] = getCommonMessage("WC001", "ステータス");  // $labels["D_3"]
        return false;
    }

    // チェック正常を返す。
    return true;
}

/**
 * ステータス完了時のPDFファイルを生成する。
 *
 * @param array[項目名 => 値] $items 画面の入力内容
 * 
 * @return boolean 実行成否
 */
function savePdf($report_ids,$items) {
    if($items["prestatus"]=="5"){
        return;
    }

    // 提出済帳票の保存がOFFの場合はスキップ。
    if (PDF_SAVE_ON != true) return;

    //とりあえず、ファイル管理とは別個との事で所定の名称を付与して
    //ファイルを作成するのみとし、DBへの情報更新等は行わない。＝＞DBで情報管理し、履歴を表示する

    // 帳票ID一覧定義
    $PDF_TYPES = array(
        "01-2" => "M06-03-NINTEI",         // 在留資格認定証明書交付申請書
        "02-2" => "M30-02-KOUSHIN",        // 在留期間更新許可申請書
        "03-2" => "M30-01-HENKOU",         // 在留資格変更許可申請書
        "04-2" => "R01-17-SHIEN_KEIKAKU",  // 支援計画書
        "05-2" => "R03-07-SHIEN_JISSHI",   // 支援実施状況に係る届出
        "06-2" => "R01-07-GUIDANCE",       // 事前ガイダンスの確認書
        "07-2" => "R05-08-ORIENTATION",    // 生活オリエンテーションの確認書
        "08-2" => "R05-04-SOUDAN_KIROKU",  // 相談記録書
        "09-1" => "R05-06-MENDAN_ADMIN",   // 定期面談報告書(監督者)
        "09-2" => "R05-05-MENDAN_WORKER",  // 定期面談報告書(労働者)
        "10-2" => "R01-01-RIREKI"          // 履歴書
    );

    // 検索処理を実行。
    $rowdatas = search($_REQUEST["report_ids"],1);

    //ステータスの判定
    //提出済状態（5）になった帳票をPDFにして保存する
    foreach ($rowdatas as &$row) {
        // 提出済み未満の場合はスキップ。
        if ($row["status"] != "5") continue;

        // 帳票IDを取得。
        $reportId = $PDF_TYPES[$row["report_id"]."-".$row["user_type"]];

        // 帳票出力クラスを生成。
        $pdf = new ReportOutput($reportId);

        // 帳票を生成する。
        switch($row["report_id"]) {
            case "01":
                require_once '../../da/WK02/WK99001.php';
                $retAry = makePdfData01($row["r_user_id"]);
                $strm = $pdf->output($retAry, false);
                break;
            case "02":
                require_once '../../da/WK02/WK99002.php';
                $retAry = makePdfData02($row["r_user_id"]);
                $strm = $pdf->output($retAry, false);
                break;
            case "03":
                require_once '../../da/WK02/WK99003.php';
                $retAry = makePdfData03($row["r_user_id"]);
                $strm = $pdf->output($retAry, false);
                break;
            case "04":
                require_once '../../da/WK02/WK99004.php';
                $retAry = makePdfData04($row["r_user_id"]);
                $strm = $pdf->output($retAry, false);
                break;
            case "05":
                require_once '../../da/WK02/WK99005.php';
                $retAry = makePdfData05($row["r_user_id"]);
                $strm = $pdf->output($retAry, false);
                break;
            case "06":
                require_once '../../da/WK02/WK99006.php';
                $retAry = makePdfData06($row["r_user_id"]);
                $strm = $pdf->output($retAry, false);
                break;
            case "07":
                require_once '../../da/WK02/WK99007.php';
                $retAry = makePdfData07($row["r_user_id"]);
                $strm = $pdf->output($retAry, false);
                break;
            case "08":
                require_once '../../da/WK02/WK99008.php';
                $pageDatas = getPageDatas08($row["r_user_id"]);
                $strm = $pdf->outputMultiPage($pageDatas, false);
                break;
            case "09":
                if ($row["user_type"] == "1") {
                    require_once '../../da/WK02/WK99009_ADM.php';
                    $retAry = makePdfData09a($row["r_user_id"]);
                } else {
                    require_once '../../da/WK02/WK99009.php';
                    $retAry = makePdfData09b($row["r_user_id"]);
                }
                $strm = $pdf->output($retAry, false);
                break;
            case "10":
                require_once '../../da/WK02/WK99010.php';
                $retAry = makePdfData10($row["r_user_id"]);
                $strm = $pdf->output($retAry, false);
                break;
        }

        // ファイルをストレージに保存する。
        $S3 = new S3_FileAccess();

        $dirname  = $row["r_user_id"];
        $filename = $reportId."_". date('YmdHis').".pdf";
        $S3->PDFFileUpload($strm, $dirname, $filename);

        $arg_arr=[];
        $arg_arr["file_id"]    = $dirname."/".$filename;    // 保存ファイル名
        $arg_arr["company_id"] = $row["r_cmp_id"];          // 企業ID
        $arg_arr["user_type"]  = $row["user_type"];         // ユーザー種別フラグ
        $arg_arr["user_id"]    = $row["user_id"];           // 労働者または管理者ID
        $arg_arr["report_id"]  = $row["report_id"];         // 帳票ID

        // DBに登録
        pdfInsert($arg_arr);
    }

    return true;
}
