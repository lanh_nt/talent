<?php
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../lib/PageData.php';
require_once '../../da/FM01/FM01001.php';

const FUNC_ID  = "FM01";
const SCENE_ID = "FM01001";

const MODE_LOAD   = "load";
const MODE_SEARCH = "search";
const MODE_DETAIL = "detail";
const MODE_UPDATE = "update";
const MODE_MULTI_UPD = "multi-update";


session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
//　※労働者メニューではないので省略？
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。 ★★★
$condItemNames = array(
    "C_01", "C_02", 
    "C_05_1", "C_05_2", 
    "C_06", 
    "C_07_1", "C_07_2", 
    "C_08_1", "C_08_2", "C_08_3", "C_08_4", "C_08_5", "C_08_6", "C_08_7", "C_08_8", "C_08_9",  "C_08_10", 
    "C_09_1", "C_09_2", "C_09_3", "C_09_4", "C_09_5", 
    "page_no", "is_search", "mode", "report_ids"
);

// POST内容から構成。
$items = getParamsArray($_POST, $condItemNames);
if ($mode == MODE_LOAD) {
    // 初期表示の場合、チェックボックスのデフォルトをONに設定。
    setDefaultValues($items);
}

// モードに対応する処理を実行。
if ($mode == MODE_DETAIL) {
    // ------------------------------------------
    // 詳細ボタン処理
    // ------------------------------------------
    // 指定された帳票ステータスレコードIDをセッションに設定。
    $_SESSION['targetReportIds'] = $items["report_ids"];

    // 帳票管理　詳細画面に遷移する。
    goForwardPage("../FM01/FM01005", $items);
    exit();

}

// モードに対応する処理を実行。
if ($mode == MODE_UPDATE) {
    // ------------------------------------------
    // 更新ボタン処理
    // ------------------------------------------
    // 指定された帳票ステータスレコードIDをセッションに設定。
    $_SESSION['targetReportIds'] = $items["report_ids"];

    // 帳票管理　更新画面に遷移する。
    goForwardPage("../FM01/FM01002", $items);
    exit();

}

// モードに対応する処理を実行。
if ($mode == MODE_MULTI_UPD) {
    // ------------------------------------------
    // 一括更新ボタン処理
    // ------------------------------------------
    // 指定された帳票ステータスレコードIDをセッションに設定。
    $_SESSION['targetReportIds'] = $items["report_ids"];

    // 帳票管理　一括更新画面に遷移する。
    goForwardPage("../FM01/FM01002", $items);
    exit();

}

// ------------------------------------------
// 検索処理を実行する。
// ・検索ボタン押下時 or CSV登録ボタン押下時
// ------------------------------------------
$pageData = null;
if ($mode == MODE_SEARCH ) {
    if (checkInput($items)) {
        // 表示ページを設定。
        $pageNo = ($items["page_no"] != "") ? $items["page_no"] : 1;

        // 検索処理を実行。
        $pageData = search($items, $pageNo);

        // 検索実施済みを立てる。
        $items["is_search"] = "yes";
    }
} else {
    $items["is_search"] = "";
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("purposes", getOptionItems("comm", "purpose"));
$smarty->assign("companies", getCompanies());
$smarty->assign("pageData", $pageData);

// 画面を表示する。
$smarty->display('FM01/FM01001.tpl');


/**
 * デフォルト値を設定する。
 *
 * @param array[項目名 => 値] $items 画面項目一覧
 */
function setDefaultValues(&$items) {
    // チェックボックスのデフォルトをONに設定。
    $items["C_03_1"] = true;
    $items["C_03_2"] = true;
    $items["C_03_3"] = true;
    $items["C_03_4"] = true;
    $items["C_03_5"] = true;
    $items["C_03_6"] = true;

    $items["C_04_1"] = true;
    $items["C_04_2"] = true;
    $items["C_04_3"] = true;
    $items["C_04_4"] = true;
    $items["C_04_5"] = true;
    $items["C_04_6"] = true;

    $items["C_05_1"] = true;
    $items["C_05_2"] = true;

    $items["C_08_1"] = true;
    $items["C_08_2"] = true;
    $items["C_08_3"] = true;
    $items["C_08_4"] = true;
    $items["C_08_5"] = true;
    $items["C_08_6"] = true;
    $items["C_08_7"] = true;
    $items["C_08_8"] = true;
    $items["C_08_9"] = true;
    $items["C_08_10"] = true;

    $items["C_09_1"] = true;
    $items["C_09_2"] = true;
    $items["C_09_3"] = true;
    $items["C_09_4"] = true;
    $items["C_09_5"] = true;
}


/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;
    global $labels;

    $isCheckOk = true;

    // 禁止文字のチェックを行う。
    if (!validateProhibitedCharacters($items)) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // ・登録日
    if ($items["C_07_1"] != "" and $items["C_07_2"] != "") {
        if ($items["C_07_1"] > $items["C_07_2"]) {
            // from>toはエラー。
            $messages[] = getCommonMessage("WC017", "登録日");
            $isCheckOk = false;
        }
    }

    // チェック結果を返す。
    return $isCheckOk;
}
