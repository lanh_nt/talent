<?php
/**
 * 帳票管理：履歴PDF
 * 
 */
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../lib/S3_FileAccess.php';
require_once '../../da/FM01/FM99001.php';

const FUNC_ID  = "FM01";
const SCENE_ID = "FM99001";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// トークンキーからファイルIDを取得する。
$fileRowId = getFileRowId($_GET["r"]);

// ファイル情報を取得する。
$items = search_rireki($fileRowId);

$arr = explode("/",$items["file_id"]);
$u_mail = $arr[0];
$file_id = $arr[1];

// ファイルを出力する。
$s3 = new S3_FileAccess();
$s3->outputFile(
    2,  // 帳票管理
    $u_mail,
    $file_id,
    $file_id
);


/**
 * トークンキーからファイルIDを取得する。
 *
 * @param string $tokenKey トークンキー
 * 
 * @return string ファイルID
 */
function getFileRowId($tokenKey) {
    // キーの存在をチェックする。
    if (!isset($_SESSION["FM_token"][$tokenKey])) {
        // キーが存在しない場合、アクセスエラーとする。
        $errMessage = getCommonMessage("EC002");
        goErrorPage($errMessage);
    }

    // キーマップからファイルIDを取得して返す。
    return $_SESSION["FM_token"][$tokenKey];
}
