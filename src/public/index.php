<?php
/**
 * 労働者ログイン画面
 * 
 */

require_once '../vendor/autoload.php';
require_once '../const.inc';
require_once '../lib/common.inc';
require_once '../lib/JinzaiDb.php';
require_once '../da/LG01/LG02001.php';

const FUNC_ID  = "LG02";
const SCENE_ID = "LG02001";

const MODE_LOAD  = "load";
const MODE_ENTER = "enter";
const MODE_BACK  = "back";

session_start();

checkMaintenanceNologin();

// モード指定がない場合(=初期表示時)、セッション情報をリセットする。
if (!isset($_REQUEST["mode"])) {
    session_destroy();
    session_start();
}

// 画面の動作モードを設定。
$mode = (isset($_REQUEST["mode"])) ? $_REQUEST["mode"] : "";

// ラベル情報を取得する。
$lang = getLangValue();
$labels = getLabels(FUNC_ID, SCENE_ID, $lang);

// メッセージ情報を生成。
$messages = array();

// POST内容から画面表示内容を構成。
$itemNames = array(
    "loginId"
);
$items = getParamsArray($_POST, $itemNames);

// モードに対応する処理を実行。
if ($mode == MODE_ENTER) {
    enterAction($items);
} elseif ($mode == MODE_BACK) {
    if (isset($_POST["message"])) {
        $messages[] = $_POST["message"];
    }
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../templates/';
$smarty->compile_dir  = '../templates_c/';
$smarty->config_dir   = '../configs/';
$smarty->cache_dir    = '../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("helpItems", getHelpInfo($lang));

// 画面を表示する。
$smarty->display('index.tpl');


/**
 * enter処理
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean 実行成否
 */
function enterAction($items) {
    // 入力内容をチェックする。
    if (!checkInput($items)) {
        return false;
    }

    // パスワード入力画面に遷移する。
    $items = array(
        "mode" => "next",
        "loginId" => $items["loginId"]
    );
    goForwardPage("LG01/LG02002", $items, true);
    exit();
}

/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;
    global $labels;

    // ログインID
    if ($items["loginId"] == "") {
        $messages[] = getCommonMessage("WC001", $labels["L-01"]);
        return false;
    }
    if (!validateMailAddress($items["loginId"])) {
        $messages[] = getCommonMessage("WC014");
        return false;
    }

    // チェック正常を返す。
    return true;
}
