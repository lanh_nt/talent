<?php
/**
 * 企業管理 詳細画面
 * 
 */
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/CM01/CM01002.php';

const FUNC_ID  = "CM01";
const SCENE_ID = "CM01002";

const MODE_LOAD   = "load";
const MODE_SEARCH = "search";
const MODE_DETAIL = "detail";
const MODE_UPDATE = "update";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
//　※労働者メニューではないので省略？
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。 ★★★
$condItemNames = array(
    "C_01",     //ID
    "C_02",     //法人番号
    "C_03",    //企業名
    "C_04_1","C_04_2","C_04_3",  //種別
    "C_05_1", "C_05_2",   //登録日
    "page_no", "is_search", "mode", "cm_id"
);

// POST内容から構成。
$items = getParamsArray($_POST, $condItemNames);

$items["cm_id"] = $_SESSION['cm_id'];


// モードに対応する処理を実行。
if ($mode == MODE_SEARCH) {
    // 検索一覧へ移動。
    goForwardPage("../CM01/CM01001", $items);
    exit();

}
if ($mode == MODE_UPDATE) {
    // ------------------------------------------
    // 編集ボタン処理
    // ------------------------------------------
    // 編集画面に遷移する。
    goForwardPage("../CM01/CM01003", $items);
    exit();

}
// 検索処理を実行。
$rowdatas = search($items["cm_id"]);


// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("rowdata", $rowdatas);
$smarty->assign("purposes", getOptionItems("comm", "purpose"));


// Smartyに変数をバインド。
$smarty->assign("labels", getLabels("CM01", "CM01002", $_SESSION['loginUserLang']));

// 画面を表示する。
$smarty->display('CM01/CM01002.tpl');

