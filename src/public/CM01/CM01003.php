<?php
/**
 * 企業管理 編集画面
 * 
 */
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/CM01/CM01003.php';

const FUNC_ID  = "CM01";
const SCENE_ID = "CM01003";

const MODE_LOAD   = "load";
const MODE_REGIST = "regist";
const MODE_SEARCH = "search";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// ラベル情報を取得する。
//　※労働者メニューではないので省略？
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。 ★★★
$condItemNames = array(
    "C_01",     //ID
    "C_02",     //法人番号
    "C_03",    //企業名
    "C_04_1","C_04_2","C_04_3",  //種別
    "C_05_1", "C_05_2",   //登録日
    "page_no", "is_search", "mode", "cm_id"
);

$condItemNames2 = array(
    "company_id",
    "company_name",
    "company_kana",
    "classification",
    "corporation_no",
    "postal_code",
    "address",
    "tel",
    "representative_name",
    "representative_kana",
    "registration_no",
    "registration_date_year",
    "registration_date_month",
    "registration_date_day",
    "supervisor_name1",
    "supervisor_department1",
    "supervisor_position1",
    "supervisor_name2",
    "supervisor_department2",
    "supervisor_position2",
    "supervisor_name3",
    "supervisor_department3",
    "supervisor_position3",
    "supervisor_name4",
    "supervisor_department4",
    "supervisor_position4",
    "supervisor_name5",
    "supervisor_department5",
    "supervisor_position5",
    "support_scheduled_date_year",
    "support_scheduled_date_month",
    "support_scheduled_date_day",
    "support_company_name",
    "support_manager_name",
    "support_manager_kana",
    "support_manager_department",
    "support_handler_name",
    "support_handler_kana",
    "support_handler_department",
    "support_postal_code",
    "support_address",
    "support_tel",
    "available_language",
    "worker_mumber",
    "support_stuff_number",        
    "industry_type",
    "industry_type_etc",    
    "industry_type_etc01",   "industry_type_etc02",   "industry_type_etc03", 
    "industry_type_etc04",   "industry_type_etc05",   "industry_type_etc06", 
    "industry_type_etc07",   "industry_type_etc08",   "industry_type_etc09", 
    "industry_type_etc10",  "industry_type_etc11",  "industry_type_etc12", 
    "industry_type_etc13",  "industry_type_etc14",  "industry_type_etc15", 
    "industry_type_etc16",  "industry_type_etc17",  "industry_type_etc18", 
    "industry_type_etc19",  "industry_type_etc20",  "industry_type_etc21", 
    "industry_type_etc22",  "industry_type_etc23",  "industry_type_etc24", 
    "industry_type_etc25",  "industry_type_etc26",  "industry_type_etc27", 
    "industry_type_etc28",  "industry_type_etc29",  "industry_type_etc30", 
    "industry_type_etc31",  "industry_type_etc32",  "industry_type_etc33", 
    "industry_type_etc34",  "industry_type_etc35",  "industry_type_etc36", 
    "industry_type_etc37",  "industry_type_etc38",  "industry_type_etc39", 
    "industry_type_etc40",  "industry_type_etc41",  "industry_type_etc42", 
    "industry_type_etc43",  "industry_type_etc44",  "industry_type_etc45", 
    "industry_type_etc46",  "industry_type_etc47",     
    "manufacturing_other",
    "wholesale_other",    
    "retail_other",       
    "service_other",      
    "industry_other",     
    "capital",        
    "annual_sales_amount",
    "company_stuff_count",  
    "council_id",  
    "council_apply_status",
    "mode", "cm_id"

);

// POST内容から構成。
$items = getParamsArray($_POST, $condItemNames);
$rowdatas = getParamsArray($_POST, $condItemNames2);
$items["cm_id"] = $_SESSION['cm_id'];

// モードに対応する処理を実行。
if ($mode == MODE_SEARCH) {
    // 検索一覧へ移動。
    goForwardPage("../CM01/CM01001", $items);
    exit();

}
if ($mode == MODE_REGIST) {
    //-----------------------------
    // 登録
    //-----------------------------
    
    registAction($rowdatas);

} else {
        //-----------------------------
    // 初期表示
    //-----------------------------
    // トークンを更新する。
    updateToken();

    // 検索処理を実行。
    $rowdatas = search($items["cm_id"]);
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels",    $labels);
$smarty->assign("messages",  $messages);
$smarty->assign("items",     $items);
$smarty->assign("rowdata",   $rowdatas);
$smarty->assign("years",     getYears());
$smarty->assign("months",    getMonths());
$smarty->assign("days",      getDays());
$smarty->assign("industrys", getOptionItems("comm", "industry"));

// Smartyに変数をバインド。
$smarty->assign("labels", getLabels("CM01", "CM01003", $_SESSION['loginUserLang']));

// 画面を表示する。
$smarty->display('CM01/CM01003.tpl');


/**
 * regist処理
 *
 * @param array[項目名] $items     画面の内容
 * 
 * @return boolean 実行成否
 */
function registAction($items) {
    global $messages;

    // トークンをチェックする。
    checkToken();

    // 入力内容をチェックする。
    if (!checkInput($items)) {
        return false;
    }
    // DB登録を実行。
    $isUpdateOk = update($items);

    // トークンを更新する。
    updateToken();

    if ($isUpdateOk) {
        $messages[] = getCommonMessage("IC001");
    } else {
        $messages[] = getCommonMessage("EC005");
    }
    return $isUpdateOk;
}
/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items   画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;

    $isCheckOk = true;

    // 禁止文字のチェックを行う。
    if (!validateProhibitedCharacters($items)) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // 未入力チェック---------------------------------------

    // 企業ID
    if ($items["company_id"] == "") {
        $messages[] = getCommonMessage("WC001", "企業ID");  
        $isCheckOk = false;
    }
    // 企業名
    if ($items["company_name"] == "") {
        $messages[] = getCommonMessage("WC001", "企業名");  
        $isCheckOk = false;
    }

    // 日付チェック--------------------------------------------
    // 登録年月日
    if($items["registration_date_year"].$items["registration_date_month"].$items["registration_date_day"] != "") {
        if(checkdate(intval($items["registration_date_month"]), $items["registration_date_day"], intval($items["registration_date_year"])) == false) {
            $messages[] = getCommonMessage("WC001", "登録年月日");  
            $isCheckOk = false;
        }
    }
    // 支援業務を開始する予定年月日
    if($items["support_scheduled_date_year"].$items["support_scheduled_date_month"].$items["support_scheduled_date_day"] != "") {
        if(checkdate(intval($items["support_scheduled_date_month"]), $items["support_scheduled_date_day"], intval($items["support_scheduled_date_year"])) == false) {
            $messages[] = getCommonMessage("WC001", "支援業務を開始する予定年月日");  
            $isCheckOk = false;
        }
    }


    // チェック結果を返す。
    return $isCheckOk;
}


