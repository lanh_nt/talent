<?php
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';

session_start();

$message = "";

// エラー情報を取得。
$err = $_SESSION['err'];

$message = $err;

// ログイン状態を判定する。
if (isset($_SESSION['loginUserId']) && isset($_SESSION['loginUserType']) && isset($_SESSION['loginUserLang'])) {
    $isLogin = $_SESSION['loginUserType'];
} else {
    $isLogin = 0;
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// POST内容から画面表示内容を構成。
$items = getParamsArray($_POST, array("loginId"));

// Smartyに変数をバインド。
$smarty->assign("labels", getLabels("TP01", "ER01001"));
$smarty->assign("message", $message);
$smarty->assign("isLogin", $isLogin);

// 画面を表示する。
$smarty->display('TP01/ER01001.tpl');
