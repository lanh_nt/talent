<?php
/**
 * TOP画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/TP01/TP01001.php';

const FUNC_ID  = "TP01";
const SCENE_ID = "TP01001";

const MODE_LOAD   = "load";
const MODE_DETAIL = "detail";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    $mode = MODE_LOAD;
}

// 詳細ボタン押下時は、各画面に遷移。
if ($mode == MODE_DETAIL) {
    detailAction();
}

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// 表示内容を取得する。
if ($_SESSION['loginUserType'] == "1") {
    if ($_SESSION['loginCompanyType'] == "2") {
        // 支援機関：全データが対象。
        $items = search();
    } else {
        // 受入機関：自社データのみ対象。
        $items = search($_SESSION['loginCompanyRowId']);
    }
} else {
    // 労働者：自分データのみ対象。
    $items = search($_SESSION['loginCompanyRowId'], $_SESSION['loginUserRowId']);
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("items", $items);

// 画面を表示する。
$smarty->display('TP01/TP01001.tpl');


/**
 * 詳細ボタン処理
 *
 * @return void
 */
function detailAction() {
    $forwardPages = array(
        "01" => "../WK02/WK02003",
        "02" => "../WK02/WK02013",
        "03" => "../WK02/WK02014",
        "04" => "../WK02/WK02004",
        "05" => "../WK02/WK02005",
        "06" => "../WK02/WK02006",
        "07" => "../WK02/WK02007",
        "08" => "../WK02/WK02008",
        "09" => "../WK02/WK02009",
        "10" => "../WK02/WK02010"
    );

    // 飛び先が存在しない場合は処理しない。
    if (!isset($forwardPages[$_POST["report_id"]])) {
        return;
    }

    // 指定された労働者IDをセッションに設定。
    $_SESSION['targetWorkerId'] = $_POST["worker_id"];

    // 労働者一覧の検索条件が存在する場合はクリア。
    if (isset($_SESSION["WK01_conds"])) {
        unset($_SESSION["WK01_conds"]);
    }
    
    // 労働者詳細の各画面に遷移する。
    $forwardPage = $forwardPages[$_POST["report_id"]];
    goForwardPage($forwardPage, array());
    exit();
}
