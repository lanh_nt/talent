<?php
require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';

session_start();

$title = "";
$message = "";

//システムルートのmaintenanceフォルダにMTファイルが存在するかチェック
$filename = '../../maintenance/MT';
if (file_exists($filename)) {
    //存在したらMTファイルの中身を読む
    //　１行目タイトル
    //　２行目メッセージ
    $msg_ary = file($filename);
    if(isset($msg_ary[0])){
        $title = $msg_ary[0];
    }else{
        $title = "MAINTENANCE"; 
    }
    if(isset($msg_ary[1])){
        $message = $msg_ary[1];
    }else{
        $message = "Under maintenance now.";
    }

}else{
    //ここでメンテ解除され（ファイルが無い）ても戻せないので固定メッセージを出す
    $title = "MAINTENANCE";
    $message = "Under maintenance now.";
}

// ログイン状態を判定する。
if (isset($_SESSION['loginUserId']) && isset($_SESSION['loginUserType']) && isset($_SESSION['loginUserLang'])) {
    $isLogin = $_SESSION['loginUserType'];
} else {
    $isLogin = 0;
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// POST内容から画面表示内容を構成。
$items = getParamsArray($_POST, array("loginId"));

// Smartyに変数をバインド。
$smarty->assign("title", $title);
$smarty->assign("message", $message);
$smarty->assign("isLogin", 0);

// 画面を表示する。
$smarty->display('TP01/MAINTENANCE.tpl');
