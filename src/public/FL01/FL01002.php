<?php
/**
 * ファイル管理：詳細画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../da/FL01/FL01002.php';

const FUNC_ID  = "FL01";
const SCENE_ID = "FL01002";

const MODE_LOAD        = "detail-load";
const MODE_LOAD_WORKER = "detail-load-worker";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (!isset($_REQUEST["mode"])) {
    // モード未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$mode = $_REQUEST["mode"];

if ($mode == MODE_LOAD) {
    // 検索条件を保存する。
    $_SESSION["FL01_conds"] = $_POST;
} else if ($mode == MODE_LOAD_WORKER) {
    // 戻り先を「労働者詳細」とするためのフラグを設定する。
    $backToWorkerInfo = true;
} else {
    // モード不正はエラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
if (!isset($_POST["file_rowid"]) or $_POST["file_rowid"] == "") {
    // ファイルID未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$fileRowId = $_POST["file_rowid"];

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

//---------------------------------
// 表示内容を取得する。
//---------------------------------
$items = search($fileRowId);

// トークンを更新する。
updateToken();

// セッションにファイルの行IDを保存。
// (ファイルを開くときに使用する。)
$_SESSION["FL_rowid"] = $items["file_rowid"];

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("items", $items);
if (isset($backToWorkerInfo)) {
    $smarty->assign("backToWorkerInfo", $backToWorkerInfo);
}

// 画面を表示する。
$smarty->display('FL01/FL01002.tpl');
