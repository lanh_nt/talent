<?php
/**
 * ファイル管理：ファイルダウンロード機能
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../lib/S3_FileAccess.php';
require_once '../../da/FL01/FL01002P.php';

const FUNC_ID  = "FL01";
const SCENE_ID = "FL01002P";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// トークンをチェックする。
checkToken(isset($_GET["t"]) ? $_GET["t"] : null, "EC002");

// ファイル情報を取得する。
$fileRowId = $_SESSION["FL_rowid"];
$items = search($fileRowId);

// ファイルを出力する。
$s3 = new S3_FileAccess();
$s3->outputFile(
    1,  // ファイル管理
    $items["file_user_id"],
    $items["file_id"],
    $items["file_name_org"]
);
