<?php
/**
 * ファイル管理：登録画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../lib/S3_FileAccess.php';
require_once '../../da/FL01/FL01003.php';

const FUNC_ID  = "FL01";
const SCENE_ID = "FL01003";

const MODE_LOAD   = "insert-load";
const MODE_REGIST = "regist";
const MODE_BACK   = "back";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (!isset($_REQUEST["mode"])) {
    // モード未設定はありえないので、エラー画面に飛ばす。
    $errMessage = getCommonMessage("EC002");
    goErrorPage($errMessage);
}
$mode = $_REQUEST["mode"];

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// モードに対応する処理を実行。
if ($mode == MODE_LOAD) {
    //-----------------------------
    // 検索画面からの遷移時(初期表示時)処理。
    //-----------------------------
    // セッションに検索条件を保存する。
    $_SESSION["FL01_conds"] = $_POST;

    // トークンを更新する。
    updateToken();
}

// 画面表示内容を構成。
if ($mode == MODE_LOAD) {
    // 初期表示時は、初期値を設定。
    $items = getParamsArray(array(), getItemNames());
    setDefaultValues($items);
} else {
    // それ以外の場合は、POST内容から構成。
    $items = getParamsArray($_POST, getItemNames());
}

// モードに対応する処理を実行。
if ($mode == MODE_REGIST) {
    $upfile = $_FILES["D_1"];
    $result = registAction($items, $upfile);
    if ($result == true) {
        // 検索画面に遷移する。
        $items = array(
            "mode" => MODE_BACK,
            "message" => getCommonMessage("IC001")
        );
        goForwardPage("FL01001", $items);
    }
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("fileTypes", getOptionItems("comm", "file_type"));
if ($_SESSION['loginUserType'] == "1") {
    if ($_SESSION['loginCompanyType'] == "1") {
        $smarty->assign("workers", getWorkers($_SESSION['loginCompanyRowId']));
    } else {
        $smarty->assign("workers", getWorkers());
    }
}

// 画面を表示する。
$smarty->display('FL01/FL01003.tpl');


/**
 * フォーム項目名の一覧を返す。
 *
 * @return array[項目名] フォーム項目名の一覧
 */
function getItemNames() {
    return array(
        "D_1", "D_2", "D_3", "D_4"
    );
}

/**
 * デフォルト値を設定する。
 *
 * @param array[項目名 => 値] $items 画面項目一覧
 */
function setDefaultValues(&$items) {
    // 労働者の場合、対象者に自分を設定。
    if ($_SESSION['loginUserType'] == "2") {
        $items["D_2"] = $_SESSION['loginUserRowId'];
    }
}


/**
 * regist処理
 *
 * @param array[項目名] $items 画面の内容
 * @param file $upfile アップロードファイル情報
 * 
 * @return boolean 実行成否
 */
function registAction($items, $upfile) {
    global $messages;

    // トークンをチェックする。
    checkToken();

    // 入力内容をチェックする。
    if (!checkInput($items, $upfile)) {
        return false;
    }

    // DB&ファイル登録を実行。
    $isRegistOk = insert($items, $upfile);
    if (!$isRegistOk) {
        $messages[] = getCommonMessage("EC005");
        return false;
    }

    // トークンを更新する。
    updateToken();

    return true;
}

/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items 画面の内容
 * @param file $upfile アップロードファイル情報
 * 
 * @return boolean チェック結果
 */
function checkInput($items, $upfile) {
    global $messages;
    global $labels;

    $isOk = true;

    // ・ファイル
    if ($upfile["name"] == "") {
        // 未設定はエラー。
        $messages[] = getCommonMessage("WC001", $labels["L-01-01"]);
        $isOk = false;
    } else if ($upfile["size"] == 0) {
        // 内容が空ならエラー。
        $messages[] = getCommonMessage("WC004");
        $isOk = false;
    } else {
        $t = getHeaderAttributes($upfile["name"], false);
        if ($t == null) {
            // 対応しない拡張子ならエラー。
            $messages[] = getCommonMessage("WC003");
            $isOk = false;
        }
    }

    // ・対象者
    if ($items["D_2"] == "") {
        // 未入力はエラー。
        $messages[] = getCommonMessage("WC001", $labels["L-01-02"]);
        $isOk = false;
    }

    // ・種別
    if ($items["D_4"] == "") {
        // 未入力はエラー。
        $messages[] = getCommonMessage("WC001", $labels["L-01-04"]);
        $isOk = false;
    }

    // チェック結果を返す。
    return $isOk;
}
