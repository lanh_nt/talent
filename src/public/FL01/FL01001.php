<?php
/**
 * ファイル管理：検索画面
 * 
 */

require_once '../../vendor/autoload.php';
require_once '../../const.inc';
require_once '../../lib/common.inc';
require_once '../../lib/JinzaiDb.php';
require_once '../../lib/PageData.php';
require_once '../../lib/S3_FileAccess.php';
require_once '../../da/FL01/FL01001.php';

const FUNC_ID  = "FL01";
const SCENE_ID = "FL01001";

const MODE_LOAD_ADMIN  = "load-admin";
const MODE_LOAD_WORKER = "load-worker";
const MODE_SEARCH      = "search";
const MODE_DELETE      = "delete";
const MODE_BACK        = "back";

session_start();

// ログイン状態であることをチェックする。
checkLogin();

// 画面の動作モードを設定。
if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
} else {
    // 初期表示時は、管理者/労働者のロードを設定。
    $mode = ($_SESSION['loginUserType'] == "1") ? MODE_LOAD_ADMIN : MODE_LOAD_WORKER;

    // トークンを更新する。
    updateToken();
}

// ラベル情報を取得する。
$labels = getLabels(FUNC_ID, SCENE_ID);

// メッセージ情報を生成。
$messages = array();

// 画面表示内容を構成。
$condItemNames = array(
    "C_company", "C_worker",
    "C_01",
    "C_02",
    "C_03_1", "C_03_2", "C_03_3",
    "C_04_1", "C_04_2",
    "page_no", "is_search", "file_rowid"
);
if ($mode == MODE_BACK) {
    // 別画面から戻ってきた場合は、sessionから条件を復帰。
    $items = getParamsArray($_SESSION["FL01_conds"], $condItemNames);

    // メッセージが設定されている場合は、その内容を設定。
    if (isset($_POST["message"])) {
        $messages[] = $_POST["message"];
    }
} else {
    // それ以外の場合は、POST内容から構成。
    $items = getParamsArray($_POST, $condItemNames);

    if ($mode == MODE_LOAD_ADMIN or $mode == MODE_LOAD_WORKER) {
        // 初期表示の場合、デフォルトを設定。
        setDefaultValues($items);
    }
}

// 削除ボタン押下時、削除処理を実行する。
if ($mode == MODE_DELETE) {
    deleteAction($items);
}

// ------------------------------------------
// 検索処理を実行する。
// ・検索ボタン押下時 or 別画面から戻ってきた時
// ------------------------------------------
$pageData = null;
if ($mode == MODE_SEARCH or ($items["is_search"] == "yes" and ($mode == MODE_DELETE or $mode == MODE_BACK))) {
    if (checkInput($items)) {
        // 表示ページを設定。
        $pageNo = ($items["page_no"] != "") ? $items["page_no"] : 1;

        // ------------------------------------------
        // 検索処理
        // ------------------------------------------
        $pageData = search($items, $pageNo);

        // 検索実施済みを立てる。
        $items["is_search"] = "yes";
    }
} else {
    $items["is_search"] = "";
}

// Smartyテンプレートエンジンを生成。
$smarty = new Smarty();
$smarty->template_dir = '../../templates/';
$smarty->compile_dir  = '../../templates_c/';
$smarty->config_dir   = '../../configs/';
$smarty->cache_dir    = '../../cache/';

// Smartyに変数をバインド。
$smarty->assign("labels", $labels);
$smarty->assign("messages", $messages);
$smarty->assign("items", $items);
$smarty->assign("pageData", $pageData);

// 画面を表示する。
$smarty->display('FL01/FL01001.tpl');


/**
 * デフォルト値を設定する。
 *
 * @param array[項目名 => 値] $items 画面項目一覧
 */
function setDefaultValues(&$items) {
    // チェックボックスのデフォルトをONに設定。
    $items["C_03_1"] = true;
    $items["C_03_2"] = true;
    $items["C_03_3"] = true;

    // 支援企業の管理者でない場合、自企業IDを固定セット。
    if ($_SESSION["loginCompanyType"] != "2") {
        $items["C_company"] = $_SESSION["loginCompanyRowId"];
    }
    // 労働者ログイン場合、自労働者IDを固定セット。
    if ($_SESSION["loginUserType"] == "2") {
        $items["C_worker"] = $_SESSION["loginUserRowId"];
    }
}


/**
 * 入力内容をチェックする。
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean チェック結果
 */
function checkInput($items) {
    global $messages;
    global $labels;

    $isCheckOk = true;

    // 禁止文字のチェックを行う。
    if (!validateProhibitedCharacters($items)) {
        $messages[] = getCommonMessage("WC013");
        $isCheckOk = false;
    }

    // ・登録日
    if ($items["C_04_1"] != "" and $items["C_04_2"] != "") {
        if ($items["C_04_1"] > $items["C_04_2"]) {
            // from>toはエラー。
            $messages[] = getCommonMessage("WC017", $labels["L-01-03"]);
            $isCheckOk = false;
        }
    }

    // チェック結果を返す。
    return $isCheckOk;
}


/**
 * delete処理
 *
 * @param array[項目名] $items 画面の内容
 * 
 * @return boolean 実行成否
 */
function deleteAction($items) {
    global $messages;
    global $labels;

    // トークンをチェックする。
    checkToken();

    // 入力内容をチェックする。
    if (!isset($items["file_rowid"])) {
        $messages[] = getCommonMessage("WC001", $labels["L-01-01"]);
        return false;
    }

    // DB&ファイル削除を実行。
    $isDeleteOk = delete($items["file_rowid"]);
    if (!$isDeleteOk) {
        $messages[] = getCommonMessage("EC005");
        return false;
    }

    // トークンを更新する。
    updateToken();

    $messages[] = getCommonMessage("IC003");
    return true;
}
