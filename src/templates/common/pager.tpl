<nav class="pagination has-margin-top-80" role="navigation" aria-label="pagination">
    <a class="pager-button pagination-previous" id="pagelink_prev_{$pageData->prevPageNo}">{$labels["previous_page"]|escape}</a>
    <a class="pager-button pagination-next" id="pagelink_next_{$pageData->nextPageNo}">{$labels["next_page"]|escape}</a>
    <ul class="pagination-list">
{foreach from=$pageData->pagerItems item=$item}
        <li>
{if $item == $pageData->currentPageNo}
            <a class="pager-button pagination-link is-current" aria-current="page">{$item}</a>
{else if $item != ""}
            <a class="pager-button pagination-link" id="pagelink_{$item}">{$item}</a>
{else}
            <span class="pagination-ellipsis">&hellip;</span>
{/if}
        </li>
{/foreach}
    </ul>
</nav>
<input type="hidden" id="pagerSelectedNo">
