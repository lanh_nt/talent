<nav class="footer-navbar" aria-label="main navigation">
	<div class="navbar-end footer-navbar-end">
		<a href="../TERMS/terms" class="navbar-item footer-navbar-item">{$labels["term_link"]|escape}</a>
		<a href="../TERMS/privacy" class="navbar-item footer-navbar-item">{$labels["privacy_link"]|escape}</a>
	</div>
</nav>
