<nav class="navbar is-link" role="navigation" aria-label="main navigation">
	<div class="navbar-brand">
		<a class="navbar-item is-font-bold" href="../TP01/TP01001"><img src="../assets/img/logo.png"></a>

		<a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
			<span aria-hidden="true"></span>
			<span aria-hidden="true"></span>
			<span aria-hidden="true"></span>
		</a>
	</div>

	<div id="navbarBasicExample" class="navbar-menu">
		<div class="navbar-end">
			<a href="../TP01/TP01001" class="navbar-item">TOP</a>
			<a href="../IF01/IF01001" class="navbar-item">お知らせ</a>
			<a href="../WK01/WK01001" class="navbar-item">労働者管理</a>
			<a href="../IN01/IN01001" class="navbar-item">面談記録</a>
			<a href="../QA01/QA01001" class="navbar-item">Q&A管理</a>
			<a href="../CN01/CN01001" class="navbar-item">問い合わせ</a>
			<a href="../SP01/SP01001" class="navbar-item">支援計画</a>
			<a href="../FM01/FM01001" class="navbar-item">帳票管理</a>
			<a href="../CM01/CM01001" class="navbar-item">企業管理</a>
			<a href="../AD01/AD01001" class="navbar-item">管理者管理</a>
			<a href="../FL01/FL01001" class="navbar-item">ファイル管理</a>
			<a href="../LG01/LG03001" class="navbar-item">ログアウト</a>
		</div>
	</div>
</nav>

<div class="is-font-bold has-margin-left-20 has-margin-top-10">
	<p>ユーザー名:<span class="has-margin-left-10">{$smarty.session.loginUserName}</span></p>
	<p>ログインID:<span class="has-margin-left-10">{$smarty.session.loginUserId}</span></p>
</div>

<script>
	$(document).ready(function() {
		// Check for click events on the navbar burger icon
		$(".navbar-burger").click(function() {
			// Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
			$(".navbar-burger").toggleClass("is-active");
			$(".navbar-menu").toggleClass("is-active");
		});
	});
</script>
