<nav class="navbar is-link" role="navigation" aria-label="main navigation">
	<div class="navbar-brand">
		<a class="navbar-item is-font-bold" href="../TP01/TP01001"><img src="../assets/img/logo.png"></a>

		<a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
			<span aria-hidden="true"></span>
			<span aria-hidden="true"></span>
			<span aria-hidden="true"></span>
		</a>
	</div>

	<div id="navbarBasicExample" class="navbar-menu">
		<div class="navbar-end">
			<a href="../TP01/TP01001" class="navbar-item">TOP</a>
			<a href="../IF01/IF01001" class="navbar-item">{$labels["menu-01"]|escape}</a>
			<a href="../WK02/WK02001" class="navbar-item">{$labels["menu-02"]|escape}</a>
			<a href="../QA01/QA02001" class="navbar-item">{$labels["menu-05"]|escape}</a>
			<a href="../CN01/CN01001" class="navbar-item">{$labels["menu-03"]|escape}</a>
			<a href="../FL01/FL01001" class="navbar-item">{$labels["menu-04"]|escape}</a>
			<a href="../LG01/LG03001" class="navbar-item">{$labels["menu-06"]|escape}</a>
		</div>
	</div>
</nav>

<div class="is-font-bold has-margin-left-20 has-margin-top-10">
	<p>{$labels["user_name"]|escape}:<span class="has-margin-left-10">{$smarty.session.loginUserName}</span></p>
	<p>{$labels["login_id"]|escape}:<span class="has-margin-left-10">{$smarty.session.loginUserId}</span></p>
</div>

<script>
	$(document).ready(function() {

		// Check for click events on the navbar burger icon
		$(".navbar-burger").click(function() {

		// Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
		$(".navbar-burger").toggleClass("is-active");
		$(".navbar-menu").toggleClass("is-active");

		});
	});
</script>
