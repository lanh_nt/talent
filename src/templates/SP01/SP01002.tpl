<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/SP01/SP01002.js"></script>
</head>
<body id="SP01002">
	<form id="mainForm" enctype="multipart/form-data" method="POST" action="SP01002">
	<input type="hidden" name="mode" id="mode" />
	<input type="hidden" name="sp_id" id="sp_id" value="{$items["sp_id"]|escape}" />

	<input type="hidden" name="page_no" id="page_no" value="{$items["page_no"]|escape}"  />
	<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}"  />

	<input type="hidden" name="C_01" id="C_01" value="{$items["C_01"]|escape}"  />
	<input type="hidden" name="C_02" id="C_02" value="{$items["C_02"]|escape}"  />
	<input type="hidden" name="C_05_1" id="C_05_1" value="{$items["C_05_1"]|escape}"  />
	<input type="hidden" name="C_05_2" id="C_05_2" value="{$items["C_05_2"]|escape}"  />
	<input type="hidden" name="C_06" id="C_06" value="{$items["C_06"]|escape}"  />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">支援計画/詳細</h2>

		<h3 class="title is-5 has-margin-top-30">事前ガイダンスの提供</h3>
		<table class="table is-bordered">
			<tr>
				<th colspan="2">従事する業務の内容，報酬の額その他の労働条件に関する事項</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_1_01_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_1_01_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_1_01_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_1_01_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_1_01_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_1_01_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">本邦において行うことができる活動の内容</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_1_02_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_1_02_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_1_02_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_1_02_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_1_02_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_1_02_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">入国に当たっての手続に関する事項</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_1_03_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_1_03_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_1_03_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_1_03_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_1_03_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_1_03_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">保証金の徴収，契約の不履行についての違約金契約等の締結の禁止</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_1_04_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_1_04_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_1_04_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_1_04_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_1_04_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_1_04_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">入国の準備に関し外国の機関に支払った費用について，当該費用の額及び内訳を十分に理解して支払わなければならないこと</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_1_05_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_1_05_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_1_05_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_1_05_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_1_05_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_1_05_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">支援に要する費用を負担させないこととしていること</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_1_06_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_1_06_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_1_06_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_1_06_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_1_06_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_1_06_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">入国する際の送迎に関する支援の内容</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_1_07_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_1_07_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_1_07_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_1_07_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_1_07_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_1_07_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">住居の確保に関する支援の内容</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_1_08_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_1_08_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_1_08_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_1_08_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_1_08_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_1_08_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">相談・苦情の対応に関する内容</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_1_09_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_1_09_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_1_09_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_1_09_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_1_09_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_1_09_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">特定技能所属機関等の支援担当者氏名及び連絡先</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_1_10_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_1_10_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_1_10_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_1_10_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_1_10_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_1_10_6"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">{$rowdata["C_1_11_1"]|escape}&nbsp;</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_1_11_2"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_1_11_3"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_1_11_4"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_1_11_5"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_1_11_6"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_1_11_7"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">実施言語</th>
			</tr>
			<tr>
				<td colspan="2">{$rowdata["C_1_12"]|escape}&nbsp;</td>
			</tr>
			<tr>
				<th colspan="2">実施予定時間</th>
			</tr>
			<tr>
				<td colspan="2">{$rowdata["C_1_13"]|escape}&nbsp;</td>
			</tr>
		</table><!--  /.table -->

		<h3 class="title is-5 has-margin-top-30">出入国する際の送迎</h3>
		<table class="table is-bordered">
			<tr>
				<th colspan="2">到着空港等での出迎え及び特定技能所属機関又は住居までの送迎</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_2_1_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_2_1_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_2_1_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_2_1_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_2_1_5"]|escape}</td>
			</tr>
			<tr>
				<th>空港・方法</th>
				<td>{$rowdata["C_2_1_air_contents"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">出国予定空港等までの送迎及び保安検査場入場までの出国手続の補助</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_2_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_2_2_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_2_2_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_2_2_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_2_2_5"]|escape}</td>
			</tr>
			<tr>
				<th>空港・方法</th>
				<td>{$rowdata["C_2_2_air_contents"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">{$rowdata["C_2_3_1"]|escape}&nbsp;</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_2_3_2"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_2_3_3"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_2_3_4"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_2_3_5"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_2_3_6"]|escape}</td>
			</tr>
		</table>

		<h3 class="title is-5 has-margin-top-30">適切な住居の確保に係る支援・生活に必要な契約に係る支援</h3>
		<table class="table is-bordered">
			<tr>
				<th colspan="2">不動産仲介事業者や賃貸物件の情報を提供し，必要に応じて住宅確保に係る手続に同行し，住居探しの補助を行う。また，賃貸借契約の締結時に連帯保証人が必要な場合に，適当な連帯保証人がいないときは，支援対象者の連帯保証人となる又は利用可能な家賃債務保証業者を確保し自らが緊急連絡先となる</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_3_1_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_3_1_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_3_1_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_3_1_4"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">自ら賃借人となって賃貸借契約を締結した上で，１号特定技能外国人の合意の下，住居として提供する</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_3_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_3_2_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_3_2_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_3_2_4"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">所有する社宅等を，１号特定技能外国人の合意の下，当該外国人に対して住居として提供する</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_3_3_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_3_3_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_3_3_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_3_3_4"]|escape}</td>
			</tr>


			<tr>
				<th colspan="2">{$rowdata["C_3_4_1"]|escape}&nbsp;</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_3_4_2"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_3_4_3"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_3_4_4"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_3_4_5"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_3_4_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">情報提供する又は住居として提供する住居の概要（確保予定の場合を含む）</th>
			</tr>
			<tr>
				<td colspan="2">
					<ol class="input__subtext">
						<li>在留資格変更許可申請（又は在留資格認定証明書交付申請）の時点で確保しているもの</li>
						<li>在留資格変更許可申請（又は在留資格認定証明書交付申請）の後に確保するもの</li>
					</ol>
					{$rowdata["C_3_5_01"]|escape}&nbsp;
				</td>
			</tr>
			<tr>
				<th>居室の広さ</th>
				<td>
					<ol class="input__subtext">
						<li>１人当たり7.5㎡以上</li>
						<li>１人当たり7.5㎡未満</li>
					</ol>
					{$rowdata["C_3_5_02"]|escape}&nbsp;
				</td>
			</tr>
			<tr>
				<th>寝室の広さ</th>
				<td>
					<ol class="input__subtext">
						<li>１人当たり4.5㎡以上</li>
						<li>１人当たり4.5㎡未満</li>
					</ol>
					{$rowdata["C_3_5_03"]|escape}&nbsp;
				</td>
			</tr>
			<tr>
				<th>同居人合計</th>
				<td>{$rowdata["C_3_5_04"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">銀行その他の金融機関における預金口座又は貯金口座の開設の手続の補助</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_3_5_05"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_3_5_06"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_3_5_07"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_3_5_08"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_3_5_09"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_3_5_10"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">携帯電話の利用に関する契約の手続の補助</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_3_6_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_3_6_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_3_6_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_3_6_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_3_6_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_3_6_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">電気・水道・ガス等のライフラインに関する手続の補助</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_3_7_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_3_7_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_3_7_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_3_7_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_3_7_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_3_7_6"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">{$rowdata["C_3_8_1"]|escape}&nbsp;</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_3_8_2"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_3_8_3"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_3_8_4"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_3_8_5"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_3_8_6"]|escape}</td>
			</tr>
		</table>

		<h3 class="title is-5 has-margin-top-30">生活オリエンテーションの実施</h3>
		<table class="table is-bordered">
			<tr>
				<th colspan="2">本邦での生活一般に関する事項</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_4_1_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_4_1_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_4_1_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_4_1_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_4_1_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_4_1_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">法令の規定により外国人が履行しなければならない国又は地方公共団体の機関に対する届出その他の手続に関する事項及び必要に応じて同行し手続を補助すること</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_4_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_4_2_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_4_2_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_4_2_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_4_2_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_4_2_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">相談・苦情の連絡先，申出をすべき国又は地方公共団体の機関の連絡先</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_4_3_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_4_3_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_4_3_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_4_3_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_4_3_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_4_3_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">十分に理解することができる言語により医療を受けることができる医療機関に関する事項</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_4_4_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_4_4_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_4_4_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_4_4_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_4_4_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_4_4_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">防災・防犯に関する事項，急病その他の緊急時における対応に必要な事項</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_4_5_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_4_5_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_4_5_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_4_5_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_4_5_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_4_5_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">出入国又は労働に関する法令規定の違反を知ったときの対応方法その他当該外国人の法的保護に必要な事項</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_4_6_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_4_6_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_4_6_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_4_6_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_4_6_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_4_6_6"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">{$rowdata["C_4_7_1"]|escape}&nbsp;</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_4_7_2"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_4_7_3"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_4_7_4"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_4_7_5"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_4_7_6"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_4_7_7"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">実施言語</th>
			</tr>
			<tr>
				<td colspan="2">{$rowdata["C_4_8"]|escape}&nbsp;</td>
			</tr>
			<tr>
				<th colspan="2">実施予定時間</th>
			</tr>
			<tr>
				<td colspan="2">{$rowdata["C_4_9"]|escape}&nbsp;</td>
			</tr>
		</table>

		<h3 class="title is-5 has-margin-top-30">日本語学習の機会の提供</h3>
		<table class="table is-bordered">
			<tr>
				<th colspan="2">日本語教室や日本語教育機関に関する入学案内の情報を提供し，必要に応じて同行して入学の手続の補助を行う</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_5_1_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_5_1_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_5_1_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_5_1_4"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">自主学習のための日本語学習教材やオンラインの日本語講座に関する情報の提供し，必要に応じて日本語学習教材の入手やオンラインの日本語講座の利用契約手続の補助を行う</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_5_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_5_2_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_5_2_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_5_2_4"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">１号特定技能外国人との合意の下，日本語教師と契約して１号特定技能外国人に日本語の講習の機会を提供する</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_5_3_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_5_3_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_5_3_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_5_3_4"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">{$rowdata["C_5_4_1"]|escape}&nbsp;</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_5_4_2"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_5_4_3"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_5_4_4"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_5_4_5"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_5_4_6"]|escape}</td>
			</tr>
		</table>

		<h3 class="title is-5 has-margin-top-30">相談又は苦情への対応</h3>
		<table class="table is-bordered">
			<tr>
				<th colspan="2">相談又は苦情に対し，遅滞なく十分に理解できる言語により適切に対応し，必要な助言及び指導を行う</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_6_1_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_6_1_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_6_1_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_6_1_4"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">必要に応じ，相談内容に対応する関係行政機関を案内し，同行する等必要な手続の補助を行う</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_6_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_6_2_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_6_2_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_6_2_4"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">{$rowdata["C_6_3_1"]|escape}&nbsp;</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_6_3_2"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_6_3_3"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_6_3_4"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_6_3_5"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">対応時間</th>
			</tr>
			<tr>
				<th>平日</th>
				<td colspan="2">
					<div class="has-margin-top-10">
						月：{$rowdata["monday_time"]|escape}
					</div>
					<div class="has-margin-top-10">
						火：{$rowdata["tuesday_time"]|escape}
					</div>
					<div class="has-margin-top-10">
						水：{$rowdata["wednesday_time"]|escape}
					</div>
					<div class="has-margin-top-10">
						木：{$rowdata["thursday_time"]|escape}
					</div>
					<div class="has-margin-top-10">
						金：{$rowdata["friday_time"]|escape}
					</div>
				</td>
			</tr>
			<tr>
				<th>土・日・祝</th>
				<td>
					<div class="has-margin-top-10">
						土：{$rowdata["saturday_time"]|escape}
					</div>
					<div class="has-margin-top-10">
						日：{$rowdata["sunday_time"]|escape}
					</div>
					<div class="has-margin-top-10">
						祝：{$rowdata["holiday_time"]|escape}
					</div>
				</td>
			</tr>
			<tr>
				<th>相談方法</th>
				<td>{$rowdata["C_6_8_1"]|escape}</td>
			</tr>
			<tr>
				<th>電話番号</th>
				<td>{$rowdata["C_6_8_2"]|escape}</td>
			</tr>
			<tr>
				<th>メールアドレス</th>
				<td>{$rowdata["C_6_8_3"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_6_8_4"]|escape}</td>
			</tr>
			<tr>
				<th>緊急時　相談方法</th>
				<td>{$rowdata["C_6_9_1"]|escape}</td>
			</tr>
			<tr>
				<th>緊急時　電話番号</th>
				<td>{$rowdata["C_6_9_2"]|escape}</td>
			</tr>
			<tr>
				<th>緊急時　メールアドレス</th>
				<td>{$rowdata["C_6_9_3"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_6_9_4"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">実施言語</th>
			</tr>
			<tr>
				<td colspan="2">{$rowdata["C_6_9_5"]|escape}&nbsp;</td>
			</tr>
		</table>

		<h3 class="title is-5 has-margin-top-30">日本人との交流促進に係る支援</h3>
		<table class="table is-bordered">
			<tr>
				<th colspan="2">必要に応じ，地方公共団体やボランティア団体等が主催する地域住民との交流の場に関する情報の提供や地域の自治会等の案内を行い，各行事等への参加の手続の補助を行うほか，必要に応じて同行して各行事の注意事項や実施方法を説明するなどの補助を行う</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_7_1_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_7_1_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_7_1_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_7_1_4"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">日本の文化を理解するために必要な情報として，就労又は生活する地域の行事に関する案内を行うほか，必要に応じて同行し現地で説明するなどの補助を行う</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_7_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_7_2_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_7_2_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_7_2_4"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">{$rowdata["C_7_3_1"]|escape}&nbsp;</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_7_3_2"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_7_3_3"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_7_3_4"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_7_3_5"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_7_3_6"]|escape}</td>
			</tr>
		</table>

		<h3 class="title is-5 has-margin-top-30">非自発的離職時の転職支援</h3>
		<table class="table is-bordered">
			<tr>
				<th colspan="2">所属する業界団体や関連企業等を通じて次の受入れ先に関する情報を入手し提供する</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_8_1_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_8_1_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_8_1_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_8_1_4"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">公共職業安定所，その他の職業安定機関等を案内し，必要に応じて支援対象者に同行して次の受入れ先を探す補助を行う</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_8_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_8_2_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_8_2_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_8_2_4"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">１号特定技能外国人の希望条件，技能水準，日本語能力等を踏まえ，適切に職業相談・職業紹介が受けられるよう又は円滑に就職活動が行えるよう推薦状を作成する</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_8_3_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_8_3_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_8_3_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_8_3_4"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">職業紹介事業の許可又は届出を受けて職業紹介を行うことができる場合は，就職先の紹介あっせんを行う</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_8_4_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_8_4_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_8_4_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_8_4_4"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">１号特定技能外国人が求職活動をするために必要な有給休暇を付与する</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_8_5_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_8_5_2"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">離職時に必要な行政手続について情報を提供する</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_8_6_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_8_6_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_8_6_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_8_6_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_8_6_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_8_6_6"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">倒産等により，転職のための支援が適切に実施できなくなることが見込まれるときは，それに備え，当該機関に代わって支援を行う者を確保する</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_8_7_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_8_7_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_8_7_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_8_7_4"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">{$rowdata["C_8_8_1"]|escape}&nbsp;</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_8_8_2"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_8_8_3"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_8_8_4"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_8_8_5"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_8_8_6"]|escape}</td>
			</tr>
		</table>

		<h3 class="title is-5 has-margin-top-30">定期的な面談の実施・行政機関への通報</h3>
		<table class="table is-bordered">
			<tr>
				<th colspan="2">１号特定技能外国人の労働状況や生活状況を確認するため，当該外国人及びその監督をする立場にある者それぞれと定期的な面談を実施する</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_9_1_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_9_1_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_9_1_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_9_1_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_9_1_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_9_1_6"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">再確認のため，生活オリエンテーションにおいて提供した情報について，改めて提供する</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_9_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_9_2_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_9_2_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_9_2_4"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_9_2_5"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$rowdata["C_9_2_6"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">労働基準法その他の労働に関する法令の規定に違反していることを知ったときは，労働基準監督署その他の関係行政機関へ通報する</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_9_3_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_9_3_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_9_3_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_9_3_4"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">資格外活動等の入管法違反又は旅券及び在留カードの取上げ等その他の問題の発生を知ったときは，その旨を地方出入国在留管理局に通報する</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_9_4_1"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_9_4_2"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_9_4_3"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_9_4_4"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">{$rowdata["C_9_5_1"]|escape}&nbsp;</th>
			</tr>
			<tr>
				<th>実施予定</th>
				<td>{$rowdata["C_9_5_2"]|escape}</td>
			</tr>
			<tr>
				<th>実施予定日</th>
				<td>{$rowdata["C_9_5_3"]|escape}</td>
			</tr>
			<tr>
				<th>委託の有無</th>
				<td>{$rowdata["C_9_5_4"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者</th>
				<td>{$rowdata["C_9_5_5"]|escape}</td>
			</tr>
			<tr>
				<th>実施方法</th>
				<td>{$rowdata["C_9_5_6"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">実施言語</th>
			</tr>
			<tr>
				<td colspan="2">{$rowdata["C_9_6"]|escape}&nbsp;</td>
			</tr>
			<tr>
				<th colspan="2">実施予定時間</th>
			</tr>
			<tr>
				<td colspan="2">{$rowdata["C_9_7"]|escape}&nbsp;</td>
			</tr>
		</table>


		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark return-button" id="return-button">{$labels["back"]|escape}</button>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
