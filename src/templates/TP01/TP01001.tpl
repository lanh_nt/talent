<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/TP01/TP01001.js"></script>
</head>

<body id="TP01001">
<form id="mainForm" method="POST" action="TP01001">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="report_id" id="report_id" />
<input type="hidden" name="worker_id" id="worker_id" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<section class="has-margin-top-50">
			<div>
				<span class="has-width-50pct">{$labels["L-01-01-01"]|escape}：</span><a href="../CN01/CN01001">{$items["deadlineInquiryCount"]}{$labels["L-01-01-02"]|escape}</a>
			</div>
			<div class="has-margin-top-10">
				<span class="has-width-50pct">{$labels["L-02-01-01"]|escape}：</span><a href="../CN01/CN01001">{$items["noReplyInquiryCount"]}{$labels["L-02-01-02"]|escape}</a>
			</div>
		</section>

		<section class="has-margin-top-50">
			<h2 class="title is-5">{$labels["L-03"]|escape}</h2>
{if count($items["nearlyInputLimitReports"]) == 0}
		<div class="align-center">{$labels["no_records"]|escape}</div>
{else}
			<div class="sammary">
				<table class="table has-width-100pct responsive-list">
					<thead>
						<tr>
							<th>{$labels["L-03-01"]|escape}</th>
							<th>{$labels["L-03-02"]|escape}</th>
							<th>{$labels["L-03-03"]|escape}</th>
							<th>{$labels["L-03-04"]|escape}</th>
						</tr>
					</thead>
					<tbody>
{foreach from=$items["nearlyInputLimitReports"] item=$row}
						<tr class="{$row["row_class"|escape]}">
							<td>{$row["user_name"|escape]}</td>
							<td>{$row["report_name"|escape]}</td>
							<td>{$row["input_deadline_ymd"|escape]}</td>
							<td>
								<button type="button" class="button is-dark detail-button" id="detail_button_1_{$row["report_id"]|escape}_{$row["user_id"]|escape}">{$labels["contents"]|escape}</button>
							</td>
						</tr>
{/foreach}
					</tbody>
				</table>
			</div>
{/if}
		</section>

		<section class="has-margin-top-50">
			<h2 class="title is-5">{$labels["L-04"]|escape}</h2>
{if count($items["nearlySubmitLimitReports"]) == 0}
		<div class="align-center">{$labels["no_records"]|escape}</div>
{else}
			<div class="sammary">
				<table class="table has-width-100pct responsive-list">
					<thead>
						<tr>
							<th>{$labels["L-04-01"]|escape}</th>
							<th>{$labels["L-04-02"]|escape}</th>
							<th>{$labels["L-04-03"]|escape}</th>
							<th>{$labels["L-04-04"]|escape}</th>
						</tr>
					</thead>
					<tbody>
{foreach from=$items["nearlySubmitLimitReports"] item=$row}
						<tr class="{$row["row_class"|escape]}">
							<td>{$row["user_name"|escape]}</td>
							<td>{$row["report_name"|escape]}</td>
							<td>{$row["submission_deadline_ymd"|escape]}</td>
							<td>
								<button type="button" class="button is-dark detail-button" id="detail_button_2_{$row["report_id"]|escape}_{$row["user_id"]|escape}">{$labels["contents"]|escape}</button>
							</td>
						</tr>
{/foreach}
					</tbody>
				</table>
			</div>
{/if}
		</section>

		<section class="has-margin-top-50">
			<h2 class="title is-5">{$labels["L-05-01"]|escape}</h2>
{if count($items["rejectReports"]) == 0}
		<div class="align-center">{$labels["no_records"]|escape}</div>
{else}
			<div class="sammary">
				<table class="table has-width-100pct responsive-list">
					<thead>
						<tr>
							<th>{$labels["L-05-02"]|escape}</th>
							<th>{$labels["L-05-03"]|escape}</th>
							<th>{$labels["L-05-04"]|escape}</th>
							<th>{$labels["L-05-05"]|escape}</th>
						</tr>
					</thead>
					<tbody>
{foreach from=$items["rejectReports"] item=$row}
						<tr class="{$row["row_class"|escape]}">
							<td>{$row["user_name"|escape]}</td>
							<td>{$row["report_name"|escape]}</td>
							<td>{$row["input_deadline_ymd"|escape]}</td>
							<td>
								<button type="button" class="button is-dark detail-button" id="detail_button_3_{$row["report_id"]|escape}_{$row["user_id"]|escape}">{$labels["contents"]|escape}</button>
							</td>
						</tr>
{/foreach}
					</tbody>
				</table>
			</div>
{/if}
		</section>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
