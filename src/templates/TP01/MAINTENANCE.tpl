<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title></title>

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
</head>

<body>
{if $isLogin == 1}
{include file='common/header-admin.tpl'}
{else if $isLogin == 2}
{include file='common/header-worker.tpl'}
{else}
<nav class="navbar is-link" role="navigation" aria-label="main navigation">
	<div class="navbar-brand navbar-item is-font-bold">
		<a class="navbar-item is-font-bold" href="#"><img src="../assets/img/logo.png"></a>
	</div>
</nav>
{/if}

<input type="hidden" name="mode" id="mode" />
	<!-- メインコンテンツ -->
	<div class="contents">
		<div class="columns has-margin-10">
			<div class="column is-8 is-offset-2">
				<div class="box has-margin-top-50">
					<h2 class="has-text-centered title is-4">{$title}</h2>
					<div class="box">{$message}</div>
				</div>
			</div>
		</div>
	</div> 
</body>
</html>
