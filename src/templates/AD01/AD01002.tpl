<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/AD01/AD01002.js"></script>
</head>

<body id="AD01002">
	<form id="mainForm" enctype="multipart/form-data" method="POST" action="AD01002">
	<input type="hidden" name="mode" id="mode" />
	<input type="hidden" name="ad_id" id="ad_id" value="{$items["ad_id"]|escape}" />

	<input type="hidden" name="page_no" id="page_no" value="{$items["page_no"]|escape}"  />
	<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}"  />

	<input type="hidden" name="C_01" id="C_01" value="{$items["C_01"]|escape}"  />
	<input type="hidden" name="C_02" id="C_02" value="{$items["C_02"]|escape}"  />
	<input type="hidden" name="C_03" id="C_03" value="{$items["C_03"]|escape}"  />
	<input type="hidden" name="C_04" id="C_04" value="{$items["C_04"]|escape}"  />
	<input type="hidden" name="C_05_1" id="C_05_1" value="{$items["C_05_1"]|escape}"  />
	<input type="hidden" name="C_05_2" id="C_05_2" value="{$items["C_05_2"]|escape}"  />
	<input type="hidden" name="C_06_1" id="C_06_1" value="{$items["C_06_1"]|escape}"  />
	<input type="hidden" name="C_06_2" id="C_06_2" value="{$items["C_06_2"]|escape}"  />
	<input type="hidden" name="C_06_3" id="C_06_3" value="{$items["C_06_3"]|escape}"  />
	<input type="hidden" name="C_07_1" id="C_07_2" value="{$items["C_07_1"]|escape}"  />
	<input type="hidden" name="C_07_1" id="C_07_2" value="{$items["C_07_2"]|escape}"  />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">システム管理者管理/詳細</h2>

		<table class="table is-bordered has-width-100pct">
			<tr>
				<th>ID</th>
				<td>
					<p>{$rowdata["user_id"]|escape}</p>
				</td>
			</tr>

			<tr>
				<th>氏名</th>
				<td>
					<p>{$rowdata["user_name"]|escape}</p>
				</td>
			</tr>

			<tr>
				<th>性別</th>
				<td>
					<p>{$rowdata["sex"]|escape}</p>
				</td>
			</tr>

			<tr>
				<th>企業名</th>
				<td>
					<p>{$rowdata["company_name"]|escape}</p>
				</td>
			</tr>

			<tr>
				<th>役職</th>
				<td>
					<p>{$rowdata["department"]|escape}</p>
				</td>
			</tr>
			<tr>
				<th>種別</th>
				<td>
					<p>{$rowdata["class"]|escape}</p>
				</td>
			</tr>
		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark return-button" id="return-button">{$labels["back"]|escape}</button>
			</div>
{if $smarty.session.loginUserAuth <= '2'}
			<div class="column is-6 has-text-right">
				<button type="button" class="button is-primary" id="udate_button">{$labels["edit"]|escape}</button>
			</div>
{/if}
		</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
	</form>
</body>
</html>
