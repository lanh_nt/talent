<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/AD01/AD01003.js"></script>
</head>

<body id="AD01003">
	<form id="mainForm" enctype="multipart/form-data" method="POST" action="AD01003">
	<input type="hidden" name="mode" id="mode" />
	<input type="hidden" name="ad_id" id="ad_id" value="{$items["ad_id"]|escape}" />
	<input type="hidden" name="csrf_token" value="{$smarty.session["csrf_token"]}" />
	<input type="hidden" name="page_no" id="page_no" value="{$items["page_no"]|escape}"  />
	<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}"  />
	<input type="hidden" name="C_01" id="C_01" value="{$items["C_01"]|escape}"  />
	<input type="hidden" name="C_02" id="C_02" value="{$items["C_02"]|escape}"  />
	<input type="hidden" name="C_03" id="C_03" value="{$items["C_03"]|escape}"  />
	<input type="hidden" name="C_04" id="C_04" value="{$items["C_04"]|escape}"  />
	<input type="hidden" name="C_05_1" id="C_05_1" value="{$items["C_05_1"]|escape}"  />
	<input type="hidden" name="C_05_2" id="C_05_2" value="{$items["C_05_2"]|escape}"  />
	<input type="hidden" name="C_06_1" id="C_06_1" value="{$items["C_06_1"]|escape}"  />
	<input type="hidden" name="C_06_2" id="C_06_2" value="{$items["C_06_2"]|escape}"  />
	<input type="hidden" name="C_06_3" id="C_06_3" value="{$items["C_06_3"]|escape}"  />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">システム管理者管理/編集</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

		<table class="table is-bordered has-width-100pct">
			<tr>
				<th>氏名</th>
				<td>
					<input class="input" name="user_name" id="user_name" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["user_name"]|escape}">
				</td>
			</tr>

			<tr>
				<th>性別</th>
				<td>
					<div class="field">
						<input class="is-checkradio" id="sex_1" type="radio" name="sex" value="1" {if $rowdata["sex"] == "1"} checked="checked"{/if}>
						<label for="sex_1">男性</label>
						<input class="is-checkradio" id="sex_2" type="radio" name="sex" value="2" {if $rowdata["sex"] == "2"} checked="checked"{/if}>
						<label for="sex_2">女性</label>
					</div>
				</td>
			</tr>

			<tr>
				<th>企業名</th>
				<td>
				<div class="column is-10">
					<div class="select has-vertical-middle has-width-100pct">
						<select name="company_id" id="company_id" class="has-width-100pct">
							<option value=""></option>
{foreach from=$companies item=$company}
							<option value="{$company["id"]}"{if $company["id"] == $rowdata["company_id"]} selected="selected"{/if}>{$company["name"]}</option>
{/foreach}
						</select>
					</div>
				</div>
				</td>
			</tr>

			<tr>
				<th>役職</th>
				<td>
					<input class="input"  name="department" id="department" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["department"]|escape}">
				</td>
			</tr>

			<tr>
				<th>メールアドレス</th>
				<td>
					<input class="input"  name="user_id" id="user_id" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["user_id"]|escape}">
				</td>
			</tr>

			<tr>
				<th>種別</th>
				<td>
					<div class="field">
						<input class="is-checkradio" id="classification_1" type="radio" name="classification" value="1" {if $rowdata["classification"] == "1"} checked="checked"{/if}>
						<label for="classification_1">責任者（編集・閲覧）</label>
						<input class="is-checkradio" id="classification_2" type="radio" name="classification" value="2" {if $rowdata["classification"] == "2"} checked="checked"{/if}>
						<label for="classification_2">担当者（編集・閲覧）</label>
						<input class="is-checkradio" id="classification_3" type="radio" name="classification" value="3" {if $rowdata["classification"] == "3"} checked="checked"{/if}>
						<label for="classification_3">担当者（閲覧）</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>新規ログインパスワード</th>
				<td><input class="input"  name="new_password" id="new_password" type="password" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["new_password"]|escape}">
			</tr>
			<tr>
				<th>新規ログインパスワード(確認)</th>
				<td>
				    <input class="input"  name="new_password2" id="new_password2" type="password" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["new_password2"]|escape}">
				</td>
			</tr>

		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark return-button" id="return-button">{$labels["back"]|escape}</button>
			</div>
			<div class="column is-6 has-text-right">
				<button type="button" class="button is-primary" id="regist_button">{$labels["regist"]|escape}</button>
			</div>
		</div>


	</div> <!-- /.contents -->


	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
	</form>
</body>
</html>
