<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/AD01/AD01001.js"></script>
</head>

<body id="AD01001">
<form id="mainForm" enctype="multipart/form-data" method="POST" action="AD01001">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}" />
<input type="hidden" name="ad_id" id="ad_id"  value="{$items["ad_id"]|escape}" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
	<h2 class="title is-4">システム管理者管理</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

{if $smarty.session.loginUserAuth <= '2'}
		<div class="has-text-right has-margin-bottom-20">
			<input type="file" name="upfile" id="upfile" style="display:none;" />
			<button type="button" class="button is-primary has-margin-right-20" id="csv_import_button">{$labels["csv_import"]|escape}</button>
			<button type="button" class="button is-primary has-margin-right-20" id="csv_export_button">{$labels["csv_export"]|escape}</button>
			<button type="button" class="button is-primary has-margin-right-20" id="csv_format_button">{$labels["csv_format"]|escape}</button>
		</div>
{/if}

		<div class="box has-padding-20">
			<div class="columns is-multiline formbox-gap">
				<!-- ID -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">ID:</p>
				</div>
				<div class="column is-10">
					<input class="input" type="text" name="C_01" id="C_01" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_01"]|escape}">
				</div>

				<!-- 氏名 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">氏名：</p>
				</div>
				<div class="column is-10">
					<input class="input" type="text" name="C_02" id="C_02" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_02"]|escape}">
				</div>

{if $items["loginCompanyType"] == '2'}
				<!-- 企業名 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">企業名：</p>
				</div>
				<div class="column is-10">
					<input class="input" type="text" name="C_03" id="C_03" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_03"]|escape}">
				</div>
{/if}
				<!-- 役職 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">役職：</p>
				</div>
				<div class="column is-10">
					<input class="input" type="text" name="C_04" id="C_04" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_04"]|escape}">
				</div>

				<!-- 性別 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">性別：</p>
				</div>
				<div class="column is-10">
					<div class="field">
						<input class="is-checkradio" type="checkbox" name="C_05_1" id="C_05_1" {if $items["C_05_1"] != ""}checked="checked"{/if}>
						<label for="C_05_1">男性</label>
						<input class="is-checkradio" type="checkbox" name="C_05_2" id="C_05_2" {if $items["C_05_2"] != ""}checked="checked"{/if}>
						<label for="C_05_2">女性</label>
					</div>
				</div>

				<!-- 種別 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">種別：</p>
				</div>
				<div class="column is-10">
					<div class="field">
						<input class="is-checkradio" type="checkbox" name="C_06_1" id="C_06_1" {if $items["C_06_1"] != ""}checked="checked"{/if}>
						<label for="C_06_1">責任者（編集・閲覧）</label>
						<input class="is-checkradio" type="checkbox" name="C_06_2" id="C_06_2" {if $items["C_06_2"] != ""}checked="checked"{/if}>
						<label for="C_06_2">担当者（編集・閲覧）</label>
						<input class="is-checkradio" type="checkbox" name="C_06_3" id="C_06_3" {if $items["C_06_3"] != ""}checked="checked"{/if}>
						<label for="C_06_3">担当者（閲覧）</label>
					</div>
				</div>

				<!-- 登録日 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">登録日：</p>
				</div>
				<div class="column is-10">
					<input id="C_07_1" name="C_07_1" data-mindate="today" type="text" class="input has-width-max40pct is-inline-block flatpickr" placeholder="日付を選択してください" value="{$items["C_07_1"]|escape}" required />
					<span class="has-vertical-middle">～</span>
					<input id="C_07_2" name="C_07_2" data-mindate="today" type="text" class="input has-width-max40pct is-inline-block flatpickr is-back-white" placeholder="日付を選択してください" value="{$items["C_07_2"]|escape}" required />
				</div>

			</div><!--  /.columns -->

			<div class="has-text-centered has-margin-top-20">
				<a class="button is-info submit-button" id="search_button">検索</a>
			</div>
		</div>
{if isset($pageData->pageDatas)}
{if count($pageData->pageDatas) == 0}
		<div class="align-center">検索結果がありません。</div>
{else}

		<table class="table has-margin-top-50 has-width-100pct responsive-list">
			<thead>
				<tr>
					<th>ID</th>
					<th>氏名</th>
					<th>性別</th>
					<th>役職</th>
					<th>種別</th>
					<th>登録日</th>
					<th>詳細</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>ID</th>
					<th>氏名</th>
					<th>性別</th>
					<th>役職</th>
					<th>種別</th>
					<th>登録日</th>
					<th>詳細</th>
				</tr>
			</tfoot>
			<tbody>
{foreach from=$pageData->pageDatas item=$row}
				<tr>
					<td>{$row["u_id"]|escape}</td>
					<td>{$row["user_name"]|escape}</td>
					<td>{$row["sex"]|escape}</td>
					<td>{$row["department"]|escape}</td>
					<td>{$row["classification"]|escape}</td>
					<td>{$row["update_date"]|escape}</td>
					<td>
						<button type="button" class="button is-dark detail-button" id="detail_button_{$row["r_id"]|escape}">{$labels["contents"]|escape}</button></br>
					</td>
				</tr>
{/foreach}
			</tbody>
		</table>

{include file='common/pager.tpl' pageData=$pageData}{* --- ページング部品 --- *}

{/if}
{/if}

	</div> <!-- /.contents -->


	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
