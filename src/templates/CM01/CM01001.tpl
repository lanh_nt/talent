<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/CM01/CM01001.js"></script>
</head>

<body id="CM01001">
<form id="mainForm" enctype="multipart/form-data" method="POST" action="CM01001">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}" />
<input type="hidden" name="cm_id" id="cm_id"  value="{$items["cm_id"]|escape}" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">企業管理</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

{if $items["loginCompanyType"] == '2'}
{if $smarty.session.loginUserAuth <= '2'}
		<div class="has-text-right has-margin-bottom-20">
			<input type="file" name="upfile" id="upfile" style="display:none;" />
			<button type="button" class="button is-primary has-margin-right-20" id="csv_import_button">{$labels["csv_import"]|escape}</button>
			<button type="button" class="button is-primary has-margin-right-20" id="csv_export_button">{$labels["csv_export"]|escape}</button>
			<button type="button" class="button is-primary has-margin-right-20" id="csv_format_button">{$labels["csv_format"]|escape}</button>
		</div>
{/if}
{/if}

		<div class="box has-padding-20">
			<div class="columns is-multiline formbox-gap">
				<!-- ID -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">ID:</p>
				</div>
				<div class="column is-10">
					<input class="input" type="text" name="C_01" id="C_01" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_01"]|escape}">
				</div>

				<!-- 法人番号 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">法人番号:</p>
				</div>
				<div class="column is-10">
					<input class="input" type="text" name="C_02" id="C_02" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_02"]|escape}">
				</div>

{if $items["loginCompanyType"] == '2'}
				<!-- 企業名 -->

				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">企業名:</p>
				</div>
				<div class="column is-10">
					<input class="input" type="text" name="C_03" id="C_03" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_03"]|escape}">
				</div>
{/if}
{if $items["loginCompanyType"] == '2'}
				<!-- 種別 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">種別：</p>
				</div>
				<div class="column is-10">
					<div class="field">
						<input class="is-checkradio" type="checkbox" name="C_04_1" id="C_04_1" {if $items["C_04_1"] != ""}checked="checked"{/if}>
						<label for="C_04_1">受入機関</label>
						<input class="is-checkradio" type="checkbox" name="C_04_2" id="C_04_2" {if $items["C_04_2"] != ""}checked="checked"{/if}>
						<label for="C_04_2">支援機関</label>
						<input class="is-checkradio" type="checkbox" name="C_04_3" id="C_04_3" {if $items["C_04_3"] != ""}checked="checked"{/if}>
						<label for="C_04_3">委託先</label>
					</div>

				</div>
{/if}
				<!-- 登録日 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">登録日：</p>
				</div>
				<div class="column is-10">
					<input id="C_05_1" name="C_05_1" type="text" class="input has-width-max40pct is-inline-block flatpickr"　placeholder="日付を選択してください" value="{$items["C_05_1"]|escape}" required />
					<span class="has-vertical-middle">～</span>
					<input id="C_05_2" name="C_05_2" type="text" class="input has-width-max40pct is-inline-block flatpickr is-back-white" value="{$items["C_05_2"]|escape}"　placeholder="日付を選択してください" required />
				</div>
			</div><!--  /.columns -->

			<div class="has-text-centered has-margin-top-20">
				<a class="button is-info submit-button" id="search_button">検索</a>
			</div>
		</div>
{if isset($pageData->pageDatas)}
{if count($pageData->pageDatas) == 0}
		<div class="align-center">検索結果がありません。</div>
{else}
		<table class="table has-margin-top-50 has-width-100pct responsive-list">
			<thead>
				<tr>
					<th>ID</th>
					<th>機関名</th>
					<th>法人番号</th>
					<th>種別</th>
					<th>登録日</th>
					<th>詳細</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>ID</th>
					<th>機関名</th>
					<th>法人番号</th>
					<th>種別</th>
					<th>登録日</th>
					<th>詳細</th>
				</tr>
			</tfoot>
			<tbody>
{foreach from=$pageData->pageDatas item=$row}
				<tr>
					<td>{$row["u_id"]|escape}</td>
					<td>{$row["company_name"]|escape}</td>
					<td>{$row["corporation_no"]|escape}</td>
					<td>{$row["classification"]|escape}</td>
					<td>{$row["update_date"]|escape}</td>
					<td>
						<button type="button" class="button is-dark detail-button" id="detail_button_{$row["r_id"]|escape}">{$labels["contents"]|escape}</button></br>
					</td>
				</tr>
{/foreach}
			</tbody>
		</table>

{include file='common/pager.tpl' pageData=$pageData}{* --- ページング部品 --- *}

{/if}
{/if}

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
