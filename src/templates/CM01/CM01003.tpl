<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/CM01/CM01003.js"></script>
</head>

<body id="CM01003">
	<form id="mainForm" enctype="multipart/form-data" method="POST" action="CM01003">
	<input type="hidden" name="mode" id="mode" />
	<input type="hidden" name="cm_id" id="cm_id" value="{$items["cm_id"]|escape}" />
	<input type="hidden" name="csrf_token" value="{$smarty.session["csrf_token"]}" />
	<input type="hidden" name="page_no" id="page_no" value="{$items["page_no"]|escape}"  />
	<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}"  />
	<input type="hidden" name="C_01" id="C_01" value="{$items["C_01"]|escape}"  />
	<input type="hidden" name="C_02" id="C_02" value="{$items["C_02"]|escape}"  />
	<input type="hidden" name="C_03" id="C_03" value="{$items["C_03"]|escape}"  />
	<input type="hidden" name="C_04_1" id="C_04_1" value="{$items["C_04_1"]|escape}"  />
	<input type="hidden" name="C_04_2" id="C_04_2" value="{$items["C_04_2"]|escape}"  />
	<input type="hidden" name="C_04_3" id="C_04_3" value="{$items["C_04_3"]|escape}"  />
	<input type="hidden" name="C_05_1" id="C_05_1" value="{$items["C_05_1"]|escape}"  />
	<input type="hidden" name="C_05_2" id="C_05_2" value="{$items["C_05_2"]|escape}"  />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">企業管理/編集</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

		<table class="table is-bordered has-width-100pct">
			<tr>
				<th>種別</th>
				<td>
					<div class="field">
						<input class="is-checkradio" id="classification1" type="radio" name="classification" value="1" {if $rowdata["classification"] == "1"} checked="checked"{/if}>
						<label for="classification1">受入機関</label>
						<input class="is-checkradio" id="classification2" type="radio" name="classification" value="2" {if $rowdata["classification"] == "2"} checked="checked"{/if}>
						<label for="classification2">支援機関</label>
						<input class="is-checkradio" id="classification3" type="radio" name="classification" value="3" {if $rowdata["classification"] == "3"} checked="checked"{/if}>
						<label for="classification3">委託先</label>
					</div>
				</td>
			</tr>

			<tr>
				<th>企業ID</th>
				<td>
					<input class="input" type="text" name="company_id" id= "company_id" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["company_id"]|escape}">
				</td>
			</tr>
			<tr>
				<th>氏名又は名称(ふりがな)</th>
				<td>
					<input class="input" type="text" name="company_kana" id= "company_kana" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["company_kana"]|escape}">
				</td>
			</tr>


			<tr>
				<th>氏名又は名称</th>
				<td>
					<input class="input" type="text" name="company_name" id= "company_name" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["company_name"]|escape}">
				</td>
			</tr>

			<tr>
				<th>法人番号</th>
				<td>
					<input class="input" type="text" name="corporation_no" id= "corporation_no" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["corporation_no"]|escape}">
				</td>
			</tr>

			<tr>
				<th>郵便番号（所在地）</th>
				<td>
					<input class="input" type="text" name="postal_code" id= "postal_code" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["postal_code"]|escape}">
				</td>
			</tr>

			<tr>
				<th>住所（所在地）</th>
				<td>
					<input class="input" type="text" name="address" id= "address" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["address"]|escape}">
				</td>
			</tr>

			<tr>
				<th>電話番号</th>
				<td>
					<input class="input" type="text" name="tel" id= "tel" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["tel"]|escape}">
				</td>
			</tr>

			<tr>
				<th>代表者の氏名（ふりがな）</th>
				<td>
					<input class="input" type="text" name="representative_kana" id= "representative_kana" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["representative_kana"]|escape}">
				</td>
			</tr>

			<tr>
				<th>代表者の氏名</th>
				<td>
					<input class="input" type="text" name="representative_name" id= "representative_name" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["representative_name"]|escape}">
				</td>
			</tr>

			<tr>
				<th>登録番号(許可 &middot; 届出番号)</th>
				<td>
					<input class="input" type="text" name="registration_no" id= "registration_no" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["registration_no"]|escape}">
				</td>
			</tr>

			<tr>
				<th>
					登録年月日<br>(許可 &middot; 届出番号
					受理年月日)
				</th>
				<td>
					<div class="columns is-gapless is-mobile">
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="registration_date_year" id="registration_date_year">
									<option value=""></option>
{foreach from=$years item=$year}
									<option value="{$year}"{if $year == $rowdata["registration_date_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["year"]|escape}</span>
						</div>
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="registration_date_month" id="registration_date_month">
									<option value=""></option>
{foreach from=$months item=$month}
									<option value="{$month}"{if $month == $rowdata["registration_date_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
								</select>
							</div>

							<span class="has-vertical-middle">{$labels["month"]|escape}</span>
						</div>
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="registration_date_day" id="registration_date_day">
									<option value=""></option>
{foreach from=$days item=$day}
									<option value="{$day}"{if $day == $rowdata["registration_date_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["day"]|escape}</span>
						</div>
					</div>
				</td>
			</tr>

			<tr>
				<th>所属機関監督者 氏名1</th>
				<td>
					<input class="input" type="text" name="supervisor_name1" id= "supervisor_name1" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_name1"]|escape}">
				</td>
			</tr>

			<tr>
				<th>所属機関監督者 所属部署1</th>
				<td>
					<input class="input" type="text" name="supervisor_department1" id= "supervisor_department1" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_department1"]|escape}">
				</td>
			</tr>

			<tr>
				<th>所属機関監督者 役職1</th>
				<td>
					<input class="input" type="text" name="supervisor_position1" id= "supervisor_position1" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_position1"]|escape}">
				</td>
			</tr>

			<tr>
				<th>所属機関監督者 氏名2</th>
				<td>
					<input class="input" type="text" name="supervisor_name2" id= "supervisor_name2" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_name2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>所属機関監督者 所属部署2</th>
				<td>
					<input class="input" type="text" name="supervisor_department2" id= "supervisor_department2" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_department2"]|escape}">
				</td>			
			</tr>

			<tr>
				<th>所属機関監督者 役職2</th>
				<td>
					<input class="input" type="text" name="supervisor_position2" id= "supervisor_position2" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_position2"]|escape}">
				</td>
			</tr>

			<tr>
				<th>所属機関監督者 氏名3</th>
				<td>
					<input class="input" type="text" name="supervisor_name3" id= "supervisor_name3" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_name3"]|escape}">
				</td>			
			</tr>
			<tr>
				<th>所属機関監督者 所属部署3</th>
				<td>
					<input class="input" type="text" name="supervisor_department3" id= "supervisor_department3" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_department3"]|escape}">
				</td>			
			</tr>

			<tr>
				<th>所属機関監督者 役職3</th>
				<td>
					<input class="input" type="text" name="supervisor_position3" id= "supervisor_position3" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_position3"]|escape}">
				</td>
			</tr>

			<tr>
				<th>所属機関監督者 氏名4</th>
				<td>
					<input class="input" type="text" name="supervisor_name4" id= "supervisor_name4" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_name4"]|escape}">
				</td>
			</tr>
			<tr>
				<th>所属機関監督者 所属部署4</th>
				<td>
					<input class="input" type="text" name="supervisor_department4" id= "supervisor_department4" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_department4"]|escape}">
				</td>
			</tr>

			<tr>
				<th>所属機関監督者 役職4</th>
				<td>
					<input class="input" type="text" name="supervisor_position4" id= "supervisor_position4" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_position4"]|escape}">
				</td>
			</tr>

			<tr>
				<th>所属機関監督者 氏名5</th>
				<td>
					<input class="input" type="text" name="supervisor_name5" id= "supervisor_name5" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_name5"]|escape}">
				</td>
			</tr>
			<tr>
				<th>所属機関監督者 所属部署5</th>
				<td>
					<input class="input" type="text" name="supervisor_department5" id= "supervisor_department5" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_department5"]|escape}">
				</td>
			</tr>

			<tr>
				<th>所属機関監督者 役職5</th>
				<td>
					<input class="input" type="text" name="supervisor_position5" id= "supervisor_position5" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["supervisor_position5"]|escape}">
				</td>
			</tr>

			<tr>
				<th>
					支援業務を開始する予定年月日
				</th>
				<td>
					<div class="columns is-gapless is-mobile">
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="support_scheduled_date_year" id="support_scheduled_date_year">
									<option value=""></option>
{foreach from=$years item=$year}
									<option value="{$year}"{if $year == $rowdata["support_scheduled_date_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["year"]|escape}</span>
						</div>
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="support_scheduled_date_month" id="support_scheduled_date_month">
									<option value=""></option>
{foreach from=$months item=$month}
									<option value="{$month}"{if $month == $rowdata["support_scheduled_date_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
								</select>
							</div>

							<span class="has-vertical-middle">{$labels["month"]|escape}</span>
						</div>
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="support_scheduled_date_day" id="support_scheduled_date_day">
									<option value=""></option>
{foreach from=$days item=$day}
									<option value="{$day}"{if $day == $rowdata["support_scheduled_date_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["day"]|escape}</span>
						</div>
					</div>
				</td>
			</tr>

			<tr>
				<th>支援を行う事業所の名称</th>
				<td>
					<input class="input" type="text" name="support_company_name" id= "support_company_name" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["support_company_name"]|escape}">
				</td>
			</tr>

			<tr>
				<th>支援を行う事業所 郵便番号</th>
				<td>
					<input class="input" type="text" name="support_postal_code" id= "support_postal_code" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["support_postal_code"]|escape}">
				</td>
			</tr>

			<tr>
				<th>支援を行う事業所 住所</th>
				<td>
					<input class="input" type="text" name="support_address" id= "support_address" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["support_address"]|escape}">
				</td>
			</tr>

			<tr>
				<th>支援を行う事業所  電話番号</th>
				<td>
					<input class="input" type="text" name="support_tel" id= "support_tel" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["support_tel"]|escape}">
				</td>
			</tr>

			<tr>
				<th>支援責任者名(ふりがな)</th>
				<td>
					<input class="input" type="text" name="support_manager_kana" id= "support_manager_kana" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["support_manager_kana"]|escape}">
				</td>
			</tr>

			<tr>
				<th>支援責任者名(氏名)</th>
				<td>
					<input class="input" type="text" name="support_manager_name" id= "support_manager_name" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["support_manager_name"]|escape}">
				</td>
			</tr>

			<tr>
				<th>支援責任者 役職</th>
				<td>
					<input class="input" type="text" name="support_manager_department" id= "support_manager_department" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["support_manager_department"]|escape}">
				</td>
			</tr>

			<tr>
				<th>支援担当者名(ふりがな)</th>
				<td>
					<input class="input" type="text" name="support_handler_kana" id= "support_handler_kana" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["support_handler_kana"]|escape}">
				</td>
			</tr>

			<tr>
				<th>支援担当者名(氏名)</th>
				<td>
					<input class="input" type="text" name="support_handler_name" id= "support_handler_name" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["support_handler_name"]|escape}">
				</td>
			</tr>

			<tr>
				<th>支援担当者 役職</th>
				<td>
					<input class="input" type="text" name="support_handler_department" id= "support_handler_department" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["support_handler_department"]|escape}">
				</td>
			</tr>

			<tr>
				<th>対応可能言語</th>
				<td>
					<input class="input" type="text" name="available_language" id= "available_language" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["available_language"]|escape}">
				</td>
			</tr>

			<tr>
				<th>
					支援を行っている<br>
				1号特定技能外国人数
				</th>
				<td>
					<input class="input" type="text" name="worker_mumber" id= "worker_mumber" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["worker_mumber"]|escape}">
				</td>
			</tr>

			<tr>
				<th>支援担当者数</th>
				<td>
					<input class="input" type="text" name="support_stuff_number" id= "support_stuff_number" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["support_stuff_number"]|escape}">
				</td>
			</tr>

			<tr>
				<th>業種</th>
				<td>
					<div class="is-inlineblock select has-vertical-middle">
						<select name="industry_type" id="industry_type">
							<option value=""></option>
{foreach from=$industrys key=$pkey item=$pname}
								<option value="{$pkey}"{if $pkey == $rowdata["industry_type"]} selected="selected"{/if}>{$pname}</option>
{/foreach}
						</select>
					</div>
				</td>
			</tr>

			<tr>
				<th>他に業種<br>
					（複数選択可）</th>
				<td>
					【製造業】<br>
						<input class="is-checkradio" id="industry_type_etc01" type="checkbox" name="industry_type_etc01" value="01"{if $rowdata["industry_type_etc01"]} =="01" checked="checked"{/if}>
						<label for="industry_type_etc01">卸売業(機械器具)</label>
						<input class="is-checkradio" id="industry_type_etc02" type="checkbox" name="industry_type_etc02" value="02"{if "02" == $rowdata["industry_type_etc02"]} checked="checked"{/if}>
						<label for="industry_type_etc02">製造業(繊維工業)</label>
						<input class="is-checkradio" id="industry_type_etc03" type="checkbox" name="industry_type_etc03" value="03"{if "03" == $rowdata["industry_type_etc03"]} checked="checked"{/if}>
						<label for="industry_type_etc03">製造業(プラスチック製品)</label>
						<input class="is-checkradio" id="industry_type_etc04" type="checkbox" name="industry_type_etc04" value="04"{if "04" == $rowdata["industry_type_etc04"]} checked="checked"{/if}>
						<label for="industry_type_etc04">製造業(金属製品)</label>
						<input class="is-checkradio" id="industry_type_etc05" type="checkbox" name="industry_type_etc05" value="05"{if "05" == $rowdata["industry_type_etc05"]} checked="checked"{/if}>
						<label for="industry_type_etc05">製造業(生産用機械器具)</label>
						<input class="is-checkradio" id="industry_type_etc06" type="checkbox" name="industry_type_etc06" value="06"{if "06" == $rowdata["industry_type_etc06"]} checked="checked"{/if}>
						<label for="industry_type_etc06">製造業(電気機械器具)</label>
						<input class="is-checkradio" id="industry_type_etc07" type="checkbox" name="industry_type_etc07" value="07"{if "07" == $rowdata["industry_type_etc07"]} checked="checked"{/if}>
						<label for="industry_type_etc07">製造業(輸送用機械器具)</label>
						<input class="is-checkradio" id="industry_type_etc08" type="checkbox" name="industry_type_etc08" value="08"{if "08" == $rowdata["industry_type_etc08"]} checked="checked"{/if}>
						<label for="industry_type_etc08">製造業(その他)</label>

					<br>【卸売業】<br>
						<input class="is-checkradio" id="industry_type_etc09" type="checkbox" name="industry_type_etc09" value="09"{if "09" == $rowdata["industry_type_etc09"]} checked="checked"{/if}>
						<label for="industry_type_etc09">卸売業(各種商品（総合商社等))</label>
						<input class="is-checkradio" id="industry_type_etc10" type="checkbox" name="industry_type_etc10" value="10"{if "10" == $rowdata["industry_type_etc10"]} checked="checked"{/if}>
						<label for="industry_type_etc10">卸売業(繊維・衣服等)</label>
						<input class="is-checkradio" id="industry_type_etc11" type="checkbox" name="industry_type_etc11" value="11"{if "11" == $rowdata["industry_type_etc11"]} checked="checked"{/if}>
						<label for="industry_type_etc11">卸売業(飲食料品)</label>
						<input class="is-checkradio" id="industry_type_etc12" type="checkbox" name="industry_type_etc12" value="12"{if "12" == $rowdata["industry_type_etc12"]} checked="checked"{/if}>
						<label for="industry_type_etc12">卸売業(建築材料，鉱物，金属材料等)</label>
						<input class="is-checkradio" id="industry_type_etc13" type="checkbox" name="industry_type_etc13" value="13"{if "13" == $rowdata["industry_type_etc13"]} checked="checked"{/if}>
						<label for="industry_type_etc13">卸売業(機械器具)</label>
						<input class="is-checkradio" id="industry_type_etc14" type="checkbox" name="industry_type_etc14" value="14"{if "14" == $rowdata["industry_type_etc14"]} checked="checked"{/if}>
						<label for="industry_type_etc14">卸売業(その他)</label>

					<br>【小売業】<br>			
						<input class="is-checkradio" id="industry_type_etc15" type="checkbox" name="industry_type_etc15" value="15"{if "15" == $rowdata["industry_type_etc15"]} checked="checked"{/if}>
						<label for="industry_type_etc15">小売業(各種商品)</label>
						<input class="is-checkradio" id="industry_type_etc16" type="checkbox" name="industry_type_etc16" value="16"{if "16" == $rowdata["industry_type_etc16"]} checked="checked"{/if}>
						<label for="industry_type_etc16">小売業(繊維・衣服・身の回り品)</label>
						<input class="is-checkradio" id="industry_type_etc17" type="checkbox" name="industry_type_etc17" value="17"{if "17" == $rowdata["industry_type_etc17"]} checked="checked"{/if}>
						<label for="industry_type_etc17">小売業(飲食品（コンビニエンスストア等）)</label>
						<input class="is-checkradio" id="industry_type_etc18" type="checkbox" name="industry_type_etc18" value="18"{if "18" == $rowdata["industry_type_etc18"]} checked="checked"{/if}>
						<label for="industry_type_etc18">小売業(機会器具小売業)</label>
						<input class="is-checkradio" id="industry_type_etc19" type="checkbox" name="industry_type_etc19" value="19"{if "19" == $rowdata["industry_type_etc19"]} checked="checked"{/if}>
						<label for="industry_type_etc19">小売業(その他)</label>

					<br>【学術研究, 専門・技術サービス業】<br>	
						<input class="is-checkradio" id="industry_type_etc20" type="checkbox" name="industry_type_etc20" value="20"{if "20" == $rowdata["industry_type_etc20"]} checked="checked"{/if}>
						<label for="industry_type_etc20">学術研究，専門・技術サービス業(学術・開発研究機関)</label>
						<input class="is-checkradio" id="industry_type_etc21" type="checkbox" name="industry_type_etc21" value="21"{if "21" == $rowdata["industry_type_etc21"]} checked="checked"{/if}>
						<label for="industry_type_etc21">学術研究，専門・技術サービス業(専門サービス業（他に分類されていないもの）)</label>
						<input class="is-checkradio" id="industry_type_etc22" type="checkbox" name="industry_type_etc22" value="22"{if "22" == $rowdata["industry_type_etc22"]} checked="checked"{/if}>
						<label for="industry_type_etc22">学術研究，専門・技術サービス業(広告業)</label>
						<input class="is-checkradio" id="industry_type_etc23" type="checkbox" name="industry_type_etc23" value="23"{if "23" == $rowdata["industry_type_etc23"]} checked="checked"{/if}>
						<label for="industry_type_etc23">学術研究，専門・技術サービス業(技術サービス業（他に分類されていないもの）)</label>

					<br>【医療・福祉業】<br>
						<input class="is-checkradio" id="industry_type_etc24" type="checkbox" name="industry_type_etc24" value="24"{if "24" == $rowdata["industry_type_etc24"]} checked="checked"{/if}>
						<label for="industry_type_etc24">医療・福祉業(医療業)</label>
						<input class="is-checkradio" id="industry_type_etc25" type="checkbox" name="industry_type_etc25" value="25"{if "25" == $rowdata["industry_type_etc25"]} checked="checked"{/if}>
						<label for="industry_type_etc25">医療・福祉業(保健衛生)</label>
						<input class="is-checkradio" id="industry_type_etc26" type="checkbox" name="industry_type_etc26" value="26"{if "26" == $rowdata["industry_type_etc26"]} checked="checked"{/if}>
						<label for="industry_type_etc26">医療・福祉業(社会保険・社会福祉・介護事業)</label>

					<br>【その他】<br>
						<input class="is-checkradio" id="industry_type_etc27" type="checkbox" name="industry_type_etc27" value="27"{if "27" == $rowdata["industry_type_etc27"]} checked="checked"{/if}>
						<label for="industry_type_etc27">農林業</label>
						<input class="is-checkradio" id="industry_type_etc28" type="checkbox" name="industry_type_etc28" value="28"{if "28" == $rowdata["industry_type_etc28"]} checked="checked"{/if}>
						<label for="industry_type_etc28">漁業</label>
						<input class="is-checkradio" id="industry_type_etc29" type="checkbox" name="industry_type_etc29" value="29"{if "29" == $rowdata["industry_type_etc29"]} checked="checked"{/if}>
						<label for="industry_type_etc29">鉱業，採石業，砂利採取業</label>
						<input class="is-checkradio" id="industry_type_etc30" type="checkbox" name="industry_type_etc30" value="30"{if "30" == $rowdata["industry_type_etc30"]} checked="checked"{/if}>
						<label for="industry_type_etc30">建設業</label>
						<input class="is-checkradio" id="industry_type_etc31" type="checkbox" name="industry_type_etc31" value="31"{if "31" == $rowdata["industry_type_etc31"]} checked="checked"{/if}>
						<label for="industry_type_etc31">電気・ガス・熱供給・水道業</label>
						<input class="is-checkradio" id="industry_type_etc32" type="checkbox" name="industry_type_etc32" value="32"{if "32" == $rowdata["industry_type_etc32"]} checked="checked"{/if}>
						<label for="industry_type_etc32">情報通信業</label>
						<input class="is-checkradio" id="industry_type_etc33" type="checkbox" name="industry_type_etc33" value="33"{if "33" == $rowdata["industry_type_etc33"]} checked="checked"{/if}>
						<label for="industry_type_etc33">運輸・信書便事業</label>
						<input class="is-checkradio" id="industry_type_etc34" type="checkbox" name="industry_type_etc34" value="34"{if "34" == $rowdata["industry_type_etc34"]} checked="checked"{/if}>
						<label for="industry_type_etc34">金融・保険業</label>
						<input class="is-checkradio" id="industry_type_etc35" type="checkbox" name="industry_type_etc35" value="35"{if "35" == $rowdata["industry_type_etc35"]} checked="checked"{/if}>
						<label for="industry_type_etc35">不動産・物品賃貸業</label>
						<input class="is-checkradio" id="industry_type_etc36" type="checkbox" name="industry_type_etc36" value="36"{if "36" == $rowdata["industry_type_etc36"]} checked="checked"{/if}>
						<label for="industry_type_etc36">宿泊業</label>
						<input class="is-checkradio" id="industry_type_etc37" type="checkbox" name="industry_type_etc37" value="37"{if "37" == $rowdata["industry_type_etc37"]} checked="checked"{/if}>
						<label for="industry_type_etc37">飲食サービス業 </label>
						<input class="is-checkradio" id="industry_type_etc38" type="checkbox" name="industry_type_etc38" value="38"{if "38" == $rowdata["industry_type_etc38"]} checked="checked"{/if}>
						<label for="industry_type_etc38">生活関連サービス（理容・美容等）・娯楽業</label>
						<input class="is-checkradio" id="industry_type_etc39" type="checkbox" name="industry_type_etc39" value="39"{if "39" == $rowdata["industry_type_etc39"]} checked="checked"{/if}>
						<label for="industry_type_etc39">学校教育</label>
						<input class="is-checkradio" id="industry_type_etc40" type="checkbox" name="industry_type_etc40" value="40"{if "40" == $rowdata["industry_type_etc40"]} checked="checked"{/if}>
						<label for="industry_type_etc40">その他の教育，学習支援業</label>
						<input class="is-checkradio" id="industry_type_etc41" type="checkbox" name="industry_type_etc41" value="41"{if "41" == $rowdata["industry_type_etc41"]} checked="checked"{/if}>
						<label for="industry_type_etc41">職業紹介・労働者派遣業</label>
						<input class="is-checkradio" id="industry_type_etc42" type="checkbox" name="industry_type_etc42" value="42"{if "42" == $rowdata["industry_type_etc42"]} checked="checked"{/if}>
						<label for="industry_type_etc42">複合サービス事業（郵便局，農林水産業協同組合，事業協同組合（他に分類されないもの））</label>
						<input class="is-checkradio" id="industry_type_etc43" type="checkbox" name="industry_type_etc43" value="43"{if "43" == $rowdata["industry_type_etc43"]} checked="checked"{/if}>
						<label for="industry_type_etc43">その他の事業サービス業（速記・ワープロ入力・複写業，建物サービス業，警備業等）</label>
						<input class="is-checkradio" id="industry_type_etc44" type="checkbox" name="industry_type_etc44" value="44"{if "44" == $rowdata["industry_type_etc44"]} checked="checked"{/if}>
						<label for="industry_type_etc44">その他のサービス業</label>
						<input class="is-checkradio" id="industry_type_etc45" type="checkbox" name="industry_type_etc45" value="45"{if "45" == $rowdata["industry_type_etc45"]} checked="checked"{/if}>
						<label for="industry_type_etc45">宗教</label>
						<input class="is-checkradio" id="industry_type_etc46" type="checkbox" name="industry_type_etc46" value="46"{if "46" == $rowdata["industry_type_etc46"]} checked="checked"{/if}>
						<label for="industry_type_etc46">公務（他に分類されていないもの）</label>
						<input class="is-checkradio" id="industry_type_etc47" type="checkbox" name="industry_type_etc47" value="47"{if "47" == $rowdata["industry_type_etc47"]} checked="checked"{/if}>
						<label for="industry_type_etc47">分類不能の産業</label>


				</td>
			</tr>

			<tr>
				<th>製造業(その他) 内容</th>
				<td>
					<input class="input" type="text" name="manufacturing_other" id= "manufacturing_other" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["manufacturing_other"]|escape}">
				</td>
			</tr>

			<tr>
				<th>卸売業(その他) 内容</th>
				<td>
					<input class="input" type="text" name="wholesale_other" id= "wholesale_other" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["wholesale_other"]|escape}">
				</td>
			</tr>

			<tr>
				<th>小売業(その他)　内容</th>
				<td>
					<input class="input" type="text" name="retail_other" id= "retail_other" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["retail_other"]|escape}">
				</td>
			</tr>

			<tr>
				<th>その他のサービス業 内容</th>
				<td>
					<input class="input" type="text" name="service_other" id= "service_other" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["service_other"]|escape}">
				</td>
			</tr>

			<tr>
				<th>分類不能の産業 内容</th>
				<td>
					<input class="input" type="text" name="industry_other" id= "industry_other" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["industry_other"]|escape}">
				</td>
			</tr>

			<tr>
				<th>資本金</th>
				<td>
					<input class="input" type="text" name="capital" id= "capital" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["capital"]|escape}">
				</td>
			</tr>

			<tr>
				<th>年間売上金額（直近年度）</th>
				<td>
					<input class="input" type="text" name="annual_sales_amount" id= "annual_sales_amount" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["annual_sales_amount"]|escape}">
				</td>
			</tr>
			<tr>
				<th>常勤職員数</th>
				<td>
					<input class="input" type="text" name="company_stuff_count" id= "company_stuff_count" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["company_stuff_count"]|escape}">
				</td>
			</tr>


			<tr>
				<th>所属協議会</th>
				<td>
					<input class="input" type="text" name="council_id" id= "council_id" placeholder="{$labels["placeholder"]|escape}" value="{$rowdata["council_id"]|escape}">
				</td>
			</tr>

			<tr>
				<th>
					協議会申請書<br>
					提出ステータス
				</th>
				<td>
					<div class="field has-width-100pct">
						<input class="is-checkradio" id="council_apply_status_1" type="radio" name="council_apply_status" value="1" {if $rowdata["council_apply_status"] == "1"} checked="checked"{/if}>
						<label for="council_apply_status_1">提出済み</label>
						<input class="is-checkradio" id="council_apply_status_2" type="radio" name="council_apply_status" value="2" {if $rowdata["council_apply_status"] == "2"} checked="checked"{/if}>
						<label for="council_apply_status_2">未提出</label>
					</div>
				</td>
			</tr>

		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark return-button" id="return-button">{$labels["back"]|escape}</button>
			</div>
			<div class="column is-6 has-text-right">
				<button type="button" class="button is-primary" id="regist_button">{$labels["regist"]|escape}</button>
			</div>
		</div>

	</div> <!-- /.contents -->

	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
	</form>
</body>
</html>
