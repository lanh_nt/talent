<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/CM01/CM01002.js"></script>

</head>

<body id="CM01002">
	<form id="mainForm" enctype="multipart/form-data" method="POST" action="CM01002">
	<input type="hidden" name="mode" id="mode" />
	<input type="hidden" name="cm_id" id="cm_id" value="{$items["cm_id"]|escape}" />

	<input type="hidden" name="page_no" id="page_no" value="{$items["page_no"]|escape}"  />
	<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}"  />

	<input type="hidden" name="C_01" id="C_01" value="{$items["C_01"]|escape}"  />
	<input type="hidden" name="C_02" id="C_02" value="{$items["C_02"]|escape}"  />
	<input type="hidden" name="C_03" id="C_03" value="{$items["C_03"]|escape}"  />
	<input type="hidden" name="C_04_1" id="C_04_1" value="{$items["C_04_1"]|escape}"  />
	<input type="hidden" name="C_04_2" id="C_04_2" value="{$items["C_04_2"]|escape}"  />
	<input type="hidden" name="C_04_3" id="C_04_3" value="{$items["C_04_3"]|escape}"  />
	<input type="hidden" name="C_05_1" id="C_05_1" value="{$items["C_05_1"]|escape}"  />
	<input type="hidden" name="C_05_2" id="C_05_2" value="{$items["C_05_2"]|escape}"  />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">企業管理/詳細</h2>

		<table class="table is-bordered has-width-100pct">
			<tr>
				<th>種別</th>
				<td>{$rowdata["classification"]|escape}</td>
			</tr>
			<tr>
				<th>企業ID</th>
				<td>{$rowdata["company_id"]|escape}</td>
			</tr>
			<tr>
				<th>氏名又は名称(ふりがな)</th>
				<td>{$rowdata["company_kana"]|escape}</td>
			</tr>

			<tr>
				<th>氏名又は名称</th>
				<td>{$rowdata["company_name"]|escape}</td>
			</tr>

			<tr>
				<th>法人番号</th>
				<td>{$rowdata["corporation_no"]|escape}</td>
			</tr>
			<tr>
				<th>郵便番号（所在地）</th>
				<td>{$rowdata["postal_code"]|escape}</td>
				</td>
			</tr>

			<tr>
				<th>住所（所在地）</th>
				<td>{$rowdata["address"]|escape}</td>
			</tr>

			<tr>
				<th>電話番号</th>
				<td>{$rowdata["tel"]|escape}</td>
			</tr>
			<tr>
				<th>代表者の氏名（ふりがな）</th>
				<td>{$rowdata["representative_kana"]|escape}</td>
			</tr>
			<tr>
				<th>代表者の氏名</th>
				<td>{$rowdata["representative_name"]|escape}</td>
			</tr>
			<tr>
				<th>登録番号(許可 &middot; 届出番号)</th>
				<td>{$rowdata["registration_no"]|escape}</td>
			</tr>

			<tr>
				<th>
					登録年月日<br>(許可 &middot; 届出番号
					受理年月日)
				</th>
				<td>{$rowdata["registration_date"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 氏名1</th>
				<td>{$rowdata["supervisor_name1"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 所属部署1</th>
				<td>{$rowdata["supervisor_department1"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 役職1</th>
				<td>{$rowdata["supervisor_position1"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 氏名2</th>
				<td>{$rowdata["supervisor_name2"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 所属部署2</th>
				<td>{$rowdata["supervisor_department2"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 役職2</th>
				<td>{$rowdata["supervisor_position2"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 氏名3</th>
				<td>{$rowdata["supervisor_name3"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 所属部署3</th>
				<td>{$rowdata["supervisor_department3"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 役職3</th>
				<td>{$rowdata["supervisor_position3"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 氏名4</th>
				<td>{$rowdata["supervisor_name4"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 所属部署4</th>
				<td>{$rowdata["supervisor_department4"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 役職4</th>
				<td>{$rowdata["supervisor_position4"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 氏名5</th>
				<td>{$rowdata["supervisor_name5"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 所属部署5</th>
				<td>{$rowdata["supervisor_department5"]|escape}</td>
			</tr>
			<tr>
				<th>所属機関監督者 役職5</th>
				<td>{$rowdata["supervisor_position5"]|escape}</td>
			</tr>
			<tr>
				<th>支援業務を開始する予定年月日</th>
				<td>{$rowdata["support_scheduled_date"]|escape}</td>
			</tr>
			<tr>
				<th>支援を行う事業所の名称</th>
				<td>{$rowdata["support_company_name"]|escape}</td>
			</tr>
			
			<tr>
				<th>支援を行う事業所 郵便番号</th>
				<td>{$rowdata["support_postal_code"]|escape}</td>
				</td>
			</tr>

			<tr>
				<th>支援を行う事業所 住所</th>
				<td>{$rowdata["support_address"]|escape}</td>
				</td>
			</tr>

			<tr>
				<th>支援を行う事業所  電話番号</th>
				<td>{$rowdata["support_tel"]|escape}</td>
				</td>
			</tr>
			<tr>
				<th>支援責任者名(ふりがな)</th>
				<td>{$rowdata["support_manager_kana"]|escape}</td>
			</tr>
			<tr>
				<th>支援責任者名(氏名)</th>
				<td>{$rowdata["support_manager_name"]|escape}</td>
			</tr>
			<tr>
				<th>支援責任者 役職</th>
				<td>{$rowdata["support_manager_department"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者名(ふりがな)</th>
				<td>{$rowdata["support_handler_kana"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者名(氏名)</th>
				<td>{$rowdata["support_handler_name"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者 役職</th>
				<td>{$rowdata["support_handler_department"]|escape}</td>
			</tr>
			<tr>
				<th>対応可能言語</th>
				<td>{$rowdata["available_language"]|escape}</td>
			</tr>
			<tr>
				<th>
					支援を行っている<br>
					1号特定技能外国人数
				</th>
				<td>{$rowdata["worker_mumber"]|escape}</td>
			</tr>
			<tr>
				<th>支援担当者数</th>
				<td>{$rowdata["support_stuff_number"]|escape}</td>
			</tr>
			<tr>
				<th>業種</th>
				<td>{$rowdata["industry_type"]|escape}</td>
			</tr>
			<tr>
				<th>他に業種</th>
				<td>{$rowdata["industry_type_etc"]|escape}</td>
			</tr>

			<tr>
				<th>製造業(その他) 内容</th>
				<td>{$rowdata["manufacturing_other"]|escape}</td>
			</tr>

			<tr>
				<th>卸売業(その他) 内容</th>
				<td>{$rowdata["wholesale_other"]|escape}</td>
			</tr>

			<tr>
				<th>小売業(その他)　内容</th>
				<td>{$rowdata["retail_other"]|escape}</td>
			</tr>

			<tr>
				<th>その他のサービス業 内容</th>
				<td>{$rowdata["service_other"]|escape}</td>
			</tr>

			<tr>
				<th>分類不能の産業 内容</th>
				<td>{$rowdata["industry_other"]|escape}</td>
			</tr>

			<tr>
				<th>資本金</th>
				<td>{$rowdata["capital"]|escape}</td>
			</tr>
			<tr>
				<th>年間売上金額（直近年度）</th>
				<td>{$rowdata["annual_sales_amount"]|escape}</td>
			</tr>
			<tr>
				<th>常勤職員数</th>
				<td>{$rowdata["company_stuff_count"]|escape}</td>
			</tr>

			<tr>
				<th>所属協議会</th>
				<td>{$rowdata["council_id"]|escape}</td>
			</tr>

			<tr>
				<th>
					協議会申請書<br>
					提出ステータス
				</th>
				<td>{$rowdata["council_apply_status"]|escape}</td>
			</tr>

		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark return-button" id="return-button">{$labels["back"]|escape}</button>
			</div>
{if $smarty.session.loginUserAuth <= '2'}
			<div class="column is-6 has-text-right">
				<button type="button" class="button is-primary" id="udate_button">{$labels["edit"]|escape}</button>
			</div>
{/if}
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
	</form>
</body>
</html>
