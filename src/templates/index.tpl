<!DOCTYPE html>
<html lang="ja" prefix="og: http://ogp.me/ns#">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="./favicon.ico">

<link rel="stylesheet" type="text/css" href="./assets/css/style.css">
<script type="text/javascript" src="./assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="./assets/js/flatpickr.js"></script>
<script type="text/javascript" src="./assets/js/modaal.min.js"></script>
<script type="text/javascript" src="./assets/js/functions.js"></script>
<script type="text/javascript" src="./js/common.js"></script>
<script type="text/javascript" src="./js/index.js"></script>

<meta property="og:url" content="https://talent-asia.jp/" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{$labels["menu-title"]|escape}" />
<meta property="og:image" content="https://talent-asia.jp/assets/img/logo.png" />
<meta property="og:site_name" content="{$labels["menu-title"]|escape}" />
<meta property="og:description" content="{$labels["L-description"]|escape}" />

</head>

<body id="C-LG01001">
<form id="mainForm" method="POST" action="index">
<input type="hidden" name="mode" id="mode" />

	<!-- メインコンテンツ -->
	<div class="contents">

		<div class="columns has-margin-10">
			<div class="column is-8 is-offset-2">
				<div class="box has-margin-top-50">
					<h2 class="has-text-centered title is-4">{$labels["L-title"]|escape}</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

					<div class="has-margin-top-80">
						<div class="is-relative">
							<p class='login__text'>{$labels["L-01"]|escape}</p>
						</div>
						<input class="input" type="text" name="loginId" id="loginId" value="{$items.loginId|escape}">
					</div>

					<div class="has-text-right has-margin-top-80">
						<button type="button" class="button is-dark submit-button" id="next_button">{$labels["next"]|escape}</button>
					</div>

					<div class="help__block">
						<h3 class="has-text-centered title is-5">-HELP-</h3>
{foreach from=$helpItems item=$row}
						<p class="is-font-bold has-margin-top-20">{$row["event"]|nl2br}</p>
{if $row["site_name"] != ""}
						<a href="{$row["site_url"]}" target="_blank">{$row["site_name"]|escape}</a>
{/if}
{/foreach}
					</div>
				</div>
			</div>
		</div>

	</div> <!-- ./contents -->

</form>
</body>
</html>
