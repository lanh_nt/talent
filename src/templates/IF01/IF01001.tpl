<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/IF01/IF01001.js"></script>
</head>

<body id="IF01001">
<form id="mainForm" method="POST" action="IF01001">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}" />
<input type="hidden" name="info_id" id="info_id" />
	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{$labels["L-title"]|escape}</h2>

{* --- メッセージ欄部品 --- *}
{include file='common/message.tpl' messages=$messages}

{if $smarty.session.loginUserType == '2'}
		<input type="hidden" name="C_01" id="C_01" value="{$items["C_01"]|escape}">
{/if}
{if $smarty.session.loginUserType == '1'}
{if $smarty.session.loginUserAuth <= '2'}
		<div class="has-text-right has-margin-bottom-20">
			<button type="button" class="button is-primary" id="insert_button">{$labels["new_create"]|escape}</button>
		</div>
{/if}

		<div class="box has-padding-20">
			<div class="columns is-multiline formbox-gap">
				<!-- ID -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">労働者ID：</p>
				</div>
				<div class="column is-9">
					<input class="input" type="text" name="C_01" id="C_01" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_01"]|escape}">
				</div>

				<!-- 労働者氏名 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">労働者氏名：</p>
				</div>
				<div class="column is-9">
					<input class="input" type="text" name="C_02" id="C_02" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_02"]|escape}">
				</div>

				<!-- 在留資格 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">区分：</p>
				</div>
				<div class="column is-9">
					<div class="field">
						<input class="is-checkradio" type="checkbox" name="C_03_1" id="C_03_1" {if $items["C_03_1"] != ""}checked="checked"{/if}>
						<label for="C_03_1">留学生</label>
						<input class="is-checkradio" type="checkbox" name="C_03_2" id="C_03_2" {if $items["C_03_2"] != ""}checked="checked"{/if}>
						<label for="C_03_2">特定技能1号</label>
						<input class="is-checkradio" type="checkbox" name="C_03_3" id="C_03_3" {if $items["C_03_3"] != ""}checked="checked"{/if}>
						<label for="C_03_3">特定技能2号</label>
						<input class="is-checkradio" type="checkbox" name="C_03_4" id="C_03_4" {if $items["C_03_4"] != ""}checked="checked"{/if}>
						<label for="C_03_4">技人国</label>
						<input class="is-checkradio" type="checkbox" name="C_03_5" id="C_03_5" {if $items["C_03_5"] != ""}checked="checked"{/if}>
						<label for="C_03_5">国外在留者</label>
						<input class="is-checkradio" type="checkbox" name="C_03_6" id="C_03_6" {if $items["C_03_6"] != ""}checked="checked"{/if}>
						<label for="C_03_6">その他</label>
						<div class="select is-small">
							<select name="C_03_etc" id="C_03_etc">
								<option value=""></option>
{foreach from=$purposes key=$pkey item=$pname}
								<option value="{$pkey}"{if $pkey == $items["C_03_etc"]} selected="selected"{/if}>{$pname}</option>
{/foreach}
							</select>
						</div>
					</div>
				</div>

				<!-- 企業名 -->
{if $smarty.session.loginCompanyType != '2'}
				<input type="hidden" name="C_04" id="C_04" value="{$smarty.session.loginCompanyRowId|escape}">
{else}
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">企業名：</p>
				</div>
				<div class="column is-9">
					<div class="select has-vertical-middle has-width-100pct">
						<select name="C_04" id="C_04" class="has-width-100pct">
							<option value=""></option>
{foreach from=$companies item=$company}
							<option value="{$company["id"]}"{if $company["id"] == $items["C_04"]} selected="selected"{/if}>{$company["name"]}</option>
{/foreach}
						</select>
					</div>
				</div>
{/if}

				<!-- 配信者名 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">配信者名：</p>
				</div>
				<div class="column is-9">
					<input class="input" type="text" name="C_05" id="C_05" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_05"]|escape}">
				</div>

				<!-- 件名 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">件名：</p>
				</div>
				<div class="column is-9">
					<input class="input" type="text" name="C_06" id="C_06" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_06"]|escape}">
				</div>

				<!-- 配信日 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">配信日：</p>
				</div>
				<div class="column is-9">
					<input id="C_07_1" name="C_07_1" data-mindate="today" type="text" class="input has-width-max40pct is-inline-block flatpickr" value="{$items["C_07_1"]|escape}" required />
					<span class="has-vertical-middle">～</span>
					<input id="C_07_2" name="C_07_2" data-mindate="today" type="text" class="input has-width-max40pct is-inline-block flatpickr" value="{$items["C_07_2"]|escape}" required />
				</div>

				<!-- 配信言語 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">配信言語：</p>
				</div>
				<div class="column is-9">
					<div class="field">
						<input class="is-checkradio" type="checkbox" name="C_08_1" id="C_08_1" {if $items["C_08_1"] != ""}checked="checked"{/if}>
						<label for="C_08_1">英語</label>
						<input class="is-checkradio" type="checkbox" name="C_08_2" id="C_08_2" {if $items["C_08_2"] != ""}checked="checked"{/if}>
						<label for="C_08_2">ベトナム語</label>
					</div>
				</div>
			</div><!--  /.columns -->

			<div class="has-text-centered has-margin-top-20">
				<a class="button is-info submit-button" id="search_button">{$labels["select"]|escape}</a>
			</div>
		</div>
{/if}{* --- END loginUserType == '1' --- *}

{if isset($pageData->pageDatas)}
{if count($pageData->pageDatas) == 0}
		<div class="align-center">{$labels["no_records"]|escape}</div>
{else}
		<input type="hidden" name="page_no" id="page_no" value="{$pageData->currentPageNo}">
		<table class="table has-margin-top-50 has-width-100pct responsive-list">
			<thead>
				<tr>
					<th>{$labels["L-01-01"]|escape}</th>
					<th>{$labels["L-01-02"]|escape}</th>
					<th>{$labels["L-01-03"]|escape}</th>
					<th>{$labels["L-01-04"]|escape}</th>
					<th>{$labels["L-01-05"]|escape}</th>
					<th>{$labels["L-01-06"]|escape}</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>{$labels["L-01-01"]|escape}</th>
					<th>{$labels["L-01-02"]|escape}</th>
					<th>{$labels["L-01-03"]|escape}</th>
					<th>{$labels["L-01-04"]|escape}</th>
					<th>{$labels["L-01-05"]|escape}</th>
					<th>{$labels["L-01-06"]|escape}</th>
				</tr>
			</tfoot>
			<tbody>
{foreach from=$pageData->pageDatas item=$row}
				<tr>
					<td>{$row["dday"]|escape}</td>
					<td>{$row["worker_name"]|escape|nl2br}</td>
					<td>{$row["subject"]|escape}</td>
					<td>{$row["msg_body"]|escape|nl2br}</td>
					<td>{$row["sender_name"]|escape}</td>
					<td>
						<button type="button" class="button is-dark detail-button" id="detail_button_{$row["info_id"]|escape}">{$labels["contents"]|escape}</button>
					</td>
				</tr>
{/foreach}
			</tbody>
		</table>

{* --- ページング部品 --- *}
{include file='common/pager.tpl' pageData=$pageData}

{/if}{* --- END count(pageDatas) --- *}
{/if}{* --- END isset(pageDatas) --- *}

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
