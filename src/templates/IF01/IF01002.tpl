<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/IF01/IF01002.js"></script>
</head>

<body id="IF01002">
<form id="mainForm" method="POST" action="IF01002">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="csrf_token" value="{$smarty.session["csrf_token"]}" />
	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">お知らせ管理/新規登録</h2>

{* --- メッセージ欄部品 --- *}
{include file='common/message.tpl' messages=$messages}

		<table class="table is-bordered has-width-100pct">
			<!-- 配信日 -->
			<tr>
				<th>配信日</th>
				<td colspan="2">{$items["D_1"]|escape}<input type="hidden" name="D_1" value="{$items["D_1"]|escape}"></td>
			</tr>

			<!-- 配信言語 -->
			<tr>
				<th>配信言語</th>
				<td colspan="2">
					<div class="select has-vertical-middle">
						<select name="D_2" id="D_2">
							<option value="">選択必須です</option>
							<option value="2"{if $items["D_2"] == "2"} selected="selected"{/if}>英語</option>
							<option value="3"{if $items["D_2"] == "3"} selected="selected"{/if}>ベトナム語</option>
						</select>
					</div>
				</td>
			</tr>

			<!-- 企業名 -->
{if $smarty.session.loginCompanyType != '2'}
			<input type="hidden" name="D_3" id="D_3" value="{$smarty.session.loginCompanyRowId|escape}">
{else}
			<tr>
				<th>企業名</th>
				<td colspan="2">
					<div class="select has-vertical-middle">
						<select name="D_3" id="D_3">
							<option value="">選択必須です</option>
{foreach from=$companies item=$company}
							<option value="{$company["id"]}"{if $company["id"] == $items["D_3"]} selected="selected"{/if}>{$company["name"]}</option>
{/foreach}
						</select>
						</select>
					</div>
				</td>
			</tr>
{/if}

			<!-- 宛先 -->
			<tr>
				<th>宛先</th>
				<td colspan="2">
					<a id="d4openbutton" href="#d4modal" data-modaal-type="inline" class="button modaal is-primary" data-modaal-scope="modaal_15772668525016faa178e6d695">選択</a>
					<a id="d4dummybutton" href="#" disabled="disabled" class="button is-primary is-hidden">選択</a>
					<div id="d4sts" style="display: inline-block; margin-top:6px; margin-left:18px;"></div>
				</td>
			</tr>

			<!-- 件名 -->
			<tr>
				<th rowspan="2" class="has-vertical-top">件名</th>
				<th>日本語</th>
				<td><input class="input" type="text" name="D_5_1" id="D_5_1" placeholder="{$labels["placeholder"]|escape}" value="{$items["D_5_1"]|escape}"></td>
			</tr>
			<tr>
				<th>母国語</th>
				<td><input class="input" type="text" name="D_5_2" id="D_5_2" placeholder="{$labels["placeholder"]|escape}" value="{$items["D_5_2"]|escape}"></td>
			</tr>

			<!-- 本文 -->
			<tr>
				<th rowspan="2" class="has-vertical-top">本文</th>
				<th class="has-vertical-top">日本語</th>
				<td><textarea class="textarea" name="D_6_1" id="D_6_1" placeholder="{$labels["placeholder"]|escape}">{$items["D_6_1"]|escape}</textarea></td>
			</tr>
			<tr>
				<th class="has-vertical-top">母国語</th>
				<td><textarea class="textarea" name="D_6_2" id="D_6_2" placeholder="{$labels["placeholder"]|escape}">{$items["D_6_2"]|escape}</textarea></td>
			</tr>
		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
			</div>
			<div class="column is-6 has-text-right">
				<button type="button" class="button is-primary" id="regist_button">{$labels["regist"]|escape}</button>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- 配信対象者検索用モーダル -->
	<div id="d4modal" style="display:none;">
		<p>配信対象者を選択してください。</p>

		<div class="has-margin-top-30">
			<input type="radio" class="is-checkradio" name="D_4_1" id="D_4_1_all" value="1"{if $items["D_4_1"] == "1"} checked="checked"{/if}>
			<label for="D_4_1_all">全員に配信する</label>
		</div>

		<div class="has-margin-top-20">
			<input type="radio" class="is-checkradio" name="D_4_1" id="D_4_1_sep" value="2"{if $items["D_4_1"] == "2"} checked="checked"{/if}>
			<label for="D_4_1_sep">個人名から選択する</label>
		</div>

		<section class="has-margin-top-10">
			<ul class="info__selectlist">
				<li>
					<p class="is-font-bold has-padding-left-30">労働者ID：名前</p>
				</li>
{foreach from=$workers item=$worker}
				<li>
					<input id="D_4_2_{$worker["id"]}" name="D_4_2[]" type="checkbox" class="is-checkradio worker-radio" value="{$worker["id"]}"{if is_array($items["D_4_2"]) and in_array($worker["id"], $items["D_4_2"])} checked="checked"{/if}>
					<label id="D_4_2_{$worker["id"]}_L" for="D_4_2_{$worker["id"]}">{$worker["name"]}</label>
				</li>
{/foreach}
			</ul>
		</section>

		<div class="has-text-centered has-margin-top-50">
			<button type="button" id="d4okbutton" class="button is-primary">決定</button>
		</div>
	</div>

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>

<script>
$('.modaal').modaal({
	overlay_close: false, // 背景押下時にクローズさせるか
	before_open: d4OpenEvent,
	after_close: d4QuitEvent
});
</script>

</form>
</body>
</html>