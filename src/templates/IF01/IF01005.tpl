<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/IF01/IF01005.js"></script>
</head>

<body id="IF01005">
<form id="mainForm" method="POST" action="IF01001">
<input type="hidden" name="mode" id="mode" />
	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{$labels["L-title"]|escape}</h2>

		<table class="table table-conf is-bordered has-width-100pct">
			<!-- 配信日 -->
			<tr>
				<th>{$labels["L-01-01-01"]|escape}</th>
				<td>{$items["dday"]|escape}</td>
				<th>{$labels["L-01-01-02"]|escape}</th>
				<td>{$items["sender_name"]|escape}</td>
			</tr>

			<!-- 配信言語 -->
			<tr>
				<th>{$labels["L-01-02"]|escape}</th>
				<td colspan="3">{$items["delivery_language_string"]|escape}</td>
			</tr>

			<!-- 宛先 -->
			<tr>
				<th>{$labels["L-01-03"]|escape}</th>
				<td colspan="3">{$items["worker_name"]|escape|nl2br}</td>
			</tr>

			<!-- 件名 -->
			<tr>
				<th rowspan="2" class="has-vertical-top">{$labels["L-01-04"]|escape}</th>
				<th>{$labels["L-01-04-01"]|escape}</th>
				<td colspan="2">{$items["subject_jp"]|escape}</td>
			</tr>
			<tr>
				<th>{$labels["L-01-04-02"]|escape}</th>
				<td colspan="2">{$items["subject_nv"]|escape}</td>
			</tr>

			<!-- 本文 -->
			<tr>
				<th rowspan="2" class="has-vertical-top">{$labels["L-01-05"]|escape}</th>
				<th class="has-vertical-top">{$labels["L-01-05-01"]|escape}</th>
				<td colspan="2">
					{$items["msg_body_jp"]|escape|nl2br}
				</td>
			</tr>
			<tr>
				<th class="has-vertical-top">{$labels["L-01-05-02"]|escape}</th>
				<td colspan="2">
					{$items["msg_body_nv"]|escape|nl2br}
				</td>
			</tr>
		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
			</div>
		</div>


	</div> <!-- /.contents -->


	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
