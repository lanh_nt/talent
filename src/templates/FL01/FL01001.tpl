<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/FL01/FL01001.js"></script>
</head>

<body>
<form id="mainForm" method="POST" action="FL01001">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}" />
<input type="hidden" name="file_rowid" id="file_rowid" />
<input type="hidden" name="csrf_token" value="{$smarty.session["csrf_token"]}" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{$labels["L-title"]|escape}</h2>

{* --- メッセージ欄部品 --- *}
{include file='common/message.tpl' messages=$messages}

{if $smarty.session.loginUserAuth <= '2'}
		<div class="has-text-right has-margin-bottom-20">
			<button type="button" class="button is-primary" id="upload_button">{$labels["upload"]|escape}</button>
		</div>
{/if}

		<div class="box has-padding-20">
			<div class="columns is-multiline formbox-gap">
				<!-- 企業ID -->
				<input type="hidden" name="C_company" id="C_company" value="{$items["C_company"]|escape}">
				<!-- 労働者ID -->
				<input type="hidden" name="C_worker" id="C_worker" value="{$items["C_worker"]|escape}">

				<!-- ファイル名 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">{$labels["L-01-01"]|escape}:</p>
				</div>
				<div class="column is-9">
					<input name="C_01" id="C_01" value="{$items["C_01"]|escape}"
					       class="input" type="text" placeholder="{$labels["placeholder"]|escape}">
				</div>

				<!-- 登録者名 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">登録者名：</p>
				</div>
				<div class="column is-9">
					<input name="C_02" id="C_02" value="{$items["C_02"]|escape}"
					       class="input" type="text" placeholder="{$labels["placeholder"]|escape}">
				</div>

				<!-- 区分 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">{$labels["L-01-02"]|escape}：</p>
				</div>
				<div class="column is-9">
					<div class="field">
						<input class="is-checkradio" type="checkbox" name="C_03_1" id="C_03_1" {if $items["C_03_1"] != ""}checked="checked"{/if}>
						<label for="C_03_1">{$labels["L-01-02-01"]|escape}</label>
						<input class="is-checkradio" type="checkbox" name="C_03_2" id="C_03_2" {if $items["C_03_2"] != ""}checked="checked"{/if}>
						<label for="C_03_2">{$labels["L-01-02-02"]|escape}</label>
						<input class="is-checkradio" type="checkbox" name="C_03_3" id="C_03_3" {if $items["C_03_3"] != ""}checked="checked"{/if}>
						<label for="C_03_3">{$labels["L-01-02-03"]|escape}</label>
					</div>
				</div>

				<!-- 登録日 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">{$labels["L-01-03"]|escape}：</p>
				</div>
				<div class="column is-9">
					<input id="C_04_1" name="C_04_1" value="{$items["C_04_1"]|escape}" type="text" class="input has-width-max40pct is-inline-block flatpickr" placeholder="{$labels["placeholder_date"]|escape}"required />
					<span class="has-vertical-middle">～</span>
					<input id="C_04_2" name="C_04_2" value="{$items["C_04_2"]|escape}" type="text" class="input has-width-max40pct is-inline-block flatpickr is-back-white" placeholder="{$labels["placeholder_date"]|escape}" required />
				</div>

			</div><!--  /.columns -->

			<div class="has-text-centered has-margin-top-20">
				<a id="search_button" class="button is-info">{$labels["select"]|escape}</a>
			</div>
		</div>

{if isset($pageData->pageDatas)}
{if count($pageData->pageDatas) == 0}
		<div class="align-center">{$labels["no_records"]|escape}</div>
{else}
		<input type="hidden" name="page_no" id="page_no" value="{$pageData->currentPageNo}">
		<table class="table has-margin-top-50 has-width-100pct responsive-list">
			<thead>
				<tr>
					<th>{$labels["L-02-01"]|escape}</th>
					<th>{$labels["L-02-02"]|escape}</th>
					<th>{$labels["L-02-03"]|escape}</th>
					<th>{$labels["L-02-04"]|escape}</th>
					<th>{$labels["L-02-05"]|escape}</th>
{if $smarty.session.loginUserAuth <= '2'}
					<th></th>
{/if}
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>{$labels["L-02-01"]|escape}</th>
					<th>{$labels["L-02-02"]|escape}</th>
					<th>{$labels["L-02-03"]|escape}</th>
					<th>{$labels["L-02-04"]|escape}</th>
					<th>{$labels["L-02-05"]|escape}</th>
{if $smarty.session.loginUserAuth <= '2'}
					<th></th>
{/if}
				</tr>
			</tfoot>
			<tbody>
{foreach from=$pageData->pageDatas item=$row}
				<tr>
					<td>{$row["file_type_name"]|escape}</td>
					<td>{$row["upload_user_name"]|escape}</td>
					<td>{$row["upload_ymd"]|escape}</td>
					<td>{$row["file_comment"]|escape|nl2br}</td>
					<td>
						<button type="button" class="button is-dark detail-button"
						        id="detail_button_{$row["file_rowid"]|escape}">{$labels["contents"]|escape}</button>
					</td>
{if $smarty.session.loginUserAuth <= '2'}
					<td>
						<button type="button" class="button is-dark delete-button" data-modaal-type="delAlert" 
						        id="delete_button_{$row["file_rowid"]|escape}">{$labels["delete"]|escape}</button>
					</td>
{/if}
				</tr>
{/foreach}
			</tbody>
		</table>

{* --- ページング部品 --- *}
{include file='common/pager.tpl' pageData=$pageData}

{/if}{* --- END count(pageDatas) --- *}
{/if}{* --- END isset(pageDatas) --- *}

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>

	<!-- 削除確認 -->
	<div id="delAlert" style="display:none;">
		<h1 class="title is-4 has-text-centered">【{$labels["L-03"]|escape}】</h1>
		<div class="has-margin-top-30">
			<p class="has-text-centered">{$labels["L-03-01"]|escape}</p>

			<div class="columns is-mobile has-text-centered has-margin-top-20">
				<div class="column is-6 has-padding-30">
					<a class="inline_close button is-inverted">{$labels["cancel"]|escape}</a>
				</div>
				<div class="column is-6 has-padding-30">
					<button type="button" id="execDeleteButton" class="button is-danger">{$labels["ok"]|escape}</button>
				</div>
			</div>
		</div>
	</div>
	<a id="openDelDialog" href="#delAlert" class="modaal" style="display:none;"></a>

	<script>
	$('.modaal').modaal({
		overlay_close: false, // 背景押下時にクローズさせるか
	});
	$('.inline_close').click(function(){
		$('.modaal').modaal('close');
	});
	</script>
</form>
</body>
</html>
