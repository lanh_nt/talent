<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/FL01/FL01003.js"></script>
</head>

<body id="FL01003">
<form id="mainForm" enctype="multipart/form-data" method="POST" action="FL01003">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="csrf_token" value="{$smarty.session["csrf_token"]}" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{$labels["L-title"]|escape}</h2>

{* --- メッセージ欄部品 --- *}
{include file='common/message.tpl' messages=$messages}

		<table class="table is-bordered has-width-100pct">
			<!-- ファイル -->
			<tr>
				<th>{$labels["L-01-01"]|escape}</th>
				<td>
					<input type="file" name="D_1" id="D_1" style="display:none;" />
					<a id="D_1_button" href="#" class="button">{$labels["file_select"]|escape}</a>
					<span id="D_1_label" class="has-margin-left-10 has-vertical-middle"></span>
				</td>
			</tr>
			<!-- 対象者 -->
{if $smarty.session.loginUserType != '1'}
			<input type="hidden" name="D_2" value="{$items["D_2"]}" />
{else}
			<tr>
				<th>{$labels["L-01-02"]|escape}</th>
				<td colspan="2">
					<a id="d2openbutton" href="#d2modal" data-modaal-type="inline" class="button modaal is-primary" data-modaal-scope="modaal_15772668525016faa178e6d695">{$labels["L-01-02-01"]|escape}</a>
					<div id="d2sts" style="display: inline-block; margin-top:6px; margin-left:18px;"></div>
				</td>
			</tr>
{/if}
			<!-- コメント -->
			<tr>
				<th>{$labels["L-01-03"]|escape}</th>
				<td>
					<textarea name="D_3" id="D_3" class="textarea"
					          placeholder="{$labels["placeholder"]|escape}">{$items["D_3"]}</textarea>
				</td>
			</tr>
			<!-- 種別 -->
			<tr>
				<th>{$labels["L-01-04"]|escape}</th>
				<td>
					<div class="select has-vertical-middle">
						<select name="D_4" id="D_4">
							<option value=""></option>
{foreach from=$fileTypes key=id item=$name}
							<option value="{$id}"{if $id == $items["D_4"]} selected="selected"{/if}>{$name}</option>
{/foreach}
						</select>
					</div>
				</td>
			</tr>

		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
			</div>
			<div class="column is-6 has-text-right">
				<button type="button" class="button is-primary" id="regist_button">{$labels["regist"]|escape}</button>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- 対象者検索用モーダル -->
{if $smarty.session.loginUserType == '1'}
	<div id="d2modal" style="display:none;">
		<p>{$labels["L-02-01"]|escape}</p>

		<section class="has-margin-top-10">
			<ul class="info__selectlist">
				<li>
					<p class="is-font-bold has-padding-left-30">{$labels["L-02-02"]|escape}：{$labels["L-02-03"]|escape}</p>
				</li>
{foreach from=$workers item=$worker}
				<li>
					<input id="D_2_{$worker["id"]}" name="D_2" type="radio" class="is-checkradio" value="{$worker["id"]}"{if $items["D_2"] == $worker["id"]} checked="checked"{/if}>
					<label id="D_2_{$worker["id"]}_L" for="D_2_{$worker["id"]}">{$worker["worker_id"]}：{$worker["user_name"]}</label>
					<input id="D_2_{$worker["id"]}_N" type="hidden" value="{$worker["user_name"]}" />
				</li>
{/foreach}
			</ul>
		</section>

		<div class="has-text-centered has-margin-top-50">
			<button type="button" id="d2okbutton" class="button is-primary">{$labels["decision"]|escape}</button>
		</div>
	</div>
{/if}

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
<script>
$('.modaal').modaal({
	overlay_close: false, // 背景押下時にクローズさせるか
	before_open: d2OpenEvent,
	after_close: d2QuitEvent
});
</script>
</body>
</html>
