<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/FL01/FL01002.js"></script>
</head>

<body id="FL01002">
<form id="mainForm" method="POST" action="FL01002">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="csrf_token" id="csrf_token" value="{$smarty.session["csrf_token"]}" />
<input type="hidden" name="isDispDirect" id="isDispDirect" value="{$items["isDispDirect"]|escape}" />
{if isset($backToWorkerInfo)}<input type="hidden" id="backToWorkerInfo" value="1" />{/if}

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{$labels["L-title"]|escape}</h2>

		<table class="table has-margin-top-50 has-width-50pct responsive-list target-table">
			<thead>
				<tr>
					<th>{$labels["L-01-01"]|escape}</th>
					<th>{$labels["L-01-02"]|escape}</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{$items["upload_user_name"]|escape}</td>
					<td>{$items["upload_ymd"]|escape}</td>
				</tr>
			</tbody>
		</table>

		<table class="table is-bordered has-width-100pct">
			<tr>
				<th>{$labels["L-02-01"]|escape}</th>
				<td>
					<a href="#" id="filedownload">{$items["file_name_org"]|escape}</a>
				</td>
			</tr>

			<tr>
				<th>{$labels["L-02-02"]|escape}</th>
				<td>
					<p>{$items["file_comment"]|escape|nl2br}</p>
				</td>
			</tr>

			<tr>
				<th>{$labels["L-02-03"]|escape}</th>
				<td>
					<div class="has-vertical-middle">
						<p>{$items["file_type_name"]|escape}</p>
					</div>
				</td>
			</tr>

		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
