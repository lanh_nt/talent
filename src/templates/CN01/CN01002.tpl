<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/CN01/CN01002.js"></script>
</head>

<body>
<form id="mainForm" method="POST" action="CN01002">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="inq_id" id="inq_id" value="{$items["inq_id"]|escape}" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{$labels["L-title"]|escape}</h2>

		<table class="table table-conf is-bordered has-width-100pct">

			<!-- 問い合わせNo -->
			<tr>
				<th>{$labels["L-01-01"]|escape}</th>
				<td colspan="3">{$items["inq_no"]|escape}</td>
			</tr>

			<!-- 受信日・回答期限 -->
			<tr>
				<th>{$labels["L-01-02"]|escape}</th>
				<td>{$items["received_ymd"]|escape}</td>
				<th>{$labels["L-01-02b"]|escape}</th>
				<td>{$items["deadline_ymd"]|escape}</td>
			</tr>

			<!-- 配信者・配信言語 -->
			<tr>
				<th>{$labels["L-01-03"]|escape}</th>
				<td>{$items["user_name"]|escape}</td>
				<th>{$labels["L-01-04"]|escape}</th>
				<td>{$items["lang_string"]|escape}</td>
			</tr>

			<!-- 件名 -->
			<tr>
				<th rowspan="2" class="has-vertical-top">{$labels["L-01-05"]|escape}</th>
				<th>{$labels["L-01-05-01"]|escape}</th>
				<td colspan="2">{$items["subject_nv"]|escape}</td>
			</tr>
			<tr>
				<th>{$labels["L-01-05-02"]|escape}</th>
				<td colspan="2">{$items["subject_jp"]|escape}</td>
			</tr>

			<!-- 問い合わせ内容 -->
			<tr>
				<th rowspan="2" class="has-vertical-top">{$labels["L-01-06"]|escape}</th>
				<th class="has-vertical-top">{$labels["L-01-06-01"]|escape}</th>
				<td colspan="2">
					{$items["content_nv"]|escape|nl2br}
				</td>
			</tr>
			<tr>
				<th class="has-vertical-top">{$labels["L-01-06-02"]|escape}</th>
				<td colspan="2">
					{$items["content_jp"]|escape|nl2br}
				</td>
			</tr>

			<!-- 返信 -->
			<tr>
				<th rowspan="2" class="has-vertical-top">{$labels["L-01-07"]|escape}</th>
				<th class="has-vertical-top">{$labels["L-01-07-01"]|escape}</th>
				<td colspan="2">
					{$items["reply_nv"]|escape|nl2br}
				</td>
			</tr>
			<tr>
				<th class="has-vertical-top">{$labels["L-01-07-02"]|escape}</th>
				<td colspan="2">
					{$items["reply_jp"]|escape|nl2br}
				</td>
			</tr>
		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
			</div>
{if $smarty.session.loginUserType == '1'}
{if $smarty.session.loginUserAuth <= '2'}
			<div class="column is-6 has-text-right">
{if $items["status"] == '1'}
				<button type="button" class="button is-primary" id="edit_button">{$labels["regist_translate"]|escape}</button>
{/if}
{if $items["status"] == '2'}
				<button type="button" class="button is-primary" id="edit_button">{$labels["reply"]|escape}</button>
{/if}
			</div>
{/if}
{/if}
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
