<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/CN01/CN01003.js"></script>
</head>

<body>
<form id="mainForm" method="POST" action="CN01003">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="csrf_token" value="{$smarty.session["csrf_token"]}" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{$labels["L-title"]|escape}</h2>

{* --- メッセージ欄部品 --- *}
{include file='common/message.tpl' messages=$messages}

		<table class="table table-conf is-bordered has-width-100pct">

			<!-- 配信言語 -->
			<tr>
				<th>{$labels["L-01-01"]|escape}</th>
				<td>
					<div class="select has-vertical-middle">
						<select name="CN_01" id="CN_01">
							<option value=""></option>
							<option value="1"{if $items["CN_01"] == "1"} selected="selected"{/if}>{$labels["lang-1"]|escape}</option>
							<option value="2"{if $items["CN_01"] == "2"} selected="selected"{/if}>{$labels["lang-2"]|escape}</option>
							<option value="3"{if $items["CN_01"] == "3"} selected="selected"{/if}>{$labels["lang-3"]|escape}</option>
						</select>
					</div>
				</td>
			</tr>

			<!-- 回答期限 -->
			<tr>
				<th>{$labels["L-01-02"]|escape}</th>
				<td>
					<input id="CN_02" name="CN_02" type="text" value="{$items["CN_02"]|escape}"
						class="input has-width-max40pct is-inline-block flatpickr is-back-white" placeholder="{$labels["placeholder_date"]|escape}" required />
				</td>
			</tr>

			<!-- 件名 -->
			<tr>
				<th>{$labels["L-01-03"]|escape}</th>
				<td><input id="CN_03" name="CN_03" value="{$items["CN_03"]|escape}"
				    	   class="input" type="text" placeholder="{$labels["placeholder"]|escape}"></td>
			</tr>

			<!-- 問い合わせ内容 -->
			<tr>
				<th>{$labels["L-01-04"]|escape}</th>
				<td><textarea id="CN_04" name="CN_04" value="{$items["CN_03"]|escape}"
				              class="textarea" placeholder="{$labels["placeholder"]|escape}"></textarea></td>
			</tr>
		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
			</div>
			<div class="column is-6 has-text-right">
				<button type="button" class="button is-primary" id="regist_button">{$labels["regist"]|escape}</button>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer"></footer>
</form>
</body>
</html>
