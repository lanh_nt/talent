<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/CN01/CN01001.js"></script>
</head>

<body>
<form id="mainForm" method="POST" action="CN01001">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}" />
<input type="hidden" name="inq_id" id="inq_id" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">

		<h2 class="title is-4">{$labels["L-title"]|escape}</h2>

{* --- メッセージ欄部品 --- *}
{include file='common/message.tpl' messages=$messages}

{if $smarty.session.loginUserType == '2'}
{* 労働者画面の場合、「Q&A」「新規作成」ボタンを表示、検索条件を非表示。 *}
		<div class="has-text-right has-margin-bottom-20">
			<a href="../QA01/QA02001" class="button is-primary">{$labels["qa"]|escape}</a>
			<button type="button" class="button is-primary detail-button" id="insert_button">{$labels["new_create"]|escape}</button>
		</div>
		<input type="hidden" name="C_01" id="C_01" value="{$items["C_01"]|escape}">
{/if}
{if $smarty.session.loginUserType == '1'}
{* 管理者画面の場合、検索条件を表示。 *}
		<div class="box has-padding-20">
			<div class="columns is-multiline formbox-gap">
				<!-- 顧客ID -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">顧客ID：</p>
				</div>
				<div class="column is-10">
					<input class="input" type="text" type="text" name="C_01" id="C_01" placeholder="入力してください">
				</div>

				<!-- 顧客氏名-->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">顧客氏名：</p>
				</div>
				<div class="column is-10">
					<input class="input" type="text" type="text" name="C_02" id="C_02" placeholder="入力してください">
				</div>

				<!-- 企業名 -->
{if $smarty.session.loginCompanyType != '2'}
				<input type="hidden" name="C_03" id="C_03" value="{$smarty.session.loginCompanyRowId|escape}">
{else}
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">企業名：</p>
				</div>
				<div class="column is-10">
					<div class="select has-vertical-middle has-width-100pct">
						<select name="C_03" id="C_03" class="has-width-100pct">
							<option value=""></option>
{foreach from=$companies item=$company}
							<option value="{$company["id"]}"{if $company["id"] == $items["C_03"]} selected="selected"{/if}>{$company["name"]}</option>
{/foreach}
						</select>
					</div>
				</div>
{/if}

				<!-- 受信日 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">送受信日：</p>
				</div>
				<div class="column is-10">
					<input id="C_04_1" name="C_04_1" type="text" class="input has-width-max40pct is-inline-block flatpickr" placeholder="日付を選択してください" value="{$items["C_04_1"]|escape}" required />
					<span class="has-vertical-middle">～</span>
					<input id="C_04_2" name="C_04_2" type="text" class="input has-width-max40pct is-inline-block flatpickr is-back-white" placeholder="日付を選択してください" value="{$items["C_04_2"]|escape}" required />
				</div>

				<!-- 回答期限 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">回答期限：</p>
				</div>
				<div class="column is-10">
					<input id="C_05_1" name="C_05_1" type="text" class="input has-width-max40pct is-inline-block flatpickr" placeholder="日付を選択してください" value="{$items["C_05_1"]|escape}" required />
					<span class="has-vertical-middle">～</span>
					<input id="C_05_2" name="C_05_2" type="text" class="input has-width-max40pct is-inline-block flatpickr is-back-white" placeholder="日付を選択してください" value="{$items["C_05_2"]|escape}" required />
				</div>

				<!-- 返信 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">返信：</p>
				</div>
				<div class="column is-10">
					<div class="field">
						<input class="is-checkradio" type="checkbox" name="C_06_1" id="C_06_1" {if $items["C_06_1"] != ""}checked="checked"{/if}>
						<label for="C_06_1">返信済</label>
						<input class="is-checkradio" type="checkbox" name="C_06_2" id="C_06_2" {if $items["C_06_2"] != ""}checked="checked"{/if}>
						<label for="C_06_2">未返信</label>
					</div>
				</div>

			</div><!--  /.columns -->

			<div class="has-text-centered has-margin-top-20">
				<a class="button is-info submit-button" id="search_button">{$labels["select"]|escape}</a>
			</div>
		</div>
{/if}{* --- END loginUserType == '1' --- *}

{if isset($pageData->pageDatas)}
{if count($pageData->pageDatas) == 0}
		<div class="align-center">{$labels["no_records"]|escape}</div>
{else}
		<input type="hidden" name="page_no" id="page_no" value="{$pageData->currentPageNo}">
		<table class="table has-margin-top-50 has-width-100pct responsive-list">
			<thead>
				<tr>
{if $smarty.session.loginUserType == '1'}
					<th>顧客ID</th>
					<th>顧客氏名</th>
{/if}
					<th>{$labels["L-01-01"]|escape}</th>
					<th>{$labels["L-01-02"]|escape}</th>
{if $smarty.session.loginUserType == '1'}
					<th>回答期限</th>
{/if}
					<th>{$labels["L-01-03"]|escape}</th>
					<th>{$labels["L-01-04"]|escape}</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
{if $smarty.session.loginUserType == '1'}
					<th>顧客ID</th>
					<th>顧客氏名</th>
{/if}
					<th>{$labels["L-01-01"]|escape}</th>
					<th>{$labels["L-01-02"]|escape}</th>
{if $smarty.session.loginUserType == '1'}
					<th>回答期限</th>
{/if}
					<th>{$labels["L-01-03"]|escape}</th>
					<th>{$labels["L-01-04"]|escape}</th>
				</tr>
			</tfoot>
			<tbody>
{foreach from=$pageData->pageDatas item=$row}
				<tr>
{if $smarty.session.loginUserType == '1'}
					<td>{$row["user_id"]|escape}</td>
					<td>{$row["user_name"]|escape}</td>
{/if}
					<td>{$row["company_name"]|escape}</td>
					<td>{$row["received_ymd"]|escape}</td>
{if $smarty.session.loginUserType == '1'}
					<td>{$row["deadline_ymd"]|escape}</td>
{/if}
					<td>{$row["reply_status"]|escape}</td>
					<td>
						<button type="button" class="button is-dark detail-button" id="detail_button_{$row["inq_id"]|escape}">{$labels["contents"]|escape}</button>
					</td>
				</tr>
{/foreach}
			</tbody>
		</table>

{* --- ページング部品 --- *}
{include file='common/pager.tpl' pageData=$pageData}

{/if}{* --- END count(pageDatas) --- *}
{/if}{* --- END isset(pageDatas) --- *}

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
