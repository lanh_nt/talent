<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/CN01/CN01004.js"></script>
</head>

<body>
<form id="mainForm" method="POST" action="CN01004">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="csrf_token" value="{$smarty.session["csrf_token"]}" />
<input type="hidden" name="inq_id" id="inq_id" value="{$items["inq_id"]|escape}" />
<input type="hidden" name="inq_no" id="inq_no" value="{$items["inq_no"]|escape}" />
<input type="hidden" name="status" id="status" value="{$items["status"]|escape}" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
{if $items["status"] == 1}
		<h2 class="title is-4">問い合わせ管理/翻訳登録</h2>
{else}
		<h2 class="title is-4">問い合わせ管理/返信</h2>
{/if}

{* --- メッセージ欄部品 --- *}
{include file='common/message.tpl' messages=$messages}

		<table class="table table-conf is-bordered has-width-100pct">

			<!-- 問い合わせNo -->
			<tr>
				<th>問い合わせNo</th>
				<td colspan="3">{$items["inq_no"]|escape}</td>
			</tr>

			<!-- 受信日・回答期限 -->
			<tr>
				<th>受信日</th>
				<td>{$items["received_ymd"]|escape}</td>
				<th>回答期限</th>
				<td>{$items["deadline_ymd"]|escape}</td>
				<input type="hidden" name="received_ymd" value="{$items["received_ymd"]|escape}" />
				<input type="hidden" name="deadline_ymd" value="{$items["deadline_ymd"]|escape}" />
			</tr>

			<!-- 配信者・配信言語 -->
			<tr>
				<th>配信者</th>
				<td>{$items["user_name"]|escape}</td>
				<th>配信言語</th>
				<td>{$items["lang_string"]|escape}</td>
				<input type="hidden" name="user_name"   value="{$items["user_name"]|escape}" />
				<input type="hidden" name="lang_string" value="{$items["lang_string"]|escape}" />
			</tr>

			<!-- 件名 -->
			<tr>
				<th rowspan="2" class="has-vertical-top">件名</th>
				<th>母国語</th>
				<td colspan="2">{$items["subject_nv"]|escape}</td>
				<input type="hidden" name="subject_nv" value="{$items["subject_nv"]|escape}" />
			</tr>
			<tr>
				<th>日本語</th>
				<td colspan="2">
{if $items["status"] == 1}
					<input name="subject_jp" id="subject_jp" value="{$items["subject_jp"]|escape}"
					       class="input" type="text" placeholder="{$labels["placeholder"]|escape}">
{else}
					{$items["subject_jp"]|escape}
					<input type="hidden" name="subject_jp" value="{$items["subject_jp"]|escape}" />
{/if}
				</td>
			</tr>

			<!-- 問い合わせ内容 -->
			<tr>
				<th rowspan="2" class="has-vertical-top">問い合わせ内容</th>
				<th class="has-vertical-top">母国語</th>
				<td colspan="2">
					{$items["content_nv"]|escape|nl2br}
					<input type="hidden" name="content_nv" value="{$items["content_nv"]|escape}" />
				</td>
			</tr>
			<tr>
				<th class="has-vertical-top">日本語</th>
				<td colspan="2">
{if $items["status"] == 1}
					<textarea id="content_jp" name="content_jp" 
					          class="textarea" placeholder="{$labels["placeholder"]|escape}"
					>{$items["content_jp"]|escape}</textarea>
{else}
					{$items["content_jp"]|escape|nl2br}
					<input type="hidden" name="content_jp" value="{$items["content_jp"]|escape}" />
{/if}
				</td>
			</tr>

			<!-- 返信 -->
			<tr>
				<th rowspan="2" class="has-vertical-top">返信</th>
				<th class="has-vertical-top">母国語</th>
				<td colspan="2">
{if $items["status"] == 2}
					<textarea id="reply_nv" name="reply_nv" 
					          class="textarea" placeholder="{$labels["placeholder"]|escape}"
					>{$items["reply_nv"]|escape}</textarea>
{else}
					{$items["reply_nv"]|escape|nl2br}
					<input type="hidden" name="reply_nv" value="{$items["reply_nv"]|escape}" />
{/if}
				</td>
			</tr>
			<tr>
				<th class="has-vertical-top">日本語</th>
				<td colspan="2">
{if $items["status"] == 2}
					<textarea id="reply_jp" name="reply_jp" 
					          class="textarea" placeholder="{$labels["placeholder"]|escape}"
					>{$items["reply_jp"]|escape}</textarea>
{else}
					{$items["reply_jp"]|escape|nl2br}
					<input type="hidden" name="reply_jp" value="{$items["reply_jp"]|escape}" />
{/if}
				</td>
			</tr>
		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
			</div>
			<div class="column is-6 has-text-right">
				<button type="button" class="button is-primary" id="regist_button">{$labels["regist"]|escape}</button>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer"></footer>
</form>
</body>
</html>
