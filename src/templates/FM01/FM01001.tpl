<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/FM01/FM01001.js"></script>
</head>

<body id="FM01001">
<form id="mainForm" enctype="multipart/form-data" method="POST" action="FM01001">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}" />
<input type="hidden" name="report_ids" id="report_ids"  value="{$items["report_ids"]|escape}" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">帳票管理</h2>
{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}
		<div class="box has-padding-20">
			<div class="columns is-multiline formbox-gap">
				<!-- ID -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">ID:</p>
				</div>
				<div class="column is-10">
					<input class="input" type="text" name="C_01" id="C_01" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_01"]|escape}">
				</div>

				<!-- 氏名 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">氏名：</p>
				</div>
				<div class="column is-10">
					<input class="input" type="text" name="C_02" id="C_02" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_02"]|escape}">
				</div>

				<!-- 性別 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">性別：</p>
				</div>
				<div class="column is-10">
					<div class="field">
						<input class="is-checkradio" type="checkbox" name="C_05_1" id="C_05_1" {if $items["C_05_1"] != ""}checked="checked"{/if}>
						<label for="C_05_1">男</label>
						<input class="is-checkradio" type="checkbox" name="C_05_2" id="C_05_2" {if $items["C_05_2"] != ""}checked="checked"{/if}>
						<label for="C_05_2">女</label>
					</div>
				</div>

				<!-- 企業名 -->
{if $smarty.session.loginCompanyType != '2'}
				<input type="hidden" name="C_06" id="C_06" value="{$smarty.session.loginCompanyRowId|escape}">
{else}
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">企業名：</p>
				</div>
				<div class="column is-10">
					<div class="select has-vertical-middle has-width-100pct">
						<select name="C_06" id="C_06" class="has-width-100pct">
							<option value=""></option>
{foreach from=$companies item=$company}
							<option value="{$company["id"]}"{if $company["id"] == $items["C_06"]} selected="selected"{/if}>{$company["name"]}</option>
{/foreach}
						</select>
					</div>
				</div>
{/if}

				<!-- 登録日 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">登録日：</p>
				</div>
				<div class="column is-10">
					<input id="C_07_1" name="C_07_1" type="text" class="input has-width-max40pct is-inline-block flatpickr" value="{$items["C_07_1"]|escape}" required />
					<span class="has-vertical-middle">～</span>
					<input id="C_07_2" name="C_07_2" type="text" class="input has-width-max40pct is-inline-block flatpickr is-back-white" value="{$items["C_07_2"]|escape}" required />
				</div>

				<!-- 帳票 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">帳票：</p>
				</div>
				<div class="column is-10">
					<div class="field">
						<input class="is-checkradio" type="checkbox" name="C_08_1" id="C_08_1" {if $items["C_08_1"] != ""}checked="checked"{/if}>
						<label for="C_08_1">在留資格認定証明書交付申請書</label>
						<input class="is-checkradio" type="checkbox" name="C_08_2" id="C_08_2" {if $items["C_08_2"] != ""}checked="checked"{/if}>
						<label for="C_08_2">在留期間更新許可申請書</label>
						<input class="is-checkradio" type="checkbox" name="C_08_3" id="C_08_3" {if $items["C_08_3"] != ""}checked="checked"{/if}>
						<label for="C_08_3">在留資格変更許可申請書</label>
						<input class="is-checkradio" type="checkbox" name="C_08_4" id="C_08_4" {if $items["C_08_4"] != ""}checked="checked"{/if}>
						<label for="C_08_4">支援計画書</label>
						<input class="is-checkradio" type="checkbox" name="C_08_5" id="C_08_5" {if $items["C_08_5"] != ""}checked="checked"{/if}>
						<label for="C_08_5">支援実施状況に係る届出</label>
						<input class="is-checkradio" type="checkbox" name="C_08_6" id="C_08_6" {if $items["C_08_6"] != ""}checked="checked"{/if}>
						<label for="C_08_6">事前ガイダンスの確認書</label>
						<input class="is-checkradio" type="checkbox" name="C_08_7" id="C_08_7" {if $items["C_08_7"] != ""}checked="checked"{/if}>
						<label for="C_08_7">生活オリエンテーションの確認書</label>
						<input class="is-checkradio" type="checkbox" name="C_08_8" id="C_08_8" {if $items["C_08_8"] != ""}checked="checked"{/if}>
						<label for="C_08_8">相談記録書</label>
						<input class="is-checkradio" type="checkbox" name="C_08_9" id="C_08_9" {if $items["C_08_9"] != ""}checked="checked"{/if}>
						<label for="C_08_9">定期面談報告書</label>
						<input class="is-checkradio" type="checkbox" name="C_08_10" id="C_08_10" {if $items["C_08_10"] != ""}checked="checked"{/if}>
						<label for="C_08_10">履歴書</label>
					</div>
				</div>

				<!-- ステータス -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">ステータス：</p>
				</div>
				<div class="column is-10">
					<div class="field">
						<input class="is-checkradio" type="checkbox" name="C_09_1" id="C_09_1" {if $items["C_09_1"] != ""}checked="checked"{/if}>
						<label for="C_09_1">未入力</label>
						<input class="is-checkradio" type="checkbox" name="C_09_2" id="C_09_2" {if $items["C_09_2"] != ""}checked="checked"{/if}>
						<label for="C_09_2">入力中</label>
						<input class="is-checkradio" type="checkbox" name="C_09_3" id="C_09_3" {if $items["C_09_3"] != ""}checked="checked"{/if}>
						<label for="C_09_3">入力完了</label>
						<input class="is-checkradio" type="checkbox" name="C_09_4" id="C_09_4" {if $items["C_09_4"] != ""}checked="checked"{/if}>
						<label for="C_09_4">差し戻し</label>
						<input class="is-checkradio" type="checkbox" name="C_09_5" id="C_09_5" {if $items["C_09_5"] != ""}checked="checked"{/if}>
						<label for="C_09_5">提出済</label>
					</div>
				</div>

			</div><!--  /.columns -->

			<div class="has-text-centered has-margin-top-20">
				<a class="button is-info submit-button" id="search_button">検索</a>
			</div>
		</div>

{if isset($pageData->pageDatas)}
{if count($pageData->pageDatas) == 0}
		<div class="align-center">検索結果がありません。</div>
{else}
		<input type="hidden" name="page_no" id="page_no" value="{$pageData->currentPageNo}">
		<table class="table has-margin-top-50 has-width-100pct responsive-list">
			<thead>
				<tr>
					<th><input id="checkAll" type="checkbox" class="is-checkradio" checked="checked"><label for="checkAll">選択</label></th>
					<th>ID</th>
					<th>氏名</th>
					<th>企業名</th>
					<th>帳票</th>
					<th>ステータス</th>
					<th>最終更新日</th>
					<th></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>選択</th>
					<th>ID</th>
					<th>氏名</th>
					<th>企業名</th>
					<th>帳票</th>
					<th>ステータス</th>
					<th>最終更新日</th>
					<th></th>
				</tr>
			</tfoot>
			<tbody>
{foreach from=$pageData->pageDatas item=$row}
				<tr>
					<td>
						<input type="checkbox" class="is-checkradio c-10" name="C_10" id="C_10_{$row["id"]|escape}" value="{$row["id"]|escape}" checked="checked"><label for="C_10_{$row["id"]|escape}"></label>
					</td>
					<td>{$row["u_id"]|escape}</td>
					<td>{$row["user_name"]|escape}</td>
					<td>{$row["company_name"]|escape}</td>
					<td>{$row["report_name"]|escape}</td>
					<td>{$row["report_status_name"]|escape}</td>
					<td>{$row["update_date"]|escape}</td>
					<td>
						<button type="button" class="button has-margin-top-10 is-dark detail-button" id="detail_button_{$row["id"]|escape}">{$labels["contents"]|escape}</button>
{if $smarty.session.loginUserAuth <= '2'}
						<button type="button" class="button has-margin-top-10 update-button" id="update_button_{$row["id"]|escape}">{$labels["update"]|escape}</button>
{/if}
					</td>
				</tr>
{/foreach}
			</tbody>
		</table>

{if $smarty.session.loginUserAuth <= '2'}
		<div class="has-margin-top-50 has-text-right">
			<span class="has-vertical-middle is-font-bold">選択行を一括で更新する</span>
			<button type="button" class="button multi-update-button" id="multi-update-button">{$labels["update"]|escape}</button>
		</div>
{/if}

{include file='common/pager.tpl' pageData=$pageData}{* --- ページング部品 --- *}

{/if}
{/if}

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
