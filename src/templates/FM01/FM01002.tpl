<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/FM01/FM01002.js"></script>
</head>

<body id="FM01002">
	<form id="mainForm" enctype="multipart/form-data" method="POST" action="FM01002">
	<input type="hidden" name="mode" id="mode" />
	<input type="hidden" name="premode" id="premode" value="{$items["premode"]|escape}" />
	<input type="hidden" name="prestatus" id="prestatus" value="{$items["prestatus"]|escape}" />
	<input type="hidden" name="report_ids" id="report_ids" value="{$items["report_ids"]|escape}" />
	<input type="hidden" name="r_user_type" id="r_user_type" value="{$items["r_user_type"]|escape}" />
	<input type="hidden" name="csrf_token" value="{$smarty.session["csrf_token"]}" />

	<input type="hidden" name="page_no" id="page_no" value="{$items["page_no"]|escape}"  />
	<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}"  />

	<input type="hidden" name="C_01" id="C_01" value="{$items["C_01"]|escape}"  />
	<input type="hidden" name="C_02" id="C_02" value="{$items["C_02"]|escape}"  />
	<input type="hidden" name="C_05_1" id="C_05_1" value="{$items["C_05_1"]|escape}"  />
	<input type="hidden" name="C_05_2" id="C_05_2" value="{$items["C_05_2"]|escape}"  />
	<input type="hidden" name="C_06" id="C_06" value="{$items["C_06"]|escape}"  />
	<input type="hidden" name="C_08_1" id="C_08_1" value="{$items["C_08_1"]|escape}"  />
	<input type="hidden" name="C_08_2" id="C_08_2" value="{$items["C_08_2"]|escape}"  />
	<input type="hidden" name="C_08_3" id="C_08_3" value="{$items["C_08_3"]|escape}"  />
	<input type="hidden" name="C_08_4" id="C_08_4" value="{$items["C_08_4"]|escape}"  />
	<input type="hidden" name="C_08_5" id="C_08_5" value="{$items["C_08_5"]|escape}"  />
	<input type="hidden" name="C_08_6" id="C_08_6" value="{$items["C_08_6"]|escape}"  />
	<input type="hidden" name="C_08_7" id="C_08_7" value="{$items["C_08_7"]|escape}"  />
	<input type="hidden" name="C_08_8" id="C_08_8" value="{$items["C_08_8"]|escape}"  />
	<input type="hidden" name="C_08_9" id="C_08_9" value="{$items["C_08_9"]|escape}"  />
	<input type="hidden" name="C_08_10" id="C_08_10" value="{$items["C_08_10"]|escape}"  />

	<input type="hidden" name="C_09_1" id="C_09_1" value="{$items["C_09_1"]|escape}"  />
	<input type="hidden" name="C_09_2" id="C_09_2" value="{$items["C_09_2"]|escape}"  />
	<input type="hidden" name="C_09_3" id="C_09_3" value="{$items["C_09_3"]|escape}"  />
	<input type="hidden" name="C_09_4" id="C_09_4" value="{$items["C_09_4"]|escape}"  />
	<input type="hidden" name="C_09_5" id="C_09_5" value="{$items["C_09_5"]|escape}"  />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">帳票管理/ステータス
		{if $items["mode"] == 'multi-update'}一括{/if}更新</h2>
{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

		<h3 class="title is-5 has-margin-top-30">入力者情報</h3>
		<table class="table has-width-100pct responsive-list target-table">
			<thead>
				<tr>
					<th>ID</th>
					<th>氏名</th>
					<th>企業名</th>
					{if $items["mode"] == 'multi-update'}
					<th>帳票名</th>
					{/if}
					<th>登録日</th>
					<th>ステータス</th>
				</tr>
			</thead>
			<tbody>
{foreach from=$rowdatas item=$row}
				<tr>
					<td>{$row["r_user_id"]|escape}</td>
					<td>{$row["r_user_name"]|escape}</td>
					<td>{$row["r_company_name"]|escape}</td>
					{if $items["mode"] == 'multi-update'}
					<td>{$row["report_name"]|escape}</td>
					{/if}
					<td>{$row["r_create_date"]|escape}</td>
					<td>{$row["report_status_name"]|escape}</td>
				</tr>
{/foreach}
			</tbody>
		</table>

		<h3 class="title is-5 has-margin-top-50">ステータス</h3>
		<table class="table is-bordered has-width-100pct">
{if $items["mode"] != 'multi-update'}
			<tr>
				<th>⼊⼒完了期限</th>
				<td ><input id="input_deadline" name="input_deadline" data-mindate="today" type="text" class="input has-width-max40pct is-inline-block flatpickr" placeholder="日付を選択してください" value="{$row["input_deadline"]|escape}" required /></td>
				<th>提出期限</th>
				<td ><input id="submission_deadline" name="submission_deadline" data-mindate="today" type="text" class="input has-width-max40pct is-inline-block flatpickr" placeholder="日付を選択してください" value="{$row["submission_deadline"]|escape}" required /></td>
			</tr>
			<tr>
				<th>帳票名</th>
				<td colspan="3">
					<div class="columns is-mobile">
						<div class="column has-text-left">
							<p>{$row["report_name"]|escape}</p>
							<input type="hidden" name="report_id" id="report_id" value="{$row["report_id"]|escape}"  />
							<input type="hidden" name="r_user_id" id="r_user_id" value="{$row["r_user_id"]|escape}"  />
						</div>
						<div class="column has-text-right">
							<button type="button" class="button report-button" id="report-button">{$labels["edit"]|escape}</button>
						</div>
					</div>
				</td>
			</tr>
{/if}

			<tr>
				<th>ステータス</th>
				<td colspan="3">
					<div class="select has-vertical-middle">
						<select name="C_09" id="C_09">
							<option value=""></option>
{foreach from=$report_status key=$pkey item=$pname}
							<option value="{$pkey}" {if $pkey == $items["C_09"]} selected="selected"{/if}>{$pname}</option>
{/foreach}
						</select>
					</div>
				</td>
			</tr>
		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark return-button" id="return-button">{$labels["back"]|escape}</button>
			</div>
			<div class="column is-6 has-text-right">
				<button type="button" class="button is-primary regist-button" id="regist-button">{$labels["regist"]|escape}</button>
			</div>
		</div>


	</div> <!-- /.contents -->


	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
	</form>
</body>
</html>
