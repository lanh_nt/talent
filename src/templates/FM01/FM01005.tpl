<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/FM01/FM01005.js"></script>
</head>

<body id="FM01005">
	<form id="mainForm" enctype="multipart/form-data" method="POST" action="FM01001">
	<input type="hidden" name="mode" id="mode" />
	<input type="hidden" name="report_ids" id="report_ids" value="{$items["report_ids"]|escape}" />

	<input type="hidden" name="page_no" id="page_no" value="{$items["page_no"]|escape}"  />
	<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}"  />

	<input type="hidden" name="C_01" id="C_01" value="{$items["C_01"]|escape}"  />
	<input type="hidden" name="C_02" id="C_02" value="{$items["C_02"]|escape}"  />
	<input type="hidden" name="C_05_1" id="C_05_1" value="{$items["C_05_1"]|escape}"  />
	<input type="hidden" name="C_05_2" id="C_05_2" value="{$items["C_05_2"]|escape}"  />
	<input type="hidden" name="C_06" id="C_06" value="{$items["C_06"]|escape}"  />
	<input type="hidden" name="C_08_1" id="C_08_1" value="{$items["C_08_1"]|escape}"  />
	<input type="hidden" name="C_08_2" id="C_08_2" value="{$items["C_08_2"]|escape}"  />
	<input type="hidden" name="C_08_3" id="C_08_3" value="{$items["C_08_3"]|escape}"  />
	<input type="hidden" name="C_08_4" id="C_08_4" value="{$items["C_08_4"]|escape}"  />
	<input type="hidden" name="C_08_5" id="C_08_5" value="{$items["C_08_5"]|escape}"  />
	<input type="hidden" name="C_08_6" id="C_08_6" value="{$items["C_08_6"]|escape}"  />
	<input type="hidden" name="C_08_7" id="C_08_7" value="{$items["C_08_7"]|escape}"  />
	<input type="hidden" name="C_08_8" id="C_08_8" value="{$items["C_08_8"]|escape}"  />
	<input type="hidden" name="C_08_9" id="C_08_9" value="{$items["C_08_9"]|escape}"  />
	<input type="hidden" name="C_08_10" id="C_08_10" value="{$items["C_08_10"]|escape}"  />

	<input type="hidden" name="C_09_1" id="C_09_1" value="{$items["C_09_1"]|escape}"  />
	<input type="hidden" name="C_09_2" id="C_09_2" value="{$items["C_09_2"]|escape}"  />
	<input type="hidden" name="C_09_3" id="C_09_3" value="{$items["C_09_3"]|escape}"  />
	<input type="hidden" name="C_09_4" id="C_09_4" value="{$items["C_09_4"]|escape}"  />
	<input type="hidden" name="C_09_5" id="C_09_5" value="{$items["C_09_5"]|escape}"  />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">帳票管理/ステータス更新</h2>
		<h3 class="title is-5 has-margin-top-30">入力者情報</h3>
		<table class="table is-bordered has-width-100pct">
			<tr>
				<th>ID</th>
				<td>{$rowdata["r_user_id"]|escape}</td>
			</tr>
			<tr>
				<th>氏名</th>
				<td>{$rowdata["r_user_name"]|escape}</td>
			</tr>
			<tr>
				<th>在留資格</th>
				<td>{$rowdata["qualifications_name"]|escape}</td>
			</tr>
			<tr>
				<th>企業名</th>
				<td>{$rowdata["r_company_name"]|escape}</td>
			</tr>
			<tr>
				<th>登録日</th>
				<td>{$rowdata["r_create_date"]|escape}</td>
			</tr>
		</table>


		<h3 class="title is-5 has-margin-top-30">ステータス</h3>
		<table class="table is-bordered has-width-100pct">
			<tr>
				<th>⼊⼒完了期限</th>
				<td >{$rowdata["input_deadline"]|escape}</td>
				<th>提出期限</th>
				<td >{$rowdata["submission_deadline"]|escape}</td>
			</tr>
			<tr>
				<th>帳票名</th>
				<td  colspan="3">{$rowdata["report_name"]|escape}</td>
			</tr>
			<tr>
				<th>ステータス</th>
				<td  colspan="3">{$rowdata["report_status_name"]|escape}</td>
			</tr>
		</table><!--  /.table -->

		<h3 class="title is-5 has-margin-top-30">提出済帳票履歴</h3>
{if count($rirekidatas) == 0}
履歴はありません。
{else}
		<table class="table is-bordered has-width-100pct">		
{foreach from=$rirekidatas item=$rireki_row}
			<tr>
				<th>出力日時</th>
				<td >{$rireki_row["create_date"]|escape}</td>
				<td class="has-text-centered has-width-30pct">
					<button type="button" id="rirekiReport_{$rireki_row["pdf_token"]|escape}" class="report-button button is-dark">出力</button>
				</td>
			</tr>
{/foreach}
		</table><!--  /.table -->
{/if}

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark return-button" id="return-button">{$labels["back"]|escape}</button>
			</div>
		</div>


	</div> <!-- /.contents -->


	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
