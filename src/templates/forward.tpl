<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
{if $isRoot}
<script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>
{else}
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
{/if}
<script type="text/javascript">
$(function() {
	// 画面ロード完了時に、指定ページに submit する。
	$("#mainForm").submit();
});
</script>
</head>

<body>
<form id="mainForm" method="POST" action="{$nextpage}">
{foreach from=$items key=formName item=formValue}
<input type="hidden" name="{$formName}" value="{$formValue}" />
{/foreach}
</form>
</body>
</html>
