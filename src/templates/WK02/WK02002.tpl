<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}ｓ</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/WK02/WK02.js"></script>
<script type="text/javascript" src="../js/WK02/WK02002.js"></script>
</head>

<body id="WK02002">
<form id="mainForm" method="POST" action="WK02002">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="csrf_token" value="{$smarty.session["csrf_token"]}" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{if $smarty.session.loginUserType == '1'}{$labels["L-title-A"]|escape}{else}{$labels["L-title-C"]|escape}{/if}</h2>

{* --- メッセージ欄部品 --- *}
{include file='common/message.tpl' messages=$messages}

		<div class="columns sidemenu">
			<div class="column is-9">
				<h3 class="title is-5">{$labels["L-01"]|escape}</h3>
				<table class="table is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-01-01"]|escape}</th>
						<td>{$items["qualifications"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-02"]|escape}</th>
						<td>{$items["status"]|escape}</td>
					</tr>
{if $smarty.session.loginUserType == '1'}
					<tr>
						<th>{$labels["L-01-03"]|escape}</th>
						<td>{$items["id"]|escape}</td>
					</tr>
{/if}
					<tr>
						<th>{$labels["L-01-04"]|escape}</th>
						<td>{$items["worker_id"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-05"]|escape}</th>
				<td><input class="input" type="password" name="password" id="password" placeholder="{$labels["placeholder"]|escape}" value=""></td>
					</tr>
					<tr>
						<th>{$labels["L-01-06"]|escape}</th>
				<td><input class="input" type="password" name="password2" id="password2" placeholder="{$labels["placeholder"]|escape}" value=""></td>
					</tr>
				</table><!--  /.table -->

				<div class="has-margin-top-30 has-text-centered">
{if $smarty.session.loginUserType == '1' }
					<div class="is-pulled-left">
						<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
					</div>
{/if}
{if $smarty.session.loginUserAuth <= '2' || $smarty.session.loginUserAuth == '9'}
					<button type="button" class="button is-primary" id="regist_button">{$labels["regist"]|escape}</button>
{/if}
				</div>
			</div>
			<div class="column is-3">
				<aside class="menu">
					<ul class="menu-list">
						<li><a href="./WK02001">{$labels["sub_menu_01"]|escape}</a></li>
						<li><a class="is-active" href="./WK02002">{$labels["sub_menu_02"]|escape}</a></li>
						<li>
							<a class="acMenu">{$labels["sub_menu_03"]|escape}</a>
							<ul style="display: none;">
								<li><a href="./WK02003">{$labels["sub_menu_03_01"]|escape}</a></li>
								<li><a href="./WK02013">{$labels["sub_menu_03_02"]|escape}</a></li>
								<li><a href="./WK02014">{$labels["sub_menu_03_03"]|escape}</a></li>
								<li><a href="./WK02004">{$labels["sub_menu_03_04"]|escape}</a></li>
								<li><a href="./WK02005">{$labels["sub_menu_03_05"]|escape}</a></li>
								<li><a href="./WK02006">{$labels["sub_menu_03_06"]|escape}</a></li>
								<li><a href="./WK02007">{$labels["sub_menu_03_07"]|escape}</a></li>
								<li><a href="./WK02008">{$labels["sub_menu_03_08"]|escape}</a></li>
								<li><a href="./WK02009">{$labels["sub_menu_03_09"]|escape}</a></li>
								<li><a href="./WK02010">{$labels["sub_menu_03_10"]|escape}</a></li>
							</ul>
						</li>
						<li><a href="./WK02011">{$labels["sub_menu_04"]|escape}</a></li>
						<li><a href="./WK02012">{$labels["sub_menu_05"]|escape}</a></li>
					</ul>
				</aside>
			</div>
		</div>
	</div> <!-- /.contents -->


	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</body>
</html>
