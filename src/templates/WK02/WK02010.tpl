<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/WK02/WK02.js"></script>
<script type="text/javascript" src="../js/WK02/WK02010.js"></script>
</head>

<body id="WK02010">
<form id="mainForm" enctype="multipart/form-data" method="POST" action="WK02010">
<input type="hidden" name="mode" id="mode" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{if $smarty.session.loginUserType == '1'}{$labels["L-title-A"]|escape}{else}{$labels["L-title-C"]|escape}{/if}</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

		<div class="columns sidemenu">
			<div class="column is-9">
				<h3 class="title is-5">{$labels["L-sub"]|escape}</h3>
				<table class="table is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-01-01"]|escape}</th>
						<td>{$items["user_name_e"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-02"]|escape}</th>
						<td>{$items["user_name"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03"]|escape}</th>
						<td>{$items["sex"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-04"]|escape}</th>
						<td>{$items["birthday_ymd"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-05"]|escape}</th>
						<td>{$items["nationality_region"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-06"]|escape}</th>
						<td>{$items["speakable_lang"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-07"]|escape}</th>
						<td>{$items["address"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-08"]|escape}</th>
						<td>{$items["telephone"]|escape}</td>
					</tr>
				</table><!--  /.table -->

				<h3 class="title is-6 has-margin-top-30">{$labels["L-02"]|escape}</h3>
				<table class="table is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-02-01"]|escape}</th>
						<td>
							<ul>
								<li>{$items["job_1_ym"]|escape}</li>
								<li>{$items["job_1_detail"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th>{$labels["L-02-02"]|escape}</th>
						<td>
							<ul>
								<li>{$items["job_2_ym"]|escape}</li>
								<li>{$items["job_2_detail"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th>{$labels["L-02-03"]|escape}</th>
						<td>
							<ul>
								<li>{$items["job_3_ym"]|escape}</li>
								<li>{$items["job_3_detail"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th>{$labels["L-02-04"]|escape}</th>
						<td>
							<ul>
								<li>{$items["job_4_ym"]|escape}</li>
								<li>{$items["job_4_detail"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th>{$labels["L-02-05"]|escape}</th>
						<td>
							<ul>
								<li>{$items["job_5_ym"]|escape}</li>
								<li>{$items["job_5_detail"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th>{$labels["L-02-06"]|escape}</th>
						<td>
							<ul>
								<li>{$items["job_6_ym"]|escape}</li>
								<li>{$items["job_6_detail"]|escape}</li>
							</ul>
						</td>
					</tr>
				</table><!--  /.table -->

				<h3 class="title is-6 has-margin-top-30">{$labels["L-03"]|escape}</h3>
				<table class="table is-bordered has-width-100pct">
					<tr>
						<td colspan="2">{$items["license"]|escape|nl2br|default:'&nbsp;'}</td>
					</tr>
				</table>

				<h3 class="title is-6 has-margin-top-30">{$labels["L-04"]|escape}</h3>
				<table class="table is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-04-01"]|escape}</th>
						<td>
							<ul>
								<li>{$items["visit_1_ym"]|escape}</li>
								<li>{$items["visit_1_license"]|escape}</li>
								<li>{$items["visit_1_company"]|escape}</li>
								<li>{$items["visit_1_superviser"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th>{$labels["L-04-02"]|escape}</th>
						<td>
							<ul>
								<li>{$items["visit_2_ym"]|escape}</li>
								<li>{$items["visit_2_license"]|escape}</li>
								<li>{$items["visit_2_company"]|escape}</li>
								<li>{$items["visit_2_superviser"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th>{$labels["L-04-03"]|escape}</th>
						<td>
							<ul>
								<li>{$items["visit_3_ym"]|escape}</li>
								<li>{$items["visit_3_license"]|escape}</li>
								<li>{$items["visit_3_company"]|escape}</li>
								<li>{$items["visit_3_superviser"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th>{$labels["L-04-04"]|escape}</th>
						<td>
							<ul>
								<li>{$items["visit_4_ym"]|escape}</li>
								<li>{$items["visit_4_license"]|escape}</li>
								<li>{$items["visit_4_company"]|escape}</li>
								<li>{$items["visit_4_superviser"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th>{$labels["L-04-05"]|escape}</th>
						<td>
							<ul>
								<li>{$items["visit_5_ym"]|escape}</li>
								<li>{$items["visit_5_license"]|escape}</li>
								<li>{$items["visit_5_company"]|escape}</li>
								<li>{$items["visit_5_superviser"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th>{$labels["L-04-06"]|escape}</th>
						<td>
							<ul>
								<li>{$items["visit_6_ym"]|escape}</li>
								<li>{$items["visit_6_license"]|escape}</li>
								<li>{$items["visit_6_company"]|escape}</li>
								<li>{$items["visit_6_superviser"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th>{$labels["L-04-07"]|escape}</th>
						<td>
							<ul>
								<li>{$items["visit_7_ym"]|escape}</li>
								<li>{$items["visit_7_license"]|escape}</li>
								<li>{$items["visit_7_company"]|escape}</li>
								<li>{$items["visit_7_superviser"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th>{$labels["L-04-08"]|escape}</th>
						<td>
							<ul>
								<li>{$items["visit_8_ym"]|escape}</li>
								<li>{$items["visit_8_license"]|escape}</li>
								<li>{$items["visit_8_company"]|escape}</li>
								<li>{$items["visit_8_superviser"]|escape}</li>
							</ul>
						</td>
					</tr>
				</table>

{if $smarty.session.loginUserType == '1'}
				<div class="has-margin-top-30 has-text-centered">
					<div class="is-pulled-left">
						<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
					</div>
{if $smarty.session.loginUserAuth <= '2'}
					<input type="file" name="upfile" id="upfile" style="display:none;" />
					<button type="button" class="button is-primary has-margin-right-20" id="csv_import_button">{$labels["csv_import"]|escape}</button>
					<button type="button" class="button is-primary has-margin-right-20" id="csv_export_button">{$labels["csv_export"]|escape}</button>
					<button type="button" class="button is-primary has-margin-right-20" id="csv_format_button">{$labels["csv_format"]|escape}</button>
{/if}
				</div>
{/if}
			</div>

			<div class="column is-3">
				<aside class="menu">
					<ul class="menu-list">
						<li><a href="./WK02001">{$labels["sub_menu_01"]|escape}</a></li>
						<li><a href="./WK02002">{$labels["sub_menu_02"]|escape}</a></li>
						<li>
							<a class="acMenu">{$labels["sub_menu_03"]|escape}</a>
							<ul>
								<li><a href="./WK02003">{$labels["sub_menu_03_01"]|escape}</a></li>
								<li><a href="./WK02013">{$labels["sub_menu_03_02"]|escape}</a></li>
								<li><a href="./WK02014">{$labels["sub_menu_03_03"]|escape}</a></li>
								<li><a href="./WK02004">{$labels["sub_menu_03_04"]|escape}</a></li>
								<li><a href="./WK02005">{$labels["sub_menu_03_05"]|escape}</a></li>
								<li><a href="./WK02006">{$labels["sub_menu_03_06"]|escape}</a></li>
								<li><a href="./WK02007">{$labels["sub_menu_03_07"]|escape}</a></li>
								<li><a href="./WK02008">{$labels["sub_menu_03_08"]|escape}</a></li>
								<li><a href="./WK02009">{$labels["sub_menu_03_09"]|escape}</a></li>
								<li><a href="./WK02010" class="is-active">{$labels["sub_menu_03_10"]|escape}</a></li>
							</ul>
						</li>
						<li><a href="./WK02011">{$labels["sub_menu_04"]|escape}</a></li>
						<li><a href="./WK02012">{$labels["sub_menu_05"]|escape}</a></li>
					</ul>
				</aside>

			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
