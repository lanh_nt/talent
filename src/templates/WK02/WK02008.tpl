<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/WK02/WK02.js"></script>
<script type="text/javascript" src="../js/WK02/WK02008.js"></script>
</head>

<body id="WK02008">
<form id="mainForm" method="POST" action="WK02008">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="csrf_token" value="{$smarty.session["csrf_token"]}" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{if $smarty.session.loginUserType == '1'}{$labels["L-title-A"]|escape}{else}{$labels["L-title-C"]|escape}{/if}</h2>

{* --- メッセージ欄部品 --- *}
{include file='common/message.tpl' messages=$messages}

		<div class="columns sidemenu">
			<div class="column is-9">
				<h3 class="title is-5">{$labels["L-sub"]|escape}</h3>

				<table class="table is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-02-01"]|escape}</th>
						<td>
{if $smarty.session.loginUserType == '1'}
							<input name="span_from" id="span_from" type="text" class="input flatpickr has-width-max40pct is-inline-block"
								value="{$items["span_from"]|escape}" placeholder="{$labels["placeholder_date"]|escape}" />
							<span class="has-vertical-middle">～</span>
							<input name="span_to" id="span_to" type="text" class="input flatpickr has-width-max40pct is-inline-block is-back-white"
								value="{$items["span_to"]|escape}" placeholder="{$labels["placeholder_date"]|escape}" />
{else}
							{$items["span_disp"]|escape}
{/if}
						</td>
					</tr>
				</table><!--  /.table -->

{if $smarty.session.loginUserType == '1'}
{if $smarty.session.loginUserAuth <= '2'}
				<div class="has-margin-top-30 has-margin-bottom-50 has-text-centered">
					<button type="button" class="button is-primary" id="regist_button">{$labels["regist"]|escape}</button>
				</div>
{/if}
{/if}

{foreach from=$reportDatas item=$row}
				<table class="table is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-01-01"]|escape}</th>
						<td>{$row["consultation_ymd"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-02"]|escape}</th>
						<td>{$row["consultation_content"]|escape|nl2br}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03"]|escape}</th>
						<td>{$row["response_results"]|escape|nl2br}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-04"]|escape}</th>
						<td>{$row["corresponding_person"]|escape}</td>
					</tr>
				</table>
{/foreach}

{if $smarty.session.loginUserType == '1'}
				<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
{/if}
			</div>

			<div class="column is-3">
				<aside class="menu">
					<ul class="menu-list">
						<li><a href="./WK02001">{$labels["sub_menu_01"]|escape}</a></li>
						<li><a href="./WK02002">{$labels["sub_menu_02"]|escape}</a></li>
						<li>
							<a class="acMenu">{$labels["sub_menu_03"]|escape}</a>
							<ul>
								<li><a href="./WK02003">{$labels["sub_menu_03_01"]|escape}</a></li>
								<li><a href="./WK02013">{$labels["sub_menu_03_02"]|escape}</a></li>
								<li><a href="./WK02014">{$labels["sub_menu_03_03"]|escape}</a></li>
								<li><a href="./WK02004">{$labels["sub_menu_03_04"]|escape}</a></li>
								<li><a href="./WK02005">{$labels["sub_menu_03_05"]|escape}</a></li>
								<li><a href="./WK02006">{$labels["sub_menu_03_06"]|escape}</a></li>
								<li><a href="./WK02007">{$labels["sub_menu_03_07"]|escape}</a></li>
								<li><a href="./WK02008" class="is-active">{$labels["sub_menu_03_08"]|escape}</a></li>
								<li><a href="./WK02009">{$labels["sub_menu_03_09"]|escape}</a></li>
								<li><a href="./WK02010">{$labels["sub_menu_03_10"]|escape}</a></li>
							</ul>
						</li>
						<li><a href="./WK02011">{$labels["sub_menu_04"]|escape}</a></li>
						<li><a href="./WK02012">{$labels["sub_menu_05"]|escape}</a></li>
					</ul>
				</aside>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
