<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/WK02/WK02.js"></script>
<script type="text/javascript" src="../js/WK02/WK02004.js"></script>
</head>

<body id="WK02004">
<form id="mainForm" enctype="multipart/form-data" method="POST" action="WK02004">
<input type="hidden" name="mode" id="mode" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{if $smarty.session.loginUserType == '1'}{$labels["L-title-A"]|escape}{else}{$labels["L-title-C"]|escape}{/if}</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

		<div class="columns sidemenu">
			<div class="column is-9">
				<h3 class="title is-5">{$labels["L-sub"]|escape}</h3>
				<h3 class="title is-6 has-margin-top-30">{$labels["L-01"]|escape}</h3>
				<!-- 支援対象者 -->
				<table class="table is-bordered  has-width-100pct">
					<tr>
						<th>{$labels["L-01-01"]|escape}</th>
						<td>{$items["user_name"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-02"]|escape}</th>
						<td>{$items["sex"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03"]|escape}</th>
						<td>{$items["birthday"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-04"]|escape}</th>
						<td>{$items["nationality_region"]|escape}</td>
					</tr>
				</table>

				<!-- 特定技能所属機関 -->
				<h3 class="title is-6 has-margin-top-30">{$labels["L-02"]|escape}</h3>
				<table class="table is-bordered">
					<tr>
						<th>{$labels["L-02-01"]|escape}</th>
						<td>{$items["company_name"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-02"]|escape}</th>
						<td>{$items["company_kana"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-03"]|escape}</th>
						<td>{$items["postal_code"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-04"]|escape}</th>
						<td>{$items["address"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-05"]|escape}</th>
						<td>{$items["tel"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-06"]|escape}</th>
						<td>{$items["corporation_no"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-07"]|escape}</th>
						<td>{$items["support_manager_name"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-08"]|escape}</th>
						<td>{$items["support_manager_kana"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-09"]|escape}</th>
						<td>{$items["support_manager_department"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-10"]|escape}</th>
						<td>{$items["worker_mumber"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-11"]|escape}</th>
						<td>{$items["support_stuff_number"]|escape}</td>
					</tr>
				</table>

				<!-- 登録支援機関 -->
				<h3 class="title is-6 has-margin-top-30">{$labels["L-03"]|escape}</h3>
				<table class="table is-bordered">
					<tr>
						<th>{$labels["L-03-01"]|escape}</th>
						<td>登-{$items["s_registration_no"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-02"]|escape}</th>
						<td>{$items["s_registration_date"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-03"]|escape}</th>
						<td>{$items["s_support_scheduled_date"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-04"]|escape}</th>
						<td>{$items["s_company_name"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-05"]|escape}</th>
						<td>{$items["s_company_kana"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-06"]|escape}</th>
						<td>{$items["s_postal_code"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-07"]|escape}</th>
						<td>{$items["s_address"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-08"]|escape}</th>
						<td>{$items["s_tel"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-09"]|escape}</th>
						<td>{$items["s_representative_name"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-10"]|escape}</th>
						<td>{$items["s_representative_kana"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-11"]|escape}</th>
						<td>{$items["s_corporation_no"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-12"]|escape}</th>
						<td>{$items["s_support_postal_code"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-13"]|escape}</th>
						<td>{$items["s_support_address"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-14"]|escape}</th>
						<td>{$items["s_support_tel"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-15"]|escape}</th>
						<td>{$items["s_support_manager_name"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-16"]|escape}</th>
						<td>{$items["s_support_manager_kana"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-17"]|escape}</th>
						<td>{$items["s_support_manager_department"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-18"]|escape}</th>
						<td>{$items["s_worker_mumber"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-19"]|escape}</th>
						<td>{$items["s_support_stuff_number"]|escape}</td>
					</tr>
				</table>

				<!-- 事前ガイダンスの提供 -->
				<h3 class="title is-6 has-margin-top-30">{$labels["L-04"]|escape}</h3>
				<table class="table is-bordered">
					<tr>
						<th colspan="2">{$labels["L-04-01"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-04-01-01"]|escape}</th>
						<td>{$items["C_1_01_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-01-02"]|escape}</th>
						<td>{$items["C_1_01_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-01-03"]|escape}</th>
						<td>{$items["C_1_01_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-01-04"]|escape}</th>
						<td>{$items["C_1_01_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-01-05"]|escape}</th>
						<td>{$items["C_1_01_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-01-06"]|escape}</th>
						<td>{$items["C_1_01_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-04-02"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-04-02-01"]|escape}</th>
						<td>{$items["C_1_02_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-02-02"]|escape}</th>
						<td>{$items["C_1_02_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-02-03"]|escape}</th>
						<td>{$items["C_1_02_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-02-04"]|escape}</th>
						<td>{$items["C_1_02_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-02-05"]|escape}</th>
						<td>{$items["C_1_02_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-02-06"]|escape}</th>
						<td>{$items["C_1_02_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-04-03"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-04-03-01"]|escape}</th>
						<td>{$items["C_1_03_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-03-02"]|escape}</th>
						<td>{$items["C_1_03_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-03-03"]|escape}</th>
						<td>{$items["C_1_03_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-03-04"]|escape}</th>
						<td>{$items["C_1_03_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-03-05"]|escape}</th>
						<td>{$items["C_1_03_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-03-06"]|escape}</th>
						<td>{$items["C_1_03_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-04-04"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-04-04-01"]|escape}</th>
						<td>{$items["C_1_04_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-04-02"]|escape}</th>
						<td>{$items["C_1_04_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-04-03"]|escape}</th>
						<td>{$items["C_1_04_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-04-04"]|escape}</th>
						<td>{$items["C_1_04_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-04-05"]|escape}</th>
						<td>{$items["C_1_04_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-04-06"]|escape}</th>
						<td>{$items["C_1_04_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-04-05"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-04-05-01"]|escape}</th>
						<td>{$items["C_1_05_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-05-02"]|escape}</th>
						<td>{$items["C_1_05_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-05-03"]|escape}</th>
						<td>{$items["C_1_05_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-05-04"]|escape}</th>
						<td>{$items["C_1_05_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-05-05"]|escape}</th>
						<td>{$items["C_1_05_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-05-06"]|escape}</th>
						<td>{$items["C_1_05_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-04-06"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-04-06-01"]|escape}</th>
						<td>{$items["C_1_06_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-06-02"]|escape}</th>
						<td>{$items["C_1_06_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-06-03"]|escape}</th>
						<td>{$items["C_1_06_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-06-04"]|escape}</th>
						<td>{$items["C_1_06_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-06-05"]|escape}</th>
						<td>{$items["C_1_06_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-06-06"]|escape}</th>
						<td>{$items["C_1_06_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-04-07"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-04-07-01"]|escape}</th>
						<td>{$items["C_1_07_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-07-02"]|escape}</th>
						<td>{$items["C_1_07_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-07-03"]|escape}</th>
						<td>{$items["C_1_07_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-07-04"]|escape}</th>
						<td>{$items["C_1_07_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-07-05"]|escape}</th>
						<td>{$items["C_1_07_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-07-06"]|escape}</th>
						<td>{$items["C_1_07_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-04-08"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-04-08-01"]|escape}</th>
						<td>{$items["C_1_08_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-08-02"]|escape}</th>
						<td>{$items["C_1_08_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-08-03"]|escape}</th>
						<td>{$items["C_1_08_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-08-04"]|escape}</th>
						<td>{$items["C_1_08_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-08-05"]|escape}</th>
						<td>{$items["C_1_08_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-08-06"]|escape}</th>
						<td>{$items["C_1_08_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-04-09"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-04-09-01"]|escape}</th>
						<td>{$items["C_1_09_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-09-02"]|escape}</th>
						<td>{$items["C_1_09_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-09-03"]|escape}</th>
						<td>{$items["C_1_09_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-09-04"]|escape}</th>
						<td>{$items["C_1_09_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-09-05"]|escape}</th>
						<td>{$items["C_1_09_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-09-06"]|escape}</th>
						<td>{$items["C_1_09_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-04-10"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-04-10-01"]|escape}</th>
						<td>{$items["C_1_10_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-10-02"]|escape}</th>
						<td>{$items["C_1_10_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-10-03"]|escape}</th>
						<td>{$items["C_1_10_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-10-04"]|escape}</th>
						<td>{$items["C_1_10_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-10-05"]|escape}</th>
						<td>{$items["C_1_10_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-10-06"]|escape}</th>
						<td>{$items["C_1_10_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$items["C_1_11_1"]|escape}&nbsp;</th>
					</tr>
					<tr>
						<th>{$labels["L-04-11-01"]|escape}</th>
						<td>{$items["C_1_11_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-11-02"]|escape}</th>
						<td>{$items["C_1_11_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-11-03"]|escape}</th>
						<td>{$items["C_1_11_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-11-04"]|escape}</th>
						<td>{$items["C_1_11_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-11-05"]|escape}</th>
						<td>{$items["C_1_11_6"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-11-06"]|escape}</th>
						<td>{$items["C_1_11_7"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-04-12"]|escape}</th>
					</tr>
					<tr>
						<td colspan="2">{$items["C_1_12"]|escape}&nbsp;</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-04-13"]|escape}</th>
					</tr>
					<tr>
						<td colspan="2">{$items["C_1_13"]|escape}&nbsp;</td>
					</tr>
				</table><!--  /.table -->

				<!-- 出入国する際の送迎 -->
				<h3 class="title is-6 has-margin-top-30">{$labels["L-05"]|escape}</h3>
				<table class="table is-bordered">
					<tr>
						<th colspan="2">{$labels["L-05-01"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-05-01-01"]|escape}</th>
						<td>{$items["C_2_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-05-01-02"]|escape}</th>
						<td>{$items["C_2_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-05-01-03"]|escape}</th>
						<td>{$items["C_2_1_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-05-01-04"]|escape}</th>
						<td>{$items["C_2_1_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-05-01-05"]|escape}</th>
						<td>{$items["C_2_1_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-05-01-06"]|escape}</th>
						<td>{$items["C_2_1_air_contents"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-05-02"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-05-02-01"]|escape}</th>
						<td>{$items["C_2_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-05-02-02"]|escape}</th>
						<td>{$items["C_2_2_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-05-02-03"]|escape}</th>
						<td>{$items["C_2_2_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-05-02-04"]|escape}</th>
						<td>{$items["C_2_2_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-05-02-05"]|escape}</th>
						<td>{$items["C_2_2_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-05-02-06"]|escape}</th>
						<td>{$items["C_2_2_air_contents"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$items["C_2_3_1"]|escape}&nbsp;</th>
					</tr>
					<tr>
						<th>{$labels["L-05-03-01"]|escape}</th>
						<td>{$items["C_2_3_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-05-03-02"]|escape}</th>
						<td>{$items["C_2_3_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-05-03-03"]|escape}</th>
						<td>{$items["C_2_3_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-05-03-04"]|escape}</th>
						<td>{$items["C_2_3_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-05-03-05"]|escape}</th>
						<td>{$items["C_2_3_6"]|escape}</td>
					</tr>
				</table>

				<!-- 適切な住居の確保に係る支援・生活に必要な契約に係る支援 -->
				<h3 class="title is-6 has-margin-top-30">{$labels["L-06"]|escape}</h3>
				<table class="table is-bordered">
					<tr>
						<th colspan="2">{$labels["L-06-01"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-06-01-01"]|escape}</th>
						<td>{$items["C_3_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-01-02"]|escape}</th>
						<td>{$items["C_3_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-01-03"]|escape}</th>
						<td>{$items["C_3_1_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-01-04"]|escape}</th>
						<td>{$items["C_3_1_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-06-02"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-06-02-01"]|escape}</th>
						<td>{$items["C_3_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-02-02"]|escape}</th>
						<td>{$items["C_3_2_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-02-03"]|escape}</th>
						<td>{$items["C_3_2_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-02-04"]|escape}</th>
						<td>{$items["C_3_2_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-06-03"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-06-03-01"]|escape}</th>
						<td>{$items["C_3_3_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-03-02"]|escape}</th>
						<td>{$items["C_3_3_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-03-03"]|escape}</th>
						<td>{$items["C_3_3_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-03-04"]|escape}</th>
						<td>{$items["C_3_3_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$items["C_3_4_1"]|escape}&nbsp;</th>
					</tr>
					<tr>
						<th>{$labels["L-06-04-01"]|escape}</th>
						<td>{$items["C_3_4_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-04-02"]|escape}</th>
						<td>{$items["C_3_4_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-04-03"]|escape}</th>
						<td>{$items["C_3_4_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-04-04"]|escape}</th>
						<td>{$items["C_3_4_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-04-05"]|escape}</th>
						<td>{$items["C_3_4_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-06-05"]|escape}</th>
					</tr>
					<tr>
						<td colspan="2">{$items["C_3_5_01"]|escape}&nbsp;</td>
					</tr>
					<tr>
						<th>{$labels["L-06-05-02"]|escape}</th>
						<td>
							{$items["C_3_5_02"]|escape}
						</td>
					</tr>
					<tr>
						<th>{$labels["L-06-05-03"]|escape}</th>
						<td>
							{$items["C_3_5_03"]|escape}
						</td>
					</tr>
					<tr>
						<th>{$labels["L-06-05-04"]|escape}</th>
						<td>{$items["C_3_5_04"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-06-06"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-06-06-01"]|escape}</th>
						<td>{$items["C_3_5_05"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-06-02"]|escape}</th>
						<td>{$items["C_3_5_06"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-06-03"]|escape}</th>
						<td>{$items["C_3_5_07"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-06-04"]|escape}</th>
						<td>{$items["C_3_5_08"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-06-05"]|escape}</th>
						<td>{$items["C_3_5_09"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-06-06"]|escape}</th>
						<td>{$items["C_3_5_10"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-06-07"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-06-07-01"]|escape}</th>
						<td>{$items["C_3_6_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-07-02"]|escape}</th>
						<td>{$items["C_3_6_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-07-03"]|escape}</th>
						<td>{$items["C_3_6_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-07-04"]|escape}</th>
						<td>{$items["C_3_6_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-07-05"]|escape}</th>
						<td>{$items["C_3_6_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-07-06"]|escape}</th>
						<td>{$items["C_3_6_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-06-08"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-06-08-01"]|escape}</th>
						<td>{$items["C_3_7_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-08-02"]|escape}</th>
						<td>{$items["C_3_7_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-08-03"]|escape}</th>
						<td>{$items["C_3_7_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-08-04"]|escape}</th>
						<td>{$items["C_3_7_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-08-05"]|escape}</th>
						<td>{$items["C_3_7_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-08-06"]|escape}</th>
						<td>{$items["C_3_7_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$items["C_3_8_1"]|escape}&nbsp;</th>
					</tr>
					<tr>
						<th>{$labels["L-06-09-01"]|escape}</th>
						<td>{$items["C_3_8_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-09-02"]|escape}</th>
						<td>{$items["C_3_8_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-09-03"]|escape}</th>
						<td>{$items["C_3_8_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-09-04"]|escape}</th>
						<td>{$items["C_3_8_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-06-09-05"]|escape}</th>
						<td>{$items["C_3_8_6"]|escape}</td>
					</tr>
				</table>

				<!-- 生活オリエンテーションの実施 -->
				<h3 class="title is-6 has-margin-top-30">{$labels["L-07"]|escape}</h3>
				<table class="table is-bordered">
					<tr>
						<th colspan="2">{$labels["L-07-01"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-07-01-01"]|escape}</th>
						<td>{$items["C_4_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-01-02"]|escape}</th>
						<td>{$items["C_4_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-01-03"]|escape}</th>
						<td>{$items["C_4_1_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-01-04"]|escape}</th>
						<td>{$items["C_4_1_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-01-05"]|escape}</th>
						<td>{$items["C_4_1_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-01-06"]|escape}</th>
						<td>{$items["C_4_1_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-07-02"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-07-02-01"]|escape}</th>
						<td>{$items["C_4_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-02-02"]|escape}</th>
						<td>{$items["C_4_2_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-02-03"]|escape}</th>
						<td>{$items["C_4_2_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-02-04"]|escape}</th>
						<td>{$items["C_4_2_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-02-05"]|escape}</th>
						<td>{$items["C_4_2_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-02-06"]|escape}</th>
						<td>{$items["C_4_2_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-07-03"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-07-03-01"]|escape}</th>
						<td>{$items["C_4_3_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-03-02"]|escape}</th>
						<td>{$items["C_4_3_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-03-03"]|escape}</th>
						<td>{$items["C_4_3_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-03-04"]|escape}</th>
						<td>{$items["C_4_3_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-03-05"]|escape}</th>
						<td>{$items["C_4_3_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-03-06"]|escape}</th>
						<td>{$items["C_4_3_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-07-04"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-07-04-01"]|escape}</th>
						<td>{$items["C_4_4_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-04-02"]|escape}</th>
						<td>{$items["C_4_4_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-04-03"]|escape}</th>
						<td>{$items["C_4_4_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-04-04"]|escape}</th>
						<td>{$items["C_4_4_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-04-05"]|escape}</th>
						<td>{$items["C_4_4_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-04-06"]|escape}</th>
						<td>{$items["C_4_4_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-07-05"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-07-05-01"]|escape}</th>
						<td>{$items["C_4_5_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-05-02"]|escape}</th>
						<td>{$items["C_4_5_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-05-03"]|escape}</th>
						<td>{$items["C_4_5_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-05-04"]|escape}</th>
						<td>{$items["C_4_5_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-05-05"]|escape}</th>
						<td>{$items["C_4_5_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-05-06"]|escape}</th>
						<td>{$items["C_4_5_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-07-06"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-07-06-01"]|escape}</th>
						<td>{$items["C_4_6_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-06-02"]|escape}</th>
						<td>{$items["C_4_6_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-06-03"]|escape}</th>
						<td>{$items["C_4_6_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-06-04"]|escape}</th>
						<td>{$items["C_4_6_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-06-05"]|escape}</th>
						<td>{$items["C_4_6_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-06-06"]|escape}</th>
						<td>{$items["C_4_6_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$items["C_4_7_1"]|escape}&nbsp;</th>
					</tr>
					<tr>
						<th>{$labels["L-07-07-01"]|escape}</th>
						<td>{$items["C_4_7_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-07-02"]|escape}</th>
						<td>{$items["C_4_7_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-07-03"]|escape}</th>
						<td>{$items["C_4_7_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-07-04"]|escape}</th>
						<td>{$items["C_4_7_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-07-05"]|escape}</th>
						<td>{$items["C_4_7_6"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-07-07-06"]|escape}</th>
						<td>{$items["C_4_7_7"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-07-08"]|escape}</th>
					</tr>
					<tr>
						<td colspan="2">{$items["C_4_8"]|escape}&nbsp;</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-07-09"]|escape}</th>
					</tr>
					<tr>
						<td colspan="2">{$items["C_4_9"]|escape}&nbsp;</td>
					</tr>
				</table>

				<!-- 日本語学習の機会の提供 -->
				<h3 class="title is-6 has-margin-top-30">{$labels["L-08"]|escape}</h3>
				<table class="table is-bordered">
					<tr>
						<th colspan="2">{$labels["L-08-01"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-08-01-01"]|escape}</th>
						<td>{$items["C_5_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-08-01-02"]|escape}</th>
						<td>{$items["C_5_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-08-01-03"]|escape}</th>
						<td>{$items["C_5_1_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-08-01-04"]|escape}</th>
						<td>{$items["C_5_1_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-08-02"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-08-02-01"]|escape}</th>
						<td>{$items["C_5_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-08-02-02"]|escape}</th>
						<td>{$items["C_5_2_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-08-02-03"]|escape}</th>
						<td>{$items["C_5_2_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-08-02-04"]|escape}</th>
						<td>{$items["C_5_2_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-08-03"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-08-03-01"]|escape}</th>
						<td>{$items["C_5_3_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-08-03-02"]|escape}</th>
						<td>{$items["C_5_3_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-08-03-03"]|escape}</th>
						<td>{$items["C_5_3_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-08-03-04"]|escape}</th>
						<td>{$items["C_5_3_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$items["C_5_4_1"]|escape}&nbsp;</th>
					</tr>
					<tr>
						<th>{$labels["L-08-04-01"]|escape}</th>
						<td>{$items["C_5_4_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-08-04-02"]|escape}</th>
						<td>{$items["C_5_4_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-08-04-03"]|escape}</th>
						<td>{$items["C_5_4_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-08-04-04"]|escape}</th>
						<td>{$items["C_5_4_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-08-04-05"]|escape}</th>
						<td>{$items["C_5_4_6"]|escape}</td>
					</tr>
				</table>

				<!-- 相談又は苦情への対応 -->
				<h3 class="title is-6 has-margin-top-30">{$labels["L-09"]|escape}</h3>
				<table class="table is-bordered">
					<tr>
						<th colspan="2">{$labels["L-09-01"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-09-01-01"]|escape}</th>
						<td>{$items["C_6_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-01-02"]|escape}</th>
						<td>{$items["C_6_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-01-03"]|escape}</th>
						<td>{$items["C_6_1_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-01-04"]|escape}</th>
						<td>{$items["C_6_1_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-09-02"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-09-02-01"]|escape}</th>
						<td>{$items["C_6_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-02-02"]|escape}</th>
						<td>{$items["C_6_2_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-02-03"]|escape}</th>
						<td>{$items["C_6_2_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-02-04"]|escape}</th>
						<td>{$items["C_6_2_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$items["C_6_3_1"]|escape}&nbsp;</th>
					</tr>
					<tr>
						<th>{$labels["L-09-03-01"]|escape}</th>
						<td>{$items["C_6_3_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-03-02"]|escape}</th>
						<td>{$items["C_6_3_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-03-03"]|escape}</th>
						<td>{$items["C_6_3_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-03-04"]|escape}</th>
						<td>{$items["C_6_3_5"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-09-04"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-09-04-01"]|escape}</th>
						<td colspan="2">
							<div class="has-margin-top-10">
								{$labels["L-09-04-01-01"]|escape}：
								{$items["monday_time"]|escape}
							</div>
							<div class="has-margin-top-10">
								{$labels["L-09-04-01-02"]|escape}：
								{$items["tuesday_time"]|escape}
							</div>
							<div class="has-margin-top-10">
								{$labels["L-09-04-01-03"]|escape}：
								{$items["wednesday_time"]|escape}
							</div>
							<div class="has-margin-top-10">
								{$labels["L-09-04-01-04"]|escape}：
								{$items["thursday_time"]|escape}
							</div>
							<div class="has-margin-top-10">
								{$labels["L-09-04-01-05"]|escape}：
								{$items["friday_time"]|escape}
							</div>
						</td>
					</tr>
					<tr>
						<th>{$labels["L-09-04-02"]|escape}</th>
						<td>
							<div class="has-margin-top-10">
								{$labels["L-09-04-02-01"]|escape}：
								{$items["saturday_time"]|escape}
							</div>
							<div class="has-margin-top-10">
								{$labels["L-09-04-02-02"]|escape}：
								{$items["sunday_time"]|escape}
							</div>
							<div class="has-margin-top-10">
								{$labels["L-09-04-02-03"]|escape}：
								{$items["holiday_time"]|escape}
							</div>
						</td>
					</tr>
					<tr>
						<th>{$labels["L-09-04-02-03"]|escape}</th>
						<td>{$items["C_6_8_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-04-04"]|escape}</th>
						<td>{$items["C_6_8_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-04-05"]|escape}</th>
						<td>{$items["C_6_8_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-04-06"]|escape}</th>
						<td>{$items["C_6_8_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-04-07"]|escape}</th>
						<td>{$items["C_6_9_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-04-08"]|escape}</th>
						<td>{$items["C_6_9_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-04-09"]|escape}</th>
						<td>{$items["C_6_9_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-09-04-10"]|escape}</th>
						<td>{$items["C_6_9_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-09-05"]|escape}&nbsp;</th>
					</tr>
					<tr>
						<td colspan="2">{$items["C_6_9_5"]|escape}&nbsp;</td>
					</tr>
				</table>

				<!-- 日本人との交流促進に係る支援 -->
				<h3 class="title is-6 has-margin-top-30">{$labels["L-10"]|escape}</h3>
				<table class="table is-bordered">
					<tr>
						<th colspan="2">{$labels["L-10-01"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-10-01-01"]|escape}</th>
						<td>{$items["C_7_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-10-01-02"]|escape}</th>
						<td>{$items["C_7_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-10-01-03"]|escape}</th>
						<td>{$items["C_7_1_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-10-01-04"]|escape}</th>
						<td>{$items["C_7_1_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-10-02"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-10-02-01"]|escape}</th>
						<td>{$items["C_7_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-10-02-02"]|escape}</th>
						<td>{$items["C_7_2_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-10-02-03"]|escape}</th>
						<td>{$items["C_7_2_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-10-02-04"]|escape}</th>
						<td>{$items["C_7_2_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$items["C_7_3_1"]|escape}&nbsp;</th>
					</tr>
					<tr>
						<th>{$labels["L-10-03-01"]|escape}</th>
						<td>{$items["C_7_3_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-10-03-02"]|escape}</th>
						<td>{$items["C_7_3_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-10-03-03"]|escape}</th>
						<td>{$items["C_7_3_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-10-03-04"]|escape}</th>
						<td>{$items["C_7_3_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-10-03-05"]|escape}</th>
						<td>{$items["C_7_3_6"]|escape}</td>
					</tr>
				</table>

				<!-- 非自発的離職時の転職支援 -->
				<h3 class="title is-6 has-margin-top-30">{$labels["L-11"]|escape}</h3>
				<table class="table is-bordered">
					<tr>
						<th colspan="2">{$labels["L-11-01"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-11-01-01"]|escape}</th>
						<td>{$items["C_8_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-01-02"]|escape}</th>
						<td>{$items["C_8_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-01-03"]|escape}</th>
						<td>{$items["C_8_1_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-01-04"]|escape}</th>
						<td>{$items["C_8_1_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-11-02"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-11-02-01"]|escape}</th>
						<td>{$items["C_8_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-02-02"]|escape}</th>
						<td>{$items["C_8_2_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-02-03"]|escape}</th>
						<td>{$items["C_8_2_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-02-04"]|escape}</th>
						<td>{$items["C_8_2_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-11-03"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-11-03-01"]|escape}</th>
						<td>{$items["C_8_3_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-03-02"]|escape}</th>
						<td>{$items["C_8_3_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-03-03"]|escape}</th>
						<td>{$items["C_8_3_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-03-04"]|escape}</th>
						<td>{$items["C_8_3_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-11-04"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-11-04-01"]|escape}</th>
						<td>{$items["C_8_4_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-04-02"]|escape}</th>
						<td>{$items["C_8_4_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-04-03"]|escape}</th>
						<td>{$items["C_8_4_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-04-04"]|escape}</th>
						<td>{$items["C_8_4_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-11-05"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-11-05-01"]|escape}</th>
						<td>{$items["C_8_5_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-05-02"]|escape}</th>
						<td>{$items["C_8_5_2"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-11-06"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-11-06-01"]|escape}</th>
						<td>{$items["C_8_6_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-06-02"]|escape}</th>
						<td>{$items["C_8_6_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-06-03"]|escape}</th>
						<td>{$items["C_8_6_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-06-04"]|escape}</th>
						<td>{$items["C_8_6_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-06-05"]|escape}</th>
						<td>{$items["C_8_6_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-06-06"]|escape}</th>
						<td>{$items["C_8_6_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-11-07"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-11-07-01"]|escape}</th>
						<td>{$items["C_8_7_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-07-02"]|escape}</th>
						<td>{$items["C_8_7_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-07-03"]|escape}</th>
						<td>{$items["C_8_7_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-07-04"]|escape}</th>
						<td>{$items["C_8_7_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$items["C_8_8_1"]|escape}&nbsp;</th>
					</tr>
					<tr>
						<th>{$labels["L-11-08-01"]|escape}</th>
						<td>{$items["C_8_8_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-08-02"]|escape}</th>
						<td>{$items["C_8_8_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-08-03"]|escape}</th>
						<td>{$items["C_8_8_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-08-04"]|escape}</th>
						<td>{$items["C_8_8_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-11-08-05"]|escape}</th>
						<td>{$items["C_8_8_6"]|escape}</td>
					</tr>
				</table>

				<!-- 実施方法 -->
				<h3 class="title is-6 has-margin-top-30">{$labels["L-11-08-05"]|escape}</h3>
				<table class="table is-bordered">
					<tr>
						<th colspan="2">{$labels["L-12-01"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-12-01-01"]|escape}</th>
						<td>{$items["C_9_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-01-02"]|escape}</th>
						<td>{$items["C_9_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-01-03"]|escape}</th>
						<td>{$items["C_9_1_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-01-04"]|escape}</th>
						<td>{$items["C_9_1_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-01-05"]|escape}</th>
						<td>{$items["C_9_1_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-01-06"]|escape}</th>
						<td>{$items["C_9_1_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-12-02"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-12-02-01"]|escape}</th>
						<td>{$items["C_9_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-02-02"]|escape}</th>
						<td>{$items["C_9_2_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-02-03"]|escape}</th>
						<td>{$items["C_9_2_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-02-04"]|escape}</th>
						<td>{$items["C_9_2_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-02-05"]|escape}</th>
						<td>{$items["C_9_2_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-02-06"]|escape}</th>
						<td>{$items["C_9_2_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-12-03"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-12-03-01"]|escape}</th>
						<td>{$items["C_9_3_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-03-02"]|escape}</th>
						<td>{$items["C_9_3_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-03-03"]|escape}</th>
						<td>{$items["C_9_3_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-03-04"]|escape}</th>
						<td>{$items["C_9_3_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-12-04"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-12-04-01"]|escape}</th>
						<td>{$items["C_9_4_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-04-02"]|escape}</th>
						<td>{$items["C_9_4_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-04-03"]|escape}</th>
						<td>{$items["C_9_4_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-04-04"]|escape}</th>
						<td>{$items["C_9_4_4"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$items["C_9_5_1"]|escape}&nbsp;</th>
					</tr>
					<tr>
						<th>{$labels["L-12-05-01"]|escape}</th>
						<td>{$items["C_9_5_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-05-02"]|escape}</th>
						<td>{$items["C_9_5_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-05-03"]|escape}</th>
						<td>{$items["C_9_5_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-05-04"]|escape}</th>
						<td>{$items["C_9_5_5"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-12-05-05"]|escape}</th>
						<td>{$items["C_9_5_6"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-12-06"]|escape}</th>
					</tr>
					<tr>
						<td colspan="2">{$items["C_9_6"]|escape}&nbsp;</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-12-07"]|escape}</th>
					</tr>
					<tr>
						<td colspan="2">{$items["C_9_7"]|escape}&nbsp;</td>
					</tr>
				</table>

{if $smarty.session.loginUserType == '1'}
				<div class="has-margin-top-30 has-text-centered">
					<div class="is-pulled-left">
						<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
					</div>
{if $smarty.session.loginUserAuth <= '2'}
					<input type="file" name="upfile" id="upfile" style="display:none;" />
					<button type="button" class="button is-primary has-margin-right-20" id="csv_import_button">{$labels["csv_import"]|escape}</button>
					<button type="button" class="button is-primary has-margin-right-20" id="csv_export_button">{$labels["csv_export"]|escape}</button>
					<button type="button" class="button is-primary has-margin-right-20" id="csv_format_button">{$labels["csv_format"]|escape}</button>
{/if}
				</div>
{/if}

			</div>
			<div class="column is-3">
				<aside class="menu">
					<ul class="menu-list">
						<li><a href="./WK02001">{$labels["sub_menu_01"]|escape}</a></li>
						<li><a href="./WK02002">{$labels["sub_menu_02"]|escape}</a></li>
						<li>
							<a class="acMenu">{$labels["sub_menu_03"]|escape}</a>
							<ul>
								<li><a href="./WK02003">{$labels["sub_menu_03_01"]|escape}</a></li>
								<li><a href="./WK02013">{$labels["sub_menu_03_02"]|escape}</a></li>
								<li><a href="./WK02014">{$labels["sub_menu_03_03"]|escape}</a></li>
								<li><a class="is-active" href="./WK02004">{$labels["sub_menu_03_04"]|escape}</a></li>
								<li><a href="./WK02005">{$labels["sub_menu_03_05"]|escape}</a></li>
								<li><a href="./WK02006">{$labels["sub_menu_03_06"]|escape}</a></li>
								<li><a href="./WK02007">{$labels["sub_menu_03_07"]|escape}</a></li>
								<li><a href="./WK02008">{$labels["sub_menu_03_08"]|escape}</a></li>
								<li><a href="./WK02009">{$labels["sub_menu_03_09"]|escape}</a></li>
								<li><a href="./WK02010">{$labels["sub_menu_03_10"]|escape}</a></li>
							</ul>
						</li>
						<li><a href="./WK02011">{$labels["sub_menu_04"]|escape}</a></li>
						<li><a href="./WK02012">{$labels["sub_menu_05"]|escape}</a></li>
					</ul>
				</aside>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</body>
</html>
