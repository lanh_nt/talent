<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/WK02/WK02013.js"></script>
<script type="text/javascript" src="../js/WK02/WK02.js"></script>
</head>

<body id="WK02013">
<form id="mainForm" enctype="multipart/form-data" method="POST" action="WK02013">
<input type="hidden" name="mode" id="mode" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{if $smarty.session.loginUserType == '1'}{$labels["L-title-A"]|escape}{else}{$labels["L-title-C"]|escape}{/if}</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

		<div class="columns sidemenu">
			<div class="column is-9">
				<h3 class="title is-5">{$labels["L-sub"]|escape}</h3>

				<!-- 更新許可申請書　固有項目 -->
				<h4 class="title is-6 has-margin-top-30">{$labels["L-01-01"]|escape}</h4>
				<table class="table is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-01-02"]|escape}</th>
						<td>{$items["A_09_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03"]|escape}</th>
						<td>{$items["A_09_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-04"]|escape}</th>
						<td>{$items["A_09_3_ymd"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-05"]|escape}</th>
						<td>{$items["A_10"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-06"]|escape}</th>
						<td>{$items["A_11"]|escape}</td>
					</tr>				
					<tr>
						<th>{$labels["L-01-07"]|escape}</th>
						<td>{$items["A_12"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-08"]|escape}</th>
						<td>{$items["A_13"]|escape}</td>
					</tr>
					<tr>
						<th rowspan="4">{$labels["L-02-18"]|escape}</th>
						<td>
							<ul>
								<li>{$labels["L-02-18-05-01"]|escape}：{$items["A_19_5_1"]|escape}</li>
								<li>{$labels["L-02-18-05-02"]|escape}：{$items["A_19_5_2"]|escape}</li>
								<li>{$labels["L-02-18-05-03"]|escape}：{$items["A_19_5_3_ymd"]|escape}</li>
								<li>{$labels["L-02-18-05-04"]|escape}：{$items["A_19_5_4"]|escape}</li>
								<li>{$labels["L-02-18-05-05"]|escape}：{$items["A_19_5_5"]|escape}</li>
								<li>{$labels["L-02-18-05-06"]|escape}：{$items["A_19_5_6"]|escape}</li>
								<li>{$labels["L-02-18-05-07"]|escape}：{$items["A_19_5_7"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-02-18-06-01"]|escape}：{$items["A_19_6_1"]|escape}</li>
								<li>{$labels["L-02-18-06-02"]|escape}：{$items["A_19_6_2"]|escape}</li>
								<li>{$labels["L-02-18-06-03"]|escape}：{$items["A_19_6_3_ymd"]|escape}</li>
								<li>{$labels["L-02-18-06-04"]|escape}：{$items["A_19_6_4"]|escape}</li>
								<li>{$labels["L-02-18-06-05"]|escape}：{$items["A_19_6_5"]|escape}</li>
								<li>{$labels["L-02-18-06-06"]|escape}：{$items["A_19_6_6"]|escape}</li>
								<li>{$labels["L-02-18-06-07"]|escape}：{$items["A_19_6_7"]|escape}</li>
							</ul>
						</td>
					</tr>
				<table>

				<!-- 申請人等作成 -->
				<h4 class="title is-6 has-margin-top-30">{$labels["L-02"]|escape}</h4>
				<table class="table is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-02-01"]|escape}</th>
						<td>{$items["nationality_region"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-02"]|escape}</th>
						<td>{$items["birthday_ymd"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-03"]|escape}</th>
						<td>{$items["family_name"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-04"]|escape}</th>
						<td>{$items["given_name"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-05"]|escape}</th>
						<td>{$items["sex"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-06"]|escape}</th>
						<td>{$items["place_birth"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-07"]|escape}</th>
						<td>{$items["marital_flag"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-08"]|escape}</th>
						<td>{$items["occupation"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-09"]|escape}</th>
						<td>{$items["home_town"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-10"]|escape}</th>
						<td>{$items["address"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-11"]|escape}</th>
						<td>{$items["telephone"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-12"]|escape}</th>
						<td>{$items["cellular_phone"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-13"]|escape}</th>
						<td>{$items["A_01_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-14"]|escape}</th>
						<td>{$items["A_01_2_ymd"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-15"]|escape}</th>
						<td>{$items["A_15_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-16"]|escape}</th>
						<td>{$items["A_15_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-17"]|escape}</th>
						<td>{$items["A_18_1"]|escape}</td>
					</tr>

					<tr>
						<th rowspan="4">{$labels["L-02-18"]|escape}</th>
						<td>
							<ul>
								<li>{$labels["L-02-18-01-01"]|escape}：{$items["A_19_1_1"]|escape}</li>
								<li>{$labels["L-02-18-01-02"]|escape}：{$items["A_19_1_2"]|escape}</li>
								<li>{$labels["L-02-18-01-03"]|escape}：{$items["A_19_1_3_ymd"]|escape}</li>
								<li>{$labels["L-02-18-01-04"]|escape}：{$items["A_19_1_4"]|escape}</li>
								<li>{$labels["L-02-18-01-05"]|escape}：{$items["A_19_1_5"]|escape}</li>
								<li>{$labels["L-02-18-01-06"]|escape}：{$items["A_19_1_6"]|escape}</li>
								<li>{$labels["L-02-18-01-07"]|escape}：{$items["A_19_1_7"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-02-18-02-01"]|escape}：{$items["A_19_2_1"]|escape}</li>
								<li>{$labels["L-02-18-02-02"]|escape}：{$items["A_19_2_2"]|escape}</li>
								<li>{$labels["L-02-18-02-03"]|escape}：{$items["A_19_2_3_ymd"]|escape}</li>
								<li>{$labels["L-02-18-02-04"]|escape}：{$items["A_19_2_4"]|escape}</li>
								<li>{$labels["L-02-18-02-05"]|escape}：{$items["A_19_2_5"]|escape}</li>
								<li>{$labels["L-02-18-02-06"]|escape}：{$items["A_19_2_6"]|escape}</li>
								<li>{$labels["L-02-18-02-07"]|escape}：{$items["A_19_2_7"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-02-18-03-01"]|escape}：{$items["A_19_3_1"]|escape}</li>
								<li>{$labels["L-02-18-03-02"]|escape}：{$items["A_19_3_2"]|escape}</li>
								<li>{$labels["L-02-18-03-03"]|escape}：{$items["A_19_3_3_ymd"]|escape}</li>
								<li>{$labels["L-02-18-03-04"]|escape}：{$items["A_19_3_4"]|escape}</li>
								<li>{$labels["L-02-18-03-05"]|escape}：{$items["A_19_3_5"]|escape}</li>
								<li>{$labels["L-02-18-03-06"]|escape}：{$items["A_19_3_6"]|escape}</li>
								<li>{$labels["L-02-18-03-07"]|escape}：{$items["A_19_3_7"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-02-18-04-01"]|escape}：{$items["A_19_4_1"]|escape}</li>
								<li>{$labels["L-02-18-04-02"]|escape}：{$items["A_19_4_2"]|escape}</li>
								<li>{$labels["L-02-18-04-03"]|escape}：{$items["A_19_4_3_ymd"]|escape}</li>
								<li>{$labels["L-02-18-04-04"]|escape}：{$items["A_19_4_4"]|escape}</li>
								<li>{$labels["L-02-18-04-05"]|escape}：{$items["A_19_4_5"]|escape}</li>
								<li>{$labels["L-02-18-04-06"]|escape}：{$items["A_19_4_6"]|escape}</li>
								<li>{$labels["L-02-18-04-07"]|escape}：{$items["A_19_4_7"]|escape}</li>
							</ul>
						</td>
					</tr>

					<tr>
						<th>{$labels["L-02-19"]|escape}</th>
						<td>{$items["A_20_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-20"]|escape}</th>
						<td>{$items["A_20_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-21"]|escape}</th>
						<td>{$items["A_20_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-22"]|escape}</th>
						<td>{$items["A_21_1"]|escape}</td>
					</tr>

					<tr>
						<th rowspan="3">{$labels["L-02-23-01"]|escape}</th>
						<td>{$items["A_21_3"]|escape}&nbsp;</td>
					</tr>
					<tr>
						<td>{$items["A_21_4"]|escape}&nbsp;</td>
					</tr>
					<tr>
						<td>{$items["A_21_5"]|escape}&nbsp;</td>
					</tr>

					<tr>
						<th>{$labels["L-02-23-02"]|escape}</th>
						<td>{$items["A_21_7"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-24"]|escape}</th>
						<td>{$items["A_22_1"]|escape}</td>
					</tr>

					<tr>
						<th rowspan="3">{$labels["L-02-25-01"]|escape}</th>
						<td>{$items["A_22_3"]|escape}&nbsp;</td>
					</tr>
					<tr>
						<td>{$items["A_22_4"]|escape}&nbsp;</td>
					</tr>
					<tr>
						<td>{$items["A_22_5"]|escape}&nbsp;</td>
					</tr>

					<tr>
						<th>{$labels["L-02-25-02"]|escape}</th>
						<td>{$items["A_22_7"]|escape}&nbsp;</td>
					</tr>

					<tr>
						<th>{$labels["L-02-26"]|escape}</th>
						<td>{$items["A_23_1_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-27"]|escape}</th>
						<td>{$items["A_23_1_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-28"]|escape}</th>
						<td>{$items["A_23_1_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-29"]|escape}</th>
						<td>{$items["A_23_2_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-30"]|escape}</th>
						<td>{$items["A_23_2_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-31"]|escape}</th>
						<td>{$items["A_23_2_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-32"]|escape}</th>
						<td>{$items["A_24_ym"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-33"]|escape}</th>
						<td>{$items["A_25_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-34"]|escape}</th>
						<td>{$items["A_25_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-35"]|escape}</th>
						<td>{$items["A_25_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-36"]|escape}</th>
						<td>{$items["A_26_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-37"]|escape}</th>
						<td>{$items["A_26_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-38"]|escape}</th>
						<td>{$items["A_26_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-39"]|escape}</th>
						<td>{$items["A_27"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-40"]|escape}</th>
						<td>{$items["A_28"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-41"]|escape}</th>
						<td>{$items["A_29"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-42"]|escape}</th>
						<td>{$items["A_30"]|escape}</td>
					</tr>

					<tr>
						<th rowspan="6">{$labels["L-02-43"]|escape}</th>
						<td>
							<ul>
								<li>{$labels["L-02-43-01-01"]|escape}：{$items["A_31_1_1_ymd"]|escape}</li>
								<li>{$labels["L-02-43-01-02"]|escape}：{$items["A_31_1_2_ymd"]|escape}</li>
								<li>{$labels["L-02-43-01-03"]|escape}：{$items["A_31_1_3"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-02-43-02-01"]|escape}：{$items["A_31_2_1_ymd"]|escape}</li>
								<li>{$labels["L-02-43-02-02"]|escape}：{$items["A_31_2_2_ymd"]|escape}</li>
								<li>{$labels["L-02-43-02-03"]|escape}：{$items["A_31_2_3"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-02-43-03-01"]|escape}：{$items["A_31_3_1_ymd"]|escape}</li>
								<li>{$labels["L-02-43-03-02"]|escape}：{$items["A_31_3_2_ymd"]|escape}</li>
								<li>{$labels["L-02-43-03-03"]|escape}：{$items["A_31_3_3"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-02-43-04-01"]|escape}：{$items["A_31_4_1_ymd"]|escape}</li>
								<li>{$labels["L-02-43-04-02"]|escape}：{$items["A_31_4_2_ymd"]|escape}</li>
								<li>{$labels["L-02-43-04-03"]|escape}：{$items["A_31_4_3"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-02-43-05-01"]|escape}：{$items["A_31_5_1_ymd"]|escape}</li>
								<li>{$labels["L-02-43-05-02"]|escape}：{$items["A_31_5_2_ymd"]|escape}</li>
								<li>{$labels["L-02-43-05-03"]|escape}：{$items["A_31_5_3"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-02-43-06-01"]|escape}：{$items["A_31_6_1_ymd"]|escape}</li>
								<li>{$labels["L-02-43-06-02"]|escape}：{$items["A_31_6_2_ymd"]|escape}</li>
								<li>{$labels["L-02-43-06-03"]|escape}：{$items["A_31_6_3"]|escape}</li>
							</ul>
						</td>
					</tr>

					<tr>
						<th>{$labels["L-02-44"]|escape}</th>
						<td>{$items["A_32"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-45"]|escape}</th>
						<td>{$items["A_33"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-46"]|escape}</th>
						<td>{$items["A_34"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-47"]|escape}</th>
						<td>{$items["A_35"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-48"]|escape}</th>
						<td>{$items["A_36"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-49"]|escape}</th>
						<td>{$items["A_37"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-50"]|escape}</th>
						<td>{$items["A_38"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-51"]|escape}</th>
						<td>{$items["A_39"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-52"]|escape}</th>
						<td>{$items["A_40"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-53"]|escape}</th>
						<td>{$items["A_41"]|escape}</td>
					</tr>

				</table><!--  /.table -->
{if $smarty.session.loginUserAuth <= '2' || $smarty.session.loginUserAuth == '9'}
				<div class="has-margin-top-30 has-text-centered">
					<button type="button" class="button is-primary" id="udate_button">{$labels["edit"]|escape}</button>
				</div>
{/if}
				<!-- 所属機関等作成 -->
				<h4 class="title is-6 has-margin-top-30">{$labels["L-03"]|escape}</h4>
				<table class="table is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-03-01"]|escape}</th>
						<td>{$items["userName"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-02"]|escape}</th>
						<td>{$items["B_1_01_1_ymd"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-03"]|escape}</th>
						<td>{$items["B_1_01_2_ymd"]|escape}</td>
					</tr>

					<tr>
						<th rowspan="3">{$labels["L-03-04"]|escape}</th>
						<td>{$items["B_1_02_1"]|escape|default:'&nbsp;'}</td>
					</tr>
					<tr>
						<td>{$items["B_1_02_2"]|escape|default:'&nbsp;'}</td>
					</tr>
					<tr>
						<td>{$items["B_1_02_3"]|escape|default:'&nbsp;'}</td>
					</tr>

					<tr>
						<th rowspan="3">{$labels["L-03-05"]|escape}</th>
						<td>{$items["B_1_02_4"]|escape|default:'&nbsp;'}</td>
					</tr>
					<tr>
						<td>{$items["B_1_02_5"]|escape|default:'&nbsp;'}</td>
					</tr>
					<tr>
						<td>{$items["B_1_02_6"]|escape|default:'&nbsp;'}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-06"]|escape}</th>
						<td>{$items["B_1_02_7"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-07"]|escape}</th>
						<td>{$items["B_1_02_8"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-08"]|escape}</th>
						<td>{$items["B_1_03_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-09"]|escape}</th>
						<td>{$items["B_1_03_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-10"]|escape}</th>
						<td>{$items["B_1_04_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-11"]|escape}</th>
						<td>{$items["B_1_04_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-12"]|escape}</th>
						<td>{$items["B_1_04_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-13"]|escape}</th>
						<td>{$items["B_1_05"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-14"]|escape}</th>
						<td>{$items["B_1_06_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-15"]|escape}</th>
						<td>{$items["B_1_06_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-16"]|escape}</th>
						<td>{$items["B_1_07"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-17"]|escape}</th>
						<td>{$items["B_1_08"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-18"]|escape}</th>
						<td>{$items["B_1_09"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-19"]|escape}</th>
						<td>{$items["B_1_10"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-20"]|escape}</th>
						<td>{$items["B_1_11"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-21"]|escape}</th>
						<td>{$items["B_1_12_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-22"]|escape}</th>
						<td>{$items["B_1_12_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-23"]|escape}</th>
						<td>{$items["B_1_12_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-24"]|escape}</th>
						<td>{$items["B_1_12_4"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-25"]|escape}</th>
						<td>{$items["B_1_12_5"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-26"]|escape}</th>
						<td>{$items["B_1_12_6_ymd"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-27"]|escape}</th>
						<td>{$items["B_1_12_7_ymd"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-28"]|escape}</th>
						<td>{$items["B_1_13_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-29"]|escape}</th>
						<td>{$items["B_1_13_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-30"]|escape}</th>
						<td>{$items["B_1_13_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-31"]|escape}</th>
						<td>{$items["B_1_13_4"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-32"]|escape}</th>
						<td>{$items["B_1_13_5"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-33"]|escape}</th>
						<td>{$items["B_1_13_6_ymd"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-34"]|escape}</th>
						<td>{$items["B_1_14_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-35"]|escape}</th>
						<td>{$items["B_1_14_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-03-36"]|escape}</th>
						<td>{$items["B_1_14_3"]|escape}</td>
					</tr>
				</table><!--  /.table -->

				<!-- 特定技能所属機関 -->
				<h4 class="title is-6 has-margin-top-30">{$labels["L-04"]|escape}</h4>
				<table class="table is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-04-01"]|escape}</th>
						<td>{$items["company_name"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-02"]|escape}</th>
						<td>{$items["corporation_no"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-03"]|escape}</th>
						<td>{$items["industry_type"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-04"]|escape}</th>
						<td>{$items["cmp_address"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-05"]|escape}</th>
						<td>{$items["capital"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-06"]|escape}</th>
						<td>{$items["annual_sales_amount"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-07"]|escape}</th>
						<td>{$items["company_stuff_count"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-08"]|escape}</th>
						<td>{$items["representative_name"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-09"]|escape}</th>
						<td>{$items["support_company_name"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-10"]|escape}</th>
						<td>{$items["support_address"]|escape}</td>
					</tr>


					<tr>
						<th>{$labels["L-04-11"]|escape}</th>
						<td>{$items["B_2_09_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-12"]|escape}</th>
						<td>{$items["B_2_09_4"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-13"]|escape}</th>
						<td>{$items["B_2_09_5"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-14"]|escape}</th>
						<td>{$items["B_2_10_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-15"]|escape}</th>
						<td>{$items["B_2_10_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-16"]|escape}</th>
						<td>{$items["B_2_11_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-17"]|escape}</th>
						<td>{$items["B_2_11_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-18"]|escape}</th>
						<td>{$items["B_2_12_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-19"]|escape}</th>
						<td>{$items["B_2_12_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-20"]|escape}</th>
						<td>{$items["B_2_13_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-21"]|escape}</th>
						<td>{$items["B_2_13_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-22"]|escape}</th>
						<td>{$items["B_2_14_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-23"]|escape}</th>
						<td>{$items["B_2_14_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-24"]|escape}</th>
						<td>{$items["B_2_15_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-25"]|escape}</th>
						<td>{$items["B_2_15_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-26"]|escape}</th>
						<td>{$items["B_2_16_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-27"]|escape}</th>
						<td>{$items["B_2_16_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-28"]|escape}</th>
						<td>{$items["B_2_17_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-29"]|escape}</th>
						<td>{$items["B_2_17_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-30"]|escape}</th>
						<td>{$items["B_2_18_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-31"]|escape}</th>
						<td>{$items["B_2_18_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-32"]|escape}</th>
						<td>{$items["B_2_19_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-33"]|escape}</th>
						<td>{$items["B_2_19_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-34-1"]|escape}</th>
						<td>{$items["B_2_20_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-34-2"]|escape}</th>
						<td>{$items["B_2_20_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-35"]|escape}</th>
						<td>{$items["B_2_21_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-36"]|escape}</th>
						<td>{$items["B_2_21_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-37"]|escape}</th>
						<td>{$items["B_2_22"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-38"]|escape}</th>
						<td>{$items["B_2_23_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-39"]|escape}</th>
						<td>{$items["B_2_23_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-40"]|escape}</th>
						<td>{$items["B_2_24_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-41"]|escape}</th>
						<td>{$items["B_2_24_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-42"]|escape}</th>
						<td>{$items["B_2_25"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-43"]|escape}<br>
							{$labels["L-04-43-01"]|escape}<br>
							{$labels["L-04-43-02"]|escape}<br>
							{$labels["L-04-43-03"]|escape}<br>
							{$labels["L-04-43-04"]|escape}

						</th>
						<td>
							{$items["B_2_26_1"]|escape}<br>
							{$items["B_2_26_2"]|escape}
						</td>
					</tr>
					</tr>
						<th>{$labels["L-04-43-05"]|escape}</th>
						<td>{$items["B_2_26_3_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-43-06"]|escape}</th>
						<td>{$items["B_2_26_3_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-04-43-07"]|escape}</th>
						<td>{$items["B_2_26_3_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-45"]|escape}</th>
						<td>{$items["B_2_27_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-46"]|escape}</th>
						<td>{$items["B_2_27_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-47"]|escape}</th>
						<td>{$items["B_2_28_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-48"]|escape}</th>
						<td>{$items["B_2_28_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-49"]|escape}</th>
						<td>{$items["B_2_29"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-50"]|escape}</th>
						<td>{$items["B_2_30"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-51"]|escape}</th>
						<td>{$items["B_2_31"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-52"]|escape}</th>
						<td>{$items["support_manager_name"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-53"]|escape}</th>
						<td>{$items["support_manager_department"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-54"]|escape}</th>
						<td>{$items["B_2_32"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-55"]|escape}</th>
						<td>{$items["support_handler_name"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-56"]|escape}</th>
						<td>{$items["support_handler_department"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-57"]|escape}</th>
						<td>{$items["B_2_33"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-58"]|escape}<br>
							{$labels["L-04-58-01"]|escape}<br>
							{$labels["L-04-58-02"]|escape}<br>
							{$labels["L-04-58-03"]|escape}

						</th>
						<td>
						{$items["B_2_34_1"]|escape}<br>
						{$items["B_2_34_2"]|escape}
						</td>
					</tr>

					<tr>
						<th>{$labels["L-04-59"]|escape}</th>
						<td>{$items["B_2_34_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-60"]|escape}</th>
						<td>{$items["B_2_35"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-61"]|escape}</th>
						<td>{$items["B_2_36"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-62"]|escape}</th>
						<td>{$items["B_2_37"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-63"]|escape}</th>
						<td>{$items["B_2_38_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-64"]|escape}</th>
						<td>{$items["B_2_38_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-65"]|escape}</th>
						<td>{$items["B_2_39"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-04-66"]|escape}</th>
						<td>{$items["B_2_40"]|escape}</td>
					</tr>
				</table>
				<!-- 1号特定技能外国人支援計画 -->
				<h4 class="title is-6 has-margin-top-30">{$labels["L-05"]|escape}</h4>
				<table class="table is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-05-01"]|escape}</th>
						<td>{$items["B_3_01"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-02"]|escape}</th>
						<td>{$items["B_3_02"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-03"]|escape}</th>
						<td>{$items["B_3_03"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-04"]|escape}</th>
						<td>{$items["B_3_04"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-05"]|escape}</th>
						<td>{$items["B_3_05"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-06"]|escape}</th>
						<td>{$items["B_3_06"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-07"]|escape}</th>
						<td>{$items["B_3_07"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-08"]|escape}</th>
						<td>{$items["B_3_08"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-09"]|escape}</th>
						<td>{$items["B_3_09"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-10"]|escape}</th>
						<td>{$items["B_3_10"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-11"]|escape}</th>
						<td>{$items["B_3_11"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-12"]|escape}</th>
						<td>{$items["B_3_12"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-13"]|escape}</th>
						<td>{$items["B_3_13"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-14"]|escape}</th>
						<td>{$items["B_3_14"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-15"]|escape}</th>
						<td>{$items["B_3_15"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-05-16"]|escape}</th>
						<td>{$items["B_3_16"]|escape}</td>
					</tr>
				</table><!--  /.table -->
{if $smarty.session.loginUserType == '1'}
{if $smarty.session.loginUserAuth <= '2'}
				<div class="has-margin-top-30 has-text-centered">
					<input type="file" name="upfile" id="upfile" style="display:none;" />
					<button type="button" class="button is-primary has-margin-right-20" id="csv_import_button">{$labels["csv_import"]|escape}</button>
					<button type="button" class="button is-primary has-margin-right-20" id="csv_export_button">{$labels["csv_export"]|escape}</button>
					<button type="button" class="button is-primary has-margin-right-20" id="csv_format_button">{$labels["csv_format"]|escape}</button>
				</div>
{/if}
{/if}
				<h4 class="title is-6 has-margin-top-30">{$labels["L-06"]|escape}</h4>
				<table class="table is-bordered has-width-100pct">
					<tr>
						<td colspan="2">{$items["support_campany"]|escape}</td>
					</tr>
				</table>

{if $smarty.session.loginUserType == '1'}
				<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
{/if}

			</div>
			<div class="column is-3">
				<aside class="menu">
					<ul class="menu-list">
						<li><a href="./WK02001">{$labels["sub_menu_01"]|escape}</a></li>
						<li><a href="./WK02002">{$labels["sub_menu_02"]|escape}</a></li>
						<li>
							<a class="acMenu">{$labels["sub_menu_03"]|escape}</a>
							<ul>
								<li><a href="./WK02003">{$labels["sub_menu_03_01"]|escape}</a></li>
								<li><a class="is-active" href="./WK02013">{$labels["sub_menu_03_02"]|escape}</a></li>
								<li><a href="./WK02014">{$labels["sub_menu_03_03"]|escape}</a></li>
								<li><a href="./WK02004">{$labels["sub_menu_03_04"]|escape}</a></li>
								<li><a href="./WK02005">{$labels["sub_menu_03_05"]|escape}</a></li>
								<li><a href="./WK02006">{$labels["sub_menu_03_06"]|escape}</a></li>
								<li><a href="./WK02007">{$labels["sub_menu_03_07"]|escape}</a></li>
								<li><a href="./WK02008">{$labels["sub_menu_03_08"]|escape}</a></li>
								<li><a href="./WK02009">{$labels["sub_menu_03_09"]|escape}</a></li>
								<li><a href="./WK02010">{$labels["sub_menu_03_10"]|escape}</a></li>
							</ul>
						</li>
						<li><a href="./WK02011">{$labels["sub_menu_04"]|escape}</a></li>
						<li><a href="./WK02012">{$labels["sub_menu_05"]|escape}</a></li>
					</ul>
				</aside>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</body>
</html>
