<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/WK02/WK02.js"></script>
<script type="text/javascript" src="../js/WK02/WK02005.js"></script>
</head>

<body id="WK02005">
<form id="mainForm" enctype="multipart/form-data" method="POST" action="WK02005">
<input type="hidden" name="mode" id="mode" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{if $smarty.session.loginUserType == '1'}{$labels["L-title-A"]|escape}{else}{$labels["L-title-C"]|escape}{/if}</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

		<div class="columns sidemenu">
			<div class="column is-9">
				<h3 class="title is-5">{$labels["L-sub"]|escape}</h3>

				<h3 class="title is-5 has-margin-top-30">{$labels["L-00"]|escape}</h3>
				<table class="table is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-00-01"]|escape}</th>
						<td>{$items["D_0_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-00-02"]|escape}</th>
						<td>{$items["D_0_2"]|escape}</td>
					</tr>
				</table><!--  /.table -->

				<h3 class="title is-5 has-margin-top-30">{$labels["L-01"]|escape}</h3>
				<table class="table is-bordered has-width-100pct">

					<tr>
						<th colspan="2">{$labels["L-01-01"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-01-01"]|escape}</th>
						<td>{$items["D_1_01_1_select"]|escape}</td>
					</tr>
{if $items["D_1_01_1"] == 1}
					<tr>
						<th>{$labels["L-01-01-02"]|escape}</th>
						<td>{$items["D_1_01_2_select"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_01_1"] == 1 && $items["D_1_01_2"] == 2}
					<tr>
						<th>{$labels["L-01-01-03"]|escape}</th>
						<td>{$items["D_1_01_3ymd"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_01_1"] == 2} 
					<tr>
						<th>{$labels["L-01-01-04"]|escape}</th>
						<td>{$items["D_1_01_4"]|escape}</td>
					</tr>
{/if}

					<tr>
						<th colspan="2">{$labels["L-01-02"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-02-01"]|escape}</th>
						<td>{$items["D_1_02_1_select"]|escape}</td>
					</tr>
{if $items["D_1_02_1"] == 1}
					<tr>
						<th>{$labels["L-01-02-02"]|escape}</th>
						<td>{$items["D_1_02_2_select"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_02_1"] == 1 && $items["D_1_02_2"] == 2}
					<tr>
						<th>{$labels["L-01-02-03"]|escape}</th>
						<td>{$items["D_1_02_3ymd"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_02_1"] == 2} 
					<tr>
						<th>{$labels["L-01-02-04"]|escape}</th>
						<td>{$items["D_1_02_4"]|escape}</td>
					</tr>
{/if}

					<tr>
						<th colspan="2">{$labels["L-01-03"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-03-01"]|escape}</th>
						<td>{$items["D_1_03_1_select"]|escape}</td>
					</tr>
{if $items["D_1_03_1"] == 1}
					<tr>
						<th>{$labels["L-01-03-02"]|escape}</th>
						<td>{$items["D_1_03_2_select"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_03_1"] == 1 && $items["D_1_03_2"] == 2}
					<tr>
						<th>{$labels["L-01-03-03"]|escape}</th>
						<td>{$items["D_1_03_3ymd"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_03_1"] == 2} 
					<tr>
						<th>{$labels["L-01-03-04"]|escape}</th>
						<td>{$items["D_1_03_4"]|escape}</td>
					</tr>
{/if}

					<tr>
						<th colspan="2">{$labels["L-01-04"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-04-01"]|escape}</th>
						<td>{$items["D_1_04_1_select"]|escape}</td>
					</tr>
{if $items["D_1_04_1"] == 1}
					<tr>
						<th>{$labels["L-01-04-02"]|escape}</th>
						<td>{$items["D_1_04_2_select"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_04_1"] == 1 && $items["D_1_04_2"] == 2}
					<tr>
						<th>{$labels["L-01-04-03"]|escape}</th>
						<td>{$items["D_1_04_3ymd"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_04_1"] == 2} 
					<tr>
						<th>{$labels["L-01-04-04"]|escape}</th>
						<td>{$items["D_1_04_4"]|escape}</td>
					</tr>
{/if}

					<tr>
						<th colspan="2">{$labels["L-01-05"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-05-01"]|escape}</th>
						<td>{$items["D_1_05_1_select"]|escape}</td>
					</tr>
{if $items["D_1_05_1"] == 1}
					<tr>
						<th>{$labels["L-01-05-02"]|escape}</th>
						<td>{$items["D_1_05_2_select"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_05_1"] == 1 && $items["D_1_05_2"] == 2}
					<tr>
						<th>{$labels["L-01-05-03"]|escape}</th>
						<td>{$items["D_1_05_3ymd"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_05_1"] == 2} 
					<tr>
						<th>{$labels["L-01-05-04"]|escape}</th>
						<td>{$items["D_1_05_4"]|escape}</td>
					</tr>
{/if}

					<tr>
						<th colspan="2">{$labels["L-01-06"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-06-01"]|escape}</th>
						<td>{$items["D_1_06_1_select"]|escape}</td>
					</tr>
{if $items["D_1_06_1"] == 1}
					<tr>
						<th>{$labels["L-01-06-02"]|escape}</th>
						<td>{$items["D_1_06_2_select"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_06_1"] == 1 && $items["D_1_06_2"] == 2}
					<tr>
						<th>{$labels["L-01-06-03"]|escape}</th>
						<td>{$items["D_1_06_3ymd"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_06_1"] == 2} 
					<tr>
						<th>{$labels["L-01-06-04"]|escape}</th>
						<td>{$items["D_1_06_4"]|escape}</td>
					</tr>
{/if}

					<tr>
						<th colspan="2">{$labels["L-01-07"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-07-01"]|escape}</th>
						<td>{$items["D_1_07_1_select"]|escape}</td>
					</tr>
{if $items["D_1_07_1"] == 1}
					<tr>
						<th>{$labels["L-01-07-02"]|escape}</th>
						<td>{$items["D_1_07_2_select"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_07_1"] == 1 && $items["D_1_07_2"] == 2}
					<tr>
						<th>{$labels["L-01-07-03"]|escape}</th>
						<td>{$items["D_1_07_3ymd"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_07_1"] == 2} 
					<tr>
						<th>{$labels["L-01-07-04"]|escape}</th>
						<td>{$items["D_1_07_4"]|escape}</td>
					</tr>
{/if}

					<tr>
						<th colspan="2">{$labels["L-01-08"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-08-01"]|escape}</th>
						<td>{$items["D_1_08_01_select"]|escape}</td>
					</tr>
{if $items["D_1_08_01"] == 1}
					<tr>
						<th>{$labels["L-01-08-01-02"]|escape}</th>
						<td>{$items["D_1_08_02_select"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_08_01"] == 1 && $items["D_1_08_02"] == 2}
					<tr>
						<th>{$labels["L-01-08-01-03"]|escape}</th>
						<td>{$items["D_1_08_03ymd"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_08_01"] == 2} 
					<tr>
						<th>{$labels["L-01-08-01-04"]|escape}</th>
						<td>{$items["D_1_08_04"]|escape}</td>
					</tr>
{/if}
					<tr>
						<th colspan="2">{$labels["L-01-08-02"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-08-02-01"]|escape}</th>
						<td>{$items["D_1_08_05ymd"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-08-02-02"]|escape}</th>
						<td>{$items["D_1_08_06"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-08-02-03"]|escape}</th>
						<td>{$items["D_1_08_07ymd"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-08-02-04"]|escape}</th>
						<td>{$items["D_1_08_08"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-08-02-05"]|escape}</th>
						<td>{$items["D_1_08_09"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-08-02-06"]|escape}</th>
						<td>{$items["D_1_08_10"]|escape}</td>
					</tr>

					<tr>
						<th colspan="2">{$labels["L-01-09"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-09-01"]|escape}</th>
						<td>{$items["D_1_09_1_select"]|escape}</td>
					</tr>
{if $items["D_1_09_1"] == 1}
					<tr>
						<th>{$labels["L-01-09-02"]|escape}</th>
						<td>{$items["D_1_09_2_select"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_09_1"] == 1 && $items["D_1_09_2"] == 2}
					<tr>
						<th>{$labels["L-01-09-03"]|escape}</th>
						<td>{$items["D_1_09_3ymd"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_09_1"] == 2} 
					<tr>
						<th>{$labels["L-01-09-04"]|escape}</th>
						<td>{$items["D_1_09_4"]|escape}</td>
					</tr>
{/if}

					<tr>
						<th colspan="2">{$labels["L-01-10"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-10-01-01"]|escape}</th>
						<td>{$items["D_1_10_1_select"]|escape}</td>
					</tr>
{if $items["D_1_10_1"] == 1}
					<tr>
						<th>{$labels["L-01-10-01-02"]|escape}</th>
						<td>{$items["D_1_10_2_select"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_10_1"] == 1 && $items["D_1_10_2"] == 2}
					<tr>
						<th>{$labels["L-01-10-01-03"]|escape}</th>
						<td>{$items["D_1_10_3ymd"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_10_1"] == 2} 
					<tr>
						<th>{$labels["L-01-10-01-04"]|escape}</th>
						<td>{$items["D_1_10_4"]|escape}</td>
					</tr>
{/if}

					<tr>
						<th colspan="2">{$labels["L-01-10-02"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-10-02-01"]|escape}</th>
						<td>{$items["D_1_10_5ymd"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-02-02"]|escape}</th>
						<td>{$items["D_1_10_6"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-02-03"]|escape}</th>
						<td>{$items["D_1_10_7"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-02-04"]|escape}</th>
						<td>{$items["D_1_10_8ymd"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-02-05"]|escape}</th>
						<td>{$items["D_1_10_9"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-02-06"]|escape}</th>
						<td>{$items["D_1_10_10"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-02-07"]|escape}</th>
						<td>{$items["D_1_10_11"]|escape}</td>
					</tr>

					<tr>
						<th colspan="2">{$labels["L-01-11"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-11-01"]|escape}</th>
						<td>{$items["D_1_11_1_select"]|escape}</td>
					</tr>
{if $items["D_1_11_1"] == 1}
					<tr>
						<th>{$labels["L-01-11-02"]|escape}</th>
						<td>{$items["D_1_11_2_select"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_11_1"] == 1 && $items["D_1_11_2"] == 2}
					<tr>
						<th>{$labels["L-01-11-03"]|escape}</th>
						<td>{$items["D_1_11_3ymd"]|escape}</td>
					</tr>
{/if}
{if $items["D_1_11_1"] == 2} 
					<tr>
						<th>{$labels["L-01-11-04"]|escape}</th>
						<td>{$items["D_1_11_4"]|escape}</td>
					</tr>
{/if}
				</table><!--  /.table -->

				<h3 class="title is-5 has-margin-top-30">{$labels["L-02"]|escape}</h3>
				<table class="table is-bordered has-width-100pct">

					<tr>
						<th>{$labels["L-02-01-01"]|escape}</th>
						<td>{$items["D_2_1_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-02-01-02"]|escape}</th>
						<td>{$items["D_2_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-01-03"]|escape}</th>
						<td>{$items["D_2_1_1"]|escape}</td>
					</tr>
				</table><!--  /.table -->

				<h3 class="title is-5 has-margin-top-30">{$labels["L-03"]|escape}</h3>
				<table class="table is-bordered has-width-100pct">

					<tr>
						<th>{$labels["L-03-01-01"]|escape}</th>
						<td>{$items["D_3_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-01-02"]|escape}</th>
						<td>{$items["D_3_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-01-03"]|escape}</th>
						<td>{$items["D_3_1_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-03-01-04"]|escape}</th>
						<td>{$items["D_3_1_4"]|escape}</td>
					</tr>
				</table><!--  /.table -->

{if $smarty.session.loginUserType == '1'}
				<div class="has-margin-top-30 has-text-centered">
					<div class="is-pulled-left">
						<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
					</div>
{if $smarty.session.loginUserAuth <= '2'}
					<input type="file" name="upfile" id="upfile" style="display:none;" />
					<button type="button" class="button is-primary has-margin-right-20" id="csv_import_button">{$labels["csv_import"]|escape}</button>
					<button type="button" class="button is-primary has-margin-right-20" id="csv_export_button">{$labels["csv_export"]|escape}</button>
					<button type="button" class="button is-primary has-margin-right-20" id="csv_format_button">{$labels["csv_format"]|escape}</button>
{/if}
				</div>
{/if}

			</div>
			<div class="column is-3">
				<aside class="menu">
					<ul class="menu-list">
						<li><a href="./WK02001">{$labels["sub_menu_01"]|escape}</a></li>
						<li><a href="./WK02002">{$labels["sub_menu_02"]|escape}</a></li>
						<li>
							<a class="acMenu">{$labels["sub_menu_03"]|escape}</a>
							<ul>
								<li><a href="./WK02003">{$labels["sub_menu_03_01"]|escape}</a></li>
								<li><a href="./WK02013">{$labels["sub_menu_03_02"]|escape}</a></li>
								<li><a href="./WK02014">{$labels["sub_menu_03_03"]|escape}</a></li>
								<li><a href="./WK02004">{$labels["sub_menu_03_04"]|escape}</a></li>
								<li><a class="is-active" href="./WK02005">{$labels["sub_menu_03_05"]|escape}</a></li>
								<li><a href="./WK02006">{$labels["sub_menu_03_06"]|escape}</a></li>
								<li><a href="./WK02007">{$labels["sub_menu_03_07"]|escape}</a></li>
								<li><a href="./WK02008">{$labels["sub_menu_03_08"]|escape}</a></li>
								<li><a href="./WK02009">{$labels["sub_menu_03_09"]|escape}</a></li>
								<li><a href="./WK02010">{$labels["sub_menu_03_10"]|escape}</a></li>
							</ul>
						</li>
						<li><a href="./WK02011">{$labels["sub_menu_04"]|escape}</a></li>
						<li><a href="./WK02012">{$labels["sub_menu_05"]|escape}</a></li>
					</ul>
				</aside>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</body>
</html>
