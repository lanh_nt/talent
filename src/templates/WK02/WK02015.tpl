<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/WK02/WK02015.js"></script>
</head>

<body id="WK02015">
<form id="mainForm" enctype="multipart/form-data" method="POST" action="WK02015">
<input type="hidden" name="csrf_token" value="{$smarty.session["csrf_token"]}" />
<input type="hidden" name="mode" id="mode" />

	<!-- header読込部 -->
	<header id="header"></header>
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{$labels["L-title-A"]|escape}</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

		<table class="table is-bordered has-width-100pct">
			<tr>
				<th>{$labels["L-01-01"]|escape}</th>
				<td><input class="input" type="text" name="worker_id" id="worker_id" placeholder="{$labels["placeholder"]|escape}" value="{$items["worker_id"]|escape}"></td>
			</tr>
			<tr>
				<th>{$labels["L-01-69"]|escape}</th>
				<td><input class="input" type="text" name="user_name" id="user_name" placeholder="{$labels["placeholder"]|escape}" value="{$items["user_name"]|escape}"></td>
			</tr>
			<tr>
				<th>{$labels["L-01-70"]|escape}</th>
				<td><input class="input" type="text" name="user_name_e" id="user_name_e" placeholder="{$labels["placeholder"]|escape}" value="{$items["user_name_e"]|escape}"></td>
			</tr>
			<tr>
				<th>{$labels["L-01-02"]|escape}</th>
				<td>
					<div class="select has-vertical-middle">
						<select name="disp_lang" id="disp_lang">
							<option value="2"{if $items["disp_lang"] == "2"} selected="selected"{/if}>{$labels["L-lang-01"]|escape}</option>
							<option value="3"{if $items["disp_lang"] == "3"} selected="selected"{/if}>{$labels["L-lang-02"]|escape}</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<th>{$labels["L-01-03"]|escape}</th>
				<td>
					<div class="field">
						<input class="is-checkradio" id="qualifications1" type="radio" name="qualifications" value="1" {if $items["qualifications"] == "1"} checked="checked"{/if}>
						<label for="qualifications1">{$labels["L-sel2-01"]|escape}</label>
						<input class="is-checkradio" id="qualifications2" type="radio" name="qualifications" value="2" {if $items["qualifications"] == "2"} checked="checked"{/if}>
						<label for="qualifications2">{$labels["L-sel2-02"]|escape}</label>
						<input class="is-checkradio" id="qualifications3" type="radio" name="qualifications" value="3" {if $items["qualifications"] == "3"} checked="checked"{/if}>
						<label for="qualifications3">{$labels["L-sel2-03"]|escape}</label>
						<input class="is-checkradio" id="qualifications4" type="radio" name="qualifications" value="4" {if $items["qualifications"] == "4"} checked="checked"{/if}>
						<label for="qualifications4">{$labels["L-sel2-04"]|escape}</label>
						<input class="is-checkradio" id="qualifications5" type="radio" name="qualifications" value="5" {if $items["qualifications"] == "5"} checked="checked"{/if}>
						<label for="qualifications5">{$labels["L-sel2-05"]|escape}</label>
						<input class="is-checkradio" id="qualifications6" type="radio" name="qualifications" value="6" {if $items["qualifications"] == "6"} checked="checked"{/if}>
						<label for="qualifications6">{$labels["L-sel2-06"]|escape}</label>
						<div class="select is-small">
							<select name="qualifications_etc" id="qualifications_etc">
								<option value=""></option>
{foreach from=$purpose key=$pkey item=$pname}
								<option value="{$pkey}"{if $pkey == $items["qualifications_etc"]} selected="selected"{/if}>{$pname}</option>
{/foreach}
							</select>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<th>{$labels["L-01-04"]|escape}</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input type="radio" class="is-checkradio" id="status1" name="status" value="1" {if $items["status"] == "1"} checked="checked"{/if}><label for="status1">{$labels["L-sel3-01"]|escape}</label>
						<input type="radio" class="is-checkradio" id="status2" name="status" value="2" {if $items["status"] == "2"} checked="checked"{/if}><label for="status2">{$labels["L-sel3-02"]|escape}</label>
						<input type="radio" class="is-checkradio" id="status3" name="status" value="3" {if $items["status"] == "3"} checked="checked"{/if}><label for="status3">{$labels["L-sel3-03"]|escape}</label>
						<input type="radio" class="is-checkradio" id="status4" name="status" value="4" {if $items["status"] == "4"} checked="checked"{/if}><label for="status4">{$labels["L-sel3-04"]|escape}</label>
						<input type="radio" class="is-checkradio" id="status5" name="status" value="5" {if $items["status"] == "5"} checked="checked"{/if}><label for="status5">{$labels["L-sel3-05"]|escape}</label>
						<input type="radio" class="is-checkradio" id="status6" name="status" value="6" {if $items["status"] == "6"} checked="checked"{/if}><label for="status6">{$labels["L-sel3-06"]|escape}</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>{$labels["L-01-05"]|escape}</th>
				<td><input class="input" name="nationality_region" id="nationality_region" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["nationality_region"]|escape}"></td>
			</tr>

			<tr>
				<th>{$labels["L-01-06"]|escape}</th>
				<td>
					<div class="columns is-gapless is-mobile">
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="birthday_year" id="birthday_year">
									<option value=""></option>

{foreach from=$years item=$year}
									<option value="{$year}"{if $year == $items["birthday_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["year"]|escape}</span>
						</div>
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="birthday_month" id="birthday_month">
									<option value=""></option>
{foreach from=$months item=$month}
									<option value="{$month}"{if $month == $items["birthday_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
								</select>
							</div>

							<span class="has-vertical-middle">{$labels["month"]|escape}</span>
						</div>
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="birthday_day" id="birthday_day">
									<option value=""></option>
{foreach from=$days item=$day}
									<option value="{$day}"{if $day == $items["birthday_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["day"]|escape}</span>
						</div>
					</div>
				</td>
			</tr>

			<tr>
				<th>{$labels["L-01-07"]|escape}</th>
				<td><input name="family_name" id ="family_name" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["family_name"]|escape}"></td>
			</tr>

			<tr>
				<th>{$labels["L-01-08"]|escape}</th>
				<td><input name="given_name" id="given_name" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["given_name"]|escape}"></td>
			</tr>

			<tr>
				<th>{$labels["L-01-09"]|escape}</th>
				<td>
					<div class="field">
						<input type="radio" class="is-checkradio" id="sex1" name="sex" value="1" {if $items["sex"] == "1"} checked="checked"{/if}>
						<label for="sex1">{$labels["L-man"]|escape}</label>
						<input type="radio" class="is-checkradio" id="sex2" name="sex" value="2" {if $items["sex"] == "2"} checked="checked"{/if}>
						<label for="sex2">{$labels["L-woman"]|escape}</label>
					</div>
				</td>
			</tr>

			<tr>
				<th>{$labels["L-01-10"]|escape}</th>
				<td><input name="place_birth" id="place_birth" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["place_birth"]|escape}"></td>
			</tr>

			<tr>
				<th>{$labels["L-01-11"]|escape}</th>
				<td>
					<div class="field">
						<input type="radio" class="is-checkradio" id="marital_flag1" name="marital_flag" value="1" {if $items["marital_flag"] == "1"} checked="checked"{/if}>
						<label for="marital_flag1">{$labels["L-yes"]|escape}</label>
						<input type="radio" class="is-checkradio" id="marital_flag2" name="marital_flag" value="2" {if $items["marital_flag"] == "2"} checked="checked"{/if}>
						<label for="marital_flag2">{$labels["L-no"]|escape}</label>
					</div>
				</td>
			</tr>

			<tr>
				<th>{$labels["L-01-12"]|escape}</th>
				<td><input name="occupation" id="occupation" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["occupation"]|escape}"></td>
			</tr>

			<tr>
				<th>{$labels["L-01-13"]|escape}</th>
				<td><input name="home_town" id="home_town" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["home_town"]|escape}"></td>
			</tr>

			<tr>
				<th>{$labels["L-01-71"]|escape}</th>
				<td><input name="postal_code" id="postal_code" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["postal_code"]|escape}"></td>
			</tr>

			<tr>
				<th>{$labels["L-01-14"]|escape}</th>
				<td><input name="address" id="address" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["address"]|escape}"></td>
			</tr>

			<tr>
				<th>{$labels["L-01-15"]|escape}</th>
				<td><input name="telephone" id="telephone" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["telephone"]|escape}"></td>
			</tr>

			<tr>
				<th>{$labels["L-01-16"]|escape}</th>
				<td><input name="cellular_phone" id="cellular_phone" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["cellular_phone"]|escape}"></td>
			</tr>

			<tr>
				<th>{$labels["L-01-17"]|escape}</th>
				<td><input name="A_01_1" id="A_01_1" class="input" type="number" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_01_1"]|escape}"></td>
			</tr>

			<tr>
				<th>{$labels["L-01-18"]|escape}</th>
				<td>
					<div class="columns is-gapless is-mobile">
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="A_01_2_year" id="A_01_2_year">
									<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_01_2_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["year"]|escape}</span>
						</div>
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="A_01_2_month" id="A_01_2_month">
									<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_01_2_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["month"]|escape}</span>
						</div>
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="A_01_2_day" id="A_01_2_day">
									<option value=""></option>
{foreach from=$days item=$day}
							<option value="{$day}"{if $day == $items["A_01_2_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["day"]|escape}</span>
						</div>
					</div>
				</td>
			</tr>

			<tr>
				<th>{$labels["L-01-19"]|escape}</th>
				<td>
					<div class="select has-vertical-middle">
						<select name="A_02_1" id="A_02_1">
{foreach from=$purpose key=$pkey item=$pname}
								<option value="{$pkey}"{if $pkey == $items["A_02_1"]} selected="selected"{/if}>{$pname}</option>
{/foreach}
						</select>
					</div>
				</td>
			</tr>

			<tr>
				<th>{$labels["L-01-20"]|escape}</th>
				<td><input name="A_02_2" id="A_02_2" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_02_2"]|escape}"></td>
			</tr>

			<tr>
				<th>{$labels["L-01-21"]|escape}</th>
				<td>
					<div class="columns is-gapless is-mobile">
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="A_03_year" id="A_03_year">
									<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_03_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["year"]|escape}</span>
						</div>
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="A_03_month" id="A_03_month">
									<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_03_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["month"]|escape}</span>
						</div>
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="A_03_day" id="A_03_day">
									<option value=""></option>
{foreach from=$days item=$day}
							<option value="{$day}"{if $day == $items["A_03_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["day"]|escape}</span>
						</div>
					</div>
				</td>
			</tr>

			<tr>
				<th>{$labels["L-01-22"]|escape}</th>
				<td><input name="A_04" id="A_04" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_04"]|escape}"></td>
			</tr>

			<tr>
				<th>{$labels["L-01-23"]|escape}</th>
				<td><input name="A_05" id="A_05" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_05"]|escape}"></td>
			</tr>

			<tr>
				<th>{$labels["L-01-24"]|escape}</th>
				<td>
					<div class="field">
						<input type="radio" class="is-checkradio" id="A_06_01" name="A_06" value="1" {if $items["A_06"] == "1"} checked="checked"{/if}>
						<label for="A_06_01">{$labels["L-yes"]|escape}</label>
						<input type="radio" class="is-checkradio" id="A_06_02" name="A_06" value="2" {if $items["A_06"] == "2"} checked="checked"{/if}>
						<label for="A_06_02">{$labels["L-no"]|escape}</label>
					</div>
				</td>
			</tr>

				<tr>
					<th>{$labels["L-01-25"]|escape}</th>
					<td>
						<input name="A_07" id="A_07" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_07"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-26"]|escape}</th>
					<td>
						<div class="field">
							<input type="radio" class="is-checkradio" id="A_08_1_01" name="A_08_1" value="1" {if $items["A_08_1"] == "1"} checked="checked"{/if}>
							<label for="A_08_1_01">{$labels["L-yes"]|escape}</label>
							<input type="radio" class="is-checkradio" id="A_08_1_02" name="A_08_1" value="2" {if $items["A_08_1"] == "2"} checked="checked"{/if}>
							<label for="A_08_1_02">{$labels["L-no"]|escape}</label>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-27"]|escape}</th>
					<td>
						<input name="A_08_2" id="A_08_2" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_08_2"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-28"]|escape}</th>
					<td>
						<div class="columns is-gapless is-mobile">
							<div class="column is-4">
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_08_3_year" id="A_08_3_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_08_3_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span class="has-vertical-middle">{$labels["year"]|escape}</span>
							</div>
							<div class="column is-4">
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_08_3_month" id="A_08_3_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_08_3_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span class="has-vertical-middle">{$labels["month"]|escape}</span>
							</div>
							<div class="column is-4">
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_08_3_day" id="A_08_3_day">
										<option value=""></option>
{foreach from=$days item=$day}
							<option value="{$day}"{if $day == $items["A_08_3_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
									</select>
								</div>
								<span class="has-vertical-middle">{$labels["day"]|escape}</span>
							</div>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-29"]|escape}</th>
					<td>
						<div class="columns is-gapless is-mobile">
							<div class="column is-4">
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_08_4_year" id="A_08_4_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_08_4_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span class="has-vertical-middle">{$labels["year"]|escape}</span>
							</div>
							<div class="column is-4">
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_08_4_month" id="A_08_4_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_08_4_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span class="has-vertical-middle">{$labels["month"]|escape}</span>
							</div>
							<div class="column is-4">
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_08_4_day" id="A_08_4_day">
										<option value=""></option>
{foreach from=$days item=$day}
							<option value="{$day}"{if $day == $items["A_08_4_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
									</select>
								</div>
								<span class="has-vertical-middle">{$labels["day"]|escape}</span>
							</div>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-30"]|escape}</th>
					<td>
						<div class="field">
							<input type="radio" class="is-checkradio" id="A_15_1_01" name="A_15_1" value="1" {if $items["A_15_1"] == "1"} checked="checked"{/if}>
							<label for="A_15_1_01">{$labels["L-yes"]|escape}</label>
							<input type="radio" class="is-checkradio" id="A_15_1_02" name="A_15_1" value="2" {if $items["A_15_1"] == "2"} checked="checked"{/if}>
							<label for="A_15_1_02">{$labels["L-no"]|escape}</label>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-31"]|escape}</th>
					<td>
						<input name="A_15_2" id="A_15_2" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_15_2"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-32"]|escape}</th>
					<td>
						<div class="field">
							<input type="radio" class="is-checkradio" id="A_16_1_01" name="A_16_1" value="1" {if $items["A_16_1"] == "1"} checked="checked"{/if}>
							<label for="A_16_1_01">{$labels["L-yes"]|escape}</label>
							<input type="radio" class="is-checkradio" id="A_16_1_02" name="A_16_1" value="1" {if $items["A_16_1"] == "1"} checked="checked"{/if}>
							<label for="A_16_1_02">{$labels["L-no"]|escape}</label>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-33"]|escape}</th>
					<td>
						<input name="A_17_1" id="A_17_1" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_17_1"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-34"]|escape}</th>
					<td>
						<div class="columns is-gapless is-mobile">
							<div class="column is-4">
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_17_2_year" id="A_17_2_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_17_2_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span class="has-vertical-middle">{$labels["year"]|escape}</span>
							</div>
							<div class="column is-4">
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_17_2_month" id="A_17_2_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_17_2_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span class="has-vertical-middle">{$labels["month"]|escape}</span>
							</div>
							<div class="column is-4">
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_17_2_day" id="A_17_2_day">
										<option value=""></option>
{foreach from=$days item=$day}
							<option value="{$day}"{if $day == $items["A_17_2_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
									</select>
								</div>
								<span class="has-vertical-middle">{$labels["day"]|escape}</span>
							</div>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-35"]|escape}</th>
					<td>
						<div class="field">
							<input type="radio" class="is-checkradio" id="A_18_1_01" name="A_18_1" value="1" {if $items["A_18_1"] == "1"} checked="checked"{/if}>
							<label for="A_18_1_01">{$labels["L-yes"]|escape}</label>
							<input type="radio" class="is-checkradio" id="A_18_1_02" name="A_18_1" value="2" {if $items["A_18_1"] == "2"} checked="checked"{/if}>
							<label for="A_18_1_02">{$labels["L-no"]|escape}</label>
						</div>
					</td>
				</tr>

				<tr>
					<th rowspan="4">{$labels["L-01-36"]|escape}</th>
					<td>
						<div class="columns is-gapless is-multiline">
							<div class="column is-6 has-margin-top-10">
									<span>{$labels["L-01-36-01-01"]|escape}</span>
									<input name="A_19_1_1" id="A_19_1_1" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_1_1"]|escape}">
							</div>
							<div class="column is-6 has-margin-top-10">
									<span>{$labels["L-01-36-01-02"]|escape}</span>
									<input name="A_19_1_2" id="A_19_1_2" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_1_2"]|escape}">
							</div>
							<div class="column is-12 has-margin-top-10">
								<span class="has-vertical-middle has-margin-right-10">{$labels["L-01-36-01-03"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_1_3_year" id="A_19_1_3_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_19_1_3_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_1_3_month" id="A_19_1_3_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_19_1_3_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_1_3_day" id="A_19_1_3_day">
										<option value=""></option>
{foreach from=$days item=$day}
							<option value="{$day}"{if $day == $items["A_19_1_3_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["day"]|escape}</span>
							</div>

							<div class="column is-6 has-margin-top-10">
								<span>{$labels["L-01-36-01-04"]|escape}</span>
								<input name="A_19_1_4" id="A_19_1_4" class="input has-vertical-middle has-width-60pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_1_4"]|escape}">
							</div>

							<div class="column is-6 has-margin-top-10">
								<span class="has-margin-right-10 has-vertical-middle">{$labels["L-01-36-01-05"]|escape}</span>
								<div class="is-inline field has-vertical-middle">
									<input type="radio" class="is-checkradio" id="A_19_1_5_01" name="A_19_1_5" value="1" {if $items["A_19_1_5"] == "1"} checked="checked"{/if}>
									<label for="A_19_1_5_01">{$labels["L-yes"]|escape}</label>
									<input type="radio" class="is-checkradio" id="A_19_1_5_02" name="A_19_1_5" value="2" {if $items["A_19_1_5"] == "2"} checked="checked"{/if}>
									<label for="A_19_1_5_02">{$labels["L-no"]|escape}</label>
								</div>
							</div>

							<div class="column is-12 has-margin-top-10">
								<span class="has-margin-right-10">{$labels["L-01-36-01-06"]|escape}</span>
								<input name="A_19_1_6" id="A_19_1_6" class="input has-vertical-middle has-width-70pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_1_7"]|escape}">
							</div>

							<div class="column is-12 has-margin-top-10">
								<span class="has-margin-right-10">{$labels["L-01-36-01-07"]|escape}</span>
								<input name="A_19_1_7" id="A_19_1_7" class="input has-vertical-middle has-width-50pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_1_7"]|escape}">
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="columns is-gapless is-multiline">
							<div class="column is-6 has-margin-top-10">
									<span>{$labels["L-01-36-02-01"]|escape}</span>
									<input name="A_19_2_1" id="A_19_2_1" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_2_1"]|escape}">
							</div>
							<div class="column is-6 has-margin-top-10">
									<span>{$labels["L-01-36-02-02"]|escape}</span>
									<input name="A_19_2_2" id="A_19_2_2" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_2_2"]|escape}">
							</div>
							<div class="column is-12 has-margin-top-10">
								<span class="has-vertical-middle has-margin-right-10">{$labels["L-01-36-02-03"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_2_3_year" id="A_19_2_3_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_19_2_3_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_2_3_month" id="A_19_2_3_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_19_2_3_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_2_3_day" id="A_19_2_3_day">
										<option value=""></option>
{foreach from=$days item=$day}
							<option value="{$day}"{if $day == $items["A_19_2_3_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["day"]|escape}</span>
							</div>

							<div class="column is-6 has-margin-top-10">
								<span>{$labels["L-01-36-02-04"]|escape}</span>
								<input name="A_19_2_4" id="A_19_2_4" class="input has-vertical-middle has-width-60pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_2_4"]|escape}">
							</div>

							<div class="column is-6 has-margin-top-10">
								<span class="has-margin-right-10 has-vertical-middle">{$labels["L-01-36-02-05"]|escape}</span>
								<div class="is-inline field has-vertical-middle">
									<input type="radio" class="is-checkradio" id="A_19_2_5_01" name="A_19_2_5" value="1" {if $items["A_19_2_5"] == "1"} checked="checked"{/if}>
									<label for="A_19_2_5_01">{$labels["L-yes"]|escape}</label>
									<input type="radio" class="is-checkradio" id="A_19_2_5_02" name="A_19_2_5" value="2" {if $items["A_19_2_5"] == "2"} checked="checked"{/if}>
									<label for="A_19_2_5_02">{$labels["L-no"]|escape}</label>
								</div>
							</div>

							<div class="column is-12 has-margin-top-10">
								<span class="has-margin-right-10">{$labels["L-01-36-02-06"]|escape}</span>
								<input name="A_19_2_6" id="A_19_2_6" class="input has-vertical-middle has-width-70pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_2_6"]|escape}"> 
							</div>

							<div class="column is-12 has-margin-top-10">
								<span class="has-margin-right-10">{$labels["L-01-36-02-07"]|escape}</span>
								<input name="A_19_2_7" id="A_19_2_7" class="input has-vertical-middle has-width-50pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_2_7"]|escape}">
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="columns is-gapless is-multiline">
							<div class="column is-6 has-margin-top-10">
									<span>{$labels["L-01-36-03-01"]|escape}</span>
									<input name="A_19_3_1" id="A_19_3_1" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_3_1"]|escape}">
							</div>
							<div class="column is-6 has-margin-top-10">
									<span>{$labels["L-01-36-03-02"]|escape}</span>
									<input name="A_19_3_2" id="A_19_3_2" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_3_2"]|escape}">
							</div>
							<div class="column is-12 has-margin-top-10">
								<span class="has-vertical-middle has-margin-right-10">{$labels["L-01-36-03-03"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_3_3_year" id="A_19_3_3_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_19_3_3_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_3_3_month" id="A_19_3_3_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_19_3_3_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_3_3_day" id="A_19_3_3_day">
										<option value=""></option>
{foreach from=$days item=$day}
							<option value="{$day}"{if $day == $items["A_19_2_3_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["day"]|escape}</span>
							</div>

							<div class="column is-6 has-margin-top-10">
								<span>{$labels["L-01-36-03-04"]|escape}</span>
								<input name="A_19_3_4" id="A_19_3_4" class="input has-vertical-middle has-width-60pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_3_4"]|escape}">
							</div>

							<div class="column is-6 has-margin-top-10">
								<span class="has-margin-right-10 has-vertical-middle">{$labels["L-01-36-03-05"]|escape}</span>
								<div class="is-inline field has-vertical-middle">
									<input type="radio" class="is-checkradio" id="A_19_3_5_01" name="A_19_3_5" value="1" {if $items["A_19_3_5"] == "1"} checked="checked"{/if}>
									<label for="A_19_3_5_01">{$labels["L-yes"]|escape}</label>
									<input type="radio" class="is-checkradio" id="A_19_3_5_02" name="A_19_3_5" value="2" {if $items["A_19_3_5"] == "2"} checked="checked"{/if}>
									<label for="A_19_3_5_02">{$labels["L-no"]|escape}</label>
								</div>
							</div>

							<div class="column is-12 has-margin-top-10">
								<span class="has-margin-right-10">{$labels["L-01-36-03-06"]|escape}</span>
								<input name="A_19_3_6" id="A_19_3_6" class="input has-vertical-middle has-width-70pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_3_6"]|escape}">
							</div>

							<div class="column is-12 has-margin-top-10">
								<span class="has-margin-right-10">{$labels["L-01-36-03-07"]|escape}</span>
								<input name="A_19_3_7" id="A_19_3_7" class="input has-vertical-middle has-width-50pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_3_7"]|escape}">
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="columns is-gapless is-multiline">
							<div class="column is-6 has-margin-top-10">
									<span>{$labels["L-01-36-04-01"]|escape}</span>
									<input name="A_19_4_1" id="A_19_4_1" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_4_1"]|escape}">
							</div>
							<div class="column is-6 has-margin-top-10">
									<span>{$labels["L-01-36-04-02"]|escape}</span>
									<input name="A_19_4_2" id="A_19_4_2" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_4_2"]|escape}">
							</div>
							<div class="column is-12 has-margin-top-10">
								<span class="has-vertical-middle has-margin-right-10">{$labels["L-01-36-04-03"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_4_3_year" id="A_19_4_3_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_19_4_3_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_4_3_month" id="A_19_4_3_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_19_4_3_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_4_3_day" id="A_19_4_3_day">
										<option value=""></option>
{foreach from=$days item=$day}
							<option value="{$day}"{if $day == $items["A_19_4_3_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["day"]|escape}</span>
							</div>

							<div class="column is-6 has-margin-top-10">
								<span>{$labels["L-01-36-04-04"]|escape}</span>
								<input name="A_19_4_4" id="A_19_4_4" class="input has-vertical-middle has-width-60pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_4_4"]|escape}">
							</div>

							<div class="column is-6 has-margin-top-10">
								<span class="has-margin-right-10 has-vertical-middle">{$labels["L-01-36-04-05"]|escape}</span>
								<div class="is-inline field has-vertical-middle">
									<input type="radio" class="is-checkradio" id="A_19_4_5_01" name="A_19_4_5" value="1" {if $items["A_19_4_5"] == "1"} checked="checked"{/if}>
									<label for="A_19_4_5_01">{$labels["L-yes"]|escape}</label>
									<input type="radio" class="is-checkradio" id="A_19_4_5_02" name="A_19_4_5" value="2" {if $items["A_19_4_5"] == "2"} checked="checked"{/if}>
									<label for="A_19_4_5_02">{$labels["L-no"]|escape}</label>
								</div>
							</div>

							<div class="column is-12 has-margin-top-10">
								<span class="has-margin-right-10">{$labels["L-01-36-04-06"]|escape}</span>
								<input name="A_19_4_6" id="A_19_4_6" class="input has-vertical-middle has-width-70pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_4_6"]|escape}">
							</div>

							<div class="column is-12 has-margin-top-10">
								<span class="has-margin-right-10">{$labels["L-01-36-04-07"]|escape}</span>
								<input name="A_19_4_7" id="A_19_4_7" class="input has-vertical-middle has-width-50pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_4_7"]|escape}">
							</div>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-37"]|escape}</th>
					<td>
						<input name="A_20_1" id="A_20_1" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_20_1"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-38"]|escape}</th>
					<td>
						<input name="A_20_2" id="A_20_2" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_20_2"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-39"]|escape}</th>
					<td>
						<input name="A_20_3" id="A_20_3" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_20_3"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-40"]|escape}</th>
					<td>
						<ol class="input__subtext">
							<li>{$labels["L-01-40-01"]|escape}</li>
							<li>{$labels["L-01-40-02"]|escape}</li>
						</ol>
						<div class="select">
							<select name="A_21_1" id="A_21_1">
								<option value=""></option>
								<option value="1" {if $items["A_21_1"] == "1"} selected="selected"{/if}>(1)</option>
								<option value="2" {if $items["A_21_1"] == "2"} selected="selected"{/if}>(2)</option>
							</select>
						</div>
					</td>
				</tr>

				<tr>
					<th rowspan="3">{$labels["L-01-41"]|escape}</th>
					<td>
						<input name="A_21_3" id="A_21_3" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_21_3"]|escape}">
					</td>
				</tr>
				<tr>
					<td>
						<input name="A_21_4" id="A_21_4" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_21_4"]|escape}">
					</td>
				</tr>
				<tr>
					<td>
						<input name="A_21_5" id="A_21_5" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_21_5"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-42"]|escape}</th>
					<td><input name="A_21_7" id="A_21_7" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_21_7"]|escape}"></td>
				</tr>

				<tr>
					<th>{$labels["L-01-43"]|escape}</th>
					<td>
						<ol class="input__subtext">
							<li>{$labels["L-01-43-01"]|escape}</li>
							<li>{$labels["L-01-43-02"]|escape}</li>
						</ol>
						<div class="select">
							<select name="A_22_1" id="A_22_1">
								<option value=""></option>
								<option value="1" {if $items["A_22_1"] == "1"} selected="selected"{/if}>(1)</option>
								<option value="2" {if $items["A_22_1"] == "2"} selected="selected"{/if}>(2)</option>
							</select>
						</div>
					</td>
				</tr>

				<tr>
					<th rowspan="3">{$labels["L-01-44"]|escape}</th>
					<td>
						<input name="A_22_3" id="A_22_3" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_22_3"]|escape}">
					</td>
				</tr>
				<tr>
					<td>
						<input name="A_22_4" id="A_22_4" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_22_4"]|escape}">
					</td>
				</tr>
				<tr>
					<td>
						<input name="A_22_5" id="A_22_5" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_22_5"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-45"]|escape}</th>
					<td><input name="A_22_7" id="A_22_7" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_22_7"]|escape}"></td>
				</tr>

				<tr>
					<th>{$labels["L-01-46"]|escape}</th>
					<td>
						<input name="A_23_1_1" id="A_23_1_1" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_23_1_1"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-47"]|escape}</th>
					<td>
						<input name="A_23_1_2" id="A_23_1_2" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_23_1_2"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-48"]|escape}</th>
					<td>
						<ol class="input__subtext">
							<li>{$labels["L-01-48-01"]|escape}</li>
							<li>{$labels["L-01-48-02"]|escape}</li>
						</ol>
						<div class="select">
							<select name="A_23_1_3" id="A_23_1_3">
								<option value=""></option>
								<option value="1" {if $items["A_23_1_3"] == "1"} selected="selected"{/if}>(1)</option>
								<option value="2" {if $items["A_23_1_3"] == "2"} selected="selected"{/if}>(2)</option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<th>{$labels["L-01-49"]|escape}</th>
					<td>
						<input name="A_23_2_1" id="A_23_2_1" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_23_2_1"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-50"]|escape}</th>
					<td>
						<input name="A_23_2_2" id="A_23_2_2" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_23_2_2"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-51"]|escape}</th>
					<td>
						<ol class="input__subtext">
							<li>{$labels["L-01-51-01"]|escape}</li>
							<li>{$labels["L-01-51-02"]|escape}</li>
						</ol>
						<div class="select">
							<select name="A_23_2_3" id="A_23_2_3">
								<option value=""></option>
								<option value="1" {if $items["A_23_2_3"] == "1"} selected="selected"{/if}>(1)</option>
								<option value="2" {if $items["A_23_2_3"] == "2"} selected="selected"{/if}>(2)</option>
							</select>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-52"]|escape}</th>
					<td>
						<div class="columns is-gapless is-mobile">
							<div class="column is-4">
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_24_1" id="A_24_1">
										<option value=""></option>
{foreach from=$periodYears item=$year}
							<option value="{$year}"{if $year == $items["A_24_1"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span class="has-vertical-middle">{$labels["year"]|escape}</span>
							</div>
							<div class="column is-4">
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_24_2" id="A_24_2">
										<option value=""></option>
{foreach from=$periodMonths item=$month}
							<option value="{$month}"{if $month == $items["A_24_2"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span class="has-vertical-middle">{$labels["month"]|escape}</span>
							</div>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-53"]|escape}</th>
					<td>
						<div class="field">
							<input type="radio" class="is-checkradio" id="A_25_1_01" name="A_25_1" value="1" {if $items["A_25_1"] == "1"} checked="checked"{/if}>
							<label for="A_25_1_01">{$labels["L-yes"]|escape}</label>
							<input type="radio" class="is-checkradio" id="A_25_1_02" name="A_25_1" value="2" {if $items["A_25_1"] == "2"} checked="checked"{/if}>
							<label for="A_25_1_02">{$labels["L-no"]|escape}</label>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-54"]|escape}</th>
					<td>
						<input name="A_25_2" id="A_25_2" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_25_2"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-55"]|escape}</th>
					<td>
						<input name="A_25_3" id="A_25_3" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_25_3"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-56"]|escape}</th>
					<td>
						<div class="field">
							<input type="radio" class="is-checkradio" id="A_26_1_01" name="A_26_1" value="1" {if $items["A_26_1"] == "1"} checked="checked"{/if}>
							<label for="A_26_1_01">{$labels["L-yes"]|escape}</label>
							<input type="radio" class="is-checkradio" id="A_26_1_02" name="A_26_1" value="2" {if $items["A_26_1"] == "2"} checked="checked"{/if}>
							<label for="A_26_1_02">{$labels["L-no"]|escape}</label>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-57"]|escape}</th>
					<td>
						<input name="A_26_2" id="A_26_2" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_26_2"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-58"]|escape}</th>
					<td>
						<input name="A_26_3" id="A_26_3" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_26_3"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-59"]|escape}</th>
					<td>
						<div class="field">
							<input type="radio" class="is-checkradio" id="A_27_01" name="A_27" value="1" {if $items["A_27"] == "1"} checked="checked"{/if}>
							<label for="A_27_01">{$labels["L-yes"]|escape}</label>
							<input type="radio" class="is-checkradio" id="A_27_02" name="A_27" value="2" {if $items["A_27"] == "2"} checked="checked"{/if}>
							<label for="A_27_02">{$labels["L-no"]|escape}</label>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-60"]|escape}</th>
					<td>
						<div class="field">
							<input type="radio" class="is-checkradio" id="A_28_01" name="A_28" value="1" {if $items["A_28"] == "1"} checked="checked"{/if}>
							<label for="A_28_01">{$labels["L-yes"]|escape}</label>
							<input type="radio" class="is-checkradio" id="A_28_02" name="A_28" value="2" {if $items["A_28"] == "2"} checked="checked"{/if}>
							<label for="A_28_02">{$labels["L-no"]|escape}</label>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-61"]|escape}</th>
					<td>
						<div class="field">
							<input type="radio" class="is-checkradio" id="A_29_01" name="A_29" value="1" {if $items["A_29"] == "1"} checked="checked"{/if}>
							<label for="A_29_01">{$labels["L-yes"]|escape}</label>
							<input type="radio" class="is-checkradio" id="A_29_02" name="A_29" value="2" {if $items["A_29"] == "2"} checked="checked"{/if}>
							<label for="A_29_02">{$labels["L-no"]|escape}</label>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-62"]|escape}</th>
					<td>
						<div class="field">
							<input type="radio" class="is-checkradio" id="A_30_01" name="A_30" value="1" {if $items["A_30"] == "1"} checked="checked"{/if}>
							<label for="A_30_01">{$labels["L-yes"]|escape}</label>
							<input type="radio" class="is-checkradio" id="A_30_02" name="A_30" value="2" {if $items["A_30"] == "2"} checked="checked"{/if}>
							<label for="A_30_02">{$labels["L-no"]|escape}</label>
						</div>
					</td>
				</tr>

				<tr>
					<th rowspan="6">{$labels["L-01-63"]|escape}</th>
					<td>
						<div class="columns is-gapless is-multiline">
							<div class="column is-6">
								<span class="has-vertical-middle has-margin-right-10">{$labels["L-01-63-01-01"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_1_1_year" id="A_31_1_1_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_31_1_1_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_1_1_month" id="A_31_1_1_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_31_1_1_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>

							</div>
							<div class="column is-6">
								<span class="has-margin-right-10">{$labels["L-01-63-01-02"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_1_2_year" id="A_31_1_2_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_31_1_2_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_1_2_month" id="A_31_1_2_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_31_1_2_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>
							</div>
							<div class="column is-12 has-margin-top-10">
									<span>{$labels["L-01-63-01-03"]|escape}</span>
									<input name="A_31_1_3" id="A_31_1_3" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_31_1_3"]|escape}">
							</div>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<div class="columns is-gapless is-multiline">
							<div class="column is-6">
								<span class="has-vertical-middle has-margin-right-10">{$labels["L-01-63-02-01"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_2_1_year" id="A_31_2_1_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_31_2_1_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_2_1_month" id="A_31_2_1_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_31_2_1_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>

							</div>
							<div class="column is-6">
								<span class="has-margin-right-10">{$labels["L-01-63-02-02"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_2_2_year" id="A_31_2_2_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_31_2_2_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_2_2_month" id="A_31_2_2_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_31_2_2_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>
							</div>
							<div class="column is-12 has-margin-top-10">
									<span>{$labels["L-01-63-02-03"]|escape}</span>
									<input name="A_31_2_3" id="A_31_2_3" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_31_2_3"]|escape}">
							</div>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<div class="columns is-gapless is-multiline">
							<div class="column is-6">
								<span class="has-vertical-middle has-margin-right-10">{$labels["L-01-63-03-01"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_3_1_year" id="A_31_3_1_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_31_3_1_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_3_1_month" id="A_31_3_1_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_31_3_1_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>

							</div>
							<div class="column is-6">
								<span class="has-margin-right-10">{$labels["L-01-63-03-02"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_3_2_year" id="A_31_3_2_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_31_3_2_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_3_2_month" id="A_31_3_2_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_31_3_2_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>
							</div>
							<div class="column is-12 has-margin-top-10">
									<span>{$labels["L-01-63-03-03"]|escape}</span>
									<input name="A_31_3_3" id="A_31_3_3" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}"  value="{$items["A_31_3_3"]|escape}">
							</div>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<div class="columns is-gapless is-multiline">
							<div class="column is-6">
								<span class="has-vertical-middle has-margin-right-10">{$labels["L-01-63-04-01"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_4_1_year" id="A_31_4_1_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_31_4_1_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_4_1_month" id="A_31_4_1_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_31_4_1_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>

							</div>
							<div class="column is-6">
								<span class="has-margin-right-10">{$labels["L-01-63-04-02"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_4_2_year" id="A_31_4_2_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_31_4_2_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_4_2_month" id="A_31_4_2_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_31_4_2_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>
							</div>
							<div class="column is-12 has-margin-top-10">
									<span>{$labels["L-01-63-04-03"]|escape}</span>
									<input name="A_31_4_3" id="A_31_4_3" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_31_4_3"]|escape}">
							</div>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<div class="columns is-gapless is-multiline">
							<div class="column is-6">
								<span class="has-vertical-middle has-margin-right-10">{$labels["L-01-63-05-01"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_5_1_year" id="A_31_5_1_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_31_5_1_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_5_1_month" id="A_31_5_1_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_31_5_1_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>

							</div>
							<div class="column is-6">
								<span class="has-margin-right-10">{$labels["L-01-63-05-02"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_5_2_year" id="A_31_5_2_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_31_5_2_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_5_2_month" id="A_31_5_2_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_31_5_2_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>
							</div>
							<div class="column is-12 has-margin-top-10">
									<span>{$labels["L-01-63-05-03"]|escape}</span>
									<input name="A_31_5_3" id="A_31_5_3" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_31_5_3"]|escape}">
							</div>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<div class="columns is-gapless is-multiline">
							<div class="column is-6">
								<span class="has-vertical-middle has-margin-right-10">{$labels["L-01-63-06-01"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_6_1_year" id="A_31_6_1_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_31_6_1_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_6_1_month" id="A_31_6_1_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_31_6_1_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>

							</div>
							<div class="column is-6">
								<span class="has-margin-right-10">{$labels["L-01-63-06-02"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_6_2_year" id="A_31_6_2_year">
										<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_31_6_2_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_31_6_2_month" id="A_31_6_2_month">
										<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_31_6_2_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>
							</div>
							<div class="column is-12 has-margin-top-10">
									<span>{$labels["L-01-63-06-03"]|escape}</span>
									<input name="A_31_6_3" id="A_31_6_3" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_31_6_3"]|escape}">
							</div>
						</div>
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-64"]|escape}</th>
					<td>
						<input name="A_32" id="A_32" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_32"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-65"]|escape}</th>
					<td>
						<input name="A_33" id="A_33" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_33"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-66"]|escape}</th>
					<td>
						<input name="A_34" id="A_34" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_34"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-67"]|escape}</th>
					<td>
						<input name="A_35" id="A_35" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_35"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-68"]|escape}</th>
					<td>
						<input name="A_36" id="A_36" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_36"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-72"]|escape}</th>
					<td>
						<input name="A_37" id="A_37" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_37"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-73"]|escape}</th>
					<td>
						<input name="A_38" id="A_38" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_38"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-74"]|escape}</th>
					<td>
						<input name="A_39" id="A_39" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_39"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-75"]|escape}</th>
					<td>
						<input name="A_40" id="A_40" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_40"]|escape}">
					</td>
				</tr>

				<tr>
					<th>{$labels["L-01-76"]|escape}</th>
					<td>
						<input name="A_41" id="A_41" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_41"]|escape}">
					</td>
				</tr>

		</table><!--  /.table -->
		
		<h3 class="title is-6 has-margin-top-50">{$labels["L-02-title"]|escape}</h3>
		<table class="table is-bordered has-width-100pct">
				<tr>
					<th>{$labels["L-02-01-01"]|escape}</th>
					<td>
						<input name="A_09_2" id="A_09_2" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_09_2"]|escape}">
					</td>
				</tr>
				<tr>
					<th>{$labels["L-02-01-02"]|escape}</th>
				<td>
					<div class="columns is-gapless is-mobile">
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="A_09_3_year" id="A_09_3_year">
									<option value=""></option>
{foreach from=$years item=$year}
							<option value="{$year}"{if $year == $items["A_09_3_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["year"]|escape}</span>
						</div>
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="A_09_3_month" id="A_09_3_month">
									<option value=""></option>
{foreach from=$months item=$month}
							<option value="{$month}"{if $month == $items["A_09_3_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["month"]|escape}</span>
						</div>
						<div class="column is-4">
							<div class="is-inlineblock select has-vertical-middle">
								<select name="A_09_3_day" id="A_09_3_day">
									<option value=""></option>
{foreach from=$days item=$day}
							<option value="{$day}"{if $day == $items["A_09_3_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">{$labels["day"]|escape}</span>
						</div>
					</div>
				</td>
				</tr>
				<tr>
					<th>{$labels["L-02-01-03"]|escape}</th>
					<td>
						<input name="A_10" id="A_10" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_10"]|escape}">
					</td>
				</tr>
				<tr>
					<th>{$labels["L-02-01-04"]|escape}</th>
					<td><input name="A_12" id="A_12" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_12"]|escape}"></td>
				</tr>
				<tr>
					<th>{$labels["L-02-01-05"]|escape}</th>
					<td><input name="A_13" id="A_13" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_13"]|escape}"></td>
				</tr>
				<tr>
				<th>{$labels["L-02-01-06"]|escape}</th>
					<td>
						<div class="field">
							<input class="is-checkradio" id="A_11_1_01" type="radio" name="A_11_1" value="1" {if $items["A_11_1"] == "1"} checked="checked"{/if}>
							<label for="A_11_1_01">{$labels["L-sel2-01"]|escape}</label>
							<input class="is-checkradio" id="A_11_1_02" type="radio" name="A_11_1" value="2" {if $items["A_11_1"] == "2"} checked="checked"{/if}>
							<label for="A_11_1_02">{$labels["L-sel2-02"]|escape}</label>
							<input class="is-checkradio" id="A_11_1_03" type="radio" name="A_11_1" value="3" {if $items["A_11_1"] == "3"} checked="checked"{/if}>
							<label for="A_11_1_03">{$labels["L-sel2-03"]|escape}</label>
							<input class="is-checkradio" id="A_11_1_04" type="radio" name="A_11_1" value="4" {if $items["A_11_1"] == "4"} checked="checked"{/if}>
							<label for="A_11_1_04">{$labels["L-sel2-04"]|escape}</label>
							<input class="is-checkradio" id="A_11_1_05" type="radio" name="A_11_1" value="5" {if $items["A_11_1"] == "5"} checked="checked"{/if}>
							<label for="A_11_1_05">{$labels["L-sel2-05"]|escape}</label>
							<input class="is-checkradio" id="A_11_1_06" type="radio" name="A_11_1" value="6" {if $items["A_11_1"] == "6"} checked="checked"{/if}>
							<label for="A_11_1_06">{$labels["L-sel2-06"]|escape}</label>
							<div class="select is-small">
								<select name="A_11_2" id="A_11_2">
									<option value=""></option>
{foreach from=$purpose key=$pkey item=$pname}
								    <option value="{$pkey}"{if $pkey == $items["A_11_2"]} selected="selected"{/if}>{$pname}</option>
{/foreach}
								</select>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<th>{$labels["L-02-01-07"]|escape}</th>
					<td><input name="A_14" id="A_14" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_14"]|escape}"></td>
				</tr>
				<tr>
					<th rowspan="2">{$labels["L-02-02"]|escape}</th>
					<td>
						<div class="columns is-gapless is-multiline">
							<div class="column is-6 has-margin-top-10">
									<span>{$labels["L-02-02-01-01"]|escape}</span>
									<input name="A_19_5_1" id="A_19_5_1" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_5_1"]|escape}">
							</div>
							<div class="column is-6 has-margin-top-10">
									<span>{$labels["L-02-02-01-02"]|escape}</span>
									<input name="A_19_5_2" id="A_19_5_2" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_5_2"]|escape}">
							</div>
							<div class="column is-12 has-margin-top-10">
								<span class="has-vertical-middle has-margin-right-10">{$labels["L-02-02-01-03"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_5_3_year" id="A_19_5_3_year">
										<option value=""></option>
{foreach from=$years item=$year}
										<option value="{$year}"{if $year == $items["A_19_5_3_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_5_3_month" id="A_19_5_3_month">
										<option value=""></option>
{foreach from=$months item=$month}
										<option value="{$month}"{if $month == $items["A_19_5_3_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_5_3_day" id="A_19_5_3_day">
										<option value=""></option>
{foreach from=$days item=$day}
									<option value="{$day}"{if $day == $items["A_19_5_3_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
								</select>
								</div>
								<span>{$labels["day"]|escape}</span>
							</div>

							<div class="column is-6 has-margin-top-10">
								<span>{$labels["L-02-02-01-04"]|escape}</span>
								<input name="A_19_5_4" id="A_19_5_4" class="input has-vertical-middle has-width-60pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_5_4"]|escape}">
							</div>

							<div class="column is-6 has-margin-top-10">
								<span class="has-margin-right-10 has-vertical-middle">{$labels["L-02-02-01-05"]|escape}</span>
								<div class="is-inline field has-vertical-middle">
									<input type="radio" class="is-checkradio" id="A_19_5_5_01" name="A_19_5_5" value="1" {if $items["A_19_5_5"] == "1"} checked="checked"{/if}>
									<label for="A_19_5_5_01">{$labels["L-yes"]|escape}</label>
									<input type="radio" class="is-checkradio" id="A_19_5_5_02" name="A_19_5_5" value="2" {if $items["A_19_5_5"] == "2"} checked="checked"{/if}>
									<label for="A_19_5_5_02">{$labels["L-no"]|escape}</label>
								</div>
							</div>

							<div class="column is-12 has-margin-top-10">
								<span class="has-margin-right-10">{$labels["L-02-02-01-06"]|escape}</span>
								<input name="A_19_5_6" id="A_19_5_6" class="input has-vertical-middle has-width-70pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_5_6"]|escape}">
							</div>

							<div class="column is-12 has-margin-top-10">
								<span class="has-margin-right-10">{$labels["L-02-02-01-07"]|escape}</span>
								<input name="A_19_5_7" id="A_19_5_7" class="input has-vertical-middle has-width-50pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_5_7"]|escape}">
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="columns is-gapless is-multiline">
							<div class="column is-6 has-margin-top-10">
									<span>{$labels["L-02-02-02-01"]|escape}</span>
									<input name="A_19_6_1" id="A_19_6_1" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_6_1"]|escape}">
							</div>
							<div class="column is-6 has-margin-top-10">
									<span>{$labels["L-02-02-02-02"]|escape}</span>
									<input name="A_19_6_2" id="A_19_6_2" class="input has-width-80pct has-vertical-middle" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_6_2"]|escape}">
							</div>
							<div class="column is-12 has-margin-top-10">
								<span class="has-vertical-middle has-margin-right-10">{$labels["L-02-02-02-03"]|escape}</span>
								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_6_3_year" id="A_19_6_3_year">
										<option value=""></option>
{foreach from=$years item=$year}
										<option value="{$year}"{if $year == $items["A_19_6_3_year"]} selected="selected"{/if}>{$year}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["year"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_6_3_month" id="A_19_6_3_month">
										<option value=""></option>
{foreach from=$months item=$month}
										<option value="{$month}"{if $month == $items["A_19_6_3_month"]} selected="selected"{/if}>{$month}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["month"]|escape}</span>

								<div class="is-inlineblock select has-vertical-middle">
									<select name="A_19_6_3_day" id="A_19_6_3_day">
										<option value=""></option>
{foreach from=$days item=$day}
									<option value="{$day}"{if $day == $items["A_19_6_3_day"]} selected="selected"{/if}>{$day}</option>
{/foreach}
									</select>
								</div>
								<span>{$labels["day"]|escape}</span>
							</div>

							<div class="column is-6 has-margin-top-10">
								<span>{$labels["L-02-02-02-04"]|escape}</span>
								<input name="A_19_6_4" id="A_19_6_4" class="input has-vertical-middle has-width-60pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_6_4"]|escape}">
							</div>

							<div class="column is-6 has-margin-top-10">
								<span class="has-margin-right-10 has-vertical-middle">{$labels["L-02-02-02-05"]|escape}</span>
								<div class="is-inline field has-vertical-middle">
									<input type="radio" class="is-checkradio" id="A_19_6_5_01" name="A_19_6_5" value="1" {if $items["A_19_6_5"] == "1"} checked="checked"{/if}>
									<label for="A_19_6_5_01">{$labels["L-yes"]|escape}</label>
									<input type="radio" class="is-checkradio" id="A_19_6_5_02" name="A_19_6_5" value="2" {if $items["A_19_6_5"] == "2"} checked="checked"{/if}>
									<label for="A_19_6_5_02">{$labels["L-no"]|escape}</label>
								</div>
							</div>

							<div class="column is-12 has-margin-top-10">
								<span class="has-margin-right-10">{$labels["L-02-02-02-06"]|escape}</span>
								<input name="A_19_6_6" id="A_19_6_6" class="input has-vertical-middle has-width-70pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_6_6"]|escape}">
							</div>

							<div class="column is-12 has-margin-top-10">
								<span class="has-margin-right-10">{$labels["L-02-02-02-07"]|escape}</span>
								<input name="A_19_6_7" id="A_19_6_7" class="input has-vertical-middle has-width-50pct" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["A_19_6_7"]|escape}">
							</div>
						</div>
					</td>
				</tr>

		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
			</div>
			<div class="column is-6 has-text-right">
				<button type="button" class="button is-primary" id="regist_button">{$labels["regist"]|escape}</button>
			</div>
		</div>


	</div> <!-- /.contents -->


	<!-- footer読込部 -->
	<footer id="footer"></footer>
	</form>
</body>
</html>
