<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/WK02/WK02.js"></script>
<script type="text/javascript" src="../js/WK02/WK02007.js"></script>
</head>

<body id="WK02007">
<form id="mainForm" enctype="multipart/form-data" method="POST" action="WK02007">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="csrf_token" value="{$smarty.session["csrf_token"]}" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{if $smarty.session.loginUserType == '1'}{$labels["L-title-A"]|escape}{else}{$labels["L-title-C"]|escape}{/if}</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

		<div class="columns sidemenu">
			<div class="column is-9">
				<h3 class="title is-5">{$labels["L-sub"]|escape}</h3>
				<table class="table is-bordered has-width-100pct">
					<!--  実施日時1 -->	
					<tr>
						<th>{$labels["L-01-01"]|escape}</th>
						<td>
{if $smarty.session.loginUserType == '1'} 
							<input class="input has-width-max40pct is-inline-block flatpickr" type="text" name="impl1_from_date" id="impl1_from_date" data-mindate="today"  placeholder="日付を選択してください" value="{$items["impl1_from_date"]|escape}"　required />
							<div class="is-inlineblock select has-vertical-middle">
									<select name="impl1_from_time" id="impl1_from_time">
										<option value=""></option>
{foreach from=$times item=$time}
										<option value="{$time}"{if $time == $items["impl1_from_time"]} selected="selected"{/if}>{$time}</option>
{/foreach}
									</select>
							</div>
							<span class="has-vertical-middle">時</span>
							<div class="is-inlineblock select has-vertical-middle">
									<select name="impl1_from_minute" id="impl1_from_minute">
										<option value=""></option>
{foreach from=$minutes item=$minute}
										<option value="{$minute}"{if $minute == $items["impl1_from_minute"]} selected="selected"{/if}>{$minute}</option>
{/foreach}
									</select>
							</div>
							<span class="has-vertical-middle">分</span>
							<span class="has-vertical-middle">{$labels["from"]|escape}</span>
							<div class="is-inlineblock select has-vertical-middle">
									<select name="impl1_to_time" id="impl1_to_time">
										<option value=""></option>
{foreach from=$times item=$time}
										<option value="{$time}"{if $time == $items["impl1_to_time"]} selected="selected"{/if}>{$time}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">時</span>
							<div class="is-inlineblock select has-vertical-middle">
									<select name="impl1_to_minute" id="impl1_to_minute">
										<option value=""></option>
{foreach from=$minutes item=$minute}
										<option value="{$minute}"{if $minute == $items["impl1_to_minute"]} selected="selected"{/if}>{$minute}</option>
{/foreach}
									</select>
							</div>
							<span class="has-vertical-middle">分</span>
							<span class="has-vertical-middle">{$labels["to"]|escape}</span>
{else}
							{$items["impl1"]|escape}
{/if}
						</td>
					</tr>
					<!--  実施日時2 -->	
					<tr>
						<th>{$labels["L-01-02"]|escape}</th>
						<td>
{if $smarty.session.loginUserType == '1'} 
							<input class="input has-width-max40pct is-inline-block flatpickr" type="text" name="impl2_from_date" id="impl2_from_date" data-mindate="today"  placeholder="日付を選択してください" value="{$items["impl2_from_date"]|escape}"　required />
							<div class="is-inlineblock select has-vertical-middle">
									<select name="impl2_from_time" id="impl2_from_time">
										<option value=""></option>
{foreach from=$times item=$time}
										<option value="{$time}"{if $time == $items["impl2_from_time"]} selected="selected"{/if}>{$time}</option>
{/foreach}
									</select>
							</div>
							<span class="has-vertical-middle">時</span>
							<div class="is-inlineblock select has-vertical-middle">
									<select name="impl2_from_minute" id="impl2_from_minute">
										<option value=""></option>
{foreach from=$minutes item=$minute}
										<option value="{$minute}"{if $minute == $items["impl2_from_minute"]} selected="selected"{/if}>{$minute}</option>
{/foreach}
									</select>
							</div>
							<span class="has-vertical-middle">分</span>
							<span class="has-vertical-middle">{$labels["from"]|escape}</span>
							<div class="is-inlineblock select has-vertical-middle">
									<select name="impl2_to_time" id="impl2_to_time">
										<option value=""></option>
{foreach from=$times item=$time}
										<option value="{$time}"{if $time == $items["impl2_to_time"]} selected="selected"{/if}>{$time}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">時</span>
							<div class="is-inlineblock select has-vertical-middle">
									<select name="impl2_to_minute" id="impl2_to_minute">
										<option value=""></option>
{foreach from=$minutes item=$minute}
										<option value="{$minute}"{if $minute == $items["impl2_to_minute"]} selected="selected"{/if}>{$minute}</option>
{/foreach}
									</select>
							</div>
							<span class="has-vertical-middle">分</span>
							<span class="has-vertical-middle">{$labels["to"]|escape}</span>
{else}
							{$items["impl2"]|escape}
{/if}
						</td>
					<tr>
					<!--  実施日時3 -->	
					<tr>
						<th>{$labels["L-01-03"]|escape}</th>
						<td>
{if $smarty.session.loginUserType == '1'} 
							<input class="input has-width-max40pct is-inline-block flatpickr" type="text" name="impl3_from_date" id="impl3_from_date" data-mindate="today"  placeholder="日付を選択してください" value="{$items["impl3_from_date"]|escape}"　required />
							<div class="is-inlineblock select has-vertical-middle">
									<select name="impl3_from_time" id="impl3_from_time">
										<option value=""></option>
{foreach from=$times item=$time}
										<option value="{$time}"{if $time == $items["impl3_from_time"]} selected="selected"{/if}>{$time}</option>
{/foreach}
									</select>
							</div>
							<span class="has-vertical-middle">時</span>
							<div class="is-inlineblock select has-vertical-middle">
									<select name="impl3_from_minute" id="impl3_from_minute">
										<option value=""></option>
{foreach from=$minutes item=$minute}
										<option value="{$minute}"{if $minute == $items["impl3_from_minute"]} selected="selected"{/if}>{$minute}</option>
{/foreach}
									</select>
							</div>
							<span class="has-vertical-middle">分</span>
							<span class="has-vertical-middle">{$labels["from"]|escape}</span>
							<div class="is-inlineblock select has-vertical-middle">
									<select name="impl3_to_time" id="impl3_to_time">
										<option value=""></option>
{foreach from=$times item=$time}
										<option value="{$time}"{if $time == $items["impl3_to_time"]} selected="selected"{/if}>{$time}</option>
{/foreach}
								</select>
							</div>
							<span class="has-vertical-middle">時</span>
							<div class="is-inlineblock select has-vertical-middle">
									<select name="impl3_to_minute" id="impl3_to_minute">
										<option value=""></option>
{foreach from=$minutes item=$minute}
										<option value="{$minute}"{if $minute == $items["impl3_to_minute"]} selected="selected"{/if}>{$minute}</option>
{/foreach}
									</select>
							</div>
							<span class="has-vertical-middle">分</span>
							<span class="has-vertical-middle">{$labels["to"]|escape}</span>
{else}
							{$items["impl3"]|escape}
{/if}						</td>
					</tr>
				</table><!--  /.table -->
{if $smarty.session.loginUserType == '1'}
				<div class="has-margin-top-30 has-text-centered">
					<div class="is-pulled-left">
						<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
					</div>
{if $smarty.session.loginUserAuth <= '2'}
					<button type="button" class="button is-primary" id="regist_button">{$labels["regist"]|escape}</button>
{/if}
				</div>
{/if}
			</div>
			<div class="column is-3">
				<aside class="menu">
					<ul class="menu-list">
						<li><a href="./WK02001">{$labels["sub_menu_01"]|escape}</a></li>
						<li><a href="./WK02002">{$labels["sub_menu_02"]|escape}</a></li>
						<li>
							<a class="acMenu">{$labels["sub_menu_03"]|escape}</a>
							<ul>
								<li><a href="./WK02003">{$labels["sub_menu_03_01"]|escape}</a></li>
								<li><a href="./WK02013">{$labels["sub_menu_03_02"]|escape}</a></li>
								<li><a href="./WK02014">{$labels["sub_menu_03_03"]|escape}</a></li>
								<li><a href="./WK02004">{$labels["sub_menu_03_04"]|escape}</a></li>
								<li><a href="./WK02005">{$labels["sub_menu_03_05"]|escape}</a></li>
								<li><a href="./WK02006">{$labels["sub_menu_03_06"]|escape}</a></li>
								<li><a class="is-active" href="./WK02007">{$labels["sub_menu_03_07"]|escape}</a></li>
								<li><a href="./WK02008">{$labels["sub_menu_03_08"]|escape}</a></li>
								<li><a href="./WK02009">{$labels["sub_menu_03_09"]|escape}</a></li>
								<li><a href="./WK02010">{$labels["sub_menu_03_10"]|escape}</a></li>
							</ul>
						</li>
						<li><a href="./WK02011">{$labels["sub_menu_04"]|escape}</a></li>
						<li><a href="./WK02012">{$labels["sub_menu_05"]|escape}</a></li>
					</ul>
				</aside>
			</div>
		</div>
	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</body>
</html>
