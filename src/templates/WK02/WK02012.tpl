<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/WK02/WK02.js"></script>
<script type="text/javascript" src="../js/WK02/WK02012.js"></script>
</head>

<body id="WK02012">
<form id="mainForm" method="POST" action="WK02010">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="file_rowid" id="file_rowid" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{if $smarty.session.loginUserType == '1'}{$labels["L-title-A"]|escape}{else}{$labels["L-title-C"]|escape}{/if}</h2>

		<div class="columns sidemenu">
			<div class="column is-9">
				<h3 class="title is-5">{$labels["L-sub"]|escape}</h3>

{if count($files) == 0}
		<div class="align-center">{$labels["no_records"]|escape}</div>
{else}
				<table class="table is-bordered has-width-100pct responsive-list">
					<thead>
						<tr>
							<th class="has-width-30pct">{$labels["L-01-01-01"]|escape}</th>
							<th>{$labels["L-01-01-02"]|escape}</th>
							<th>{$labels["L-01-01-03"]|escape}</th>
							<th class="has-text-centered">{$labels["L-01-01-04"]|escape}</th>
						</tr>
					</thead>
					<tbody>
{foreach from=$files item=$row}
						<tr>
							<td>{$row["file_name"]|escape}</td>
							<td>{$row["upload_ymd"]|escape}</td>
							<td>{$row["file_type_name"]|escape}</td>
							<td class="has-text-centered">
								<button type="button" class="button is-dark detail-button"
										id="detail_button_{$row["file_rowid"]|escape}">{$labels["contents"]|escape}</button>
							</td>
						</tr>
{/foreach}
					</tbody>
				</table><!--  /.table -->
{/if}

{if $smarty.session.loginUserType == '1'}
				<button type="button" class="button is-dark has-margin-top-20" id="back_button">{$labels["back"]|escape}</button>
{/if}
			</div>
			<div class="column is-3">
				<aside class="menu">
					<ul class="menu-list">
						<li><a href="./WK02001">{$labels["sub_menu_01"]|escape}</a></li>
						<li><a href="./WK02002">{$labels["sub_menu_02"]|escape}</a></li>
						<li>
							<a class="acMenu">{$labels["sub_menu_03"]|escape}</a>
							<ul style="display: none;">
								<li><a href="./WK02003">{$labels["sub_menu_03_01"]|escape}</a></li>
								<li><a href="./WK02013">{$labels["sub_menu_03_02"]|escape}</a></li>
								<li><a href="./WK02014">{$labels["sub_menu_03_03"]|escape}</a></li>
								<li><a href="./WK02004">{$labels["sub_menu_03_04"]|escape}</a></li>
								<li><a href="./WK02005">{$labels["sub_menu_03_05"]|escape}</a></li>
								<li><a href="./WK02006">{$labels["sub_menu_03_06"]|escape}</a></li>
								<li><a href="./WK02007">{$labels["sub_menu_03_07"]|escape}</a></li>
								<li><a href="./WK02008">{$labels["sub_menu_03_08"]|escape}</a></li>
								<li><a href="./WK02009">{$labels["sub_menu_03_09"]|escape}</a></li>
								<li><a href="./WK02010">{$labels["sub_menu_03_10"]|escape}</a></li>
							</ul>
						</li>
						<li><a href="./WK02011">{$labels["sub_menu_04"]|escape}</a></li>
						<li><a class="is-active" href="./WK02012">{$labels["sub_menu_05"]|escape}</a></li>
					</ul>
				</aside>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
