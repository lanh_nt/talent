<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/WK02/WK02.js"></script>
<script type="text/javascript" src="../js/WK02/WK02001.js"></script>
</head>

<body id="WK02001">
<form id="mainForm" method="POST" action="WK02001">
<input type="hidden" name="mode" id="mode" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{if $smarty.session.loginUserType == '1'}{$labels["L-title-A"]|escape}{else}{$labels["L-title-C"]|escape}{/if}</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}
		
		<div class="columns sidemenu">
			<div class="column is-9">
				<h3 class="title is-5">{$labels["L-01"]|escape}</h3>
				<table class="table is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-01-63"]|escape}</th>
						<td>{$items["user_name"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-64"]|escape}</th>
						<td>{$items["user_name_e"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-01"]|escape}</th>
						<td>{$items["nationality_region"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-02"]|escape}</th>
						<td>{$items["birthday_ymd"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-03"]|escape}</th>
						<td>{$items["family_name"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-04"]|escape}</th>
						<td>{$items["given_name"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-05"]|escape}</th>
						<td>{$items["sex"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-06"]|escape}</th>
						<td>{$items["place_birth"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-07"]|escape}</th>
						<td>{$items["marital_flag"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-08"]|escape}</th>
						<td>{$items["occupation"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-09"]|escape}</th>
						<td>{$items["home_town"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-65"]|escape}</th>
						<td>{$items["postal_code"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-10"]|escape}</th>
						<td>{$items["address"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-11"]|escape}</th>
						<td>{$items["telephone"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-12"]|escape}</th>
						<td>{$items["cellular_phone"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-13"]|escape}</th>
						<td>{$items["A_01_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-14"]|escape}</th>
						<td>{$items["A_01_2_ymd"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-15"]|escape}</th>
						<td>{$items["A_02_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-16"]|escape}</th>
						<td>{$items["A_02_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-17"]|escape}</th>
						<td>{$items["A_03_ymd"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-18"]|escape}</th>
						<td>{$items["A_04"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-19"]|escape}</th>
						<td>{$items["A_05"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-20"]|escape}</th>
						<td>{$items["A_06"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-21"]|escape}</th>
						<td>{$items["A_07"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-22"]|escape}</th>
						<td>{$items["A_08_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-23"]|escape}</th>
						<td>{$items["A_08_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-24"]|escape}</th>
						<td>{$items["A_08_3_ymd"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-25"]|escape}</th>
						<td>{$items["A_08_4_ymd"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-26"]|escape}</th>
						<td>{$items["A_15_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-27"]|escape}</th>
						<td>{$items["A_15_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-28"]|escape}</th>
						<td>{$items["A_16_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-29"]|escape}</th>
						<td>{$items["A_17_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-30"]|escape}</th>
						<td>{$items["A_17_2_ymd"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-31"]|escape}</th>
						<td>{$items["A_18_1"]|escape}</td>
					</tr>

					<tr>
						<th rowspan="4">{$labels["L-01-32"]|escape}</th>
						<td>
							<ul>
								<li>{$labels["L-01-32-01-01"]|escape}：{$items["A_19_1_1"]|escape}</li>
								<li>{$labels["L-01-32-01-02"]|escape}：{$items["A_19_1_2"]|escape}</li>
								<li>{$labels["L-01-32-01-03"]|escape}：{$items["A_19_1_3_ymd"]|escape}</li>
								<li>{$labels["L-01-32-01-04"]|escape}：{$items["A_19_1_4"]|escape}</li>
								<li>{$labels["L-01-32-01-05"]|escape}：{$items["A_19_1_5"]|escape}</li>
								<li>{$labels["L-01-32-01-06"]|escape}：{$items["A_19_1_6"]|escape}</li>
								<li>{$labels["L-01-32-01-07"]|escape}：{$items["A_19_1_7"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-01-32-02-01"]|escape}：{$items["A_19_2_1"]|escape}</li>
								<li>{$labels["L-01-32-02-02"]|escape}：{$items["A_19_2_2"]|escape}</li>
								<li>{$labels["L-01-32-02-03"]|escape}：{$items["A_19_2_3_ymd"]|escape}</li>
								<li>{$labels["L-01-32-02-04"]|escape}：{$items["A_19_2_4"]|escape}</li>
								<li>{$labels["L-01-32-02-05"]|escape}：{$items["A_19_2_5"]|escape}</li>
								<li>{$labels["L-01-32-02-06"]|escape}：{$items["A_19_2_6"]|escape}</li>
								<li>{$labels["L-01-32-02-07"]|escape}：{$items["A_19_2_7"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-01-32-03-01"]|escape}：{$items["A_19_3_1"]|escape}</li>
								<li>{$labels["L-01-32-03-02"]|escape}：{$items["A_19_3_2"]|escape}</li>
								<li>{$labels["L-01-32-03-03"]|escape}：{$items["A_19_3_3_ymd"]|escape}</li>
								<li>{$labels["L-01-32-03-04"]|escape}：{$items["A_19_3_4"]|escape}</li>
								<li>{$labels["L-01-32-03-05"]|escape}：{$items["A_19_3_5"]|escape}</li>
								<li>{$labels["L-01-32-03-06"]|escape}：{$items["A_19_3_6"]|escape}</li>
								<li>{$labels["L-01-32-03-07"]|escape}：{$items["A_19_3_7"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-01-32-04-01"]|escape}：{$items["A_19_4_1"]|escape}</li>
								<li>{$labels["L-01-32-04-02"]|escape}：{$items["A_19_4_2"]|escape}</li>
								<li>{$labels["L-01-32-04-03"]|escape}：{$items["A_19_4_3_ymd"]|escape}</li>
								<li>{$labels["L-01-32-04-04"]|escape}：{$items["A_19_4_4"]|escape}</li>
								<li>{$labels["L-01-32-04-05"]|escape}：{$items["A_19_4_5"]|escape}</li>
								<li>{$labels["L-01-32-04-06"]|escape}：{$items["A_19_4_6"]|escape}</li>
								<li>{$labels["L-01-32-04-07"]|escape}：{$items["A_19_4_7"]|escape}</li>
							</ul>
						</td>
					</tr>

					<tr>
						<th>{$labels["L-01-33"]|escape}</th>
						<td>{$items["A_20_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-34"]|escape}</th>
						<td>{$items["A_20_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-35"]|escape}</th>
						<td>{$items["A_20_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-36"]|escape}</th>
						<td>{$items["A_21_1"]|escape}</td>
					</tr>

					<tr>
						<th rowspan="3">{$labels["L-01-37-01"]|escape}</th>
						<td>&nbsp;{$items["A_21_3"]|escape}</td>
					</tr>
					<tr>
						<td>&nbsp;{$items["A_21_4"]|escape}</td>
					</tr>
					<tr>
						<td>&nbsp;{$items["A_21_5"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-37-02"]|escape}</th>
						<td>{$items["A_21_7"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-38"]|escape}</th>
						<td>{$items["A_22_1"]|escape}</td>
					</tr>

					<tr>
						<th rowspan="3">{$labels["L-01-39-01"]|escape}</th>
						<td>&nbsp;{$items["A_22_3"]|escape}</td>
					</tr>
					<tr>
						<td>&nbsp;{$items["A_22_4"]|escape}</td>
					</tr>
					<tr>
						<td>&nbsp;{$items["A_22_5"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-39-02"]|escape}</th>
						<td>{$items["A_22_7"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-40"]|escape}</th>
						<td>{$items["A_23_1_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-41"]|escape}</th>
						<td>{$items["A_23_1_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-42"]|escape}</th>
						<td>{$items["A_23_1_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-43"]|escape}</th>
						<td>{$items["A_23_2_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-44"]|escape}</th>
						<td>{$items["A_23_2_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-45"]|escape}</th>
						<td>{$items["A_23_2_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-46"]|escape}</th>
						<td>{$items["A_24_ym"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-47"]|escape}</th>
						<td>{$items["A_25_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-48"]|escape}</th>
						<td>{$items["A_25_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-49"]|escape}</th>
						<td>{$items["A_25_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-50"]|escape}</th>
						<td>{$items["A_26_1"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-51"]|escape}</th>
						<td>{$items["A_26_2"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-52"]|escape}</th>
						<td>{$items["A_26_3"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-53"]|escape}</th>
						<td>{$items["A_27"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-54"]|escape}</th>
						<td>{$items["A_28"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-55"]|escape}</th>
						<td>{$items["A_29"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-56"]|escape}</th>
						<td>{$items["A_30"]|escape}</td>
					</tr>

					<tr>
						<th rowspan="6">{$labels["L-01-57"]|escape}</th>
						<td>
							<ul>
								<li>{$labels["L-01-57-01-01"]|escape}：{$items["A_31_1_1_ymd"]|escape}</li>
								<li>{$labels["L-01-57-01-02"]|escape}：{$items["A_31_1_2_ymd"]|escape}</li>
								<li>{$labels["L-01-57-01-03"]|escape}：{$items["A_31_1_3"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-01-57-02-01"]|escape}：{$items["A_31_2_1_ymd"]|escape}</li>
								<li>{$labels["L-01-57-02-02"]|escape}：{$items["A_31_2_2_ymd"]|escape}</li>
								<li>{$labels["L-01-57-02-03"]|escape}：{$items["A_31_2_3"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-01-57-03-01"]|escape}：{$items["A_31_3_1_ymd"]|escape}</li>
								<li>{$labels["L-01-57-03-02"]|escape}：{$items["A_31_3_2_ymd"]|escape}</li>
								<li>{$labels["L-01-57-03-03"]|escape}：{$items["A_31_3_3"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-01-57-04-01"]|escape}：{$items["A_31_4_1_ymd"]|escape}</li>
								<li>{$labels["L-01-57-04-02"]|escape}：{$items["A_31_4_2_ymd"]|escape}</li>
								<li>{$labels["L-01-57-04-03"]|escape}：{$items["A_31_4_3"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-01-57-05-01"]|escape}：{$items["A_31_5_1_ymd"]|escape}</li>
								<li>{$labels["L-01-57-05-02"]|escape}：{$items["A_31_5_2_ymd"]|escape}</li>
								<li>{$labels["L-01-57-05-03"]|escape}：{$items["A_31_5_3"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-01-57-06-01"]|escape}：{$items["A_31_6_1_ymd"]|escape}</li>
								<li>{$labels["L-01-57-06-02"]|escape}：{$items["A_31_6_2_ymd"]|escape}</li>
								<li>{$labels["L-01-57-06-03"]|escape}：{$items["A_31_6_3"]|escape}</li>
							</ul>
						</td>
					</tr>

					<tr>
						<th>{$labels["L-01-58"]|escape}</th>
						<td>{$items["A_32"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-59"]|escape}</th>
						<td>{$items["A_33"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-60"]|escape}</th>
						<td>{$items["A_34"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-61"]|escape}</th>
						<td>{$items["A_35"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-62"]|escape}</th>
						<td>{$items["A_36"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-66"]|escape}</th>
						<td>{$items["A_37"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-67"]|escape}</th>
						<td>{$items["A_38"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-68"]|escape}</th>
						<td>{$items["A_39"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-69"]|escape}</th>
						<td>{$items["A_40"]|escape}</td>
					</tr>

					<tr>
						<th>{$labels["L-01-70"]|escape}</th>
						<td>{$items["A_41"]|escape}</td>
					</tr>

				</table><!--  /.table -->

				<h3 class="title is-5  has-margin-top-50">{$labels["L-02-title"]|escape}</h3>
				<table class="table is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-02-01-01"]|escape}</th>
						<td>{$items["A_09_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-01-02"]|escape}</th>
						<td>{$items["A_09_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-01-03"]|escape}</th>
						<td>{$items["A_09_3_ymd"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-01-04"]|escape}</th>
						<td>{$items["A_10"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-01-05"]|escape}</th>
						<td>{$items["A_12"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-01-06"]|escape}</th>
						<td>{$items["A_13"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-01-07"]|escape}</th>
						<td>{$items["A_11"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-02-01-08"]|escape}</th>
						<td>{$items["A_14"]|escape}</td>
					</tr>
					<tr>
						<th rowspan="2">{$labels["L-02-02"]|escape}</th>
						<td>
							<ul>
								<li>{$labels["L-02-02-05-01"]|escape}：{$items["A_19_5_1"]|escape}</li>
								<li>{$labels["L-02-02-05-02"]|escape}：{$items["A_19_5_2"]|escape}</li>
								<li>{$labels["L-02-02-05-03"]|escape}：{$items["A_19_5_3_ymd"]|escape}</li>
								<li>{$labels["L-02-02-05-04"]|escape}：{$items["A_19_5_4"]|escape}</li>
								<li>{$labels["L-02-02-05-05"]|escape}：{$items["A_19_5_5"]|escape}</li>
								<li>{$labels["L-02-02-05-06"]|escape}：{$items["A_19_5_6"]|escape}</li>
								<li>{$labels["L-02-02-05-07"]|escape}：{$items["A_19_5_7"]|escape}</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td>
							<ul>
								<li>{$labels["L-02-02-06-01"]|escape}：{$items["A_19_6_1"]|escape}</li>
								<li>{$labels["L-02-02-06-02"]|escape}：{$items["A_19_6_2"]|escape}</li>
								<li>{$labels["L-02-02-06-03"]|escape}：{$items["A_19_6_3_ymd"]|escape}</li>
								<li>{$labels["L-02-02-06-04"]|escape}：{$items["A_19_6_4"]|escape}</li>
								<li>{$labels["L-02-02-06-05"]|escape}：{$items["A_19_6_5"]|escape}</li>
								<li>{$labels["L-02-02-06-06"]|escape}：{$items["A_19_6_6"]|escape}</li>
								<li>{$labels["L-02-02-06-07"]|escape}：{$items["A_19_6_7"]|escape}</li>
							</ul>
						</td>
					</tr>
				</table><!--  /.table -->
				<div class="has-margin-top-30 has-text-centered">
{if $smarty.session.loginUserType == '1' }
					<div class="is-pulled-left">
						<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
					</div>
{/if}
{if $smarty.session.loginUserAuth <= '2' || $smarty.session.loginUserAuth == '9'}
					<button type="button" class="button is-primary" id="udate_button">{$labels["edit"]|escape}</button>
{/if}
				</div>
			</div>

			<div class="column is-3">
				<aside class="menu">
					<ul class="menu-list">
						<li><a class="is-active" href="./WK02001">{$labels["sub_menu_01"]|escape}</a></li>
						<li><a href="./WK02002">{$labels["sub_menu_02"]|escape}</a></li>
						<li>
							<a class="acMenu">{$labels["sub_menu_03"]|escape}</a>
							<ul style="display: none;">
								<li><a href="./WK02003">{$labels["sub_menu_03_01"]|escape}</a></li>
								<li><a href="./WK02013">{$labels["sub_menu_03_02"]|escape}</a></li>
								<li><a href="./WK02014">{$labels["sub_menu_03_03"]|escape}</a></li>
								<li><a href="./WK02004">{$labels["sub_menu_03_04"]|escape}</a></li>
								<li><a href="./WK02005">{$labels["sub_menu_03_05"]|escape}</a></li>
								<li><a href="./WK02006">{$labels["sub_menu_03_06"]|escape}</a></li>
								<li><a href="./WK02007">{$labels["sub_menu_03_07"]|escape}</a></li>
								<li><a href="./WK02008">{$labels["sub_menu_03_08"]|escape}</a></li>
								<li><a href="./WK02009">{$labels["sub_menu_03_09"]|escape}</a></li>
								<li><a href="./WK02010">{$labels["sub_menu_03_10"]|escape}</a></li>
							</ul>
						</li>
						<li><a href="./WK02011">{$labels["sub_menu_04"]|escape}</a></li>
						<li><a href="./WK02012">{$labels["sub_menu_05"]|escape}</a></li>
					</ul>
				</aside>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</body>
</html>
