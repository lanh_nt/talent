<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/WK02/WK02.js"></script>
<script type="text/javascript" src="../js/WK02/WK02009.js"></script>
</head>

<body id="WK02009">
<form id="mainForm" enctype="multipart/form-data" method="POST" action="WK02009">
<input type="hidden" name="mode" id="mode" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{if $smarty.session.loginUserType == '1'}{$labels["L-title-A"]|escape}{else}{$labels["L-title-C"]|escape}{/if}</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

		<div class="columns sidemenu">
			<div class="column is-9">
				<h3 class="title is-5">{$labels["L-sub"]|escape}</h3>
				<h3 class="title is-6 has-margin-top-50">{$labels["L-01"]|escape}</h3>
				<table class="table is-bordered is-bordered has-width-100pct">
					<tr>
						<th>{$labels["L-01-01-01"]|escape}</th>
						<td>{$items["E_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-01-02"]|escape}</th>
						<td>{$items["E_2_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-01-03"]|escape}</th>
						<td>{$items["E_2_3"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-01-02"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-02-01"]|escape}</th>
						<td>{$items["E_3_1_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-02-02"]|escape}</th>
						<td>{$items["E_3_1_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-02-03"]|escape}</th>
						<td>{$items["E_3_1_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-02-04"]|escape}</th>
						<td>{$items["E_3_1_2_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-02-05"]|escape}</th>
						<td>{$items["E_3_1_3_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-02-06"]|escape}</th>
						<td>{$items["E_3_1_3_2"]|escape}</td>
					</tr>

					<tr>
						<th colspan="2">{$labels["L-01-03"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-03-01"]|escape}</th>
						<td>{$items["E_3_2_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03-02"]|escape}</th>
						<td>{$items["E_3_2_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03-03"]|escape}</th>
						<td>{$items["E_3_2_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03-04"]|escape}</th>
						<td>{$items["E_3_2_2_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03-05"]|escape}</th>
						<td>{$items["E_3_2_3_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03-06"]|escape}</th>
						<td>{$items["E_3_2_3_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03-07"]|escape}</th>
						<td>{$items["E_3_2_4_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03-08"]|escape}</th>
						<td>{$items["E_3_2_4_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03-09"]|escape}</th>
						<td>{$items["E_3_2_5_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03-10"]|escape}</th>
						<td>{$items["E_3_2_5_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03-11"]|escape}</th>
						<td>{$items["E_3_2_6_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-03-12"]|escape}</th>
						<td>{$items["E_3_2_6_2"]|escape}</td>
					</tr>

					<tr>
						<th colspan="2">{$labels["L-01-04"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-04-01"]|escape}</th>
						<td>{$items["E_3_3_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-04-02"]|escape}</th>
						<td>{$items["E_3_3_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-04-03"]|escape}</th>
						<td>{$items["E_3_3_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-04-04"]|escape}</th>
						<td>{$items["E_3_3_2_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-04-05"]|escape}</th>
						<td>{$items["E_3_3_3_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-04-06"]|escape}</th>
						<td>{$items["E_3_3_3_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-04-07"]|escape}</th>
						<td>{$items["E_3_3_4_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-04-08"]|escape}</th>
						<td>{$items["E_3_3_4_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-04-09"]|escape}</th>
						<td>{$items["E_3_3_5_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-04-10"]|escape}</th>
						<td>{$items["E_3_3_5_2"]|escape}</td>
					</tr>

					<tr>
						<th colspan="2">{$labels["L-01-05"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-05-01"]|escape}</th>
						<td>{$items["E_3_4_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-05-02"]|escape}</th>
						<td>{$items["E_3_4_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-05-03"]|escape}</th>
						<td>{$items["E_3_4_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-05-04"]|escape}</th>
						<td>{$items["E_3_4_2_2"]|escape}</td>
					</tr>

					<tr>
						<th colspan="2">{$labels["L-01-06"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-06-01"]|escape}</th>
						<td>{$items["E_3_5_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-06-02"]|escape}</th>
						<td>{$items["E_3_5_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-06-03"]|escape}</th>
						<td>{$items["E_3_5_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-06-04"]|escape}</th>
						<td>{$items["E_3_5_2_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-06-05"]|escape}</th>
						<td>{$items["E_3_5_2_3"]|escape}</td>
					</tr>

					<tr>
						<th colspan="2">{$labels["L-01-07"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-07-01"]|escape}</th>
						<td>{$items["E_3_6"]|escape}</td>
					</tr>

					<tr>
						<th colspan="2">{$labels["L-01-08"]|escape}</th>
					</tr>
					<tr>
						<td colspan="2">{$items["E_3_7"]|escape}&nbsp;</td>
					</tr>
					<tr>
						<th>{$labels["L-01-09-01"]|escape}</th>
						<td>{$items["E_4_1_ymd"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-09-02"]|escape}</th>
						<td>{$items["E_4_2"]|escape}</td>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-01-10"]|escape}</th>
					</tr>
					<tr>
						<th colspan="2">{$labels["L-01-10-01"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-10-01-01"]|escape}</th>
						<td>{$items["E_4_3_1_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-01-02"]|escape}</th>
						<td>{$items["E_4_3_1_2"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-01-03"]|escape}</th>
						<td>{$items["E_4_3_1_3"]|escape}</td>
					</tr>

					<tr>
						<th colspan="2">{$labels["L-01-10-02"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-10-02-01"]|escape}</th>
						<td>{$items["E_4_3_2_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-02-02"]|escape}</th>
						<td>{$items["E_4_3_2_2_ymd"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-02-03"]|escape}</th>
						<td>{$items["E_4_3_2_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-02-04"]|escape}</th>
						<td>{$items["E_4_3_2_4"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-02-05"]|escape}</th>
						<td>{$items["E_4_3_2_5"]|escape}</td>
					</tr>

					<tr>
						<th colspan="2">{$labels["L-01-10-03"]|escape}</th>
					</tr>
					<tr>
						<th>{$labels["L-01-10-03-01"]|escape}</th>
						<td>{$items["E_4_3_3_1"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-03-02"]|escape}</th>
						<td>{$items["E_4_3_3_2_ymd"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-03-03"]|escape}</th>
						<td>{$items["E_4_3_3_3"]|escape}</td>
					</tr>
					<tr>
						<th>{$labels["L-01-10-03-04"]|escape}</th>
						<td>{$items["E_4_3_3_4"]|escape|nl2br}</td>
					</tr>
				</table><!--  /.table -->

{if $smarty.session.loginUserType == '1'}
				<div class="has-margin-top-30 has-text-centered">
					<div class="is-pulled-left">
						<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
					</div>
{if $smarty.session.loginUserAuth <= '2'}
					<input type="file" name="upfile" id="upfile" style="display:none;" />
					<button type="button" class="button is-primary has-margin-right-20" id="csv_import_button">{$labels["csv_import"]|escape}</button>
					<button type="button" class="button is-primary has-margin-right-20" id="csv_export_button">{$labels["csv_export"]|escape}</button>
					<button type="button" class="button is-primary has-margin-right-20" id="csv_format_button">{$labels["csv_format"]|escape}</button>
{/if}
				</div>
{/if}

			</div>

			<div class="column is-3">
				<aside class="menu">
					<ul class="menu-list">
						<li><a href="./WK02001">{$labels["sub_menu_01"]|escape}</a></li>
						<li><a href="./WK02002">{$labels["sub_menu_02"]|escape}</a></li>
						<li>
							<a class="acMenu">{$labels["sub_menu_03"]|escape}</a>
							<ul>
								<li><a href="./WK02003">{$labels["sub_menu_03_01"]|escape}</a></li>
								<li><a href="./WK02013">{$labels["sub_menu_03_02"]|escape}</a></li>
								<li><a href="./WK02014">{$labels["sub_menu_03_03"]|escape}</a></li>
								<li><a href="./WK02004">{$labels["sub_menu_03_04"]|escape}</a></li>
								<li><a href="./WK02005">{$labels["sub_menu_03_05"]|escape}</a></li>
								<li><a href="./WK02006">{$labels["sub_menu_03_06"]|escape}</a></li>
								<li><a href="./WK02007">{$labels["sub_menu_03_07"]|escape}</a></li>
								<li><a href="./WK02008">{$labels["sub_menu_03_08"]|escape}</a></li>
								<li><a class="is-active" href="./WK02009">{$labels["sub_menu_03_09"]|escape}</a></li>
								<li><a href="./WK02010">{$labels["sub_menu_03_10"]|escape}</a></li>
							</ul>
						</li>
						<li><a href="./WK02011">{$labels["sub_menu_04"]|escape}</a></li>
						<li><a href="./WK02012">{$labels["sub_menu_05"]|escape}</a></li>
					</ul>
				</aside>
			</div>
		</div>
	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</body>
</html>
