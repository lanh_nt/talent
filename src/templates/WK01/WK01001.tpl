<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/WK01/WK01001.js"></script>
</head>

<body id="WK01001">
<form id="mainForm" enctype="multipart/form-data" method="POST" action="WK01001">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}" />
<input type="hidden" name="worker_id" id="worker_id" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">労働者管理</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

{if $smarty.session.loginUserAuth <= '2'}
		<div class="has-text-right has-margin-bottom-20">
			<input type="file" name="upfile" id="upfile" style="display:none;" />
			<button type="button" class="button is-primary has-margin-right-20" id="csv_import_button">{$labels["csv_import"]|escape}</button>
			<button type="button" class="button is-primary has-margin-right-20" id="csv_export_button">{$labels["csv_export"]|escape}</button>
			<button type="button" class="button is-primary has-margin-right-20" id="csv_format_button">{$labels["csv_format"]|escape}</button>
		</div>
{/if}

		<div class="box has-padding-20">
			<div class="columns is-multiline formbox-gap">
				<!-- ID -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">労働者ID：</p>
				</div>
				<div class="column is-9">
					<input class="input" type="text" name="C_01" id="C_01" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_01"]|escape}">
				</div>

				<!-- 労働者氏名 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">労働者氏名：</p>
				</div>
				<div class="column is-9">
					<input class="input" type="text" name="C_02" id="C_02" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_02"]|escape}">
				</div>

				<!-- 在留資格 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">在留資格：</p>
				</div>
				<div class="column is-9">
					<div class="field">
						<input class="is-checkradio" type="checkbox" name="C_03_1" id="C_03_1" {if $items["C_03_1"] != ""}checked="checked"{/if}>
						<label for="C_03_1">留学生</label>
						<input class="is-checkradio" type="checkbox" name="C_03_2" id="C_03_2" {if $items["C_03_2"] != ""}checked="checked"{/if}>
						<label for="C_03_2">特定技能1号</label>
						<input class="is-checkradio" type="checkbox" name="C_03_3" id="C_03_3" {if $items["C_03_3"] != ""}checked="checked"{/if}>
						<label for="C_03_3">特定技能2号</label>
						<input class="is-checkradio" type="checkbox" name="C_03_4" id="C_03_4" {if $items["C_03_4"] != ""}checked="checked"{/if}>
						<label for="C_03_4">技人国</label>
						<input class="is-checkradio" type="checkbox" name="C_03_5" id="C_03_5" {if $items["C_03_5"] != ""}checked="checked"{/if}>
						<label for="C_03_5">国外在留者</label>
						<input class="is-checkradio" type="checkbox" name="C_03_6" id="C_03_6" {if $items["C_03_6"] != ""}checked="checked"{/if}>
						<label for="C_03_6">その他</label>
						<div class="select is-small">
							<select name="C_03_etc" id="C_03_etc">
								<option value=""></option>
{foreach from=$purposes key=$pkey item=$pname}
								<option value="{$pkey}"{if $pkey == $items["C_03_etc"]} selected="selected"{/if}>{$pname}</option>
{/foreach}
							</select>
						</div>
					</div>
				</div>

				<!-- ステータス -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">ステータス：</p>
				</div>
				<div class="column is-9">
					<div class="field">
						<input class="is-checkradio" type="checkbox" name="C_04_1" id="C_04_1" {if $items["C_04_1"] != ""}checked="checked"{/if}>
						<label for="C_04_1">在籍</label>
						<input class="is-checkradio" type="checkbox" name="C_04_2" id="C_04_2" {if $items["C_04_2"] != ""}checked="checked"{/if}>
						<label for="C_04_2">退職</label>
						<input class="is-checkradio" type="checkbox" name="C_04_3" id="C_04_3" {if $items["C_04_3"] != ""}checked="checked"{/if}>
						<label for="C_04_3">辞退</label>
						<input class="is-checkradio" type="checkbox" name="C_04_4" id="C_04_4" {if $items["C_04_4"] != ""}checked="checked"{/if}>
						<label for="C_04_4">不許可</label>
						<input class="is-checkradio" type="checkbox" name="C_04_5" id="C_04_5" {if $items["C_04_5"] != ""}checked="checked"{/if}>
						<label for="C_04_5">帰国</label>
						<input class="is-checkradio" type="checkbox" name="C_04_6" id="C_04_6" {if $items["C_04_6"] != ""}checked="checked"{/if}>
						<label for="C_04_6">入社前</label>
					</div>
				</div>

				<!-- 性別 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">性別：</p>
				</div>
				<div class="column is-9">
					<div class="field">
						<input class="is-checkradio" type="checkbox" name="C_05_1" id="C_05_1" {if $items["C_05_1"] != ""}checked="checked"{/if}>
						<label for="C_05_1">男</label>
						<input class="is-checkradio" type="checkbox" name="C_05_2" id="C_05_2" {if $items["C_05_2"] != ""}checked="checked"{/if}>
						<label for="C_05_2">女</label>
					</div>
				</div>

				<!-- 企業名 -->
{if $smarty.session.loginCompanyType != '2'}
				<input type="hidden" name="C_06" id="C_06" value="{$smarty.session.loginCompanyRowId|escape}">
{else}
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">企業名：</p>
				</div>
				<div class="column is-9">
					<div class="select has-vertical-middle has-width-100pct">
						<select name="C_06" id="C_06" class="has-width-100pct">
							<option value=""></option>
{foreach from=$companies item=$company}
							<option value="{$company["id"]}"{if $company["id"] == $items["C_06"]} selected="selected"{/if}>{$company["name"]}</option>
{/foreach}
						</select>
					</div>
				</div>
{/if}

				<!-- 登録日 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">登録日：</p>
				</div>
				<div class="column is-9">
					<input id="C_07_1" name="C_07_1" type="text" class="input has-width-max40pct is-inline-block flatpickr" value="{$items["C_07_1"]|escape}" required />
					<span class="has-vertical-middle">～</span>
					<input id="C_07_2" name="C_07_2" type="text" class="input has-width-max40pct is-inline-block flatpickr is-back-white" value="{$items["C_07_2"]|escape}" required />
				</div>

				<!-- 在留期間の満了日 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">在留期間の満了日：</p>
				</div>
				<div class="column is-9">
					<input id="C_08_1" name="C_08_1" type="text" class="input has-width-max40pct is-inline-block flatpickr" value="{$items["C_08_1"]|escape}" required />
					<span class="has-vertical-middle">～</span>
					<input id="C_08_2" name="C_08_2" type="text" class="input has-width-max40pct is-inline-block flatpickr is-back-white" value="{$items["C_08_2"]|escape}" required />
				</div>

			</div><!--  /.columns -->

			<div class="has-text-centered has-margin-top-20">
				<a class="button is-info submit-button" id="search_button">検索</a>
			</div>
		</div>

{if isset($pageData->pageDatas)}
{if count($pageData->pageDatas) == 0}
		<div class="align-center">検索結果がありません。</div>
{else}
		<input type="hidden" name="page_no" id="page_no" value="{$pageData->currentPageNo}">
		<table class="table has-margin-top-50 has-width-100pct responsive-list">
			<thead>
				<tr>
					<th>ID</th>
					<th>氏名</th>
					<th>在留資格</th>
					<th>企業名</th>
					<th>登録日</th>
					<th>詳細</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>ID</th>
					<th>氏名</th>
					<th>在留資格</th>
					<th>企業名</th>
					<th>登録日</th>
					<th>詳細</th>
				</tr>
			</tfoot>
			<tbody>
{foreach from=$pageData->pageDatas item=$row}
				<tr>
					<td>{$row["worker_id"]|escape}</td>
					<td>{$row["user_name"]|escape}</td>
					<td>{$row["qualifications_name"]|escape}</td>
					<td>{$row["company_name"]|escape}</td>
					<td>{$row["create_date_ymd"]|escape}</td>
					<td>
						<button type="button" class="button is-dark detail-button" id="detail_button_{$row["worker_id"]|escape}">{$labels["contents"]|escape}</button>
					</td>
				</tr>
{/foreach}
			</tbody>
		</table>

{include file='common/pager.tpl' pageData=$pageData}{* --- ページング部品 --- *}

{/if}
{/if}

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
