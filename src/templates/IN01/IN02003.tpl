<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/IN01/IN02003.js"></script>
</head>

<body id="IN02003">
<form id="mainForm" method="POST" action="IN02003">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="csrf_token" value="{$smarty.session["csrf_token"]}" />
<input type="hidden" name="user_id" id="user_id" value="{$items["user_id"]|escape}" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">面談記録/登録</h2>

{* --- メッセージ欄部品 --- *}
{include file='common/message.tpl' messages=$messages}

		<table class="table is-bordered has-margin-top-50 has-width-100pct">
			<tr>
				<th>管理者ID</th>
				<td>{$items["user_id"]|escape}</td>
			</tr>
			<tr>
				<th>監督者の氏名及び役職</th>
				<td>{$items["user_name"]|escape} {$items["user_department"]|escape}</td>
				<input type="hidden" name="user_name" value="{$items["user_name"]|escape}" />
			</tr>
			<tr>
				<th>監督者の所属部署</th>
				<td>{$items["supervisor_department"]|escape}</td>
				<input type="hidden" name="supervisor_department" value="{$items["supervisor_department"]|escape}" />
			</tr>
			<tr>
				<th>面談日</th>
				<td>
					<input id="interview_ymd" name="interview_ymd" type="text" class="input has-width-max40pct flatpickr" placeholder="日付を選択してください" value="{$items["interview_ymd"]|escape}" required />
				</td>
			</tr>
		</table>

		<h3 class="title is-5 has-margin-top-50">面談対応者</h3>
		<table class="table is-bordered is-bordered has-width-100pct">
			<tr>
				<th>対応者の氏名</th>
				<td>
					<input name="F_2_1" id="F_2_1" type="text" class="input" placeholder="入力してください" value="{$items["F_2_1"]|escape}">
				</td>
			</tr>
			<tr>
				<th>対応者の役職</th>
				<td>
					<div class="select has-vertical-middle">
						<select name="F_2_2" id="F_2_2">
							<option value=""></option>
							<option value="1"{if $items["F_2_2"] == "1"} selected="selected"{/if}>支援責任者</option>
							<option value="2"{if $items["F_2_2"] == "2"} selected="selected"{/if}>支援担当者</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<th>役職名</th>
				<td>
					<input name="F_2_3" id="F_2_3" type="text" class="input" placeholder="入力してください" value="{$items["F_2_3"]|escape}">
				</td>
			</tr>
		</table>

		<h3 class="title is-5 has-margin-top-50">面談結果</h3>
		<table class="table is-bordered is-bordered has-width-100pct">
			<tr>
				<th colspan="2">業務内容に関する事項</th>
			</tr>
			<tr>
				<th>雇用契約と異なる業務に従事させていないこと。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_1_1_1" id="F_3_1_1_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_1_1_1"] == "1"} checked="checked"{/if}><label for="F_3_1_1_1_1">有</label>
						<input name="F_3_1_1_1" id="F_3_1_1_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_1_1_1"] == "2"} checked="checked"{/if}><label for="F_3_1_1_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_1_1_2" id="F_3_1_1_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_1_1_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>他の事業主の下で業務に従事させていないこと。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_1_2_1" id="F_3_1_2_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_1_2_1"] == "1"} checked="checked"{/if}><label for="F_3_1_2_1_1">有</label>
						<input name="F_3_1_2_1" id="F_3_1_2_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_1_2_1"] == "2"} checked="checked"{/if}><label for="F_3_1_2_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_1_2_2" id="F_3_1_2_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_1_2_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>安全衛生に配慮して適切に業務を行わせていること。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_1_3_1" id="F_3_1_3_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_1_3_1"] == "1"} checked="checked"{/if}><label for="F_3_1_3_1_1">有</label>
						<input name="F_3_1_3_1" id="F_3_1_3_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_1_3_1"] == "2"} checked="checked"{/if}><label for="F_3_1_3_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_1_3_2" id="F_3_1_3_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_1_3_2"]|escape}">
				</td>
			</tr>

			<tr>
				<th colspan="2">待遇に関する事項</th>
			</tr>
			<tr>
				<th>雇用契約に基づき毎月適切に報酬を支払っていること。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_2_1_1" id="F_3_2_1_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_2_1_1"] == "1"} checked="checked"{/if}><label for="F_3_2_1_1_1">有</label>
						<input name="F_3_2_1_1" id="F_3_2_1_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_2_1_1"] == "2"} checked="checked"{/if}><label for="F_3_2_1_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_2_1_2" id="F_3_2_1_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_2_1_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>雇用契約と異なる労働時間とさせていないこと。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_2_2_1" id="F_3_2_2_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_2_2_1"] == "1"} checked="checked"{/if}><label for="F_3_2_2_1_1">有</label>
						<input name="F_3_2_2_1" id="F_3_2_2_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_2_2_1"] == "2"} checked="checked"{/if}><label for="F_3_2_2_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_2_2_2" id="F_3_2_2_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_2_2_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>休日，休暇等を適切に付与していること（一時帰国休暇を含む。）。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_2_3_1" id="F_3_2_3_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_2_3_1"] == "1"} checked="checked"{/if}><label for="F_3_2_3_1_1">有</label>
						<input name="F_3_2_3_1" id="F_3_2_3_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_2_3_1"] == "2"} checked="checked"{/if}><label for="F_3_2_3_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_2_3_2" id="F_3_2_3_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_2_3_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>適切な住居を確保していること。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_2_4_1" id="F_3_2_4_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_2_4_1"] == "1"} checked="checked"{/if}><label for="F_3_2_4_1_1">有</label>
						<input name="F_3_2_4_1" id="F_3_2_4_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_2_4_1"] == "2"} checked="checked"{/if}><label for="F_3_2_4_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_2_4_2" id="F_3_2_4_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_2_4_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>定期的に負担する食費，居住費等を合意したとおりの内容で徴収していること。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_2_5_1" id="F_3_2_5_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_2_5_1"] == "1"} checked="checked"{/if}><label for="F_3_2_5_1_1">有</label>
						<input name="F_3_2_5_1" id="F_3_2_5_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_2_5_1"] == "2"} checked="checked"{/if}><label for="F_3_2_5_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_2_5_2" id="F_3_2_5_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_2_5_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>支援計画にのっとった支援の提供を行っていること。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_2_6_1" id="F_3_2_6_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_2_6_1"] == "1"} checked="checked"{/if}><label for="F_3_2_6_1_1">有</label>
						<input name="F_3_2_6_1" id="F_3_2_6_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_2_6_1"] == "2"} checked="checked"{/if}><label for="F_3_2_6_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_2_6_2" id="F_3_2_6_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_2_6_2"]|escape}">
				</td>
			</tr>

			<tr>
				<th colspan="2">保護に関する事項</th>
			</tr>
			<tr>
				<th>暴行・脅迫・監禁等の不法行為を行っていないこと。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_3_1_1" id="F_3_3_1_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_3_1_1"] == "1"} checked="checked"{/if}><label for="F_3_3_1_1_1">有</label>
						<input name="F_3_3_1_1" id="F_3_3_1_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_3_1_1"] == "2"} checked="checked"{/if}><label for="F_3_3_1_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_3_1_2" id="F_3_3_1_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_3_1_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>相手方を問わず保証金の徴収・違約金を定める契約等を締結していないこと。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_3_2_1" id="F_3_3_2_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_3_2_1"] == "1"} checked="checked"{/if}><label for="F_3_3_2_1_1">有</label>
						<input name="F_3_3_2_1" id="F_3_3_2_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_3_2_1"] == "2"} checked="checked"{/if}><label for="F_3_3_2_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_3_2_2" id="F_3_3_2_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_3_2_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>預金通帳の管理など不当な財産管理を行っていないこと。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_3_3_1" id="F_3_3_3_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_3_3_1"] == "1"} checked="checked"{/if}><label for="F_3_3_3_1_1">有</label>
						<input name="F_3_3_3_1" id="F_3_3_3_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_3_3_1"] == "2"} checked="checked"{/if}><label for="F_3_3_3_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_3_3_2" id="F_3_3_3_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_3_3_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>旅券・在留カードを管理していないこと。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_3_4_1" id="F_3_3_4_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_3_4_1"] == "1"} checked="checked"{/if}><label for="F_3_3_4_1_1">有</label>
						<input name="F_3_3_4_1" id="F_3_3_4_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_3_4_1"] == "2"} checked="checked"{/if}><label for="F_3_3_4_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_3_4_2" id="F_3_3_4_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_3_4_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>私生活上の自由を不当に制限していないこと。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_3_5_1" id="F_3_3_5_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_3_5_1"] == "1"} checked="checked"{/if}><label for="F_3_3_5_1_1">有</label>
						<input name="F_3_3_5_1" id="F_3_3_5_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_3_5_1"] == "2"} checked="checked"{/if}><label for="F_3_3_5_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_3_5_2" id="F_3_3_5_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_3_5_2"]|escape}">
				</td>
			</tr>

			<tr>
				<th colspan="2">生活に関する事項</th>
			</tr>
			<tr>
				<th>日常生活においてトラブルが発生していないこと。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_4_1_1" id="F_3_4_1_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_4_1_1"] == "1"} checked="checked"{/if}><label for="F_3_4_1_1_1">有</label>
						<input name="F_3_4_1_1" id="F_3_4_1_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_4_1_1"] == "2"} checked="checked"{/if}><label for="F_3_4_1_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_4_1_2" id="F_3_4_1_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_4_1_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>健康診断を定期的に実施し，健康状態に異常がないことを確認していること。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_4_2_1" id="F_3_4_2_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_4_2_1"] == "1"} checked="checked"{/if}><label for="F_3_4_2_1_1">有</label>
						<input name="F_3_4_2_1" id="F_3_4_2_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_4_2_1"] == "2"} checked="checked"{/if}><label for="F_3_4_2_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_4_2_2" id="F_3_4_2_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_4_2_2"]|escape}">
				</td>
			</tr>

			<tr>
				<th colspan="2">その他の事項</th>
			</tr>
			<tr>
				<th>不法就労者を雇用していないこと。</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_5_1_1" id="F_3_5_1_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_5_1_1"] == "1"} checked="checked"{/if}><label for="F_3_5_1_1_1">有</label>
						<input name="F_3_5_1_1" id="F_3_5_1_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_5_1_1"] == "2"} checked="checked"{/if}><label for="F_3_5_1_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_5_1_2" id="F_3_5_1_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_5_1_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>その他</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_5_2_1" id="F_3_5_2_1_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_5_2_1"] == "1"} checked="checked"{/if}><label for="F_3_5_2_1_1">有</label>
						<input name="F_3_5_2_1" id="F_3_5_2_1_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_5_2_1"] == "2"} checked="checked"{/if}><label for="F_3_5_2_1_2">無</label>
					</div>
				</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>
					<input name="F_3_5_2_2" id="F_3_5_2_2" type="text" class="input" placeholder="入力してください" value="{$items["F_3_5_2_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>
					<input name="F_3_5_2_3" id="F_3_5_2_3" type="text" class="input" placeholder="入力してください" value="{$items["F_3_5_2_3"]|escape}">
				</td>
			</tr>

			<tr>
				<th colspan="2">法令違反等の有無</th>
			</tr>
			<tr>
				<th>法令違反の有無</th>
				<td>
					<div class="is-inline field has-vertical-middle">
						<input name="F_3_6" id="F_3_6_1" type="radio" class="is-checkradio" value="1"{if $items["F_3_6"] == "1"} checked="checked"{/if}><label for="F_3_6_1">有</label>
						<input name="F_3_6" id="F_3_6_2" type="radio" class="is-checkradio" value="2"{if $items["F_3_6"] == "2"} checked="checked"{/if}><label for="F_3_6_2">無</label>
					</div>
				</td>
			</tr>

			<tr>
				<th colspan="2">その他特筆すべき事項</th>
			</tr>
			<tr>
				<td colspan="2">
					<input name="F_3_7" id="F_3_7" type="text" class="input" placeholder="入力してください" value="{$items["F_3_7"]|escape}">
				</td>
			</tr>
		</table>

		<!-- 法令違反が発生 -->
		<h3 class="title is-5">法令違反への対応</h3>

		<table class="table is-bordered">
			<tr>
				<th>法令違反事実の発生年月</th>
				<td>
					<input id="F_4_1_ymd" name="F_4_1_ymd" type="text" class="input is-inline-block flatpickr" placeholder="日付を選択してください" value="{$items["F_4_1_ymd"]|escape}" required />
				</td>
			</tr>
			<tr>
				<th>法令違反事実の内容</th>
				<td>
					<input name="F_4_2" id="F_4_2" type="text" class="input" placeholder="入力してください" value="{$items["F_4_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th colspan="2">法令違反事実への対応結果</th>
			</tr>
			<tr>
				<th colspan="2">1号特定技能外国人への対応</th>
			</tr>
			<tr>
				<th>1号特定技能外国人への対応</th>
				<td>
					<ol class="input__subtext">
						<li>労働基準監督署等の関係行政機関を案内</li>
						<li>特段対応なし</li>
					</ol>
					<div class="select">
						<select name="F_4_3_1_1" id="F_4_3_1_1">
							<option value=""></option>
							<option value="1"{if $items["F_4_3_1_1"] == "1"} selected="selected"{/if}>(1)</option>
							<option value="2"{if $items["F_4_3_1_1"] == "2"} selected="selected"{/if}>(2)</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<th>案内した機関</th>
				<td>
					<input name="F_4_3_1_2" id="F_4_3_1_2" type="text" class="input" placeholder="入力してください" value="{$items["F_4_3_1_2"]|escape}">
				</td>
			</tr>
			<tr>
				<th>特段対応なしの理由</th>
				<td>
					<input name="F_4_3_1_3" id="F_4_3_1_3" type="text" class="input" placeholder="入力してください" value="{$items["F_4_3_1_3"]|escape}">
				</td>
			</tr>

			<tr>
				<th colspan="2">特定技能所属機関への対応</th>
			</tr>
			<tr>
				<th>責任者への法令違反事実の通知</th>
				<td>
					<div class="select">
						<select name="F_4_3_2_1" id="F_4_3_2_1">
							<option value=""></option>
							<option value="1"{if $items["F_4_3_2_1"] == "1"} selected="selected"{/if}>通知済み</option>
							<option value="2"{if $items["F_4_3_2_1"] == "2"} selected="selected"{/if}>未通知</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<th>通知日</th>
				<td>
					<input id="F_4_3_2_2_ymd" name="F_4_3_2_2_ymd" type="text" class="input has-width-max40pct flatpickr" placeholder="日付を選択してください" value="{$items["F_4_3_2_2_ymd"]|escape}" required />
				</td>
			</tr>
			<tr>
				<th>通知の相手方</th>
				<td>
					<input name="F_4_3_2_3" id="F_4_3_2_3" type="text" class="input" placeholder="入力してください" value="{$items["F_4_3_2_3"]|escape}">
				</td>
			</tr>
			<tr>
				<th>未通知の理由</th>
				<td>
					<textarea name="F_4_3_2_4" id="F_4_3_2_4" class="textarea" placeholder="入力してください">{$items["F_4_3_2_4"]|escape}</textarea>
				</td>
			</tr>
			<tr>
				<th>法令違反事実の出入国在留管理庁への届出の案内</th>
				<td>
					<div class="select">
						<select name="F_4_3_2_5" id="F_4_3_2_5">
							<option value=""></option>
							<option value="1"{if $items["F_4_3_2_5"] == "1"} selected="selected"{/if}>案内済み</option>
							<option value="2"{if $items["F_4_3_2_5"] == "2"} selected="selected"{/if}>未了</option>
						</select>
					</div>
				</td>
			</tr>

			<tr>
				<th colspan="2">関係行政機関への対応</th>
			</tr>
			<tr>
				<th>関係行政機関への対応</th>
				<td>
					<ol class="input__subtext">
						<li>関係行政機関への通報済み</li>
						<li>関係行政機関への通報未了（通報不要と判断した場合を含む。）</li>
					</ol>
					<div class="select">
						<select name="F_4_3_3_1" id="F_4_3_3_1">
							<option value=""></option>
							<option value="1"{if $items["F_4_3_3_1"] == "1"} selected="selected"{/if}>(1)</option>
							<option value="2"{if $items["F_4_3_3_1"] == "2"} selected="selected"{/if}>(2)</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<th>通報日</th>
				<td>
					<input id="F_4_3_3_2_ymd" name="F_4_3_3_2_ymd" type="text" class="input has-width-max40pct flatpickr" placeholder="日付を選択してください" value="{$items["F_4_3_3_2_ymd"]|escape}" required />
				</td>
			</tr>
			<tr>
				<th>通報先機関</th>
				<td>
					<input name="F_4_3_3_3" id="F_4_3_3_3" type="text" class="input" placeholder="入力してください" value="{$items["F_4_3_3_3"]|escape}">
				</td>
			</tr>
			<tr>
				<th>通報未了の理由</th>
				<td>
					<textarea name="F_4_3_3_4" id="F_4_3_3_4" class="textarea" placeholder="入力してください">{$items["F_4_3_3_4"]|escape}</textarea>
				</td>
			</tr>
		</table>

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
			</div>
			<div class="column is-6 has-text-right">
				<button type="button" class="button is-primary" id="regist_button">{$labels["regist"]|escape}</button>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
