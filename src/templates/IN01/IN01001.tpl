<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/IN01/IN01001.js"></script>
</head>

<body id="IN01001">
<form id="mainForm" method="POST" action="IN01001">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}" />
<input type="hidden" name="user_id" id="user_id" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">面談記録 労働者</h2>

{* --- メッセージ欄部品 --- *}
{include file='common/message.tpl' messages=$messages}

		<div class="columns">
			<div class="column is-2 is-offset-8 has-text-right">
				<a href="IN01001" class="button is-info">労働者</a>
			</div>
			<div class="column is-2 has-text-right">
				<a href="IN02001" class="button">監督者</a>
			</div>
		</div>

		<div class="box has-padding-20">
			<div class="columns is-multiline formbox-gap">
				<!-- ID -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">労働者ID：</p>
				</div>
				<div class="column is-9">
					<input name="C_01" id="C_01" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_01"]|escape}">
				</div>

				<!-- 氏名 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">労働者氏名：</p>
				</div>
				<div class="column is-9">
					<input name="C_02" id="C_02" class="input" type="text" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_02"]|escape}">
				</div>

				<!-- 企業名 -->
{if $smarty.session.loginCompanyType != '2'}
				<input type="hidden" name="C_03" id="C_03" value="{$smarty.session.loginCompanyRowId|escape}">
{else}
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">企業名：</p>
				</div>
				<div class="column is-9">
					<div class="select has-vertical-middle has-width-100pct">
						<select name="C_03" id="C_03" class="has-width-100pct">
							<option value=""></option>
{foreach from=$companies item=$company}
							<option value="{$company["id"]}"{if $company["id"] == $items["C_03"]} selected="selected"{/if}>{$company["name"]}</option>
{/foreach}
						</select>
					</div>
				</div>
{/if}

				<!-- 登録日 -->
				<div class="column has-padding-left-10 is-3">
					<p class="is-font-bold">登録日：</p>
				</div>
				<div class="column is-9">
					<input name="C_04_1" id="C_04_1" type="text" class="input flatpickr-pastday has-width-max40pct is-inline-block"
						value="{$items["C_04_1"]|escape}" placeholder="日付を選択してください" />
					<span class="has-vertical-middle">～</span>
					<input name="C_04_2" id="C_04_2" type="text" class="input flatpickr-pastday has-width-max40pct is-inline-block is-back-white"
						value="{$items["C_04_2"]|escape}" placeholder="日付を選択してください" />
				</div>

				<!-- 未実施者も含める -->
				<div class="column is-9 is-offset-3 has-margin-top-20">
					<input name="C_05" id="C_05" type="checkbox" class="is-checkradio" {if $items["C_05"] != ""}checked="checked"{/if}>
					<label for="C_05">未実施者も含める</label>
				</div>

			</div><!--  /.columns -->

			<div class="has-text-centered has-margin-top-20">
				<a class="button is-info submit-button" id="search_button">検索</a>
			</div>
		</div>

{if isset($pageData->pageDatas)}
{if count($pageData->pageDatas) == 0}
		<div class="align-center">検索結果がありません。</div>
{else}
		<input type="hidden" name="page_no" id="page_no" value="{$pageData->currentPageNo}">
		<table class="table has-margin-top-50 has-width-100pct responsive-list">
			<thead>
				<tr>
					<th>顧客ID</th>
					<th>氏名</th>
					<th>所属機関</th>
					<th>登録日</th>
					<th>面談日</th>
{if $smarty.session.loginUserAuth <= '2'}
					<th>登録</th>
{/if}
					<th>詳細</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>顧客ID</th>
					<th>氏名</th>
					<th>所属機関</th>
					<th>登録日</th>
					<th>面談日</th>
{if $smarty.session.loginUserAuth <= '2'}
					<th>登録</th>
{/if}
					<th>詳細</th>
				</tr>
			</tfoot>
			<tbody>
{foreach from=$pageData->pageDatas item=$row}
				<tr>
					<td>{$row["user_id"]|escape}</td>
					<td>{$row["user_name"]|escape}</td>
					<td>{$row["company_name"]|escape}</td>
					<td>{$row["create_ymd"]|escape}</td>
					<td>{$row["interview_ymd"]|escape}</td>
{if $smarty.session.loginUserAuth <= '2'}
					<td>
						<button type="button" class="button is-primary regist-button" id="regist_button_{$row["user_id"]|escape}">{$labels["regist"]|escape}</button>
					</td>
{/if}
					<td>
						<button type="button" class="button is-dark detail-button" id="detail_button_{$row["user_id"]|escape}">{$labels["contents"]|escape}</button>
					</td>
				</tr>
{/foreach}
			</tbody>
		</table>

{include file='common/pager.tpl' pageData=$pageData}{* --- ページング部品 --- *}

{/if}
{/if}

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
