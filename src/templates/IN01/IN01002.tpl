<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/IN01/IN01002.js"></script>
</head>

<body id="IN01002">
<form id="mainForm" method="POST" action="IN01002">
<input type="hidden" name="mode" id="mode" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">面談記録/詳細</h2>

		<table class="table is-bordered has-margin-top-50 has-width-100pct">
			<tr>
				<th>労働者ID</th>
				<td>{$items["user_id"]|escape}</td>
			</tr>
			<tr>
				<th>特定技能外国人の氏名</th>
				<td>{$items["user_name"]|escape}</td>
			</tr>
			<tr>
				<th>特定技能所属機関の氏名又は名称</th>
				<td>{$items["company_name"]|escape}</td>
			</tr>
			<tr>
				<th>面談日</th>
				<td>{$items["interview_ymd"]|escape}</td>
			</tr>
		</table>

		<h3 class="title is-5 has-margin-top-50">面談対応者</h3>
		<table class="table is-bordered is-bordered has-width-100pct">
			<tr>
				<th>対応者の氏名</th>
				<td>{$items["E_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>対応者の役職</th>
				<td>{$items["E_2_2"]|escape}</td>
			</tr>
			<tr>
				<th>役職名</th>
				<td>{$items["E_2_3"]|escape}</td>
			</tr>
		</table>

		<h3 class="title is-5 has-margin-top-50">面談結果</h3>
		<table class="table is-bordered is-bordered has-width-100pct">
			<tr>
				<th colspan="2">業務内容に関する事項</th>
			</tr>
			<tr>
				<th>雇用契約と異なる業務に従事していないこと。</th>
				<td>{$items["E_3_1_1_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_1_1_2"]|escape}</td>
			</tr>
			<tr>
				<th>他の事業主の下で業務に従事していないこと。</th>
				<td>{$items["E_3_1_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_1_2_2"]|escape}</td>
			</tr>
			<tr>
				<th>安全衛生に配慮して適切に業務を行っていること。</th>
				<td>{$items["E_3_1_3_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_1_3_2"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">待遇に関する事項</th>
			</tr>
			<tr>
				<th>雇用契約に基づき毎月適切に報酬を受け取っていること。</th>
				<td>{$items["E_3_2_1_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_2_1_2"]|escape}</td>
			</tr>
			<tr>
				<th>雇用契約と異なる労働時間となっていないこと。</th>
				<td>{$items["E_3_2_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_2_2_2"]|escape}</td>
			</tr>
			<tr>
				<th>休日，休暇等が適切に付与されていること（一時帰国休暇を含む。）。</th>
				<td>{$items["E_3_2_3_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_2_3_2"]|escape}</td>
			</tr>
			<tr>
				<th>適切な住居が確保されていること。</th>
				<td>{$items["E_3_2_4_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_2_4_2"]|escape}</td>
			</tr>
			<tr>
				<th>定期的に負担する食費，居住費等が合意したとおりの内容であること。</th>
				<td>{$items["E_3_2_5_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_2_5_2"]|escape}</td>
			</tr>
			<tr>
				<th>支援計画にのっとった支援の提供を受けていること。</th>
				<td>{$items["E_3_2_6_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_2_6_2"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">保護に関する事項</th>
			</tr>
			<tr>
				<th>暴行・脅迫・監禁等の不法行為を受けていないこと。</th>
				<td>{$items["E_3_3_1_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_3_1_2"]|escape}</td>
			</tr>
			<tr>
				<th>相手方を問わず保証金の徴収・違約金を定める契約等がないこと。</th>
				<td>{$items["E_3_3_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_3_2_2"]|escape}</td>
			</tr>
			<tr>
				<th>預金通帳の管理など不当な財産管理を受けていないこと。</th>
				<td>{$items["E_3_3_3_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_3_3_2"]|escape}</td>
			</tr>
			<tr>
				<th>旅券・在留カードを自分で保管していること。</th>
				<td>{$items["E_3_3_4_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_3_4_2"]|escape}</td>
			</tr>
			<tr>
				<th>私生活上の自由を不当に制限されていないこと。</th>
				<td>{$items["E_3_3_5_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_3_5_2"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">生活に関する事項</th>
			</tr>
			<tr>
				<th>日常生活においてトラブルが発生していないこと。</th>
				<td>{$items["E_3_4_1_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_4_1_2"]|escape}</td>
			</tr>
			<tr>
				<th>健康状態に異常がないこと。</th>
				<td>{$items["E_3_4_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_4_2_2"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">その他の事項</th>
			</tr>
			<tr>
				<th>不法就労者が働いていないこと。</th>
				<td>{$items["E_3_5_1_1"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_5_1_2"]|escape}</td>
			</tr>
			<tr>
				<th>その他</th>
				<td>{$items["E_3_5_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>その他の内容</th>
				<td>{$items["E_3_5_2_2"]|escape}</td>
			</tr>
			<tr>
				<th>問題の内容</th>
				<td>{$items["E_3_5_2_3"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">法令違反等の有無</th>
			</tr>
			<tr>
				<th>法令違反等の有無</th>
				<td>{$items["E_3_6"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">その他特筆すべき事項</th>
			</tr>
			<tr>
				<td colspan="2">{$items["E_3_7"]|escape|nl2br|default:'&nbsp;'}</td>
			</tr>
		</table>


		<!-- 法令違反が発生 -->
		<h3 class="title is-5">法令違反への対応</h3>

		<table class="table is-bordered">
			<tr>
				<th>法令違反事実の発生年月</th>
				<td>{$items["E_4_1_ymd"]|escape}</td>
			</tr>
			<tr>
				<th>法令違反事実の内容</th>
				<td>{$items["E_4_2"]|escape}</td>
			</tr>
			<tr>
				<th colspan="2">法令違反事実への対応結果</th>
			</tr>
			<tr>
				<th colspan="2">1号特定技能外国人への対応</th>
			</tr>
			<tr>
				<th>1号特定技能外国人への対応</th>
				<td>{$items["E_4_3_1_1"]|escape}</td>
			</tr>
			<tr>
				<th>案内した機関</th>
				<td>{$items["E_4_3_1_2"]|escape}</td>
			</tr>
			<tr>
				<th>特段対応なしの理由</th>
				<td>{$items["E_4_3_1_3"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">特定技能所属機関への対応</th>
			</tr>
			<tr>
				<th>責任者への法令違反事実の通知</th>
				<td>{$items["E_4_3_2_1"]|escape}</td>
			</tr>
			<tr>
				<th>通知日</th>
				<td>{$items["E_4_3_2_2_ymd"]|escape}</td>
			</tr>
			<tr>
				<th>通知の相手方</th>
				<td>{$items["E_4_3_2_3"]|escape}</td>
			</tr>
			<tr>
				<th>未通知の理由</th>
				<td>{$items["E_4_3_2_4"]|escape|nl2br}</td>
			</tr>
			<tr>
				<th>法令違反事実の出入国在留管理庁への届出の案内</th>
				<td>{$items["E_4_3_2_5"]|escape}</td>
			</tr>

			<tr>
				<th colspan="2">関係行政機関への対応</th>
			</tr>
			<tr>
				<th>関係行政機関への対応</th>
				<td>{$items["E_4_3_3_1"]|escape}</td>
			</tr>
			<tr>
				<th>通報日</th>
				<td>{$items["E_4_3_3_2_ymd"]|escape}</td>
			</tr>
			<tr>
				<th>通報先機関</th>
				<td>{$items["E_4_3_3_3"]|escape}</td>
			</tr>
			<tr>
				<th>通報未了の理由</th>
				<td>{$items["E_4_3_3_4"]|escape|nl2br}</td>
			</tr>
		</table>

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
