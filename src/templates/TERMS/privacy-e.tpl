<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
</head>

<body id="">
	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<div class="columns">
			<div class="column is-6 is-offset-3">
				<div id="btm">
					<h2>●個人情報の取り扱いについて●</h2>
					<pre id="rules" class="privacy-contents-en" style="
    border: 1px solid #CCCCCC;
    padding: 30px 25px;
    overflow: scroll;
    /* margin-bottom: 25px; */
    height: 600px;
    white-space: pre-wrap;
    background-color: white;
">
個人情報の取り扱いについて
株式会社廣済堂ビジネスサポート(以下「弊社」といいます)　では、「Talent　Asia(タレントアジア)」(以下「本サービス」といいます)で会員登録を行ったうえで会員向けサービスをご利用されるみなさま（利用規約「会員」で定義されるもの。以下「会員」といいます）のプライバシーを尊重し、細心の注意を払い、責任を持って個人情報を管理・取り扱い致します。
個人情報とは、氏名、住所、電話番号、年齢、メールアドレス、その他の記述などにより、特定の個人を識別できる情報(他の情報と容易に照合することができ、それにより識別できるものも含む)をいいます。

いただいた個人情報は、以下の目的で使用致します。
・会員への本サービスの提供
・本サービスにおける個人認証
・会員への通知、問い合わせ・相談等に対する回答等
・会員の雇用主への提供
※雇用主に提供された個人情報は、雇用主の個人情報の取り扱いに関する定めに則り管理されることになります。
※雇用主は、提供された個人情報を元に在留資格関連書類等に利用致します。
・本サービスその他の弊社運用サービスのご案内の提供
・本サービスの継続的な品質改善および新規サービスの開発
・アンケートの依頼
・弊社が提供するサービスの説明をする際など合法的な目的のために、企業そのほかの第三者に開示する会員の統計情報
※これらの統計情報には、箇々の回答者を識別できる情報は含まれません。

弊社は、ご提供いただいた個人情報を正確に処理するように努めます。

弊社は、弊社が信頼に足ると判断した外部の企業に、個人情報のデータ処理を委託することがあります。

弊社は、個人を識別し連絡をとるために、登録情報の確認をさせていただくことがあります。会員の氏名、住所、電話番号、年齢、メールアドレスなど、個人の属性に関する情報をお聞きすることがあります。必ずしもすべての質問にお応えいただく必要はありませんが、特定の質問に回答がない場合、利用できないサービスもあります。
ご提供いただいた内容は、会員ご本人の承諾なしに第三者に開示することはありません。
ただし、以下のような場合は、例外として情報を開示できるものと致します。
・法令の規定による場合
・会員または公衆の生命、健康、財産等の重大な利益を保護するために必要な場合

会員登録などTalent　Asiaよりご提供いただいた個人情報については、以下の内容に従って、共同利用することがあります。
・共同利用者の範囲は、当社、会員の雇用主及び特定技能外国人に対する登録支援機関など、会員・その雇用主のサポートを行う団体等に限ります。
・共同利用する個人情報の項目は、氏名、住所、電話番号、年齢、メールアドレス、その他Talent　Asiaよりご提供いただいた個人情報です。
・共同利用する個人情報の当社の利用目的は、上記利用目的の通りです。

お預かりした個人情報は、原則として会員ご本人、または代理人にかぎり、開示・訂正・削除を求めることができます。
Talent　Asiaでは、会員の利便性の向上のため、Cookie(クッキー)を使用します。そのCookieには個人情報が含まれますが、通信時はセキュリティで保護されており、最終の利用完了後から60分経過後にCookie上の個人情報はクリアされます。また、Talent　Asiaの広告の配信および統計データの取得のためCookieを使用しますが、取得のため使用するCookieの中には個人情報は含まれません。
一定期間(12ヶ月)以上ご利用が無い場合、弊社が一方的に個人情報を破棄(登録情報抹消)することがあります。

弊社の過失に起因しないID・パスワードの第三者使用によって生じる損害被害は、全て会員が負担するものとし、弊社は、その責任を負わないものとします。
Talent　Asiaは、デジタルIDを利用した暗号化通信機能(SSL)を利用しています。この機能を利用することにより、会員のプライバシーを損なうことなく、会員の情報を受け取ることが可能となります。

■個人情報保護管理者の表示
株式会社　廣済堂ビジネスサポート
個人情報保護管理者　門間　貴之
〒105-8318　東京都港区芝浦一丁目2番3号　シーバンスS館 13階
TEL：03-3438-1185
※個人情報管理についての連絡先、個人情報管理についてのお問い合わせは下記までお願いします。

■個人情報に関するお問い合わせ窓口
株式会社 廣済堂ビジネスサポート　お客様相談窓口
〒984-0806　　宮城県仙台市若林区舟丁18－2
TEL:022-714-6166　FAX: 022-714-6167
e-mail: kbs_vn@kosaido.co.jp
※株式会社廣済堂ビジネスサポートは「プライバシーマーク」使用許諾事業者として認定されています。

<a href="https://www.kosaido-biz.co.jp/privacy/" target="_blank">個人情報保護方針</a>
</pre>
				</div>
			</div>
		</div>
    </div>

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</body>
</html>
