<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/QA01/QA01005.js"></script>
</head>

<body id="QA01005">
	<form id="mainForm" enctype="multipart/form-data" method="POST" action="QA01005">
	<input type="hidden" name="mode" id="mode" />
	<input type="hidden" name="qa_id" id="qa_id" value="{$items["qa_id"]|escape}" />

	<input type="hidden" name="page_no" id="page_no" value="{$items["page_no"]|escape}"  />
	<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}"  />

	<input type="hidden" name="C_01" id="C_01" value="{$items["C_01"]|escape}"  />
	<input type="hidden" name="C_02" id="C_02" value="{$items["C_02"]|escape}"  />
	<input type="hidden" name="C_03" id="C_03" value="{$items["C_03"]|escape}"  />
	<input type="hidden" name="C_04_1" id="C_04_1" value="{$items["C_04_1"]|escape}"  />
	<input type="hidden" name="C_04_2" id="C_04_2" value="{$items["C_04_2"]|escape}"  />
	<input type="hidden" name="C_05_1" id="C_05_1" value="{$items["C_05_1"]|escape}"  />
	<input type="hidden" name="C_05_2" id="C_05_2" value="{$items["C_05_2"]|escape}"  />
	<input type="hidden" name="C_05_3" id="C_05_3" value="{$items["C_05_3"]|escape}"  />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">Q&A/詳細</h2>
<!--// 対象QAID：：{$items["qa_id"]|escape}</br> //-->
		<!-- 配信言語 -->
		<div class="columns is-multiline formbox-gap has-text-right">
		</div>
		<div class="field has-text-right">
			<label for="Secction01">[{$rowsData["release_status_name"]|escape}]</label>
		</div>

		<table class="table table-conf is-bordered has-width-100pct">

			<tr>
				<th>質問No</th>
				<td >{$rowsData["question_no"]|escape}</td>
				<th>配信言語</th>
				<td>
{foreach from=$displangs key=$pkey item=$pname}
					{if $pkey == $rowsData["delivery_language"]}{$pname}{/if}
{/foreach}
				</td>
			</tr>

			<tr>
				<th>作成日</th>
				<td>{$rowsData["qa_create_date"]|escape}</td>
				<th>最終更新日</th>
				<td>{$rowsData["qa_update_date"]|escape}</td>
			</tr>

			<tr>
				<th>作成者</th>
				<td>{$rowsData["cre_user"]|escape}</td>
				<th>更新者</th>
				<td>{$rowsData["upd_user"]|escape}</td>
			</tr>

			<tr>
				<th class="has-vertical-top">質問内容</th>
				<td colspan="3">
					<p>{$rowsData["question_contents"]|escape}</p>
				</td>
			</tr>

			<tr>
				<th class="has-vertical-top">回答内容</th>
				<td colspan="3">
					<p>{$rowsData["answer_contents"]|escape}</p>
				</td>
			</tr>
		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark return-button" id="return-button">{$labels["back"]|escape}</button>
			</div>
{if $smarty.session.loginUserAuth <= '2'}
			<div class="column is-6 has-text-right">
				<button type="button" class="button is-primary update-button" id="update-button">{$labels["edit"]|escape}</button>
			</div>
{/if}
		</div>


	</div> <!-- /.contents -->


	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</form>
</body>
</html>
