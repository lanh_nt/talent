<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/QA01/QA01002.js"></script>
</head>

<body id="QA01002">
	<form id="mainForm" enctype="multipart/form-data" method="POST" action="QA01002">
	<input type="hidden" name="mode" id="mode" />
	<input type="hidden" name="qa_id" id="qa_id" value="{$items["qa_id"]|escape}" />
	<input type="hidden" name="csrf_token" value="{$smarty.session["csrf_token"]}" />

	<input type="hidden" name="page_no" id="page_no" value="{$items["page_no"]|escape}"  />
	<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}"  />

	<input type="hidden" name="C_01" id="C_01" value="{$items["C_01"]|escape}"  />
	<input type="hidden" name="C_02" id="C_02" value="{$items["C_02"]|escape}"  />
	<input type="hidden" name="C_03" id="C_03" value="{$items["C_03"]|escape}"  />
	<input type="hidden" name="C_04_1" id="C_04_1" value="{$items["C_04_1"]|escape}"  />
	<input type="hidden" name="C_04_2" id="C_04_2" value="{$items["C_04_2"]|escape}"  />
	<input type="hidden" name="C_05_1" id="C_05_1" value="{$items["C_05_1"]|escape}"  />
	<input type="hidden" name="C_05_2" id="C_05_2" value="{$items["C_05_2"]|escape}"  />
	<input type="hidden" name="C_05_3" id="C_05_3" value="{$items["C_05_3"]|escape}"  />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">

		<h2 class="title is-4">Q&A管理/新規登録　編集</h2>

{* --- メッセージ欄部品 --- *}
{include file='common/message.tpl' messages=$messages}
		<table class="table is-bordered has-width-100pct">

			<tr>
				<th>質問NO</th>
				<td>
						<input class="input" type="text" name="question_no" id="question_no" placeholder="{$labels["placeholder"]|escape}" value="{$items["question_no"]|escape}">
				</td>
			</tr>
{if $smarty.session.loginCompanyType != '2'}
			<input type="hidden" name="c_name" id="c_name" value="{$smarty.session.loginCompanyRowId|escape}">
{else}
			<tr>
				<th>企業名</th>
				<td>
					<div class="select has-vertical-middle">
						<select name="c_name" id="c_name" class="has-width-100pct">
							<option value="">選択必須です</option>
{foreach from=$companies item=$company}
							<option value="{$company["id"]}"{if $company["id"] == $items["c_name"]} selected="selected"{/if}>{$company["name"]}</option>
{/foreach}
						</select>
					</div>
				</td>
			</tr>
{/if}
			<tr>
				<th>表示言語</th>
				<td>
					<div class="select has-vertical-middle">
						<select name="disp_lang" id="disp_lang" class="has-width-100pct">
							<option value="">選択必須です</option>
{foreach from=$displangs key=$pkey item=$pname}
							<option value="{$pkey}" {if $pkey == $items["disp_lang"]} selected="selected"{/if}>{$pname}</option>
{/foreach}
						</select>
					</div>
				</td>
			</tr>

			<tr>
				<th>公開状況</th>
				<td>
					<div class="select has-vertical-middle">
						<select name="release_status" id="release_status">
{foreach from=$release_statuses key=$pkey item=$pname}
							<option value="{$pkey}" {if $pkey == $items["release_status"]} selected="selected"{/if}>{$pname}</option>
{/foreach}
						</select>
					</div>
				</td>
			</tr>

			<!-- 本文 -->
			<tr>
				<th class="has-vertical-top">質問内容</th>
				<td><textarea name="question_contents" id="question_contents" class="textarea" placeholder="入力してください">{$items["question_contents"]}</textarea></td>
			</tr>
			<tr>
				<th class="has-vertical-top">回答内容</th>
				<td><textarea name="answer_contents" id="answer_contents" class="textarea" placeholder="入力してください">{$items["answer_contents"]}</textarea></td>
			</tr>
		</table><!--  /.table -->

		<div class="columns is-mobile has-margin-top-30">
			<div class="column is-6 has-text-left">
				<button type="button" class="button is-dark return-button" id="return-button">{$labels["back"]|escape}</button>
			</div>
			<div class="column is-6 has-text-right">
				<button type="button" class="button is-primary update-button" id="update-button">{$labels["update"]|escape}</button>
			</div>
		</div>

	</div> <!-- /.contents -->

	<!-- 配信対象者検索用モーダル -->
	<div id="inline" style="display:none;">
		<p>配信対象者を選択してください。</p>

		<div class="has-margin-top-30">
			<input class="is-checkradio" id="target01" type="radio" name="target">
			<label for="target01">全員に配信する</label>
		</div>

		<div class="has-margin-top-20">
			<input class="is-checkradio" id="target02" type="radio" name="target">
			<label for="target02">個人名から選択する</label>
		</div>

		<section class="has-margin-top-10">
			<ul class="info__selectlist">
				<li>
					<p class="is-font-bold has-padding-left-30">労働者ID：名前</p>
				</li>
				<li>
					<input class="is-checkradio" id="list01" type="checkbox" name="list[]">
					<label for="list01">00001：David Andrew Buchanan</label>
				</li>
				<li>
					<input class="is-checkradio" id="list02" type="checkbox" name="list[]">
					<label for="list02">00002：Wladimir Ramon Balentien</label>
				</li>
				<li>
					<input class="is-checkradio" id="list03" type="checkbox" name="list[]">
					<label for="list03">00003：Albert Joe Suarez Subero</label>
				</li>
				<li>
					<input class="is-checkradio" id="list04" type="checkbox" name="list[]">
					<label for="list04">00004：David Gregory Huff</label>
				</li>
				<li>
					<input class="is-checkradio" id="list05" type="checkbox" name="list[]">
					<label for="list05">00005：Ernesto Antonio Mejía Alvarado</label>
				</li>
				<li>
					<input class="is-checkradio" id="list06" type="checkbox" name="list[]">
					<label for="list06">00006：Deunte Raymon Heath</label>
				</li>
				<li>
					<input class="is-checkradio" id="list07" type="checkbox" name="list[]">
					<label for="list07">00007：Zachary Sheridan Neal</label>
				</li>
				<li>
					<input class="is-checkradio" id="list08" type="checkbox" name="list[]">
					<label for="list08">00008：Kyle Jared Martin</label>
				</li>
			</ul>
		</section>

		<div class="has-text-centered has-margin-top-50">
			<button class="button is-primary">決定</button>
		</div>
	</div>

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>

<script>
$('.modaal').modaal({
	overlay_close: false, // 背景押下時にクローズさせるか
});
</script>

</body>
</html>