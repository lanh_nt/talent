<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>特定技能外国人材申請支援システム</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../assets/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/QA01/QA01001.js"></script>
</head>

<body id="QA01001">
<form id="mainForm" enctype="multipart/form-data" method="POST" action="QA01001">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="is_search" id="is_search" value="{$items["is_search"]|escape}" />
<input type="hidden" name="qa_id" id="qa_id" />
<input type="hidden" name="qa_arr" id="qa_arr" />

	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>

	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">Q&A管理</h2>

{* --- メッセージ欄部品 --- *}
{include file='common/message.tpl' messages=$messages}

{if $smarty.session.loginUserAuth <= '2'}
		<div class="has-text-right has-margin-bottom-20">
			<input type="file" name="upfile" id="upfile" style="display:none;" />
			<button type="button" class="button is-primary has-margin-right-20" id="csv_import_button">{$labels["csv_import"]|escape}</button>
			<button type="button" class="button is-primary has-margin-right-20" id="csv_export_button">{$labels["csv_export"]|escape}</button>
			<button type="button" class="button is-primary has-margin-right-20" id="csv_format_button">{$labels["csv_format"]|escape}</button>
		</div>
{/if}

		<div class="box has-padding-20">
			<div class="columns is-multiline formbox-gap">
				<!-- 質問内容 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">質問内容:</p>
				</div>
				<div class="column is-10">
					<input class="input" type="text" name="C_01" id="C_01" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_01"]|escape}">
				</div>

				<!-- 回答内容： -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">回答内容：</p>
				</div>
				<div class="column is-10">
					<input class="input" type="text" name="C_02" id="C_02" placeholder="{$labels["placeholder"]|escape}" value="{$items["C_02"]|escape}">
				</div>

				<!-- 企業名 -->
{if $smarty.session.loginCompanyType != '2'}
				<input type="hidden" name="C_03" id="C_03" value="{$smarty.session.loginCompanyRowId|escape}">
{else}
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">企業名：</p>
				</div>
				<div class="column is-9">
					<div class="select has-vertical-middle has-width-100pct">
						<select name="C_03" id="C_03" class="has-width-100pct">
							<option value=""></option>
{foreach from=$companies item=$company}
							<option value="{$company["id"]}"{if $company["id"] == $items["C_03"]} selected="selected"{/if}>{$company["name"]}</option>
{/foreach}
						</select>
					</div>
				</div>
{/if}

				<!-- 登録日 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">登録日：</p>
				</div>
				<div class="column is-9">
					<input id="C_04_1" name="C_04_1" type="text" class="input has-width-max40pct is-inline-block flatpickr" value="{$items["C_04_1"]|escape}" required />
					<span class="has-vertical-middle">～</span>
					<input id="C_04_2" name="C_04_2" type="text" class="input has-width-max40pct is-inline-block flatpickr is-back-white" value="{$items["C_04_2"]|escape}" required />
				</div>

				<!-- 種別 -->
				<div class="column has-padding-left-10 is-2">
					<p class="is-font-bold">公開状況：</p>
				</div>
				<div class="column is-10">
					<div class="field">
						<input class="is-checkradio" type="checkbox" name="C_05_1" id="C_05_1" {if $items["C_05_1"] != ""}checked="checked"{/if}>
						<label for="C_05_1">公開中</label>
						<input class="is-checkradio" type="checkbox" name="C_05_2" id="C_05_2" {if $items["C_05_2"] != ""}checked="checked"{/if}>
						<label for="C_05_2">作成中</label>
						<input class="is-checkradio" type="checkbox" name="C_05_3" id="C_05_3" {if $items["C_05_3"] != ""}checked="checked"{/if}>
						<label for="C_05_3">公開終了</label>

					</div>
				</div>
			</div><!--  /.columns -->
			<div class="has-text-centered has-margin-top-20">
				<a class="button is-info submit-button" id="search_button">検索</a>
			</div>
		</div>

{if isset($pageData->pageDatas)}
{if count($pageData->pageDatas) == 0}
		<div class="align-center">検索結果がありません。</div>
{else}

{if $smarty.session.loginUserAuth <= '2'}
		<div class="has-text-right has-margin-top-50">
			<a class="button is-info seqchange-button" id="seqchange-button">表示順変更</a>
		</div>
{/if}
		<input type="hidden" name="page_no" id="page_no" value="{$pageData->currentPageNo}"/>
		<table class="table has-margin-top-20 has-width-100pct responsive-list">
			<thead>
				<tr>
					<th>ID</th>
					<th>質問内容</th>
					<th>回答内容</th>
					<th>公開状況</th>
					<th>登録日</th>
					<th>詳細</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>ID</th>
					<th>質問内容</th>
					<th>回答内容</th>
					<th>公開状況</th>
					<th>登録日</th>
					<th>詳細</th>
				</tr>
			</tfoot>
			<tbody id="sortdata1">
{foreach from=$pageData->pageDatas item=$row}
				<tr>
					<td>{$row["question_no"]|escape}<input type="hidden" name="seq_no" value="{$row["qa_id"]|escape}"></td>
					<td>{$row["question_contents"]|escape}</td>
					<td>{$row["answer_contents"]|escape}</td>
					<td>{$row["release_status_name"]|escape}</td>
					<td>{$row["qa_create_date"]|escape}</td>
					<td>
						<button type="button" class="button is-dark detail-button" id="detail_button_{$row["qa_id"]|escape}">{$labels["contents"]|escape}</button>
					</td>
				</tr>
{/foreach}
			</tbody>
		</table>

{include file='common/pager.tpl' pageData=$pageData}{* --- ページング部品 --- *}

{/if}
{/if}

	</div> <!-- /.contents -->

	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
<script>
	var arr = [];
	$('[name="seq_no"]').each(function(){
    	arr.push($(this).val());
	});

	//順序変更
	$('#sortdata1').sortable();
	// sortstopイベントをバインド
	$('#sortdata1').bind('sortstop',function(){
		// 番号を設定している要素に対しループ処理
		$(this).find('[name="seq_no"]').each(function(idx){
			// タグ内に通し番号を設定（idxは0始まりなので+1する）
			//$(this).html(idx+1);
			$( this ).next( 'input[type="text"]' ).val(idx+1);
		});

		arr = [];
		$('[name="seq_no"]').each(function(){
			arr.push($(this).val());
		});

		$('[name="qa_arr"]').val(arr);
		//alert($('[name="qa_arr"]').val());
	});
</script>
</body>
</html>
