<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<!-- アコーディオン用 -->
<script src="../assets/js/handy-collapse.js"></script>
{literal}
<script>
document.addEventListener(
    "DOMContentLoaded",
    () => {
		//Basic Accordion
		const basic = new HandyCollapse();
		//Nested
		const nested = new HandyCollapse({
		nameSpace: "nested",
		closeOthers: false
		});
		//Callback
		const callback = new HandyCollapse({
			nameSpace: "callback",
			closeOthers: false,
			onSlideStart: (isOpen, contentID) => {
				console.log(isOpen);
				const buttonEl = document.querySelector(`[data-callback-control='${contentID}']`);
				if (!buttonEl) return;
				if (isOpen) {
					buttonEl.textContent = "Opened " + contentID;
				} else {
				buttonEl.textContent = "Closed " + contentID;
				}
			}
		});
		//Callback
		const tab = new HandyCollapse({
			nameSpace: "tab",
			closeOthers: true,
			isAnimation: false
		});
	},
	false
);
</script>
{/literal}

</head>

<body id="QA02001">
	<!-- header読込部 -->
	<header id="header">
{if $smarty.session.loginUserType == '1'}{include file='common/header-admin.tpl'}{else}{include file='common/header-worker.tpl'}{/if}
	</header>


	<!-- メインコンテンツ -->
	<div class="contents">
		<h2 class="title is-4">{$labels["L-title"]|escape}</h2>
				<p class="has-text-left has-margin-top-20">
					{$labels["L-01-01"]|escape}
					<br>
					{$labels["L-01-02"]|escape}
				</p>

			<section class="section">
			 
{foreach from=$rowsDatas item=$row name='QA_LOOP'}
			<button type="button" class="has-margin-top-10 has-text-left button is-primary is-fullwidth is-medium" data-hc-control="content{$smarty.foreach.QA_LOOP.iteration}">
				Q.{$row["question_contents"]|escape}
			</button>
			{if $smarty.foreach.QA_LOOP.iteration==1}
			  <div class="is-active" data-hc-content="content{$smarty.foreach.QA_LOOP.iteration}">
			{else}
			  <div class="" data-hc-content="content{$smarty.foreach.QA_LOOP.iteration}">
			{/if}
			    <div class="box">
			      <h3>A.{$labels["L-01-03"]|escape}</h3>
			      <p>{$row["answer_contents"]|escape}</p>
			    </div>
			  </div>
{/foreach}
			</section>

	</div> <!-- /.contents -->


	<!-- footer読込部 -->
	<footer id="footer">
{include file='common/footer.tpl'}
	</footer>
</body>
</html>
