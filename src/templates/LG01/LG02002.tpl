<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<title>{$labels["menu-title"]|escape}</title>
<link rel="shortcut icon" href="../favicon.ico">

<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<script type="text/javascript" src="../assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/flatpickr.js"></script>
<script type="text/javascript" src="../assets/js/modaal.min.js"></script>
<script type="text/javascript" src="../assets/js/functions.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/LG01/LG02002.js"></script>
</head>

<body id="C-LG01002">
<form id="mainForm" method="POST" action="LG02002">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="loginId" value="{$items.loginId|escape}" />

	<!-- メインコンテンツ -->
	<div class="contents">

		<div class="columns has-margin-10">
			<div class="column is-8 is-offset-2">
				<div class="box has-margin-top-50">
					<h2 class="has-text-centered title is-4">{$labels["L-title"]|escape}</h2>

{include file='common/message.tpl' messages=$messages}{* --- メッセージ欄部品 --- *}

					<div class="has-margin-top-40">
						<div class="id__block">
							<p>{$items.loginId|escape}</p>
						</div>
					</div>

					<div class="has-margin-top-40">
						<div class="is-relative">
							<p class='login__text'>{$labels["L-01"]|escape}</p>
						</div>
						<input type="password" class="input" name="pwd" id="pwd">
						<a href="#" id="pwdreset" class="is-font-small">{$labels["L-02"]|escape}</a>
					</div>

					<div class="columns is-mobile has-margin-top-80">
						<div class="column has-text-left">
							<button type="button" class="button is-dark" id="back_button">{$labels["back"]|escape}</button>
						</div>
						<div class="column has-text-right">
							<button type="button" class="button is-dark" id="next_button">{$labels["next"]|escape}</button>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div> <!-- ./contents -->

</form>
</body>
</html>
